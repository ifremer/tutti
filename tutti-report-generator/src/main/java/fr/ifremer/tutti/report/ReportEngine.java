package fr.ifremer.tutti.report;

/*
 * #%L
 * Tutti :: Report Generator
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.eclipse.birt.core.exception.BirtException;
import org.eclipse.birt.core.framework.Platform;
import org.eclipse.birt.report.engine.api.EngineConfig;
import org.eclipse.birt.report.engine.api.EngineConstants;
import org.eclipse.birt.report.engine.api.EngineException;
import org.eclipse.birt.report.engine.api.IRenderOption;
import org.eclipse.birt.report.engine.api.IReportEngine;
import org.eclipse.birt.report.engine.api.IReportEngineFactory;
import org.eclipse.birt.report.engine.api.IReportRunnable;
import org.eclipse.birt.report.engine.api.IRunAndRenderTask;
import org.eclipse.birt.report.engine.api.RenderOption;

import java.io.Closeable;
import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;

/**
 * Created on 3/10/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.13.2
 */
public class ReportEngine implements Closeable {

    private static final int RENDER_DPI = 300;

    private final IReportEngine engine;

    public ReportEngine(File reportHomeDirectory, File reportLogDirectory) {

        EngineConfig engineConfig = new EngineConfig();
        engineConfig.setBIRTHome(reportHomeDirectory.getAbsolutePath());
        engineConfig.setResourcePath(reportHomeDirectory.getAbsolutePath());
        engineConfig.setLogConfig(reportLogDirectory.getAbsolutePath(), Level.ALL);

        try {
            Platform.startup(engineConfig);
        } catch (BirtException e) {
            throw new RuntimeException("Could not init birt", e);
        }

        IReportEngineFactory factory = (IReportEngineFactory) Platform.createFactoryObject(IReportEngineFactory.EXTENSION_REPORT_ENGINE_FACTORY);

        engine = factory.createReportEngine(engineConfig);

    }

    public void generate(ReportConfig reportConfig) {

        IReportRunnable rptDoc;

        try {
            rptDoc = engine.openReportDesign(reportConfig.getReportFile().getAbsolutePath());
        } catch (EngineException e) {

            throw new RuntimeException("Could not get report " + reportConfig.getReportFile(), e);

        }

        /**
         * rpt-param_dirfile : répertoire contant les fichiers
         + rpt-param_codestation : code de la station (ex: G101)
         + rpt-param_numerotrait : code du trait (ex: 53)
         */

        // Create the render task
        IRunAndRenderTask task = engine.createRunAndRenderTask(rptDoc);

        try {

            // Configure the render options
            IRenderOption renderOption = new RenderOption();
            renderOption.setOutputFileName(reportConfig.getOutputFile().getAbsolutePath());
            renderOption.setOutputFormat(IRenderOption.OUTPUT_FORMAT_PDF);

            task.setRenderOption(renderOption);

            // Report context
            Map<String, Object> context = new HashMap<>();
            context.put(EngineConstants.APPCONTEXT_CHART_RESOLUTION, RENDER_DPI);

            task.setAppContext(context);
            task.setParameterValue("rpt-param_dirfile", reportConfig.getDataDirectory().getAbsolutePath());
            task.setParameterValue("rpt-param_codestation", reportConfig.getStationNumber());
            task.setParameterValue("rpt-param_numerotrait", reportConfig.getFishingOperationNumber());

            task.run();

        } catch (EngineException e) {

            throw new RuntimeException("Could not execute report", e);

        } finally {

            // close task
            task.close();

        }

    }

    @Override
    public void close() {

        try {
            engine.destroy();
        } finally {

            Platform.shutdown();

        }

    }
}

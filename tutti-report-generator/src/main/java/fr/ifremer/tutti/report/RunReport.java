package fr.ifremer.tutti.report;

/*
 * #%L
 * Tutti :: Report Generator
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.io.File;

/**
 * Created on 3/10/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.13.2
 */
public class RunReport {

    public static void main(String... args) {

        if (args.length != 7) {

            String help = RunReport.class.getName() + " requires 6 arguments:" +
                          "\n\tReport Home directory"
                          + "\n\tReport Log directory"
                          + "\n\tReport file to use"
                          + "\n\tOutput file to generate"
                          + "\n\trpt-param_dirfile : répertoire contant les fichiers de données"
                          + "\n\trpt-param_codestation : code de la station (ex: G101)"
                          + "\n\trpt-param_numerotrait : code du trait (ex: 53)";

            throw new IllegalArgumentException(help);
        }

        File reportHomeDirectory = new File(args[0]);
        File reportLogDirectory = new File(args[1]);
        File reportFile = new File(args[2]);
        File outputFile = new File(args[3]);
        File dataDirectory = new File(args[4]);
        String stationNumber = args[5];
        int fishingOperationNumber = Integer.valueOf(args[6]);

        ReportConfig model = new ReportConfig(reportFile, dataDirectory, outputFile, stationNumber, fishingOperationNumber);

        try (ReportEngine engine = new ReportEngine(reportHomeDirectory, reportLogDirectory)) {

            engine.generate(model);

        }

    }

}

.. -
.. * #%L
.. * Tutti
.. * $Id$
.. * $HeadURL$
.. * %%
.. * Copyright (C) 2012 - 2014 Ifremer
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU General Public License as
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/gpl-3.0.html>.
.. * #L%
.. -

===================
Tutti - Référentiel
===================

Présentation
~~~~~~~~~~~~

Toutes les requetes spécifiques à l'application sont détaillées dans le `document de spécifications`_.

Consulter les différentes sections **7.2 Alimentation du référentiel** de ce document pour plus de détails.

.. _document de spécifications: ./help/AllegroCampagne-Specifications.pdf
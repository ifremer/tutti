.. -
.. * #%L
.. * Tutti
.. * $Id$
.. * $HeadURL$
.. * %%
.. * Copyright (C) 2012 - 2014 Ifremer
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU General Public License as
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/gpl-3.0.html>.
.. * #L%
.. -

===============
Fonctionnalités
===============

Abstract
--------

Ce document décrit les fonctionnalités de **Tutti**, à savoir:

- `Gestionnaire de base`_
- `Gestionnaire de référentiel`_
- `Gestionnaire de protocole`_

Gestionnaire de base
--------------------

Tutti utilise une base de travail compatible **Allegro**, l'application permet
de gérer ces bases, à savoir :

- Installer une base à partir d'une url distante via le mécanisme de mise à jour intégré.
- Mise à jour automatique via le mécanisme de mise à jour intégré (mis à jour des référentiels).
- Exporter les données de Tutti  (base de travail / pièces-jointes) (sous forme d'archive zip).
- Exporter les données de Tutti puis les supprimer , permet alors de pouvoir installer une nouvelle base ou importer des données de Tutti.
- Import les données de Tuttii : permet d'importer les données d'une autre instance de Tutti précedemment exportées.

Le format de l'archive d'un export est le suivant :

::

  tutti-2.0-SNAPSHOT-2013-04-23/
  |-- db
  |   |-- allegro.backup
  |   |-- allegro.data
  |   |-- allegro.log
  |   |-- allegro.properties
  |   |-- allegro.script
  |   `-- version.appup
  `-- meas_files
      |-- CATCH_BATCH
      |   |-- OBJ100000
      |   |   |-- CATCH_BATCH-OBJ100000-100005.tnk
      |   |   `-- CATCH_BATCH-OBJ100000-100006.car
      |   `-- OBJ100003
      |-- OPERATION
      |   `-- OBJ100000
      |       |-- OPERATION-OBJ100000-100000.dat
      |       `-- OPERATION-OBJ100000-100007.dat
      `-- SAMPLE
          |-- OBJ100000
          |   `-- SAMPLE-OBJ100000-100002.asc
          |-- OBJ100015
          |   `-- SAMPLE-OBJ100015-100001.dat
          |-- OBJ100018
          |   `-- SAMPLE-OBJ100018-100002.dat
          |-- OBJ100022
          |   `-- SAMPLE-OBJ100022-100003.dat
          `-- OBJ100040
              `-- SAMPLE-OBJ100040-100004.dat

Pour le moment si vous voulez importer une base sans les autres données
(pièces-jointes / protocole), il vous suffit alors simplement de créer une
archive zip qui respecte ce format.

Pour utiliser ces fonctionnalités, rendez-vous sur l'écran **Gestionnaire de base**
(Menu fichier -> Gestionnaire de base).

Gestionnaire de référentiel
---------------------------

Tutti permet de gérer certains référentiels temporaires via des imports csv.
Afin de faciliter l'utilisation du format des imports, il est possible d'exporter
pour chaque type d'import un exemple de fichier avec le bon format.

Pour import ces référentiels, rendez-vous sur l'écran **Import des référentiels
temporaires** (Menu Administration -> Réferentiels temporaires).

A noter que cet écran est protégé par un mot de passe pour mieux contrôler les
ajouts de référentiels. (à partir de la version 2.0).

Les différents formats des fichiers csv sont décrits dans le `document de spécifications`_.

Consulter la section **6.1 Gestion des référentiels temporaires** de ce document pour de plus de détails.

Gestionnaire de protocole
-------------------------

Tutti permet de gérer un protocole léger est simple à mettre en application.

Création d'un protocole
~~~~~~~~~~~~~~~~~~~~~~~

Pour créer un nouveau protocole, rendez-vous sur l'écran **Sélection d'une
campagne** (Menu Actions -> Sélectionner une campagne), sur la ligne
Protocol de saisie, survoler le bouton *Nouveau* puis cliquez sur le bouton
Nouveau qui apparaît dans la liste déroulante d'actions.

Vous arrivez alors sur un formulaire qui permet la saisie du protocole
organisé sous forme d'onglets.

Import de données dans un protocole
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Il est possible dans le formulaire de création - mise à jour d'un protocole
d'importer à partir de fichier au format csv. Plus précisement on peut importer

- dans l'onglet *caractéristiques*, les caractéristiques *préférées* dans
  les différents écrans de saisie.
- dans l'onglet *espèces*, les espèces *préférées* à utiliser dans les écrans
  de saisie.

Import de caractéristiques
~~~~~~~~~~~~~~~~~~~~~~~~~~

Le format du ficher d'import est décrit dans le `document de spécifications`_.

Consulter la section **3.3.2. Export/Import des caractéristiques** de ce document pour plus de détails.

Import d'espèces
~~~~~~~~~~~~~~~~

Le format du ficher d'import est décrit dans le `document de spécifications`_.

Consulter la section **3.3.4. Export/Import des espèces et du benthos** de ce document pour plus de détails.

Exporter un protocole
~~~~~~~~~~~~~~~~~~~~~

Une fois le protocole saisie dans *Tutti*, il est possible de l'exporter afin
de pouvoir le réimporter ensuite sur une autre machine.

Pour ce faire, retourner sur l'écran de **Sélection d'une campagne**, survoler
le bouton *Editer* puis cliquer sur le bouton **Exporter**. Il vous sera alors
demander de spécifier l'emplacement de sauvegarde du protocole.

A noter que l'extension *.tuttiProtocol* sera ajouté au nom du fichier saisie.

Remarque: Le format de ce fichier bien qu'humainement compréhensible ne doit pas
être modifié à la main.

Importer un protocole
~~~~~~~~~~~~~~~~~~~~~

En survolant le bouton **Nouveau**, et en cliquant sur le bouton **Importer**, il
vous sera demander de fournir une fichier de protocole
(avec une extension *.tuttiProtocol*).

Une fois votre fichier sélectionné, vous arriverez sur l'écran de
*création - modification d'un protocole*.

Il faut faudra alors l'enregistrer pour finaliser l'import dans Tutti.

Cloner un protocole
~~~~~~~~~~~~~~~~~~~

Une fois un protocole saisie dans *Tutti*, il est possible de le cloner.

Pour ce faire, retourner sur l'écran de **Sélection d'une campagne**, survoler
le bouton *Editer* puis cliquer sur le bouton **Cloner**.

Le protocole sera alors dupliquer, vous arriverez sur l'écran de
*création - modification d'un protocole*.

Il faut faudra alors l'enregistrer pour finaliser la duplication du protocole.

Supprimer un protocole
~~~~~~~~~~~~~~~~~~~~~~

Une fois un protocole saisie dans *Tutti*, il est possible de le supprimer.

Pour ce faire, retourner sur l'écran de **Sélection d'une campagne**, survoler
le bouton *Editer* puis cliquer sur le bouton **Supprimer**. Il vous sera alors
demander de confirmer la suppression du protocole.

.. _document de spécifications: ./help/AllegroCampagne-Specifications.pdf

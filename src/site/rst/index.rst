.. -
.. * #%L
.. * Tutti
.. * $Id$
.. * $HeadURL$
.. * %%
.. * Copyright (C) 2012 - 2014 Ifremer
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU General Public License as
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/gpl-3.0.html>.
.. * #L%
.. -

=====
Tutti
=====

Présentation
------------

Bienvenue sur le site de l'application Tutti.

**Tutti** est un outil de saisie de données d'opérations et de captures au
cours des campagnes halieutiques.

Ce logiciel permettra la saisie en mer des données d'opération de
pêche (positions, environnement, engin, etc) et des captures
associées (composition de la capture en espèces scientifiques
avec poids, nombres, tailles etc) pour l'ensemble des campagnes
halieutiques réalisées par l'Ifremer.

Lancement de l'application
--------------------------

Pour télécharger l'application aller `ici
<http://forge.codelutin.com/projects/tutti/files>`_ .

Après téléchargement, décompresser l'archive dans un dossier.

Aucun pré-requis logiciel n'est nécessaire au lancement de Tutti. Nous
préconisons cependant d'avoir au moins 1Go de mémoire.

Sous windows
~~~~~~~~~~~~

Lancement de l'application via **tutti.exe** ou **tutti.bat**

Sous linux
~~~~~~~~~~

Lancement de l'application via **tutti.sh**

Première utilisation
--------------------

Lors d'une première utilisation, l'application démarre et affiche l'écran de
gestion des bases de travail. Il suffit alors d'installer une base via l'action
**Installer**. La dernière base disponible sera alors téléchargée puis installer.

A noter que cette opération peut-être longue (temps du téléchargement), soyez
patient.

Une fois la base téléchargée et installée l'application est pleinement fonctionnel.

Fonctionnalités
---------------

Retrouvez l'ensemble des fonctionnalités de **Tutti** sur la page des `fonctionnalités`_.

.. _fonctionnalités: ./features.html

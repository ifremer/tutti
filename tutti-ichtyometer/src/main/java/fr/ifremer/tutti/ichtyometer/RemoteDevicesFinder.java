package fr.ifremer.tutti.ichtyometer;

/*
 * #%L
 * Tutti :: Ichtyometer API
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Maps;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.bluetooth.BluetoothStateException;
import javax.bluetooth.DeviceClass;
import javax.bluetooth.DiscoveryAgent;
import javax.bluetooth.DiscoveryListener;
import javax.bluetooth.LocalDevice;
import javax.bluetooth.RemoteDevice;
import javax.bluetooth.ServiceRecord;
import java.io.IOException;
import java.util.Map;

/**
 * Created on 4/4/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.14.4
 */
public class RemoteDevicesFinder {

    /** Logger. */
    private static final Log log = LogFactory.getLog(RemoteDevicesFinder.class);

    private final Map<String, RemoteDevice> devices;

    private final int maximumNumberOfTryToConnect;

    private final Object lock;

    public RemoteDevicesFinder(int maximumNumberOfTryToConnect, Object lock) {
        this.maximumNumberOfTryToConnect = maximumNumberOfTryToConnect;
        this.lock = lock;
        this.devices = Maps.newTreeMap();
    }

    public Map<String, RemoteDevice> findDevices(LocalDevice localDevice) throws BluetoothStateException, InterruptedException {

        DevicesDiscoveryListener listener = new DevicesDiscoveryListener();


        synchronized (lock) {

            int tryRound = 0;

            while (tryRound < maximumNumberOfTryToConnect && devices.isEmpty()) {

                boolean started = localDevice.getDiscoveryAgent().startInquiry(DiscoveryAgent.LIAC, listener);
                if (started) {
                    if (log.isInfoEnabled()) {
                        log.info("Wait for device inquiry to complete... (try " + tryRound + ")");
                    }
                    lock.wait();
                    if (log.isInfoEnabled()) {
                        log.info(devices.size() + " device(s) found");
                    }
                }
                tryRound++;
            }

        }

        return devices;

    }

    protected class DevicesDiscoveryListener implements DiscoveryListener {

        @Override
        public void deviceDiscovered(RemoteDevice btDevice, DeviceClass cod) {
            if (log.isInfoEnabled()) {
                log.info("Device " + btDevice.getBluetoothAddress() + " found");
            }

            String friendlyName = null;

            int tryRound = 0;

            while (tryRound < maximumNumberOfTryToConnect && friendlyName == null) {

                if (log.isInfoEnabled()) {
                    log.info("Try to get device friendlyName (try " + tryRound + ")");
                }
                friendlyName = getName(btDevice);

                tryRound++;
            }

            if (friendlyName == null) {
                throw new RemoteDeviceCantGetNameException(btDevice.getBluetoothAddress());
            }

            devices.put(friendlyName, btDevice);
        }

        protected String getName(RemoteDevice btDevice) {
            String friendlyName = null;
            try {
                friendlyName = btDevice.getFriendlyName(false);
                if (log.isInfoEnabled()) {
                    log.info("Name: " + friendlyName);
                }
            } catch (IOException e) {
                if (log.isDebugEnabled()) {
                    log.debug("Can't get name of remote", e);
                }

            }
            return friendlyName;
        }

        @Override
        public void inquiryCompleted(int discType) {
            if (log.isDebugEnabled()) {
                log.debug("Device Inquiry completed!");
            }
            synchronized (lock) {
                lock.notifyAll();
            }
        }

        @Override
        public void serviceSearchCompleted(int transID, int respCode) {
        }

        @Override
        public void servicesDiscovered(int transID, ServiceRecord[] servRecord) {
        }

    }

}

package fr.ifremer.tutti.ichtyometer.tool;

/*
 * #%L
 * Tutti :: Ichtyometer API
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Lists;
import fr.ifremer.tutti.ichtyometer.IchtyometerClient;
import fr.ifremer.tutti.ichtyometer.RemoteDeviceChooser;
import fr.ifremer.tutti.ichtyometer.interactive.Command;
import fr.ifremer.tutti.ichtyometer.interactive.CommandEngine;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.Console;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

/**
 * Created on 2/4/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.1.2
 */
public class SendCommandTool {

    /** Logger. */
    private static final Log log = LogFactory.getLog(SendCommandTool.class);

    enum ConsoleCommand {
        a, b, c, d, e, g, q
    }

    public static void main(String... args) throws IOException {

        final Console console = System.console();
        final PrintWriter writer = console.writer();
        writer.println("CommandTool: to send command to the board and wait a result v1.0");

        RemoteDeviceChooser remoteDeviceChooser = remoteDeviceNames -> {
            List<String> remoteDeviceNameList = Lists.newArrayList(remoteDeviceNames);
            writer.println("Choose you device");
            int i = 0;
            for (String remoteDeviceName : remoteDeviceNameList) {
                writer.println(i++ + " for device " + remoteDeviceName);
            }
            writer.println("q (to quit)");
            writer.print("Your choice: ");
            writer.flush();

            String command = console.readLine();

            if ("q".equals(command)) {
                System.exit(0);
            }
            return remoteDeviceNameList.get(Integer.valueOf(command));
        };

        IchtyometerClient client = new IchtyometerClient(2);

        client.open(remoteDeviceChooser, true);

        CommandEngine reader = new CommandEngine();

        writer.println("client " + client + " is ready.open and wait for a command");

        reader.start(client);

        boolean quit = false;
        while (!quit) {

            writer.print("new command: [a, b, c, d, e, f, g or q to quit]:");
            writer.flush();
            String command = console.readLine();
            ConsoleCommand checkCommand;
            try {
                checkCommand = ConsoleCommand.valueOf(command);
            } catch (IllegalArgumentException e) {
                writer.println("Command " + command + " not possible");
                continue;
            }
            switch (checkCommand) {

                case q:
                    reader.stop();

                    quit = true;
                    break;
                default:
                    try {
                        Command sendCommand = reader.sendCommand(checkCommand.name());
                        writer.println(String.format(
                                "------------------------------------------------------------------------------\n%s" +
                                "\n------------------------------------------------------------------------------", sendCommand.getResponse()));
                    } catch (Exception e) {
                        if (log.isErrorEnabled()) {
                            log.error("Something wrong happen", e);
                        }
                    }


            }
        }
    }
}

package fr.ifremer.tutti.ichtyometer.feed.record;

/*
 * #%L
 * Tutti :: Ichtyometer API
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.LinkedHashSet;
import java.util.Set;

/**
 * Created on 12/9/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.10
 */
public class IchtyometerFeedReaderRecordFactory {

    protected final Set<FeedReaderRecordAceptor> acceptors;

    public IchtyometerFeedReaderRecordFactory() {
        acceptors = new LinkedHashSet<>();
        acceptors.add(new FeedReaderRecordAceptor<IchtyometerFeedReaderStylusMotionRecord>() {
            @Override
            public boolean accept(String record) {
                return IchtyometerFeedReaderStylusMotionRecord.acceptRecord(record);
            }

            @Override
            public IchtyometerFeedReaderStylusMotionRecord newRecord(String record) {
                return new IchtyometerFeedReaderStylusMotionRecord(record);
            }
        });
        acceptors.add(new FeedReaderRecordAceptor<IchtyometerFeedReaderMeasureRecord>() {
            @Override
            public boolean accept(String record) {
                return IchtyometerFeedReaderMeasureRecord.acceptRecord(record);
            }

            @Override
            public IchtyometerFeedReaderMeasureRecord newRecord(String record) {
                return new IchtyometerFeedReaderMeasureRecord(record);
            }
        });

    }

    public IchtyometerFeedReaderRecordSupport newRecord(String record) {

        IchtyometerFeedReaderRecordSupport feedRecord = null;

        for (FeedReaderRecordAceptor acceptor : acceptors) {

            boolean accept = acceptor.accept(record);
            if (accept) {

                feedRecord = acceptor.newRecord(record);
                break;

            }

        }

        return feedRecord;

    }

    protected static interface FeedReaderRecordAceptor<F extends IchtyometerFeedReaderRecordSupport> {

        boolean accept(String record);


        F newRecord(String record);

    }

}

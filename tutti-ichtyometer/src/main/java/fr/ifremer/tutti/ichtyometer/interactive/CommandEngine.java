package fr.ifremer.tutti.ichtyometer.interactive;

/*
 * #%L
 * Tutti :: Ichtyometer API
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import com.google.common.io.Closeables;
import fr.ifremer.tutti.ichtyometer.IchtyometerClient;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.microedition.io.StreamConnection;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

/**
 * To send command to the ichtyometer and read his reponses.
 *
 * Created on 1/28/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.1
 */
public class CommandEngine {

    /** Logger. */
    private static final Log log = LogFactory.getLog(CommandEngine.class);

    protected IchtyometerClient client;

    protected StreamConnection connection;

    protected DataOutputStream dataOutputStream;

    protected DataInputStream dataInputStream;

    protected ExecutorService service;

    public void start(IchtyometerClient client) throws IOException {

        Preconditions.checkNotNull(client, "client can not be null");
        Preconditions.checkState(client.isOpen(), "client must be opened");
        this.client = client;

        // create a service executor
        this.service = Executors.newSingleThreadScheduledExecutor();

        // get bluetooth connection
        this.connection = client.openConnection();

        // get input stream (to read)
        this.dataInputStream = connection.openDataInputStream();

        // get output stream (to write)
        this.dataOutputStream = connection.openDataOutputStream();

        // engine is ready
        if (log.isDebugEnabled()) {
            log.debug("Ready to read remote device...");
        }
    }

    public void stop() throws IOException {

        Closeables.close(dataInputStream, true);
        Closeables.close(dataOutputStream, true);
        Closeables.close(client, true);
    }

    public Command sendCommand(String question) throws IOException {

        Preconditions.checkNotNull(question, "command can not be null");

        Callable<Command> call = new CommandCallable(question, dataInputStream, dataOutputStream);

        Future<Command> submit = service.submit(call);

        try {
            return submit.get(1, TimeUnit.MINUTES);
        } catch (InterruptedException | TimeoutException e) {
            throw new IchtyometerCommandException("Time out on command " + question, e);
        } catch (ExecutionException e) {
            throw new IchtyometerCommandException(e.getCause());
        }
    }

    protected static class CommandCallable implements Callable<Command> {

        protected String question;

        protected DataInputStream dataInputStream;

        protected DataOutputStream dataOutputStream;

        protected CommandCallable(String question,
                                  DataInputStream dataInputStream,
                                  DataOutputStream dataOutputStream) {

            this.question = question;
            this.dataInputStream = dataInputStream;
            this.dataOutputStream = dataOutputStream;
        }

        @Override
        public Command call() throws Exception {

            // send question
            dataOutputStream.writeChars(question);

            StringBuilder responseBuilder = new StringBuilder();

            boolean responseComplete = false;

            while (!responseComplete) {

                while (dataInputStream.available() > 0) {

                    int c = dataInputStream.read();

                    switch (c) {
                        case '\r':

                            // end of line
                            responseBuilder.append('\n');
                            break;
                        case '#':

                            // sentinel car
                            responseComplete = true;

                            // still add the sentinel char to response
                            responseBuilder.append((char) c);
                            break;
                        default:
                            // add caracter to response
                            responseBuilder.append((char) c);
                    }
                }
            }

            String response = responseBuilder.toString();

            return new Command(question, response, null);
        }
    }

}

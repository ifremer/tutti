package fr.ifremer.tutti.ichtyometer.feed;

/*
 * #%L
 * Tutti :: Ichtyometer API
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import com.google.common.io.Closeables;
import fr.ifremer.tutti.ichtyometer.IchtyometerClient;
import fr.ifremer.tutti.ichtyometer.feed.event.IchtyometerFeedReaderEvent;
import fr.ifremer.tutti.ichtyometer.feed.event.IchtyometerFeedReaderListener;
import fr.ifremer.tutti.ichtyometer.feed.record.IchtyometerFeedReaderMeasureRecord;
import fr.ifremer.tutti.ichtyometer.feed.record.IchtyometerFeedReaderRecordFactory;
import fr.ifremer.tutti.ichtyometer.feed.record.IchtyometerFeedReaderRecordSupport;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.microedition.io.StreamConnection;
import javax.swing.event.EventListenerList;
import java.io.Closeable;
import java.io.DataInputStream;
import java.io.IOException;

/**
 * To read some records from a ichtyometer in feed mode.
 *
 * Created on 1/24/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.1
 */
public class IchtyometerFeedReader implements Closeable {

    /** Logger. */
    private static final Log log = LogFactory.getLog(IchtyometerFeedReader.class);

    /**
     * Ichtyometer client given in the {@code start} method.
     */
    protected IchtyometerClient client;

    /**
     * Reader runnable code (to be used in a thread).
     */
    protected ReadingRunnable readingRunnable;

    /**
     * To keep list of {@link IchtyometerFeedReaderListener} listeners.
     */
    protected final EventListenerList listenerList;

    /**
     * To generate some feed record from the rax record send by the board.
     *
     * @since 3.10
     */
    protected final IchtyometerFeedReaderRecordFactory recordFactory;

    public IchtyometerFeedReader() {
        listenerList = new EventListenerList();
        recordFactory = new IchtyometerFeedReaderRecordFactory();
    }

    public void start(IchtyometerClient client) throws IOException {

        Preconditions.checkNotNull(client, "client can not be null");
        Preconditions.checkState(client.isOpen(), "client must be opened");
        this.client = client;

        // get connection
        StreamConnection connection = client.openConnection();

        // get input stream
        DataInputStream dataInputStream = connection.openDataInputStream();

        // create the reader runnable
        readingRunnable = new ReadingRunnable(dataInputStream);

        // start the reader thread
        new Thread(readingRunnable).start();

        if (log.isDebugEnabled()) {
            log.debug("Ready to read remote device...");
        }
    }

    @Override
    public void close() throws IOException {

        Preconditions.checkNotNull(client, "client can not be null");
        Preconditions.checkState(client.isOpen(), "client must be opened");

        try {
            readingRunnable.stop();

        } finally {

            Closeables.close(client, true);
        }
    }

    public String getClientName() {
        return client.getName();
    }

    public void addFeedModeReaderListener(IchtyometerFeedReaderListener listener) {
        listenerList.add(IchtyometerFeedReaderListener.class, listener);
    }

    public void removeFeedModeReaderListener(IchtyometerFeedReaderListener listener) {
        listenerList.remove(IchtyometerFeedReaderListener.class, listener);
    }

    public void removeAllFeedModeReaderListeners() {
        for (IchtyometerFeedReaderListener listener : listenerList.getListeners(IchtyometerFeedReaderListener.class)) {
            listenerList.remove(IchtyometerFeedReaderListener.class, listener);
        }
    }

    protected class ReadingRunnable implements Runnable {

        /**
         * Input stream to read new record.
         */
        protected final DataInputStream dataInputStream;

        /**
         * Flag to stop the runnable.
         */
        protected boolean stop;

        protected ReadingRunnable(DataInputStream dataInputStream) {
            this.dataInputStream = dataInputStream;
        }

        protected void stop() {
            this.stop = true;
        }

        @Override
        public void run() {

            if (log.isInfoEnabled()) {
                log.info("Reader thread start... " + this);
            }

            while (!stop) {

                try {
                    // get a new record
                    IchtyometerFeedReaderRecordSupport readerRecord = readRecord(dataInputStream);

                    if (!stop && readerRecord != null && readerRecord instanceof IchtyometerFeedReaderMeasureRecord) {

                        // send new record to listeners
                        IchtyometerFeedReaderEvent e = new IchtyometerFeedReaderEvent(IchtyometerFeedReader.this, (IchtyometerFeedReaderMeasureRecord) readerRecord);
                        for (IchtyometerFeedReaderListener listener : listenerList.getListeners(IchtyometerFeedReaderListener.class)) {
                            listener.recordRead(e);
                        }
                        
                    }
                } catch (IOException e) {
                    if (log.isErrorEnabled()) {
                        log.error("Could not read record", e);
                    }
                }
            }

            if (log.isInfoEnabled()) {
                log.info("Reader thread stop..." + this);
            }
        }

        protected IchtyometerFeedReaderRecordSupport readRecord(DataInputStream dataInputStream) throws IOException {

            String result = "";

            // wait until got a #

            boolean complete = false;

            while (!complete) {

                if (stop) {
                    break;
                }
                while (dataInputStream.available() > 0) {
                    if (complete || stop) {
                        break;
                    }
                    int c = dataInputStream.read();

                    result += (char) c;

                    if ('#' == (char) c) {

                        complete = true;
                    }
                }
            }

            IchtyometerFeedReaderRecordSupport readerRecord = null;

            if (!stop) {

                if (log.isInfoEnabled()) {
                    log.info("New raw record: " + result);
                }
                readerRecord = recordFactory.newRecord(result);

                if (log.isDebugEnabled()) {
                    log.debug("New feed record: " + readerRecord);
                }

            }

            return readerRecord;
        }
    }
}

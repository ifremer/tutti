package fr.ifremer.tutti.ichtyometer;

/*
 * #%L
 * Tutti :: Ichtyometer API
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.bluetooth.BluetoothStateException;
import javax.bluetooth.DataElement;
import javax.bluetooth.DeviceClass;
import javax.bluetooth.DiscoveryListener;
import javax.bluetooth.LocalDevice;
import javax.bluetooth.RemoteDevice;
import javax.bluetooth.ServiceRecord;
import javax.bluetooth.UUID;
import java.util.ArrayList;
import java.util.List;

/**
 * Created on 4/4/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.14.4
 */
public class ServiceRecordsFinder {

    /** Logger. */
    private static final Log log = LogFactory.getLog(ServiceRecordsFinder.class);

    private final int maximumNumberOfTryToConnect;

    private final Object lock;

    private final List<ServiceRecord> serviceRecords;

    public ServiceRecordsFinder(int maximumNumberOfTryToConnect, Object lock) {
        this.maximumNumberOfTryToConnect = maximumNumberOfTryToConnect;
        this.lock = lock;
        this.serviceRecords = new ArrayList<>();
    }

    public List<ServiceRecord> findServices(LocalDevice localDevice, RemoteDevice device, int serviceIndex) throws InterruptedException, BluetoothStateException {

        ServicesDiscoveryListener listener = new ServicesDiscoveryListener();
        int tryRound = 0;

        while (tryRound < maximumNumberOfTryToConnect && serviceRecords.isEmpty()) {
            synchronized (lock) {

                int[] attrIDs = new int[]{
                        0x0100 // Service name
                };


                if (log.isInfoEnabled()) {
                    log.info("Trying to get services (try " + tryRound + ")");
                }
                localDevice.getDiscoveryAgent().searchServices(attrIDs,
                                                               new UUID[]{new UUID(serviceIndex)},
                                                               device,
                                                               listener);

                lock.wait();
            }

            tryRound++;
        }

        return serviceRecords;

    }

    protected class ServicesDiscoveryListener implements DiscoveryListener {

        @Override
        public void deviceDiscovered(RemoteDevice btDevice, DeviceClass cod) {
        }

        @Override
        public void inquiryCompleted(int discType) {
        }

        @Override
        public void serviceSearchCompleted(int transID, int respCode) {
            if (log.isDebugEnabled()) {
                log.debug("Service search completed!");
            }
            synchronized (lock) {
                lock.notifyAll();
            }
        }

        @Override
        public void servicesDiscovered(int transID, ServiceRecord[] servRecord) {
            for (ServiceRecord aServRecord : servRecord) {
                String url = aServRecord.getConnectionURL(ServiceRecord.NOAUTHENTICATE_NOENCRYPT, false);
                if (url == null) {
                    continue;
                }
                serviceRecords.add(aServRecord);
                String serviceName = getServiceName(aServRecord);
                if (log.isDebugEnabled()) {
                    log.debug("service found " + url + (serviceName == null ? "" : ", name: " + serviceName));
                }
            }
        }
    }

    protected String getServiceName(ServiceRecord serviceRecord) {

        DataElement serviceName = serviceRecord.getAttributeValue(0x0100);
        String result;

        if (serviceName == null) {
            result = null;
        } else {
            result = String.valueOf(serviceName.getValue());
        }
        return result;

    }
}

package fr.ifremer.tutti.ichtyometer;

/*
 * #%L
 * Tutti :: Ichtyometer API
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.intel.bluetooth.BlueCoveImpl;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.bluetooth.BluetoothStateException;
import javax.bluetooth.LocalDevice;
import javax.bluetooth.RemoteDevice;
import javax.bluetooth.ServiceRecord;
import javax.microedition.io.Connector;
import javax.microedition.io.StreamConnection;
import java.io.Closeable;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Ichtyometer bluetooth client.
 *
 * Created on 10/11/13.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.1
 */
public class IchtyometerClient implements Closeable {

    /** Logger. */
    private static final Log log = LogFactory.getLog(IchtyometerClient.class);

    /**
     * To keep a cache of discovered remote device. Keys are the name of device, values are the remote device.
     */
    protected static Map<String, RemoteDevice> REMOTE_DEVICE_CACHE;

    /**
     * To keep a cache of discovered connection url on remote devices. Keys are the name of device, values are
     * the connection of the remote device.
     */
    protected static Map<String, String> REMOTE_CONNECTION_URL_CACHE;

    private final int maximumNumberOfTryToConnect;

    public IchtyometerClient(int maximumNumberOfTryToConnect) {
        this.maximumNumberOfTryToConnect = maximumNumberOfTryToConnect;
    }

    public static void clearRemoteDeviceCaches() {
        REMOTE_DEVICE_CACHE = null;
        REMOTE_CONNECTION_URL_CACHE = null;
    }

    /**
     * Local bluetooth device.
     */
    protected LocalDevice localDevice;

    /**
     * Set of opened connections.
     */
    protected final Set<StreamConnection> connections = Sets.newHashSet();

    /**
     * Remote BigFin bluetooth device.
     */
    protected RemoteDevice remoteDevice;

    /**
     * Url connection to the remote device.
     */
    protected String connectionUrl;

    /**
     * Flag set to {@code true} if {@link #open(RemoteDeviceChooser, boolean)} method was invoked with success.
     */
    protected boolean open;

    /**
     * Friendly name of the device.
     */
    protected String name;

    public void open(RemoteDeviceChooser remoteDeviceChooser, boolean forceCompleteScan) throws IOException {

        Preconditions.checkState(!open, "Client is already opened");

        try {
            localDevice = LocalDevice.getLocalDevice();
        } catch (BluetoothStateException e) {
            throw new LocalDeviceNotFoundException();
        }

        if (forceCompleteScan || REMOTE_DEVICE_CACHE == null) {

            // build map of remote devices

            REMOTE_DEVICE_CACHE = discoverRemoteDevices();

        }

        if (REMOTE_DEVICE_CACHE.isEmpty()) {
            throw new RemoteDeviceNotFoundException("No remote device found");
        }

        // ask user to choose the device
        name = remoteDeviceChooser.chooseRemoteDevice(REMOTE_DEVICE_CACHE.keySet());

        if (name == null) {
            throw new RemoteDeviceNotFoundException("User did not choose a remote device");
        }

        remoteDevice = REMOTE_DEVICE_CACHE.get(name);

        if (remoteDevice == null) {
            throw new RemoteDeviceNotFoundException(
                    "Could not find remote device with name '" + name + "'");
        }

        if (forceCompleteScan || REMOTE_CONNECTION_URL_CACHE == null) {
            REMOTE_CONNECTION_URL_CACHE = Maps.newTreeMap();
        }

        if (!REMOTE_CONNECTION_URL_CACHE.containsKey(name)) {

            int serviceIndex = 3;

            List<ServiceRecord> serviceRecords = discoverServiceRecords(serviceIndex);

            if (log.isInfoEnabled()) {
                log.info("Found some services for index: " + serviceIndex);
            }
            
            // take first service record
            ServiceRecord serviceRecord = serviceRecords.get(0);

            // get connection url
            String url = serviceRecord.getConnectionURL(ServiceRecord.NOAUTHENTICATE_NOENCRYPT, true);

            if (log.isInfoEnabled()) {
                log.info("Found service(" + serviceIndex + "): " + url + ", name: " + name);
            }
            REMOTE_CONNECTION_URL_CACHE.put(name, url);

        }

        // get connection url
        connectionUrl = REMOTE_CONNECTION_URL_CACHE.get(name);

        open = true;
    }

    @Override
    public void close() throws IOException {

        if (!open) {
            return;
        }
        try {
            for (StreamConnection connection : Sets.newHashSet(connections)) {
                closeConnection(connection);
            }
        } finally {

            BlueCoveImpl.shutdown();
        }
    }

    public StreamConnection openConnection() throws IOException {

        checkIsOpened();

        StreamConnection connection = (StreamConnection) Connector.open(connectionUrl, 2);
        connections.add(connection);
        return connection;
    }

    public void closeConnection(StreamConnection connection) throws IOException {
        checkIsOpened();
        boolean remove = connections.remove(connection);
        if (!remove) {
            throw new IllegalArgumentException("Connection is not coming from this client, won't close it!");
        }
        connection.close();
    }

    public boolean isOpen() {
        return open;
    }

    public String getName() {
        checkIsOpened();
        return name;
    }

    protected void checkIsOpened() {
        if (!open) {
            throw new IllegalStateException("Client is not opened!");
        }
    }

    protected Map<String, RemoteDevice> discoverRemoteDevices() throws RemoteDeviceNotFoundException {

        RemoteDevicesFinder remoteDevicesFinder = new RemoteDevicesFinder(maximumNumberOfTryToConnect, this);

        Map<String, RemoteDevice> devices;
        try {
            devices = remoteDevicesFinder.findDevices(localDevice);

        } catch (Exception e) {
            throw new RemoteDeviceNotFoundException("Could not detected devices", e);
        }

        return devices;

    }

    protected List<ServiceRecord> discoverServiceRecords(int serviceIndex) throws RemoteDeviceNotFoundException, RemoteDeviceServiceNotFoundException {

        ServiceRecordsFinder serviceRecordsFinder = new ServiceRecordsFinder(maximumNumberOfTryToConnect, this);
        List<ServiceRecord> serviceRecords;
        try {
            serviceRecords = serviceRecordsFinder.findServices(localDevice, remoteDevice, serviceIndex);
        } catch (Exception e) {
            throw new RemoteDeviceNotFoundException("Could not read remote device services", e);
        }

        if (serviceRecords.isEmpty()) {
            throw new RemoteDeviceServiceNotFoundException("No services detected.");
        }

        return serviceRecords;

    }


}

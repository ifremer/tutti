package fr.ifremer.tutti.ichtyometer.interactive;

/*
 * #%L
 * Tutti :: Ichtyometer API
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.ichtyometer.BigFins;
import fr.ifremer.tutti.ichtyometer.IchtyometerClient;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import java.io.IOException;

/**
 * Created on 1/28/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.1
 */
@Ignore
public class CommandEngineTest {

    /*
    Commands to all Fishboards
a#  (presence check)-  Response: %a:e#
b# (board identify) - Response: %b:T,V,rU,rT# where
        T is board type: 10MF1, 10MF2, DFS/2
        V is firmware version numeric, lowest 2 digits are minor (i.e. 102 = v1.02)
        rU is fish records used
        rT is fish records total
e# (environmental data) - 
       Response: %e:fn1,fn2,..<cr> e1f1,e1f2,...<cr>e2f1,e2f2,...,<cr>#<cr>
       Where 
       fn1 is environmental field name 1, etc.
       e1f1 is environmental record 1 field 1 value
       e1f2 is environmental record 1 field 2 value, etc.
%h,ver (download firmware)
        This begins an interactive series of transactions between the host and board.

Commands to DFS/2 and 10MF1-Dolphin Only


&1mm,v#  (specify cal point 1 location) (no response)
&2mm,v# (specify cal point 2 location) (no response)
        These commands send the calibration positions (v, in mm)to the board
&1r# (request raw cal point 1 value) - Response: &1c,v#
&2r# (request raw cal point 2 value) - Response &2c,v#
         These commands request a raw measurement value from the board. The board waits for the user to touch the stylus, yielding "v" - the raw sensor value, which is then communicated to the host.
&ca# (request cal mode) (no response) - this doesn't cause any behavior on the board
&cr,m1,m2,raw1,raw2# (restore calibration) (no response)
         This causes the board to internally calibrate based on the points (m1, raw1) and (m2, raw2) specified by the host.  The board is then internally in the "calibrated" state, allowing it to transmit length messages (see next section).

Messages from DFS/2 and 10MF1-Dolphin to host

These messages are sent on a user-triggered basis; the host does not poll for them, but must be ready to receive them.
%t,v# (stylus up/down) - no response expected from host
        If v is 0, then the board is saying that the stylus is "down" on the sensor
        If v is 1, then the board is saying that the stylus is "up" off of the sensor
%l,v# (sensor position taken) - no response expected from host
        v is the position in mm where the stylus was touched.  Delays and multiple samples (selected for by the &m command above) are applied before the reading is taken and the message is sent.  This message is usually sandwiched between%t messages.

     */

    /** Logger. */
    private static final Log log = LogFactory.getLog(CommandEngineTest.class);

    protected IchtyometerClient client;

    @Before
    public void setUp() throws Exception {

        client = new IchtyometerClient(2);

        BigFins.open(client);

    }

    @After
    public void tearDown() throws Exception {

        if (client != null) {
            client.close();
        }

    }

    @Test
    public void testSendCommand() throws Exception {

        CommandEngine commandEngine = new CommandEngine();

        commandEngine.start(client);

        if (log.isInfoEnabled()) {
            log.info("Engine is ready let's ask some questions now...");
        }
        assertCommand(commandEngine, "a#", "%a:e#");
        assertCommand(commandEngine, "b#", "%b:DFS/2,212,0,13824,26270#");
        assertCommand(commandEngine, "e#", "%e:time,tempC,relHumid,...");

        commandEngine.stop();
    }

    protected void assertCommand(CommandEngine commandEngine, String question, String response) throws IOException {

        Command actual = commandEngine.sendCommand(question);

        if (log.isInfoEnabled()) {
            log.info("Actual command: " + actual);
            log.info("Expected result: " + response);
        }

        Assert.assertEquals("Expect question " + question + ", but had " + actual.getQuestion(), question, actual.getQuestion());
    }


}

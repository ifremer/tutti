package fr.ifremer.tutti.ui.swing;

/*
 * #%L
 * Tutti :: UI
 * %%
 * Copyright (C) 2012 - 2016 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Optional;
import fr.ifremer.tutti.ui.swing.util.SoundUtil;
import fr.ifremer.tutti.util.Numbers;
import org.junit.Test;



/**
 * @author Kevin Morin (Code Lutin)
 * @since 4.4
 */
public class SoundUtilTest {

    @Test
    public void testSound() throws InterruptedException {
        testReadNumber(101.5f, Numbers.CM_UNIT);
        testReadNumber(1, Numbers.CM_UNIT);
        testReadNumber(35, Numbers.MM_UNIT);
        testReadNumber(2001, Numbers.CM_UNIT);
        testReadNumber(2300, Numbers.CM_UNIT);
        testReadNumber(2000.5f, Numbers.CM_UNIT);
        testReadNumber(2000, Numbers.CM_UNIT);
        testReadNumber(200, Numbers.CM_UNIT);
        testReadNumber(201, Numbers.CM_UNIT);
        testReadNumber(201, null);
    }

    protected void testReadNumber(float measure, String unit) throws InterruptedException {

        SoundUtil.readNumber(measure, Optional.fromNullable(unit));
        Thread.sleep(1000);
    }
}

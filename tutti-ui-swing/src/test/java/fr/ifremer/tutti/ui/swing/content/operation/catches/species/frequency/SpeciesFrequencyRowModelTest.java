package fr.ifremer.tutti.ui.swing.content.operation.catches.species.frequency;

/*
 * #%L
 * Tutti :: UI
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.type.WeightUnit;
import org.junit.Assert;
import org.junit.Test;

/**
 * @author Tony Chemit - chemit@codelutin.com
 * @since 0.2
 */
public class SpeciesFrequencyRowModelTest {

    @Test
    public void compareTo() throws Exception {

        SpeciesFrequencyRowModel s0 = new SpeciesFrequencyRowModel(WeightUnit.KG);
        SpeciesFrequencyRowModel s1 = new SpeciesFrequencyRowModel(WeightUnit.KG);

        // s0=null, s1=null
        Assert.assertTrue(s0.compareTo(s1) == 0);

        // s0=1, s1=null
        s0.setLengthStep(1f);
        Assert.assertTrue(s0.compareTo(s1) > 0);

        // s0=null, s1=1
        s0.setLengthStep(null);
        s1.setLengthStep(1f);
        Assert.assertTrue(s0.compareTo(s1) < 0);

        // s0=2, s1=1
        s0.setLengthStep(2f);
        Assert.assertTrue(s0.compareTo(s1) > 0);

        // s0=2, s1=2
        s1.setLengthStep(2f);
        Assert.assertTrue(s0.compareTo(s1) == 0);

        // s0=2, s1=3
        s1.setLengthStep(3f);
        Assert.assertTrue(s0.compareTo(s1) < 0);
    }
}

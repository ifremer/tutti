package fr.ifremer.tutti.ui.swing.content.operation.catches.species.frequency;

/*
 * #%L
 * Tutti :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2016 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.entities.referential.Caracteristic;
import fr.ifremer.tutti.ui.swing.util.table.CaracteristicColumnIdentifier;

import static org.nuiton.i18n.I18n.n;

/**
 * Définition de la colonne de maturité (sa caractéristique est variale selon l'espèce du lot).
 *
 * Created on 26/04/16.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 4.5
 */
public class MaturityColumnIdentifier extends CaracteristicColumnIdentifier<IndividualObservationBatchRowModel> {

    public MaturityColumnIdentifier() {
        super(null,
              IndividualObservationBatchRowModel.PROPERTY_DEFAULT_CARACTERISTICS,
              n("tutti.editIndividualObservationBatch.table.header.maturityNotUsed"),
              n("tutti.editIndividualObservationBatch.table.header.maturityNotUsed.tip"));
    }

    public void setCaracteristic(Caracteristic caracteristic) {
        this.caracteristic = caracteristic;
    }

    public boolean withCaracteristic() {
        return caracteristic != null;
    }

    @Override
    public Object getValue(IndividualObservationBatchRowModel entry) {
        return caracteristic == null ? null : super.getValue(entry);
    }

    @Override
    public void setValue(IndividualObservationBatchRowModel entry, Object value) {
        if (caracteristic != null) {
            super.setValue(entry, value);
        }
    }

}

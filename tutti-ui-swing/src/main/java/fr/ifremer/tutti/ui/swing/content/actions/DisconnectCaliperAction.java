package fr.ifremer.tutti.ui.swing.content.actions;

/*
 * #%L
 * Tutti :: UI
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.caliper.feed.CaliperFeedReader;
import fr.ifremer.tutti.ui.swing.content.MainUIHandler;
import org.apache.commons.io.IOUtils;

import static org.nuiton.i18n.I18n.t;

/**
 * Close the connection to caliper.
 *
 * @author Kevin Morin (Code Lutin)
 * @since 4.6
 */
public class DisconnectCaliperAction extends AbstractMainUITuttiAction {

    public DisconnectCaliperAction(MainUIHandler handler) {
        super(handler, false);
    }

    @Override
    public void doAction() throws Exception {

        CaliperFeedReader caliperReader = getContext().getCaliperReader();

        // there is a feed reader, stop it now
        IOUtils.closeQuietly(caliperReader);
        getContext().setCaliperReader(null);

        sendMessage(t("tutti.caliper.connection.stop", caliperReader.getSerialPortName()));
    }
}

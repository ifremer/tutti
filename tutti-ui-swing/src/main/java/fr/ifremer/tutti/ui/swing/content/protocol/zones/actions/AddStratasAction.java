package fr.ifremer.tutti.ui.swing.content.protocol.zones.actions;

/*
 * #%L
 * Tutti :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2016 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.ui.swing.content.protocol.EditProtocolUIModel;
import fr.ifremer.tutti.ui.swing.content.protocol.zones.tree.node.StrataNode;
import fr.ifremer.tutti.ui.swing.content.protocol.zones.tree.node.SubStrataNode;
import fr.ifremer.tutti.ui.swing.content.protocol.zones.ZoneEditorUI;
import fr.ifremer.tutti.ui.swing.content.protocol.zones.tree.node.ZoneNode;
import fr.ifremer.tutti.ui.swing.util.actions.SimpleActionSupport;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.swing.JTree;
import javax.swing.tree.TreePath;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Optional;
import java.util.Set;

/**
 * @author Kevin Morin (Code Lutin)
 * @since 4.5
 */
public class AddStratasAction extends SimpleActionSupport<ZoneEditorUI> {

    /** Logger. */
    private static final Log log = LogFactory.getLog(AddStratasAction.class);

    public AddStratasAction(ZoneEditorUI zoneEditorUI) {
        super(zoneEditorUI);
        setEnabled(false);
    }

    @Override
    protected void onActionPerformed(ZoneEditorUI zoneEditorUI) {

        Optional<ZoneNode> optionalSelectedZoneNode = getSelectedZone(zoneEditorUI);
        if (!optionalSelectedZoneNode.isPresent()) {
            if (log.isInfoEnabled()) {
                log.info("No zone selected");
            }
            return;
        }

        JTree availableStratasTree = zoneEditorUI.getAvailableStratasTree();
        TreePath[] selectedAvailableStrataPaths = availableStratasTree.getSelectionPaths();

        if (selectedAvailableStrataPaths == null) {

            if (log.isInfoEnabled()) {
                log.info("No available stratas, nor subStratas selected");
            }
            return;
        }

        Set<StrataNode> strataNodesToAdd = getStrataNodesToAdd(selectedAvailableStrataPaths);
        Set<SubStrataNode> subStrataNodesToAdd = getSubStrataNodesToAdd(selectedAvailableStrataPaths, strataNodesToAdd);

        ZoneNode selectedZoneNode = optionalSelectedZoneNode.get();

        EditProtocolUIModel model = zoneEditorUI.getModel();

        model.setModifyingZones(true);

        try {

            strataNodesToAdd.forEach(strataNode -> model.selectStrataNodes(selectedZoneNode, strataNode));
            subStrataNodesToAdd.forEach(subStrataNode -> model.selectedSubStraNodes(selectedZoneNode, subStrataNode));

        } finally {

            model.setModifyingZones(false);

        }

    }

    protected Optional<ZoneNode> getSelectedZone(ZoneEditorUI zoneEditorUI) {

        JTree zonesTree = zoneEditorUI.getZonesTree();
        TreePath selectedZonePath = zonesTree.getSelectionPath();
        return Optional.ofNullable(selectedZonePath == null ? null : (ZoneNode) selectedZonePath.getPathComponent(1));
    }

    protected Set<StrataNode> getStrataNodesToAdd(TreePath[] selectedStratas) {
        Set<StrataNode> result = new HashSet<>();
        for (TreePath selectedStrata : selectedStratas) {

            Object node = selectedStrata.getLastPathComponent();
            if (node instanceof StrataNode) {
                StrataNode strataNode = (StrataNode) node;
                if (log.isInfoEnabled()) {
                    log.info("found strata " + strataNode + " to add");
                }
                result.add(strataNode);
            }

        }
        return result;
    }

    protected Set<SubStrataNode> getSubStrataNodesToAdd(TreePath[] selectedStratas, Set<StrataNode> stratasNodeToAdd) {
        Set<SubStrataNode> result = new LinkedHashSet<>();
        for (TreePath selectedStrata : selectedStratas) {

            Object node = selectedStrata.getLastPathComponent();
            if (node instanceof SubStrataNode) {
                SubStrataNode subStrataNode = (SubStrataNode) node;
                StrataNode strataNode = subStrataNode.getParent();
                if (!stratasNodeToAdd.contains(strataNode)) {
                    if (log.isInfoEnabled()) {
                        log.info("found subStrata " + subStrataNode + " to add");
                    }
                    result.add(subStrataNode);
                }
            }

        }
        return result;
    }
}

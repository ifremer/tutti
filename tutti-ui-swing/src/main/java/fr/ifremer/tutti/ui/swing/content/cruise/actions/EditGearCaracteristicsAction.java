package fr.ifremer.tutti.ui.swing.content.cruise.actions;

/*
 * #%L
 * Tutti :: UI
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.entities.CaracteristicMap;
import fr.ifremer.tutti.persistence.entities.referential.GearWithOriginalRankOrder;
import fr.ifremer.tutti.persistence.entities.referential.GearWithOriginalRankOrders;
import fr.ifremer.tutti.service.PersistenceService;
import fr.ifremer.tutti.ui.swing.util.actions.LongActionSupport;
import fr.ifremer.tutti.ui.swing.content.cruise.EditCruiseUI;
import fr.ifremer.tutti.ui.swing.content.cruise.EditCruiseUIHandler;
import fr.ifremer.tutti.ui.swing.content.cruise.EditCruiseUIModel;
import fr.ifremer.tutti.ui.swing.content.cruise.GearCaracteristicsEditorUI;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * @author Kevin Morin - kmorin@codelutin.com
 * @since 2.1
 */
public class EditGearCaracteristicsAction extends LongActionSupport<EditCruiseUIModel, EditCruiseUI, EditCruiseUIHandler> {

    /** Logger. */
    private static final Log log =
            LogFactory.getLog(EditGearCaracteristicsAction.class);

    protected final PersistenceService persistenceService;

    public EditGearCaracteristicsAction(EditCruiseUIHandler handler) {
        super(handler, false);
        persistenceService = getContext().getPersistenceService();
    }

    @Override
    public void doAction() throws Exception {
        GearCaracteristicsEditorUI editor = getUI().getGearCaracteristicsEditor();
        editor.getModel().setEditable(true);

        GearWithOriginalRankOrder gear =
                EditCruiseUIHandler.GEAR_EDIT_CONTEXT.getContextValue(getUI());

        CaracteristicMap caracteristics =
                persistenceService.getGearCaracteristics(
                        getDataContext().getCruise().getIdAsInt(),
                        gear.getIdAsInt(),
                        gear.getOriginalRankOrder());

        GearWithOriginalRankOrder gearToView =
                GearWithOriginalRankOrders.newGearWithOriginalRankOrder(gear);
        gearToView.setCaracteristics(caracteristics);

        if (log.isInfoEnabled()) {
            log.info("Will edit gear " + decorate(gearToView) +
                     " with " + gearToView.getCaracteristics().size() + " caracteristics.");
        }
        editor.getModel().setGear(gearToView);

        getUI().getMainPanelLayout().setSelected(EditCruiseUIHandler.GEAR_CARACTERISTICS_CARD);
    }
}

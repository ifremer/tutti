package fr.ifremer.tutti.ui.swing.content.operation.catches.species.create.actions;

/*
 * #%L
 * Tutti :: UI
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.entities.referential.Species;
import fr.ifremer.tutti.persistence.entities.referential.Speciess;
import fr.ifremer.tutti.service.DecoratorService;
import fr.ifremer.tutti.ui.swing.content.operation.catches.species.create.CreateSpeciesBatchUI;
import fr.ifremer.tutti.ui.swing.content.operation.catches.species.create.CreateSpeciesBatchUIHandler;
import fr.ifremer.tutti.ui.swing.content.operation.catches.species.create.CreateSpeciesBatchUIModel;
import fr.ifremer.tutti.ui.swing.util.actions.LongActionSupport;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static org.nuiton.i18n.I18n.t;

/**
 * Enable to select a species to the species selected in the protocol.
 *
 * @author Kevin Morin - kmorin@codelutin.com
 * @since 1.0
 */
public class SelectSpeciesForSpeciesBatchAction extends LongActionSupport<CreateSpeciesBatchUIModel, CreateSpeciesBatchUI, CreateSpeciesBatchUIHandler> {

    protected List<Species> allSpecies;

    protected List<Species> referentSpecies;

    protected Species selectedSpecies;

    protected List<Species> availableSpecies;

    public SelectSpeciesForSpeciesBatchAction(CreateSpeciesBatchUIHandler handler) {
        super(handler, false);

        allSpecies = getDataContext().getSpecies();
        referentSpecies = getDataContext().getReferentSpecies();
    }

    @Override
    public boolean prepareAction() throws Exception {
        boolean canContinue = super.prepareAction();
        if (canContinue) {

            CreateSpeciesBatchUIModel model = getModel();

            List<Species> species = new ArrayList<>(allSpecies);
            species.removeAll(model.getAvailableSpecies());

            selectedSpecies = openAddSpeciesDialog(t("tutti.selectSpeciesForSpeciesBatch.title"), species);
            canContinue = selectedSpecies != null;
        }
        return canContinue;
    }

    @Override
    public void doAction() throws Exception {

        CreateSpeciesBatchUIModel model = getModel();

        availableSpecies = new ArrayList<>(model.getAvailableSpecies());

        if (!selectedSpecies.isReferenceTaxon()) {
            String decoratedSynonym = decorate(selectedSpecies, DecoratorService.FROM_PROTOCOL);
            List<Species> referents = referentSpecies;
            Map<String, Species> referentsById = Speciess.splitReferenceSpeciesByReferenceTaxonId(referents);
            String taxonId = String.valueOf(selectedSpecies.getReferenceTaxonId());
            selectedSpecies = referentsById.get(taxonId);
            String decoratedReferent = decorate(selectedSpecies, DecoratorService.FROM_PROTOCOL);
            sendMessage(t("tutti.flash.info.species.replaced", decoratedSynonym, decoratedReferent));
        }

        if (!availableSpecies.contains(selectedSpecies)) {
            availableSpecies.add(selectedSpecies);
        }

    }

    @Override
    public void postSuccessAction() {
        super.postSuccessAction();

        CreateSpeciesBatchUIModel model = getModel();
        model.setAvailableSpecies(availableSpecies);
        model.setSpecies(selectedSpecies);
    }

    @Override
    protected void releaseAction() {
        super.releaseAction();
        availableSpecies = null;
        selectedSpecies = null;
    }
}

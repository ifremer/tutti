package fr.ifremer.tutti.ui.swing.content.protocol.maturity;

/*
 * #%L
 * Tutti :: UI
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.entities.protocol.MaturityCaracteristic;
import fr.ifremer.tutti.persistence.entities.referential.Caracteristic;
import fr.ifremer.tutti.persistence.entities.referential.CaracteristicQualitativeValue;
import fr.ifremer.tutti.ui.swing.util.AbstractTuttiUIHandler;
import jaxx.runtime.SwingUtil;
import jaxx.runtime.validator.swing.SwingValidator;

import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JList;
import javax.swing.ListCellRenderer;
import java.awt.Component;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.List;

import static org.nuiton.i18n.I18n.t;

/**
 * @author Kevin Morin - kmorin@codelutin.com
 * @since 4.5
 */
public class EditMaturityCaracteristicPopupUIHandler extends AbstractTuttiUIHandler<EditMaturityCaracteristicPopupUIModel, EditMaturityCaracteristicPopupUI> {

    @Override
    public void afterInit(EditMaturityCaracteristicPopupUI ui) {
        initUI(ui);

        JList<CaracteristicQualitativeValue> maturityValuesEditor = ui.getMaturityValuesEditor();
        maturityValuesEditor.setCellRenderer(new CheckboxListRenderer());
        maturityValuesEditor.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent event) {
                JList<CaracteristicQualitativeValue> list = (JList<CaracteristicQualitativeValue>) event.getSource();

                // Get index of item clicked

                int index = list.locationToIndex(event.getPoint());
                CaracteristicQualitativeValue item = list.getModel().getElementAt(index);

                // Toggle selected state
                if (item != null) {
                    if (getModel().isMature(item)) {
                        getModel().removeMatureState(item);

                    } else {
                        getModel().addMatureState(item);
                    }
                }

                // Repaint cell

                list.repaint(list.getCellBounds(index, index));
            }
        });

        getModel().addPropertyChangeListener(EditMaturityCaracteristicPopupUIModel.PROPERTY_ALL_MATURITY_STATES, evt -> {
            List<CaracteristicQualitativeValue> newValue = (List<CaracteristicQualitativeValue>) evt.getNewValue();
            if (newValue != null) {
                maturityValuesEditor.setListData(newValue.toArray(new CaracteristicQualitativeValue[newValue.size()]));
            } else {
                maturityValuesEditor.setListData(new CaracteristicQualitativeValue[0]);
            }
        });
    }

    @Override
    public void onCloseUI() {
        getUI().dispose();
    }

    @Override
    public SwingValidator<EditMaturityCaracteristicPopupUIModel> getValidator() {
        return ui.getValidator();
    }

    @Override
    protected JComponent getComponentToFocus() {
        return getUI().getMaturityValuesEditor();
    }

    public void open(Caracteristic caracteristic, MaturityCaracteristic maturityCaracteristic) {
        getModel().setAllMaturityStates(caracteristic.getQualitativeValue());
        getModel().setMatureStateIds(maturityCaracteristic.getMatureStateIds());
        getModel().setValid(false);

        getUI().getMessage().setText(t("tutti.editProtocol.maturityCaracteristic.dialog.message", decorate(caracteristic)));

        getUI().pack();
        SwingUtil.center(getContext().getMainUI(), ui);
        getUI().setVisible(true);
    }

    public void validate() {
        if (getValidator().isValid()) {
            getModel().setValid(true);
            onCloseUI();
        }
    }

    public void cancel() {
        getModel().setAllMaturityStates(null);
        getModel().setMatureStateIds(null);
        onCloseUI();
    }

    class CheckboxListRenderer extends JCheckBox implements
            ListCellRenderer<CaracteristicQualitativeValue> {

        @Override
        public Component getListCellRendererComponent(
                JList<? extends CaracteristicQualitativeValue> list, CaracteristicQualitativeValue value,
                int index, boolean isSelected, boolean cellHasFocus) {
            setEnabled(list.isEnabled());
            setSelected(EditMaturityCaracteristicPopupUIHandler.this.getModel().isMature(value));
            setFont(list.getFont());
            setBackground(list.getBackground());
            setForeground(list.getForeground());
            setText(decorate(value));
            return this;
        }
    }
}

package fr.ifremer.tutti.ui.swing.content.category.actions;

/*
 * #%L
 * Tutti :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import fr.ifremer.tutti.service.genericformat.GenericFormatExportService;
import fr.ifremer.tutti.ui.swing.util.actions.LongActionSupport;
import fr.ifremer.tutti.ui.swing.content.referential.actions.ExportExistingTemporaryGearAction;
import fr.ifremer.tutti.ui.swing.content.category.EditSampleCategoryModelUI;
import fr.ifremer.tutti.ui.swing.content.category.EditSampleCategoryModelUIHandler;
import fr.ifremer.tutti.ui.swing.content.category.EditSampleCategoryModelUIModel;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.File;

import static org.nuiton.i18n.I18n.t;

/**
 * Created on 3/4/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.14
 */
public class ExportSampleCategoryModelAction extends LongActionSupport<EditSampleCategoryModelUIModel, EditSampleCategoryModelUI, EditSampleCategoryModelUIHandler> {

    /** Logger. */
    private static final Log log =
            LogFactory.getLog(ExportExistingTemporaryGearAction.class);

    private File file;

    public ExportSampleCategoryModelAction(EditSampleCategoryModelUIHandler handler) {
        super(handler, true);
    }

    @Override
    public boolean prepareAction() throws Exception {

        boolean doAction = super.prepareAction();

        if (doAction) {

            // choose file to export
            file = saveFile(
                    "sampleCategory",
                    "csv",
                    t("tutti.exportSampleCategoryModel.title.choose.exportSampleCategoryModelFile"),
                    t("tutti.exportSampleCategoryModel.action.chooseSampleCategoryModelFile.export"),
                    "^.*\\.csv", t("tutti.common.file.csv"));
            doAction = file != null;
        }
        return doAction;

    }

    @Override
    public void releaseAction() {
        file = null;
        super.releaseAction();
    }

    @Override
    public void doAction() throws Exception {
        Preconditions.checkNotNull(file);
        if (log.isInfoEnabled()) {
            log.info("Will export sample category model to file: " + file);
        }

        GenericFormatExportService service = getContext().getGenericFormatExportService();
        service.exportSampleCategoryModel(file);

    }

    @Override
    public void postSuccessAction() {
        super.postSuccessAction();
        sendMessage(t("tutti.exportSampleCategoryModel.action.export.success", file));
    }
}
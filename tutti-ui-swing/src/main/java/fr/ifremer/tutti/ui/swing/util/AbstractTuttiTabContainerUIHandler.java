package fr.ifremer.tutti.ui.swing.util;

/*
 * #%L
 * Tutti :: UI
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.nuiton.jaxx.application.swing.AbstractApplicationUIHandler;
import org.nuiton.jaxx.application.swing.ApplicationUI;
import org.nuiton.jaxx.application.swing.tab.CustomTab;
import org.nuiton.jaxx.application.swing.tab.DelegateTabContainerHandler;
import org.nuiton.jaxx.application.swing.tab.TabContainerHandler;
import org.nuiton.jaxx.application.swing.tab.TabContentModel;
import org.nuiton.jaxx.application.swing.tab.TabHandler;

import javax.swing.*;
import java.awt.*;

/**
 * UI containing a tab panel.
 *
 * @param <M> type of the ui model
 * @author Kevin Morin - kmorin@codelutin.com
 * @since 0.3
 */
public abstract class AbstractTuttiTabContainerUIHandler<M, UI extends TuttiUI<M, ?>> extends AbstractTuttiUIHandler<M, UI> implements TabContainerHandler {

    DelegateTabContainerHandler delegateTabHandler;

    /**
     * Returns the tab handler of the tab i.
     *
     * @param index the index of the tab
     * @return the tab handler of the index i if the handler implements
     * the {@link TabHandler} interface,
     * <code>null</code> otherwise
     */
    @Override
    public TabHandler getTabHandler(int index) {
        TabHandler tabHandler = null;
        JTabbedPane tabPanel = getTabPanel();
        if (index >= 0 && index < tabPanel.getTabCount()) {
            Component tab = tabPanel.getComponentAt(index);
            if (ApplicationUI.class.isInstance(tab)) {
                ApplicationUI tuttiTab = (ApplicationUI) tabPanel.getComponentAt(index);
                AbstractApplicationUIHandler handler = tuttiTab.getHandler();
                if (TabHandler.class.isInstance(handler)) {
                    tabHandler = (TabHandler) handler;
                }
            }
        }
        return tabHandler;
    }

    @Override
    public void setCustomTab(int index, TabContentModel model) {
        JTabbedPane tabPanel = getTabPanel();
        tabPanel.setTabComponentAt(index, new CustomTab(model, this));
    }

    @Override
    protected void initUI(UI ui) {
        super.initUI(ui);
        delegateTabHandler = new DelegateTabContainerHandler(getTabPanel());
        init();
    }

    @Override
    public void init() {

        getTabPanel().setModel(new DefaultSingleSelectionModel() {

            private static final long serialVersionUID = 1L;

            @Override
            public void setSelectedIndex(int index) {
                int currentIndex = getTabPanel().getSelectedIndex();
                boolean mustChangeTab = onTabChanged(currentIndex, index);

                if (mustChangeTab) {
                    super.setSelectedIndex(index);
                }
            }

        });
    }

    @Override
    public boolean onTabChanged(int currentIndex, int newIndex) {
        boolean result = true;
        if (currentIndex != newIndex) {
            TabHandler handler = getTabHandler(currentIndex);
            if (handler != null) {
                result = handler.onHideTab(currentIndex, newIndex);
            }

            handler = getTabHandler(newIndex);
            if (handler != null) {
                handler.onShowTab(currentIndex, newIndex);
            }
        }
        return result;
    }
}

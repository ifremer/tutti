package fr.ifremer.tutti.ui.swing.content.operation.catches.species.edit.actions;

/*
 * #%L
 * Tutti :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import fr.ifremer.tutti.ui.swing.content.operation.catches.species.EditSpeciesBatchPanelUI;
import fr.ifremer.tutti.ui.swing.content.operation.catches.species.edit.SpeciesBatchRowModel;
import fr.ifremer.tutti.ui.swing.content.operation.catches.species.edit.SpeciesBatchTableModel;
import fr.ifremer.tutti.ui.swing.content.operation.catches.species.edit.SpeciesBatchUI;
import fr.ifremer.tutti.ui.swing.content.operation.catches.species.split.SplitSpeciesBatchUI;
import fr.ifremer.tutti.ui.swing.util.actions.SimpleActionSupport;
import jaxx.runtime.SwingUtil;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import static org.nuiton.i18n.I18n.t;

/**
 * Created on 3/7/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.15
 */
public class OpenAddSpeciesBatchSampleCategoryUIAction extends SimpleActionSupport<SpeciesBatchUI> {

    /** Logger. */
    private static final Log log = LogFactory.getLog(OpenAddSpeciesBatchSampleCategoryUIAction.class);

    private static final long serialVersionUID = -6540241422935319461L;

    public  OpenAddSpeciesBatchSampleCategoryUIAction(SpeciesBatchUI ui) {
        super(ui);
    }

    @Override
    protected void onActionPerformed(SpeciesBatchUI ui) {

        // get table model
        SpeciesBatchTableModel tableModel = ui.getHandler().getTableModel();

        // get selected row
        int rowIndex = SwingUtil.getSelectedModelRow(ui.getTable());
        Preconditions.checkState(rowIndex != -1, "Cant split batch if no batch selected");

        // get selected column
        int columnIndex = SwingUtil.getSelectedModelColumn(ui.getTable());
        Preconditions.checkState(columnIndex != -1, "Cant split batch if no batch selected");

        // get selected row
        SpeciesBatchRowModel selectedRow = tableModel.getEntry(rowIndex);

        // get his sample category
        Integer sampleCategoryId = tableModel.getSampleCategoryId(columnIndex);

        // get the first ancestor row using this category
        SpeciesBatchRowModel firstAncestorRow = selectedRow.getFirstAncestor(sampleCategoryId);

        // get his parent (the one we will edit)
        SpeciesBatchRowModel parentBatch = firstAncestorRow.getParentBatch();

        if (log.isDebugEnabled()) {
            log.debug("Open addChangeCategory batch ui for row [" + rowIndex + "] and category " + sampleCategoryId);
        }

        EditSpeciesBatchPanelUI parent = ui.getParentContainer(EditSpeciesBatchPanelUI.class);
        SplitSpeciesBatchUI splitBatchEditor = parent.getAddSampleCategoryBatch();

//        parentUI.getHandler().addSampleCategorySpeciesBatch(parentBatch, splitBatchEditor, sampleCategoryId);

        splitBatchEditor.getHandler().editBatch(parentBatch, sampleCategoryId);

        // open split editor
        parent.switchToAddSampleCategory();

        // update title
        String title = parent.getHandler().buildReminderLabelTitle(parentBatch.getSpecies(),
                                                                     parentBatch,
                                                                     parent.getEditBatchesUIPanel().getTitle(),
                                                                     t("tutti.addSampleCategorySpeciesBatch.title"));
        parent.getAddSampleCategoryBatchPanel().setTitle(title);

    }
}

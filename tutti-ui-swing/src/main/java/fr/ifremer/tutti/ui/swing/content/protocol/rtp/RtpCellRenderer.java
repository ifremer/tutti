package fr.ifremer.tutti.ui.swing.content.protocol.rtp;

/*
 * #%L
 * Tutti :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2016 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.ui.swing.content.protocol.EditProtocolSpeciesRowModel;
import fr.ifremer.tutti.ui.swing.content.protocol.EditProtocolSpeciesTableModel;
import jaxx.runtime.SwingUtil;

import javax.swing.JComponent;
import javax.swing.JTable;
import javax.swing.UIManager;
import javax.swing.table.DefaultTableCellRenderer;
import java.awt.Color;
import java.awt.Font;

import static org.nuiton.i18n.I18n.n;
import static org.nuiton.i18n.I18n.t;

/**
 * Created on 14/01/16.
 *
 * @author Tony Chemit - chemit@codelutin.com
 */
public class RtpCellRenderer extends DefaultTableCellRenderer {

    public static final String TEXT_PATTERN = "<html><body>%s</body></html>";

    private static final long serialVersionUID = 1L;

    private final String withoutRtp;

    private final String withRtp;

    private Font defaultFont;

    private Font selectedFont;

    public RtpCellRenderer() {
        setHorizontalAlignment(CENTER);
        setIcon(SwingUtil.createActionIcon("edit-rtp"));
        this.withoutRtp = n("tutti.editRtp.withoutRtp.tip");
        this.withRtp = n("tutti.editRtp.withRtp.tip");
    }

    @Override
    protected void setValue(Object value) {
        // do nothing
    }

    @Override
    public JComponent getTableCellRendererComponent(JTable table,
                                                    Object value,
                                                    boolean isSelected,
                                                    boolean hasFocus,
                                                    int row,
                                                    int column) {

        if (defaultFont == null) {
            defaultFont = UIManager.getFont("Table.font");
            selectedFont = defaultFont.deriveFont(Font.BOLD);
        }

        int rowIndex = table.convertRowIndexToModel(row);
        EditProtocolSpeciesRowModel rowModel = ((EditProtocolSpeciesTableModel) table.getModel()).getEntry(rowIndex);

        boolean useRtp = rowModel.isUseRtp();

        String toolTipTextValue;
        String textValue;
        if (!useRtp) {

            // use HTML to show the tooltip in italic
            toolTipTextValue = "<i>" + t(withoutRtp) + "</i>";
            textValue = t(withoutRtp);


        } else {
            toolTipTextValue = t("tutti.editRtp.tooltip",
                                 rowModel.getRtpMale().getA(), rowModel.getRtpMale().getB(),
                                 rowModel.getRtpFemale().getA(), rowModel.getRtpFemale().getB(),
                                 rowModel.getRtpUndefined().getA(), rowModel.getRtpUndefined().getB());
            textValue = t(withRtp);

        }
        boolean editable = table.isCellEditable(row, column);
        toolTipTextValue = String.format(TEXT_PATTERN, toolTipTextValue);
        setEnabled(editable);
        setText(textValue);
        setToolTipText(toolTipTextValue);
        setBackground(null);
        setForeground(Color.BLACK);

        if (isSelected) {
            setFont(selectedFont);
        } else {
            setFont(defaultFont);
        }

        return this;
    }
}
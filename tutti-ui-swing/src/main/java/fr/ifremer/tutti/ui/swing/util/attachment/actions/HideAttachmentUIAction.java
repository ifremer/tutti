package fr.ifremer.tutti.ui.swing.util.attachment.actions;

/*
 * #%L
 * Tutti :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.ui.swing.util.actions.SimpleActionSupport;
import fr.ifremer.tutti.ui.swing.util.attachment.AttachmentEditorUI;
import jaxx.runtime.SwingUtil;

import javax.swing.Action;
import javax.swing.ImageIcon;

import static org.nuiton.i18n.I18n.t;

/**
 * Created on 3/9/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.15
 */
public class HideAttachmentUIAction extends SimpleActionSupport<AttachmentEditorUI> {

    private static final long serialVersionUID = 1L;

    public HideAttachmentUIAction(AttachmentEditorUI ui) {
        super(ui);
        ImageIcon actionIcon = SwingUtil.createActionIcon("close-dialog");
        putValue(Action.SMALL_ICON, actionIcon);
        putValue(Action.LARGE_ICON_KEY, actionIcon);
        putValue(Action.ACTION_COMMAND_KEY, "close");
        putValue(Action.NAME, "close");
        putValue(Action.SHORT_DESCRIPTION, t("tutti.attachmentEditor.action.closeAttachment.tip"));
    }

    @Override
    protected void onActionPerformed(AttachmentEditorUI ui) {

        ui.setVisible(false);
        ui.getHandler().onCloseUI();

    }
}

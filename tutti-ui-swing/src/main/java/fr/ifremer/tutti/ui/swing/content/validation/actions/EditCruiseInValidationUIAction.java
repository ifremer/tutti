package fr.ifremer.tutti.ui.swing.content.validation.actions;

/*
 * #%L
 * Tutti :: UI
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Lists;
import fr.ifremer.tutti.persistence.entities.data.Cruise;
import fr.ifremer.tutti.service.catches.ValidateCruiseOperationsService;
import fr.ifremer.tutti.ui.swing.util.actions.LongActionSupport;
import fr.ifremer.tutti.ui.swing.content.cruise.EditCruiseUI;
import fr.ifremer.tutti.ui.swing.content.cruise.EditCruiseUIModel;
import fr.ifremer.tutti.ui.swing.content.operation.EditFishingOperationUIModel;
import fr.ifremer.tutti.ui.swing.content.validation.ValidateCruiseUI;
import fr.ifremer.tutti.ui.swing.content.validation.ValidateCruiseUIHandler;
import fr.ifremer.tutti.ui.swing.content.validation.ValidateCruiseUIModel;
import org.nuiton.validator.NuitonValidatorResult;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.List;

import static org.nuiton.i18n.I18n.t;

/**
 * To edit the given cruise in the validation ui.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.6
 */
public class EditCruiseInValidationUIAction extends LongActionSupport<ValidateCruiseUIModel, ValidateCruiseUI, ValidateCruiseUIHandler> {

    /** Validation service. */
    private final ValidateCruiseOperationsService validationService;

    /**
     * The incoming cruise to edit.
     *
     * Can be null (means do not edit any fishing operation), or with no id
     * (means create a ne fishing operation), or with an id (means edit an
     * existing fishing operation).
     */
    protected Cruise cruise;

    protected final PropertyChangeListener modelListener = new PropertyChangeListener() {

        protected final List<String> propertiesToIgnore = Lists.newArrayList(
                EditFishingOperationUIModel.PROPERTY_VALID,
                EditFishingOperationUIModel.PROPERTY_PERSISTED
        );

        @Override
        public void propertyChange(PropertyChangeEvent evt) {

            EditCruiseUIModel model = (EditCruiseUIModel) evt.getSource();

            if (EditCruiseUIModel.PROPERTY_MODIFY.equals(evt.getPropertyName())) {

                if (!model.isModify()) {

                    // after a save, or a reset, reload model cruise, since the synchronizationStatus may have changed
                    Cruise cruise = getDataContext().reloadCruise();
                    getModel().setCruise(cruise);
                }

            } else if (!propertiesToIgnore.contains(evt.getPropertyName())) {

                Cruise cruise = model.toEntity();
                NuitonValidatorResult validationResult = validationService.validateCruiseCruise(cruise);

                ValidateCruiseUIModel uiModel = getModel();
                uiModel.setCruiseValidatorResult(validationResult);

                getHandler().updateCurrentCruiseNode(validationResult);
            }

        }
    };

    public EditCruiseInValidationUIAction(ValidateCruiseUIHandler handler) {
        super(handler, true);
        setActionDescription(t("tutti.validateCruise.action.editCruise.tip"));
        validationService = getContext().getValidateCruiseOperationsService();
    }

    public void setCruise(Cruise cruise) {
        this.cruise = cruise;
    }

    @Override
    public boolean prepareAction() throws Exception {
        removeListener();

        return super.prepareAction();
    }

    public void removeListener() {
        EditCruiseUI operationPanel = getUI().getCruisePanel();
        operationPanel.getModel().removePropertyChangeListener(modelListener);
    }

    @Override
    public void doAction() throws Exception {
        getUI().getCruisePanel().getHandler().reloadCruise(cruise);
    }

    @Override
    public void postSuccessAction() {
        super.postSuccessAction();

        EditCruiseUI cruisePanel = getUI().getCruisePanel();
        cruisePanel.getModel().addPropertyChangeListener(modelListener);
    }

    @Override
    public void releaseAction() {
        super.releaseAction();
        cruise = null;
    }
}

package fr.ifremer.tutti.ui.swing.content.genericformat.actions;

/*
 * #%L
 * Tutti :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Joiner;
import fr.ifremer.tutti.persistence.entities.data.Program;
import fr.ifremer.tutti.service.genericformat.GenericFormatExportConfiguration;
import fr.ifremer.tutti.service.genericformat.GenericFormatExportResult;
import fr.ifremer.tutti.service.genericformat.GenericFormatExportService;
import fr.ifremer.tutti.ui.swing.content.genericformat.GenericFormatExportUI;
import fr.ifremer.tutti.ui.swing.content.genericformat.GenericFormatExportUIHandler;
import fr.ifremer.tutti.ui.swing.content.genericformat.GenericFormatExportUIModel;
import fr.ifremer.tutti.ui.swing.util.actions.LongActionSupport;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.jaxx.application.ApplicationBusinessException;

import java.io.File;
import java.util.List;

import static org.nuiton.i18n.I18n.t;

/**
 * Created on 3/29/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.14.3
 */
public class GenericFormatExportAction extends LongActionSupport<GenericFormatExportUIModel, GenericFormatExportUI, GenericFormatExportUIHandler> {

    /** Logger. */
    private static final Log log = LogFactory.getLog(GenericFormatImportAction.class);

    private GenericFormatExportResult exportResult;

    private File exportFile;

    public GenericFormatExportAction(GenericFormatExportUIHandler handler) {
        super(handler, false);
    }

    @Override
    public boolean prepareAction() throws Exception {

        boolean doAction = super.prepareAction();

        if (doAction) {

            doAction = getModel().isCanExport();

        }

        if (doAction) {

            Program program = getModel().getProgram();

            // choose file to export
            exportFile = saveFileWithStartDirectory(
                    getConfig().getExportBackupDirectory(),
                    false,
                    "exportProgram-" + program.getName(),
                    "zip",
                    t("tutti.genericFormat.title.choose.exportFile"),
                    t("tutti.genericFormat.action.chooseExportFile"),
                    "^.+\\.zip$", t("tutti.common.file.genericFormat")
            );
            doAction = exportFile != null;
        }

        return doAction;

    }

    @Override
    public void doAction() throws Exception {

        GenericFormatExportConfiguration configuration = getModel().toExportConfiguration();
        configuration.setExportFile(exportFile);

        GenericFormatExportService service = getContext().getGenericFormatExportService();

        int nbSteps = service.getExportNbSteps(configuration);

        if (log.isInfoEnabled()) {
            log.info("Export nb steps: " + nbSteps);
        }
        createProgressionModelIfRequired(nbSteps);

        Program program = getModel().getProgram();

        if (log.isInfoEnabled()) {
            log.info("Do generic format export for program: " + program.getName() + " to file: " + exportFile);
        }

        exportResult = service.export(configuration, getProgressionModel());

        sendMessage(t("tutti.genericFormat.export.action.success", exportFile));

        if (!exportResult.isSuccess()) {

            List<String> errorsByCruise = exportResult.getErrorsByCruise();
            String errorMessage = t("tutti.exportCruise.action.exportErrors", program.getName(), Joiner.on('\n').join(errorsByCruise));
            throw new ApplicationBusinessException(errorMessage);
        }

    }

    @Override
    public void releaseAction() {
        exportFile = null;
        exportResult = null;
        super.releaseAction();
    }

}
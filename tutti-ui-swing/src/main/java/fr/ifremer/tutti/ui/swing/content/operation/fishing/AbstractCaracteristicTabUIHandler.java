
package fr.ifremer.tutti.ui.swing.content.operation.fishing;

/*
 * #%L
 * Tutti :: UI
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import org.nuiton.jaxx.application.swing.table.AbstractApplicationTableModel;
import fr.ifremer.tutti.persistence.entities.CaracteristicMap;
import fr.ifremer.tutti.persistence.entities.TuttiEntities;
import fr.ifremer.tutti.persistence.entities.data.FishingOperation;
import fr.ifremer.tutti.persistence.entities.referential.Caracteristic;
import fr.ifremer.tutti.ui.swing.util.TuttiBeanMonitor;
import fr.ifremer.tutti.ui.swing.util.TuttiUI;
import fr.ifremer.tutti.ui.swing.util.table.AbstractTuttiTableUIHandler;
import jaxx.runtime.swing.editor.bean.BeanFilterableComboBox;
import jaxx.runtime.validator.swing.SwingValidator;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.swing.JComponent;
import java.util.List;

/**
 * @author Kevin Morin - kmorin@codelutin.com
 * @since 1.0
 */
public abstract class AbstractCaracteristicTabUIHandler
        <RM extends AbstractCaracteristicRowModel<RM>,
                M extends AbstractCaracteristicTabUIModel<RM, M>,
                TM extends AbstractApplicationTableModel<RM>,
                UI extends TuttiUI<M, ?>>
        extends AbstractTuttiTableUIHandler<RM, M, UI> {

    private static final Log log =
            LogFactory.getLog(AbstractCaracteristicTabUIHandler.class);

    public AbstractCaracteristicTabUIHandler(String... properties) {
        super(properties);
    }

    //------------------------------------------------------------------------//
    //-- AbstractTuttiTableUIHandler methods                                --//
    //------------------------------------------------------------------------//

    @Override
    public TM getTableModel() {
        return (TM) getTable().getModel();
    }

    @Override
    protected boolean isRowValid(RM row) {
        return row.getValue() != null;
    }

    @Override
    protected void onRowModified(int rowIndex, RM row, String propertyName, Object oldValue, Object newValue) {
        recomputeRowValidState(row);
        super.onRowModified(rowIndex, row, propertyName, oldValue, newValue);
        saveSelectedRowIfNeeded();
    }

    @Override
    protected void saveSelectedRowIfRequired(TuttiBeanMonitor<RM> rowMonitor, RM row) {
        if (row.isValid()) {
            // there is a valid bean attached to the monitor

            if (rowMonitor.wasModified()) {

                // monitored bean was modified, save it
                if (log.isInfoEnabled()) {
                    log.info("Row " + row + " was modified, will save it");
                }

                saveRow(row);

            }
        }
    }

    //------------------------------------------------------------------------//
    //-- AbstractTuttiUIHandler methods                                     --//
    //------------------------------------------------------------------------//

    @Override
    public SwingValidator<M> getValidator() {
        return null;
    }

    @Override
    protected void onAfterSelectedRowChanged(int oldRowIndex,
                                             RM oldRow,
                                             int newRowIndex,
                                             RM newRow) {

        super.onAfterSelectedRowChanged(oldRowIndex, oldRow, newRowIndex, newRow);
        getModel().setRemoveCaracteristicEnabled(newRow != null);
    }

    @Override
    public void beforeInit(UI ui) {
        super.beforeInit(ui);
        M model = createModel();
        ui.setContextValue(model);
    }

    @Override
    public void afterInit(UI ui) {
        initUI(ui);

        initBeanFilterableComboBox(getKeyCombo(), Lists.<Caracteristic>newArrayList(), null);

    }

    @Override
    protected JComponent getComponentToFocus() {
        return getKeyCombo();
    }

    @Override
    public void onCloseUI() {
        if (log.isDebugEnabled()) {
            log.debug("closing: " + getUI());
        }
    }

    //------------------------------------------------------------------------//
    //-- Protected methods                                                  --//
    //------------------------------------------------------------------------//

    protected void saveRow(RM row) {

        if (row.isValid()) {
            CaracteristicMap caracteristics = getModel().getCaracteristicMap();
            Preconditions.checkNotNull(caracteristics);

            caracteristics.put(row.getKey(), row.getValue());
        }
    }

    protected abstract M createModel();

    protected abstract BeanFilterableComboBox<Caracteristic> getKeyCombo();

    protected abstract CaracteristicMap getCaracteristics(FishingOperation operation);

    protected abstract List<String> getProtocolPmfmIds();

    //------------------------------------------------------------------------//
    //-- Public methods                                                     --//
    //------------------------------------------------------------------------//

    /** Adds a row with the parameter selected in the combo box */
    public void addRow() {
        BeanFilterableComboBox<Caracteristic> keyCombo = getKeyCombo();
        Caracteristic selectedItem = (Caracteristic) keyCombo.getSelectedItem();
        TM tableModel = getTableModel();

        RM row = tableModel.createNewRow();
        row.setKey(selectedItem);
//        tableModel.addNewRow(row);
        getModel().getRows().add(row);

        int rowIndex = tableModel.getRowCount() - 1;
        tableModel.fireTableRowsInserted(rowIndex, rowIndex);

        keyCombo.getHandler().removeItem(selectedItem);

        M model = getModel();
        model.setModify(true);
        recomputeRowValidState(row);
    }

    /** Resets the table with the data from the database */
    public void reset(FishingOperation fishingOperation) {
        M model = getModel();

        CaracteristicMap caracteristicMap = getCaracteristics(fishingOperation);
        if (caracteristicMap == null) {
            caracteristicMap = new CaracteristicMap();
        }
        model.setCaracteristicMap(caracteristicMap);

        updateRows();
        model.setModify(false);
    }

    /** Resets the table with the data from the database */
    public void mergeCaracteristics(FishingOperation fishingOperation) {
        CaracteristicMap caracteristicMap = getCaracteristics(fishingOperation);
        if (caracteristicMap != null) {
            CaracteristicMap thisCaracteristicMap = getModel().getCaracteristicMap();
            for (Caracteristic caracteristic : caracteristicMap.keySet()) {
                thisCaracteristicMap.put(caracteristic, caracteristicMap.get(caracteristic));
            }
            getModel().setModify(true);

            updateRows();
        }
    }

    protected void updateRows() {
        TM tableModel = getTableModel();
        M model = getModel();
        List<String> pmfmIds = Lists.newArrayList();

        if (getDataContext().isProtocolFilled()) {
            List<String> protocolPmfmId = getProtocolPmfmIds();
            if (protocolPmfmId != null) {
                pmfmIds.addAll(protocolPmfmId);
            }
        }

        CaracteristicMap caracteristicMap = model.getCaracteristicMap();
        List<RM> rows = Lists.newArrayList();
        List<Caracteristic> caracteristics = Lists.newArrayList(caracteristicMap.keySet());

        List<Caracteristic> availableCaracteristics = model.getAvailableCaracteristics();
        for (String id : pmfmIds) {
            Caracteristic caracteristic = TuttiEntities.findById(availableCaracteristics, id);
            if (!caracteristics.contains(caracteristic)) {
                caracteristics.add(caracteristic);
            }
        }

        for (Caracteristic key : caracteristics) {
            RM newRow = tableModel.createNewRow();
            newRow.setKey(key);
            newRow.setValue(caracteristicMap.get(key));
            rows.add(newRow);
        }

        model.setRows(rows);

        List<Caracteristic> caracteristicList = Lists.newArrayList();

        for (Caracteristic caracteristic : availableCaracteristics) {
            if (!caracteristics.contains(caracteristic)) {
                caracteristicList.add(caracteristic);
            }
        }

        BeanFilterableComboBox<Caracteristic> keyCombo = getKeyCombo();
        keyCombo.setData(caracteristicList);
//        selectFirstInCombo(keyCombo);
        keyCombo.getHandler().reset();
    }

    public void removeCaracteristic() {
        int rowIndex = getTable().getSelectedRow();

        Preconditions.checkState(
                rowIndex != -1,
                "Cant remove caracteristic if no caracteristic selected");

        RM row = getTableModel().getEntry(rowIndex);

        CaracteristicMap caracteristicMap = getModel().getCaracteristicMap();
        if (caracteristicMap != null) {
            caracteristicMap.remove(row.getKey());
        }

        //add the row in the combo
        BeanFilterableComboBox keyCombo = getKeyCombo();
        keyCombo.addItem(row.getKey());
//        selectFirstInCombo(keyCombo);
        keyCombo.getHandler().reset();

        // remove the row from the model
        getModel().getRows().remove(rowIndex);

        // refresh all the table
        getTableModel().fireTableRowsDeleted(rowIndex, rowIndex);

        getModel().removeRowInError(row);
    }
}

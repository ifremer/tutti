package fr.ifremer.tutti.ui.swing.content.operation.catches.species.edit.actions;

/*
 * #%L
 * Tutti :: UI
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import fr.ifremer.tutti.persistence.entities.TuttiEntities;
import fr.ifremer.tutti.ui.swing.content.operation.catches.species.edit.SpeciesBatchRowModel;
import fr.ifremer.tutti.ui.swing.content.operation.catches.species.edit.SpeciesBatchUI;
import fr.ifremer.tutti.ui.swing.content.operation.catches.species.edit.SpeciesBatchUIHandler;
import fr.ifremer.tutti.ui.swing.content.operation.catches.species.edit.SpeciesBatchUIModel;
import fr.ifremer.tutti.ui.swing.util.TuttiUIUtil;
import fr.ifremer.tutti.ui.swing.util.actions.LongActionSupport;
import jaxx.runtime.SwingUtil;
import org.jdesktop.swingx.JXTable;

import javax.swing.JOptionPane;
import java.util.HashSet;
import java.util.Set;

import static org.nuiton.i18n.I18n.t;

/**
 * To remove a species batch children.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.0
 */
public class RemoveSpeciesSubBatchAction extends LongActionSupport<SpeciesBatchUIModel, SpeciesBatchUI, SpeciesBatchUIHandler> {

    /**
     * Selected row to treat.
     *
     * @since 2.8
     */
    protected int rowIndex;

    /**
     * Rows to delete after all.
     *
     * @since 3.0-rc-1
     */
    protected Set<SpeciesBatchRowModel> rowToRemove;

    /**
     * Parent batch of rows to delete.
     *
     * @since 3.0-rc-1
     */
    protected SpeciesBatchRowModel parentBatch;

    public RemoveSpeciesSubBatchAction(SpeciesBatchUIHandler handler) {
        super(handler, false);
    }

    @Override
    public boolean prepareAction() throws Exception {
        boolean result = super.prepareAction();

        rowIndex = -1;

        if (result) {
            int answer = JOptionPane.showConfirmDialog(getContext().getActionUI(),
                                                       t("tutti.editSpeciesBatch.action.removeSubBatch.confirm.message"),
                                                       t("tutti.editSpeciesBatch.action.removeSubBatch.confirm.title"),
                                                       JOptionPane.YES_NO_OPTION);
            result = answer == JOptionPane.YES_OPTION;
        }

        return result;
    }

    @Override
    public void doAction() throws Exception {

        JXTable table = handler.getTable();

        rowIndex = SwingUtil.getSelectedModelRow(table);

        Preconditions.checkState(rowIndex != -1, "Cant remove sub batch if no batch selected");

        parentBatch = handler.getTableModel().getEntry(rowIndex);

        Preconditions.checkState(!TuttiEntities.isNew(parentBatch), "Can't remove sub batch if batch is not persisted");

        // save parent batch (will destroy all his childs from db)
        getModel().getSpeciesOrBenthosBatchUISupport().deleteSpeciesSubBatch(parentBatch.getIdAsInt());

        // collect of rows to remove from model

        rowToRemove = new HashSet<>();

        handler.collectChildren(parentBatch, rowToRemove);
    }

    @Override
    public void postSuccessAction() {
        super.postSuccessAction();

        JXTable table = handler.getTable();

        // remove all rows from the model
        getModel().getRows().removeAll(rowToRemove);

        // remove childs from parent batch
        parentBatch.setChildBatch(null);

        // refresh table from parent batch row index to the end
        handler.getTableModel().fireTableDataChanged();

        // select parent batch row
        TuttiUIUtil.selectFirstCellOnRow(table, rowIndex, true);
    }
}

package fr.ifremer.tutti.ui.swing.content.db.actions;

/*
 * #%L
 * Tutti :: UI
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.ProgressionModel;
import fr.ifremer.tutti.ui.swing.RunTutti;
import fr.ifremer.tutti.ui.swing.TuttiUIContext;
import fr.ifremer.tutti.ui.swing.content.MainUIHandler;
import fr.ifremer.tutti.ui.swing.content.actions.AbstractMainUITuttiAction;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.jaxx.application.ApplicationIOUtil;

import java.io.File;
import java.util.Date;

import static org.nuiton.i18n.I18n.t;

/**
 * To install (or reinstall) a db from last network one.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.4
 */
public class ReinstallDbAction extends AbstractMainUITuttiAction {

    /** Logger. */
    private static final Log log = LogFactory.getLog(ReinstallDbAction.class);

    protected File backupFile;

    protected String jdbcUrl;

    public ReinstallDbAction(MainUIHandler handler) {
        super(handler, true);
        setActionDescription(t("tutti.dbManager.action.installDb.tip"));
    }

    @Override
    public boolean prepareAction() throws Exception {
        boolean doAction = super.prepareAction();

        if (doAction) {

            // check we can connect to remote install server

            // check db url is reachable
            TuttiUIContext context = getContext();
            doAction = context.checkUpdateDataReachable(true);
        }

        if (doAction) {

            jdbcUrl = null;
            backupFile = null;

            jdbcUrl = getConfig().getJdbcUrl();

            if (getModel().isDbExist()) {

                if (getConfig().isImportDbSkipBackup()) {

                    if (log.isInfoEnabled()) {
                        log.info("Skip backup before import, lucky you...");
                    }

                } else {

                    displayInfoMessage(
                            t("tutti.dbManager.title.backup.db"),
                            t("tutti.dbManager.action.installDb.backup.db")
                    );

                    // choose backup file
                    backupFile = saveFile(
                            getConfig().getDbBackupDirectory(),
                            "tutti-db-" + ExportDbAction.df.format(new Date()),
                            "zip",
                            t("tutti.dbManager.title.choose.dbExportFile"),
                            t("tutti.dbManager.action.chooseDbExportFile"),
                            "^.*\\.zip", t("tutti.common.file.zip")
                    );

                    if (backupFile == null) {

                        displayWarningMessage(
                                t("tutti.dbManager.title.backup.db"),
                                t("tutti.dbManager.action.installDb.no.backup.db.choosen")
                        );

                        doAction = false;
                    }

                }

            }
        }

        if (doAction) {

            ProgressionModel progressionModel = new ProgressionModel();
            progressionModel.setTotal(3 + (backupFile == null ? 0 : 1));
            setProgressionModel(progressionModel);
        }
        return doAction;
    }

    @Override
    public void doAction() {

        ProgressionModel progressionModel = getProgressionModel();

        boolean doBackup = backupFile != null;

        // close db
        progressionModel.increments(t("tutti.reinstallDb.step.closeDb", jdbcUrl));

        if (!doBackup) {
            getContext().getPersistenceService().setSkipShutdownDbWhenClosing();
        }
        getContext().closePersistenceService();

        if (doBackup) {

            // backup db
            progressionModel.increments(t("tutti.reinstallDb.step.backupDb", backupFile));
            getContext().getPersistenceService().exportDb(backupFile);

        }

        // clean db context
        getContext().clearDbContext();

        // write restart action file (will be loaded at restart)
        String actionContent = InstallDbAction.class.getName();

        File startActionFile = getConfig().getStartActionFile();
        ApplicationIOUtil.writeContent(startActionFile, actionContent, t("tutti.error.write.startActionFile", startActionFile));

        // delete db files on exit
        getContext().deleteDbOnExit();

        // restart application
        progressionModel.increments(t("tutti.reinstallDb.step.reloadApplication"));
        RunTutti.closeTutti(getHandler(), RunTutti.RESTART_EXIT_CODE);

    }
}

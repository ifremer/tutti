package fr.ifremer.tutti.ui.swing.update.module;

/*
 * #%L
 * Tutti :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import fr.ifremer.adagio.core.service.technical.synchro.ReferentialSynchroContext;
import fr.ifremer.adagio.core.service.technical.synchro.ReferentialSynchroResult;
import fr.ifremer.tutti.persistence.entities.data.SampleCategoryModel;
import fr.ifremer.tutti.service.PersistenceService;
import fr.ifremer.tutti.service.referential.TuttiReferentialSynchronizeService;
import fr.ifremer.tutti.ui.swing.TuttiUIContext;
import fr.ifremer.tutti.ui.swing.updater.DeleteHelper;
import fr.ifremer.tutti.ui.swing.updater.UpdateModule;
import org.apache.commons.io.FileUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.jaxx.application.ApplicationIOUtil;
import org.nuiton.jaxx.application.ApplicationTechnicalException;
import org.nuiton.jaxx.application.swing.action.ApplicationActionUI;
import org.nuiton.jaxx.application.type.ApplicationProgressionModel;
import org.nuiton.updater.ApplicationInfo;
import org.nuiton.updater.ApplicationUpdater;

import java.io.File;
import java.io.IOException;

import static org.nuiton.i18n.I18n.t;

/**
 * Created on 1/28/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.13
 */
public class DbModuleUpdater extends ModuleUpdaterSupport {

    /** Logger. */
    private static final Log log = LogFactory.getLog(DbModuleUpdater.class);

    protected boolean dbInstalled;

    protected boolean dbUpdated;

    public DbModuleUpdater() {
        super(UpdateModule.db);
    }

    public boolean isDbInstalled() {
        return dbInstalled;
    }

    public boolean isDbUpdated() {
        return dbUpdated;
    }

    @Override
    protected void onUpdateToDo(TuttiUIContext context, ApplicationInfo info) {

        if (info == null) {
            dbInstalled = false;
            dbUpdated = false;
        } else {

            if (log.isInfoEnabled()) {
                log.info("Find a updatable module : " + updateModule);
            }

            if (context.isDbExist()) {

                // when db exists always an update
                dbUpdated = true;
            } else {

                // when no db, then always install
                dbInstalled = true;
            }
        }

    }

    @Override
    public void onUpdateDone(TuttiUIContext context, ApplicationInfo info) {

        if (log.isInfoEnabled()) {
            log.info(String.format(
                    "A db update was downloaded (oldVersion: %s, newVersion: %s).",
                    info.oldVersion, info.newVersion));
        }

        if (dbInstalled || dbUpdated) {

            File source = getDbDirectory(info);

            if (dbInstalled) {

                // first database, just copy it to correct directory
                prepareFirstDatabase(context, info, source);

            } else if (dbUpdated) {

                // launch a referential synchronize operation
                synchronizeDatabase(context, info, source);

            }

            if (log.isInfoEnabled()) {
                log.info("Delete update directory: " + source);
            }
            try {
                FileUtils.deleteDirectory(source);
            } catch (IOException e) {
                throw new ApplicationTechnicalException(t("tutti.applicationUpdater.prepareFirstDB.deleteDirectory.error", source), e);
            }

        }

    }

    @Override
    public String getLabel() {
        return t("tutti.update.db");
    }

    protected void prepareFirstDatabase(TuttiUIContext context, ApplicationInfo info, File source) {

        if (log.isInfoEnabled()) {
            log.info("First time database was downloaded at version: " + info.newVersion);
        }

        File target = context.getConfig().getDbDirectory();
        if (log.isInfoEnabled()) {
            log.info("Copy from " + source + " to " + target);
        }

        try {
            FileUtils.copyDirectory(source, target);
        } catch (IOException e) {
            throw new ApplicationTechnicalException(t("tutti.applicationUpdater.prepareFirstDB.copyDirectory.error", source, target), e);
        }

    }

    protected void synchronizeDatabase(TuttiUIContext context, ApplicationInfo info, File source) {

        if (log.isInfoEnabled()) {
            log.info(String.format("A database update was downloaded (oldVersion: %s, newVersion: %s), will launch a referential synchronize operation ", info.oldVersion, info.newVersion));
        }

        TuttiReferentialSynchronizeService service = context.getTuttiReferentialSynchronizeService();

        File dbDirectory = getDbDirectoryCopy(source);

        ReferentialSynchroContext synchroContext = service.createSynchroContext(dbDirectory);
        ReferentialSynchroResult result = synchroContext.getResult();

        ApplicationActionUI actionUI = context.getActionUI();
        actionUI.getModel().setProgressionModel(new DelegateProgressionModel(result.getProgressionModel()));
        service.prepare(synchroContext);

        if (!result.isSuccess()) {
            throw new ApplicationTechnicalException(t("tutti.applicationUpdater.synchroDB.prepare.error"), result.getError());
        }

        service.synchronize(synchroContext);

        if (!result.isSuccess()) {
            throw new ApplicationTechnicalException(t("tutti.applicationUpdater.synchroDB.synchro.error"), result.getError());
        }

        // reset cache
        if (log.isInfoEnabled()) {
            log.info("Reset all caches.");
        }
        PersistenceService persistence = context.getPersistenceService();
        persistence.clearAllCaches();

        // clean data context
        SampleCategoryModel sampleCategoryModel = context.getConfig().getSampleCategoryModel();
        if (log.isInfoEnabled()) {
            log.info("SampleCategoryModel to reload: " + sampleCategoryModel);
        }
        if (log.isInfoEnabled()) {
            log.info("Clean data context.");
        }
        context.getDataContext().clearContext();
        if (log.isInfoEnabled()) {
            log.info("SampleCategoryModel to reload (after clearContext): " + sampleCategoryModel);
        }
        context.getDataContext().setSampleCategoryModel(sampleCategoryModel);

        // replace the version.appup file content
        File target = context.getConfig().getDbDirectory();
        File versionFile = ApplicationUpdater.getVersionFile(target);
        if (log.isInfoEnabled()) {
            log.info("Replace content of file " + versionFile + " with " + info.newVersion);
        }
        try {
            ApplicationUpdater.storeVersionFile(target, info.newVersion);
        } catch (IOException e) {
            throw new ApplicationTechnicalException(
                    t("tutti.applicationUpdater.synchroDB.writeVersion.error", versionFile));
        }
    }

    protected File getDbDirectory(ApplicationInfo info) {
        File[] sources = info.destDir.listFiles();
        Preconditions.checkNotNull(sources, "Downloaded db must have at least on directory, see " + info.destDir);
        Preconditions.checkState(sources.length == 1, "Downloaded db should contains one directory at " + info.destDir);
        return sources[0];
    }

    protected File getDbDirectoryCopy(File source) {

        File temporaryDirectory = ApplicationIOUtil.createTemporaryDirectory(this.toString());
        File[] files = source.listFiles();
        if (files != null) {

            for (File file : files) {
                ApplicationIOUtil.copyFileToDirectory(file, temporaryDirectory, "Can't copy file " + file + " to directory " + temporaryDirectory);
            }

        }

        try {
            DeleteHelper.deleteDirectoryOnExit(temporaryDirectory.toPath());
        } catch (IOException e) {
            throw new ApplicationTechnicalException("Can't mark directory " + temporaryDirectory + "to be deleted on exit", e);
        }

        return temporaryDirectory;

    }

    private static class DelegateProgressionModel extends ApplicationProgressionModel {

        private static final long serialVersionUID = 1L;

        private final fr.ifremer.adagio.core.type.ProgressionModel progressionModel;

        public DelegateProgressionModel(fr.ifremer.adagio.core.type.ProgressionModel progressionModel) {

            this.progressionModel = progressionModel;
            this.progressionModel.addPropertyChangeListener(evt -> firePropertyChange(evt.getPropertyName(), evt.getOldValue(), evt.getNewValue()));
        }

        @Override
        public void setMessage(String message) {
            progressionModel.setMessage(message);
        }

        @Override
        public void increments(String message) {
            progressionModel.increments(message);
        }

        @Override
        public String getMessage() {
            return progressionModel.getMessage();
        }

        @Override
        public void setRate(float rate) {
            progressionModel.setRate(rate);
        }

        @Override
        public float getRate() {
            return progressionModel.getRate();
        }

        @Override
        public void increments(int nb) {
            progressionModel.increments(nb);
        }

        @Override
        public void setCurrent(int current) {
            progressionModel.setCurrent(current);
        }

        @Override
        public int getCurrent() {
            return progressionModel.getCurrent();
        }

        @Override
        public void adaptTotal(int total) {
            progressionModel.adaptTotal(total);
        }

        @Override
        public void setTotal(int total) {
            progressionModel.setTotal(total);
        }

        @Override
        public int getTotal() {
            return progressionModel.getTotal();
        }
    }
}

package fr.ifremer.tutti.ui.swing.util.species;

/*
 * #%L
 * Tutti :: UI
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Lists;
import fr.ifremer.tutti.persistence.entities.referential.Species;
import fr.ifremer.tutti.ui.swing.util.AbstractTuttiBeanUIModel;

import java.util.List;

/**
 * @author Kevin Morin - kmorin@codelutin.com
 * @since 1.0
 */
public class SelectSpeciesUIModel extends AbstractTuttiBeanUIModel<SelectSpeciesUIModel, SelectSpeciesUIModel> {

    public static final String PROPERTY_SPECIES = "species";

    public static final String PROPERTY_FILTERED_SPECIES = "filteredSpecies";

    public static final String PROPERTY_SELECTED_SPECIES = "selectedSpecies";

    public static final String PROPERTY_SHOW_ALL_SPECIES = "showAllSpecies";

    protected List<Species> species = Lists.newArrayList();

    protected List<Species> filteredSpecies = null;

    protected Species selectedSpecies;

    protected boolean showAllSpecies;

    public SelectSpeciesUIModel() {
        super(null, null);
    }

    public List<Species> getSpecies() {
        return species;
    }

    public void setSpecies(List<Species> species) {
        Object oldValue = getSpecies();
        this.species = Lists.newArrayList(species);
        firePropertyChange(PROPERTY_SPECIES, oldValue, this.species);
    }

    public Species getSelectedSpecies() {
        return selectedSpecies;
    }

    public void setSelectedSpecies(Species selectedSpecies) {
        Object oldValue = getSelectedSpecies();
        this.selectedSpecies = selectedSpecies;
        firePropertyChange(PROPERTY_SELECTED_SPECIES, oldValue, selectedSpecies);
    }

    public boolean isShowAllSpecies() {
        return showAllSpecies;
    }

    public void setShowAllSpecies(boolean showAllSpecies) {
        Object oldValue = isShowAllSpecies();
        this.showAllSpecies = showAllSpecies;
        firePropertyChange(PROPERTY_SHOW_ALL_SPECIES, oldValue, showAllSpecies);
    }

    public List<Species> getFilteredSpecies() {
        return filteredSpecies;
    }

    public void setFilteredSpecies(List<Species> filteredSpecies) {
        Object oldValue = getFilteredSpecies();
        this.filteredSpecies = filteredSpecies;
        firePropertyChange(PROPERTY_FILTERED_SPECIES, oldValue, filteredSpecies);
    }

    @Override
    protected SelectSpeciesUIModel newEntity() {
        return new SelectSpeciesUIModel();
    }
}

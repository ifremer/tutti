package fr.ifremer.tutti.ui.swing.content.operation.catches.species.frequency;

/*
 * #%L
 * Tutti :: UI
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.ui.swing.content.operation.catches.species.frequency.actions.DeleteSpeciesFrequencyLogRowAction;
import jaxx.runtime.swing.JAXXWidgetUtil;

import javax.swing.AbstractCellEditor;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.UIManager;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Font;

/**
 * Component to render and delete log items from logs table.
 *
 * @author Kevin Morin (Code Lutin)
 * @since 3.8
 */
public class SpeciesFrequencyLogCellComponent extends JPanel {

    private static final long serialVersionUID = -3050615446905949687L;

    private JLabel label = new JLabel();

    public SpeciesFrequencyLogCellComponent(DeleteSpeciesFrequencyLogRowAction action) {

        setLayout(new BorderLayout());

        Font defaultFont = UIManager.getFont("Table.font");
        label.setFont(defaultFont);
        label.setOpaque(false);
        add(label, BorderLayout.CENTER);

        JButton deleteButton = new JButton();
        deleteButton.setIcon(JAXXWidgetUtil.createActionIcon("delete"));
        deleteButton.setBorderPainted(false);
        deleteButton.setBorder(null);
        deleteButton.setBackground(null);
        label.setOpaque(false);
        add(deleteButton, BorderLayout.EAST);
        if (action != null) {
            deleteButton.setAction(action);
        }

    }

    public void setData(String data) {
        label.setText(data);
    }

    public static TableCellRenderer newRender() {
        return new FrequencyLogCellRenderer();
    }

    public static TableCellEditor newEditor(SpeciesFrequencyUI speciesFrequencyUI) {
        return new FrequencyLogCellEditor(speciesFrequencyUI);
    }

    public static class FrequencyLogCellEditor extends AbstractCellEditor implements TableCellEditor {

        private static final long serialVersionUID = 1L;

        protected final SpeciesFrequencyLogCellComponent component;

        private SpeciesFrequencyLogRowModel row;

        public FrequencyLogCellEditor(SpeciesFrequencyUI speciesFrequencyUI) {
            component = new SpeciesFrequencyLogCellComponent(new DeleteSpeciesFrequencyLogRowAction(speciesFrequencyUI, this));
        }

        public void setRow(SpeciesFrequencyLogRowModel row) {
            this.row = row;
        }

        public SpeciesFrequencyLogRowModel getRow() {
            return row;
        }

        @Override
        public Component getTableCellEditorComponent(JTable table,
                                                     Object value,
                                                     boolean isSelected,
                                                     int row,
                                                     int column) {

            SpeciesFrequencyLogsTableModel tableModel = (SpeciesFrequencyLogsTableModel) table.getModel();
            SpeciesFrequencyLogRowModel editRow = tableModel.getEntry(row);
            setRow(editRow);

            String data = (String) value;
            component.setData(data);

            return component;
        }

        @Override
        public Object getCellEditorValue() {
            return null;
        }

    }

    public static class FrequencyLogCellRenderer implements TableCellRenderer {

        protected final SpeciesFrequencyLogCellComponent component;

        public FrequencyLogCellRenderer() {
            component = new SpeciesFrequencyLogCellComponent(null);
        }

        @Override
        public Component getTableCellRendererComponent(JTable table,
                                                       Object value,
                                                       boolean isSelected,
                                                       boolean hasFocus,
                                                       int row,
                                                       int column) {
            String data = (String) value;
            component.setData(data);

            component.setBackground(Color.WHITE);

            return component;
        }

    }

}
package fr.ifremer.tutti.ui.swing.content.category.actions;

/*
 * #%L
 * Tutti :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import fr.ifremer.tutti.ui.swing.content.category.EditSampleCategoryModelUI;
import fr.ifremer.tutti.ui.swing.util.actions.SimpleActionSupport;

/**
 * Created on 3/7/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.15
 */
public class MoveUpSampleCategoryRowAction extends SimpleActionSupport<EditSampleCategoryModelUI> {

    private static final long serialVersionUID = 1L;

    public MoveUpSampleCategoryRowAction(EditSampleCategoryModelUI ui) {
        super(ui);
    }

    @Override
    protected void onActionPerformed(EditSampleCategoryModelUI ui) {

        int selectedRow = ui.getTable().getSelectedRow();
        Preconditions.checkState(selectedRow > -1);

        int newRow = selectedRow - 1;

        ui.getHandler().getTableModel().permuteEntry(newRow, selectedRow);
        ui.getTable().getSelectionModel().setSelectionInterval(newRow, newRow);
        ui.getModel().setModify(true);

    }

}
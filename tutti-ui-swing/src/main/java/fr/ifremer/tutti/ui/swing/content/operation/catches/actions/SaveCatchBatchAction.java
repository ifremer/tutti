package fr.ifremer.tutti.ui.swing.content.operation.catches.actions;

/*
 * #%L
 * Tutti :: UI
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.entities.data.CatchBatch;
import fr.ifremer.tutti.service.PersistenceService;
import fr.ifremer.tutti.ui.swing.content.operation.catches.EditCatchesUI;
import fr.ifremer.tutti.ui.swing.content.operation.catches.EditCatchesUIHandler;
import fr.ifremer.tutti.ui.swing.content.operation.catches.EditCatchesUIModel;
import fr.ifremer.tutti.ui.swing.util.TuttiBeanMonitor;
import fr.ifremer.tutti.ui.swing.util.actions.LongActionSupport;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * To save a {@link CatchBatch}.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.0
 */
public class SaveCatchBatchAction extends LongActionSupport<EditCatchesUIModel, EditCatchesUI, EditCatchesUIHandler> {

    /** Logger. */
    private static final Log log = LogFactory.getLog(SaveCatchBatchAction.class);

    /**
     * A flag to update ui after create or save the edit catch batch.
     *
     * @since 1.0
     */
    protected boolean updateUI;

    public SaveCatchBatchAction(EditCatchesUIHandler handler) {
        super(handler, true);
    }

    public void setUpdateUI(boolean updateUI) {
        this.updateUI = updateUI;
    }

    @Override
    public void releaseAction() {
        updateUI = true;
        super.releaseAction();
    }

    @Override
    public void doAction() throws Exception {

        TuttiBeanMonitor<EditCatchesUIModel> monitor = handler.getCatchBatchMonitor();

        // previous fishingOperation was modified, let's save it
        EditCatchesUIModel beanToSave = monitor.getBean();

        // must save when bean is new or was modifiy and is valid
        boolean mustSave = (beanToSave.isCreate() || beanToSave.isModify()) && beanToSave.isValid();

        if (mustSave) {

            PersistenceService persistenceService = getContext().getPersistenceService();

            CatchBatch catchBatch = beanToSave.toEntity();

            if (log.isInfoEnabled()) {
                log.info("FishingOperation " + catchBatch.getId() + " was modified, will save it.");
            }

            //FIXME I18n
            sendMessage("[ Captures - Caractéristiques générales ] Sauvegarde des modifications du résumé de la capture.");

            persistenceService.saveCatchBatch(catchBatch);

            monitor.clearModified();
            getModel().setModify(false);
        }

        getUI().getSpeciesTabPanel().getEditBatchesUI().getHandler().clearTableSelection();
        getUI().getBenthosTabPanel().getEditBatchesUI().getHandler().clearTableSelection();

    }

}

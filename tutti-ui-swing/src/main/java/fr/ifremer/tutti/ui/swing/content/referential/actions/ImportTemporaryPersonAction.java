package fr.ifremer.tutti.ui.swing.content.referential.actions;

/*
 * #%L
 * Tutti :: UI
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.entities.referential.Person;
import fr.ifremer.tutti.service.referential.ReferentialImportResult;
import fr.ifremer.tutti.service.referential.ReferentialTemporaryPersonService;
import fr.ifremer.tutti.ui.swing.content.referential.ManageTemporaryReferentialUIHandler;

import java.io.File;

import static org.nuiton.i18n.I18n.t;

/**
 * Import temporary person referential.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.0
 */
public class ImportTemporaryPersonAction extends ImportTemporaryActionSupport<Person> {

    public ImportTemporaryPersonAction(ManageTemporaryReferentialUIHandler handler) {
        super(handler);
    }

    @Override
    protected File chooseImportFile() {

        return chooseFile(
                t("tutti.manageTemporaryReferential.title.choose.importTemporaryPersonFile"),
                t("tutti.manageTemporaryReferential.action.chooseReferentialPersonFile.import"),
                "^.*\\.csv", t("tutti.common.file.csv"));

    }

    @Override
    protected ReferentialImportResult<Person> doImport(File file) {

        ReferentialTemporaryPersonService service = getContext().getReferentialTemporaryPersonService();
        return service.importTemporaryPerson(file);

    }

    @Override
    public void postSuccessAction(File file, ReferentialImportResult<Person> result) {

        int nbRef = getModel().getNbTemporaryPersons();
        getModel().setNbTemporaryPersons(nbRef + result.getNbRefAdded() - result.getNbRefDeleted());

        // reset ui cache
        getDataContext().resetPersons();
        reloadCruise();
        reloadFishingOperation();

        getHandler().resetComboBoxAction(getUI().getPersonActionComboBox());

        String title = t("tutti.manageTemporaryReferential.action.chooseReferentialPersonFile.import.dialog.title");
        String message = t("tutti.manageTemporaryReferential.action.chooseReferentialPersonFile.import.dialog.message",
                           result.getNbRefAdded(), result.getNbRefUpdated(), result.getNbRefDeleted());
        displayInfoMessage(title, message);

        sendMessage(t("tutti.manageTemporaryReferential.action.chooseReferentialPersonFile.import.success", file));

    }

}

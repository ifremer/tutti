package fr.ifremer.tutti.ui.swing.content.config;

/*
 * #%L
 * Tutti :: UI
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.TuttiConfiguration;
import fr.ifremer.tutti.TuttiConfigurationOption;
import fr.ifremer.tutti.type.WeightUnit;
import fr.ifremer.tutti.ui.swing.TuttiUIContext;
import fr.ifremer.tutti.ui.swing.content.actions.GoToPreviousScreenAction;
import fr.ifremer.tutti.ui.swing.content.config.actions.ReloadTuttiAction;
import fr.ifremer.tutti.ui.swing.util.AbstractTuttiUIHandler;
import fr.ifremer.tutti.ui.swing.util.editor.TuttiLocationTableCell;
import fr.ifremer.tutti.ui.swing.util.editor.VesselTableCell;
import jaxx.runtime.SwingUtil;
import jaxx.runtime.swing.config.ConfigCategoryUI;
import jaxx.runtime.swing.config.ConfigUI;
import jaxx.runtime.swing.config.ConfigUIHelper;
import jaxx.runtime.swing.config.model.MainCallBackFinalizer;
import jaxx.runtime.swing.editor.EnumEditor;
import jaxx.runtime.validator.swing.SwingValidator;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jdesktop.swingx.JXTable;

import javax.swing.DefaultCellEditor;
import javax.swing.DefaultListCellRenderer;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JList;
import javax.swing.JTable;
import javax.swing.ListCellRenderer;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellEditor;
import java.awt.BorderLayout;
import java.awt.Component;
import java.util.Objects;

import static org.nuiton.i18n.I18n.n;
import static org.nuiton.i18n.I18n.t;

/**
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.1
 */
public class TuttiConfigUIHandler extends AbstractTuttiUIHandler<TuttiUIContext, TuttiConfigUI> {

    /** Logger. */
    private static final Log log =
            LogFactory.getLog(TuttiConfigUIHandler.class);


    public static final String CALLBACK_APPLICATION = "application";

    public static final String CALLBACK_UI = "ui";

    public static final String CALLBACK_SHORTCUT = "shortcut";

    @Override
    public SwingValidator<TuttiUIContext> getValidator() {
        return null;
    }

    @Override
    public void afterInit(TuttiConfigUI ui) {

        initUI(ui);

        TuttiConfiguration config = getConfig();

        ConfigUIHelper helper = new ConfigUIHelper(config.getApplicationConfig(),
                                                   config.getConfigFile());

        helper.registerCallBack(
                CALLBACK_UI, n("tutti.config.action.reload.ui"),
                SwingUtil.createActionIcon("reload-ui"),
                this::reloadUI
        ).registerCallBack(
                CALLBACK_APPLICATION, n("tutti.config.action.reload.application"),
                SwingUtil.createActionIcon("reload-application"),
                this::reloadApplication
        ).registerCallBack(
                CALLBACK_SHORTCUT, n("tutti.config.action.reload.actions"),
                SwingUtil.createActionIcon("reload-shortcut"),
                this::reloadShortcuts
        );

        TableCellEditor stringDefaultEditor = new JXTable().getDefaultEditor(String.class);

        // Application category

        helper.addCategory(n("tutti.config.category.applications"),
                           n("tutti.config.category.applications.description"))
              .addOption(TuttiConfigurationOption.CSV_SEPARATOR)
              .setOptionShortLabel(t("tutti.config.option.csv.separator.shortLabel"))

              .addOption(TuttiConfigurationOption.UI_CONFIG_FILE)
              .setOptionShortLabel(t("tutti.config.option.ui.config.file.shortLabel"))
              .setOptionCallBack(CALLBACK_APPLICATION)

              .addOption(TuttiConfigurationOption.TOTAL_SORTED_WEIGHTS_DIFFERENCE_RATE)
              .setOptionShortLabel(t("tutti.config.option.weights.rate.difference.totalAndSorted.shortLabel"))

              .addOption(TuttiConfigurationOption.RTP_WEIGHTS_DIFFERENCE_RATE)
              .setOptionShortLabel(t("tutti.config.option.weights.rate.difference.rtp.shortLabel"))

              .addOption(TuttiConfigurationOption.MAX_INDIVIDUAL_OBSERVATION_ROW_TO_CREATE)
              .setOptionShortLabel(t("tutti.config.option.ui.individualObservationRowsToCreate.shortLabel"))

              .addOption(TuttiConfigurationOption.SAMPLING_CODE_PREFIX)
              .setOptionShortLabel(t("tutti.config.option.ui.samplingCodePrefix.shortLabel"))
              .setOptionEditor(stringDefaultEditor) // On force ici le type car les éditeurs de navire et pays sont aussi de type String...

              .addOption(TuttiConfigurationOption.SHOW_BATCH_LOG)
              .setOptionShortLabel(t("tutti.config.option.ui.showBatchLog.shortLabel"))

              .addOption(TuttiConfigurationOption.SHOW_MEMORY_USAGE)
              .setOptionShortLabel(t("tutti.config.option.ui.showMemoryUsage.shortLabel"))
              .setOptionCallBack(CALLBACK_UI)

              .addOption(TuttiConfigurationOption.GENERIC_FORMAT_IMPORT_SKIP_BACKUP)
              .setOptionShortLabel(t("tutti.config.option.ui.genericFormat.import.skipBackup.shortLabel"))

              .addOption(TuttiConfigurationOption.GENERIC_FORMAT_IMPORT_MAXIMUM_ROWS_IN_ERROR_PER_FILE)
              .setOptionShortLabel(t("tutti.config.option.ui.genericFormat.import.maximumRowsInErrorPerFile.shortLabel"))

              .addOption(TuttiConfigurationOption.IMPORT_DB_SKIP_BACKUP)
              .setOptionShortLabel(t("tutti.config.option.ui.db.import.skipBackup.shortLabel"))

              .addOption(TuttiConfigurationOption.PUPITRI_IMPORT_MISSING_BATCHES)
              .setOptionShortLabel(t("tutti.config.option.pupitriImportMissingBatches.shortLabel"));


        TuttiUIContext context = getContext();

        if (context.isDbLoaded()) {

            VesselTableCell vesselTableCellComponent = new VesselTableCell(context);
            helper.addOption(TuttiConfigurationOption.TREMIE_CAROUSSEL_VESSEL_ID)
                  .setOptionShortLabel(t("tutti.config.option.tremieCarousselVessel.shortLabel"))
                  .setOptionEditor(vesselTableCellComponent.getNewTableCellEditor())
                  .setOptionRenderer(vesselTableCellComponent.getNewTableCellRenderer());

            TuttiLocationTableCell tuttiLocationTableCellComponent = new TuttiLocationTableCell(context);
            helper.addOption(TuttiConfigurationOption.EXPORT_COUNTRY_ID)
                  .setOptionShortLabel(t("tutti.config.option.export.countryId.shortLabel"))
                  .setOptionEditor(tuttiLocationTableCellComponent.getNewTableCellEditor())
                  .setOptionRenderer(tuttiLocationTableCellComponent.getNewTableCellRenderer());
        }

        // UI category

        helper.addCategory(n("tutti.config.category.ui"),
                           n("tutti.config.category.ui.description"),
                           CALLBACK_UI)
              .addOption(TuttiConfigurationOption.COLOR_ALTERNATE_ROW)
              .setOptionShortLabel(t("tutti.config.option.ui.color.alternateRow.shortLabel"))

              .addOption(TuttiConfigurationOption.COLOR_SELECTED_ROW)
              .setOptionShortLabel(t("tutti.config.option.ui.color.selectedRow.shortLabel"))

              .addOption(TuttiConfigurationOption.COLOR_ROW_INVALID)
              .setOptionShortLabel(t("tutti.config.option.ui.color.rowInvalid.shortLabel"))

              .addOption(TuttiConfigurationOption.COLOR_ROW_READ_ONLY)
              .setOptionShortLabel(t("tutti.config.option.ui.color.rowReadOnly.shortLabel"))

              .addOption(TuttiConfigurationOption.COLOR_CELL_WITH_VALUE)
              .setOptionShortLabel(t("tutti.config.option.ui.color.cellWithValue.shortLabel"))

              .addOption(TuttiConfigurationOption.COLOR_ROW_TO_CONFIRM)
              .setOptionShortLabel(t("tutti.config.option.ui.color.rowToConfirm.shortLabel"))

              .addOption(TuttiConfigurationOption.COLOR_WARNING_ROW)
              .setOptionShortLabel(t("tutti.config.option.ui.color.rowInWarning.shortLabel"))

              .addOption(TuttiConfigurationOption.COLOR_COMPUTED_WEIGHTS)
              .setOptionShortLabel(t("tutti.config.option.ui.color.computedWeights.shortLabel"))

              .addOption(TuttiConfigurationOption.COLOR_BLOCKING_LAYER)
              .setOptionShortLabel(t("tutti.config.option.ui.color.blockingLayer.shortLabel"))

              .addOption(TuttiConfigurationOption.COLOR_CATCH)
              .setOptionShortLabel(t("tutti.config.option.ui.color.catch.shortLabel"))

              .addOption(TuttiConfigurationOption.COLOR_SPECIES)
              .setOptionShortLabel(t("tutti.config.option.ui.color.species.shortLabel"))

              .addOption(TuttiConfigurationOption.COLOR_BENTHOS)
              .setOptionShortLabel(t("tutti.config.option.ui.color.benthos.shortLabel"))

              .addOption(TuttiConfigurationOption.COLOR_SPECIES_OR_BENTHOS_UNSORTED_COMPUTED_WEIGHT_IN_WARNING)
              .setOptionShortLabel(t("tutti.config.option.ui.color.speciesOrBenthosUnsortedComputedWeightInWarning.shortLabel"))

              .addOption(TuttiConfigurationOption.COLOR_MARINE_LITTER)
              .setOptionShortLabel(t("tutti.config.option.ui.color.marineLitter.shortLabel"))

              .addOption(TuttiConfigurationOption.COLOR_HIGHLIGHT_INFO_BACKGROUND)
              .setOptionShortLabel(t("tutti.config.option.ui.color.hightlightInfo.background.shortLabel"))

              .addOption(TuttiConfigurationOption.COLOR_HIGHLIGHT_INFO_FOREGROUND)
              .setOptionShortLabel(t("tutti.config.option.ui.color.hightlightInfo.foreground.shortLabel"))

              .addOption(TuttiConfigurationOption.DATE_FORMAT)
              .setOptionShortLabel(t("tutti.config.option.ui.dateFormat.shortLabel"))

              .addOption(TuttiConfigurationOption.COORDINATE_EDITOR_TYPE)
              .setOptionShortLabel(t("tutti.config.option.ui.coordinateEditorType.shortLabel"));

        // Weight Unit category

        WeightUnitTableCellRenderer weightUnitTableCellRenderer = new WeightUnitTableCellRenderer();

        JComboBox<WeightUnit> weightUnitEnumEditor = EnumEditor.newEditor(WeightUnit.class);
        weightUnitEnumEditor.setRenderer(new WeightUnitListCellRenderer());
        DefaultCellEditor weightUnitTableCellEditor = new DefaultCellEditor(weightUnitEnumEditor);
        helper.addCategory(n("tutti.config.category.weightUnit"),
                           n("tutti.config.category.weightUnit.description"),
                           CALLBACK_UI)
              .addOption(TuttiConfigurationOption.WEIGHT_UNIT_SPECIES)
              .setOptionShortLabel(t("tutti.config.option.weight.unit.species.shortLabel"))
              .setOptionRenderer(weightUnitTableCellRenderer)
              .setOptionEditor(weightUnitTableCellEditor)

              .addOption(TuttiConfigurationOption.WEIGHT_UNIT_BENTHOS)
              .setOptionShortLabel(t("tutti.config.option.weight.unit.benthos.shortLabel"))
              .setOptionRenderer(weightUnitTableCellRenderer)
              .setOptionEditor(weightUnitTableCellEditor)

              .addOption(TuttiConfigurationOption.WEIGHT_UNIT_MARINE_LITTER)
              .setOptionShortLabel(t("tutti.config.option.weight.unit.marineLitter.shortLabel"))
              .setOptionRenderer(weightUnitTableCellRenderer)
              .setOptionEditor(weightUnitTableCellEditor)

              .addOption(TuttiConfigurationOption.WEIGHT_UNIT_INDIVIDUAL_OBSERVATION)
              .setOptionShortLabel(t("tutti.config.option.weight.unit.individualObservation.shortLabel"))
              .setOptionRenderer(weightUnitTableCellRenderer)
              .setOptionEditor(weightUnitTableCellEditor)

              .addOption(TuttiConfigurationOption.WEIGHT_UNIT_ACCIDENTAL_CATCH)
              .setOptionShortLabel(t("tutti.config.option.weight.unit.accidentalCatch.shortLabel"))
              .setOptionRenderer(weightUnitTableCellRenderer)
              .setOptionEditor(weightUnitTableCellEditor);


        // external devices category

        helper.addCategory(n("tutti.config.category.externalDevices"),
                           n("tutti.config.category.externalDevices.description"))

              // ichtyometer

              .addOption(TuttiConfigurationOption.FULL_BLUETOOTH_SCAN)
              .setOptionShortLabel(t("tutti.config.option.ui.fullBluetoothScan.shortLabel"))

              .addOption(TuttiConfigurationOption.ICHTYOMETER_MAXIMUM_NUMBER_OF_ATTEMPT_TO_CONNECT)
              .setOptionShortLabel(t("tutti.config.option.ui.ichtyometerMaximumNumberOfAttemptToConnect.shortLabel"))

              // caliper

              .addOption(TuttiConfigurationOption.CALIPER_SERIAL_PORT)
              .setOptionShortLabel(t("tutti.config.option.ui.caliperSerialPort.shortLabel"))

              // all devices

              .addOption(TuttiConfigurationOption.EXTERNAL_DEVICES_VOICE_ENABLED)
              .setOptionShortLabel(t("tutti.config.option.ui.externalDevicesVoiceEnabled.shortLabel"))

              .addOption(TuttiConfigurationOption.EXTERNAL_DEVICES_READS_UNIT)
              .setOptionShortLabel(t("tutti.config.option.ui.externalDevicesReadsUnit.shortLabel"))

              .addOption(TuttiConfigurationOption.EXTERNAL_DEVICES_DATA_RECEPTION_BEEP_ENABLED)
              .setOptionShortLabel(t("tutti.config.option.ui.externalDevicesDataBeepEnabled.shortLabel"))

              .addOption(TuttiConfigurationOption.EXTERNAL_DEVICES_DATA_RECEPTION_BEEP_FREQUENCY)
              .setOptionShortLabel(t("tutti.config.option.ui.externalDevicesDataBeepFrequency.shortLabel"))

              .addOption(TuttiConfigurationOption.EXTERNAL_DEVICES_ERROR_RECEPTION_BEEP_ENABLED)
              .setOptionShortLabel(t("tutti.config.option.ui.externalDevicesErrorBeepEnabled.shortLabel"))

              .addOption(TuttiConfigurationOption.EXTERNAL_DEVICES_ERROR_RECEPTION_BEEP_FREQUENCY)
              .setOptionShortLabel(t("tutti.config.option.ui.externalDevicesErrorBeepFrequency.shortLabel"));

        // Technical category

        helper.addCategory(n("tutti.config.category.technical"),
                           n("tutti.config.category.technical.description"))
              .addOption(TuttiConfigurationOption.BASEDIR)
              .setOptionShortLabel(t("tutti.config.option.basedir.shortLabel"))

              .addOption(TuttiConfigurationOption.DATA_DIRECTORY)
              .setOptionShortLabel(t("tutti.config.option.data.directory.shortLabel"))

              .addOption(TuttiConfigurationOption.TMP_DIRECTORY)
              .setOptionShortLabel(t("tutti.config.option.tmp.directory.shortLabel"))

              .addOption(TuttiConfigurationOption.I18N_DIRECTORY)
              .setOptionShortLabel(t("tutti.config.option.i18n.directory.shortLabel"))

              .addOption(TuttiConfigurationOption.HELP_DIRECTORY)
              .setOptionShortLabel(t("tutti.config.option.help.directory.shortLabel"))

              .addOption(TuttiConfigurationOption.REPORT_DIRECTORY)
              .setOptionShortLabel(t("tutti.config.option.service.report.directory.shortLabel"))

              .addOption(TuttiConfigurationOption.REPORT_LOG_DIRECTORY)
              .setOptionShortLabel(t("tutti.config.option.service.report.log.directory.shortLabel"))

              .addOption(TuttiConfigurationOption.DB_DIRECTORY)
              .setOptionShortLabel(t("tutti.config.option.persistence.db.directory.shortLabel"))

              .addOption(TuttiConfigurationOption.DB_ATTACHMENT_DIRECTORY)
              .setOptionShortLabel(t("tutti.config.option.persistence.db.attachment.directory.shortLabel"))

              .addOption(TuttiConfigurationOption.DB_PROTOCOL_DIRECTORY)
              .setOptionShortLabel(t("tutti.config.option.persistence.db.protocol.directory.shortLabel"))

              .addOption(TuttiConfigurationOption.DB_CACHE_DIRECTORY)
              .setOptionShortLabel(t("tutti.config.option.persistence.db.cache.directory.shortLabel"))

              .addOption(TuttiConfigurationOption.JDBC_URL)
              .setOptionShortLabel(t("tutti.config.option.persistence.jdbc.url.shortLabel"))

              .addOption(TuttiConfigurationOption.SITE_URL)
              .setOptionShortLabel(t("tutti.config.option.site.url.shortLabel"))

              .addOption(TuttiConfigurationOption.DB_BACKUP_DIRECTORY)
              .setOptionShortLabel(t("tutti.config.option.persistence.db.backup.directory.shortLabel"))

              .addOption(TuttiConfigurationOption.REPORT_BACKUP_DIRECTORY)
              .setOptionShortLabel(t("tutti.config.option.service.report.backup.directory.shortLabel"))

              .addOption(TuttiConfigurationOption.EXPORT_BACKUP_DIRECTORY)
              .setOptionShortLabel(t("tutti.config.option.service.export.backup.directory.shortLabel"))

              .addOption(TuttiConfigurationOption.GENERIC_FORMAT_REPORT_BACKUP_DIRECTORY)
              .setOptionShortLabel(t("tutti.config.option.service.genericFormat.export.backup.directory.shortLabel"))

              .addOption(TuttiConfigurationOption.UPDATE_APPLICATION_URL)
              .setOptionShortLabel(t("tutti.config.option.update.application.url.shortLabel"))

              .addOption(TuttiConfigurationOption.UPDATE_DATA_URL)
              .setOptionShortLabel(t("tutti.config.option.update.data.url.shortLabel"));

        helper.setFinalizer(new MainCallBackFinalizer(CALLBACK_APPLICATION));

        helper.setCloseAction(() -> getContext().getActionEngine().runInternalAction(TuttiConfigUIHandler.this, GoToPreviousScreenAction.class));
        ConfigUI configUI = helper.buildUI(getUI(), n("tutti.config.category.applications"));

        configUI.getHandler().setTopContainer(getUI());

        // Forbids to sort rows in categories ui table
        // See https://forge.codelutin.com/issues/6947
        // FIXME Move this in jaxx helper
        int tabCount = configUI.getCategories().getTabCount();
        for (int i = 0; i < tabCount; i++) {

            ConfigCategoryUI configCategoryUI = (ConfigCategoryUI) configUI.getCategories().getComponentAt(i);
            JXTable table = configCategoryUI.getTable();
            table.setRowSorter(null);

            //FIXME tc ceci pour pouvoir éditer des int à déplacer dans jaxx
            TableCellEditor defaultEditor = table.getDefaultEditor(Integer.class);
            Objects.requireNonNull(defaultEditor);
            table.setDefaultEditor(int.class, defaultEditor);

        }
        getUI().add(configUI, BorderLayout.CENTER);
    }

    @Override
    protected JComponent getComponentToFocus() {
        return getUI();
    }

    @Override
    public void onCloseUI() {
        if (log.isDebugEnabled()) {
            log.debug("closing: " + ui);
        }
    }

    protected void reloadApplication() {
        ReloadTuttiAction action = getContext().getActionFactory().createLogicAction(this, ReloadTuttiAction.class);
        getContext().getActionEngine().runAction(action);
    }

    protected void reloadUI() {
        getContext().getMainUI().getHandler().reloadUI();
    }

    protected void reloadShortcuts() {
        getContext().getMainUI().getHandler().reloadUI();
    }

    private static class WeightUnitListCellRenderer implements ListCellRenderer<WeightUnit> {

        private final DefaultListCellRenderer delegate;

        private WeightUnitListCellRenderer() {
            this.delegate = new DefaultListCellRenderer();
        }

        @Override
        public Component getListCellRendererComponent(JList<? extends WeightUnit> list, WeightUnit value, int index, boolean isSelected, boolean cellHasFocus) {

            Object value2 = value;
            if (value != null) {
                value2 = value.getShortLabel();
            }
            return delegate.getListCellRendererComponent(list, value2, index, isSelected, cellHasFocus);
        }
    }

    private static class WeightUnitTableCellRenderer extends DefaultTableCellRenderer {

        private static final long serialVersionUID = 1L;

        @Override
        public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {

            if (value != null) {

                WeightUnit unit = (WeightUnit) value;
                value = unit.getShortLabel();
            }
            return super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
        }
    }

}

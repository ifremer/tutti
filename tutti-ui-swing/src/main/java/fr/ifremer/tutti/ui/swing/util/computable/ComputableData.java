package fr.ifremer.tutti.ui.swing.util.computable;

/*
 * #%L
 * Tutti :: UI
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.util.Numbers;
import org.jdesktop.beans.AbstractSerializableBean;
import org.nuiton.jaxx.application.bean.JavaBeanObject;
import org.nuiton.jaxx.application.bean.JavaBeanObjectPropagateChangeListener;


/**
 * A number data plus a possible computed value.
 *
 * @author Kevin Morin - kmorin@codelutin.com
 * @since 1.0
 */
public class ComputableData<N extends Number> extends AbstractSerializableBean implements JavaBeanObject {

    public static final String PROPERTY_DATA = "data";

    public static final String PROPERTY_COMPUTED_DATA = "computedData";

    private static final long serialVersionUID = 1L;

    protected N data;

    protected N computedData;

    public ComputableData() {
        data = null;
        computedData = null;
    }

    public ComputableData(N data, N computedData) {
        this.data = data;
        this.computedData = computedData;
    }

    public N getData() {
        return data;
    }

    public void setData(N data) {
        Object oldValue = getData();
        this.data = data;
        firePropertyChange(PROPERTY_DATA, oldValue, data);
    }

    public N getComputedData() {
        return computedData;
    }

    public void setComputedData(N computedData) {
        Object oldValue = getComputedData();
        this.computedData = computedData;
        firePropertyChange(PROPERTY_COMPUTED_DATA, oldValue, computedData);
    }

    public N getDataOrComputedData() {
        return Numbers.getValueOrComputedValue(data, computedData);
    }

    @Override
    public String toString() {
        String result = null;
        N dataOrComputedData = getDataOrComputedData();
        if (dataOrComputedData != null) {
            result = dataOrComputedData.toString();
        }
        return result;
    }

    /**
     * Add a listener to propagate the modification of the
     * {@link #PROPERTY_DATA} property to a given {@code propertyName}.
     *
     * @param propertyName name of the property to fire on given bean
     * @param otherBean    bean that will fires
     * @since 1.2
     */
    public void addPropagateListener(String propertyName,
                                     JavaBeanObject otherBean) {

        JavaBeanObjectPropagateChangeListener.listenAndPropagate(this, otherBean, PROPERTY_DATA, propertyName);
    }

    @Override
    public void firePropertyChanged(String propertyName, Object oldValue, Object newValue) {
        super.firePropertyChange(propertyName, oldValue, newValue);
    }

}

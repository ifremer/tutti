package fr.ifremer.tutti.ui.swing.content.home.actions;

/*
 * #%L
 * Tutti :: UI
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import fr.ifremer.tutti.service.ValidationService;
import fr.ifremer.tutti.ui.swing.TuttiScreen;
import fr.ifremer.tutti.ui.swing.content.actions.AbstractChangeScreenAction;
import fr.ifremer.tutti.ui.swing.content.MainUIHandler;
import fr.ifremer.tutti.ui.swing.content.validation.ValidateCruiseUI;
import fr.ifremer.tutti.ui.swing.content.validation.ValidateCruiseUIModel;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Opens the operations validation screen.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.0
 */
public class ValidateCatchesAction extends AbstractChangeScreenAction {

    /** Logger. */
    private static final Log log =
            LogFactory.getLog(ValidateCatchesAction.class);

    public ValidateCatchesAction(MainUIHandler handler) {
        super(handler, true, TuttiScreen.VALIDATE_CRUISE);
    }

    @Override
    public void doAction() throws Exception {
        Preconditions.checkState(getContext().isProgramFilled());
        Preconditions.checkState(getContext().isCruiseFilled());
        getContext().setValidationContext(ValidationService.VALIDATION_CONTEXT_VALIDATE);
        if (log.isInfoEnabled()) {
            log.info("Validate operations of cruise: " + getContext().getCruiseId());
        }
        super.doAction();
    }

    @Override
    public void postSuccessAction() {
        super.postSuccessAction();

        ValidateCruiseUI currentBody = (ValidateCruiseUI) handler.getCurrentBody();
        ValidateCruiseUIModel model = currentBody.getModel();

    }
}

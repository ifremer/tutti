package fr.ifremer.tutti.ui.swing.content.operation.catches.species.frequency.actions;

/*
 * #%L
 * Tutti :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.ui.swing.content.operation.catches.species.frequency.IndividualObservationBatchRowModel;
import fr.ifremer.tutti.ui.swing.content.operation.catches.species.frequency.SpeciesFrequencyUI;
import fr.ifremer.tutti.ui.swing.content.operation.catches.species.frequency.SpeciesFrequencyUIHandler;
import fr.ifremer.tutti.ui.swing.content.operation.catches.species.frequency.SpeciesFrequencyUIModel;
import fr.ifremer.tutti.ui.swing.util.actions.LongActionSupport;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.List;

/**
 * Created on 1/1/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.11
 */
public class CancelEditSpeciesFrequencyAction extends LongActionSupport<SpeciesFrequencyUIModel, SpeciesFrequencyUI, SpeciesFrequencyUIHandler> {

    /** Logger. */
    private static final Log log = LogFactory.getLog(CancelEditSpeciesFrequencyAction.class);

    public CancelEditSpeciesFrequencyAction(SpeciesFrequencyUIHandler handler) {
        super(handler, false);
    }

    @Override
    public boolean prepareAction() throws Exception {

        boolean doAction = super.prepareAction();

        if (doAction && getModel().isModify()) {

            // Ask confirmation to quit screen
            doAction = getHandler().askCancelEditBeforeLeaving();
        }
        return doAction;

    }

    @Override
    public void doAction() {

        if (log.isDebugEnabled()) {
            log.debug("Cancel UI " + getUI());
        }

        SpeciesFrequencyUIModel model = getModel();

        // remove indivudal observations from cache
        List<IndividualObservationBatchRowModel> individualObservations = model.getIndividualObservationModel().getRows();
        model.getIndividualObservationUICache().removeIndividualObservations(individualObservations);

        // push back old individual observations in cache
        List<IndividualObservationBatchRowModel> oldIndividualObservations = model.getBatch().getIndividualObservation();
        model.addIndividualObservationsInCache(oldIndividualObservations);

        // close dialog
        getHandler().onCloseUI();
    }
}
package fr.ifremer.tutti.ui.swing;

/*
 * #%L
 * Tutti :: UI
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.entities.data.SampleCategoryModel;

/**
 * Enumeration of any internal screen of application.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 0.1
 */
public enum TuttiScreen {

    /**
     * To display config.
     *
     * @since 1.0
     */
    CONFIG,
    /**
     * To edit {@link SampleCategoryModel}.
     *
     * @since 2.4
     */
    EDIT_SAMPLE_CATEGORY_MODEL,
    /**
     * To manage db used by Tutti.
     *
     * @since 1.0
     */
    MANAGE_DB,

    /**
     * To select program and cruise.
     *
     * @since 0.1
     */
    SELECT_CRUISE,

    /**
     * To edit a program.
     *
     * @since 0.1
     */
    EDIT_PROGRAM,

    /**
     * To edit a cruise.
     *
     * @since 0.1
     */
    EDIT_CRUISE,

    /**
     * To edit a protocol.
     *
     * @since 0.1
     */
    EDIT_PROTOCOL,

    /**
     * To open import temporary referential.
     *
     * @since 1.0
     */
    IMPORT_TEMPORARY_REFERENTIAL,
    /**
     * To fill operations.
     *
     * @since 0.1
     */
    EDIT_FISHING_OPERATION,

    /**
     * To validate the cruise.
     *
     * @since 1.4
     */
    VALIDATE_CRUISE,
    /**
     * To run reports.
     *
     * @since 2.9
     */
    REPORT,
    /**
     * To perform a generic format import.
     *
     * @since 3.14
     */
    GENERIC_FORMAT_IMPORT,
    /**
     * To perform a generic format export.
     *
     * @since 3.14.3
     */
    GENERIC_FORMAT_EXPORT,
}

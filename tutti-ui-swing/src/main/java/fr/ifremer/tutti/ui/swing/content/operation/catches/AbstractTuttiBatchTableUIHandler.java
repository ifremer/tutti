package fr.ifremer.tutti.ui.swing.content.operation.catches;

/*
 * #%L
 * Tutti :: UI
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.entities.data.FishingOperation;
import fr.ifremer.tutti.service.DecoratorService;
import fr.ifremer.tutti.ui.swing.util.AbstractTuttiBeanUIModel;
import fr.ifremer.tutti.ui.swing.util.TuttiUI;
import fr.ifremer.tutti.ui.swing.util.TuttiUIUtil;
import fr.ifremer.tutti.ui.swing.util.table.AbstractTuttiTableUIHandler;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.jdesktop.swingx.JXTable;
import org.jdesktop.swingx.decorator.HighlightPredicate;
import org.jdesktop.swingx.decorator.Highlighter;
import org.jdesktop.swingx.sort.TableSortController;
import org.jdesktop.swingx.table.TableColumnModelExt;
import org.nuiton.decorator.Decorator;
import org.nuiton.jaxx.application.swing.table.AbstractApplicationTableModel;
import org.nuiton.jaxx.application.swing.table.ColumnIdentifier;

import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableModel;
import java.awt.Color;
import java.awt.Component;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Abstract ui handler forbatch ui.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 0.2
 */
public abstract class AbstractTuttiBatchTableUIHandler<R extends AbstractTuttiBeanUIModel, M extends AbstractTuttiBatchUIModel<R, M>, T extends AbstractApplicationTableModel<R>, UI extends TuttiUI<M, ?>> extends AbstractTuttiTableUIHandler<R, M, UI> {

    protected abstract ColumnIdentifier<R> getCommentIdentifier();

    protected abstract ColumnIdentifier<R> getAttachementIdentifier();

    public abstract void selectFishingOperation(FishingOperation bean);

    protected final List<BatchSavedListener> batchSavedListeners = new ArrayList<>();

    protected AbstractTuttiBatchTableUIHandler(String... properties) {
        super(properties);
    }

    /**
     * To clear the table selection.
     *
     * This method is invoked before changing the fishing operation.
     *
     * @since 0.3
     */
    public final void clearTableSelection() {

        JXTable table = getTable();

        if (table.isEditing()) {

            // make sure to stop editor
            table.editingCanceled(null);
        }

        // make sure selection is empty (will remove bean from monitor)
        table.clearSelection();
    }

    public void addBatchSavedListener(BatchSavedListener listener) {
        batchSavedListeners.add(listener);
    }

    public void removeBatchSavedListener(BatchSavedListener listener) {
        batchSavedListeners.remove(listener);
    }

    public void fireBatchSaved(R row) {
        for (BatchSavedListener listener : batchSavedListeners) {
            listener.onBatchSaved(new BatchSavedEvent(getModel(), row));
        }
    }

    @Override
    protected void onRowModified(int rowIndex,
                                       R row,
                                       String propertyName,
                                       Object oldValue,
                                       Object newValue) {
        recomputeRowValidState(row);

        saveSelectedRowIfNeeded();
    }

    protected void initBatchTable(JXTable table,
                                  TableColumnModelExt columnModel,
                                  T tableModel) {

        installTableKeyListener(columnModel, table);

        TableSortController<TableModel> sorter = new TableSortController<>(tableModel);
        sorter.setSortable(false);
        table.setRowSorter(sorter);

        initTable(table);
    }

    @Override
    protected void addHighlighters(final JXTable table) {
        super.addHighlighters(table);
        addCommentHighlighter(table, getCommentIdentifier());
        addAttachementHighlighter(table, getAttachementIdentifier());
    }

    protected void addCommentHighlighter(JXTable table, ColumnIdentifier identifier) {
        Color cellWithValueColor = getConfig().getColorCellWithValue();

        Highlighter commentHighlighter = TuttiUIUtil.newBackgroundColorHighlighter(
                new HighlightPredicate.AndHighlightPredicate(
                        new HighlightPredicate.IdentifierHighlightPredicate(identifier),
                        // for not null value
                        (renderer, adapter) -> {
                            String value = (String) adapter.getValue();
                            return StringUtils.isNotBlank(value);
                        }), cellWithValueColor);
        table.addHighlighter(commentHighlighter);
    }

    protected void addAttachementHighlighter(JXTable table, ColumnIdentifier identifier) {
        Color cellWithValueColor = getConfig().getColorCellWithValue();

        Highlighter attachmentHighlighter = TuttiUIUtil.newBackgroundColorHighlighter(
                new HighlightPredicate.AndHighlightPredicate(
                        new HighlightPredicate.IdentifierHighlightPredicate(identifier),
                        // for not null value
                        (renderer, adapter) -> {
                            Collection attachments = (Collection) adapter.getValue();
                            return CollectionUtils.isNotEmpty(attachments);
                        }
                ), cellWithValueColor);
        table.addHighlighter(attachmentHighlighter);
    }

    protected void addIdColumnToModel(TableColumnModel model,
                                      ColumnIdentifier<R> identifier,
                                      JTable table) {

        final TableCellRenderer defaultRenderer = table.getDefaultRenderer(Number.class);
        final Decorator<String> idDecorator = getDecorator(String.class, DecoratorService.SPACE_EVERY_3_DIGIT);

        TableCellRenderer idTableCellRenderer = (table1, value, isSelected, hasFocus, row, column) -> {

            String text = null;
            if (value != null) {
                text = idDecorator.toString(value);
            }

            Component result = defaultRenderer.getTableCellRendererComponent(table1, text, isSelected, hasFocus, row, column);
            if (result instanceof JLabel) {
                JLabel jLabel = (JLabel) result;
                jLabel.setHorizontalTextPosition(SwingConstants.RIGHT);

            }
            return result;
        };

        addColumnToModel(model, null, idTableCellRenderer, identifier);

    }
}

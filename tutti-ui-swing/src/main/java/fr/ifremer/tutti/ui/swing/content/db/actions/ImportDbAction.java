package fr.ifremer.tutti.ui.swing.content.db.actions;

/*
 * #%L
 * Tutti :: UI
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import fr.ifremer.tutti.persistence.ProgressionModel;
import fr.ifremer.tutti.persistence.TuttiPersistence;
import fr.ifremer.tutti.persistence.service.UpdateSchemaContextSupport;
import fr.ifremer.tutti.service.PersistenceService;
import fr.ifremer.tutti.ui.swing.content.actions.AbstractMainUITuttiAction;
import fr.ifremer.tutti.ui.swing.content.MainUIHandler;
import fr.ifremer.tutti.ui.swing.content.actions.OpenHomeScreenAction;
import fr.ifremer.tutti.ui.swing.util.AbstractTuttiUIHandler;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.jaxx.application.ApplicationBusinessException;
import org.nuiton.version.Version;

import javax.swing.JOptionPane;
import java.io.File;

import static org.nuiton.i18n.I18n.t;

/**
 * To import a db and use it.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.1
 */
public class ImportDbAction extends AbstractMainUITuttiAction {

    /** Logger. */
    private static final Log log = LogFactory.getLog(ImportDbAction.class);

    protected File importFile;

    protected String jdbcUrl;

    protected PersistenceService.ImportStructureType importStructureType;

    private UpdateSchemaContext updateSchemaContext;

    public ImportDbAction(MainUIHandler handler) {
        super(handler, true);
        setActionDescription(t("tutti.dbManager.action.importDb.tip"));
    }

    public void setImportFile(File importFile) {
        this.importFile = importFile;
    }


    public class UpdateSchemaContext extends UpdateSchemaContextSupport {

        private boolean closeDb;

        @Override
        public void init(Version schemaVersion, Version schemaVersionIfUpdate) {

            super.init(schemaVersion, schemaVersionIfUpdate);

            if (isSchemaVersionTooHigh()) {

                // database schema version is higher than application one
                String message = t("tutti.dbManager.action.upgradeDb.schema.too.high", schemaVersion, schemaVersionIfUpdate);

                String htmlMessage = String.format(
                        AbstractTuttiUIHandler.CONFIRMATION_FORMAT,
                        message,
                        t("tutti.dbManager.action.upgradeDb.schema.too.high.help"));
                int i = JOptionPane.showConfirmDialog(
                        handler.getTopestUI(),
                        htmlMessage,
                        t("tutti.dbManager.title.schema.toupdate"),
                        JOptionPane.OK_CANCEL_OPTION,
                        JOptionPane.QUESTION_MESSAGE);
                boolean continueAction = i == JOptionPane.OK_OPTION;

                if (!continueAction) {

                    //close db
                    closeDb = true;
                }
            }
        }

        @Override
        protected boolean askUserToMigrate(Version schemaVersion, Version schemaVersionIfUpdate) {

            // database schema need to migrate
            // ask user to migrate or not (if not will close db)

            String message = t("tutti.dbManager.action.upgradeDb.schema.to.update.message", schemaVersion, schemaVersionIfUpdate);

            String htmlMessage = String.format(
                    AbstractTuttiUIHandler.CONFIRMATION_FORMAT,
                    message,
                    t("tutti.dbManager.action.upgradeDb.schema.to.update.message.help"));
            int i = JOptionPane.showConfirmDialog(
                    handler.getTopestUI(),
                    htmlMessage,
                    t("tutti.dbManager.title.schema.toupdate"),
                    JOptionPane.OK_CANCEL_OPTION,
                    JOptionPane.QUESTION_MESSAGE);
            boolean continueAction = i == JOptionPane.OK_OPTION;

            if (continueAction) {

                // will migrate
                continueAction = true;
            } else {

                //close db
                closeDb = true;
            }


            return continueAction;

        }

        public boolean isCloseDb() {
            return closeDb;
        }

    }

    @Override
    public boolean prepareAction() throws Exception {

        importStructureType = null;
        jdbcUrl = null;
        updateSchemaContext = null;

        boolean doAction = super.prepareAction();

        if (doAction) {

            jdbcUrl = getConfig().getJdbcUrl();

            if (importFile == null) {

                // choose file to import
                importFile = chooseFile(
                        t("tutti.dbManager.title.choose.dbImportFile"),
                        t("tutti.dbManager.action.chooseDbFile"),
                        "^.*\\.zip", t("tutti.common.file.zip")
                );

                if (importFile == null) {

                    displayWarningMessage(
                            t("tutti.dbManager.title.choose.dbImportFile"),
                            t("tutti.dbManager.action.importdb.no.import.file.choosen")
                    );

                    doAction = false;
                }
            }

            if (doAction) {

                ProgressionModel progressionModel = new ProgressionModel();
                progressionModel.setTotal(3);
                setProgressionModel(progressionModel);

                importStructureType =
                        getContext().getPersistenceService().checkImportStructure(importFile);
            }
        }
        return doAction;
    }

    @Override
    public void releaseAction() {
        importFile = null;
        super.releaseAction();
    }

    @Override
    public void doAction() throws Exception {
        Preconditions.checkNotNull(importFile);

        if (log.isInfoEnabled()) {
            log.info("Will import db: " + importFile);
        }

        ProgressionModel progressionModel = getProgressionModel();

        // ------------------------------------------------------------------ //
        // --- import db                                                      //
        // ------------------------------------------------------------------ //

        progressionModel.increments(t("tutti.importDb.step.unzipArchive"));

        getContext().getPersistenceService().importDb(importStructureType, importFile);

        // ------------------------------------------------------------------ //
        // --- open db                                                        //
        // ------------------------------------------------------------------ //

        progressionModel.increments(t("tutti.importDb.step.openDb", jdbcUrl));
        try {
            getContext().setDbExist(true);
            getContext().openPersistenceService();
        } catch (Exception e) {

            if (log.isErrorEnabled()) {
                log.error("Could not open db", e);
            }
            // no more db
            getContext().closePersistenceService();

            // could not load db
            throw new ApplicationBusinessException(t("tutti.dbManager.action.importdb.couldNotOpen"), e);
        }

        // ------------------------------------------------------------------ //
        // --- check schema version                                           //
        // ------------------------------------------------------------------ //

        progressionModel.increments(t("tutti.importDb.step.checkSchemaVersion"));

        TuttiPersistence persistenceService = getContext().getPersistenceService();

        updateSchemaContext = new UpdateSchemaContext();
        persistenceService.prepareUpdateSchemaContext(updateSchemaContext);

        Version schemaVersion = updateSchemaContext.getSchemaVersion();

        if (log.isInfoEnabled()) {
            log.info("Detected database version: " + schemaVersion);
        }
        Version schemaVersionIfUpdate = updateSchemaContext.getSchemaVersionIfUpdate();

        if (log.isInfoEnabled()) {
            log.info("Detected schema application version:" + schemaVersionIfUpdate);
        }


        if (updateSchemaContext.isCloseDb()) {

            // ------------------------------------------------------------------ //
            // --- close db                                                       //
            // ------------------------------------------------------------------ //

            progressionModel.increments(t("tutti.importDb.step.closeDb"));
            getActionEngine().runInternalAction(handler, CloseDbAction.class);

            return;
        }

        if (updateSchemaContext.isWillUpdate()) {

            // need to migrate schema
            progressionModel.adaptTotal(progressionModel.getTotal() + 2);

            // ------------------------------------------------------------------ //
            // --- update schema                                                  //
            // ------------------------------------------------------------------ //

            String message = t("tutti.importDb.step.will.migrateSchema", schemaVersion, schemaVersionIfUpdate);

            progressionModel.increments(message);
            sendMessage(message);
            getContext().getPersistenceService().updateSchema();

            sendMessage(t("tutti.flash.info.db.schema.updated", schemaVersion, schemaVersionIfUpdate));
        }

        // ------------------------------------------------------------------ //
        // --- check db context                                               //
        // ------------------------------------------------------------------ //

        String message = t("tutti.importDb.step.check.dbContext", schemaVersion, schemaVersionIfUpdate);

        progressionModel.increments(message);

        if (log.isDebugEnabled()) {
            log.debug("Check db context");
        }
        getContext().checkDbContext();

        // ------------------------------------------------------------------ //
        // --- change screen                                                  //
        // ------------------------------------------------------------------ //

        getActionEngine().runInternalAction(handler, OpenHomeScreenAction.class);
    }

    @Override
    public void postSuccessAction() {
        handler.reloadDbManagerText();

        super.postSuccessAction();

        if (updateSchemaContext.isCloseDb()) {
            sendMessage(t("tutti.flash.info.db.imported.but.closed", jdbcUrl));
        } else {
            sendMessage(t("tutti.flash.info.db.imported", jdbcUrl));
        }

        // make sure title is reloaded
        getUI().getHandler().changeTitle();
    }

    @Override
    public void postFailedAction(Throwable error) {
        handler.reloadDbManagerText();
        super.postFailedAction(error);
    }

}

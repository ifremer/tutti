package fr.ifremer.tutti.ui.swing.content.home;

/*
 * #%L
 * Tutti :: UI
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Lists;
import fr.ifremer.tutti.persistence.entities.data.Cruise;
import fr.ifremer.tutti.persistence.entities.data.Program;
import fr.ifremer.tutti.persistence.entities.protocol.TuttiProtocol;
import fr.ifremer.tutti.service.PersistenceService;
import fr.ifremer.tutti.ui.swing.util.AbstractTuttiUIHandler;
import jaxx.runtime.swing.editor.bean.BeanFilterableComboBox;
import jaxx.runtime.validator.swing.SwingValidator;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.swing.JComboBox;
import javax.swing.JComponent;
import java.awt.Font;
import java.beans.PropertyChangeListener;
import java.util.List;

/**
 * Main ui content to select cruise.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 0.1
 */
public class SelectCruiseUIHandler extends AbstractTuttiUIHandler<SelectCruiseUIModel, SelectCruiseUI> {

    /** Logger. */
    private static final Log log =
            LogFactory.getLog(SelectCruiseUIHandler.class);

    @Override
    public SwingValidator<SelectCruiseUIModel> getValidator() {
        return ui.getValidator();
    }

    @Override
    public void beforeInit(SelectCruiseUI ui) {
        super.beforeInit(ui);

        PersistenceService persistenceService = getContext().getPersistenceService();

        SelectCruiseUIModel model = new SelectCruiseUIModel();

        List<Program> programs = Lists.newArrayList(persistenceService.getAllProgram());
        model.setPrograms(programs);

        Program selectedProgram = null;

        if (programs.isEmpty()) {
            // do nothing

            if (log.isDebugEnabled()) {
                log.debug("No program found.");
            }
        } else {

            // get selected program (if any)


            List<Cruise> cruises = null;
            if (getContext().isProgramFilled()) {
                selectedProgram = getDataContext().getProgram();
                //TODO check selectprogram is not null
                cruises = Lists.newArrayList(persistenceService.getAllCruise(selectedProgram.getId()));
            }

            model.setProgram(selectedProgram);
            model.setCruises(cruises);

            if (CollectionUtils.isEmpty(cruises)) {

                // nothing to select
            } else {

                Cruise selectedCruise = null;
                if (getContext().isCruiseFilled()) {

                    // always reload the cruise to be sure synchronizationStatus is ok
                    selectedCruise = getDataContext().reloadCruise();
                }
                model.setCruise(selectedCruise);
            }
        }

        List<TuttiProtocol> protocols =
                Lists.newArrayList(persistenceService.getAllProtocol(selectedProgram == null ? null : selectedProgram.getId()));
        model.setProtocols(protocols);

        if (protocols.isEmpty()) {
            // do nothing

            if (log.isDebugEnabled()) {
                log.debug("No protocol found.");
            }
        } else {

            model.setProtocol(getDataContext().getProtocol());
        }
        ui.setContextValue(model);
    }

    @Override
    public void afterInit(SelectCruiseUI ui) {

        initUI(ui);

        Font font = ui.getEditCatchesButton().getFont();
        ui.getEditCatchesButton().setFont(font.deriveFont(Font.BOLD, 14));
        ui.getValidateCatchesButton().setFont(font.deriveFont(Font.BOLD, 14));

        SelectCruiseUIModel model = getModel();

        initBeanFilterableComboBox(ui.getProgramCombobox(), model.getPrograms(), model.getProgram());

        initBeanFilterableComboBox(ui.getCruiseCombobox(), model.getCruises(), model.getCruise());

        initBeanFilterableComboBox(ui.getProtocolCombobox(), model.getProtocols(), model.getProtocol());

        model.addPropertyChangeListener(SelectCruiseUIModel.PROPERTY_PROTOCOLS, evt -> {
            // reload combo box
            BeanFilterableComboBox<TuttiProtocol> combobox = SelectCruiseUIHandler.this.ui.getProtocolCombobox();
            List<TuttiProtocol> protocols = (List<TuttiProtocol>) evt.getNewValue();

            combobox.setData(null);
            if (protocols != null) {
                combobox.setData(protocols);
            }

            SelectCruiseUIHandler.this.ui.applyDataBinding(SelectCruiseUI.BINDING_PROTOCOL_COMBOBOX_ENABLED);
        });

        model.addPropertyChangeListener(SelectCruiseUIModel.PROPERTY_PROGRAM, evt -> {
            Program newValue = (Program) evt.getNewValue();
            boolean noProgram = newValue == null;
            getContext().setProgramId(noProgram ? null : newValue.getId());
            if (log.isInfoEnabled()) {
                log.info("Selected program: " + newValue);
            }
            List<Cruise> cruises;
            List<TuttiProtocol> protocols;
            if (noProgram) {
                cruises = Lists.newArrayList();
                protocols = Lists.newArrayList(getPersistenceService().getAllProtocol(null));

            } else {
                cruises = Lists.newArrayList(getPersistenceService().getAllCruise(newValue.getId()));
                protocols = Lists.newArrayList(getPersistenceService().getAllProtocol(newValue.getId()));
            }
            SelectCruiseUIModel source = (SelectCruiseUIModel) evt.getSource();
            source.setCruises(cruises);
            source.setCruise(null);
            source.setProtocols(protocols);
            source.setProtocol(null);
        });

        model.addPropertyChangeListener(SelectCruiseUIModel.PROPERTY_CRUISES, evt -> {
            // reload combo box
            BeanFilterableComboBox<Cruise> combobox = SelectCruiseUIHandler.this.ui.getCruiseCombobox();
            List<Cruise> campaigns = (List<Cruise>) evt.getNewValue();
            combobox.setData(null);
            if (campaigns != null) {
                combobox.setData(campaigns);
            }
        });

        model.addPropertyChangeListener(SelectCruiseUIModel.PROPERTY_CRUISE, evt -> {
            Cruise newValue = (Cruise) evt.getNewValue();
            getContext().setCruiseId(newValue == null ? null : newValue.getIdAsInt());
        });

        model.addPropertyChangeListener(SelectCruiseUIModel.PROPERTY_PROTOCOLS, evt -> {
            // reload combo box
            BeanFilterableComboBox<TuttiProtocol> combobox = SelectCruiseUIHandler.this.ui.getProtocolCombobox();
            List<TuttiProtocol> protocols = (List<TuttiProtocol>) evt.getNewValue();
            combobox.setData(null);
            if (protocols != null) {
                combobox.setData(protocols);
            }
        });

        model.addPropertyChangeListener(SelectCruiseUIModel.PROPERTY_PROTOCOL, evt -> {
            TuttiProtocol newValue = (TuttiProtocol) evt.getNewValue();
            getContext().setProtocolId(newValue == null ? null : newValue.getId());

            JComboBox editProtocolComboBox = SelectCruiseUIHandler.this.ui.getEditProtocolComboBox();

            // disable the actions on the combo during model modifications
            editProtocolComboBox.putClientProperty(CAN_EDIT, false);

            try {

                if (model.isProgramFound()) {

                    // can edit protocol

                    if (editProtocolComboBox.getItemCount() == 2) {

                        editProtocolComboBox.removeAllItems();
                        editProtocolComboBox.addItem(getUI().getEditProtocolButton());
                        editProtocolComboBox.addItem(getUI().getCloneProtocolButton());
                        editProtocolComboBox.addItem(getUI().getExportProtocolButton());
                        editProtocolComboBox.addItem(getUI().getDeleteProtocolButton());

                    }

                } else {

                    // can just export and delete protocol
                    if (editProtocolComboBox.getItemCount() == 4) {

                        editProtocolComboBox.removeItem(getUI().getEditProtocolButton());
                        editProtocolComboBox.removeItem(getUI().getCloneProtocolButton());

                    }

                }

            } finally {

                //reenable the combo actions
                editProtocolComboBox.putClientProperty(CAN_EDIT, true);
            }

        });

        registerValidators(getValidator());

        listenValidatorValid(getValidator(), model);

        getValidator().setBean(model);

        ui.applyDataBinding(SelectCruiseUI.BINDING_NEW_CRUISE_BUTTON_ENABLED);
        ui.applyDataBinding(SelectCruiseUI.BINDING_EDIT_CATCHES_BUTTON_ENABLED);
        ui.applyDataBinding(SelectCruiseUI.BINDING_VALIDATE_CATCHES_BUTTON_ENABLED);
    }

    @Override
    protected JComponent getComponentToFocus() {
        return getUI().getEditCatchesButton();
    }

    @Override
    public void onCloseUI() {
        if (log.isDebugEnabled()) {
            log.debug("closing: " + ui);
        }
        PropertyChangeListener[] listeners = getModel().getPropertyChangeListeners();
        for (PropertyChangeListener listener : listeners) {
            getModel().removePropertyChangeListener(listener);
        }
        clearValidators();
    }

}

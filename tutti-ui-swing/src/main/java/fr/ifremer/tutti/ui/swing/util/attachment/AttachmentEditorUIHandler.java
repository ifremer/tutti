package fr.ifremer.tutti.ui.swing.util.attachment;

/*
 * #%L
 * Tutti :: UI
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.entities.data.Attachment;
import fr.ifremer.tutti.ui.swing.TuttiUIContext;
import fr.ifremer.tutti.ui.swing.util.AbstractTuttiUIHandler;
import fr.ifremer.tutti.ui.swing.util.attachment.actions.HideAttachmentUIAction;
import fr.ifremer.tutti.ui.swing.util.attachment.actions.ShowAttachmentUIAction;
import jaxx.runtime.swing.ComponentMover;
import jaxx.runtime.swing.ComponentResizer;
import jaxx.runtime.validator.swing.SwingValidator;

import javax.swing.Action;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JRootPane;
import javax.swing.JToolBar;
import javax.swing.KeyStroke;
import java.awt.Component;
import java.awt.event.KeyEvent;
import java.util.List;

/**
 * @author Kevin Morin - kmorin@codelutin.com
 * @author Tony Chemit - chemit@codelutin.com
 * @since 0.2
 */
public class AttachmentEditorUIHandler extends AbstractTuttiUIHandler<TuttiUIContext, AttachmentEditorUI> {

    public static final String CLOSE_DIALOG_ACTION = "closeDialog";

    public static final String SHOW_DIALOG_ACTION = "showDialog";

    @Override
    public void beforeInit(AttachmentEditorUI ui) {
        super.beforeInit(ui);
        ui.setContextValue(TuttiUIContext.getApplicationContext());

    }

    @Override
    public void afterInit(AttachmentEditorUI ui) {

//        super.initUI(ui);

        initButton(ui.getAddButton());

        ui.getFile().setDialogOwner(ui);
        ui.pack();
        ui.setResizable(true);

        ComponentResizer cr = new ComponentResizer();
        cr.registerComponent(ui);
        ComponentMover cm = new ComponentMover();
        cm.setDragInsets(cr.getDragInsets());
        cm.registerComponent(ui);

        closeAction = new HideAttachmentUIAction(ui);
        openAction = new ShowAttachmentUIAction(ui);

        JRootPane rootPane = ui.getRootPane();
        KeyStroke shortcutClosePopup = KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0);
        rootPane.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(shortcutClosePopup, CLOSE_DIALOG_ACTION);
        rootPane.getActionMap().put(CLOSE_DIALOG_ACTION, closeAction);

        rootPane.getActionMap().put(SHOW_DIALOG_ACTION, openAction);

        JButton closeButton = new JButton(closeAction);
        closeButton.setText(null);
        closeButton.setFocusPainted(false);
        closeButton.setRequestFocusEnabled(false);
        closeButton.setFocusable(false);

        JToolBar jToolBar = new JToolBar();
        jToolBar.setOpaque(false);
        jToolBar.add(closeAction);
        jToolBar.setBorderPainted(false);
        jToolBar.setFloatable(false);
        ui.getAttachmentBody().setRightDecoration(jToolBar);

    }

    @Override
    protected JComponent getComponentToFocus() {
        return getUI().getFile();
    }

    @Override
    public void onCloseUI() {
        ui.dispose();
    }

    @Override
    public SwingValidator<TuttiUIContext> getValidator() {
        return null;
    }

    protected Action closeAction;

    protected Action openAction;

    public void closeEditor() {

        closeAction.actionPerformed(null);
    }

    public void openEditor(JComponent component) {

        if (component != null) {
            place(component);
        }
        openAction.actionPerformed(null);
    }

    public void init() {
        resetFields();
        ui.getAttachments().removeAll();
        AttachmentModelAware bean = ui.getBean();
        if (bean != null) {
            List<Attachment> list = bean.getAttachment();
            if (list != null) {
                for (Attachment attachment : list) {
                    addAttachment(attachment);
                }
            }
        }
    }

    public void place(JComponent component) {
        // Computes the location of bottom left corner of the cell
        Component comp = component;
        int x = 0;
        int y = component.getHeight();
        while (comp != null) {
            x += comp.getX();
            y += comp.getY();
            comp = comp.getParent();
        }

        ui.pack();
        // if the editor is too big on the right,
        // then align its right side to the right side of the cell
        if (x + ui.getWidth() > ui.getOwner().getX() + ui.getOwner().getWidth()) {
            x = x - ui.getWidth() + component.getWidth();
        }
        ui.setLocation(x, y);
    }

    public void addAttachment(Attachment attachment) {

        AttachmentItemModel model = new AttachmentItemModel();
        model.fromEntity(attachment);

        ui.setContextValue(model);
        AttachmentItem item = new AttachmentItem(ui);
        ui.getAttachments().add(item);

    }

    public void resetFields() {
        ui.getFile().setSelectedFilePath(null);
        ui.getFileName().setText("");
        ui.getFileComment().setText("");
    }

}

package fr.ifremer.tutti.ui.swing.content.protocol.calcifiedpiecessampling;

/*
 * #%L
 * Tutti :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2016 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.entities.referential.Caracteristic;
import fr.ifremer.tutti.persistence.entities.referential.Species;
import fr.ifremer.tutti.service.DecoratorService;
import fr.ifremer.tutti.ui.swing.content.protocol.EditProtocolSpeciesRowModel;
import fr.ifremer.tutti.ui.swing.content.protocol.EditProtocolUIModel;
import fr.ifremer.tutti.ui.swing.util.AbstractTuttiBeanUIModel;
import fr.ifremer.tutti.ui.swing.util.AbstractTuttiUIHandler;
import fr.ifremer.tutti.ui.swing.util.TuttiUI;
import fr.ifremer.tutti.ui.swing.util.TuttiUIUtil;
import jaxx.runtime.SwingUtil;
import jaxx.runtime.swing.JAXXWidgetUtil;
import jaxx.runtime.swing.editor.cell.NumberCellEditor;
import jaxx.runtime.validator.swing.SwingValidator;
import org.jdesktop.swingx.JXTable;
import org.jdesktop.swingx.decorator.FontHighlighter;
import org.jdesktop.swingx.decorator.HighlightPredicate;
import org.jdesktop.swingx.decorator.Highlighter;
import org.jdesktop.swingx.table.DefaultTableColumnModelExt;
import org.jdesktop.swingx.table.TableColumnExt;

import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.event.CellEditorListener;
import javax.swing.event.ChangeEvent;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableCellRenderer;
import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.TreeSet;
import java.util.stream.Collectors;

import static org.nuiton.i18n.I18n.t;

/**
 * @author Kevin Morin (Code Lutin)
 * @since 4.5
 */
public class CalcifiedPiecesSamplingEditorUIHandler extends AbstractTuttiUIHandler<EditProtocolUIModel, CalcifiedPiecesSamplingEditorUI> {

    protected Caracteristic sexCaracteristic;

    protected final PropertyChangeListener rowChangeListener = new PropertyChangeListener() {

        @Override
        public void propertyChange(PropertyChangeEvent evt) {
            getModel().setModify(true);

            String propertyName = evt.getPropertyName();
            CalcifiedPiecesSamplingEditorRowModel row = (CalcifiedPiecesSamplingEditorRowModel) evt.getSource();

            if (CalcifiedPiecesSamplingEditorRowModel.PROPERTY_MIN_SIZE.equals(propertyName)) {

                List<CalcifiedPiecesSamplingEditorRowModel> cpsRows = getModel().getCpsRows();

                int newRowIndex = cpsRows.indexOf(row);
                int previousRowIndex = newRowIndex - 1;
                CalcifiedPiecesSamplingEditorRowModel previousRow = cpsRows.get(previousRowIndex);

                Integer newValue = (Integer) evt.getNewValue();

                if (newValue == null) {
                    row.setMinSize(0);

                } else if (newValue <= previousRow.getMinSize() + 1
                           || row.getMaxSize() != null && newValue >= row.getMaxSize()) {
                    // si la nouvelle valeur sort de l'intervalle des deux lignes
                    row.setMinSize((Integer) evt.getOldValue());

                } else {
                    previousRow.setMaxSize(newValue - 1);
                    CalcifiedPiecesSamplingEditorTableModel model =
                            (CalcifiedPiecesSamplingEditorTableModel) getUI().getCpsTable().getModel();
                    model.fireTableRowsUpdated(previousRowIndex, newRowIndex);
                }

            } else if (CalcifiedPiecesSamplingEditorRowModel.PROPERTY_SAMPLING_INTERVAL.equals(propertyName)) {
                if (evt.getNewValue() == null) {
                    row.setSamplingInterval(0);
                }
            }
        }
    };

    @Override
    public void beforeInit(CalcifiedPiecesSamplingEditorUI ui) {
        super.beforeInit(ui);
        sexCaracteristic = getPersistenceService().getSexCaracteristic();
    }

    @Override
    public void afterInit(CalcifiedPiecesSamplingEditorUI calcifiedPiecesSamplingEditorUI) {
        initUI(calcifiedPiecesSamplingEditorUI);

        initBeanFilterableComboBox(ui.getSpeciesComboBox(), new ArrayList<>(), null, DecoratorService.WITH_SURVEY_CODE);

        ui.getSpeciesComboBox().getComboBoxModel().addWillChangeSelectedItemListener(evt -> {
            Species species = (Species) evt.getNextSelectedItem();
            if (species != null) {
                Optional<EditProtocolSpeciesRowModel> protocolSpecies = getModel().getProtocolSpeciesRowForSpecies(species);
                if (protocolSpecies.isPresent()) {
                    getUI().getMaturityCheckBox().setSelected(protocolSpecies.get().getMaturityPmfm() != null);
                }
            }
        });

        JXTable cpsTable = ui.getCpsTable();

        DefaultTableColumnModelExt columnModel = initTableColumnModel();

        final CalcifiedPiecesSamplingEditorTableModel tableModel =
                new CalcifiedPiecesSamplingEditorTableModel(columnModel);
        cpsTable.setModel(tableModel);
        cpsTable.setColumnModel(columnModel);

        // when model change, then rebuild the species comparator + set model as modified
        tableModel.addTableModelListener(e -> getModel().setModify(true));

        JTableHeader tableHeader = cpsTable.getTableHeader();

        // by default do not authorize to change column orders
        tableHeader.setReorderingAllowed(false);

        // always scroll to selected row
        SwingUtil.scrollToTableSelection(cpsTable);

        addHighlighters(cpsTable);

        // at the very end, set rows to model
        getModel().addPropertyChangeListener(EditProtocolUIModel.PROPERTY_CPS_ROWS,
                                             evt -> {
                                                 List<CalcifiedPiecesSamplingEditorRowModel> rows =
                                                         (List<CalcifiedPiecesSamplingEditorRowModel>) evt.getNewValue();
                                                 for (CalcifiedPiecesSamplingEditorRowModel row : rows) {
                                                     row.removePropertyChangeListener(rowChangeListener);
                                                     row.addPropertyChangeListener(rowChangeListener);
                                                 }
                                                 tableModel.setRows(rows);
                                                 tableModel.fireTableDataChanged();

                                                 getUI().getSpeciesComboBox().removeItems(rows.stream()
                                                                                              .map(CalcifiedPiecesSamplingEditorRowModel::getProtocolSpecies)
                                                                                              .map(EditProtocolSpeciesRowModel::getSpecies)
                                                                                              .collect(Collectors.toList()));
                                             });

    }

    @Override
    protected void addHighlighters(JXTable cpsTable) {
        CalcifiedPiecesSamplingEditorTableModel tableModel = (CalcifiedPiecesSamplingEditorTableModel) cpsTable.getModel();

        HighlightPredicate notSelectedPredicate = new HighlightPredicate.NotHighlightPredicate(HighlightPredicate.IS_SELECTED);
        HighlightPredicate rowIsInvalidPredicate = (renderer, adapter) -> {

            boolean result = false;
            if (adapter.isEditable()) {

                int viewRow = adapter.row;
                int modelRow = adapter.convertRowIndexToModel(viewRow);
                AbstractTuttiBeanUIModel row = tableModel.getEntry(modelRow);
                result = !row.isValid();
            }
            return result;
        };
        HighlightPredicate rowIsValidPredicate =
                new HighlightPredicate.NotHighlightPredicate(rowIsInvalidPredicate);
        Highlighter selectedHighlighter = TuttiUIUtil.newBackgroundColorHighlighter(
                HighlightPredicate.IS_SELECTED,
                getConfig().getColorSelectedRow());
        cpsTable.addHighlighter(selectedHighlighter);

        // paint in a special color for read only cells (not selected)
        Highlighter readOnlyHighlighter = TuttiUIUtil.newBackgroundColorHighlighter(
                new HighlightPredicate.AndHighlightPredicate(
                        HighlightPredicate.READ_ONLY,
                        notSelectedPredicate),
                getConfig().getColorRowReadOnly());
        cpsTable.addHighlighter(readOnlyHighlighter);

        // paint in a special color for read only cells (selected)
        Highlighter readOnlySelectedHighlighter = TuttiUIUtil.newBackgroundColorHighlighter(
                new HighlightPredicate.AndHighlightPredicate(
                        HighlightPredicate.READ_ONLY,
                        HighlightPredicate.IS_SELECTED),
                getConfig().getColorRowReadOnly().darker());
        cpsTable.addHighlighter(readOnlySelectedHighlighter);

        // paint in a special color inValid rows (not selected)
        Highlighter validHighlighter = TuttiUIUtil.newBackgroundColorHighlighter(
                new HighlightPredicate.AndHighlightPredicate(
                        HighlightPredicate.EDITABLE,
                        notSelectedPredicate,
                        rowIsInvalidPredicate),
                getConfig().getColorRowInvalid());
        cpsTable.addHighlighter(validHighlighter);

        // paint in a special color inValid rows (selected)
        Highlighter validSelectedHighlighter = TuttiUIUtil.newBackgroundColorHighlighter(
                new HighlightPredicate.AndHighlightPredicate(
                        HighlightPredicate.EDITABLE,
                        HighlightPredicate.IS_SELECTED,
                        rowIsInvalidPredicate),
                getConfig().getColorRowInvalid().darker());
        cpsTable.addHighlighter(validSelectedHighlighter);

        // use configured color odd row (not for selected)

        HighlightPredicate speciesOrderEven = (renderer, adapter) -> {
            int rowIndex = adapter.convertRowIndexToModel(adapter.row);
            return tableModel.isSpeciesOrderEven(rowIndex);
        };

        Highlighter evenHighlighter = TuttiUIUtil.newBackgroundColorHighlighter(
                new HighlightPredicate.AndHighlightPredicate(
                        new HighlightPredicate.NotHighlightPredicate(speciesOrderEven),
                        notSelectedPredicate,
                        rowIsValidPredicate,
                        HighlightPredicate.READ_ONLY),
                getConfig().getColorAlternateRow().darker());
        cpsTable.addHighlighter(evenHighlighter);

        Highlighter oddHighlighter = TuttiUIUtil.newBackgroundColorHighlighter(
                new HighlightPredicate.AndHighlightPredicate(
                        speciesOrderEven,
                        notSelectedPredicate,
                        rowIsValidPredicate,
                        HighlightPredicate.READ_ONLY),
                Color.WHITE.darker());
        cpsTable.addHighlighter(oddHighlighter);

        Highlighter evenNotReadOnlyHighlighter = TuttiUIUtil.newBackgroundColorHighlighter(
                new HighlightPredicate.AndHighlightPredicate(
                        new HighlightPredicate.NotHighlightPredicate(speciesOrderEven),
                        notSelectedPredicate,
                        rowIsValidPredicate,
                        HighlightPredicate.EDITABLE),
                getConfig().getColorAlternateRow());
        cpsTable.addHighlighter(evenNotReadOnlyHighlighter);

        Highlighter oddNotReadOnlyHighlighter = TuttiUIUtil.newBackgroundColorHighlighter(
                new HighlightPredicate.AndHighlightPredicate(
                        speciesOrderEven,
                        notSelectedPredicate,
                        rowIsValidPredicate,
                        HighlightPredicate.EDITABLE),
                Color.WHITE);
        cpsTable.addHighlighter(oddNotReadOnlyHighlighter);

        // use configured color odd row (for selected)
        Highlighter evenSelectedHighlighter = TuttiUIUtil.newBackgroundColorHighlighter(
                new HighlightPredicate.AndHighlightPredicate(
                        new HighlightPredicate.NotHighlightPredicate(speciesOrderEven),
                        HighlightPredicate.IS_SELECTED,
                        rowIsValidPredicate,
                        HighlightPredicate.EDITABLE),
                getConfig().getColorSelectedRow());
        cpsTable.addHighlighter(evenSelectedHighlighter);


        // paint in a special color inValid rows
        Font font = cpsTable.getFont().deriveFont(Font.BOLD);
        Highlighter selectHighlighter = new FontHighlighter(HighlightPredicate.IS_SELECTED, font);
        cpsTable.addHighlighter(selectHighlighter);

        HighlightPredicate rowWithoutValuePredicate = (renderer, adapter) -> {
            int rowIndex = adapter.convertRowIndexToModel(adapter.row);
            CalcifiedPiecesSamplingEditorRowModel entry = tableModel.getEntry(rowIndex);
            Integer maxByLenghtStep = entry.getMaxByLenghtStep();
            return maxByLenghtStep == null || maxByLenghtStep == 0;
        };

        font = cpsTable.getFont().deriveFont(Font.ITALIC);
        cpsTable.addHighlighter(new FontHighlighter(rowWithoutValuePredicate, font));
    }

    protected DefaultTableColumnModelExt initTableColumnModel() {

        NumberCellEditor numberCellEditor = JAXXWidgetUtil.newNumberTableCellEditor(Integer.class, false);
        numberCellEditor.getNumberEditor().setSelectAllTextOnError(true);
        numberCellEditor.getNumberEditor().getTextField().setBorder(new LineBorder(Color.GRAY, 2));
        numberCellEditor.getNumberEditor().setNumberPattern(TuttiUI.INT_6_DIGITS_PATTERN);

        // renderer to display the infinite symbol instead of null
        TableCellRenderer infiniteRenderer = (table, value, isSelected, hasFocus, row, column) -> {

            Component result = table.getDefaultRenderer(Integer.class)
                    .getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
            if (result instanceof JLabel) {
                JLabel jLabel = (JLabel) result;
                String decoratedValue = getDecorator(Integer.class, DecoratorService.NULL_INFINITE).toString(value);
                jLabel.setText(decoratedValue);
                jLabel.setHorizontalTextPosition(SwingConstants.RIGHT);
            }
            return result;
        };

        JXTable cpsTable = ui.getCpsTable();

        DefaultTableColumnModelExt columnModel = new DefaultTableColumnModelExt();

        // renderer to display the species
        TableCellRenderer speciesRenderer = (table, value, isSelected, hasFocus, row, column) -> {

            Component result = table.getDefaultRenderer(String.class)
                    .getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
            if (result instanceof JLabel) {
                JLabel jLabel = (JLabel) result;
                EditProtocolSpeciesRowModel species = (EditProtocolSpeciesRowModel) value;
                jLabel.setText(decorate(species.getSpecies(), DecoratorService.WITH_SURVEY_CODE));
            }
            return result;
        };
        addColumnToModel(columnModel,
                         null,
                         speciesRenderer,
                         CalcifiedPiecesSamplingEditorTableModel.SPECIES);

        // renderer to display infinite instead of null

        addColumnToModel(columnModel,
                         null,
                         newTableCellRender(Boolean.class, DecoratorService.MATURITY),
                         CalcifiedPiecesSamplingEditorTableModel.MATURITY);

        TableColumnExt sexColumn = addBooleanColumnToModel(columnModel, CalcifiedPiecesSamplingEditorTableModel.SEX, cpsTable);

        // sex cell listener to update the sex value of other rows with the same protocolSpecies
        sexColumn.getCellEditor().addCellEditorListener(new CellEditorListener() {

            @Override
            public void editingStopped(ChangeEvent e) {

                int selectedRow = getUI().getCpsTable().getSelectedRow();

                List<CalcifiedPiecesSamplingEditorRowModel> cpsRows = getModel().getCpsRows();
                CalcifiedPiecesSamplingEditorRowModel row = cpsRows.get(selectedRow);

                List<CalcifiedPiecesSamplingEditorRowModel> rowsToChangeSex =
                        cpsRows.stream().filter(r -> r.getProtocolSpecies().equals(row.getProtocolSpecies())).collect(Collectors.toList());

                rowsToChangeSex.forEach(r -> r.setSex(row.isSex()));

                TreeSet<Integer> indexesToUpdate =
                        new TreeSet<>(rowsToChangeSex.stream().map(cpsRows::indexOf).collect(Collectors.toSet()));

                CalcifiedPiecesSamplingEditorTableModel tableModel =
                        (CalcifiedPiecesSamplingEditorTableModel) getUI().getCpsTable().getModel();
                tableModel.fireTableRowsUpdated(indexesToUpdate.first(), indexesToUpdate.last());
            }

            @Override
            public void editingCanceled(ChangeEvent e) {
                // do nothing
            }
        });

        addIntegerColumnToModel(columnModel, CalcifiedPiecesSamplingEditorTableModel.MIN_SIZE, TuttiUI.INT_6_DIGITS_PATTERN, cpsTable);

        addColumnToModel(columnModel, null, infiniteRenderer, CalcifiedPiecesSamplingEditorTableModel.MAX_SIZE);

        addColumnToModel(columnModel, numberCellEditor, infiniteRenderer, CalcifiedPiecesSamplingEditorTableModel.MAX_BY_LENGHT_STEP);

        addIntegerColumnToModel(columnModel, CalcifiedPiecesSamplingEditorTableModel.SAMPLING_INTERVAL, TuttiUI.INT_6_DIGITS_PATTERN, cpsTable);

        addColumnToModel(columnModel, numberCellEditor, infiniteRenderer, CalcifiedPiecesSamplingEditorTableModel.OPERATION_LIMITATION);

        addColumnToModel(columnModel, numberCellEditor, infiniteRenderer, CalcifiedPiecesSamplingEditorTableModel.ZONE_LIMITATION);

        return columnModel;
    }

    @Override
    protected void beforeOpenPopup(int modelRowIndex, int modelColumnIndex) {
        super.beforeOpenPopup(modelRowIndex, modelColumnIndex);

        boolean speciesDeletable;
        boolean splitEnabled;
        boolean rowDeletable;

        JMenuItem deleteSpeciesMenu = getUI().getDeleteSpeciesMenu();

        int selectedRowCount = getUI().getCpsTable().getSelectedRowCount();

        if (selectedRowCount > 1) {

            // multi sélection
            speciesDeletable = true;
            splitEnabled = false;
            rowDeletable = false;

            deleteSpeciesMenu.setText(t("tutti.editCps.deleteMoreThanOneSpecies"));
            deleteSpeciesMenu.setToolTipText(t("tutti.editCps.deleteMoreThanOneSpecies.tip"));

        } else {

            speciesDeletable = modelRowIndex >= 0 && modelRowIndex < getModel().getCpsRows().size();
            splitEnabled = speciesDeletable;
            rowDeletable = speciesDeletable;

            if (speciesDeletable) {

                CalcifiedPiecesSamplingEditorRowModel selectedRow = getModel().getCpsRows().get(modelRowIndex);

                Integer minSize = selectedRow.getMinSize();

                splitEnabled = selectedRow.getMaxSize() == null
                        || selectedRow.getMaxSize() - minSize > 1;
                rowDeletable = minSize > 0;

            }

            deleteSpeciesMenu.setText(t("tutti.editCps.deleteOneSpecies"));
            deleteSpeciesMenu.setToolTipText(t("tutti.editCps.deleteOneSpecies.tip"));

        }

        getUI().getSplitCpsRowMenu().setEnabled(splitEnabled);

        getUI().getDeleteCpsRowMenu().setEnabled(rowDeletable);

        deleteSpeciesMenu.setEnabled(speciesDeletable);

    }

    @Override
    protected JComponent getComponentToFocus() {
        return null;
    }

    @Override
    public void onCloseUI() {

    }

    @Override
    public SwingValidator<EditProtocolUIModel> getValidator() {
        return null;
    }


    public CalcifiedPiecesSamplingEditorRowModel createNewRow(Species species,
                                                              Boolean maturity) {

        Optional<EditProtocolSpeciesRowModel> speciesProtocolRow = getEditProtocolSpeciesRowModel(species);
        boolean sex = speciesProtocolRow.isPresent()
                      && speciesProtocolRow.get().containsMandatorySampleCategoryId(sexCaracteristic.getIdAsInt());

        return createNewRow(speciesProtocolRow.orElse(null), maturity, sex, 0, null);
    }

    public CalcifiedPiecesSamplingEditorRowModel createNewRow(Species species,
                                                              Boolean maturity,
                                                              boolean sex,
                                                              Integer minSize,
                                                              Integer maxSize) {

        Optional<EditProtocolSpeciesRowModel> speciesProtocolRow = getEditProtocolSpeciesRowModel(species);

        return createNewRow(speciesProtocolRow.orElse(null), maturity, sex, minSize, maxSize);
    }

    public CalcifiedPiecesSamplingEditorRowModel createNewRow(EditProtocolSpeciesRowModel species,
                                                              Boolean maturity,
                                                              boolean sex,
                                                              Integer minSize,
                                                              Integer maxSize) {

        JXTable cpsTable = getUI().getCpsTable();
        CalcifiedPiecesSamplingEditorTableModel tableModel = (CalcifiedPiecesSamplingEditorTableModel) cpsTable.getModel();

        CalcifiedPiecesSamplingEditorRowModel newRow = tableModel.createNewRow(species, maturity, sex, minSize, maxSize);
        newRow.removePropertyChangeListener(rowChangeListener);
        newRow.addPropertyChangeListener(rowChangeListener);

        return newRow;
    }

    protected Optional<EditProtocolSpeciesRowModel> getEditProtocolSpeciesRowModel(Species species) {

        Optional<EditProtocolSpeciesRowModel> speciesProtocolRow = getModel().getSpeciesRow()
                .stream()
                .filter(sp -> species.equals(sp.getSpecies()))
                .findFirst();

        if (!speciesProtocolRow.isPresent()) {
            speciesProtocolRow = getModel().getBenthosRow()
                    .stream()
                    .filter(sp -> species.equals(sp.getSpecies()))
                    .findFirst();
        }
        return speciesProtocolRow;
    }
}

package fr.ifremer.tutti.ui.swing.content.home.actions;

/*
 * #%L
 * Tutti :: UI
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import fr.ifremer.tutti.persistence.ProgressionModel;
import fr.ifremer.tutti.persistence.entities.data.Cruise;
import fr.ifremer.tutti.service.export.sumatra.CatchesSumatraExportService;
import fr.ifremer.tutti.service.export.sumatra.SumatraExportResult;
import fr.ifremer.tutti.ui.swing.content.actions.AbstractMainUITuttiAction;
import fr.ifremer.tutti.ui.swing.content.MainUIHandler;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.util.DateUtil;

import java.io.File;
import java.util.Date;

import static org.nuiton.i18n.I18n.t;

/**
 * Opens a file chooser, exports the cruise catches into the selected file and open the default email editor.
 *
 * @author Kevin Morin - kmorin@codelutin.com
 * @since 1.0
 */
public class ExportCruiseForSumatraAction extends AbstractMainUITuttiAction {

    /** Logger. */
    private static final Log log =
            LogFactory.getLog(ExportCruiseForSumatraAction.class);

    private static final String EXPORT_FILE_NAME = "sumatra_%s_%s"; // sumatra_nom campagne - date du jour

    protected File file;

    protected SumatraExportResult sumatraExportResult;

    public ExportCruiseForSumatraAction(MainUIHandler handler) {
        super(handler, false);
    }

    @Override
    public boolean prepareAction() throws Exception {

        boolean doAction = super.prepareAction();

        if (doAction) {

            if (!getDataContext().isProtocolFilled()) {
                displayWarningMessage(
                        t("tutti.exportCruiseCsv.title.missing.protocol"),
                        t("tutti.exportCruiseCsv.message.missing.protocol")
                );
            }
        }

        if (doAction) {

            String date = DateUtil.formatDate(new Date(), "dd-MM-yyyy");
            String exportFilename = String.format(EXPORT_FILE_NAME, getDataContext().getCruise().getName(), date);

            // choose file to export
            file = saveFile(
                    exportFilename,
                    "csv",
                    t("tutti.exportCruiseCsv.title.choose.exportFile"),
                    t("tutti.exportCruiseCsv.action.chooseFile"),
                    "^.+\\.csv$", t("tutti.common.file.csv")
            );
            doAction = file != null;
        }
        return doAction;
    }

    @Override
    public void releaseAction() {
        file = null;
        sumatraExportResult = null;
        super.releaseAction();
    }

    @Override
    public void doAction() throws Exception {
        Cruise cruise = getDataContext().getCruise();
        Preconditions.checkNotNull(cruise);
        Preconditions.checkNotNull(file);

        if (log.isInfoEnabled()) {
            log.info("Will export cruise " + cruise.getId() +
                     " to file: " + file);
        }
        ProgressionModel pm = new ProgressionModel();
        pm.setTotal(3); // loading cruise + loading fishing operationIds + export
        setProgressionModel(pm);

        // export catches
        CatchesSumatraExportService service =
                getContext().getCatchesSumatraExportService();
        sumatraExportResult = service.exportCruiseForSumatra(file, cruise.getIdAsInt(), pm);

    }

    @Override
    public void postSuccessAction() {
        super.postSuccessAction();

        if (sumatraExportResult.withBadSpecies()) {

            StringBuilder badSpeciesList = new StringBuilder();
            for (String s : sumatraExportResult.getBadSpecies()) {
                badSpeciesList.append("<li>").append(s);
            }

            displayWarningMessage(
                    t("tutti.exportCruiseCsv.title.badSpecies"),
                    t("tutti.exportCruiseCsv.message.badSpecies", badSpeciesList.toString()));
        }

        if (sumatraExportResult.withBadBenthos()) {

            StringBuilder badBenthosList = new StringBuilder();
            for (String s : sumatraExportResult.getBadBenthos()) {
                badBenthosList.append("<li>").append(s);
            }

            displayWarningMessage(
                    t("tutti.exportCruiseCsv.title.badBenthos"),
                    t("tutti.exportCruiseCsv.message.badBenthos", badBenthosList.toString()));
        }

        sendMessage(t("tutti.exportCruiseCsv.action.success", file));
    }
}

package fr.ifremer.tutti.ui.swing.content.operation.catches.species.frequency;

/*
 * #%L
 * Tutti :: UI
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import fr.ifremer.tutti.persistence.entities.data.CopyIndividualObservationMode;
import fr.ifremer.tutti.type.WeightUnit;
import fr.ifremer.tutti.util.Numbers;
import fr.ifremer.tutti.util.Weights;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jdesktop.swingx.table.TableColumnModelExt;
import org.nuiton.jaxx.application.swing.table.AbstractApplicationTableModel;
import org.nuiton.jaxx.application.swing.table.ColumnIdentifier;

import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import static org.nuiton.i18n.I18n.n;

/**
 * Model of the species frequency table.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 0.2
 */
public class SpeciesFrequencyTableModel extends AbstractApplicationTableModel<SpeciesFrequencyRowModel> {

    /** Logger. */
    private static final Log log = LogFactory.getLog(SpeciesFrequencyTableModel.class);

    private static final long serialVersionUID = 1L;

    public static final ColumnIdentifier<SpeciesFrequencyRowModel> LENGTH_STEP = ColumnIdentifier.newId(
            SpeciesFrequencyRowModel.PROPERTY_LENGTH_STEP,
            n("tutti.editSpeciesFrequencies.table.header.lengthStep"),
            n("tutti.editSpeciesFrequencies.table.header.lengthStep"));

    public static final ColumnIdentifier<SpeciesFrequencyRowModel> NUMBER = ColumnIdentifier.newId(
            SpeciesFrequencyRowModel.PROPERTY_NUMBER,
            n("tutti.editSpeciesFrequencies.table.header.number"),
            n("tutti.editSpeciesFrequencies.table.header.number"));

    public static final ColumnIdentifier<SpeciesFrequencyRowModel> WEIGHT = ColumnIdentifier.newId(
            SpeciesFrequencyRowModel.PROPERTY_WEIGHT,
            n("tutti.editSpeciesFrequencies.table.header.weight"),
            n("tutti.editSpeciesFrequencies.table.header.weight"));

    public static final ColumnIdentifier<SpeciesFrequencyRowModel> RTP_COMPUTED_WEIGHT = ColumnIdentifier.newId(
            SpeciesFrequencyRowModel.PROPERTY_RTP_COMPUTED_WEIGHT,
            n("tutti.editSpeciesFrequencies.table.header.rtpComputedWeight"),
            n("tutti.editSpeciesFrequencies.table.header.rtpComputedWeight"));

    private final SpeciesFrequencyUIModel uiModel;

    private final SpeciesFrequencyUIModelCache modelCache;

    /**
     * Weight unit.
     *
     * @since 2.5
     */
    protected final WeightUnit weightUnit;

    protected final WeightUnit individualObservationWeightUnit;

    protected transient PropertyChangeListener onLengthStepChangedListener;

    protected transient PropertyChangeListener onWeightChangedListener;

    protected transient PropertyChangeListener onNumberChangedListener;

    public SpeciesFrequencyTableModel(WeightUnit weightUnit,
                                      WeightUnit individualObservationWeightUnit,
                                      TableColumnModelExt columnModel,
                                      SpeciesFrequencyUIModel uiModel) {
        super(columnModel, true, true);
        this.weightUnit = weightUnit;
        this.individualObservationWeightUnit = individualObservationWeightUnit;
        this.uiModel = uiModel;
        this.modelCache = uiModel.getCache();
        setNoneEditableCols(RTP_COMPUTED_WEIGHT);
    }

    @Override
    public boolean isCreateNewRow() {
        return uiModel.isCopyIndividualObservationNothing() && super.isCreateNewRow();
    }

    @Override
    public SpeciesFrequencyRowModel createNewRow() {
        return createNewRow(true, true);
    }

    public SpeciesFrequencyRowModel createNewRow(boolean useDefaultStep, boolean attachListeners) {
        Float defaultStep = null;

        int rowCount = getRowCount();
        if (useDefaultStep && rowCount > 0) {

            SpeciesFrequencyRowModel rowModel = getEntry(rowCount - 1);
            Float lengthStep = rowModel.getLengthStep();
            if (lengthStep != null) {
                defaultStep = uiModel.getLengthStep(lengthStep + uiModel.getStep());
            }
        }
        SpeciesFrequencyRowModel result = new SpeciesFrequencyRowModel(weightUnit);

        if (attachListeners) {
            attachListeners(result);
        }

        result.setLengthStepCaracteristic(uiModel.getLengthStepCaracteristic());
        result.setLengthStep(defaultStep);
        result.setValid(defaultStep != null);

        return result;
    }

    @Override
    protected boolean isCellEditable(int rowIndex, int columnIndex, ColumnIdentifier<SpeciesFrequencyRowModel> propertyName) {
        boolean result;

        CopyIndividualObservationMode copyIndividualObservationMode = uiModel.getCopyIndividualObservationMode();

        result = super.isCellEditable(rowIndex, columnIndex, propertyName);

        if (result && copyIndividualObservationMode != null) {
            switch (copyIndividualObservationMode) {
                case ALL:
                    result = false;
                    break;
                case NOTHING:
                    result = !(WEIGHT.equals(propertyName) && uiModel.isCopyRtpWeights());
                    break;
                case SIZE:
                    result = WEIGHT.equals(propertyName) && !uiModel.isCopyRtpWeights();
                    break;
            }
        }
        return result;
    }

    @Override
    public void setValueAt(Object aValue,
                           int rowIndex,
                           int columnIndex,
                           ColumnIdentifier<SpeciesFrequencyRowModel> propertyName,
                           SpeciesFrequencyRowModel entry) {
        super.setValueAt(aValue, rowIndex, columnIndex, propertyName, entry);
        uiModel.setModify(true);
        // TODO Rebuild the computedWeight if possible...
    }

    @Override
    protected void onRowAdded(int rowIndex, SpeciesFrequencyRowModel row) {

        uiModel.recomputeCanEditLengthStep();
        uiModel.setModify(true);

    }

    @Override
    protected void onRowUpdated(int rowIndex, SpeciesFrequencyRowModel row) {

        uiModel.recomputeCanEditLengthStep();
        uiModel.setModify(true);

    }

    @Override
    protected void onRowRemoved(int rowIndex, SpeciesFrequencyRowModel row) {

        dettachListeners(row);
        uiModel.recomputeCanEditLengthStep();

    }

    @Override
    protected void onBeforeRowsChanged(List<SpeciesFrequencyRowModel> oldRows) {

        if (oldRows != null) {
            oldRows.forEach(this::dettachListeners);
        }

    }

    @Override
    protected void onRowsChanged(List<SpeciesFrequencyRowModel> newRows) {

        if (newRows != null) {
            newRows.forEach(this::attachListeners);
        }

        uiModel.recomputeCanEditLengthStep();

    }

    public boolean recomputeCanEditLengthStep() {
        boolean result = true;
        if (rows != null) {
            for (SpeciesFrequencyRowModel row : rows) {

                if (row.isEmpty()) {
                    // la ligne est vide
                    continue;
                }
                if (row.getLengthStep() == null || row.getNumber() == null) {
                    // la ligne n'est pas complete
                    continue;
                }

                // une ligne non vide et complete a ete trouvee
                // on ne peut plus editer
                result = false;
                break;
            }
        }
        return result;
    }

    public void decrementNumberForLengthStep(Float lengthStep) {

        SpeciesFrequencyRowModel row = modelCache.getRowCache().get(lengthStep);

        if (row != null) {

            Integer number = row.getNumber();

            if (number != null) {

                if (number > 1) {
                    row.setNumber(number - 1);
                } else {
                    row.setNumber(null);
                }

                int rowIndex = getRowIndex(row);

                fireTableRowsUpdated(rowIndex, rowIndex);

            }

            uiModel.recomputeCanEditLengthStep();

        }

    }

    public SpeciesFrequencyRowModel getOrCreateRowForLengthStep(float lengthstep) {

        // obtenir la classe de taille (en fonction de la précision de la méthode de mensuration)
        float realLengthStep = uiModel.getLengthStep(lengthstep);

        Map<Float, SpeciesFrequencyRowModel> rowCache = modelCache.getRowCache();

        SpeciesFrequencyRowModel row = rowCache.get(realLengthStep);

        if (row == null) {
            row = createNewRow(false, true);
            row.setLengthStep(realLengthStep);
            rowCache.put(realLengthStep, row);

            // get new index
            List<Float> steps = new ArrayList<>(rowCache.keySet());
            steps.add(realLengthStep);
            Collections.sort(steps);

            int indexToInsert = steps.indexOf(realLengthStep);
            addNewRow(indexToInsert, row);

        }

        return row;

    }

    public Optional<SpeciesFrequencyRowModel> getOptionalRowForLengthStep(float lengthstep) {

        // obtenir la classe de taille (en fonction de la précision de la méthode de mensuration)
        float realLengthStep = uiModel.getLengthStep(lengthstep);

        Map<Float, SpeciesFrequencyRowModel> rowCache = modelCache.getRowCache();

        SpeciesFrequencyRowModel row = rowCache.get(realLengthStep);
        return Optional.ofNullable(row);

    }

    public void incrementFrequencyRowsNumbers(float lengthStepToIncrement) {

        if (log.isDebugEnabled()) {
            log.debug("incrementFrequencyRowsNumbers" + lengthStepToIncrement);
        }

        SpeciesFrequencyRowModel row = getOrCreateRowForLengthStep(lengthStepToIncrement);

        row.incNumber();
        updateRow(row);

    }

    public void decrementFrequencyRowsNumbers(float lengthStepToDecrement) {

        if (log.isDebugEnabled()) {
            log.debug("decrementFrequencyRowsNumbers " + lengthStepToDecrement);
        }

        Optional<SpeciesFrequencyRowModel> optionalRow = getOptionalRowForLengthStep(lengthStepToDecrement);

        if (optionalRow.isPresent()) {

            SpeciesFrequencyRowModel row = optionalRow.get();
            row.decNumber();

            if (!row.withNumber()) {

                // Plus d'occurrence de cette classe de taille, on la supprime
                if (log.isInfoEnabled()) {
                    log.info("Remove length class " + row + " from frequencies.");
                }

                removeRow(row);

            } else {
                updateRow(row);
            }

        }

    }

    public Float convertWeightFromIndividualObservation(float weight) {
        return Weights.convert(uiModel.getIndividualObservationWeightUnit(), weightUnit, weight);
    }

    public void addWeightToFrequencyRow(float lengthStep, float weight) {

        if (log.isDebugEnabled()) {
            log.debug("add weight to frequency (lengthStep: " + lengthStep + "): " + weight);
        }

        Preconditions.checkState(!weightUnit.isSmallerThanZero(weight));

        SpeciesFrequencyRowModel row = getOrCreateRowForLengthStep(lengthStep);

        row.addToWeight(weight);

        updateRow(row);

    }

    public void removeWeightToFrequencyRow(float lengthStep, float weight) {

        if (log.isDebugEnabled()) {
            log.debug("remove weight to frequency (lengthStep: " + lengthStep + "): " + weight);
        }

        Preconditions.checkState(!weightUnit.isSmallerThanZero(weight));

        Optional<SpeciesFrequencyRowModel> optionalRow = getOptionalRowForLengthStep(lengthStep);

        if (optionalRow.isPresent()) {

            SpeciesFrequencyRowModel row = optionalRow.get();

            row.removeFromWeight(weight);

            updateRow(row);

        }

    }

    public List<SpeciesFrequencyRowModel> loadRows(List<SpeciesFrequencyRowModel> incomingRows) {

        List<SpeciesFrequencyRowModel> result = new ArrayList<>();

        //FIXME Faire un check sur la méthode de mensuration qui doit être la même
        if (CollectionUtils.isNotEmpty(incomingRows)) {

            for (SpeciesFrequencyRowModel rowModel : incomingRows) {

                SpeciesFrequencyRowModel newRow = createNewRow(false, false);
                newRow.copy(rowModel);
                result.add(newRow);

            }

        }

        // always sort row by their length
        // see http://forge.codelutin.com/issues/2482
        Collections.sort(result);

        return result;

    }

    public SpeciesFrequencyRowModel addRafaleRow(float lengthStep) {

        Map<Float, SpeciesFrequencyRowModel> rowsByStep = modelCache.getRowCache();

        SpeciesFrequencyRowModel row = rowsByStep.get(lengthStep);

        int rowIndex;

        if (row != null) {

            // increments current row
            Integer number = row.getNumber();
            row.setNumber((number == null ? 0 : number) + 1);
            updateRow(row);

        } else {

            // create a new row

            row = createNewRow();
            row.setLengthStep(lengthStep);
            row.setNumber(1);
            row.setValid(uiModel.isRowValid(row));

            // get new index
            List<Float> steps = new ArrayList<>(rowsByStep.keySet());
            steps.add(lengthStep);

            Collections.sort(steps);

            rowIndex = steps.indexOf(lengthStep);

            addNewRow(rowIndex, row);
        }

        return row;

    }

    public void generateRows() {

        float minStep = uiModel.getLengthStep(uiModel.getMinStep());
        float maxStep = uiModel.getLengthStep(uiModel.getMaxStep());

        Map<Float, SpeciesFrequencyRowModel> rowsByStep = modelCache.getRowCache();
        Set<Float> existingKeys = new HashSet<>(rowsByStep.keySet());
        List<SpeciesFrequencyRowModel> rows = new ArrayList<>(rowsByStep.values());

        for (float lengthClass = minStep, step = uiModel.getStep(); lengthClass <= maxStep; lengthClass = Numbers.getRoundedLengthStep(lengthClass + step, true)) {

            if (!existingKeys.contains(lengthClass)) {

                // add it
                SpeciesFrequencyRowModel newRow = createNewRow(false, true);
                newRow.setLengthStep(lengthClass);
                rows.add(newRow);

            }

        }

        Collections.sort(rows);
        uiModel.setRows(rows);

    }

    public void reloadRowsFromIndividualObservations() {

        if (uiModel.mustCopyIndividualObservationSize()) {

            if (log.isInfoEnabled()) {
                log.info("Generate frequencies from individual observations (mode: " + uiModel.getCopyIndividualObservationMode() + ")");
            }

            boolean copyWeights = uiModel.isCopyIndividualObservationAll();

            rows.clear();

            uiModel.getValidIndividualObservations().stream()
                   .filter(IndividualObservationBatchRowModel::withSize)
                   .forEachOrdered(individualObservation -> {

                       SpeciesFrequencyRowModel row = getOrCreateRowForLengthStep(individualObservation.getSize());
                       row.incNumber();

                       if (copyWeights && individualObservation.withWeight()) {
                           Float weight = convertWeightFromIndividualObservation(individualObservation.getWeight());
                           row.addToWeight(weight);
                       }

                   });

        } else {
            if (rows.isEmpty()) {
                addNewRow();
            }
        }

        Collections.sort(rows);

        uiModel.reloadRows();

        fireTableDataChanged();

    }


    private void lengthStepWasRemoved(Float lengthStep) {

        uiModel.getAverageWeightsHistogramModel().removeValue(lengthStep);
        uiModel.getFrequenciesHistogramModel().removeValue(lengthStep);

    }

    private void lengthStepOrNumberWasUpdated(SpeciesFrequencyRowModel row) {

        if (row.withNumber()) {

            uiModel.getAverageWeightsHistogramModel().addOrUpdate(row);
            uiModel.getFrequenciesHistogramModel().addOrUpdate(row);

        }

    }

    private PropertyChangeListener getOnLengthStepChangedListener() {
        if (onLengthStepChangedListener == null) {
            onLengthStepChangedListener = evt -> {

                SpeciesFrequencyRowModel row = (SpeciesFrequencyRowModel) evt.getSource();

                // recompute the weight with the rtp
                uiModel.computeRowWeightWithRtp(row);

                Float oldValue = (Float) evt.getOldValue();
                if (oldValue != null) {

                    modelCache.removeLengthStep(oldValue);
                    lengthStepWasRemoved(oldValue);

                }

                Float newValue = (Float) evt.getNewValue();
                if (newValue != null) {

                    modelCache.addLengthStep(row);
                    lengthStepOrNumberWasUpdated(row);

                }

                uiModel.recomputeCanEditLengthStep();
                uiModel.recomputeRowsValidateState();
                uiModel.updateEmptyRow(row);

                // Can recompute total number and weight only after valid flag change
                uiModel.recomputeTotalNumber();
                uiModel.recomputeTotalWeight();

                //FIXME Explain why this ?
//                fireTableDataChanged();

            };
        }
        return onLengthStepChangedListener;
    }

    private PropertyChangeListener getOnNumberChangedListener() {
        if (onNumberChangedListener == null) {
            onNumberChangedListener = evt -> {

                SpeciesFrequencyRowModel row = (SpeciesFrequencyRowModel) evt.getSource();

                // recompute the weight with the rtp
                uiModel.computeRowWeightWithRtp(row);

                Float lengthStep = row.getLengthStep();

                if (lengthStep != null) {

                    if (!row.withNumber()) {

                        // remove the value for the lengthStep
                        lengthStepWasRemoved(lengthStep);

                    } else {

                        lengthStepOrNumberWasUpdated(row);

                    }

                }

                uiModel.recomputeCanEditLengthStep();
                uiModel.recomputeRowValidState(row);
                uiModel.updateEmptyRow(row);

                // Can recompute total number and weight only after valid flag change
                uiModel.recomputeTotalNumber();
                uiModel.recomputeTotalWeight();

                //FIXME Explain why this ?
//                fireTableDataChanged();

            };
        }
        return onNumberChangedListener;
    }

    private PropertyChangeListener getOnWeightChangedListener() {
        if (onWeightChangedListener == null) {
            onWeightChangedListener = evt -> {

                SpeciesFrequencyRowModel row = (SpeciesFrequencyRowModel) evt.getSource();
                modelCache.updateRowWithWeight(row);

                Float lengthStep = row.getLengthStep();

                if (lengthStep != null) {

                    if (!row.withWeight()) {

                        // remove the value for the lengthStep
                        uiModel.getAverageWeightsHistogramModel().removeValue(lengthStep);

                    } else {

                        uiModel.getAverageWeightsHistogramModel().addOrUpdate(row);

                    }

                }

                uiModel.recomputeRowsValidateState();
                uiModel.updateEmptyRow(row);

                // Can recompute total number and weight only after valid flag change
                uiModel.recomputeTotalWeight();
                uiModel.recomputeTotalNumber();

            };
        }
        return onWeightChangedListener;
    }

    private void dettachListeners(SpeciesFrequencyRowModel result) {

        result.removePropertyChangeListener(SpeciesFrequencyRowModel.PROPERTY_LENGTH_STEP, getOnLengthStepChangedListener());
        result.removePropertyChangeListener(SpeciesFrequencyRowModel.PROPERTY_WEIGHT, getOnWeightChangedListener());
        result.removePropertyChangeListener(SpeciesFrequencyRowModel.PROPERTY_NUMBER, getOnNumberChangedListener());

    }

    private void attachListeners(SpeciesFrequencyRowModel result) {

        dettachListeners(result); // prevent leaks!

        result.addPropertyChangeListener(SpeciesFrequencyRowModel.PROPERTY_LENGTH_STEP, getOnLengthStepChangedListener());
        result.addPropertyChangeListener(SpeciesFrequencyRowModel.PROPERTY_WEIGHT, getOnWeightChangedListener());
        result.addPropertyChangeListener(SpeciesFrequencyRowModel.PROPERTY_NUMBER, getOnNumberChangedListener());

    }

    private void removeRow(SpeciesFrequencyRowModel row) {

        int rowIndex = getRowIndex(row);
        removeRow(rowIndex);
        modelCache.getRowCache().remove(row.getLengthStep());

    }

}
/*
 * #%L
 * Tutti :: UI
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
*/

#importPupitriMelagWeightDialog {
  modal: true;
  title: "tutti.importPupitri.melagWeight.title";
}

#message {
  text: "tutti.importPupitri.melagWeight.message";
}

#melagWeightLabel {
  text: "tutti.importPupitri.melagWeight.field";
  labelFor: {melagWeight};
  _addWeightUnit: {handler.getConfig().getSpeciesWeightUnit()};
}

#melagWeight {
  autoPopup: {handler.getConfig().isAutoPopupNumberEditor()};
  showPopupButton: {handler.getConfig().isShowNumberEditorButton()};
  showReset: true;
  numberType: {Float.class};
  computedDataColor: {handler.getConfig().getColorComputedWeights()};
  bean: {model.getTotalMelagComputedOrNotWeight()};
  numberValue: {model.getTotalMelagWeight()};
}

#validateButton {
  actionIcon: add;
  text: "tutti.common.validate";
  toolTipText: "tutti.common.validate";
  i18nMnemonic: "tutti.common.validate.mnemonic";
  _simpleAction: {fr.ifremer.tutti.ui.swing.content.operation.catches.species.edit.actions.ImportPupitriMelagWeightPopupValidateAction.class};
}


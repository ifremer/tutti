package fr.ifremer.tutti.ui.swing.util.attachment;

/*
 * #%L
 * Tutti :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.adagio.core.dao.referential.ObjectTypeCode;
import fr.ifremer.tutti.persistence.entities.data.Attachment;
import fr.ifremer.tutti.persistence.entities.data.AttachmentBean;
import fr.ifremer.tutti.persistence.entities.data.Attachments;
import fr.ifremer.tutti.ui.swing.util.AbstractTuttiBeanUIModel;
import org.nuiton.util.beans.Binder;
import org.nuiton.util.beans.BinderFactory;

/**
 * Created on 3/7/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.15
 */
public class AttachmentItemModel extends AbstractTuttiBeanUIModel<Attachment, AttachmentItemModel> {

    private static final long serialVersionUID = 1L;

    private final Attachment editObject = new AttachmentBean();

    protected static Binder<AttachmentItemModel, Attachment> toBeanBinder =
            BinderFactory.newBinder(AttachmentItemModel.class,
                                    Attachment.class);

    protected static Binder<Attachment, AttachmentItemModel> fromBeanBinder =
            BinderFactory.newBinder(Attachment.class, AttachmentItemModel.class);

    public AttachmentItemModel() {
        super(fromBeanBinder, toBeanBinder);
    }

    @Override
    protected Attachment newEntity() {
        return Attachments.newAttachment();
    }

    public String getName() {
        return editObject.getName();
    }

    public void setName(String name) {
        editObject.setName(name);
    }

    public Integer getObjectId() {
        return editObject.getObjectId();
    }

    public void setObjectId(Integer objectId) {
        editObject.setObjectId(objectId);
    }

    public String getComment() {
        return editObject.getComment();
    }

    public void setComment(String comment) {
        editObject.setComment(comment);
    }

    public ObjectTypeCode getObjectType() {
        return editObject.getObjectType();
    }

    public void setObjectType(ObjectTypeCode objectType) {
        editObject.setObjectType(objectType);
    }

    public String getPath() {
        return editObject.getPath();
    }

    public void setPath(String path) {
        editObject.setPath(path);
    }
}

package fr.ifremer.tutti.ui.swing.content.actions;

/*
 * #%L
 * Tutti :: UI
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.ui.swing.RunTutti;
import fr.ifremer.tutti.ui.swing.content.MainUIHandler;

import static org.nuiton.i18n.I18n.t;

/**
 * To close Tutti Application.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.1
 */
public class CloseApplicationAction extends AbstractChangeScreenAction {

    protected int exitCode = RunTutti.STOP_EXIT_CODE;

    public CloseApplicationAction(MainUIHandler handler) {
        super(handler, false, null);
        setSkipCheckCurrentScreen(true);
        setActionDescription(t("tutti.main.action.exit.tip"));
    }

    public void setExitCode(int exitCode) {
        this.exitCode = exitCode;
    }

    @Override
    public void doAction() throws Exception {

        super.doAction();

        RunTutti.closeTutti(getHandler(), exitCode);
    }


    @Override
    public void releaseAction() {
        exitCode = RunTutti.STOP_EXIT_CODE;
        super.releaseAction();
    }
}

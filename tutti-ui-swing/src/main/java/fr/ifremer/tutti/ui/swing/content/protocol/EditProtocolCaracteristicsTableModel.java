package fr.ifremer.tutti.ui.swing.content.protocol;

/*
 * #%L
 * Tutti :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.entities.referential.Caracteristic;
import fr.ifremer.tutti.ui.swing.util.table.AbstractTuttiTableModel;
import org.jdesktop.swingx.table.TableColumnModelExt;
import org.nuiton.jaxx.application.swing.table.ColumnIdentifier;

import java.util.Collection;

import static org.nuiton.i18n.I18n.n;

/**
 * @author Kevin Morin (Code Lutin)
 * @since 4.2
 */
public class EditProtocolCaracteristicsTableModel extends AbstractTuttiTableModel<EditProtocolCaracteristicsRowModel> {

    private static final long serialVersionUID = 1L;

    protected Collection<Caracteristic> caracteristics;

    public static final ColumnIdentifier<EditProtocolCaracteristicsRowModel> PSFM_ID = ColumnIdentifier.newId(
            EditProtocolCaracteristicsRowModel.PROPERTY_PSFM,
            n("tutti.editProtocol.table.header.caracteristics.psfmId"),
            n("tutti.editProtocol.table.header.caracteristics.psfmId.tip"));

    public static final ColumnIdentifier<EditProtocolCaracteristicsRowModel> TYPE = ColumnIdentifier.newId(
            EditProtocolCaracteristicsRowModel.PROPERTY_TYPE,
            n("tutti.editProtocol.table.header.caracteristics.type"),
            n("tutti.editProtocol.table.header.caracteristics.type.tip"));

    public static final ColumnIdentifier<EditProtocolCaracteristicsRowModel> IMPORT_FILE_COLUMN = ColumnIdentifier.newId(
            EditProtocolCaracteristicsRowModel.PROPERTY_IMPORT_COLUMN,
            n("tutti.editProtocol.table.header.caracteristics.importFileColumn"),
            n("tutti.editProtocol.table.header.caracteristics.importFileColumn.tip"));

    public EditProtocolCaracteristicsTableModel(TableColumnModelExt columnModel, Collection<Caracteristic> caracteristics) {
        super(columnModel, false, false);
        setNoneEditableCols(PSFM_ID);
        this.caracteristics = caracteristics;
    }

    @Override
    public EditProtocolCaracteristicsRowModel createNewRow() {
        return new EditProtocolCaracteristicsRowModel(caracteristics);
    }
}

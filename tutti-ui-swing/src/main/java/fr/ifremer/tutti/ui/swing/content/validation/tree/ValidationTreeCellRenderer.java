package fr.ifremer.tutti.ui.swing.content.validation.tree;

/*
 * #%L
 * Tutti :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.entities.data.Cruise;
import fr.ifremer.tutti.persistence.entities.data.FishingOperation;
import org.nuiton.decorator.Decorator;

import javax.swing.JLabel;
import javax.swing.JTree;
import javax.swing.tree.DefaultTreeCellRenderer;
import java.awt.Component;

import static org.nuiton.i18n.I18n.t;

/**
 * Created on 7/9/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.6
 */
public class ValidationTreeCellRenderer extends DefaultTreeCellRenderer {

    private static final long serialVersionUID = 1L;

    private final Decorator<Cruise> cruiseDecorator;

    private final Decorator<FishingOperation> fishingOperationDecorator;

    public ValidationTreeCellRenderer(Decorator<Cruise> cruiseDecorator, Decorator<FishingOperation> fishingOperationDecorator) {
        this.cruiseDecorator = cruiseDecorator;
        this.fishingOperationDecorator = fishingOperationDecorator;
    }

    @Override
    public Component getTreeCellRendererComponent(JTree tree, Object value, boolean selected, boolean expanded, boolean leaf, int row, boolean hasFocus) {
        JLabel label = (JLabel) super.getTreeCellRendererComponent(tree, value, selected, expanded, leaf, row, hasFocus);

        if (value != null) {
            if (value instanceof CruiseTreeNode) {
                CruiseTreeNode node = (CruiseTreeNode) value;
                String text = cruiseDecorator.toString(node.getUserObject());
                label.setText(text);
                label.setToolTipText(text);
                label.setIcon(node.getIcon());

            } else if (value instanceof OperationTreeNode) {
                OperationTreeNode node = (OperationTreeNode) value;
                String text = fishingOperationDecorator.toString(node.getUserObject());
                label.setText(text);
                label.setToolTipText(text);
                label.setIcon(node.getIcon());

            } else if (value instanceof MessageTreeNode) {
                MessageTreeNode node = (MessageTreeNode) value;
                String text = t(node.getUserObject());
                label.setText(text);
                label.setToolTipText(text);
                label.setIcon(node.getIcon());
            }
        }
        return label;
    }
}

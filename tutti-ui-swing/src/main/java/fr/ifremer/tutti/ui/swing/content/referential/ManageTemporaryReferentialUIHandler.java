package fr.ifremer.tutti.ui.swing.content.referential;

/*
 * #%L
 * Tutti :: UI
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.entities.referential.Gear;
import fr.ifremer.tutti.persistence.entities.referential.Person;
import fr.ifremer.tutti.persistence.entities.referential.Species;
import fr.ifremer.tutti.persistence.entities.referential.Vessel;
import fr.ifremer.tutti.ui.swing.util.AbstractTuttiUIHandler;
import jaxx.runtime.validator.swing.SwingValidator;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.swing.JComponent;
import java.util.List;

/**
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.0
 */
public class ManageTemporaryReferentialUIHandler extends AbstractTuttiUIHandler<ManageTemporaryReferentialUIModel, ManageTemporaryReferentialUI> {

    /** Logger. */
    private static final Log log =
            LogFactory.getLog(ManageTemporaryReferentialUIHandler.class);

    //------------------------------------------------------------------------//
    //-- AbstractTuttiUIHandler methods                                     --//
    //------------------------------------------------------------------------//

    @Override
    public void beforeInit(ManageTemporaryReferentialUI ui) {
        super.beforeInit(ui);

        ManageTemporaryReferentialUIModel model = new ManageTemporaryReferentialUIModel();

        List<Species> temporarySpecies = getContext().getReferentialTemporarySpeciesService().getTemporarySpeciess();
        model.setNbTemporarySpecies(temporarySpecies.size());

        List<Vessel> temporaryVessels = getContext().getReferentialTemporaryVesselService().getTemporaryVessels();
        model.setNbTemporaryVessels(temporaryVessels.size());

        List<Gear> temporaryGears = getContext().getReferentialTemporaryGearService().getTemporaryGears();
        model.setNbTemporaryGears(temporaryGears.size());

        List<Person> temporaryPersons = getContext().getReferentialTemporaryPersonService().getTemporaryPersons();
        model.setNbTemporaryPersons(temporaryPersons.size());

        ui.setContextValue(model);
    }

    @Override
    public void afterInit(ManageTemporaryReferentialUI ui) {

        initUI(ui);

    }

    @Override
    protected JComponent getComponentToFocus() {
        return null;
    }

    @Override
    public void onCloseUI() {
        if (log.isDebugEnabled()) {
            log.debug("closing: " + ui);
        }
    }

    @Override
    public SwingValidator<ManageTemporaryReferentialUIModel> getValidator() {
        return null;
    }

}

package fr.ifremer.tutti.ui.swing.content.protocol;

/*
 * #%L
 * Tutti :: UI
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Lists;
import fr.ifremer.tutti.persistence.entities.referential.Species;
import org.jdesktop.swingx.table.TableColumnModelExt;
import org.nuiton.jaxx.application.swing.table.AbstractApplicationTableModel;
import org.nuiton.jaxx.application.swing.table.ColumnIdentifier;

import java.util.List;
import java.util.function.Supplier;

import static org.nuiton.i18n.I18n.n;

/**
 * @author kmorin
 * @since 0.3
 */
public class EditProtocolSpeciesTableModel extends AbstractApplicationTableModel<EditProtocolSpeciesRowModel> {

    public static final ColumnIdentifier<EditProtocolSpeciesRowModel> SPECIES_ID = ColumnIdentifier.newId(
            EditProtocolSpeciesRowModel.PROPERTY_SPECIES,
            n("tutti.editProtocol.table.header.speciesReferenceTaxonId"),
            n("tutti.editProtocol.table.header.speciesReferenceTaxonId.tip"));

    public static final ColumnIdentifier<EditProtocolSpeciesRowModel> SURVEY_CODE_ID = ColumnIdentifier.newId(
            EditProtocolSpeciesRowModel.PROPERTY_SPECIES_SURVEY_CODE,
            n("tutti.editProtocol.table.header.speciesSurveyCode"),
            n("tutti.editProtocol.table.header.speciesSurveyCode.tip"));

    public static final ColumnIdentifier<EditProtocolSpeciesRowModel> LENGTH_STEP_PMFM_ID = ColumnIdentifier.newId(
            EditProtocolSpeciesRowModel.PROPERTY_LENGTH_STEP_PMFM,
            n("tutti.editProtocol.table.header.lengthStep"),
            n("tutti.editProtocol.table.header.lengthStep.tip"));

    public static final ColumnIdentifier<EditProtocolSpeciesRowModel> WEIGHT_ENABLED = ColumnIdentifier.newId(
            EditProtocolSpeciesRowModel.PROPERTY_WEIGHT_ENABLED,
            n("tutti.editProtocol.table.header.weight"),
            n("tutti.editProtocol.table.header.weight.tip"));

    public static final ColumnIdentifier<EditProtocolSpeciesRowModel> INDIVIDUAL_OBSERVATION_ENABLED = ColumnIdentifier.newId(
            EditProtocolSpeciesRowModel.PROPERTY_INDIVIDUAL_OBSERVATION_ENABLED,
            n("tutti.editProtocol.table.header.individualObservation"),
            n("tutti.editProtocol.table.header.individualObservation.tip"));

    public static final ColumnIdentifier<EditProtocolSpeciesRowModel> MATURITY_PMFM_ID = ColumnIdentifier.newId(
            EditProtocolSpeciesRowModel.PROPERTY_MATURITY_PMFM,
            n("tutti.editProtocol.table.header.maturity"),
            n("tutti.editProtocol.table.header.maturity.tip"));

    public static final ColumnIdentifier<EditProtocolSpeciesRowModel> CALCIFIED_PIECES_SAMPLING_TYPE_PMFM_ID = ColumnIdentifier.newId(
            EditProtocolSpeciesRowModel.PROPERTY_CALCIFIED_PIECES_SAMPLING_TYPE,
            n("tutti.editProtocol.table.header.calcifiedPiecesSamplingType"),
            n("tutti.editProtocol.table.header.calcifiedPiecesSamplingType.tip"));

    public static final ColumnIdentifier<EditProtocolSpeciesRowModel> USE_RTP = ColumnIdentifier.newId(
            EditProtocolSpeciesRowModel.PROPERTY_USE_RTP,
            n("tutti.editProtocol.table.header.useRtp"),
            n("tutti.editProtocol.table.header.useRtp.tip"));

    protected final Supplier<EditProtocolSpeciesRowModel> rowSupplier;

    private static final long serialVersionUID = 1L;

    public EditProtocolSpeciesTableModel(Supplier<EditProtocolSpeciesRowModel> rowSupplier,
                                         TableColumnModelExt columnModel) {
        super(columnModel, false, false);
        this.rowSupplier = rowSupplier;
        setNoneEditableCols(SPECIES_ID);

    }

    @Override
    public EditProtocolSpeciesRowModel createNewRow() {
        return rowSupplier.get();
    }

    @Override
    protected void setValueAt(Object aValue,
                              int rowIndex,
                              int columnIndex,
                              ColumnIdentifier<EditProtocolSpeciesRowModel> propertyName,
                              EditProtocolSpeciesRowModel entry) {
        super.setValueAt(aValue, rowIndex, columnIndex, propertyName, entry);
        fireTableCellUpdated(rowIndex, columnIndex);
    }

    @Override
    protected boolean isCellEditable(int rowIndex,
                                     int columnIndex,
                                     ColumnIdentifier<EditProtocolSpeciesRowModel> propertyName) {

        boolean result = super.isCellEditable(rowIndex,
                                              columnIndex,
                                              propertyName);
        if (result) {

            if (USE_RTP.equals(propertyName) || INDIVIDUAL_OBSERVATION_ENABLED.equals(propertyName)) {

                // must have filled a species to edit this column
                EditProtocolSpeciesRowModel entry = getEntry(rowIndex);
                result = entry.withLengthStepPmfm();

            }
        }
        return result;
    }

    /**
     * Return the list of used species in the table (used to fill the
     * comparator cache for species sort)
     *
     * @return the list of used species in the table.
     * @since 2.8
     */
    public List<Species> getSpeciesList() {
        List<Species> result = Lists.newArrayList();
        for (EditProtocolSpeciesRowModel row : rows) {
            result.add(row.getSpecies());
        }
        return result;
    }

}

package fr.ifremer.tutti.ui.swing.content.protocol.actions;

/*
 * #%L
 * Tutti :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2016 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.entities.protocol.MaturityCaracteristic;
import fr.ifremer.tutti.persistence.entities.referential.Caracteristic;
import fr.ifremer.tutti.ui.swing.content.protocol.EditProtocolUI;
import fr.ifremer.tutti.ui.swing.content.protocol.maturity.EditMaturityCaracteristicPopupUI;
import fr.ifremer.tutti.ui.swing.util.actions.SimpleActionSupport;

import javax.swing.JList;
import java.util.Collection;

/**
 * @author Kevin Morin (Code Lutin)
 * @since 4.5
 */
public class EditMaturityCaracteristicAction extends SimpleActionSupport<EditProtocolUI> {

    public EditMaturityCaracteristicAction(EditProtocolUI ui) {
        super(ui);
    }

    @Override
    protected void onActionPerformed(EditProtocolUI ui) {

        EditMaturityCaracteristicPopupUI popupUI = new EditMaturityCaracteristicPopupUI(ui);
        JList selectedList = ui.getMaturityList().getSelectedList();
        Caracteristic selectedValue = (Caracteristic) selectedList.getSelectedValue();

        if (selectedValue != null) {
            MaturityCaracteristic maturityCaracteristic = ui.getModel().getMaturityCaracteristic(selectedValue.getId());
            popupUI.open(selectedValue, maturityCaracteristic);

            if (popupUI.getModel().isValid()) {

                Collection<String> matureStateIds = popupUI.getModel().getMatureStateIds();
                maturityCaracteristic.setMatureStateIds(matureStateIds);

                int selectedIndex = selectedList.getSelectedIndex();
                selectedList.repaint(selectedList.getCellBounds(selectedIndex, selectedIndex));

                ui.getValidator().doValidate();
                ui.getModel().setModify(true);

            }
        }
    }

}

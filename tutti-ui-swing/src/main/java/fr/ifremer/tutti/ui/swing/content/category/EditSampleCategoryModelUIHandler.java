package fr.ifremer.tutti.ui.swing.content.category;

/*
 * #%L
 * Tutti :: UI
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import fr.ifremer.tutti.persistence.entities.data.SampleCategoryModel;
import fr.ifremer.tutti.persistence.entities.data.SampleCategoryModels;
import fr.ifremer.tutti.persistence.entities.referential.Caracteristic;
import fr.ifremer.tutti.ui.swing.util.TuttiBeanMonitor;
import fr.ifremer.tutti.ui.swing.util.table.AbstractTuttiTableUIHandler;
import jaxx.runtime.validator.swing.SwingValidator;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jdesktop.swingx.JXTable;
import org.jdesktop.swingx.table.DefaultTableColumnModelExt;
import org.nuiton.jaxx.application.swing.util.Cancelable;
import org.nuiton.jaxx.application.swing.util.CloseableUI;

import javax.swing.JComponent;
import javax.swing.SwingUtilities;
import java.util.List;
import java.util.Set;

import static org.nuiton.i18n.I18n.t;

/**
 * Handler of ui {@link EditSampleCategoryModelUI}.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.4
 */
public class EditSampleCategoryModelUIHandler extends AbstractTuttiTableUIHandler<EditSampleCategoryModelRowModel, EditSampleCategoryModelUIModel, EditSampleCategoryModelUI> implements Cancelable, CloseableUI {

    /** Logger. */
    private static final Log log =
            LogFactory.getLog(EditSampleCategoryModelUIHandler.class);

    public EditSampleCategoryModelUIHandler() {
        super(EditSampleCategoryModelRowModel.PROPERTY_CARACTERISTIC,
              EditSampleCategoryModelRowModel.PROPERTY_LABEL,
              EditSampleCategoryModelRowModel.PROPERTY_CODE);
    }

    //------------------------------------------------------------------------//
    //-- Public methods                                                     --//
    //------------------------------------------------------------------------//

//    public void addCaracteristic() {
//        BeanFilterableComboBox<Caracteristic> keyCombo = ui.getAvailableCaracteristicsComboBox();
//        Caracteristic selectedItem = (Caracteristic) keyCombo.getSelectedItem();
//
//        getModel().removeCaracteristic(selectedItem);
//
//        EditSampleCategoryModelTableModel tableModel = getTableModel();
//
//        EditSampleCategoryModelRowModel row = tableModel.createNewRow();
//        row.setCaracteristic(selectedItem);
//
//        tableModel.addNewRow(row);
//
//        recomputeRowValidState(row);
//
//        getModel().setModify(true);
//
//        SwingUtilities.invokeLater(new Runnable() {
//            @Override
//            public void run() {
//                getTable().requestFocus();
//
//                int rowIndex = getTable().getRowCount() - 1;
//
//                SwingUtil.editCell(getTable(), rowIndex, 1);
//            }
//        });
//    }

//    public void removeEntry() {
//        int selectedRow = getTable().getSelectedRow();
//        Preconditions.checkState(selectedRow > -1);
//
//        // get selected row
//        EditSampleCategoryModelRowModel entry =
//                getTableModel().getEntry(selectedRow);
//
//        // get his caracteristic
//        Caracteristic caracteristic = entry.getCaracteristic();
//
//        // push it back to model
//        getModel().addCaracteristic(caracteristic);
//
//        // remove entry from table model
//        getTableModel().removeRow(selectedRow);
//
//        getModel().setModify(true);
//    }

//    public void moveUpEntry() {
//        int selectedRow = getTable().getSelectedRow();
//        Preconditions.checkState(selectedRow > -1);
//
//        int newRow = selectedRow - 1;
//
//        getTableModel().permuteEntry(newRow, selectedRow);
//        getTable().getSelectionModel().setSelectionInterval(newRow, newRow);
//        getModel().setModify(true);
//    }

//    public void moveDownEntry() {
//        int selectedRow = getTable().getSelectedRow();
//        Preconditions.checkState(selectedRow > -1);
//        int newRow = selectedRow + 1;
//
//        getTableModel().permuteEntry(selectedRow, newRow);
//        getTable().getSelectionModel().setSelectionInterval(newRow, newRow);
//        getModel().setModify(true);
//    }

    //------------------------------------------------------------------------//
    //-- AbstractTuttiTableUIHandler methods                                --//
    //------------------------------------------------------------------------//

    @Override
    public EditSampleCategoryModelTableModel getTableModel() {
        return (EditSampleCategoryModelTableModel) getTable().getModel();
    }

    @Override
    protected JComponent getComponentToFocus() {
        return ui.getAvailableCaracteristicsComboBox();
    }

    @Override
    public JXTable getTable() {
        return ui.getTable();
    }

    @Override
    protected boolean isRowValid(EditSampleCategoryModelRowModel row) {
        boolean result = StringUtils.isNotBlank(row.getLabel()) &&
                         StringUtils.isNotBlank(row.getCode());

        if (result) {

            // check code is alphanumerique
            String code = row.getCode();
            result = code.equals(SampleCategoryModels.getCode(code));
        }

        if (result) {

            // check unicity of codes and labels
            List<EditSampleCategoryModelRowModel> rows = getModel().getRows();
            Set<String> labels = Sets.newHashSet();

            Set<String> codes = Sets.newHashSet();

            for (EditSampleCategoryModelRowModel aRow : rows) {
                result = labels.add(aRow.getLabel());
                if (!result) {
                    if (log.isWarnEnabled()) {
                        log.warn("Duplicated label: " + aRow.getLabel());
                    }
                    break;
                }
                result = codes.add(aRow.getCode());
                if (!result) {
                    if (log.isWarnEnabled()) {
                        log.warn("Duplicated code: " + aRow.getCode());
                    }
                    break;
                }
            }
        }
        return result;
    }

    @Override
    protected void onAfterSelectedRowChanged(int oldRowIndex, EditSampleCategoryModelRowModel oldRow, int newRowIndex, EditSampleCategoryModelRowModel newRow) {
        super.onAfterSelectedRowChanged(oldRowIndex, oldRow, newRowIndex, newRow);
        recomputePopupActions();
    }

    @Override
    protected void saveSelectedRowIfRequired(TuttiBeanMonitor<EditSampleCategoryModelRowModel> rowMonitor,
                                             EditSampleCategoryModelRowModel row) {

    }

    @Override
    protected void onRowModified(int rowIndex,
                                 EditSampleCategoryModelRowModel row,
                                 String propertyName,
                                 Object oldValue,
                                 Object newValue) {
        recomputeRowValidState(row);
        saveSelectedRowIfNeeded();

        // when row valid state has changed, recompute action enabled states
        recomputePopupActions();
        getModel().setModify(true);
    }

    //------------------------------------------------------------------------//
    //-- CloseableUI methods                                                --//
    //------------------------------------------------------------------------//

    @Override
    public boolean quitUI() {
        return quitScreen(
                getModel().isValid(),
                getModel().isModify(),
                t("tutti.editProtocol.askCancelEditBeforeLeaving.cancelSaveSampleCategoryModel"),
                t("tutti.editProtocol.askSaveBeforeLeaving.saveSampleCategoryModel"),
                ui.getSaveButton().getAction()
        );
    }

    //------------------------------------------------------------------------//
    //-- AbstractTuttiUIHandler methods                                     --//
    //------------------------------------------------------------------------//

    @Override
    public void beforeInit(EditSampleCategoryModelUI ui) {

        super.beforeInit(ui);

        if (log.isDebugEnabled()) {
            log.debug("for " + ui);
        }

        List<Caracteristic> caracteristics = Lists.newArrayList(
                getPersistenceService().getAllCaracteristicForSampleCategory());

        SampleCategoryModel sampleCategoryModel =
                getDataContext().getSampleCategoryModel();

        EditSampleCategoryModelUIModel model = new EditSampleCategoryModelUIModel(
                sampleCategoryModel.getCategory(),
                caracteristics);

        ui.setContextValue(model);
    }

    @Override
    public void afterInit(EditSampleCategoryModelUI ui) {
        initUI(ui);

        EditSampleCategoryModelUIModel model = getModel();

        initBeanFilterableComboBox(ui.getAvailableCaracteristicsComboBox(), model.getCaracteristicList(), null);

        model.addPropertyChangeListener(EditSampleCategoryModelUIModel.PROPERTY_CARACTERISTIC_LIST, evt -> {
            if (log.isDebugEnabled()) {
                log.debug("propertyChange " + EditSampleCategoryModelUIModel.PROPERTY_CARACTERISTIC_LIST);
            }
            EditSampleCategoryModelUIHandler.this.ui.getAvailableCaracteristicsComboBox().setData(null);
            EditSampleCategoryModelUIHandler.this.ui.getAvailableCaracteristicsComboBox().setData((List<Caracteristic>) evt.getNewValue());
            EditSampleCategoryModelUIHandler.this.ui.getAvailableCaracteristicsComboBox().setSelectedItem(null);
        });

        JXTable table = getTable();

        // create table column model
        DefaultTableColumnModelExt columnModel =
                new DefaultTableColumnModelExt();

        {
            // Category column

            addColumnToModel(columnModel,
                             null,
                             newTableCellRender(Caracteristic.class),
                             EditSampleCategoryModelTableModel.CARACTERISTIC);


        }

        { // Code column

            addColumnToModel(columnModel,
                             null,
                             null,
                             EditSampleCategoryModelTableModel.CODE);
        }

        { // Label column

            addColumnToModel(columnModel,
                             null,
                             null,
                             EditSampleCategoryModelTableModel.LABEL);
        }

        // create table model
        EditSampleCategoryModelTableModel tableModel =
                new EditSampleCategoryModelTableModel(columnModel);

        table.setModel(tableModel);
        table.setColumnModel(columnModel);

        initTable(table);

        List<EditSampleCategoryModelRowModel> rows = model.getRows();
        tableModel.setRows(rows);

        recomputePopupActions();

        model.setModify(true);
        SwingUtilities.invokeLater(() -> getModel().setModify(false));

    }

    @Override
    protected void initTable(JXTable table) {
        super.initTable(table);
        installTableKeyListener(table.getColumnModel(), table);
    }

    @Override
    public void cancel() {

        if (log.isDebugEnabled()) {
            log.debug("Cancel UI " + ui);
        }

        // close dialog
        closeUI(ui);
    }

    @Override
    public void onCloseUI() {

        if (log.isDebugEnabled()) {
            log.debug("Closing UI " + ui);
        }
    }

    @Override
    public SwingValidator<EditSampleCategoryModelUIModel> getValidator() {
        return null;
    }

    //------------------------------------------------------------------------//
    //-- Internal methods                                                   --//
    //------------------------------------------------------------------------//

    protected void recomputePopupActions() {

        int rowIndex = getTable().getSelectedRow();

        boolean enableRemoveEntry = false;
        boolean enableMoveUpEntry = false;
        boolean enableMoveDownEntry = false;

        if (rowIndex != -1) {

            // there is a selected row
            enableRemoveEntry = true;

            enableMoveUpEntry = rowIndex > 0;

            enableMoveDownEntry = rowIndex < getTable().getModel().getRowCount() - 1;
        }
        EditSampleCategoryModelUIModel model = getModel();
        model.setRemoveEntryEnabled(enableRemoveEntry);
        model.setMoveUpEntryEnabled(enableMoveUpEntry);
        model.setMoveDownEntryEnabled(enableMoveDownEntry);
    }

}

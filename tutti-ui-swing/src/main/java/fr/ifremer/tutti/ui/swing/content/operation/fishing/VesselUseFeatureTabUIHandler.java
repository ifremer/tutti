package fr.ifremer.tutti.ui.swing.content.operation.fishing;

/*
 * #%L
 * Tutti :: UI
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.entities.CaracteristicMap;
import fr.ifremer.tutti.persistence.entities.data.FishingOperation;
import fr.ifremer.tutti.persistence.entities.protocol.TuttiProtocols;
import fr.ifremer.tutti.persistence.entities.referential.Caracteristic;
import fr.ifremer.tutti.service.DecoratorService;
import fr.ifremer.tutti.ui.swing.util.caracteristics.CaracteristicValueEditor;
import fr.ifremer.tutti.ui.swing.util.caracteristics.CaracteristicValueRenderer;
import jaxx.runtime.swing.editor.bean.BeanFilterableComboBox;
import org.jdesktop.swingx.JXTable;
import org.jdesktop.swingx.table.DefaultTableColumnModelExt;

import java.util.List;

/**
 * @author kmorin
 * @since 0.3
 */
public class VesselUseFeatureTabUIHandler extends AbstractCaracteristicTabUIHandler<VesselUseFeatureRowModel, VesselUseFeatureTabUIModel, VesselUseFeatureTableModel, VesselUseFeatureTabUI> {

    public VesselUseFeatureTabUIHandler() {
        super(VesselUseFeatureRowModel.PROPERTY_VALUE);
    }

    //------------------------------------------------------------------------//
    //-- AbstractCaracteristicTabUIHandler methods                          --//
    //------------------------------------------------------------------------//

    @Override
    protected BeanFilterableComboBox<Caracteristic> getKeyCombo() {
        return ui.getNewRowKey();
    }

    @Override
    protected VesselUseFeatureTabUIModel createModel() {
        return new VesselUseFeatureTabUIModel();
    }

    @Override
    protected CaracteristicMap getCaracteristics(FishingOperation operation) {
        return operation.getVesselUseFeatures();
    }

    @Override
    protected List<String> getProtocolPmfmIds() {
        return TuttiProtocols.vesselUseFeaturePmfmIds(getDataContext().getProtocol());
    }

    //------------------------------------------------------------------------//
    //-- AbstractTuttiTableUIHandler methods                                --//
    //------------------------------------------------------------------------//

    @Override
    public JXTable getTable() {
        return ui.getVesselUseFeatureTable();
    }

    //------------------------------------------------------------------------//
    //-- AbstractTuttiUIHandler methods                                     --//
    //------------------------------------------------------------------------//

    @Override
    public void afterInit(VesselUseFeatureTabUI ui) {
        super.afterInit(ui);

        JXTable table = getTable();

        // create table column model
        DefaultTableColumnModelExt columnModel = new DefaultTableColumnModelExt();

        {

            addColumnToModel(columnModel,
                             null,
                             newTableCellRender(Caracteristic.class, DecoratorService.CARACTERISTIC_WITH_UNIT),
                             VesselUseFeatureTableModel.KEY);
        }

        {

            addColumnToModel(columnModel,
                             new CaracteristicValueEditor(getContext()),
                             new CaracteristicValueRenderer(getContext()),
                             VesselUseFeatureTableModel.VALUE);
        }

        // create table model
        VesselUseFeatureTableModel tableModel =
                new VesselUseFeatureTableModel(columnModel);

        table.setModel(tableModel);
        table.setColumnModel(columnModel);
        initTable(table);
    }

    @Override
    protected void initTable(JXTable table) {
        super.initTable(table);
        installTableKeyListener(table.getColumnModel(), table);
    }

}

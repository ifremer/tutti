package fr.ifremer.tutti.ui.swing.content.operation.fishing.actions;

/*
 * #%L
 * Tutti :: UI
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import fr.ifremer.tutti.persistence.InvalidBatchModelException;
import fr.ifremer.tutti.persistence.ProgressionModel;
import fr.ifremer.tutti.persistence.entities.CaracteristicMap;
import fr.ifremer.tutti.persistence.entities.TuttiEntities;
import fr.ifremer.tutti.persistence.entities.data.Attachment;
import fr.ifremer.tutti.persistence.entities.data.CatchBatch;
import fr.ifremer.tutti.persistence.entities.data.CatchBatchs;
import fr.ifremer.tutti.persistence.entities.data.Cruise;
import fr.ifremer.tutti.persistence.entities.data.FishingOperation;
import fr.ifremer.tutti.persistence.entities.data.FishingOperations;
import fr.ifremer.tutti.persistence.entities.referential.Gear;
import fr.ifremer.tutti.persistence.entities.referential.GearWithOriginalRankOrders;
import fr.ifremer.tutti.persistence.entities.referential.Person;
import fr.ifremer.tutti.persistence.entities.referential.TuttiLocation;
import fr.ifremer.tutti.persistence.entities.referential.Vessel;
import fr.ifremer.tutti.service.PersistenceService;
import fr.ifremer.tutti.ui.swing.content.operation.EditFishingOperationUI;
import fr.ifremer.tutti.ui.swing.content.operation.EditFishingOperationUIHandler;
import fr.ifremer.tutti.ui.swing.content.operation.EditFishingOperationUIModel;
import fr.ifremer.tutti.ui.swing.content.operation.FishingOperationsUI;
import fr.ifremer.tutti.ui.swing.content.operation.FishingOperationsUIHandler;
import fr.ifremer.tutti.ui.swing.content.operation.FishingOperationsUIModel;
import fr.ifremer.tutti.ui.swing.content.operation.SecondaryVesselTypeEnum;
import fr.ifremer.tutti.ui.swing.content.operation.catches.EditCatchesUI;
import fr.ifremer.tutti.ui.swing.content.operation.catches.EditCatchesUIModel;
import fr.ifremer.tutti.ui.swing.content.operation.catches.actions.SaveCatchBatchAction;
import fr.ifremer.tutti.ui.swing.content.operation.catches.species.EditSpeciesBatchPanelUI;
import fr.ifremer.tutti.ui.swing.content.operation.catches.species.frequency.SpeciesFrequencyUI;
import fr.ifremer.tutti.ui.swing.content.operation.fishing.GearUseFeatureTabUI;
import fr.ifremer.tutti.ui.swing.content.operation.fishing.VesselUseFeatureTabUI;
import fr.ifremer.tutti.ui.swing.util.TuttiBeanMonitor;
import fr.ifremer.tutti.ui.swing.util.actions.LongActionSupport;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTabbedPane;
import javax.swing.SwingUtilities;
import java.awt.BorderLayout;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import static org.nuiton.i18n.I18n.t;

/**
 * To edit the given fishing operation.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.0
 */
public class EditFishingOperationAction extends LongActionSupport<FishingOperationsUIModel, FishingOperationsUI, FishingOperationsUIHandler> {

    /** Logger. */
    private static final Log log = LogFactory.getLog(EditFishingOperationAction.class);

    /**
     * The incoming fishing operation to edit.
     *
     * Can be null (means do not edit any fishing operation), or with no id
     * (means create a ne fishing operation), or with an id (means edit an
     * existing fishing operation).
     *
     * @since 1.0
     */
    protected FishingOperation fishingOperation;

    /**
     * A flag to not check if there is a previous edit.
     *
     * This flag is used when we launch the cancel action.
     *
     * @since 1.0
     */
    protected boolean checkPreviousEdit = true;

    /**
     * Delegate action to save Fising Operation.
     *
     * @since 1.0
     */
    protected SaveFishingOperationAction saveFishingOperationAction;

//    /**
//     * Delegate action to compute the weight of the operations.
//     *
//     * @since 1.1
//     */
//    protected ComputeBatchWeightsAction computeBatchWeightsAction;

    /**
     * Delegate action to save catch batch.
     *
     * @since 1.0
     */
    protected SaveCatchBatchAction saveCatchBatchAction;

    /**
     * To keep catch validation error while loading batches.
     *
     * @since 2.4
     */
    protected final List<String> errorMessages = Lists.newArrayList();

    /**
     * Flag to know if this action is part of another action.
     *
     * @since 2.4
     */
    protected boolean internalAction;

    /**
     * To recompute distance when coordinate change.
     *
     * @since 2.4
     */
    private final PropertyChangeListener coordinatePropertiesListener = new PropertyChangeListener() {

        private List<String> properties = Lists.newArrayList(
                EditFishingOperationUIModel.PROPERTY_FISHING_OPERATION_RECTILIGNE,
                EditFishingOperationUIModel.PROPERTY_GEAR_SHOOTING_END_LATITUDE,
                EditFishingOperationUIModel.PROPERTY_GEAR_SHOOTING_END_LATITUDE_DMS_SIGN,
                EditFishingOperationUIModel.PROPERTY_GEAR_SHOOTING_END_LATITUDE_DMS_DEGREE,
                EditFishingOperationUIModel.PROPERTY_GEAR_SHOOTING_END_LATITUDE_DMS_MINUTE,
                EditFishingOperationUIModel.PROPERTY_GEAR_SHOOTING_END_LATITUDE_DMS_SECOND,
                EditFishingOperationUIModel.PROPERTY_GEAR_SHOOTING_END_LATITUDE_DMD_SIGN,
                EditFishingOperationUIModel.PROPERTY_GEAR_SHOOTING_END_LATITUDE_DMD_DEGREE,
                EditFishingOperationUIModel.PROPERTY_GEAR_SHOOTING_END_LATITUDE_DMD_MINUTE,
                EditFishingOperationUIModel.PROPERTY_GEAR_SHOOTING_END_LONGITUDE,
                EditFishingOperationUIModel.PROPERTY_GEAR_SHOOTING_END_LONGITUDE_DMS_SIGN,
                EditFishingOperationUIModel.PROPERTY_GEAR_SHOOTING_END_LONGITUDE_DMS_DEGREE,
                EditFishingOperationUIModel.PROPERTY_GEAR_SHOOTING_END_LONGITUDE_DMS_MINUTE,
                EditFishingOperationUIModel.PROPERTY_GEAR_SHOOTING_END_LONGITUDE_DMS_SECOND,
                EditFishingOperationUIModel.PROPERTY_GEAR_SHOOTING_END_LONGITUDE_DMD_SIGN,
                EditFishingOperationUIModel.PROPERTY_GEAR_SHOOTING_END_LONGITUDE_DMD_DEGREE,
                EditFishingOperationUIModel.PROPERTY_GEAR_SHOOTING_END_LONGITUDE_DMD_MINUTE,
                EditFishingOperationUIModel.PROPERTY_GEAR_SHOOTING_START_LATITUDE,
                EditFishingOperationUIModel.PROPERTY_GEAR_SHOOTING_START_LATITUDE_DMS_SIGN,
                EditFishingOperationUIModel.PROPERTY_GEAR_SHOOTING_START_LATITUDE_DMS_DEGREE,
                EditFishingOperationUIModel.PROPERTY_GEAR_SHOOTING_START_LATITUDE_DMS_MINUTE,
                EditFishingOperationUIModel.PROPERTY_GEAR_SHOOTING_START_LATITUDE_DMS_SECOND,
                EditFishingOperationUIModel.PROPERTY_GEAR_SHOOTING_START_LATITUDE_DMD_SIGN,
                EditFishingOperationUIModel.PROPERTY_GEAR_SHOOTING_START_LATITUDE_DMD_DEGREE,
                EditFishingOperationUIModel.PROPERTY_GEAR_SHOOTING_START_LATITUDE_DMD_MINUTE,
                EditFishingOperationUIModel.PROPERTY_GEAR_SHOOTING_START_LONGITUDE,
                EditFishingOperationUIModel.PROPERTY_GEAR_SHOOTING_START_LONGITUDE_DMS_SIGN,
                EditFishingOperationUIModel.PROPERTY_GEAR_SHOOTING_START_LONGITUDE_DMS_DEGREE,
                EditFishingOperationUIModel.PROPERTY_GEAR_SHOOTING_START_LONGITUDE_DMS_MINUTE,
                EditFishingOperationUIModel.PROPERTY_GEAR_SHOOTING_START_LONGITUDE_DMS_SECOND,
                EditFishingOperationUIModel.PROPERTY_GEAR_SHOOTING_START_LONGITUDE_DMD_SIGN,
                EditFishingOperationUIModel.PROPERTY_GEAR_SHOOTING_START_LONGITUDE_DMD_DEGREE,
                EditFishingOperationUIModel.PROPERTY_GEAR_SHOOTING_START_LONGITUDE_DMD_MINUTE
        );

        @Override
        public void propertyChange(PropertyChangeEvent evt) {
            if (properties.contains(evt.getPropertyName())) {
                EditFishingOperationUIModel source = (EditFishingOperationUIModel) evt.getSource();
                if (source.isFishingOperationRectiligne()) {
                    source.computeDistance();
                }
            }
        }
    };

    public EditFishingOperationAction(FishingOperationsUIHandler handler) {
        super(handler, true);
        setActionDescription(t("tutti.editFishingOperation.action.editFishingOperation.tip"));
    }

    public void setFishingOperation(FishingOperation fishingOperation) {
        this.fishingOperation = fishingOperation;
        getDataContext().setFishingOperationId(fishingOperation != null ? fishingOperation.getIdAsInt() : null);
    }

    public void setCheckPreviousEdit(boolean checkPreviousEdit) {
        this.checkPreviousEdit = checkPreviousEdit;
    }

    @Override
    public void releaseAction() {
        fishingOperation = null;
        checkPreviousEdit = true;
        internalAction = false;
        super.releaseAction();
    }

    protected SaveFishingOperationAction getSaveFishingOperationAction() {
        if (saveFishingOperationAction == null) {
            saveFishingOperationAction = new SaveFishingOperationAction(getUI().getFishingOperationTabContent().getHandler());
        }
        return saveFishingOperationAction;
    }

    protected SaveCatchBatchAction getSaveCatchBatchAction() {
        if (saveCatchBatchAction == null) {
            saveCatchBatchAction = new SaveCatchBatchAction(getUI().getCatchesTabContent().getHandler());
        }
        return saveCatchBatchAction;
    }

//    protected ComputeBatchWeightsAction getComputeBatchWeightsAction() {
//        if (computeBatchWeightsAction == null) {
//            computeBatchWeightsAction = new ComputeBatchWeightsAction(getUI().getCatchesTabContent().getHandler());
//        }
//        return computeBatchWeightsAction;
//    }

    @Override
    public boolean prepareAction() {

        errorMessages.clear();

        boolean canContinue = true;
        if (checkPreviousEdit) {
            FishingOperationsUI ui = getUI();

            final FishingOperationsUIModel model = ui.getModel();

            FishingOperation editFishingOperation = model.getEditFishingOperation();

            String editFishingOperationId = null;

            if (editFishingOperation == null) {

                // no previous fishing operation in edition, can continue
                canContinue = true;
            } else {

                editFishingOperationId = editFishingOperation.getId();

                boolean create = TuttiEntities.isNew(editFishingOperation);

                boolean fishingOperationModified = getHandler().isFishingOperationModified();
                boolean catchBatchModified = getHandler().isCatchBatchModified();

                boolean fishingOperationValid = getHandler().isFishingOperationValid();
                boolean catchBatchValid = getHandler().isCatchBatchValid();

                boolean needSave = create || fishingOperationModified || catchBatchModified;


                if (needSave) {

                    boolean canSave = fishingOperationValid && catchBatchValid;

                    canContinue = false;

                    if (canSave) {

                        String message;

                        if (create) {
                            message = t("tutti.editFishingOperation.askSaveBeforeLeaving.createFishingOperation");
                        } else if (fishingOperationModified) {
                            message = t("tutti.editFishingOperation.askSaveBeforeLeaving.saveFishingOperation");
                        } else {
                            message = t("tutti.editCatchBatch.askSaveBeforeLeaving.saveCatchBatch");
                        }

                        int answer = getHandler().askSaveBeforeLeaving(message);


                        switch (answer) {
                            case JOptionPane.OK_OPTION:

                                // persist previous fishing operation
                                if (fishingOperationModified) {
                                    getSaveFishingOperationAction().setUpdateUI(false);
                                    getActionEngine().runInternalAction(getSaveFishingOperationAction());
                                }

                                if (catchBatchModified) {
                                    getSaveCatchBatchAction().setUpdateUI(false);
                                    getActionEngine().runInternalAction(getSaveCatchBatchAction());
                                }

                                canContinue = true;
                                break;

                            case JOptionPane.NO_OPTION:

                                // won't save modification
                                // so since we will edit a new operation, nothing to do here

                                canContinue = true;
                                break;
                        }
                    } else {

                        // data are not valid
                        String message;

                        if (fishingOperationValid) {
                            message = t("tutti.editCatchBatch.askCancelEditBeforeLeaving.cancelEditCatchBatch");
                        } else {
                            message = t("tutti.editFishingOperation.askCancelEditBeforeLeaving.cancelEditFishingOperation");
                        }

                        // ok will revert any modification by
                        // editing new fishing operation (if user says yes)
                        canContinue = handler.askCancelEditBeforeLeaving(message);
                    }
                }

                if (canContinue) {

                    // check can quit frequency ui (if on it)
                    if (EditCatchesUI.class.equals(getUI().getTabPane().getSelectedComponent().getClass())) {
                        // on est sur l'onglet captures

                        if (EditSpeciesBatchPanelUI.class.equals(getUI().getCatchesTabContent().getTabPane().getSelectedComponent().getClass())) {

                            // on est sur l'onglet espèce ou benthos
                            EditSpeciesBatchPanelUI speciesOrBenthosUI = (EditSpeciesBatchPanelUI) getUI().getCatchesTabContent().getTabPane().getSelectedComponent();
                            if (EditSpeciesBatchPanelUI.EDIT_FREQUENCY_CARD.equals(speciesOrBenthosUI.getTopPanelLayout().getSelected())) {

                                // on est sur l'écran des mensurations / observations individuelles
                                SpeciesFrequencyUI editFrequenciesUI = speciesOrBenthosUI.getEditFrequenciesUI();

                                canContinue = editFrequenciesUI.leaveIfConfirmed();

                            }
                        }
                    }
                }
            }

            if (!canContinue) {

                FishingOperation selectFishingOperation;
                if (TuttiEntities.isNew(editFishingOperation)) {

                    // no selection
                    selectFishingOperation = null;
                } else {
                    // rollback selected fishing operation
                    selectFishingOperation =
                            model.getFishingOperation(editFishingOperationId);
                }

                model.setEditionAdjusting(true);
                try {
                    model.setSelectedFishingOperation(selectFishingOperation);
                } finally {
                    model.setEditionAdjusting(false);
                }
            }
        }

        if (canContinue) {

            // always close attachments
            FishingOperationsUI ui = getUI();
            ui.getCatchesTabContent().getHandler().closeAttachments();
            ui.getFishingOperationTabContent().getFishingOperationAttachmentsButton().onCloseUI();
        }

        return canContinue;
    }

    @Override
    public void doAction() throws Exception {

        errorMessages.clear();

        FishingOperationsUI ui = getUI();

        FishingOperationsUIModel model = ui.getModel();

        // edit new fishing operation
        if (log.isDebugEnabled()) {
            log.debug("Edit fishingOperation: " + fishingOperation);
        }

        // now fishing operation is edited
        model.setEditFishingOperation(fishingOperation);

        loadFishingOperation(fishingOperation);

        loadCatchBatch(fishingOperation);

        JTabbedPane form = ui.getTabPane();
        JLabel noContentPane = ui.getNoTraitPane();

        if (fishingOperation == null) {

            // nothing to display

            // close fishing operation ui
            ui.getFishingOperationTabContent().getHandler().onCloseUI();
            // close catch ui
            ui.getCatchesTabContent().getHandler().onCloseUI();

            ui.remove(form);

            // just display <no trait!>
            ui.add(noContentPane, BorderLayout.CENTER);

        } else {

            ui.remove(noContentPane);

            // wait last minute to display (avoid dirty display effects)
            ui.add(form, BorderLayout.CENTER);

            ui.getFishingOperationTabContent().getFishingOperationTabPane().setSelectedIndex(0);

            if (checkPreviousEdit) {

                ui.getCatchesTabContent().getModel().setDoNotCheckLeavingFrequencyScreen(true);

                try {
                    ui.getTabPane().setSelectedIndex(0);
                } finally {

                    ui.getCatchesTabContent().getModel().setDoNotCheckLeavingFrequencyScreen(false);
                }
            }
        }

        model.addPropertyChangeListener(coordinatePropertiesListener);
    }

    @Override
    public void postSuccessAction() {
        super.postSuccessAction();

        // always go back to main content (see https://forge.codelutin.com/issues/5705)
        getUI().getCatchesTabContent().getSpeciesTabPanel().switchToEditBatch();
        getUI().getCatchesTabContent().getBenthosTabPanel().switchToEditBatch();

        final EditFishingOperationUI fishingOperationTabContent = getUI().getFishingOperationTabContent();
        fishingOperationTabContent.getModel().setModify(false);

        SwingUtilities.invokeLater(
                () -> {

                    JComponent componentToFocus = fishingOperationTabContent.getHandler().getComponentToFocus();
                    componentToFocus.requestFocusInWindow();

                }
        );

        getUI().repaint();

    }

//    public void displayValidationErrors() {
//        if (CollectionUtils.isNotEmpty(errorMessages)) {
//            for (String errorMessage : errorMessages) {
//                getContext().getErrorHelper().showErrorDialog(errorMessage, null);
//            }
//        }
//    }

    public void loadFishingOperation(FishingOperation bean) {

        EditFishingOperationUI ui = getUI().getFishingOperationTabContent();

        EditFishingOperationUIHandler handler = ui.getHandler();

        EditFishingOperationUIModel editFishingOperationUIModel = ui.getModel();

        editFishingOperationUIModel.setLoadingData(true);

        editFishingOperationUIModel.removeAllAttachment(editFishingOperationUIModel.getAttachment());

        handler.uninstallStartDateListener();
        handler.uninstallCoordinatesListener();

        if (bean == null) {

            editFishingOperationUIModel.fromEntity(
                    FishingOperations.newFishingOperation());

            editFishingOperationUIModel.setFishingOperation(null);
            editFishingOperationUIModel.setAllSecondaryVessel(null);
            editFishingOperationUIModel.setFishingSecondaryVessel(null);
            editFishingOperationUIModel.setScientificSecondaryVessel(null);
            editFishingOperationUIModel.setOnlyCruisSecondaryVessel(null);
            editFishingOperationUIModel.setSecondaryVesselType(null);

            handler.clearValidators();

            handler.resetAllModels();

        } else { //if (!bean.equals(editFishingOperationUIModel.getFishingOperation()) || handler.isAModelModified()) {

            TuttiLocation strata = bean.getStrata();
            TuttiLocation subStrata = bean.getSubStrata();
            TuttiLocation location = bean.getLocation();

            PersistenceService persistenceService = getContext().getPersistenceService();

            Cruise cruise = bean.getCruise();
            if (cruise != null) {

                // update gear universe
                // add a equals on id + rankOrder
                List<Gear> gears = Lists.newArrayList();
                for (Gear gear : cruise.getGear()) {
                    CaracteristicMap caracteristics =
                            persistenceService.getGearCaracteristics(
                                    cruise.getIdAsInt(),
                                    gear.getIdAsInt(),
                                    gear.getRankOrder());

                    Gear toKeep = GearWithOriginalRankOrders.newGearWithOriginalRankOrder(gear);
                    toKeep.setCaracteristics(caracteristics);

                    gears.add(toKeep);
                }
                ui.getGearComboBox().setData(gears);
            }

            editFishingOperationUIModel.fromEntity(bean);

            // to be sure combo list will be reloaded
            editFishingOperationUIModel.setStrata(null);
            editFishingOperationUIModel.setSubStrata(null);
            editFishingOperationUIModel.setLocation(null);

            editFishingOperationUIModel.convertGearShootingCoordinatesFromDD();

            if (strata != null) {
                ui.getStrataComboBox().setSelectedItem(strata);
            }

            if (subStrata != null) {
                ui.getSubStrataComboBox().setSelectedItem(subStrata);
            }

            if (location != null) {
                ui.getLocationComboBox().setSelectedItem(location);
            }

            editFishingOperationUIModel.setFishingOperation(bean);

            // update saisisseur selection
            List<Person> saisisseur = editFishingOperationUIModel.getRecorderPerson();
            ui.getRecorderPersonList().getHandler().setSelected(saisisseur);

            //reset gear use feature tab
            GearUseFeatureTabUI gearUseFeatureTabContent = ui.getGearUseFeatureTabContent();
            gearUseFeatureTabContent.getHandler().reset(bean);

            //reset vessel use feature tab
            VesselUseFeatureTabUI vesselUseFeatureTabContent = ui.getVesselUseFeatureTabContent();
            vesselUseFeatureTabContent.getHandler().reset(bean);

            Integer objectId = editFishingOperationUIModel.getObjectId();
            if (objectId != null) {

                List<Attachment> attachments =
                        persistenceService.getAllAttachments(
                                editFishingOperationUIModel.getObjectType(),
                                objectId);

                editFishingOperationUIModel.addAllAttachment(attachments);
            }

            Vessel cruiseVessel = getDataContext().getCruise().getVessel();
            List<Vessel> scientificVessels = getDataContext().getScientificVessels();
            List<Vessel> fishingVessels = getDataContext().getFishingVessels();

            {  // update all secondary vessel universe
                List<Vessel> vessels = Lists.newArrayList();
                vessels.addAll(scientificVessels);
                vessels.addAll(fishingVessels);
                vessels.remove(cruiseVessel);
                editFishingOperationUIModel.setAllSecondaryVessel(vessels);
            }

            {  // update scientific secondary vessel universe
                List<Vessel> vessels = Lists.newArrayList();
                vessels.addAll(scientificVessels);
                vessels.remove(cruiseVessel);
                editFishingOperationUIModel.setScientificSecondaryVessel(vessels);
            }

            {  // update fishing secondary vessel universe
                List<Vessel> vessels = Lists.newArrayList();
                vessels.addAll(fishingVessels);
                vessels.remove(cruiseVessel);
                editFishingOperationUIModel.setFishingSecondaryVessel(vessels);
            }

            { // update only cruise vessel universe

                List<FishingOperation> fishingOperation =
                        getModel().getFishingOperation();

                Set<Vessel> vesselSet = Sets.newHashSet();
                for (FishingOperation operation : fishingOperation) {
                    List<Vessel> secondaryVessel = persistenceService.getFishingOperationSecondaryVessel(operation.getIdAsInt());
                    if (CollectionUtils.isNotEmpty(secondaryVessel)) {
                        vesselSet.addAll(secondaryVessel);
                    }
                }
                List<Vessel> vessel = Lists.newArrayList(vesselSet);
                editFishingOperationUIModel.setOnlyCruisSecondaryVessel(vessel);
            }

            // will propagate to model
            ui.getSecondaryVesselList().getModel().setSelected(bean.getSecondaryVessel());

            editFishingOperationUIModel.setSecondaryVesselType(SecondaryVesselTypeEnum.ALL);

            editFishingOperationUIModel.setModify(false);
            handler.getFishingOperationMonitor().clearModified();
            handler.registerValidator();
        }

        editFishingOperationUIModel.setLoadingData(false);

        handler.installStartDateListener();
        handler.installCoordinatesListener();

    }

    public void loadCatchBatch(FishingOperation bean) {

        boolean empty = bean == null || TuttiEntities.isNew(bean);

        EditCatchesUI ui = getUI().getCatchesTabContent();

        ui.getHandler().uninstallTotalRejectWeightListener();

        TuttiBeanMonitor<EditCatchesUIModel> catchBatchMonitor =
                ui.getHandler().getCatchBatchMonitor();

        EditCatchesUIModel catchesUIModel = ui.getModel();

        catchesUIModel.setLoadingData(true);

        catchesUIModel.reset();

        CatchBatch batch;

        List<Attachment> attachments;

        boolean catchEnabled;

        Integer operationId = bean == null ? null : bean.getIdAsInt();

        ProgressionModel progressionModel = getProgressionModel();
        if (progressionModel == null) {
            progressionModel = new ProgressionModel();
            setProgressionModel(progressionModel);
        }
        progressionModel.adaptTotal(empty ? 1 : 6);

        String validationErrorMessage = null;

        if (empty) {

            // create a new CatchBatch
            progressionModel.increments("Create new catch batch");
            if (log.isDebugEnabled()) {
                log.debug("Create a new CatchBatch (fishing operation is null)");
            }
            batch = CatchBatchs.newCatchBatch();
            batch.setFishingOperation(bean);

            attachments = Collections.emptyList();

            catchEnabled = true;

        } else {

            if (log.isDebugEnabled()) {
                log.debug("Load existing CatchBatch from operation id: " +
                          operationId);
            }

            PersistenceService persistenceService =
                    getContext().getPersistenceService();

            progressionModel.increments("Chargement de la capture");

            boolean withCatchBath =
                    persistenceService.isFishingOperationWithCatchBatch(
                            operationId);

            if (withCatchBath) {

                // load it
                try {
                    batch = persistenceService.getCatchBatchFromFishingOperation(operationId);
                    batch.setFishingOperation(bean);
                    Integer objectId = batch.getIdAsInt();
                    attachments = persistenceService.getAllAttachments(catchesUIModel.getObjectType(), objectId);
                    catchEnabled = true;

                } catch (InvalidBatchModelException e) {

                    // batch is not compatible with Tutti
                    if (log.isDebugEnabled()) {
                        log.debug("Invalid batch model", e);
                    }
                    batch = null;
                    attachments = Collections.emptyList();

                    validationErrorMessage = t("tutti.fishingOperations.warn.catchBatch.invalidSampleCategoryModel");

                    getModel().setValidationErrorMessage(validationErrorMessage);

                    catchEnabled = false;
                }
            } else {

                // no catch batch
                batch = null;
                attachments = Collections.emptyList();

                catchEnabled = false;

                validationErrorMessage = t("tutti.fishingOperations.warn.catchBatch.notFound");
            }
        }

        setCatch(attachments,
                 catchesUIModel,
                 catchBatchMonitor,
                 batch,
                 catchEnabled);

        // 4) Propagate new selected fishingoperation to others tabs

        FishingOperation operationToLoad = batch == null ? null : bean;

        boolean catchValid = true;

        try {
            progressionModel.increments("Chargement des captures Espèces");

            ui.getSpeciesTabPanel().getEditBatchesUI().getHandler().selectFishingOperation(operationToLoad);
        } catch (InvalidBatchModelException e) {

            // invalid sample category model for species batches
            if (log.isDebugEnabled()) {
                log.debug("Invalid sample category model for species batches", e);
            }
            errorMessages.add(e.getMessage());
            catchValid = false;
        }

        try {
            progressionModel.increments("Chargement des captures Benthos");
            ui.getBenthosTabPanel().getEditBatchesUI().getHandler().selectFishingOperation(operationToLoad);
        } catch (InvalidBatchModelException e) {

            // invalid sample category model for benthos batches
            if (log.isDebugEnabled()) {
                log.debug("Invalid sample category model for benthos batches", e);
            }
            errorMessages.add(e.getMessage());
            catchValid = false;
        }

        if (!errorMessages.isEmpty()) {

            StringBuilder message = new StringBuilder();
            for (String errorMessage : errorMessages) {
                message.append("<li>").append(errorMessage).append("</li>");
            }
            validationErrorMessage = t("tutti.fishingOperations.warn.invalid.batch", message.toString());

        }

        getModel().setValidationErrorMessage(validationErrorMessage);

        if (catchValid) {

            // load other tabs

            progressionModel.increments("Chargmenent des macro-déchets");
            ui.getMarineLitterTabContent().getHandler().selectFishingOperation(operationToLoad);

            progressionModel.increments("Chargement des captures accidentelles");
            ui.getAccidentalTabContent().getHandler().selectFishingOperation(operationToLoad);

        } else {

            // remove catch
            setCatch(Collections.<Attachment>emptyList(),
                     catchesUIModel,
                     catchBatchMonitor,
                     null,
                     false);
        }

        ui.getHandler().installTotalRejectWeightListener();

        catchesUIModel.setLoadingData(false);
    }

    @Override
    public void postFailedAction(Throwable error) {
        super.postFailedAction(error);
    }


    protected void setCatch(List<Attachment> attachments,
                            EditCatchesUIModel catchesUIModel,
                            TuttiBeanMonitor<EditCatchesUIModel> catchBatchMonitor,
                            CatchBatch batch,
                            boolean catchEnabled) {

        getModel().setCatchEnabled(catchEnabled);
//        getModel().setCatchNotFound(catchNotFound);
//        getModel().setSampleCatchModelValid(sampleModelValid);

        catchesUIModel.fromEntity(batch);
        catchesUIModel.addAllAttachment(attachments);

        catchesUIModel.setModify(false);
        catchBatchMonitor.clearModified();
    }

    public void setInternalAction(boolean internalAction) {
        this.internalAction = internalAction;
    }
}

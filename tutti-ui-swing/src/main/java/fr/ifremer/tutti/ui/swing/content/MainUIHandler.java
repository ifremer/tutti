package fr.ifremer.tutti.ui.swing.content;

/*
 * #%L
 * Tutti :: UI
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import fr.ifremer.tutti.caliper.feed.CaliperFeedReader;
import fr.ifremer.tutti.ichtyometer.feed.IchtyometerFeedReader;
import fr.ifremer.tutti.persistence.entities.data.Cruise;
import fr.ifremer.tutti.persistence.entities.data.Program;
import fr.ifremer.tutti.persistence.entities.protocol.TuttiProtocol;
import fr.ifremer.tutti.ui.swing.RunTutti;
import fr.ifremer.tutti.ui.swing.TuttiScreen;
import fr.ifremer.tutti.ui.swing.TuttiUIContext;
import fr.ifremer.tutti.ui.swing.content.category.EditSampleCategoryModelUI;
import fr.ifremer.tutti.ui.swing.content.config.TuttiConfigUI;
import fr.ifremer.tutti.ui.swing.content.cruise.EditCruiseUI;
import fr.ifremer.tutti.ui.swing.content.cruise.EditCruiseUIHandler;
import fr.ifremer.tutti.ui.swing.content.db.DbManagerUI;
import fr.ifremer.tutti.ui.swing.content.db.DbManagerUIHandler;
import fr.ifremer.tutti.ui.swing.content.genericformat.GenericFormatExportUI;
import fr.ifremer.tutti.ui.swing.content.genericformat.GenericFormatImportUI;
import fr.ifremer.tutti.ui.swing.content.home.SelectCruiseUI;
import fr.ifremer.tutti.ui.swing.content.operation.FishingOperationsUI;
import fr.ifremer.tutti.ui.swing.content.program.EditProgramUI;
import fr.ifremer.tutti.ui.swing.content.program.EditProgramUIHandler;
import fr.ifremer.tutti.ui.swing.content.protocol.EditProtocolUI;
import fr.ifremer.tutti.ui.swing.content.protocol.EditProtocolUIHandler;
import fr.ifremer.tutti.ui.swing.content.referential.ManageTemporaryReferentialUI;
import fr.ifremer.tutti.ui.swing.content.report.ReportUI;
import fr.ifremer.tutti.ui.swing.content.validation.ValidateCruiseUI;
import fr.ifremer.tutti.ui.swing.util.AbstractTuttiUIHandler;
import fr.ifremer.tutti.ui.swing.util.RemoveablePropertyChangeListener;
import fr.ifremer.tutti.ui.swing.util.TuttiUI;
import fr.ifremer.tutti.ui.swing.util.TuttiUIUtil;
import jaxx.runtime.JAXXBinding;
import jaxx.runtime.SwingUtil;
import jaxx.runtime.validator.swing.SwingValidator;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.jaxx.application.swing.action.ApplicationActionUI;
import org.nuiton.jaxx.application.swing.util.CloseableUI;

import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JToolBar;
import java.awt.Cursor;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeListenerProxy;
import java.util.Locale;

import static org.nuiton.i18n.I18n.t;

/**
 * @author Tony Chemit - chemit@codelutin.com
 * @since 0.1
 */
public class MainUIHandler extends AbstractTuttiUIHandler<TuttiUIContext, MainUI> {

    /** Logger. */
    private static final Log log = LogFactory.getLog(MainUIHandler.class);

    protected JComponent currentBody;

    public void reloadDbManagerText() {

        TuttiUI<?, ?> body = (TuttiUI<?, ?>) currentBody;

        if (body != null && body.getHandler() instanceof DbManagerUIHandler) {
            DbManagerUIHandler dbManagerUIHandler = (DbManagerUIHandler) body.getHandler();
            dbManagerUIHandler.updateMessage();
        }
    }

    public String getIchtyometerStatusTip(boolean connected) {
        String result;
        if (connected) {
            IchtyometerFeedReader ichtyometerReader = getModel().getIchtyometerReader();
            result = t("tutti.ichtyometer.status.connected.tip", ichtyometerReader.getClientName());
        } else {
            result = t("tutti.ichtyometer.status.not.connected.tip");
        }
        return result;
    }

    public String getCaliperStatusLabel(boolean connected) {
        String result = null;
        if (connected) {
            CaliperFeedReader caliperReader = getModel().getCaliperReader();
            result = caliperReader.getSerialPortName();
        }
        return result;
    }

    public String getCaliperStatusTip(boolean connected) {
        String result;
        if (connected) {
            CaliperFeedReader caliperReader = getModel().getCaliperReader();
            result = t("tutti.caliper.status.connected.tip", caliperReader.getSerialPortName());
        } else {
            result = t("tutti.caliper.status.not.connected.tip");
        }
        return result;
    }
    //------------------------------------------------------------------------//
    //-- AbstractTuttiUIHandler methods                                     --//
    //------------------------------------------------------------------------//

    @Override
    public void beforeInit(MainUI ui) {
        super.beforeInit(ui);
        TuttiUIContext context = getContext();
        ui.setContextValue(context);
        context.setMainUI(ui);
        context.setActionUI(new ApplicationActionUI(ui, context));
        context.addPropertyChangeListener(new RemoveablePropertyChangeListener() {
            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                String propertyName = evt.getPropertyName();
                if (TuttiUIContext.PROPERTIES_TO_SAVE.contains(propertyName)) {

                    // reset cruise in case the listener which is supposed to reset
                    // the cruise is called after this one (cf #2276)
                    if (TuttiUIContext.PROPERTY_CRUISE_ID.equals(propertyName)) {
                        getDataContext().resetCruise();
                    }
                    // reset protocol in case the listener which is supposed to reset
                    // the protocol is called after this one (cf #2276)
                    if (TuttiUIContext.PROPERTY_PROTOCOL_ID.equals(propertyName)) {
                        getDataContext().resetProtocol();
                    }
                    // change the ui title
                    changeTitle();

                } else if (propertyName.equals(TuttiUIContext.PROPERTY_SCREEN)) {
                    setScreen((TuttiScreen) evt.getNewValue());
                }
            }
        });
        ui.setContextValue(ui, MainUI.class.getName());

        // ecoute des changements de l'état busy
        context.addPropertyChangeListener(TuttiUIContext.PROPERTY_BUSY, new RemoveablePropertyChangeListener() {

            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                Boolean newvalue = (Boolean) evt.getNewValue();
                updateBusyState(newvalue != null && newvalue);
            }
        });

        // ecoute des changements de l'état busy
        context.addPropertyChangeListener(TuttiUIContext.PROPERTY_HIDE_BODY, new RemoveablePropertyChangeListener() {

            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                Boolean newvalue = (Boolean) evt.getNewValue();
                if (getUI() != null && getUI().getBody() != null) {
                    getUI().getBody().setVisible(newvalue != null && newvalue);
                }
            }
        });
    }

    protected void updateBusyState(boolean busy) {
        if (busy) {
            // ui bloquee
            if (log.isDebugEnabled()) {
                log.debug("block ui in busy mode");
            }
            ui.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
        } else {
            // ui debloquee
            if (log.isDebugEnabled()) {
                log.debug("unblock ui in none busy mode");
            }
            ui.setCursor(Cursor.getDefaultCursor());
        }
    }

    @Override
    public void afterInit(MainUI ui) {

        initUI(ui);

        // installation layer de blocage en mode busy
        SwingUtil.setLayerUI(ui.getBody(), ui.getBusyBlockLayerUI());

        //FIXME-TC See why this binding is not setted ?
        ui.applyDataBinding(MainUI.BINDING_MENU_CHANGE_LOCALE_FR_ENABLED);
        ui.applyDataBinding(MainUI.BINDING_MENU_CHANGE_LOCALE_UK_ENABLED);

        // Init SwingSession
        getContext().addInSwingSession(ui, false);

        changeTitle();

        JToolBar bar = ui.getBottomBar();
        ui.getStatus().addWidget(bar, 0);
    }

    @Override
    protected JComponent getComponentToFocus() {
        return currentBody;
    }

    @Override
    public void onCloseUI() {

        TuttiUIContext context = getContext();

        // remove any screen
        context.setScreen(null);
        context.removeMessageNotifier(this);

        // clean context

        PropertyChangeListener[] propertyChangeListeners =
                context.getPropertyChangeListeners();
        for (PropertyChangeListener listener : propertyChangeListeners) {
            if (listener instanceof PropertyChangeListenerProxy) {
                PropertyChangeListenerProxy proxy = (PropertyChangeListenerProxy) listener;
                listener = proxy.getListener();
            }
            if (listener instanceof RemoveablePropertyChangeListener) {
                if (log.isDebugEnabled()) {
                    log.debug("Remove listener: " + listener);
                }
                context.removePropertyChangeListener(listener);
            }
        }

        if (ui != null) {

            // clean ui

            JAXXBinding[] bindings = ui.getDataBindings();
            for (JAXXBinding binding : bindings) {
                SwingUtil.removeDataBinding(ui, binding.getId());
            }
            ui.setVisible(false);
            ui.dispose();
        }

    }

    @Override
    public SwingValidator<TuttiUIContext> getValidator() {
        return null;
    }

    //------------------------------------------------------------------------//
    //-- Public methods                                                     --//
    //------------------------------------------------------------------------//

    public void reloadUI() {

        //close ui
        onCloseUI();

        // restart ui
        RunTutti.startTutti(getContext(), false);
    }

    public boolean acceptLocale(Locale l, String expected) {
        return l != null && l.toString().equals(expected);
    }

//    public void changeLocale(Locale locale) {
//
//        // change locale (and save configuration)
//        getModel().setLocale(locale);
//
//        // change i18n locale
//        I18n.setDefaultLocale(getConfig().getI18nLocale());
//
//        // reload decorator service (TODO Check if this is necessary)
//        getContext().reloadDecoratorService();
//
//        // close reload
//        reloadUI();
//    }

//    public void gotoSite() {
//        TuttiConfiguration config = getConfig();
//
//        URL siteURL = config.getSiteUrl();
//
//        if (log.isDebugEnabled()) {
//            log.debug("goto " + siteURL);
//        }
//        TuttiUIUtil.openLink(siteURL);
//    }

//    public void showHelp() {
//        getModel().showHelp(ui, ui.getBroker(), null);
//    }

    @Override
    public final void showInformationMessage(String message) {
        ui.getStatus().setStatus("<html><body>" + message + "</body></html>");
    }

    public void registerValidator(SwingValidator validator) {
        if (log.isInfoEnabled()) {
            log.info("Register validator: " + validator.getType() + " - " + validator.getContext() + " (" + validator + ")");
        }
        ui.getValidatorMessageWidget().registerValidator(validator);
    }

    @Override
    public void clearValidators() {
        if (log.isInfoEnabled()) {
            log.info("Clean all registred validators.");
        }
        ui.getValidatorMessageWidget().clearValidators();
    }

    public boolean quitCurrentScreen() {

        boolean canClose;
        if (getContext().getScreen() == null || currentBody == null) {

            // no screen, surely can quit
            canClose = true;
            if (log.isWarnEnabled()) {
                log.warn("==================================================");
                log.warn("No screen, Should then skipCheckCurrent in action.");
                log.warn("==================================================");
            }
        } else {
            TuttiUI<?, ?> body = (TuttiUI<?, ?>) currentBody;
            Preconditions.checkNotNull(currentBody);
            AbstractTuttiUIHandler<?, ?> handler = body.getHandler();
            if (handler instanceof CloseableUI) {

                // try to quit UI
                canClose = ((CloseableUI) handler).quitUI();
            } else {

                // can always close ui
                canClose = true;
            }
        }
        return canClose;
    }

    //------------------------------------------------------------------------//
    //-- Internal methods                                                   --//
    //------------------------------------------------------------------------//

    protected void setScreen(TuttiScreen screen) {

        TuttiUIContext context = getContext();

        // close current body (if any)
        if (currentBody != null) {
            TuttiUI<?, ?> body = (TuttiUI<?, ?>) currentBody;
            body.getHandler().onCloseUI();

            context.saveSwingSession();

            ui.getBody().remove(currentBody);

            currentBody = null;
        }

        if (screen != null) {

            // load new body

            JComponent screenUI;
            JToolBar rightDecoration = null;
            String screenTitle;

            Icon icon;
            switch (screen) {
                case CONFIG:

                    screenUI = new TuttiConfigUI(ui);
                    screenTitle = t("tutti.config.title");
                    icon = ui.getMenuFileConfiguration().getIcon();
                    break;

                case EDIT_SAMPLE_CATEGORY_MODEL:

                    screenUI = new EditSampleCategoryModelUI(ui);
                    screenTitle = t("tutti.editSampleCategoryModel.title");
                    icon = ui.getMenuEditSampleCategoryModel().getIcon();
                    break;
                default:
                case MANAGE_DB:

                    screenUI = new DbManagerUI(ui);
                    screenTitle = t("tutti.dbMabager.title");
                    icon = ui.getMenuFileManageDb().getIcon();
                    break;

                case SELECT_CRUISE:

                    screenUI = new SelectCruiseUI(ui);
                    screenTitle = t("tutti.selectCruise.title");
                    icon = ui.getMenuActionSelectCruise().getIcon();
                    break;

                case EDIT_PROGRAM:

                    screenTitle = EditProgramUIHandler.getTitle(
                            context.isProgramFilled());

                    screenUI = new EditProgramUI(ui);
                    icon = ui.getMenuActionEditProgram().getIcon();
                    break;

                case EDIT_CRUISE:

                    screenTitle = EditCruiseUIHandler.getTitle(
                            context.isCruiseFilled());

                    screenUI = new EditCruiseUI(ui);
                    rightDecoration = ((EditCruiseUI) screenUI).getTopToolBar();
                    Cruise cruise = context.getDataContext().getCruise();
                    icon = TuttiUIUtil.getCruiseIcon(cruise == null ? ((EditCruiseUI) screenUI).getModel() : cruise);
                    break;

                case EDIT_PROTOCOL:

                    screenTitle = EditProtocolUIHandler.getTitle(
                            context.isProtocolFilled());

                    screenUI = new EditProtocolUI(ui);
                    icon = ui.getMenuActionEditProtocol().getIcon();
                    break;

                case EDIT_FISHING_OPERATION:

                    screenTitle = t("tutti.fishingOperations.title.edit.operations", getSelectedCruiseTitle());
                    icon = ui.getMenuActionEditCatches().getIcon();
                    screenUI = new FishingOperationsUI(ui);

                    break;

                case IMPORT_TEMPORARY_REFERENTIAL:

                    screenTitle = t("tutti.manageTemporaryReferential.title");
                    screenUI = new ManageTemporaryReferentialUI(ui);
                    icon = ui.getMenuImportTemporaryReferential().getIcon();
                    break;

                case VALIDATE_CRUISE:

                    screenUI = new ValidateCruiseUI(ui);
                    screenTitle = t("tutti.fishingOperations.title.validate.operations", getSelectedCruiseTitle());
                    icon = ui.getMenuActionValidateCatches().getIcon();
                    break;

                case REPORT:

                    screenUI = new ReportUI(ui);
                    screenTitle = t("tutti.report.title");
                    icon = ui.getMenuActionGenerateCruiseReport().getIcon();
                    break;

                case GENERIC_FORMAT_IMPORT:

                    screenUI = new GenericFormatImportUI(ui);
                    screenTitle = t("tutti.genericFormatImport.title");
                    icon = ui.getMenuActionGenericFormatImport().getIcon();
                    break;

                case GENERIC_FORMAT_EXPORT:

                    screenUI = new GenericFormatExportUI(ui);
                    screenTitle = t("tutti.genericFormatExport.title");
                    icon = ui.getMenuActionGenericFormatExport().getIcon();
                    break;
            }

            JButton showHelp = ui.getShowHelp();
            if (rightDecoration == null) {
                rightDecoration = new JToolBar();
                rightDecoration.setFloatable(false);
                rightDecoration.setOpaque(false);
                rightDecoration.setBorderPainted(false);
            } else {
                rightDecoration.remove(showHelp);
            }
            rightDecoration.add(showHelp, 0);
            this.currentBody = screenUI;
            context.addInSwingSession(currentBody, true);
            ui.getBody().setTitle(screenTitle);
            ui.getBody().add(currentBody);
            ui.getBody().setLeftDecoration(new JLabel(icon));
            ui.getBody().setRightDecoration(rightDecoration);
            // Fix #2510: [AIDE] perte de l'aide contextuelle sur ecran campagne
            //FIXME tchemit-2013-05-28 Find out why
            ui.getBody().getRightDecoration().setVisible(true);

        }
    }

    public void changeTitle() {

        String title = getSelectedCruiseTitle();

        ui.setTitle(t("tutti.main.title.application",
                      getConfig().getVersion(),
                      title));
    }

    protected String getSelectedCruiseTitle() {

        TuttiUIContext context = getContext();

        String title;

        if (context.isDbLoaded()) {

            if (context.isProgramFilled()) {

                // selected program

                Program program = getDataContext().getProgram();

                title = t("tutti.main.title.selectedProgram", program.getName()) + " / ";

                if (context.isCruiseFilled()) {

                    // selected cruise
                    Cruise cruise = getDataContext().getCruise();

                    if (cruise != null) {
                        title += t("tutti.main.title.selectedCruise", cruise.getName());
                    }
                } else {

                    // no selected cruise

                    title += t("tutti.main.title.noSelectedCruise");
                }

            } else {

                // no program selected (so neither cruise)

                title = t("tutti.main.title.noSelectedProgram");

            }

            title += " / ";

            if (context.isProtocolFilled()) {

                // selected protocol
                TuttiProtocol protocol = getDataContext().getProtocol();

                title += t("tutti.main.title.selectedProtocol", protocol.getName());
            } else {

                // no selected protocol

                title += t("tutti.main.title.noSelectedProtocol");
            }
        } else {

            // no db loaded

            title = t("tutti.main.title.nodb");
        }
        return title;
    }

    public void setBodyTitle(String title) {
        ui.getBody().setTitle(title);
    }

    public JComponent getCurrentBody() {
        return currentBody;
    }
}

package fr.ifremer.tutti.ui.swing.content.protocol.zones.actions;

/*
 * #%L
 * Tutti :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2016 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.ui.swing.content.protocol.EditProtocolUIModel;
import fr.ifremer.tutti.ui.swing.content.protocol.zones.tree.node.StrataNode;
import fr.ifremer.tutti.ui.swing.content.protocol.zones.tree.node.SubStrataNode;
import fr.ifremer.tutti.ui.swing.content.protocol.zones.ZoneEditorUI;
import fr.ifremer.tutti.ui.swing.util.actions.SimpleActionSupport;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.swing.JTree;
import javax.swing.tree.TreePath;
import java.util.HashSet;
import java.util.Set;

/**
 * @author Kevin Morin (Code Lutin)
 * @since 4.5
 */
public class RemoveStratasAction extends SimpleActionSupport<ZoneEditorUI> {

    /** Logger. */
    private static final Log log = LogFactory.getLog(RemoveStratasAction.class);

    public RemoveStratasAction(ZoneEditorUI zoneEditorUI) {
        super(zoneEditorUI);
        setEnabled(false);
    }

    @Override
    protected void onActionPerformed(ZoneEditorUI zoneEditorUI) {

        JTree zonesTree = zoneEditorUI.getZonesTree();
        TreePath[] selectedStratas = zonesTree.getSelectionPaths();

        if (selectedStratas == null) {

            if (log.isInfoEnabled()) {
                log.info("No selected stratas, nor subStratas selected");
            }
            return;
        }

        // récupération des strates à supprimer
        Set<StrataNode> stratasNodeToRemove = getStrataNodesToRemove(selectedStratas);

        // récupération des sous-strates à supprimer
        Set<SubStrataNode> subStratasNodeToRemove = getSubStrataNodesToRemove(selectedStratas, stratasNodeToRemove);

        EditProtocolUIModel model = zoneEditorUI.getModel();

        model.setModifyingZones(true);

        try {

            stratasNodeToRemove.forEach(model::unselectStrataNode);
            subStratasNodeToRemove.forEach(model::unselectSubStrataNode);

        } finally {

            model.setModifyingZones(false);

        }

        // select zone path
        zonesTree.setSelectionPath(selectedStratas[0].getParentPath());

    }

    protected Set<StrataNode> getStrataNodesToRemove(TreePath[] selectedStratas) {
        Set<StrataNode> result = new HashSet<>();
        for (TreePath selectedStrata : selectedStratas) {

            Object node = selectedStrata.getLastPathComponent();
            if (node instanceof StrataNode) {
                StrataNode strataNode = (StrataNode) node;
                if (log.isInfoEnabled()) {
                    log.info("found strata " + strataNode + " to remove");
                }
                result.add((StrataNode) node);
            }

        }
        return result;
    }

    protected Set<SubStrataNode> getSubStrataNodesToRemove(TreePath[] selectedStratas, Set<StrataNode> stratasNodeToRemove) {
        Set<SubStrataNode> result = new HashSet<>();
        for (TreePath selectedStrata : selectedStratas) {

            Object node = selectedStrata.getLastPathComponent();
            if (node instanceof SubStrataNode) {
                SubStrataNode subStrataNode = (SubStrataNode) node;
                if (!stratasNodeToRemove.contains(subStrataNode.getParent())) {
                    if (log.isInfoEnabled()) {
                        log.info("found subStrata " + subStrataNode + " to remove");
                    }
                    result.add(subStrataNode);
                }
            }

        }
        return result;
    }

}

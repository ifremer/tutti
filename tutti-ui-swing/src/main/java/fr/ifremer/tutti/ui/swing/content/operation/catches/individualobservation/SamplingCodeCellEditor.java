package fr.ifremer.tutti.ui.swing.content.operation.catches.individualobservation;

/*
 * #%L
 * Tutti :: UI
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import fr.ifremer.tutti.ui.swing.content.operation.catches.species.frequency.IndividualObservationBatchRowModel;
import fr.ifremer.tutti.ui.swing.content.operation.catches.species.frequency.IndividualObservationBatchTableModel;
import fr.ifremer.tutti.ui.swing.content.operation.catches.species.frequency.SpeciesFrequencyUIModel;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.swing.AbstractCellEditor;
import javax.swing.JButton;
import javax.swing.JTable;
import javax.swing.border.LineBorder;
import javax.swing.table.TableCellEditor;
import java.awt.Color;
import java.awt.Component;
import java.util.EventObject;
import java.util.Objects;

import static org.nuiton.i18n.I18n.t;

/**
 * @author Kevin Morin (Code Lutin)
 * @since 4.5
 */
public class SamplingCodeCellEditor extends AbstractCellEditor implements TableCellEditor {

    private static final long serialVersionUID = 1L;

    /** Logger. */
    private static final Log log = LogFactory.getLog(SamplingCodeCellEditor.class);

    public static TableCellEditor newEditor(SpeciesFrequencyUIModel model) {

        return new SamplingCodeCellEditor(model);
    }

    protected JTable table;

    protected IndividualObservationBatchTableModel tableModel;

    protected Integer rowIndex;

    protected Integer columnIndex;

    protected final SampleCodeGenerator editorButton;

    public SamplingCodeCellEditor(SpeciesFrequencyUIModel model) {
        this.editorButton = new SampleCodeGenerator(model);
    }

    @Override
    public Component getTableCellEditorComponent(JTable table,
                                                 Object value,
                                                 boolean isSelected,
                                                 int row,
                                                 int column) {
        this.table = table;
        this.tableModel = (IndividualObservationBatchTableModel) table.getModel();

        rowIndex = row;
        columnIndex = column;

        editorButton.setRow(tableModel.getEntry(row));

        return editorButton;
    }

    @Override
    public boolean shouldSelectCell(EventObject anEvent) {
        return true;
    }

    @Override
    public Object getCellEditorValue() {

        IndividualObservationBatchRowModel model = editorButton.getRow();
        Preconditions.checkNotNull(model, "No model found in editor.");

        Object result = model.getSamplingCode();
        if (log.isDebugEnabled()) {
            log.debug("editor value: " + result);
        }

        return result;
    }

    @Override
    public boolean stopCellEditing() {
        boolean b = super.stopCellEditing();
        if (b) {
            editorButton.setRow(null);
        }
        return b;
    }

    @Override
    public void cancelCellEditing() {
        editorButton.setRow(null);
        super.cancelCellEditing();
    }

    public class SampleCodeGenerator extends JButton {

        private final SpeciesFrequencyUIModel model;

        private IndividualObservationBatchRowModel row;

        SampleCodeGenerator(SpeciesFrequencyUIModel model) {

            Objects.requireNonNull(model);

            this.model = model;
            setBorder(new LineBorder(Color.BLACK));
            setText(t("tutti.editIndividualObservationBatch.table.editor.codeSampleGenerator.label"));
            addActionListener(evt -> generateCode());
        }

        public IndividualObservationBatchRowModel getRow() {
            return row;
        }

        public void setRow(IndividualObservationBatchRowModel row) {
            this.row = row;
        }

        protected void generateCode() {

            int samplingCodeId = model.getSamplingCodeUICache().getNextSamplingCodeId();
            String samplingCode = model.getIndividualObservationModel().getSamplingCodePrefix().toSamplingCode(samplingCodeId);

            if (log.isInfoEnabled()) {
                log.info("Generated sampling code: " + samplingCode);
            }
            row.setSamplingCode(samplingCode);

            stopCellEditing();
        }

    }


}

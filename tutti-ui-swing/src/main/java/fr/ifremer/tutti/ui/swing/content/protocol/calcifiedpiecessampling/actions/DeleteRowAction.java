package fr.ifremer.tutti.ui.swing.content.protocol.calcifiedpiecessampling.actions;

/*
 * #%L
 * Tutti :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2016 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.entities.referential.Species;
import fr.ifremer.tutti.service.DecoratorService;
import fr.ifremer.tutti.ui.swing.content.protocol.EditProtocolSpeciesRowModel;
import fr.ifremer.tutti.ui.swing.content.protocol.calcifiedpiecessampling.CalcifiedPiecesSamplingEditorRowModel;
import fr.ifremer.tutti.ui.swing.content.protocol.calcifiedpiecessampling.CalcifiedPiecesSamplingEditorTableModel;
import fr.ifremer.tutti.ui.swing.content.protocol.calcifiedpiecessampling.CalcifiedPiecesSamplingEditorUI;
import fr.ifremer.tutti.ui.swing.util.actions.SimpleActionSupport;
import org.jdesktop.swingx.JXTable;

import javax.swing.JOptionPane;
import java.util.List;

import static org.nuiton.i18n.I18n.t;

/**
 * @author Kevin Morin (Code Lutin)
 * @since 4.5
 */
public class DeleteRowAction extends SimpleActionSupport<CalcifiedPiecesSamplingEditorUI> {

    public DeleteRowAction(CalcifiedPiecesSamplingEditorUI ui) {
        super(ui);
    }

    @Override
    protected void onActionPerformed(CalcifiedPiecesSamplingEditorUI ui) {

        JXTable cpsTable = ui.getCpsTable();
        CalcifiedPiecesSamplingEditorTableModel tableModel = (CalcifiedPiecesSamplingEditorTableModel) cpsTable.getModel();

        int selectedRow = cpsTable.getSelectedRow();

        List<CalcifiedPiecesSamplingEditorRowModel> cspRows = ui.getModel().getCpsRows();

        CalcifiedPiecesSamplingEditorRowModel row = cspRows.get(selectedRow);

        if (row.getMinSize() > 0) {
            EditProtocolSpeciesRowModel speciesToDelete = row.getProtocolSpecies();

            String decoratedRow = ui.getHandler().getDecorator(Species.class, null).toString(speciesToDelete.getSpecies());
            if (row.getMaturity() != null) {
                decoratedRow += " (" + ui.getHandler().getDecorator(Boolean.class, DecoratorService.MATURITY).toString(row.getMaturity()) + ")";
            }
            decoratedRow += " [" + row.getMinSize() + ", "
                            + ui.getHandler().getDecorator(Integer.class, DecoratorService.NULL_INFINITE).toString(row.getMaxSize()) + "]";

            int confirmDeletion = JOptionPane.showConfirmDialog(ui,
                                                                t("tutti.editCps.deleteRow.message", decoratedRow),
                                                                t("tutti.editCps.deleteRow.title"),
                                                                JOptionPane.YES_NO_OPTION,
                                                                JOptionPane.QUESTION_MESSAGE);

            if (confirmDeletion == JOptionPane.YES_OPTION) {

                // merge the row with the previous one
                CalcifiedPiecesSamplingEditorRowModel previousRow = cspRows.get(selectedRow - 1);
                previousRow.setMaxSize(row.getMaxSize());

                cspRows.remove(row);
                tableModel.fireTableRowsDeleted(selectedRow, selectedRow);
            }
        }
    }

}

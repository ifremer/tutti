package fr.ifremer.tutti.ui.swing.content.operation.catches.species.frequency;

/*
 * #%L
 * Tutti :: UI
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.jdesktop.swingx.table.TableColumnModelExt;
import org.nuiton.jaxx.application.swing.table.AbstractApplicationTableModel;
import org.nuiton.jaxx.application.swing.table.ColumnIdentifier;

import static org.nuiton.i18n.I18n.n;

/**
 * Model of the species frequency log table.
 *
 * @author Kevin Morin (Code Lutin)
 * @since 3.8
 */
public class SpeciesFrequencyLogsTableModel extends AbstractApplicationTableModel<SpeciesFrequencyLogRowModel> {

    private static final long serialVersionUID = 1L;

    public static final ColumnIdentifier<SpeciesFrequencyRowModel> LABEL = ColumnIdentifier.newId(
            SpeciesFrequencyLogRowModel.PROPERTY_LABEL,
            n("tutti.editSpeciesFrequencies.logTable.header.label"),
            n("tutti.editSpeciesFrequencies.logTable.header.label"));


    public SpeciesFrequencyLogsTableModel(TableColumnModelExt columnModel) {
        super(columnModel, true, false);
        setNoneEditableCols();
    }

    @Override
    public SpeciesFrequencyLogRowModel createNewRow() {
        return new SpeciesFrequencyLogRowModel();
    }

}

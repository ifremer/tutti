package fr.ifremer.tutti.ui.swing.content.operation.catches.marinelitter.create;

/*
 * #%L
 * Tutti :: UI
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Lists;
import com.google.common.collect.Multimap;
import fr.ifremer.tutti.persistence.entities.referential.Caracteristic;
import fr.ifremer.tutti.persistence.entities.referential.CaracteristicQualitativeValue;
import fr.ifremer.tutti.ui.swing.content.operation.catches.EditCatchesUI;
import fr.ifremer.tutti.ui.swing.content.operation.catches.EditCatchesUIHandler;
import fr.ifremer.tutti.ui.swing.content.operation.catches.marinelitter.MarineLitterBatchUIModel;
import fr.ifremer.tutti.ui.swing.util.AbstractTuttiUIHandler;
import jaxx.runtime.validator.swing.SwingValidator;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.swing.JComponent;

/**
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.3
 */
public class CreateMarineLitterBatchUIHandler extends AbstractTuttiUIHandler<CreateMarineLitterBatchUIModel, CreateMarineLitterBatchUI> {

    /** Logger. */
    private static final Log log = LogFactory.getLog(CreateMarineLitterBatchUIHandler.class);

    //------------------------------------------------------------------------//
    //-- AbstractTuttiUIHandler methods                                     --//
    //------------------------------------------------------------------------//

    @Override
    public void beforeInit(CreateMarineLitterBatchUI ui) {
        super.beforeInit(ui);
        CreateMarineLitterBatchUIModel model = new CreateMarineLitterBatchUIModel(getConfig().getMarineLitterWeightUnit());
        ui.setContextValue(model);
        listModelIsModify(model);
    }

    @Override
    public void afterInit(CreateMarineLitterBatchUI ui) {

        initUI(ui);

        Caracteristic marineLitterCategoryCaracteristic =
                getPersistenceService().getMarineLitterCategoryCaracteristic();
        initBeanFilterableComboBox(ui.getMarineLitterCategoryComboBox(),
                                   Lists.newArrayList(marineLitterCategoryCaracteristic.getQualitativeValue()),
                                   null);

        Caracteristic marineLitterSizeCategoryCaracteristic =
                getPersistenceService().getMarineLitterSizeCategoryCaracteristic();

        initBeanFilterableComboBox(ui.getMarineLitterSizeCategoryComboBox(),
                                   Lists.newArrayList(marineLitterSizeCategoryCaracteristic.getQualitativeValue()),
                                   null);

        listenValidatorValid(ui.getValidator(), getModel());
    }

    @Override
    protected JComponent getComponentToFocus() {
        return getUI().getMarineLitterCategoryComboBox();
    }

    @Override
    public void onCloseUI() {

        if (log.isDebugEnabled()) {
            log.debug("closing: " + ui);
        }

        // evict model from validator
        ui.getValidator().setBean(null);

        // when canceling always invalid model
        getModel().setValid(false);

        EditCatchesUI parent = getParentContainer(EditCatchesUI.class);
        parent.getHandler().setMarineLitterSelectedCard(EditCatchesUIHandler.MAIN_CARD);
    }

    @Override
    public SwingValidator<CreateMarineLitterBatchUIModel> getValidator() {
        return ui.getValidator();
    }

    //------------------------------------------------------------------------//
    //-- Public methods                                                     --//
    //------------------------------------------------------------------------//

    public void openUI(MarineLitterBatchUIModel batchModel) {

        CreateMarineLitterBatchUIModel model = getModel();

        // connect model to validator
        ui.getValidator().setBean(model);

        model.reset();

        Multimap<CaracteristicQualitativeValue, CaracteristicQualitativeValue> categoryUsed =
                model.getMarineLitterCategoryUsed();
        categoryUsed.clear();

        if (batchModel != null) {
            categoryUsed.putAll(batchModel.getMarineLitterCategoriesUsed());
        }

    }

}

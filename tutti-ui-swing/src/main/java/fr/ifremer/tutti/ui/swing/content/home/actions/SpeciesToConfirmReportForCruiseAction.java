package fr.ifremer.tutti.ui.swing.content.home.actions;

/*
 * #%L
 * Tutti :: UI
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import fr.ifremer.tutti.persistence.ProgressionModel;
import fr.ifremer.tutti.persistence.entities.data.Cruise;
import fr.ifremer.tutti.service.export.toconfirmreport.ToConfirmReportService;
import fr.ifremer.tutti.ui.swing.content.actions.AbstractMainUITuttiAction;
import fr.ifremer.tutti.ui.swing.content.MainUIHandler;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import static org.nuiton.i18n.I18n.t;

/**
 * Opens a file chooser, exports the cruise catches into the selected file and open the default email editor.
 *
 * @author Kevin Morin - kmorin@codelutin.com
 * @since 1.0
 */
public class SpeciesToConfirmReportForCruiseAction extends AbstractMainUITuttiAction {

    /** Logger. */
    private static final Log log =
            LogFactory.getLog(SpeciesToConfirmReportForCruiseAction.class);

    public static final DateFormat df = new SimpleDateFormat("yyyyMMddHHmm");

    protected File file;

    public SpeciesToConfirmReportForCruiseAction(MainUIHandler handler) {
        super(handler, true);
    }

    @Override
    public boolean prepareAction() throws Exception {

        boolean doAction = super.prepareAction();

        if (doAction) {

            Cruise cruise = getDataContext().getCruise();
            // choose file to export
            String now = df.format(new Date());
            file = saveFile(
                    t("tutti.speciesToConfirmReport.fileName.default", cruise.getName(), now),
                    "pdf",
                    t("tutti.speciesToConfirmReport.title.choose.exportFile"),
                    t("tutti.speciesToConfirmReport.action.chooseFile"),
                    "^.+\\.pdf$", t("tutti.common.file.pdf")
            );
            doAction = file != null;
        }

        return doAction;
    }

    @Override
    public void releaseAction() {
        file = null;
        super.releaseAction();
    }

    @Override
    public void doAction() throws Exception {
        Cruise cruise = getDataContext().getCruise();
        Preconditions.checkNotNull(cruise);
        Preconditions.checkNotNull(file);

        if (log.isInfoEnabled()) {
            log.info("Will export cruise " + cruise.getId() + " to file: " + file);
        }

        ToConfirmReportService toConfirmReportService = getContext().getToConfirmReportService();

        int nbSteps = toConfirmReportService.getNumberOfSteps(getDataContext().getCruiseId());
        createProgressionModelIfRequired(1);

        ProgressionModel progressionModel = getProgressionModel();
        progressionModel.adaptTotal(nbSteps);

        toConfirmReportService.createToConfirmReport(file, cruise.getIdAsInt(), progressionModel);

    }

    @Override
    public void postSuccessAction() {
        super.postSuccessAction();
        sendMessage(t("tutti.sendCruiseReport.action.success", file));
    }
}

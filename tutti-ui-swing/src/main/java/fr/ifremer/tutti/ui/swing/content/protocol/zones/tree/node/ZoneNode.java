package fr.ifremer.tutti.ui.swing.content.protocol.zones.tree.node;

/*
 * #%L
 * Tutti :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2016 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.entities.protocol.Zone;
import fr.ifremer.tutti.persistence.entities.protocol.Zones;

import java.util.Enumeration;
import java.util.Optional;

/**
 * Pour définir une zone ou bien un nœud racine.
 *
 * @author Kevin Morin (Code Lutin)
 * @author Tony Chemit - chemit@codelutin.com
 * @since 4.5
 */
public class ZoneNode extends ZoneEditorNodeSupport {

    public ZoneNode(String id, String label) {
        super(id, label, true);
    }

    public Optional<StrataNode> tryFindStrataNode(String strataId) {
        Enumeration children = children();
        while (children.hasMoreElements()) {
            StrataNode strataNode = (StrataNode) children.nextElement();
            if (strataId.equals(strataNode.getId())) {
                return Optional.of(strataNode);
            }
        }
        return Optional.empty();
    }

    public Zone toBean() {

        Zone zone = Zones.newZone();
        zone.setId(getId());
        zone.setLabel(getUserObject());

        Enumeration children = children();
        while (children.hasMoreElements()) {
            Object strataNode = children.nextElement();
            zone.addStrata(((StrataNode) strataNode).toBean());
        }

        return zone;

    }

}

package fr.ifremer.tutti.ui.swing.content.referential.actions;

/*
 * #%L
 * Tutti :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import fr.ifremer.tutti.persistence.entities.referential.TuttiReferentialEntity;
import fr.ifremer.tutti.service.referential.ReferentialImportResult;
import fr.ifremer.tutti.ui.swing.content.referential.ManageTemporaryReferentialUI;
import fr.ifremer.tutti.ui.swing.content.referential.ManageTemporaryReferentialUIHandler;
import fr.ifremer.tutti.ui.swing.content.referential.ManageTemporaryReferentialUIModel;
import fr.ifremer.tutti.ui.swing.util.actions.LongActionSupport;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.File;

/**
 * Created on 3/25/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.15
 */
public abstract class ImportTemporaryActionSupport<E extends TuttiReferentialEntity> extends LongActionSupport<ManageTemporaryReferentialUIModel, ManageTemporaryReferentialUI, ManageTemporaryReferentialUIHandler> {

    /** Logger. */
    private static final Log log = LogFactory.getLog(ImportTemporaryActionSupport.class);

    private File file;

    private ReferentialImportResult<E> result;

    protected abstract File chooseImportFile();

    protected abstract ReferentialImportResult<E> doImport(File file);

    protected abstract void postSuccessAction(File file, ReferentialImportResult<E> result) ;

    public ImportTemporaryActionSupport(ManageTemporaryReferentialUIHandler handler) {
        super(handler, true);
    }

    @Override
    public boolean prepareAction() throws Exception {

        boolean doAction = super.prepareAction();

        if (doAction) {

            // choose file to import
            file = chooseImportFile();

            doAction = file != null;
        }
        return doAction;

    }

    @Override
    public void releaseAction() {
        file = null;
        result = null;
        super.releaseAction();
    }

    @Override
    public void doAction() throws Exception {
        Preconditions.checkNotNull(file);
        if (log.isInfoEnabled()) {
            log.info("Will import temporary referential from file: " + file);
        }

        result = doImport(file);

    }

    @Override
    public void postSuccessAction() {

        super.postSuccessAction();

        postSuccessAction(file, result);

    }

    protected void reloadCruise() {
        if (getDataContext().isCruiseFilled()) {
            getDataContext().reloadCruise();
        }
    }

    protected void reloadFishingOperation() {
        if (getDataContext().isFishingOperationFilled()) {
            getDataContext().reloadFishingOperation();
        }
    }

}

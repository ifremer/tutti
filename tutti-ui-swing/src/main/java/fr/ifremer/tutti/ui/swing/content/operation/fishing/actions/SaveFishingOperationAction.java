package fr.ifremer.tutti.ui.swing.content.operation.fishing.actions;

/*
 * #%L
 * Tutti :: UI
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.entities.CaracteristicMap;
import fr.ifremer.tutti.persistence.entities.TuttiEntities;
import fr.ifremer.tutti.persistence.entities.data.CatchBatch;
import fr.ifremer.tutti.persistence.entities.data.CatchBatchs;
import fr.ifremer.tutti.persistence.entities.data.FishingOperation;
import fr.ifremer.tutti.service.PersistenceService;
import fr.ifremer.tutti.service.ValidationService;
import fr.ifremer.tutti.ui.swing.content.operation.EditFishingOperationUI;
import fr.ifremer.tutti.ui.swing.content.operation.EditFishingOperationUIHandler;
import fr.ifremer.tutti.ui.swing.content.operation.EditFishingOperationUIModel;
import fr.ifremer.tutti.ui.swing.content.operation.FishingOperationsUIModel;
import fr.ifremer.tutti.ui.swing.content.operation.fishing.AbstractCaracteristicTabUIModel;
import fr.ifremer.tutti.ui.swing.content.operation.fishing.GearUseFeatureTabUIModel;
import fr.ifremer.tutti.ui.swing.content.operation.fishing.VesselUseFeatureTabUIModel;
import fr.ifremer.tutti.ui.swing.util.actions.LongActionSupport;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Saves a fishing operation and potentially sets another screen or switch to another tab.
 *
 * @author Kevin Morin - kmorin@codelutin.com
 * @since 1.0
 */
public class SaveFishingOperationAction extends LongActionSupport<EditFishingOperationUIModel, EditFishingOperationUI, EditFishingOperationUIHandler> {

    /** Logger. */
    private static final Log log = LogFactory.getLog(SaveFishingOperationAction.class);

    /**
     * A flag to update ui after create or save the edit fishing operation.
     *
     * @since 1.0
     */
    protected boolean updateUI;

    protected boolean moveTab;

    public SaveFishingOperationAction(EditFishingOperationUIHandler handler) {
        super(handler, true);
    }

    public void setUpdateUI(boolean updateUI) {
        this.updateUI = updateUI;
    }

    @Override
    public void releaseAction() {
        updateUI = true;
        super.releaseAction();
    }

    @Override
    public void doAction() throws Exception {

        // previous fishingOperation was modified, let's save it
        EditFishingOperationUIModel beanToSave = handler.getFishingOperationMonitor().getBean();

        moveTab = true;

        // must save when bean is new or was modifiy and is valid
        boolean mustSave = beanToSave.getFishingOperation() != null && beanToSave.isValid();

        if (mustSave) {

            // prepare model
            beanToSave.convertGearShootingCoordinatesToDD();

            // save modified fishing operation
            FishingOperation toSave = beanToSave.toEntity();

            AbstractCaracteristicTabUIModel[] subModels = handler.getSubModels();
            for (AbstractCaracteristicTabUIModel subModel : subModels) {
                Class<?> modelClass = subModel.getClass();
                CaracteristicMap caracteristics = subModel.getCaracteristicMap();
                if (subModel.isModify()) {
                    // see http://forge.codelutin.com/issues/4717
                    moveTab = false;
                }
                if (modelClass.isAssignableFrom(VesselUseFeatureTabUIModel.class)) {
                    toSave.setVesselUseFeatures(caracteristics);
                } else if (modelClass.isAssignableFrom(GearUseFeatureTabUIModel.class)) {
                    toSave.setGearUseFeatures(caracteristics);
                }
                subModel.setModify(false);
            }

            sendMessage("[ Trait - Caractéristiques générales ] Sauvegarde des modifications de " + decorate(toSave) + ".");

            if (log.isInfoEnabled()) {
                log.info("FishingOperation " + toSave.getId() + " was modified, will save it.");
            }

            saveFishingOperation(toSave);

        }

    }

    @Override
    public void postSuccessAction() {
        super.postSuccessAction();
        handler.getFishingOperationMonitor().clearModified();
        getModel().setModify(false);
        if (moveTab && ValidationService.VALIDATION_CONTEXT_EDIT.equals(getContext().getValidationContext())) {
            handler.getParentUi().getTabPane().setSelectedIndex(1);
            handler.getParentUi().getCatchesTabContent().getTabPane().setSelectedIndex(1);
        }
    }

    protected void saveFishingOperation(FishingOperation toSave) {

        boolean create = TuttiEntities.isNew(toSave);

        if (create) {

            createFishingOperation(toSave);

        } else {

            FishingOperationsUIModel model = getHandler().getParentUi().getModel();
            model.setEditionAdjusting(true);

            try {

                persistFishingOperation(toSave);

            } finally {

                model.setEditionAdjusting(false);

            }

        }

    }

    private void createFishingOperation(FishingOperation toSave) {

        FishingOperationsUIModel model = getHandler().getParentUi().getModel();
        PersistenceService service = getContext().getPersistenceService();

        // create fishing operation
        FishingOperation savedFishingOperation = service.createFishingOperation(toSave);

        // create then the CatchBatch
        CatchBatch catchBatch = CatchBatchs.newCatchBatch();
        catchBatch.setFishingOperation(savedFishingOperation);
        service.createCatchBatch(catchBatch);

        // add new created fishing operation to list
        model.addFishingOperation(savedFishingOperation);

        // select it (will reload editing fishing operation)
        model.setSelectedFishingOperation(savedFishingOperation);

    }

    private void persistFishingOperation(FishingOperation toSave) {


        PersistenceService service = getContext().getPersistenceService();

        FishingOperationsUIModel model = getHandler().getParentUi().getModel();

        // save fishing operation
        FishingOperation savedFishingOperation = service.saveFishingOperation(toSave);

        model.setSelectedFishingOperation(null);

        // reinject it in model
        model.updateFishingOperation(savedFishingOperation);

        model.setSelectedFishingOperation(savedFishingOperation);

        getDataContext().reloadFishingOperation();

    }

}

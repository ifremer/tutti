package fr.ifremer.tutti.ui.swing.content.genericformat.tree;

/*
 * #%L
 * Tutti :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JTree;
import javax.swing.UIDefaults;
import javax.swing.UIManager;
import javax.swing.tree.DefaultTreeCellRenderer;
import java.awt.Component;
import java.awt.Font;

import static org.nuiton.i18n.I18n.t;

/**
 * Created on 3/29/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.14.3
 */
public class ExportDataSelectTreeCellRenderer extends DefaultTreeCellRenderer {

    private static final long serialVersionUID = 1L;

    private final JCheckBox normalCheckBox;

    private final JCheckBox partialCheckBox;

    private Font normalFont;

    private Font boldFont;

    public ExportDataSelectTreeCellRenderer() {

        normalCheckBox = new JCheckBox();
        partialCheckBox = new JCheckBox();

        Object iconPainter = UIManager.getDefaults().get("CheckBox[Disabled+Selected].iconPainter");
        UIDefaults defaults = new UIDefaults();
        defaults.put("CheckBox[Disabled].iconPainter", iconPainter);
        partialCheckBox.putClientProperty("Nimbus.Overrides", defaults);
        partialCheckBox.putClientProperty("Nimbus.Overrides.InheritDefaults", false);
        partialCheckBox.setEnabled(false);

        normalFont = UIManager.getFont("CheckBox.font");
        boldFont = normalFont.deriveFont(Font.BOLD);
    }

    @Override
    public Component getTreeCellRendererComponent(JTree tree, Object value, boolean selected, boolean expanded, boolean leaf, int row, boolean hasFocus) {

        JLabel label = (JLabel) super.getTreeCellRendererComponent(tree, value, selected, expanded, leaf, row, hasFocus);

        String text = null;
        String tip = null;
        boolean filled = false;

        JCheckBox checkBox = normalCheckBox;

        if (value != null && value instanceof DataSelectTreeNodeSupport) {

            DataSelectTreeNodeSupport node = (DataSelectTreeNodeSupport) value;

            filled = node.isSelected();

            if (node instanceof CruiseSelectTreeNode) {

                CruiseSelectTreeNode cruiseSelectTreeNode = (CruiseSelectTreeNode) node;

                text = t("tutti.genericformat.export.cruiseNode", cruiseSelectTreeNode.getLabel(), cruiseSelectTreeNode.getNbChilds());

                if (filled) {

                    tip = t("tutti.genericformat.export.cruiseNode.fullSelected", text, cruiseSelectTreeNode.getId());

                } else if (cruiseSelectTreeNode.isPartialSelected()) {

                    checkBox = partialCheckBox;

                    tip = t("tutti.genericformat.export.cruiseNode.partialSelected", text, cruiseSelectTreeNode.getId(), cruiseSelectTreeNode.getNbChildSelected());

                }else {

                    tip = text;

                }

            } else if (node instanceof OperationSelectTreeNode) {

                text = t("tutti.genericformat.export.operationNode", node.getLabel());

                if (filled) {

                    tip = t("tutti.genericformat.export.operationNode.selected", text, node.getId());

                } else {

                    tip = text;

                }
            }
        }

        Font font;
        if (filled) {
            font = this.boldFont;
        } else {
            font = normalFont;
        }

        checkBox.setFont(font);
        checkBox.setSelected(filled);
        checkBox.setBackground(label.getBackground());
        checkBox.setForeground(label.getForeground());
        checkBox.setText(text);
        checkBox.setToolTipText(tip);

        return checkBox;

    }

}

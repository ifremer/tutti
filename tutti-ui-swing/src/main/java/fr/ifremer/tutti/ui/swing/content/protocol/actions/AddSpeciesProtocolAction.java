package fr.ifremer.tutti.ui.swing.content.protocol.actions;

/*
 * #%L
 * Tutti :: UI
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import fr.ifremer.tutti.persistence.entities.referential.Species;
import fr.ifremer.tutti.ui.swing.util.actions.LongActionSupport;
import fr.ifremer.tutti.ui.swing.content.protocol.EditProtocolSpeciesRowModel;
import fr.ifremer.tutti.ui.swing.content.protocol.EditProtocolSpeciesTableModel;
import fr.ifremer.tutti.ui.swing.content.protocol.EditProtocolUI;
import fr.ifremer.tutti.ui.swing.content.protocol.EditProtocolUIHandler;
import fr.ifremer.tutti.ui.swing.content.protocol.EditProtocolUIModel;
import jaxx.runtime.SwingUtil;
import jaxx.runtime.swing.editor.bean.BeanFilterableComboBox;

import java.util.Collection;

import static org.nuiton.i18n.I18n.t;

/**
 * To add a new protocolSpecies protocol.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.0
 */
public class AddSpeciesProtocolAction extends LongActionSupport<EditProtocolUIModel, EditProtocolUI, EditProtocolUIHandler> {

    public AddSpeciesProtocolAction(EditProtocolUIHandler handler) {
        super(handler, false);
    }

    protected Species species;

    protected EditProtocolSpeciesRowModel newRow;

    @Override
    public void doAction() throws Exception {

        EditProtocolUI ui = getUI();

        BeanFilterableComboBox<Species> speciesComboBox = ui.getSpeciesComboBox();

        species = (Species) speciesComboBox.getSelectedItem();
        Preconditions.checkNotNull(
                species, "Can't add a speciesProtocol with a null species");
        Preconditions.checkArgument(species.isReferenceTaxon(),
                                    "Can't add a speciesProtocol with a not referent species");

        Integer taxonId = species.getReferenceTaxonId();
        String taxonIdStr = String.valueOf(taxonId);

        // remove all synonyms of this taxon
        Collection<Species> allSynonyms = getModel().getAllSynonyms(taxonIdStr);
        getModel().getAllSynonyms().removeAll(allSynonyms);

        // add new row to model (do it after combo stuff for ui best display)
        newRow = handler.createNewProtocolSpeciesRow();
        newRow.setSpecies(species);
        getModel().getSpeciesRow().add(newRow);
    }

    @Override
    public void postSuccessAction() {
        super.postSuccessAction();

        // update comboboxes
        getUI().getBenthosComboBox().removeItem(species);
        getUI().getSpeciesComboBox().removeItem(species);
        getUI().getCalcifiedPiecesSamplingEditorUI().getSpeciesComboBox().addItem(species);

        // fire row was inserted in table model
        EditProtocolSpeciesTableModel tableModel = getHandler().getSpeciesTableModel();
        tableModel.fireTableRowsInserted(newRow);

        // select this new row
        int rowIndex = tableModel.getRowIndex(newRow);
        SwingUtil.setSelectionInterval(handler.getSpeciesTable(), rowIndex);

        // add notification
        String speciesStr = decorate(species);
        sendMessage(t("tutti.flash.info.species.add.to.protocol",
                      speciesStr));
    }
}

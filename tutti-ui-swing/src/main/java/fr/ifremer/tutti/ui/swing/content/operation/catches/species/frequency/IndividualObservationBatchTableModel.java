package fr.ifremer.tutti.ui.swing.content.operation.catches.species.frequency;

/*
 * #%L
 * Tutti :: UI
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.entities.CaracteristicMap;
import fr.ifremer.tutti.persistence.entities.data.CopyIndividualObservationMode;
import fr.ifremer.tutti.persistence.entities.referential.Caracteristic;
import fr.ifremer.tutti.persistence.entities.referential.CaracteristicQualitativeValue;
import fr.ifremer.tutti.persistence.entities.referential.Species;
import fr.ifremer.tutti.type.WeightUnit;
import fr.ifremer.tutti.ui.swing.util.table.CaracteristicColumnIdentifier;
import org.apache.commons.collections4.CollectionUtils;
import org.jdesktop.swingx.table.TableColumnModelExt;
import org.nuiton.jaxx.application.swing.table.AbstractApplicationTableModel;
import org.nuiton.jaxx.application.swing.table.ColumnIdentifier;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

import static org.nuiton.i18n.I18n.n;

/**
 * @author Kevin Morin - kmorin@codelutin.com
 * @since 1.4
 */
public class IndividualObservationBatchTableModel extends AbstractApplicationTableModel<IndividualObservationBatchRowModel> {

    private static final long serialVersionUID = 1L;

    public static final ColumnIdentifier<IndividualObservationBatchRowModel> RANK = ColumnIdentifier.newId(
            IndividualObservationBatchRowModel.PROPERTY_RANK_ORDER,
            n("tutti.editIndividualObservationBatch.table.header.rank"),
            n("tutti.editIndividualObservationBatch.table.header.rank.tip"));

    public static final ColumnIdentifier<IndividualObservationBatchRowModel> WEIGHT = ColumnIdentifier.newId(
            IndividualObservationBatchRowModel.PROPERTY_WEIGHT,
            n("tutti.editIndividualObservationBatch.table.header.weight"),
            n("tutti.editIndividualObservationBatch.table.header.weight.tip"));

    public static final ColumnIdentifier<IndividualObservationBatchRowModel> SIZE = ColumnIdentifier.newId(
            IndividualObservationBatchRowModel.PROPERTY_SIZE,
            n("tutti.editIndividualObservationBatch.table.header.size"),
            n("tutti.editIndividualObservationBatch.table.header.size.tip"));

    public static final MaturityColumnIdentifier MATURITY = new MaturityColumnIdentifier();

    public static final ColumnIdentifier<IndividualObservationBatchRowModel> OTHER_CARACTERISTICS = ColumnIdentifier.newId(
            IndividualObservationBatchRowModel.PROPERTY_CARACTERISTICS,
            n("tutti.editIndividualObservationBatch.table.header.otherCaracteristics"),
            n("tutti.editIndividualObservationBatch.table.header.otherCaracteristics.tip"));

    public static final ColumnIdentifier<IndividualObservationBatchRowModel> SAMPLING_CODE = ColumnIdentifier.newId(
            IndividualObservationBatchRowModel.PROPERTY_SAMPLING_CODE,
            n("tutti.editIndividualObservationBatch.table.header.samplingCode"),
            n("tutti.editIndividualObservationBatch.table.header.samplingCode.tip"));

    public static final ColumnIdentifier<IndividualObservationBatchRowModel> COMMENT = ColumnIdentifier.newId(
            IndividualObservationBatchRowModel.PROPERTY_COMMENT,
            n("tutti.editIndividualObservationBatch.table.header.comment"),
            n("tutti.editIndividualObservationBatch.table.header.comment.tip"));

    public static final ColumnIdentifier<IndividualObservationBatchRowModel> ATTACHMENT = ColumnIdentifier.newReadOnlyId(
            IndividualObservationBatchRowModel.PROPERTY_ATTACHMENT,
            n("tutti.editIndividualObservationBatch.table.header.file"),
            n("tutti.editIndividualObservationBatch.table.header.file.tip"));

    /**
     * Weight unit.
     *
     * @since 2.5
     */
    protected final WeightUnit weightUnit;

    protected final CaracteristicMap defaultCaracteristicsMap;

    protected Species species;

    protected Caracteristic lengthstepCaracteristic;

    private final SpeciesFrequencyUIModel uiModel;
    private final Function<IndividualObservationBatchRowModel, Void> installListenersOnRow;
    private final Function<IndividualObservationBatchRowModel, Void> uninstallListenersOnRow;
    private final IndividualObservationBatchUIModel model;

    public IndividualObservationBatchTableModel(WeightUnit weightUnit,
                                                SpeciesFrequencyUIModel uiModel,
                                                TableColumnModelExt columnModel,
                                                Function<IndividualObservationBatchRowModel, Void> installListenersOnRow,
                                                Function<IndividualObservationBatchRowModel, Void> uninstallListenersOnRow) {
        super(columnModel, true, true);
        this.uiModel = uiModel;
        this.installListenersOnRow = installListenersOnRow;
        this.uninstallListenersOnRow = uninstallListenersOnRow;
        this.model = uiModel.getIndividualObservationModel();
        this.weightUnit = weightUnit;
        this.defaultCaracteristicsMap = new CaracteristicMap();
        setNoneEditableCols(RANK);
    }

    public void setSpecies(Species species) {
        this.species = species;
        if (rows != null) {
            rows.forEach(row -> row.setSpecies(species));
        }
    }

    public void setLengthstepCaracteristic(Caracteristic lengthstepCaracteristic) {
        this.lengthstepCaracteristic = lengthstepCaracteristic;
        if (rows != null) {
            rows.forEach(row -> row.setLengthStepCaracteristic(lengthstepCaracteristic));
        }
    }

    public void setDefaultCaracteristics(CaracteristicMap defaultCaracteristics) {
        defaultCaracteristicsMap.clear();
        defaultCaracteristicsMap.putAll(defaultCaracteristics);
    }

    @Override
    public IndividualObservationBatchRowModel createNewRow() {
        IndividualObservationBatchRowModel result = new IndividualObservationBatchRowModel(weightUnit, defaultCaracteristicsMap);
        result.setCopyIndividualObservationMode(uiModel.getCopyIndividualObservationMode());

        // by default empty row is not valid
        result.setValid(false);
        result.setRankOrder(getRowCount() + 1);
        result.setSpecies(species);
        result.setLengthStepCaracteristic(lengthstepCaracteristic);

        if (getRowCount() > 0) {
            IndividualObservationBatchRowModel lastRow = getRows().get(getRowCount() - 1);

            CaracteristicQualitativeValue gender = model.getGender(lastRow);
            model.setGenderValueToDefaultCaracterictis(result, gender);

            if (model.withMaturityCaracteristic()) {
                CaracteristicQualitativeValue maturityState = model.getMaturityValue(lastRow);
                model.setMaturityValueToDefaultCaracterictis(result, maturityState);
            }
        }

        return result;
    }

    @Override
    protected boolean isCellEditable(int rowIndex, int columnIndex,
                                     ColumnIdentifier<IndividualObservationBatchRowModel> columnIdentifier) {

        boolean editable;
        if (columnIdentifier instanceof MaturityColumnIdentifier) {

            MaturityColumnIdentifier maturityColumnIdentifier = (MaturityColumnIdentifier) columnIdentifier;

            editable = maturityColumnIdentifier.withCaracteristic();

            if (editable) {
                Serializable defaultValue = defaultCaracteristicsMap.get(maturityColumnIdentifier.getCaracteristic());
                editable = defaultValue == null;
            }

        } else if (columnIdentifier instanceof CaracteristicColumnIdentifier) {

            CaracteristicColumnIdentifier<IndividualObservationBatchRowModel> caracteristicColumnIdentifier =
                    (CaracteristicColumnIdentifier<IndividualObservationBatchRowModel>) columnIdentifier;

            Caracteristic caracteristic = caracteristicColumnIdentifier.getCaracteristic();
            Serializable defaultValue = defaultCaracteristicsMap.get(caracteristic);

            editable = defaultValue == null;

        } else if (SAMPLING_CODE.equals(columnIdentifier)) {

            editable = getValueAt(rowIndex, columnIndex) == null && getValueAt(rowIndex, identifiers.indexOf(SIZE)) != null;

        } else {
            editable = super.isCellEditable(rowIndex, columnIndex, columnIdentifier);
        }
        return editable;
    }

    public List<IndividualObservationBatchRowModel> loadRows(List<IndividualObservationBatchRowModel> individualObservations) {

        List<IndividualObservationBatchRowModel> obsRows = new ArrayList<>();

        //FIXME Faire un check sur la méthode de mensuration qui doit être la même
        if (CollectionUtils.isNotEmpty(individualObservations)) {

            CopyIndividualObservationMode copyIndividualObservationMode = null;

            int rankOrder = 1;
            for (IndividualObservationBatchRowModel rowModel : individualObservations) {

                CopyIndividualObservationMode incomingCopyIndividualObservationMode = rowModel.getCopyIndividualObservationMode();
                Objects.requireNonNull(incomingCopyIndividualObservationMode, "Mode de recopie non trouvé sur l'observation individuelle: " + rowModel.getId());
                if (copyIndividualObservationMode == null) {
                    copyIndividualObservationMode = incomingCopyIndividualObservationMode;
                } else {
                    if (copyIndividualObservationMode != incomingCopyIndividualObservationMode) {
                        throw new IllegalStateException("Plusieurs modes de recopie trouvés sur les observations individuelles du lot, ce qui est impossible");
                    }
                }
                IndividualObservationBatchRowModel newRow = createNewRow();
                newRow.copy(rowModel);
                newRow.setRankOrder(rankOrder++);
                newRow.setValid(true);

                model.moveGenderValueFromCaracteristicsToDefaultCaracteristics(newRow);
                model.moveMaturityValueFromCaracteristicsToDefaultCaracteristics(newRow);

                obsRows.add(newRow);

            }

        }

        return obsRows;

    }

    @Override
    protected void onRowAdded(int rowIndex, IndividualObservationBatchRowModel row) {

        attachListeners(row);

        uiModel.recomputeCanEditLengthStep();
        uiModel.setModify(true);

    }

    @Override
    protected void onRowUpdated(int rowIndex, IndividualObservationBatchRowModel row) {

        uiModel.recomputeCanEditLengthStep();
        uiModel.setModify(true);
        model.recomputeRowValidState(row);

    }

    @Override
    protected void onRowRemoved(int rowIndex, IndividualObservationBatchRowModel row) {

        dettachListeners(row);

        uiModel.recomputeCanEditLengthStep();
        uiModel.recomputeRowsValidateState();

    }

    @Override
    protected void onBeforeRowsChanged(List<IndividualObservationBatchRowModel> oldRows) {

        if (oldRows != null) {
            oldRows.forEach(this::dettachListeners);
        }

    }

    @Override
    protected void onRowsChanged(List<IndividualObservationBatchRowModel> newRows) {

        int rankOrder = 1;
        for (IndividualObservationBatchRowModel row : newRows) {
            attachListeners(row);
            row.setRankOrder(rankOrder++);
        }
        newRows.forEach(this::attachListeners);

        uiModel.recomputeCanEditLengthStep();
        uiModel.recomputeRowsValidateState();

    }

    public IndividualObservationBatchRowModel addRafaleRow(Float step) {

        IndividualObservationBatchRowModel row = getEmptyRow();
        if (row == null) {
            row = createNewRow();
            addNewRow(row);
        }
        row.setSize(step);
        updateRow(row);

        return row;

    }

    private IndividualObservationBatchRowModel getEmptyRow() {

        IndividualObservationBatchRowModel result = null;

        for (int i = rows.size() - 1; i >= 0; i--) {
            IndividualObservationBatchRowModel row = rows.get(i);
            // if the row has no data set, then it could be the empty row we are looking for
            Set<Caracteristic> notNullCaracteristics = defaultCaracteristicsMap.keySet().stream()
                                                                               .filter(key -> defaultCaracteristicsMap.get(key) != null)
                                                                               .collect(Collectors.toSet());
            if (row.isEmpty(notNullCaracteristics)) {

                result = row;

            } else { // if the row has data set, then the empty row we are looking for was the previous row
                break;
            }

        }

        return result;

    }

    private void dettachListeners(IndividualObservationBatchRowModel result) {
        uninstallListenersOnRow.apply(result);
    }

    private void attachListeners(IndividualObservationBatchRowModel result) {
        dettachListeners(result); // prevent leaks!
        installListenersOnRow.apply(result);
    }

}

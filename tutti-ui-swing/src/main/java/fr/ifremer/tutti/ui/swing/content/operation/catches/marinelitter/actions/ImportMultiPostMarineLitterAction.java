package fr.ifremer.tutti.ui.swing.content.operation.catches.marinelitter.actions;

/*
 * #%L
 * Tutti :: UI
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.entities.data.CatchBatch;
import fr.ifremer.tutti.persistence.entities.data.FishingOperation;
import fr.ifremer.tutti.persistence.entities.data.MarineLitterBatch;
import fr.ifremer.tutti.service.catches.multipost.MultiPostImportService;
import fr.ifremer.tutti.type.WeightUnit;
import fr.ifremer.tutti.ui.swing.content.operation.catches.actions.ImportMultiPostActionSupport;
import fr.ifremer.tutti.ui.swing.content.operation.catches.marinelitter.MarineLitterBatchUI;
import fr.ifremer.tutti.ui.swing.content.operation.catches.marinelitter.MarineLitterBatchUIHandler;
import fr.ifremer.tutti.ui.swing.content.operation.catches.marinelitter.MarineLitterBatchUIModel;

import java.io.File;
import java.util.Collection;
import java.util.Map;

import static org.nuiton.i18n.I18n.n;
import static org.nuiton.i18n.I18n.t;

/**
 * @author Kevin Morin - kmorin@codelutin.com
 * @since 2.2
 */
public class ImportMultiPostMarineLitterAction extends ImportMultiPostActionSupport<MarineLitterBatchUIModel, MarineLitterBatchUI, MarineLitterBatchUIHandler> {

    public ImportMultiPostMarineLitterAction(MarineLitterBatchUIHandler handler) {
        super(handler);
    }

    @Override
    protected String getFileExtension() {
        return "tuttiMarineLitter";
    }

    @Override
    protected String getFileExtensionDescription() {
        return t("tutti.common.file.tuttiMarineLitter");
    }

    @Override
    protected String getFileChooserTitle() {
        return t("tutti.editMarineLitterBatch.action.importMultiPost.sourceFile.title");
    }

    @Override
    protected String getFileChooserButton() {
        return t("tutti.editMarineLitterBatch.action.importMultiPost.sourceFile.button");
    }

    @Override
    protected String getSuccessMessage(File file) {
        return t("tutti.editMarineLitterBatch.action.importMultiPost.success", file);
    }

    @Override
    protected Map<String, Object> importBatches(MultiPostImportService multiPostImportExportService, File file, FishingOperation operation) {

        return multiPostImportExportService.importMarineLitter(file, operation);

    }

    @Override
    protected String buildNotImportedDataReportText(Map<String, Object> notImportedData) {

        WeightUnit marineLitterWeightUnit = getConfig().getMarineLitterWeightUnit();

        StringBuilder builder = new StringBuilder();

        Float totalWeight = (Float) notImportedData.get(CatchBatch.PROPERTY_MARINE_LITTER_TOTAL_WEIGHT);
        addNotImportedWeightToReport(builder, totalWeight, marineLitterWeightUnit, n("tutti.multiPostImportLog.totalWeight"));

        Collection<MarineLitterBatch> notImportedMarineLitterBatches =
                (Collection<MarineLitterBatch>) notImportedData.get(MultiPostImportService.BATCHES_KEY);

        for (MarineLitterBatch mlb : notImportedMarineLitterBatches) {
            builder.append("- ").append(decorate(mlb.getMarineLitterCategory())).append(" / ").append(decorate(mlb.getMarineLitterSizeCategory())).append("\n");
        }

        return builder.toString();

    }
}

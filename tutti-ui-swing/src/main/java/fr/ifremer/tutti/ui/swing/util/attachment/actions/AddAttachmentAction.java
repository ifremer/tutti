package fr.ifremer.tutti.ui.swing.util.attachment.actions;

/*
 * #%L
 * Tutti :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.entities.data.Attachment;
import fr.ifremer.tutti.persistence.entities.data.Attachments;
import fr.ifremer.tutti.ui.swing.util.actions.SimpleActionSupport;
import fr.ifremer.tutti.ui.swing.util.attachment.AttachmentEditorUI;
import fr.ifremer.tutti.ui.swing.util.attachment.AttachmentEditorUIHandler;
import fr.ifremer.tutti.ui.swing.util.attachment.AttachmentModelAware;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.File;

/**
 * Created on 3/7/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.15
 */
public class AddAttachmentAction extends SimpleActionSupport<AttachmentEditorUI> {

    /** Logger. */
    private static final Log log = LogFactory.getLog(AddAttachmentAction.class);

    private static final long serialVersionUID = 1L;

    public AddAttachmentAction(AttachmentEditorUI ui) {
        super(ui);
    }

    @Override
    protected void onActionPerformed(AttachmentEditorUI ui) {

        File file = ui.getFile().getSelectedFile();
        if (file != null) {
            String name = ui.getFileName().getText();
            if (StringUtils.isEmpty(name)) {
                name = file.getName();
            }

            if (log.isInfoEnabled()) {
                log.info("Add attachment: " + name);
            }

            AttachmentModelAware bean = ui.getBean();
            Attachment attachment = Attachments.newAttachment();

            attachment.setObjectType(bean.getObjectType());
            attachment.setObjectId(bean.getObjectId());
            attachment.setName(name);
            attachment.setComment(ui.getFileComment().getText());

            AttachmentEditorUIHandler handler = ui.getHandler();

            if (bean.isCreate()) {
                attachment.setPath(file.getPath());
            } else {
                attachment = handler.getPersistenceService().createAttachment(attachment, file);
            }
            bean.addAttachment(attachment);

            handler.resetFields();
            handler.addAttachment(attachment);

            ui.pack();

        }

    }

}
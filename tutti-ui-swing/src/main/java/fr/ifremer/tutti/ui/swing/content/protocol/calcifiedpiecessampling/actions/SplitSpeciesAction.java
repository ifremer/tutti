package fr.ifremer.tutti.ui.swing.content.protocol.calcifiedpiecessampling.actions;

/*
 * #%L
 * Tutti :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2016 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.ui.swing.content.protocol.calcifiedpiecessampling.CalcifiedPiecesSamplingEditorRowModel;
import fr.ifremer.tutti.ui.swing.content.protocol.calcifiedpiecessampling.CalcifiedPiecesSamplingEditorTableModel;
import fr.ifremer.tutti.ui.swing.content.protocol.calcifiedpiecessampling.CalcifiedPiecesSamplingEditorUI;
import fr.ifremer.tutti.ui.swing.content.protocol.calcifiedpiecessampling.MinSizePopupUI;
import fr.ifremer.tutti.ui.swing.util.actions.SimpleActionSupport;
import org.jdesktop.swingx.JXTable;

import java.util.List;

/**
 * @author Kevin Morin (Code Lutin)
 * @since 4.5
 */
public class SplitSpeciesAction extends SimpleActionSupport<CalcifiedPiecesSamplingEditorUI> {

    public SplitSpeciesAction(CalcifiedPiecesSamplingEditorUI ui) {
        super(ui);
    }

    @Override
    protected void onActionPerformed(CalcifiedPiecesSamplingEditorUI ui) {

        JXTable cpsTable = ui.getCpsTable();
        CalcifiedPiecesSamplingEditorTableModel tableModel = (CalcifiedPiecesSamplingEditorTableModel) cpsTable.getModel();

        int selectedRow = cpsTable.getSelectedRow();

        List<CalcifiedPiecesSamplingEditorRowModel> cpsRows = ui.getModel().getCpsRows();

        CalcifiedPiecesSamplingEditorRowModel row = cpsRows.get(selectedRow);

        int minMinSize = row.getMinSize() + 1;
        Integer maxMinSize = row.getMaxSize() != null ? row.getMaxSize() - 1 : null;

        MinSizePopupUI minSizePopupUI = new MinSizePopupUI(ui);
        minSizePopupUI.open(minMinSize, maxMinSize);

        if (minSizePopupUI.getModel().isValid()) {

            Integer minSize = minSizePopupUI.getModel().getMinSize();
            Integer exMaxSize = row.getMaxSize();
            row.setMaxSize(minSize - 1);

            CalcifiedPiecesSamplingEditorRowModel newRow = ui.getHandler().createNewRow(row.getProtocolSpecies(),
                                                                                        row.getMaturity(),
                                                                                        row.isSex(),
                                                                                        minSize,
                                                                                        exMaxSize);
            cpsRows.add(selectedRow + 1, newRow);

            tableModel.fireTableRowsUpdated(selectedRow, selectedRow);
            tableModel.fireTableRowsInserted(newRow);

            // select this new row
            //        int rowIndex = tableModel.getRowIndex(newRow);
            //        SwingUtil.setSelectionInterval(cpsTable, rowIndex);
        }
    }

}

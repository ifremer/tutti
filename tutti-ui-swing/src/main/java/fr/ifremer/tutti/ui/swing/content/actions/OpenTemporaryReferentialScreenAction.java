package fr.ifremer.tutti.ui.swing.content.actions;

/*
 * #%L
 * Tutti :: UI
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.ui.swing.TuttiScreen;
import fr.ifremer.tutti.ui.swing.content.MainUIHandler;

import javax.swing.SwingUtilities;

import static org.nuiton.i18n.I18n.t;

/**
 * Open screen to import referential
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.0
 */
public class OpenTemporaryReferentialScreenAction extends AbstractChangeScreenAction {

    public OpenTemporaryReferentialScreenAction(MainUIHandler handler) {
        super(handler, true, TuttiScreen.IMPORT_TEMPORARY_REFERENTIAL);
    }

    @Override
    public boolean prepareAction() throws Exception {
        boolean result = super.prepareAction();
        if (result) {
            result = askAdminPassword(
                    t("tutti.manageTemporaryReferential.passwordDialog.message"),
                    t("tutti.manageTemporaryReferential.passwordDialog.title"),
                    t("tutti.manageTemporaryReferential.passwordDialog.error.message"),
                    t("tutti.manageTemporaryReferential.passwordDialog.error.title")
            );
        }
        return result;
    }

    @Override
    public void doAction() throws Exception {
        super.doAction();
        loadReferantials(true);
        SwingUtilities.invokeLater(() -> getContext().getMainUI().getBody().revalidate());

    }
}

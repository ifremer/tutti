package fr.ifremer.tutti.ui.swing.content.operation;

/*
 * #%L
 * Tutti :: UI
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import fr.ifremer.tutti.persistence.entities.TuttiEntities;
import fr.ifremer.tutti.persistence.entities.data.Cruise;
import fr.ifremer.tutti.persistence.entities.data.Program;
import fr.ifremer.tutti.persistence.entities.protocol.OperationFieldMappingRow;
import fr.ifremer.tutti.persistence.entities.referential.Caracteristic;
import fr.ifremer.tutti.persistence.entities.referential.Gear;
import fr.ifremer.tutti.persistence.entities.referential.Person;
import fr.ifremer.tutti.persistence.entities.referential.TuttiLocation;
import fr.ifremer.tutti.persistence.entities.referential.Vessel;
import fr.ifremer.tutti.service.DecoratorService;
import fr.ifremer.tutti.service.PersistenceService;
import fr.ifremer.tutti.type.CoordinateEditorType;
import fr.ifremer.tutti.ui.swing.TuttiUIContext;
import fr.ifremer.tutti.ui.swing.content.operation.fishing.AbstractCaracteristicTabUIModel;
import fr.ifremer.tutti.ui.swing.content.operation.fishing.GearUseFeatureTabUIModel;
import fr.ifremer.tutti.ui.swing.content.operation.fishing.VesselUseFeatureTabUIModel;
import fr.ifremer.tutti.ui.swing.content.operation.fishing.actions.CancelEditFishingOperationAction;
import fr.ifremer.tutti.ui.swing.util.AbstractTuttiTabContainerUIHandler;
import fr.ifremer.tutti.ui.swing.util.TuttiBeanMonitor;
import fr.ifremer.tutti.ui.swing.util.caracteristics.GearCaracteristicListCellRenderer;
import fr.ifremer.tutti.util.DateTimes;
import fr.ifremer.tutti.util.Distances;
import jaxx.runtime.validator.swing.SwingValidator;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jdesktop.swingx.JXDatePicker;
import org.nuiton.jaxx.application.bean.JavaBeanObjectUtil;
import org.nuiton.jaxx.application.swing.tab.TabHandler;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JOptionPane;
import javax.swing.JTabbedPane;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Set;

import static org.nuiton.i18n.I18n.t;

/**
 * Handler for UI {@link EditFishingOperationUI}.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 0.1
 */
public class EditFishingOperationUIHandler extends AbstractTuttiTabContainerUIHandler<EditFishingOperationUIModel, EditFishingOperationUI>
        implements TabHandler {

    /** Logger. */
    private static final Log log =
            LogFactory.getLog(EditFishingOperationUIHandler.class);

    /**
     * listener to set the same end date as the start date
     * when the user changes it only (remove this listener before selecting the
     * fishing operation)
     */
    private final PropertyChangeListener startDateListener = new PropertyChangeListener() {

        public void propertyChange(PropertyChangeEvent evt) {
            //check if the user changed the date and not only the time)
            Date oldDate = (Date) evt.getOldValue();
            Date newDate = (Date) evt.getNewValue();
            if (newDate != null && getModel().getGearShootingEndDate() == null &&
                    (oldDate == null || !DateUtils.isSameDay(oldDate, newDate))) {
                getModel().setGearShootingEndDate(newDate);
            }
        }
    };

    private final PropertyChangeListener coordinatePropertiesListener = new PropertyChangeListener() {

        private List<String> properties = Lists.newArrayList(
                EditFishingOperationUIModel.PROPERTY_FISHING_OPERATION_RECTILIGNE,
                EditFishingOperationUIModel.PROPERTY_GEAR_SHOOTING_END_LATITUDE,
                EditFishingOperationUIModel.PROPERTY_GEAR_SHOOTING_END_LATITUDE_DMS,
                EditFishingOperationUIModel.PROPERTY_GEAR_SHOOTING_END_LATITUDE_DMS_SIGN,
                EditFishingOperationUIModel.PROPERTY_GEAR_SHOOTING_END_LATITUDE_DMS_DEGREE,
                EditFishingOperationUIModel.PROPERTY_GEAR_SHOOTING_END_LATITUDE_DMS_MINUTE,
                EditFishingOperationUIModel.PROPERTY_GEAR_SHOOTING_END_LATITUDE_DMS_SECOND,
                EditFishingOperationUIModel.PROPERTY_GEAR_SHOOTING_END_LATITUDE_DMD,
                EditFishingOperationUIModel.PROPERTY_GEAR_SHOOTING_END_LATITUDE_DMD_SIGN,
                EditFishingOperationUIModel.PROPERTY_GEAR_SHOOTING_END_LATITUDE_DMD_DEGREE,
                EditFishingOperationUIModel.PROPERTY_GEAR_SHOOTING_END_LATITUDE_DMD_MINUTE,
                EditFishingOperationUIModel.PROPERTY_GEAR_SHOOTING_END_LATITUDE_DMD_DECIMAL,
                EditFishingOperationUIModel.PROPERTY_GEAR_SHOOTING_END_LONGITUDE,
                EditFishingOperationUIModel.PROPERTY_GEAR_SHOOTING_END_LONGITUDE_DMS,
                EditFishingOperationUIModel.PROPERTY_GEAR_SHOOTING_END_LONGITUDE_DMS_SIGN,
                EditFishingOperationUIModel.PROPERTY_GEAR_SHOOTING_END_LONGITUDE_DMS_DEGREE,
                EditFishingOperationUIModel.PROPERTY_GEAR_SHOOTING_END_LONGITUDE_DMS_MINUTE,
                EditFishingOperationUIModel.PROPERTY_GEAR_SHOOTING_END_LONGITUDE_DMS_SECOND,
                EditFishingOperationUIModel.PROPERTY_GEAR_SHOOTING_END_LONGITUDE_DMD,
                EditFishingOperationUIModel.PROPERTY_GEAR_SHOOTING_END_LONGITUDE_DMD_SIGN,
                EditFishingOperationUIModel.PROPERTY_GEAR_SHOOTING_END_LONGITUDE_DMD_DEGREE,
                EditFishingOperationUIModel.PROPERTY_GEAR_SHOOTING_END_LONGITUDE_DMD_MINUTE,
                EditFishingOperationUIModel.PROPERTY_GEAR_SHOOTING_END_LONGITUDE_DMD_DECIMAL,
                EditFishingOperationUIModel.PROPERTY_GEAR_SHOOTING_START_LATITUDE,
                EditFishingOperationUIModel.PROPERTY_GEAR_SHOOTING_START_LATITUDE_DMS,
                EditFishingOperationUIModel.PROPERTY_GEAR_SHOOTING_START_LATITUDE_DMS_SIGN,
                EditFishingOperationUIModel.PROPERTY_GEAR_SHOOTING_START_LATITUDE_DMS_DEGREE,
                EditFishingOperationUIModel.PROPERTY_GEAR_SHOOTING_START_LATITUDE_DMS_MINUTE,
                EditFishingOperationUIModel.PROPERTY_GEAR_SHOOTING_START_LATITUDE_DMS_SECOND,
                EditFishingOperationUIModel.PROPERTY_GEAR_SHOOTING_START_LATITUDE_DMD,
                EditFishingOperationUIModel.PROPERTY_GEAR_SHOOTING_START_LATITUDE_DMD_SIGN,
                EditFishingOperationUIModel.PROPERTY_GEAR_SHOOTING_START_LATITUDE_DMD_DEGREE,
                EditFishingOperationUIModel.PROPERTY_GEAR_SHOOTING_START_LATITUDE_DMD_MINUTE,
                EditFishingOperationUIModel.PROPERTY_GEAR_SHOOTING_START_LATITUDE_DMD_DECIMAL,
                EditFishingOperationUIModel.PROPERTY_GEAR_SHOOTING_START_LONGITUDE,
                EditFishingOperationUIModel.PROPERTY_GEAR_SHOOTING_START_LONGITUDE_DMS,
                EditFishingOperationUIModel.PROPERTY_GEAR_SHOOTING_START_LONGITUDE_DMS_SIGN,
                EditFishingOperationUIModel.PROPERTY_GEAR_SHOOTING_START_LONGITUDE_DMS_DEGREE,
                EditFishingOperationUIModel.PROPERTY_GEAR_SHOOTING_START_LONGITUDE_DMS_MINUTE,
                EditFishingOperationUIModel.PROPERTY_GEAR_SHOOTING_START_LONGITUDE_DMS_SECOND,
                EditFishingOperationUIModel.PROPERTY_GEAR_SHOOTING_START_LONGITUDE_DMD,
                EditFishingOperationUIModel.PROPERTY_GEAR_SHOOTING_START_LONGITUDE_DMD_SIGN,
                EditFishingOperationUIModel.PROPERTY_GEAR_SHOOTING_START_LONGITUDE_DMD_DEGREE,
                EditFishingOperationUIModel.PROPERTY_GEAR_SHOOTING_START_LONGITUDE_DMD_MINUTE,
                EditFishingOperationUIModel.PROPERTY_GEAR_SHOOTING_START_LONGITUDE_DMD_DECIMAL
        );

        @Override
        public void propertyChange(PropertyChangeEvent evt) {
            if (properties.contains(evt.getPropertyName())) {
                EditFishingOperationUIModel source = (EditFishingOperationUIModel) evt.getSource();
                if (source.isFishingOperationRectiligne()) {
                    source.computeDistance();
                }

                if (log.isInfoEnabled()) {
                    log.info(String.format("Property %s changed to %s", evt.getPropertyName(), evt.getNewValue()));
                }
            }
        }
    };

    public void setQuadrantValue(int value) {
        if (log.isInfoEnabled()) {
            log.info("New quadrant value: " + value);
        }
    }

    public boolean isQuadrantSelected(Integer value, int requiredValue) {
        return value != null && value == requiredValue;
    }

    /**
     * To monitor changes on the incoming fishing operation.
     *
     * @since 0.3
     */
    private TuttiBeanMonitor<EditFishingOperationUIModel> fishingOperationMonitor;

    //------------------------------------------------------------------------//
    //-- AbstractTuttiUIHandler methods                                     --//
    //------------------------------------------------------------------------//

    @Override
    public void beforeInit(EditFishingOperationUI ui) {
        super.beforeInit(ui);

        this.fishingOperationMonitor = new TuttiBeanMonitor<>(
                EditFishingOperationUIModel.PROPERTY_STATION_NUMBER,
                EditFishingOperationUIModel.PROPERTY_FISHING_OPERATION_NUMBER,
                EditFishingOperationUIModel.PROPERTY_STRATA,
                EditFishingOperationUIModel.PROPERTY_SUB_STRATA,

                EditFishingOperationUIModel.PROPERTY_GEAR_SHOOTING_END_LATITUDE,
                EditFishingOperationUIModel.PROPERTY_GEAR_SHOOTING_END_LATITUDE_DMS,
                EditFishingOperationUIModel.PROPERTY_GEAR_SHOOTING_END_LATITUDE_DMS_SIGN,
                EditFishingOperationUIModel.PROPERTY_GEAR_SHOOTING_END_LATITUDE_DMS_DEGREE,
                EditFishingOperationUIModel.PROPERTY_GEAR_SHOOTING_END_LATITUDE_DMS_MINUTE,
                EditFishingOperationUIModel.PROPERTY_GEAR_SHOOTING_END_LATITUDE_DMS_SECOND,
                EditFishingOperationUIModel.PROPERTY_GEAR_SHOOTING_END_LATITUDE_DMD,
                EditFishingOperationUIModel.PROPERTY_GEAR_SHOOTING_END_LATITUDE_DMD_SIGN,
                EditFishingOperationUIModel.PROPERTY_GEAR_SHOOTING_END_LATITUDE_DMD_DEGREE,
                EditFishingOperationUIModel.PROPERTY_GEAR_SHOOTING_END_LATITUDE_DMD_MINUTE,
                EditFishingOperationUIModel.PROPERTY_GEAR_SHOOTING_END_LONGITUDE,
                EditFishingOperationUIModel.PROPERTY_GEAR_SHOOTING_END_LONGITUDE_DMS,
                EditFishingOperationUIModel.PROPERTY_GEAR_SHOOTING_END_LONGITUDE_DMS_SIGN,
                EditFishingOperationUIModel.PROPERTY_GEAR_SHOOTING_END_LONGITUDE_DMS_DEGREE,
                EditFishingOperationUIModel.PROPERTY_GEAR_SHOOTING_END_LONGITUDE_DMS_MINUTE,
                EditFishingOperationUIModel.PROPERTY_GEAR_SHOOTING_END_LONGITUDE_DMS_SECOND,
                EditFishingOperationUIModel.PROPERTY_GEAR_SHOOTING_END_LONGITUDE_DMD,
                EditFishingOperationUIModel.PROPERTY_GEAR_SHOOTING_END_LONGITUDE_DMD_SIGN,
                EditFishingOperationUIModel.PROPERTY_GEAR_SHOOTING_END_LONGITUDE_DMD_DEGREE,
                EditFishingOperationUIModel.PROPERTY_GEAR_SHOOTING_END_LONGITUDE_DMD_MINUTE,
                EditFishingOperationUIModel.PROPERTY_GEAR_SHOOTING_START_LATITUDE,
                EditFishingOperationUIModel.PROPERTY_GEAR_SHOOTING_START_LATITUDE_DMS,
                EditFishingOperationUIModel.PROPERTY_GEAR_SHOOTING_START_LATITUDE_DMS_SIGN,
                EditFishingOperationUIModel.PROPERTY_GEAR_SHOOTING_START_LATITUDE_DMS_DEGREE,
                EditFishingOperationUIModel.PROPERTY_GEAR_SHOOTING_START_LATITUDE_DMS_MINUTE,
                EditFishingOperationUIModel.PROPERTY_GEAR_SHOOTING_START_LATITUDE_DMS_SECOND,
                EditFishingOperationUIModel.PROPERTY_GEAR_SHOOTING_START_LATITUDE_DMD,
                EditFishingOperationUIModel.PROPERTY_GEAR_SHOOTING_START_LATITUDE_DMD_SIGN,
                EditFishingOperationUIModel.PROPERTY_GEAR_SHOOTING_START_LATITUDE_DMD_DEGREE,
                EditFishingOperationUIModel.PROPERTY_GEAR_SHOOTING_START_LATITUDE_DMD_MINUTE,
                EditFishingOperationUIModel.PROPERTY_GEAR_SHOOTING_START_LONGITUDE,
                EditFishingOperationUIModel.PROPERTY_GEAR_SHOOTING_START_LONGITUDE_DMS,
                EditFishingOperationUIModel.PROPERTY_GEAR_SHOOTING_START_LONGITUDE_DMS_SIGN,
                EditFishingOperationUIModel.PROPERTY_GEAR_SHOOTING_START_LONGITUDE_DMS_DEGREE,
                EditFishingOperationUIModel.PROPERTY_GEAR_SHOOTING_START_LONGITUDE_DMS_MINUTE,
                EditFishingOperationUIModel.PROPERTY_GEAR_SHOOTING_START_LONGITUDE_DMS_SECOND,
                EditFishingOperationUIModel.PROPERTY_GEAR_SHOOTING_START_LONGITUDE_DMD,
                EditFishingOperationUIModel.PROPERTY_GEAR_SHOOTING_START_LONGITUDE_DMD_SIGN,
                EditFishingOperationUIModel.PROPERTY_GEAR_SHOOTING_START_LONGITUDE_DMD_DEGREE,
                EditFishingOperationUIModel.PROPERTY_GEAR_SHOOTING_START_LONGITUDE_DMD_MINUTE,

                EditFishingOperationUIModel.PROPERTY_GEAR_SHOOTING_START_DATE,
                EditFishingOperationUIModel.PROPERTY_GEAR_SHOOTING_END_DATE,
                EditFishingOperationUIModel.PROPERTY_FISHING_OPERATION_RECTILIGNE,
                EditFishingOperationUIModel.PROPERTY_TRAWL_DISTANCE,
                EditFishingOperationUIModel.PROPERTY_FISHING_OPERATION_VALID,
                EditFishingOperationUIModel.PROPERTY_LOCATION,
                EditFishingOperationUIModel.PROPERTY_RECORDER_PERSON,
                EditFishingOperationUIModel.PROPERTY_SECONDARY_VESSEL,
                EditFishingOperationUIModel.PROPERTY_VESSEL,
                EditFishingOperationUIModel.PROPERTY_GEAR,
                EditFishingOperationUIModel.PROPERTY_MULTIRIG_AGGREGATION,
                EditFishingOperationUIModel.PROPERTY_COMMENT);

        EditFishingOperationUIModel model = new EditFishingOperationUIModel();
        model.setCoordinateEditorType(getConfig().getCoordinateEditorType());
        model.setValidationContext(getContext().getValidationContext());

        model.addPropertyChangeListener(EditFishingOperationUIModel.PROPERTY_STRATA, evt -> {
            EditFishingOperationUIModel source = (EditFishingOperationUIModel) evt.getSource();
            TuttiLocation newStrata = (TuttiLocation) evt.getNewValue();
            onSelectedStrata(source, newStrata);
        });

        model.addPropertyChangeListener(EditFishingOperationUIModel.PROPERTY_SUB_STRATA, evt -> {
            EditFishingOperationUIModel source = (EditFishingOperationUIModel) evt.getSource();
            TuttiLocation newStrata = (TuttiLocation) evt.getNewValue();
            onSelectedSubStrata(source, newStrata);
        });

        getContext().addPropertyChangeListener(TuttiUIContext.PROPERTY_VALIDATION_CONTEXT, evt -> getModel().setValidationContext((String) evt.getNewValue()));

        model.addPropertyChangeListener(EditFishingOperationUIModel.PROPERTY_FISHING_OPERATION_VALID, evt -> {
            Boolean valid = (Boolean) evt.getNewValue();
            Color color = null;
            Color fontColor = Color.BLACK;
            if (valid == Boolean.TRUE) {
                color = Color.GREEN;

            } else if (valid == Boolean.FALSE) {
                color = Color.RED;
                fontColor = Color.WHITE;
            }
            Component tab = getTabPanel().getTabComponentAt(0);
            tab.setForeground(fontColor);
            tab.setBackground(color);
        });

        // enable or not the import from column file
        if (getDataContext().isProtocolFilled()) {
            Collection<OperationFieldMappingRow> operationFieldMapping = getDataContext().getProtocol().getOperationFieldMapping();
            for (OperationFieldMappingRow row : operationFieldMapping) {
                if ((EditFishingOperationUIModel.PROPERTY_STATION_NUMBER.equals(row.getField())
                        || EditFishingOperationUIModel.PROPERTY_FISHING_OPERATION_NUMBER.equals(row.getField())
                        || EditFishingOperationUIModel.PROPERTY_GEAR_SHOOTING_START_DATE.equals(row.getField()))
                        && StringUtils.isNotBlank(row.getImportColumn())) {

                    model.setImportFromColumnFileEnabled(true);
                    break;
                }
            }
        }

        ui.setContextValue(model);

        fishingOperationMonitor.setBean(model);
    }

    @Override
    public void afterInit(EditFishingOperationUI ui) {

        ui.getSecondaryVesselList().getBeforeFilterPanel().add(ui.getSecondaryVesselFilterPane(), BorderLayout.CENTER);

        ui.getFishingOperationValidPanel().remove(
                ui.getFishingOperationResetRadio());

        final EditFishingOperationUIModel model = getModel();

        initUI(ui);

        List<Person> people = getDataContext().getPersons();
        initBeanList(ui.getRecorderPersonList(),
                     people,
                     model.getRecorderPerson());

        Cruise cruise = getDataContext().getCruise();
        Preconditions.checkNotNull(cruise,
                                   "Could not find cruise in ui context");
        ui.getMultirigAggregationField().setEnabled(cruise.getMultirigNumber() != 1);

        initBeanFilterableComboBox(ui.getGearComboBox(),
                                   Lists.<Gear>newArrayList(),
                                   null,
                                   DecoratorService.GEAR_WITH_RANK_ORDER);


        JComboBox combobox = ui.getGearComboBox().getCombobox();
        combobox.setRenderer(new GearCaracteristicListCellRenderer(
                combobox.getRenderer(),
                getDecorator(Caracteristic.class, DecoratorService.CARACTERISTIC_WITH_UNIT),
                getDecorator(Gear.class, DecoratorService.GEAR_WITH_RANK_ORDER),
                true));
        combobox.addItemListener(new ItemListener() {

//            Map<Gear, String> tips = Maps.newHashMap();

            public void itemStateChanged(ItemEvent e) {
                Object item = e.getItem();
                if (e.getStateChange() == ItemEvent.SELECTED) {
                    if (log.isDebugEnabled()) {
                        log.debug("itemStateChanged selected " + item + " - " + (item != null ? item.getClass() : null));
                    }

                    JComboBox combo = (JComboBox) e.getSource();
                    JComponent jC = (JComponent) combo.getEditor().getEditorComponent();

                    GearCaracteristicListCellRenderer renderer = (GearCaracteristicListCellRenderer) combo.getRenderer();

                    String toolTipText = renderer.getToolTipText((Gear) item);

                    jC.setToolTipText(toolTipText);
                } else {
                    JComboBox combo = (JComboBox) e.getSource();
                    JComponent jC = (JComponent) combo.getEditor().getEditorComponent();

                    jC.setToolTipText(null);
                }
            }
        });

        Program program = getDataContext().getProgram();
        Preconditions.checkNotNull(program,
                                   "Could not find program in ui context");

        TuttiLocation zone = program.getZone();

        TuttiLocation strata = model.getStrata();

        String strataId = strata == null ? null : strata.getId();

        TuttiLocation subStrata = model.getSubStrata();
        String substrataId = subStrata == null ? null : subStrata.getId();

        TuttiLocation location = model.getLocation();

        PersistenceService persistenceService = getPersistenceService();
        String zoneId = zone.getId();
        List<TuttiLocation> stratas =
                Lists.newArrayList(persistenceService.getAllFishingOperationStrata(zoneId));

        List<TuttiLocation> subStratas =
                Lists.newArrayList(persistenceService.getAllFishingOperationSubStrata(zoneId, strataId));

        List<TuttiLocation> locations =
                Lists.newArrayList(persistenceService.getAllFishingOperationLocation(zoneId, strataId, substrataId));

        initBeanFilterableComboBox(ui.getStrataComboBox(), stratas, strata);

        initBeanFilterableComboBox(ui.getSubStrataComboBox(), subStratas, subStrata);

        initBeanFilterableComboBox(ui.getLocationComboBox(), locations, location);

        initBeanList(ui.getSecondaryVesselList(),
                     Lists.<Vessel>newArrayList(),
                     model.getSecondaryVessel());

        // reset TrawlDistance when fishingOperationRectiligne pass to false
        // see https://forge.codelutin.com/issues/5807
        model.addPropertyChangeListener(EditFishingOperationUIModel.PROPERTY_FISHING_OPERATION_RECTILIGNE,
                                        evt -> {

                                            EditFishingOperationUIModel source = (EditFishingOperationUIModel) evt.getSource();
                                            Boolean newValue = (Boolean) evt.getNewValue();
                                            if (BooleanUtils.isFalse(newValue)) {
                                                source.setTrawlDistance(null);
                                            }
                                        });

        // Change vessel list and gear list when vessel type changes
        model.addPropertyChangeListener(EditFishingOperationUIModel.PROPERTY_SECONDARY_VESSEL_TYPE, evt -> {

            EditFishingOperationUIModel model1 =
                    (EditFishingOperationUIModel) evt.getSource();

            boolean modify = model1.isModify();

            SecondaryVesselTypeEnum vesselType =
                    (SecondaryVesselTypeEnum) evt.getNewValue();

            List<Vessel> vessels = Lists.newArrayList();

            if (vesselType != null) {
                switch (vesselType) {

                    case SCIENTIFIC:

                        vessels = model1.getScientificSecondaryVessel();
                        break;
                    case FISHING:

                        vessels = model1.getFishingSecondaryVessel();
                        break;
                    case ONLY_CRUISE:

                        vessels = model1.getOnlyCruisSecondaryVessel();
                        break;
                    case ALL:
                    default:

                        vessels = model1.getAllSecondaryVessel();
                }
            }

            // clean vessel filter
            EditFishingOperationUIHandler.this.ui.getSecondaryVesselList().getHandler().clearFilters();

            // get selected vessel
            List<Vessel> selected = Lists.newArrayList(EditFishingOperationUIHandler.this.ui.getSecondaryVesselList().getModel().getSelected());

            // set new universe of vessel
            EditFishingOperationUIHandler.this.ui.getSecondaryVesselList().getModel().setUniverse(vessels);

            // push back selected vessel
            EditFishingOperationUIHandler.this.ui.getSecondaryVesselList().getModel().setSelected(selected);

            // push back modify state
            model1.setModify(modify);
        });

        model.setSecondaryVesselType(null);

        changeValidatorContext(model.getValidationContext(), ui.getValidator());
        listenValidationTableHasNoFatalError(ui.getValidator(), model);

        setCustomTab(0, model);
        listModelIsModify(model);

        //init gear use features
        GearUseFeatureTabUIModel gearUseFeatureModel =
                ui.getGearUseFeatureTabContent().getModel();
        gearUseFeatureModel.setAvailableCaracteristics(getDataContext().getCaracteristics());
        setCustomTab(1, gearUseFeatureModel);
        listenModelModifiy(gearUseFeatureModel);

        //init vessel use features

        VesselUseFeatureTabUIModel vesselUseFeatureModel =
                ui.getVesselUseFeatureTabContent().getModel();
        vesselUseFeatureModel.setAvailableCaracteristics(getDataContext().getCaracteristics());
        listenModelModifiy(vesselUseFeatureModel);
        setCustomTab(2, vesselUseFeatureModel);

        // listen when id becomes empty or not to update cancel action and button
        getModel().addPropertyChangeListener(
                EditFishingOperationUIModel.PROPERTY_ID,
                evt -> {
                    JButton button = EditFishingOperationUIHandler.this.ui.getCancelButton();
                    CancelEditFishingOperationAction action =
                            getContext().getActionFactory().getLogicAction(button);
                    if (evt.getNewValue() == null) {
                        button.setText(t("tutti.editFishingOperation.action.cancelEditFishingOperation"));
                        button.setToolTipText(t("tutti.editFishingOperation.action.cancelEditFishingOperation.tip"));
                        button.setMnemonic(t("tutti.editFishingOperation.action.cancelEditFishingOperation.mnemonic").charAt(0));
                        action.setActionDescription(t("tutti.editFishingOperation.action.cancelEditFishingOperation.tip"));

                    } else {
                        button.setText(t("tutti.editFishingOperation.action.resetEditFishingOperation"));
                        button.setToolTipText(t("tutti.editFishingOperation.action.resetEditFishingOperation.tip"));
                        button.setMnemonic(t("tutti.editFishingOperation.action.resetEditFishingOperation.mnemonic").charAt(0));
                        action.setActionDescription(t("tutti.editFishingOperation.action.resetEditFishingOperation.tip"));
                    }
                });
    }

    @Override
    public JComponent getComponentToFocus() {
        return getUI().getStationNumberField();
    }

    @Override
    protected Set<String> getPropertiesToIgnore() {
        Set<String> result = super.getPropertiesToIgnore();
        result.add(EditFishingOperationUIModel.PROPERTY_ATTACHMENT);
        result.add(EditFishingOperationUIModel.PROPERTY_SECONDARY_VESSEL_TYPE);
        result.add(EditFishingOperationUIModel.PROPERTY_SECONDARY_VESSEL_TYPE_ALL);
        result.add(EditFishingOperationUIModel.PROPERTY_SECONDARY_VESSEL_TYPE_SCIENTIFIC);
        result.add(EditFishingOperationUIModel.PROPERTY_SECONDARY_VESSEL_TYPE_FISHING);
        result.add(EditFishingOperationUIModel.PROPERTY_SECONDARY_VESSEL_TYPE_ONLY_CRUISE);
        return result;
    }

    @Override
    public void onCloseUI() {

        if (log.isDebugEnabled()) {
            log.debug("closing: " + ui);
        }

        clearValidators();

        ui.getFishingOperationAttachmentsButton().onCloseUI();
        closeUI(ui.getGearUseFeatureTabContent());
        closeUI(ui.getVesselUseFeatureTabContent());
    }

    @Override
    public SwingValidator<EditFishingOperationUIModel> getValidator() {
        return ui.getValidator();
    }

    @Override
    public void setDate(ActionEvent event, String property) {
        JXDatePicker field = (JXDatePicker) event.getSource();
        Date value = field.getDate();
        Date date = (Date) JavaBeanObjectUtil.getProperty(getModel(), property);
        if (value != null && date != null) {
            Calendar cal = DateUtils.toCalendar(date);
            value = DateUtils.setHours(value, cal.get(Calendar.HOUR_OF_DAY));
            value = DateUtils.setMinutes(value, cal.get(Calendar.MINUTE));
        }
        JavaBeanObjectUtil.setProperty(getModel(), property, value);
    }

    //------------------------------------------------------------------------//
    //-- AbstractTuttiTabContainerUIHandler methods                         --//
    //------------------------------------------------------------------------//

    @Override
    public boolean onTabChanged(int currentIndex, int newIndex) {
        ui.getFishingOperationAttachmentsButton().onCloseUI();
        return super.onTabChanged(currentIndex, newIndex);
    }

    @Override
    public JTabbedPane getTabPanel() {
        return ui.getFishingOperationTabPane();
    }

    @Override
    public boolean removeTab(int i) {
        return false;
    }

    //------------------------------------------------------------------------//
    //-- TabHandler methods                                                 --//
    //------------------------------------------------------------------------//

    @Override
    public boolean onHideTab(int currentIndex, int newIndex) {
        boolean result;
        ui.getFishingOperationAttachmentsButton().onCloseUI();
        if (isAModelModified()) {

            // something was modified

            EditFishingOperationUIModel model = getModel();

            if (model.isValid()) {

                // ask if user want to save, do not save or cancel action

                String message;

                if (TuttiEntities.isNew(model.getFishingOperation())) {
                    message = t("tutti.editFishingOperation.askSaveBeforeLeaving.createFishingOperation");
                } else {
                    message = t("tutti.editFishingOperation.askSaveBeforeLeaving.saveFishingOperation");
                }

                int answer = askSaveBeforeLeaving(message);
//                ActionEvent event = new ActionEvent(this, newIndex, null);
                switch (answer) {
                    case JOptionPane.OK_OPTION:
                        getContext().getActionEngine().runAction(ui.getSaveButton());
                        result = true;
                        break;

                    case JOptionPane.NO_OPTION:
                        getContext().getActionEngine().runAction(ui.getCancelButton());
                        result = true;
                        break;
                    default:

                        // other case, use cancel action
                        result = false;
                }
            } else {

                // model is not valid, ask user to continue or not

                result = askCancelEditBeforeLeaving(
                        t("tutti.editFishingOperation.askCancelEditBeforeLeaving.cancelEditFishingOperation"));

                if (result) {
                    // ok will revert any modification
                    getContext().getActionEngine().runAction(ui.getCancelButton());
                }
            }

        } else {

            // model not modify, can change tab
            result = true;
        }
        return result;
    }

    @Override
    public void onShowTab(int currentIndex, int newIndex) {
        // validation if no operation
        // see http://forge.codelutin.com/issues/4991
        if (getModel().getFishingOperation() == null) {
            clearValidators();
        } else {
            registerValidators(ui.getValidator());
        }
    }

    @Override
    public boolean onRemoveTab() {
        return false;
    }

    //------------------------------------------------------------------------//
    //-- Public methods                                                     --//
    //------------------------------------------------------------------------//

    public void registerValidator() {
        registerValidators(ui.getValidator());
    }

    public AbstractCaracteristicTabUIModel[] getSubModels() {
        return new AbstractCaracteristicTabUIModel[]{
                ui.getGearUseFeatureTabContent().getModel(),
                ui.getVesselUseFeatureTabContent().getModel()
        };
    }

    public void uninstallStartDateListener() {
        getModel().removePropertyChangeListener(EditFishingOperationUIModel.PROPERTY_GEAR_SHOOTING_START_DATE, startDateListener);
    }

    public void installStartDateListener() {
        getModel().addPropertyChangeListener(EditFishingOperationUIModel.PROPERTY_GEAR_SHOOTING_START_DATE, startDateListener);
    }

    public void uninstallCoordinatesListener() {
        getModel().removePropertyChangeListener(coordinatePropertiesListener);
    }

    public void installCoordinatesListener() {
        getModel().addPropertyChangeListener(coordinatePropertiesListener);
    }

    public FishingOperationsUI getParentUi() {
        return getParentContainer(FishingOperationsUI.class);
    }

    public TuttiBeanMonitor<EditFishingOperationUIModel> getFishingOperationMonitor() {
        return fishingOperationMonitor;
    }

    public String getGearLatitudeLabelText(CoordinateEditorType coordinateEditorType) {
        return t("tutti.editFishingOperation.field.gearLatitude", coordinateEditorType.toString());
    }

    public String getGearLongitudeLabelText(CoordinateEditorType coordinateEditorType) {
        return t("tutti.editFishingOperation.field.gearLongitude", coordinateEditorType.toString());
    }

    public String getTrawlDistanceTooltipText(Integer trawlDistance) {
        return trawlDistance == null ?
                null :
                t("tutti.editFishingOperation.field.trawlDistance.inMilles.tip", Distances.getDistanceInMilles(Float.valueOf(trawlDistance)));
    }

    public String getDuration(Date startDate, Date endDate) {
        return DateTimes.getDuration(
                startDate,
                endDate,
                t("tutti.editFishingOperation.duration.format")
        );
    }

    public String decorateVessel(Vessel vessel) {
        return decorate(vessel);
    }

    //------------------------------------------------------------------------//
    //-- Protected methods                                                  --//
    //------------------------------------------------------------------------//

    protected void onSelectedStrata(EditFishingOperationUIModel model,
                                    TuttiLocation newStrata) {

        // reset substrata value
        model.setSubStrata(null);

        // reset sub strata combo
        ui.getSubStrataComboBox().setData(null);

        TuttiLocation zone = getDataContext().getProgram().getZone();

        String zoneId = zone.getId();
        String strataId = newStrata == null ? null : newStrata.getId();

        List<TuttiLocation> subStrata =
                getPersistenceService().getAllFishingOperationSubStrata(zoneId, strataId);

        List<TuttiLocation> location =
                getPersistenceService().getAllFishingOperationLocation(zoneId, strataId, null);

        ui.getSubStrataComboBox().setData(Lists.newArrayList(subStrata));

        ui.getLocationComboBox().setData(Lists.newArrayList(location));

        if (newStrata == null) {

            // reset strata, keep focus on it
            ui.getStrataComboBox().grabFocus();

        } else {

            // try to load substrata


            if (CollectionUtils.isEmpty(subStrata)) {

                // try to load localite
                ui.getLocationComboBox().grabFocus();
            }
        }
    }

    protected void onSelectedSubStrata(EditFishingOperationUIModel model,
                                       TuttiLocation newSubStrata) {
        // reset localite value
        model.setLocation(null);

        // reset localite combo
        ui.getLocationComboBox().setData(null);

        TuttiLocation zone = getDataContext().getProgram().getZone();
        String zoneId = zone.getId();

        TuttiLocation strata = model.getStrata();
        String subStrataId = newSubStrata == null ? null : newSubStrata.getId();

        String strataId = strata == null ? null : strata.getId();

        List<TuttiLocation> location =
                getPersistenceService().getAllFishingOperationLocation(zoneId, strataId, subStrataId);

        ui.getLocationComboBox().setData(Lists.newArrayList(location));

        if (newSubStrata == null) {

            // reset substrata, keep focus on it
            ui.getSubStrataComboBox().grabFocus();
        }
    }

    protected boolean areAllModelsValid() {
        boolean result = getModel().isValid();
        AbstractCaracteristicTabUIModel[] subModels = getSubModels();
        for (AbstractCaracteristicTabUIModel subModel : subModels) {
            result &= subModel.isValid();
        }
        return result;
    }

    protected boolean isAModelModified() {
        boolean result = getModel().isModify();
        AbstractCaracteristicTabUIModel[] subModels = getSubModels();
        for (AbstractCaracteristicTabUIModel subModel : subModels) {
            result |= subModel.isModify();
        }
        return result;
    }

    public void resetAllModels() {

        for (AbstractCaracteristicTabUIModel subModel : getSubModels()) {
            subModel.setModify(false);
        }
        getModel().setModify(false);
    }

}

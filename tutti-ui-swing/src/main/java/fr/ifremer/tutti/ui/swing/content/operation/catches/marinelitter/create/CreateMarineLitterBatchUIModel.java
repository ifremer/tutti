package fr.ifremer.tutti.ui.swing.content.operation.catches.marinelitter.create;

/*
 * #%L
 * Tutti :: UI
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import fr.ifremer.tutti.persistence.entities.data.MarineLitterBatch;
import fr.ifremer.tutti.persistence.entities.data.MarineLitterBatchs;
import fr.ifremer.tutti.persistence.entities.referential.CaracteristicQualitativeValue;
import fr.ifremer.tutti.type.WeightUnit;
import fr.ifremer.tutti.ui.swing.util.AbstractTuttiBeanUIModel;

/**
 * Model to create a new marine litter batch.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.3
 */
public class CreateMarineLitterBatchUIModel extends AbstractTuttiBeanUIModel<CreateMarineLitterBatchUIModel, CreateMarineLitterBatchUIModel> {

    private static final long serialVersionUID = 1L;

    /**
     * Delegate edit object.
     *
     * @since 1.3
     */
    protected final MarineLitterBatch editObject =
            MarineLitterBatchs.newMarineLitterBatch();

    /**
     * Already used categories by size category.
     *
     * @since 1.4
     */
    protected final Multimap<CaracteristicQualitativeValue, CaracteristicQualitativeValue> marineLitterCategoryUsed =
            ArrayListMultimap.create();

    private final WeightUnit weightUnit;

    public CreateMarineLitterBatchUIModel(WeightUnit weightUnit) {
        super(null, null);
        this.weightUnit = weightUnit;
    }

    public WeightUnit getWeightUnit() {
        return weightUnit;
    }

    public CaracteristicQualitativeValue getMarineLitterCategory() {
        return editObject.getMarineLitterCategory();
    }

    public void setMarineLitterCategory(CaracteristicQualitativeValue marineLitterCategory) {
        Object oldValue = getMarineLitterCategory();
        editObject.setMarineLitterCategory(marineLitterCategory);
        firePropertyChange(MarineLitterBatch.PROPERTY_MARINE_LITTER_CATEGORY, oldValue, marineLitterCategory);
    }

    public CaracteristicQualitativeValue getMarineLitterSizeCategory() {
        return editObject.getMarineLitterSizeCategory();
    }

    public void setMarineLitterSizeCategory(CaracteristicQualitativeValue marineLitterSizeCategory) {
        Object oldValue = getMarineLitterSizeCategory();
        editObject.setMarineLitterSizeCategory(marineLitterSizeCategory);
        firePropertyChange(MarineLitterBatch.PROPERTY_MARINE_LITTER_SIZE_CATEGORY, oldValue, marineLitterSizeCategory);
    }

    public Integer getNumber() {
        return editObject.getNumber();
    }

    public void setNumber(Integer number) {
        Object oldValue = getNumber();
        editObject.setNumber(number);
        firePropertyChange(MarineLitterBatch.PROPERTY_NUMBER, oldValue, number);
    }

    public Float getWeight() {
        return editObject.getWeight();
    }

    public void setWeight(Float weight) {
        Object oldValue = getWeight();
        editObject.setWeight(weight);
        firePropertyChange(MarineLitterBatch.PROPERTY_WEIGHT, oldValue, weight);
    }

    public Multimap<CaracteristicQualitativeValue, CaracteristicQualitativeValue> getMarineLitterCategoryUsed() {
        return marineLitterCategoryUsed;
    }

    public boolean isCategoryAndSizeCategoryAvailable() {
        CaracteristicQualitativeValue category = getMarineLitterCategory();
        CaracteristicQualitativeValue sizeCategory = getMarineLitterSizeCategory();

        return category != null && sizeCategory != null &&
                         isCategoryAndSizeCategoryAvailable(category, sizeCategory);
    }

    public boolean isCategoryAndSizeCategoryAvailable(CaracteristicQualitativeValue category,
                                                      CaracteristicQualitativeValue sizeCategory) {
        return !marineLitterCategoryUsed.containsEntry(sizeCategory, category);
    }

    @Override
    protected CreateMarineLitterBatchUIModel newEntity() {
        return new CreateMarineLitterBatchUIModel(weightUnit);
    }

    public void reset() {
        setMarineLitterCategory(null);
        setMarineLitterSizeCategory(null);
        setNumber(null);
        setWeight(null);
    }
}

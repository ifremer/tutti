package fr.ifremer.tutti.ui.swing.content.protocol.actions;

/*
 * #%L
 * Tutti :: UI
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Lists;
import fr.ifremer.tutti.persistence.entities.referential.Species;
import fr.ifremer.tutti.ui.swing.util.actions.LongActionSupport;
import fr.ifremer.tutti.ui.swing.content.protocol.EditProtocolUI;
import fr.ifremer.tutti.ui.swing.content.protocol.EditProtocolUIHandler;
import fr.ifremer.tutti.ui.swing.content.protocol.EditProtocolUIModel;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.List;

import static org.nuiton.i18n.I18n.t;

/**
 * Action to select a benthos which is not in the referent list.
 *
 * @author Kevin Morin - kmorin@codelutin.com
 * @since 1.1
 */
public class SelectOtherBenthosAction extends LongActionSupport<EditProtocolUIModel, EditProtocolUI, EditProtocolUIHandler> {

    /** Logger. */
    private static final Log log =
            LogFactory.getLog(SelectOtherBenthosAction.class);

    protected Species selectedSpecies;

    public SelectOtherBenthosAction(EditProtocolUIHandler handler) {
        super(handler, false);
    }

    @Override
    public boolean prepareAction() throws Exception {
        boolean result = super.prepareAction();
        if (result) {
            EditProtocolUIModel model = getModel();

            List<Species> species = Lists.newArrayList(model.getAllSynonyms());


            selectedSpecies = openAddSpeciesDialog(t("tutti.selectBenthos.title"), species);
//            selectedSpecies = getHandler().openSelectOtherSpeciesDialog(t("tutti.selectBenthos.title"), species);


            if (log.isInfoEnabled()) {
                log.info("SelectedBenthos: " + selectedSpecies);
            }
            result = selectedSpecies != null;
        }
        return result;
    }

    @Override
    public void releaseAction() {
        selectedSpecies = null;
        super.releaseAction();
    }

    @Override
    public void doAction() throws Exception {
        String decoratedSynonym = decorate(selectedSpecies);
        String taxonId = String.valueOf(selectedSpecies.getReferenceTaxonId());
        selectedSpecies = getModel().getAllReferentSpeciesByTaxonId().get(taxonId);
        String decoratedReferent = decorate(selectedSpecies);
        sendMessage(t("tutti.flash.info.benthos.replaced", decoratedSynonym, decoratedReferent));
        getUI().getBenthosComboBox().setSelectedItem(selectedSpecies);
    }

}

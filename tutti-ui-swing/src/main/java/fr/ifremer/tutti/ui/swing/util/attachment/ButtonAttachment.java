package fr.ifremer.tutti.ui.swing.util.attachment;

/*
 * #%L
 * Tutti :: UI
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.entities.data.Attachment;
import fr.ifremer.tutti.ui.swing.TuttiUIContext;
import jaxx.runtime.SwingUtil;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jdesktop.beans.AbstractSerializableBean;

import javax.swing.JToggleButton;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.Point;
import java.awt.event.HierarchyBoundsAdapter;
import java.awt.event.HierarchyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.beans.PropertyChangeListener;
import java.util.List;

import static org.nuiton.i18n.I18n.t;

/**
 * Button to edit attachments.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.1
 */
public class ButtonAttachment extends JToggleButton {

    private static final long serialVersionUID = 1L;

    /** Logger. */
    private static final Log log = LogFactory.getLog(ButtonAttachment.class);

    protected final AttachmentEditorUI popup;

    private transient PropertyChangeListener listenAttachmentsChanged;

//    protected Point popupPosition = null;

    protected boolean popupMoving;

    public ButtonAttachment(TuttiUIContext context, AttachmentModelAware model) {

        setIcon(SwingUtil.createActionIcon("edit-attachment"));
        setText(t("tutti.attachmentEditor.simpleText"));
        setToolTipText(t("tutti.attachmentEditor.action.tip"));

        popup = new AttachmentEditorUI(context);

        popup.addWindowListener(new WindowAdapter() {

            @Override
            public void windowOpened(WindowEvent e) {
                setSelected(true);
            }

            @Override
            public void windowClosing(WindowEvent e) {
                setSelected(false);
            }

            @Override
            public void windowClosed(WindowEvent e) {
                setSelected(false);
            }
        });

        addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                if (ButtonAttachment.this.isSelected()) {
                    if (!popup.isVisible()) {
                        popup.openEditor(ButtonAttachment.this);
                    }
                } else {
                    popup.closeEditor();
                }
            }
        });

        addHierarchyBoundsListener(new HierarchyBoundsAdapter() {

            @Override
            public void ancestorMoved(HierarchyEvent e) {
                if (popup.isShowing()) {

                    // place dialog just under the button
                    Point point = new Point(getLocationOnScreen());
                    point.translate(-popup.getWidth() + getWidth(), getHeight());
                    popupMoving = true;
                    try {
                        popup.setLocation(point);
                    } finally {
                        popupMoving = false;
                    }
                }
            }
        });
        if (log.isDebugEnabled()) {
            log.debug("Attach bean: " + model);
        }
        setBean(model);
    }

    public static String getButtonText(List<Attachment> attachment) {
        return t("tutti.attachmentEditor.text", attachment.size());
    }

    public void init() {
        popup.getHandler().init();
    }

    public void init(AttachmentModelAware model) {
        setBean(model);
        init();
    }

    public void onCloseUI() {
        setSelected(false);
    }

    public AttachmentModelAware getBean() {
        return popup.getBean();
    }

    protected void setBean(AttachmentModelAware model) {
        AttachmentModelAware bean = popup.getBean();
        if (bean != null) {
            ((AbstractSerializableBean) bean).removePropertyChangeListener(AttachmentModelAware.PROPERTY_ATTACHMENT, getListenAttachmentsChanged());
        }
        popup.setBean(model);

        if (model != null) {

            ((AbstractSerializableBean) model).addPropertyChangeListener(AttachmentModelAware.PROPERTY_ATTACHMENT, getListenAttachmentsChanged());
//            List<Attachment> attachment = model.getAttachment();
//            setText(ButtonAttachment.getButtonText(attachment));
        }
    }

    protected PropertyChangeListener getListenAttachmentsChanged() {
        if (listenAttachmentsChanged == null) {
            listenAttachmentsChanged = evt -> {
//                    List<Attachment> attachment = (List<Attachment>) evt.getNewValue();
//                    setText(getButtonText(attachment));
                if (!popup.isVisible()) {
                    init();
                }
            };
        }
        return listenAttachmentsChanged;
    }
}

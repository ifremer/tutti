package fr.ifremer.tutti.ui.swing.content.db.actions;

/*
 * #%L
 * Tutti :: UI
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.ui.swing.content.actions.AbstractMainUITuttiAction;
import fr.ifremer.tutti.ui.swing.content.MainUIHandler;

import static org.nuiton.i18n.I18n.t;

/**
 * To close a db.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.4
 */
public class CloseDbAction extends AbstractMainUITuttiAction {

    protected String jdbcUrl;

    public CloseDbAction(MainUIHandler handler) {
        super(handler, true);
    }

    @Override
    public boolean prepareAction() throws Exception {

        boolean canContinue = super.prepareAction();
        if (canContinue) {
            jdbcUrl = getConfig().getJdbcUrl();
        }
        return canContinue;
    }

    @Override
    public void doAction() {

        // close db
        getContext().getPersistenceService().clearAllCaches();

        // set to not reload a real db
        getContext().closePersistenceService();

        // clean db context
        getContext().clearDbContext();
    }

    @Override
    public void postSuccessAction() {
        super.postSuccessAction();

        sendMessage(t("tutti.flash.info.db.closed", jdbcUrl));

        // make sure title is reloaded
        handler.reloadDbManagerText();
        handler.changeTitle();
    }

    @Override
    public void postFailedAction(Throwable error) {
        handler.reloadDbManagerText();
        super.postFailedAction(error);
    }
}

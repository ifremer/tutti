package fr.ifremer.tutti.ui.swing.util;

/*
 * #%L
 * Tutti :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2016 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import jaxx.runtime.JAXXContext;
import jaxx.runtime.swing.editor.bean.BeanFilterableComboBox;

/**
 * @author Kevin Morin (Code Lutin)
 * @since 4.5
 */
public class WideDataBeanFilterableComboBox<O> extends BeanFilterableComboBox<O> {

    public WideDataBeanFilterableComboBox() {
        super();
    }

    public WideDataBeanFilterableComboBox(JAXXContext parentContext) {
        super(parentContext);
    }

    @Override
    protected void createCombobox() {
        super.createCombobox();
        // on change l'ui pour pouvoir redimensionner la liste mais pas sa popup
        combobox.setUI(new WideDataComboBoxUI(combobox.getPreferredSize()));
    }
}

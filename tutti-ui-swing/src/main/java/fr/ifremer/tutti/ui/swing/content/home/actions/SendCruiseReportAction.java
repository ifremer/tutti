package fr.ifremer.tutti.ui.swing.content.home.actions;

/*
 * #%L
 * Tutti :: UI
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import fr.ifremer.tutti.persistence.entities.data.Cruise;
import fr.ifremer.tutti.service.export.pdf.CatchesPdfExportService;
import fr.ifremer.tutti.ui.swing.content.MainUIHandler;
import fr.ifremer.tutti.ui.swing.content.actions.AbstractMainUITuttiAction;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.File;

import static org.nuiton.i18n.I18n.t;

/**
 * Opens a file chooser, exports the cruise catches into the selected file and open the default email editor.
 *
 * @author Kevin Morin - kmorin@codelutin.com
 * @since 1.0
 */
public class SendCruiseReportAction extends AbstractMainUITuttiAction {

    /** Logger. */
    private static final Log log =
            LogFactory.getLog(SendCruiseReportAction.class);

    protected File file;

    public SendCruiseReportAction(MainUIHandler handler) {
        super(handler, true);
    }

    @Override
    public boolean prepareAction() throws Exception {

        boolean doAction = super.prepareAction();

        if (doAction) {

            Cruise cruise = getDataContext().getCruise();
            // choose file to export
            file = saveFile(
                    "exportCruise-" + cruise.getName(),
                    "pdf",
                    t("tutti.sendCruiseReport.title.choose.exportFile"),
                    t("tutti.sendCruiseReport.action.chooseFile"),
                    "^.+\\.pdf$", t("tutti.common.file.pdf")
            );
            doAction = file != null;
        }
        return doAction;
    }

    @Override
    public void releaseAction() {
        file = null;
        super.releaseAction();
    }

    @Override
    public void doAction() throws Exception {
        Cruise cruise = getDataContext().getCruise();
        Preconditions.checkNotNull(cruise);
        Preconditions.checkNotNull(file);

        if (log.isInfoEnabled()) {
            log.info("Will export cruise " + cruise.getId() +
                     " to file: " + file);
        }

        // export catches
        CatchesPdfExportService service =
                getContext().getGeneratePDFService();
        service.generateCruisePDFFile(file, cruise.getIdAsInt(), getConfig().getI18nLocale());

    }

    @Override
    public void postSuccessAction() {
        super.postSuccessAction();
        sendMessage(t("tutti.sendCruiseReport.action.success", file));
    }
}

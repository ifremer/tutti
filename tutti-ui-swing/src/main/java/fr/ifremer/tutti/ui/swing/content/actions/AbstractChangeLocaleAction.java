package fr.ifremer.tutti.ui.swing.content.actions;

/*
 * #%L
 * Tutti :: UI
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.ui.swing.content.MainUIHandler;
import org.nuiton.i18n.I18n;

import java.util.Locale;

/**
 * TODO
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.0.3
 */
public abstract class AbstractChangeLocaleAction extends AbstractChangeScreenAction {


    protected AbstractChangeLocaleAction(MainUIHandler handler) {
        super(handler, true, null);
    }

    protected abstract Locale getLocale();

    @Override
    public void doAction() throws Exception {

        Locale locale = getLocale();

        // change locale (and save configuration)
        getModel().setLocale(locale);

        // change i18n locale
        I18n.setDefaultLocale(getConfig().getI18nLocale());

        // reload decorator service (TODO Check if this is necessary)
        getContext().reloadDecoratorService();

        // close reload
        getHandler().reloadUI();

    }
}

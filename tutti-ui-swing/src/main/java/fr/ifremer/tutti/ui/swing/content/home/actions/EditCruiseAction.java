package fr.ifremer.tutti.ui.swing.content.home.actions;

/*
 * #%L
 * Tutti :: UI
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import fr.ifremer.tutti.service.ValidationService;
import fr.ifremer.tutti.ui.swing.TuttiScreen;
import fr.ifremer.tutti.ui.swing.content.actions.AbstractChangeScreenAction;
import fr.ifremer.tutti.ui.swing.content.MainUIHandler;

/**
 * Opens the cruise edition screen.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.0
 */
public class EditCruiseAction extends AbstractChangeScreenAction {

    public EditCruiseAction(MainUIHandler handler) {
        super(handler, true, TuttiScreen.EDIT_CRUISE);
    }

    @Override
    public void doAction() throws Exception {
        Preconditions.checkState(getContext().isProgramFilled());
        Preconditions.checkState(getContext().isCruiseFilled());
        getContext().setValidationContext(ValidationService.VALIDATION_CONTEXT_EDIT);
        loadReferantials(true);
        super.doAction();
    }
}

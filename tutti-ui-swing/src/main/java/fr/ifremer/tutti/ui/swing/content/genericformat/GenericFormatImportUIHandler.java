package fr.ifremer.tutti.ui.swing.content.genericformat;

/*
 * #%L
 * Tutti :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import fr.ifremer.tutti.persistence.entities.data.Program;
import fr.ifremer.tutti.persistence.model.ProgramDataModel;
import fr.ifremer.tutti.service.genericformat.GenericFormatValidateFileResult;
import fr.ifremer.tutti.ui.swing.content.genericformat.tree.DataSelectTreeModel;
import fr.ifremer.tutti.ui.swing.content.genericformat.tree.ProgramSelectTreeNode;
import fr.ifremer.tutti.ui.swing.util.AbstractTuttiUIHandler;
import jaxx.runtime.validator.swing.SwingValidator;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.jaxx.application.swing.util.CloseableUI;

import javax.swing.JComponent;
import javax.swing.JTree;
import javax.swing.ToolTipManager;
import javax.swing.event.TreeModelEvent;
import javax.swing.event.TreeModelListener;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreeSelectionModel;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Set;

/**
 * Created on 2/24/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.14
 */
public class GenericFormatImportUIHandler extends AbstractTuttiUIHandler<GenericFormatImportUIModel, GenericFormatImportUI> implements CloseableUI {

    /** Logger. */
    private static final Log log = LogFactory.getLog(GenericFormatImportUIHandler.class);

    @Override
    public void beforeInit(GenericFormatImportUI ui) {

        super.beforeInit(ui);

        getDataContext().resetValidationDataContext();

        GenericFormatImportUIModel model = new GenericFormatImportUIModel();

        if (getContext().isProgramFilled()) {

            String programId = getContext().getProgramId();

            if (log.isInfoEnabled()) {
                log.info("Using selected program " + programId);
            }
            // load existing program
            Program program = getPersistenceService().getProgram(programId);

            model.setProgram(program);
        }

        ui.setContextValue(model);

        model.addPropertyChangeListener(new PropertyChangeListener() {

            final Set<String> propertyNamesToCanValidate = Sets.newHashSet(GenericFormatImportUIModel.PROPERTY_IMPORT_FILE,
                    GenericFormatImportUIModel.PROPERTY_PROGRAM,
                    GenericFormatImportUIModel.PROPERTY_AUTHORIZE_OBSOLETE_REFERENTIALS);

            final Set<String> propertyNamesToCanImport = Sets.newHashSet(GenericFormatImportUIModel.PROPERTY_VALIDATE_DONE,
                    GenericFormatImportUIModel.PROPERTY_PROGRAM,
                    GenericFormatImportUIModel.PROPERTY_VALIDATE_RESULT,
                    GenericFormatImportUIModel.PROPERTY_DATA_SELECTED);

            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                GenericFormatImportUIModel source = (GenericFormatImportUIModel) evt.getSource();
                String propertyName = evt.getPropertyName();

                if (propertyNamesToCanValidate.contains(propertyName)) {

                    boolean canValidate = source.computeIsCanValidate();
                    source.setCanValidate(canValidate);
                    source.setValidateResult(null);
                    source.setValidateReportFile(null);

                }

                if (propertyNamesToCanImport.contains(propertyName)) {

                    boolean canImport = source.computeIsCanImport();
                    source.setCanImport(canImport);
                    source.setImportResult(null);
                    source.setImportReportFile(null);

                }

                if (GenericFormatImportUIModel.PROPERTY_CAN_VALIDATE.equals(propertyName)) {

                    if (!(boolean) evt.getNewValue()) {
                        getUI().getValidateResultPanel().setVisible(false);
                    }
                }

                if (GenericFormatImportUIModel.PROPERTY_CAN_IMPORT.equals(propertyName)) {

                    if (!(boolean) evt.getNewValue()) {
                        getUI().getImportResultPanel().setVisible(false);
                    }
                }

                if (GenericFormatImportUIModel.PROPERTY_VALIDATE_RESULT.equals(propertyName)) {

                    onValidateResultChanged((GenericFormatValidateFileResult) evt.getNewValue());

                }
            }
        });

    }

    @Override
    public void afterInit(GenericFormatImportUI ui) {

        initUI(ui);

        JTree tree = ui.getDataSelectionTree();
        tree.getSelectionModel().setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);
        ToolTipManager.sharedInstance().registerComponent(tree);
        DataSelectTreeModel.installDataSelectionHandler(tree);

        DataSelectTreeModel treeModel = ui.getTreeModel();
        treeModel.addTreeModelListener(new TreeModelListener() {
            @Override
            public void treeNodesChanged(TreeModelEvent e) {

                DataSelectTreeModel source = (DataSelectTreeModel) e.getSource();
                boolean dataSelected = source.isDataSelected();
                getModel().setDataSelected(dataSelected);

            }

            @Override
            public void treeNodesInserted(TreeModelEvent e) {
            }

            @Override
            public void treeNodesRemoved(TreeModelEvent e) {
            }

            @Override
            public void treeStructureChanged(TreeModelEvent e) {

                DataSelectTreeModel source = (DataSelectTreeModel) e.getSource();
                boolean dataSelected = source.isDataSelected();
                getModel().setDataSelected(dataSelected);

            }
        });

        GenericFormatImportUIModel model = getModel();
        initBeanFilterableComboBox(ui.getProgramComboBox(),
                Lists.newArrayList(getPersistenceService().getAllProgram()),
                model.getProgram());

        SwingValidator validator = ui.getValidator();

        registerValidators(validator);

    }

    @Override
    protected JComponent getComponentToFocus() {
        return getUI().getProgramComboBox();
    }

    @Override
    public void onCloseUI() {
        if (log.isDebugEnabled()) {
            log.debug("closing: " + ui);
        }
        clearValidators();
    }

    @Override
    public boolean quitUI() {
        return true;
    }

    @Override
    public SwingValidator<GenericFormatImportUIModel> getValidator() {
        return ui.getValidator();
    }

    private void onValidateResultChanged(GenericFormatValidateFileResult validateFileResult) {

        ProgramSelectTreeNode root;
        if (validateFileResult == null) {

            root = null;

        } else {

            ProgramDataModel dataModel = validateFileResult.getDataModel();
            root = new ProgramSelectTreeNode(dataModel);

        }

        DefaultTreeModel treeModel = (DefaultTreeModel) ui.getDataSelectionTree().getModel();
        treeModel.setRoot(root);
        getModel().setRootNode(root);

    }

}
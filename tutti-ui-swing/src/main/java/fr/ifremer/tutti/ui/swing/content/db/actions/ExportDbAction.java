package fr.ifremer.tutti.ui.swing.content.db.actions;

/*
 * #%L
 * Tutti :: UI
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import fr.ifremer.tutti.persistence.ProgressionModel;
import fr.ifremer.tutti.ui.swing.content.actions.AbstractMainUITuttiAction;
import fr.ifremer.tutti.ui.swing.content.MainUIHandler;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import static org.nuiton.i18n.I18n.t;

/**
 * To export a db attached to Tutti.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.0
 */
public class ExportDbAction extends AbstractMainUITuttiAction {

    /** Logger. */
    private static final Log log = LogFactory.getLog(ExportDbAction.class);

    public static final int TOTAL_STEP = 3;

    protected boolean noUI;

    protected File file;

    public static final DateFormat df = new SimpleDateFormat("yyy-MM-dd");

    public ExportDbAction(MainUIHandler handler) {
        super(handler, true);
    }

    public void setFile(File file) {
        this.file = file;
    }

    public void setNoUI(boolean noUI) {
        this.noUI = noUI;
    }

    @Override
    public boolean prepareAction() throws Exception {
        file = null;
        boolean doAction = super.prepareAction();

        if (doAction) {

            // ask user file where to export db
            file = saveFile(
                    "tutti-db-" + df.format(new Date()),
                    "zip",
                    t("tutti.dbManager.title.choose.dbExportFile"),
                    t("tutti.dbManager.action.chooseDbExportFile"),
                    "^.*\\.zip", t("tutti.common.file.zip")
            );
            doAction = file != null;
        }

        if (doAction) {

            ProgressionModel progressionModel = new ProgressionModel();
            setProgressionModel(progressionModel);
            progressionModel.setTotal(TOTAL_STEP);
        }
        return doAction;
    }

    @Override
    public void doAction() {
        Preconditions.checkNotNull(file);
        if (log.isInfoEnabled()) {
            log.info("Will export db to " + file);
        }

        ProgressionModel progressionModel = getProgressionModel();

        // close db

        progressionModel.setMessage(t("tutti.exportDb.step.closeDb"));

        getContext().closePersistenceService();

        // export

        progressionModel.increments(t("tutti.exportDb.step.createArchive", file));

        getContext().getPersistenceService().exportDb(file);

        // reopen db

        progressionModel.increments(t("tutti.exportDb.step.openDb"));

        getContext().openPersistenceService();
    }

    @Override
    public void postSuccessAction() {
        super.postSuccessAction();
        sendMessage(t("tutti.flash.info.db.exported", file));

        if (!noUI) {

            // make sure title is reloaded
            getUI().getHandler().changeTitle();

        }

    }
}

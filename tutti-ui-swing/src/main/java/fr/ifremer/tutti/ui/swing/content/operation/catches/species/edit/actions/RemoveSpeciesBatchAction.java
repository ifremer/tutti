package fr.ifremer.tutti.ui.swing.content.operation.catches.species.edit.actions;

/*
 * #%L
 * Tutti :: UI
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import fr.ifremer.tutti.persistence.entities.TuttiEntities;
import fr.ifremer.tutti.ui.swing.content.operation.catches.species.edit.SpeciesBatchRowModel;
import fr.ifremer.tutti.ui.swing.content.operation.catches.species.edit.SpeciesBatchTableModel;
import fr.ifremer.tutti.ui.swing.content.operation.catches.species.edit.SpeciesBatchUI;
import fr.ifremer.tutti.ui.swing.content.operation.catches.species.edit.SpeciesBatchUIHandler;
import fr.ifremer.tutti.ui.swing.content.operation.catches.species.edit.SpeciesBatchUIModel;
import fr.ifremer.tutti.ui.swing.util.TuttiUIUtil;
import fr.ifremer.tutti.ui.swing.util.actions.LongActionSupport;
import jaxx.runtime.SwingUtil;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jdesktop.swingx.JXTable;

import javax.swing.JOptionPane;
import java.util.HashSet;
import java.util.Set;

import static org.nuiton.i18n.I18n.t;

/**
 * To remove a species batch and all his children.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.0
 */
public class RemoveSpeciesBatchAction extends LongActionSupport<SpeciesBatchUIModel, SpeciesBatchUI, SpeciesBatchUIHandler> {

    /** Logger. */
    private static final Log log = LogFactory.getLog(RemoveSpeciesBatchAction.class);

    /**
     * Rows to delete after all.
     *
     * @since 3.0-rc-1
     */
    protected Set<SpeciesBatchRowModel> rowToRemove;

    public RemoveSpeciesBatchAction(SpeciesBatchUIHandler handler) {
        super(handler, false);
    }

    @Override
    public boolean prepareAction() throws Exception {
        boolean result = super.prepareAction();

        if (result) {

            int[] selectedRowIndexes = SwingUtil.getSelectedModelRows(handler.getTable());
            int selectedRowCount = selectedRowIndexes.length;

            if (selectedRowCount > 0) {

                String message;
                if (selectedRowCount == 1) {
                    message = t("tutti.editSpeciesBatch.action.removeBatch.confirm.message");
                } else {
                    message = t("tutti.editSpeciesBatch.action.removeBatches.confirm.message", selectedRowCount);
                }

                int answer = JOptionPane.showConfirmDialog(getContext().getActionUI(),
                                                           message,
                                                           t("tutti.editSpeciesBatch.action.removeBatch.confirm.title"),
                                                           JOptionPane.YES_NO_OPTION);
                result = answer == JOptionPane.YES_OPTION;

            } else {
                result = false;
            }
        }

        return result;
    }

    @Override
    public void doAction() throws Exception {

        int[] selectedRowIndexes = SwingUtil.getSelectedModelRows(handler.getTable());

        Preconditions.checkState(selectedRowIndexes.length > 0, "Cant remove batches if no batches selected");

        SpeciesBatchTableModel tableModel = handler.getTableModel();

        rowToRemove = new HashSet<>();

        for (int rowIndex : selectedRowIndexes) {

            SpeciesBatchRowModel selectedBatch = tableModel.getEntry(rowIndex);

            Preconditions.checkState(!TuttiEntities.isNew(selectedBatch), "Can't remove batch if batch is not persisted");

            Integer selectedBatchId = selectedBatch.getIdAsInt();
            boolean batchRoot = selectedBatch.isBatchRoot();
            if (!batchRoot && rowToRemove.contains(selectedBatch)) {

                // This batch was already removed
                if (log.isInfoEnabled()) {
                    log.info("Skip to delete species batch: " + selectedBatchId + ", was already removed when his parent was deleted: " + selectedBatch.getParentBatch().getId());
                }
                continue;
            }

            if (log.isInfoEnabled()) {
                log.info("Delete species batch: " + selectedBatchId);
            }

            // remove selected batch and all his children
            getModel().getSpeciesOrBenthosBatchUISupport() .deleteSpeciesBatch(selectedBatchId);

            if (batchRoot) {
                // update speciesUsed
                handler.removeFromSpeciesUsed(selectedBatch);
            } else {

                // remove from his parent
                SpeciesBatchRowModel parentBatch = selectedBatch.getParentBatch();
                parentBatch.getChildBatch().remove(selectedBatch);
            }

            // collect of rows to remove from model
            rowToRemove.add(selectedBatch);

            handler.collectChildren(selectedBatch, rowToRemove);
        }
    }

    @Override
    public void postSuccessAction() {
        super.postSuccessAction();

        JXTable table = handler.getTable();

        // remove all rows from the model
        getModel().getRows().removeAll(rowToRemove);

        // refresh table from parent batch row index to the end
        handler.getTableModel().fireTableDataChanged();

        // select parent batch row
        TuttiUIUtil.selectFirstCellOnFirstRowAndStopEditing(table);
    }
}

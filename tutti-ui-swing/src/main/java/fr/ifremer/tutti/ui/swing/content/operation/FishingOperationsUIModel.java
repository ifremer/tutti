package fr.ifremer.tutti.ui.swing.content.operation;

/*
 * #%L
 * Tutti :: UI
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import fr.ifremer.tutti.persistence.entities.TuttiEntities;
import fr.ifremer.tutti.persistence.entities.data.FishingOperation;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jdesktop.beans.AbstractSerializableBean;

import java.util.List;

/**
 * Model fo UI {@link FishingOperationsUI}.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 0.1
 */
public class FishingOperationsUIModel extends AbstractSerializableBean {

    private static final long serialVersionUID = 1L;

    /** Logger. */
    private static final Log log =
            LogFactory.getLog(FishingOperationsUIModel.class);

    public static final String PROPERTY_FISHING_OPERATION = "fishingOperation";

    public static final String PROPERTY_SELECTED_FISHING_OPERATION = "selectedFishingOperation";

    public static final String PROPERTY_EDITED_FISHING_OPERATION = "editedFishingOperation";

    public static final String PROPERTY_CATCH_ENABLED = "catchEnabled";

//    public static final String PROPERTY_CATCH_NOT_FOUND = "catchNotFound";
//
//    public static final String PROPERTY_SAMPLE_CATCH_MODEL_VALID = "sampleCatchModelValid";

    public static final String PROPERTY_VALIDATION_ERROR_MESSAGE = "validationErrorMessage";

    /**
     * List of existing fishing operation for the selected cruise.
     *
     * <strong>Note:</strong> These objects are not fully loaded, they will
     * never be edited.
     *
     * @since 0.1
     */
    protected List<FishingOperation> fishingOperation;

    /**
     * Selected fishing operation in the combo box.
     *
     * For example if you create a new fishing operation then
     * {@code selectedFishingOperation} will be {@code null}.
     *
     * <strong>Note:</strong> These object ise not fully loaded, they will
     * never be edited.
     *
     * @since 0.1
     */
    protected FishingOperation selectedFishingOperation;

    /**
     * Current edited fishing operation.
     *
     * @since 1.0
     */
    protected FishingOperation editFishingOperation;

    /**
     * Flag to not listen the {@link #selectedFishingOperation}
     * changes while adjusting the model.
     *
     * When flag is {@code true}, then the changes of the
     * {@link #selectedFishingOperation} should not trigger any changes.
     *
     * @since 1.0
     */
    protected boolean selectionAdjusting;

    /**
     * Flag to not listen the {@link #editFishingOperation}
     * changes while adjusting the model.
     *
     * When flag is {@code true}, then the changes of the
     * {@link #editFishingOperation} should not trigger any changes.
     *
     * @since 1.0
     */
    protected boolean editionAdjusting;

    /**
     * Flag to use or not catch tab.
     *
     * @since 1.1
     */
    protected boolean catchEnabled = true;

    /**
     * contains if any error message while loading the fishing operation catch batch.
     *
     * @since 3.5
     */
    protected String validationErrorMessage;

    public List<FishingOperation> getFishingOperation() {
        return fishingOperation;
    }

    public void setFishingOperation(List<FishingOperation> fishingOperation) {
        Object oldValue = getFishingOperation();
        this.fishingOperation = fishingOperation;
        firePropertyChange(PROPERTY_FISHING_OPERATION, oldValue, fishingOperation);
    }

    public void addFishingOperation(FishingOperation fishingOperation) {
        Object oldValue = Lists.newArrayList(getFishingOperation());
        this.fishingOperation.add(fishingOperation);
        firePropertyChange(PROPERTY_FISHING_OPERATION, oldValue, this.fishingOperation);
    }

    public void removeFishingOperation(FishingOperation fishingOperation) {
        Object oldValue = Lists.newArrayList(getFishingOperation());
        this.fishingOperation.remove(fishingOperation);
        firePropertyChange(PROPERTY_FISHING_OPERATION, oldValue, this.fishingOperation);
    }

    public void updateFishingOperation(FishingOperation newFishingOperation) {

        Preconditions.checkNotNull(newFishingOperation);
        String id = newFishingOperation.getId();
        Preconditions.checkNotNull(id);
        FishingOperation oldFishingOperation = getFishingOperation(id);
        Preconditions.checkNotNull(oldFishingOperation);

        if (log.isInfoEnabled()) {
            log.info("Update existing fishing operation: " + id);
        }

        int oldFishingOperationIndex = fishingOperation.indexOf(oldFishingOperation);
        fishingOperation.remove(oldFishingOperation);
        fishingOperation.add(oldFishingOperationIndex, newFishingOperation);
        firePropertyChange(PROPERTY_FISHING_OPERATION, null, fishingOperation);
    }

    public FishingOperation getSelectedFishingOperation() {
        return selectedFishingOperation;
    }

    public void setSelectedFishingOperation(FishingOperation selectedFishingOperation) {
        Object oldValue = getSelectedFishingOperation();
        this.selectedFishingOperation = selectedFishingOperation;
        if ((oldValue != null || selectedFishingOperation != null)
            && !isSelectionAdjusting()) {

            // only fires when authorize to
            firePropertyChange(PROPERTY_SELECTED_FISHING_OPERATION, oldValue, selectedFishingOperation);
        }
    }

    public FishingOperation getEditFishingOperation() {
        return editFishingOperation;
    }

    public void setEditFishingOperation(FishingOperation editFishingOperation) {
        Object oldValue = getEditFishingOperation();
        this.editFishingOperation = editFishingOperation;
        if (!isEditionAdjusting()) {

            // only fires when authorize to
            firePropertyChange(PROPERTY_EDITED_FISHING_OPERATION, oldValue, editFishingOperation);
        }
    }

    public boolean isSelectionAdjusting() {
        return selectionAdjusting;
    }

    public void setSelectionAdjusting(boolean selectionAdjusting) {
        this.selectionAdjusting = selectionAdjusting;
    }

    public boolean isEditionAdjusting() {
        return editionAdjusting;
    }

    public void setEditionAdjusting(boolean editionAdjusting) {
        this.editionAdjusting = editionAdjusting;
    }

    public FishingOperation getFishingOperation(String id) {
        return TuttiEntities.findById(fishingOperation, id);
    }

    public boolean isCatchEnabled() {
        return catchEnabled;
    }

    public void setCatchEnabled(boolean catchEnabled) {
        boolean oldValue = isCatchEnabled();
        this.catchEnabled = catchEnabled;
        firePropertyChange(PROPERTY_CATCH_ENABLED, oldValue, catchEnabled);
    }

    public String getValidationErrorMessage() {
        return validationErrorMessage;
    }

    public void setValidationErrorMessage(String validationErrorMessage) {
        String oldValue = getValidationErrorMessage();
        this.validationErrorMessage = validationErrorMessage;
        firePropertyChange(PROPERTY_VALIDATION_ERROR_MESSAGE, oldValue, validationErrorMessage);
    }
}

package fr.ifremer.tutti.ui.swing.content.home.actions;

/*
 * #%L
 * Tutti :: UI
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import fr.ifremer.tutti.persistence.entities.protocol.TuttiProtocol;
import fr.ifremer.tutti.service.protocol.ProtocolImportExportService;
import fr.ifremer.tutti.ui.swing.util.actions.LongActionSupport;
import fr.ifremer.tutti.ui.swing.content.home.SelectCruiseUI;
import fr.ifremer.tutti.ui.swing.content.home.SelectCruiseUIHandler;
import fr.ifremer.tutti.ui.swing.content.home.SelectCruiseUIModel;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.File;

import static org.nuiton.i18n.I18n.t;

/**
 * Opens a file chooser and exports the protocol into the selected file.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.0
 */
public class ExportProtocolAction extends LongActionSupport<SelectCruiseUIModel, SelectCruiseUI, SelectCruiseUIHandler> {

    /** Logger. */
    private static final Log log =
            LogFactory.getLog(ExportProtocolAction.class);

    protected File file;

    public ExportProtocolAction(SelectCruiseUIHandler handler) {
        super(handler, true);
    }

    @Override
    public boolean prepareAction() throws Exception {

        boolean doAction = super.prepareAction();

        if (doAction) {

            TuttiProtocol protocol = getModel().getProtocol();
            // choose file to export
            file = saveFile(
                    "protocol-" + protocol.getName().replaceAll(" ", "_"),
                    "tuttiProtocol",
                    t("tutti.selectCruise.title.choose.exportProtocolFile"),
                    t("tutti.selectCruise.action.exportProtocol"),
                    "^.+\\.tuttiProtocol$", t("tutti.common.file.protocol")
            );
            doAction = file != null;
        }
        return doAction;
    }

    @Override
    public void releaseAction() {
        file = null;
        super.releaseAction();
    }

    @Override
    public void doAction() throws Exception {
        TuttiProtocol protocol = getModel().getProtocol();
        Preconditions.checkNotNull(protocol);
        Preconditions.checkNotNull(file);

        if (log.isInfoEnabled()) {
            log.info("Will save protocol " + protocol.getId() +
                     " to file: " + file);
        }

        // export protocol
        ProtocolImportExportService service =
                getContext().getTuttiProtocolImportExportService();
        service.exportProtocol(protocol, file);

    }

    @Override
    public void postSuccessAction() {

        getHandler().resetComboBoxAction(getUI().getEditProtocolComboBox());

        super.postSuccessAction();
        TuttiProtocol protocol = getModel().getProtocol();
        sendMessage(t("tutti.exportProtocol.action.success", protocol.getName(), file.getName()));

    }
}

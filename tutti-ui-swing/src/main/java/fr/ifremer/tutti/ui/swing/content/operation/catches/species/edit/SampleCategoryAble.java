package fr.ifremer.tutti.ui.swing.content.operation.catches.species.edit;

/*
 * #%L
 * Tutti :: UI
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.entities.data.SampleCategory;

import java.io.Serializable;

/**
 * Contract for model using {@link SampleCategory}.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.4
 */
public interface SampleCategoryAble<E extends SampleCategoryAble> extends Iterable<SampleCategory<?>> {

    Integer getCategoryIndex(Integer id);

    void setSampleCategory(SampleCategory sampleCategory);

    SampleCategory<?> getFirstSampleCategory();

    SampleCategory getFinestCategory();

    SampleCategory getSampleCategoryById(Integer sampleCategoryId);

    SampleCategory<?> getSampleCategoryByIndex(int sampleCategoryIndex);

    Serializable getSampleCategoryValue(Integer sampleCategoryId);

    void setSampleCategoryValue(Integer sampleCategoryId, Serializable value);

    void setSampleCategoryWeight(Integer sampleCategoryId, Object value);

    E getFirstAncestor(SampleCategory<?> entrySampleCategory);
}

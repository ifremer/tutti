package fr.ifremer.tutti.ui.swing.content.db.actions;

/*
 * #%L
 * Tutti :: UI
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.TuttiConfiguration;
import fr.ifremer.tutti.persistence.ProgressionModel;
import fr.ifremer.tutti.persistence.TuttiPersistence;
import fr.ifremer.tutti.persistence.service.UpdateSchemaContextSupport;
import fr.ifremer.tutti.ui.swing.TuttiScreen;
import fr.ifremer.tutti.ui.swing.content.actions.AbstractChangeScreenAction;
import fr.ifremer.tutti.ui.swing.content.MainUIHandler;
import fr.ifremer.tutti.ui.swing.update.TuttiDbUpdaterCallBack;
import fr.ifremer.tutti.ui.swing.update.Updates;
import fr.ifremer.tutti.ui.swing.util.AbstractTuttiUIHandler;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.jaxx.application.ApplicationBusinessException;
import org.nuiton.jaxx.application.swing.action.ApplicationUIAction;
import org.nuiton.updater.ApplicationInfo;
import org.nuiton.version.Version;

import javax.swing.JOptionPane;
import java.io.File;
import java.util.Date;

import static org.nuiton.i18n.I18n.t;

/**
 * To open existing db.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.0
 */
public class OpenDbAction extends AbstractChangeScreenAction {

    /** Logger. */
    private static final Log log = LogFactory.getLog(OpenDbAction.class);

    protected UpdateSchemaContext updateSchemaContext;

    protected boolean updateReferentiel;

    protected String jdbcUrl;

    protected ApplicationInfo updateDbVersion;

    public OpenDbAction(MainUIHandler handler) {
        super(handler, true, TuttiScreen.SELECT_CRUISE);
        setActionDescription(t("tutti.dbManager.action.openDb.tip"));
    }

    public void setUpdateReferentiel(boolean updateReferentiel) {
        this.updateReferentiel = updateReferentiel;
    }

    @Override
    public boolean prepareAction() throws Exception {

        boolean canContinue = super.prepareAction();
        if (canContinue) {

            jdbcUrl = null;
            updateSchemaContext = null;
            updateDbVersion = null;

            updateReferentiel &= getContext().checkUpdateDataReachable(false);

            if (updateReferentiel) {

                // get the next db version
                updateDbVersion = Updates.getDatabaseUpdateVersion(getConfig());

                if (updateDbVersion != null && updateDbVersion.newVersion != null) {

                    // ask user if it wants to do the update
                    String htmlMessage = String.format(
                            AbstractTuttiUIHandler.CONFIRMATION_FORMAT,
                            t("tutti.dbManager.updatedb.found", updateDbVersion.newVersion),
                            t("tutti.common.askBeforeUpdate.help"));
                    int i = JOptionPane.showConfirmDialog(
                            getHandler().getTopestUI(),
                            htmlMessage,
                            t("tutti.dbManager.title.confirm.updatedb"),
                            JOptionPane.OK_CANCEL_OPTION,
                            JOptionPane.QUESTION_MESSAGE);

                    updateReferentiel = i == JOptionPane.OK_OPTION;
                }

            }
        }
        return canContinue;
    }

    public class UpdateSchemaContext extends UpdateSchemaContextSupport {

        private boolean closeDb;

        private File file;

        private final File dbBackupDirectory;

        public UpdateSchemaContext(File dbBackupDirectory) {
            this.dbBackupDirectory = dbBackupDirectory;
        }

        @Override
        protected boolean askUserToMigrate(Version schemaVersion, Version schemaVersionIfUpdate) {

            // database schema need to migrate
            // ask user to migrate or not (if not will close db)

            // ask user where to backup db

            String message = t("tutti.dbManager.action.upgradeDb.schema.to.update.message", schemaVersion, schemaVersionIfUpdate);

            String htmlMessage = String.format(
                    AbstractTuttiUIHandler.CONFIRMATION_FORMAT,
                    message,
                    t("tutti.dbManager.action.upgradeDb.schema.to.update.message.help"));

            int i = JOptionPane.showConfirmDialog(
                    getHandler().getTopestUI(),
                    htmlMessage,
                    t("tutti.dbManager.title.schema.toupdate"),
                    JOptionPane.OK_CANCEL_OPTION,
                    JOptionPane.QUESTION_MESSAGE);

            boolean continueAction = i == JOptionPane.OK_OPTION;

            if (continueAction) {

                // ask user file where to backup db

                file = saveFile(
                        dbBackupDirectory,
                        "tutti-db-" + ExportDbAction.df.format(new Date()),
                        "zip",
                        t("tutti.dbManager.title.choose.dbBackupFile"),
                        t("tutti.dbManager.action.chooseDbBackupFile"),
                        "^.*\\.zip", t("tutti.common.file.zip")
                );

                if (file == null) {

                    // won't migrate db
                    closeDb = true;
                    continueAction = false;

                    displayWarningMessage(
                            t("tutti.dbManager.title.choose.dbBackupFile"),
                            t("tutti.dbManager.action.upgradeDb.no.backup.db.choosen")
                    );
                } else {

                    continueAction = true;
                }
            } else {

                //close db
                closeDb = true;
            }

            return continueAction;

        }

        @Override
        public void init(Version schemaVersion, Version schemaVersionIfUpdate) {

            super.init(schemaVersion, schemaVersionIfUpdate);


            if (isSchemaVersionTooHigh()) {

                // database schema version is higher than application one
                displayWarningMessage(
                        t("tutti.dbManager.title.schema.toupdate"),
                        t("tutti.dbManager.action.upgradeDb.schema.not.update.message", schemaVersion, schemaVersionIfUpdate)
                );

            }

        }

        public boolean isCloseDb() {
            return closeDb;
        }

        public File getFile() {
            return file;
        }
    }

    @Override
    public void doAction() throws Exception {

        if (log.isDebugEnabled()) {
            log.debug("Will open db...");
        }

        jdbcUrl = getConfig().getJdbcUrl();

        // at the beginning 3 steps (open db + check version + check db context)
        ProgressionModel progressionModel = new ProgressionModel();
        setProgressionModel(progressionModel);
        progressionModel.setTotal(3 + (updateReferentiel ? 1 : 0));

        // ------------------------------------------------------------------ //
        // --- open db                                                        //
        // ------------------------------------------------------------------ //

        progressionModel.increments(t("tutti.openDb.step.open", jdbcUrl));
        try {
            getContext().openPersistenceService();
        } catch (Exception e) {

            if (log.isErrorEnabled()) {
                log.error("Could not open db", e);
            }
            // no more db
            getContext().closePersistenceService();

            // could not load db
            throw new ApplicationBusinessException(t("tutti.dbManager.action.openDb.couldNotOpen"));
        }

        // ------------------------------------------------------------------ //
        // --- check schema version                                           //
        // ------------------------------------------------------------------ //

        progressionModel.increments(t("tutti.openDb.step.checkSchemaVersion"));

        TuttiPersistence persistenceService = getContext().getPersistenceService();

        TuttiConfiguration config = getConfig();

        updateSchemaContext = new UpdateSchemaContext(config.getDbBackupDirectory());
        persistenceService.prepareUpdateSchemaContext(updateSchemaContext);

        Version schemaVersion = updateSchemaContext.getSchemaVersion();

        if (log.isInfoEnabled()) {
            log.info("Detected database version: " + schemaVersion);
        }
        Version schemaVersionIfUpdate = updateSchemaContext.getSchemaVersionIfUpdate();

        if (log.isInfoEnabled()) {
            log.info("Detected schema application version:" + schemaVersionIfUpdate);
        }

        boolean updateSchema = updateSchemaContext.isWillUpdate();

        if (updateSchemaContext.isCloseDb()) {

            // ------------------------------------------------------------------ //
            // --- close current db                                               //
            // ------------------------------------------------------------------ //

            progressionModel.increments(t("tutti.importDb.step.closeDb"));
            getActionEngine().runInternalAction(getHandler(), CloseDbAction.class);

            setScreen(TuttiScreen.MANAGE_DB);
            super.doAction();
            return;
        }

        if (updateSchema) {

            // need to export db + migrate schema)
            progressionModel.adaptTotal(progressionModel.getTotal() + ExportDbAction.TOTAL_STEP + 1);

            // ------------------------------------------------------------------ //
            // --- backup current db                                              //
            // ------------------------------------------------------------------ //

            ApplicationUIAction<ExportDbAction> backupAction =
                    getActionFactory().createUIAction(getHandler(), ExportDbAction.class);
            backupAction.getLogicAction().setProgressionModel(getProgressionModel());
            backupAction.getLogicAction().setFile(updateSchemaContext.getFile());
            backupAction.getLogicAction().setNoUI(true);
            getActionEngine().runInternalAction(backupAction.getLogicAction());

            // ------------------------------------------------------------------ //
            // --- update schema                                                  //
            // ------------------------------------------------------------------ //

            String message = t("tutti.openDb.step.will.migrateSchema", schemaVersion, schemaVersionIfUpdate);

            progressionModel.increments(message);
            sendMessage(message);
            getContext().getPersistenceService().updateSchema();
        }

        if (updateReferentiel) {

            // ------------------------------------------------------------------ //
            // --- update referentiel                                             //
            // ------------------------------------------------------------------ //

            progressionModel.increments(t("tutti.openDb.step.updateReferential"));

            progressionModel.setMessage(t("tutti.dbManager.action.upgradeDb.check"));

            File current = config.getDataDirectory();
            String url = config.getUpdateDataUrl();
            TuttiDbUpdaterCallBack callback = new TuttiDbUpdaterCallBack(url, this, progressionModel);

            Updates.doUpdate(config, callback, current);

            if (callback.isDbUpdated()) {

                progressionModel.setMessage(t("tutti.dbManager.action.upgradeDb.reloading"));

            } else {
                sendMessage(t("tutti.dbManager.action.upgradeDb.upToDate"));
            }
        }

        // ------------------------------------------------------------------ //
        // --- check db context                                               //
        // ------------------------------------------------------------------ //

        String message = t("tutti.openDb.step.check.dbContext", schemaVersion, schemaVersionIfUpdate);

        progressionModel.increments(message);

        if (log.isDebugEnabled()) {
            log.debug("Check db context");
        }
        getContext().checkDbContext();

        super.doAction();
    }

    @Override
    public void postSuccessAction() {

        handler.reloadDbManagerText();

        // make sure title is reloaded
        handler.changeTitle();

        if (updateSchemaContext.isCloseDb()) {
            sendMessage(t("tutti.flash.info.db.not.opened", jdbcUrl));
        } else {
            sendMessage(t("tutti.flash.info.db.opened", jdbcUrl));
        }
    }

    @Override
    public void postFailedAction(Throwable error) {

        handler.reloadDbManagerText();

        super.postFailedAction(error);
    }
}

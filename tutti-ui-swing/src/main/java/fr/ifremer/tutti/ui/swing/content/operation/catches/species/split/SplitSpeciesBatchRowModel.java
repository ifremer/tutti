package fr.ifremer.tutti.ui.swing.content.operation.catches.species.split;

/*
 * #%L
 * Tutti :: UI
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.entities.data.SampleCategory;
import fr.ifremer.tutti.persistence.entities.data.SampleCategoryModelEntry;
import fr.ifremer.tutti.persistence.entities.referential.CaracteristicQualitativeValue;
import fr.ifremer.tutti.ui.swing.util.AbstractTuttiBeanUIModel;

import java.io.Serializable;

/**
 * A row in the {@link SplitSpeciesBatchUIModel}.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 0.3
 */
public class SplitSpeciesBatchRowModel
        extends AbstractTuttiBeanUIModel<SplitSpeciesBatchRowModel, SplitSpeciesBatchRowModel> {

    private static final long serialVersionUID = 1L;

    public static final String PROPERTY_CATEGORY_VALUE = "categoryValue";

    public static final String PROPERTY_WEIGHT = "weight";

    public static final String PROPERTY_SELECTED = "selected";

    public static final String PROPERTY_EDITABLE = "editable";

    /**
     * Delegate sample category which contains category value + weight.
     *
     * @since 0.3
     */
    protected final SampleCategory<Serializable> category = SampleCategory.newSample(null);

    /**
     * Is row selected?
     *
     * @since 2.5
     */
    protected boolean selected;

    /**
     * Is row editable ? (in add mode incoming rows are all selected and not editable).
     *
     * @since 2.6
     */
    protected boolean editable;

    public SplitSpeciesBatchRowModel() {
        super(null, null);
    }

//    public SampleCategoryModelEntry getCategorytype() {
//        return category.getCategoryDef();
//    }

    public void setCategoryType(SampleCategoryModelEntry categoryType) {
        category.setCategoryDef(categoryType);
    }

    public Serializable getCategoryValue() {
        return category.getCategoryValue();
    }

    public void setCategoryValue(Serializable categoryValue) {
        Object oldValue = getCategoryValue();
        category.setCategoryValue(categoryValue);
        firePropertyChange(PROPERTY_CATEGORY_VALUE, oldValue, categoryValue);
    }

    public void setCategoryValue(CaracteristicQualitativeValue categoryValue) {
        Object oldValue = getCategoryValue();
        category.setCategoryValue(categoryValue);
        firePropertyChange(PROPERTY_CATEGORY_VALUE, oldValue, categoryValue);
    }

    public void setCategoryValue(Float categoryValue) {
        Object oldValue = getCategoryValue();
        category.setCategoryValue(categoryValue);
        firePropertyChange(PROPERTY_CATEGORY_VALUE, oldValue, categoryValue);
    }

    public Float getWeight() {
        return category.getCategoryWeight();
    }

    public void setWeight(Float weight) {
        Object oldValue = getWeight();
        category.setCategoryWeight(weight);
        firePropertyChange(PROPERTY_WEIGHT, oldValue, weight);
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        Object oldValue = isSelected();
        this.selected = selected;
        firePropertyChange(PROPERTY_SELECTED, oldValue, selected);
    }

    public boolean isEditable() {
        return editable;
    }

    public void setEditable(boolean editable) {
        Object oldValue = isEditable();
        this.editable = editable;
        firePropertyChange(PROPERTY_EDITABLE, oldValue, editable);
    }

    @Override
    protected SplitSpeciesBatchRowModel newEntity() {
        return new SplitSpeciesBatchRowModel();
    }
}

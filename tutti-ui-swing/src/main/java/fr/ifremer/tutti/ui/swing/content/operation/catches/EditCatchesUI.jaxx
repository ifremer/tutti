<!--
  #%L
  Tutti :: UI
  %%
  Copyright (C) 2012 - 2014 Ifremer
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation, either version 3 of the 
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public 
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/gpl-3.0.html>.
  #L%
  -->
<JPanel id='editCatchesTopPanel' layout='{new BorderLayout()}' decorator='help'
        implements='fr.ifremer.tutti.ui.swing.util.TuttiUI&lt;EditCatchesUIModel, EditCatchesUIHandler&gt;'>

  <import><![CDATA[
    fr.ifremer.tutti.ui.swing.TuttiHelpBroker

    fr.ifremer.tutti.ui.swing.content.operation.FishingOperationsUI
    fr.ifremer.tutti.ui.swing.content.operation.catches.accidental.AccidentalBatchUI
    fr.ifremer.tutti.ui.swing.content.operation.catches.accidental.create.CreateAccidentalBatchUI
    fr.ifremer.tutti.ui.swing.content.operation.catches.marinelitter.MarineLitterBatchUI
    fr.ifremer.tutti.ui.swing.content.operation.catches.marinelitter.create.CreateMarineLitterBatchUI
    fr.ifremer.tutti.ui.swing.content.operation.catches.marinelitter.MarineLitterBatchUI
    fr.ifremer.tutti.ui.swing.content.operation.catches.species.EditSpeciesBatchPanelUI
    fr.ifremer.tutti.ui.swing.content.operation.catches.species.SpeciesOrBenthosBatchUISupport

    fr.ifremer.tutti.ui.swing.util.TuttiUIUtil
    fr.ifremer.tutti.ui.swing.util.attachment.ButtonAttachment
    fr.ifremer.tutti.ui.swing.util.caracteristics.CaracteristicMapEditorUI

    org.jdesktop.swingx.JXTitledPanel

    jaxx.runtime.swing.CardLayout2Ext

    static org.nuiton.i18n.I18n.t
  ]]></import>

  <script><![CDATA[

public EditCatchesUI(FishingOperationsUI parentUI) {
    TuttiUIUtil.setParentUI(this, parentUI);
}

public EditSpeciesBatchPanelUI getEditSpeciesBatchPanelUI(SpeciesOrBenthosBatchUISupport batchUISupport) { return handler.getEditSpeciesBatchPanelUI(batchUISupport); }

  ]]></script>

  <EditCatchesUIModel id='model'
                      initializer='getContextValue(EditCatchesUIModel.class)'/>

  <BeanValidator id='validator' context='validate' bean='model'
                 uiClass='jaxx.runtime.validator.swing.ui.ImageValidationUI'>
    <!--FIXME Must find a way to validate, if no field, then no validation... -->
    <!--FIXME Must find a way to show validation results on svg canvas... -->
    <!--<field name='catchTotalWeight' component='catchTotalWeightField'/>-->

    <!--<field name='catchTotalRejectedWeight'-->
           <!--component='catchTotalRejectedWeightField'/>-->

    <!--<field name='speciesTotalSortedWeight'-->
           <!--component='speciesTotalSortedWeightField'/>-->

    <!--<field name='benthosTotalSortedWeight'-->
           <!--component='benthosTotalSortedWeightField'/>-->

    <!--<field name='marineLitterTotalWeight'-->
           <!--component='marineLitterTotalWeightField'/>-->
  </BeanValidator>

  <!--CardLayout2Ext id='speciesTabPanelLayout'
                  constructorParams='this, "speciesTabPanel"'/>

  <CardLayout2Ext id='benthosTabPanelLayout'
                  constructorParams='this, "benthosTabPanel"'/-->

  <CardLayout2Ext id='marineLitterTabPanelLayout'
                  constructorParams='this, "marineLitterTabPanel"'/>

  <CardLayout2Ext id='accidentalTabPanelLayout'
                  constructorParams='this, "accidentalTabPanel"'/>

  <TuttiHelpBroker id='broker' constructorParams='"tutti.editCatchBatch.help"'/>

  <JToolBar id='catchesCaracteristicsTabToolBar'>
    <JMenuBar id='menu'>
      <JMenu id='menuAction'>
        <JMenuItem id='exportFishingOperationReportButton'/>
        <JMenuItem id='exportFishingOperationReportForSumatraButton'/>
        <JMenuItem id='importMultiPostButton'/>
        <JMenuItem id='exportMultiPostButton'/>
      </JMenu>
    </JMenuBar>
    <ButtonAttachment id='catchesCaracteristicsAttachmentsButton'
                      constructorParams='getHandler().getContext(), getModel()'/>
  </JToolBar>

  <JTabbedPane id='tabPane' constraints='BorderLayout.CENTER'>
    <tab id='catchesCaracteristicsTab'>
      <JXTitledPanel id='catchesCaracteristicsTabPane'>
        <JPanel layout='{new BorderLayout()}' id='svgCanvasPanel'>
        </JPanel>
      </JXTitledPanel>
    </tab>
    <tab id='speciesTab'>
      <EditSpeciesBatchPanelUI id='speciesTabPanel' constructorParams='this, SpeciesOrBenthosBatchUISupport.SPECIES'/>
    </tab>
    <tab id='benthosTab'>
      <EditSpeciesBatchPanelUI id='benthosTabPanel' constructorParams='this, SpeciesOrBenthosBatchUISupport.BENTHOS'/>
    </tab>
    <tab id='marineLitterTab' title='tutti.label.tab.marineLitter'>
      <JPanel id='marineLitterTabPanel'>
        <JXTitledPanel id='marineLitterTabFishingOperationReminderLabel' constraints='EditCatchesUIHandler.MAIN_CARD'>
          <MarineLitterBatchUI id='marineLitterTabContent' constructorParams='this'/>
        </JXTitledPanel>
        <JXTitledPanel id='marineLitterTabCreateBatchReminderLabel' constraints='EditCatchesUIHandler.CREATE_BATCH_CARD'>
          <CreateMarineLitterBatchUI id='marineLitterTabCreateBatch' constructorParams='this'/>
        </JXTitledPanel>
      </JPanel>
    </tab>
    <tab id='accidentalTab'>
      <JPanel id='accidentalTabPanel'>
        <JXTitledPanel id='accidentalTabFishingOperationReminderLabel' constraints='EditCatchesUIHandler.MAIN_CARD'>
          <AccidentalBatchUI id='accidentalTabContent' constructorParams='this'/>
        </JXTitledPanel>
        <JXTitledPanel id='accidentalTabCreateBatchReminderLabel' constraints='EditCatchesUIHandler.CREATE_BATCH_CARD'>
          <CreateAccidentalBatchUI id='accidentalTabCreateBatch' constructorParams='this'/>
        </JXTitledPanel>
        <JXTitledPanel id='accidentalCaracteristicMapEditorReminderLabel' constraints='EditCatchesUIHandler.EDIT_CARACTERISTICS_CARD'>
          <CaracteristicMapEditorUI id='accidentalCaracteristicMapEditor' constructorParams='accidentalTabContent'/>
        </JXTitledPanel>
      </JPanel>
    </tab>
  </JTabbedPane>

  <!-- Actions -->
  <Table id='createFishingOperationActions'
         constraints='BorderLayout.SOUTH'
         fill='both'
         insets='0' styleClass="buttonPanel">
    <row>
      <cell weightx="1">
        <JButton id='cancelButton' styleClass="buttonPanel"/>
      </cell>
      <cell weightx="1">
        <JButton id='saveButton' styleClass="buttonPanel"/>
      </cell>
      <cell weightx="1">
        <JButton id='cleanSpeciesBatchButton' styleClass="buttonPanel"/>
      </cell>
      <cell weightx="1">
        <JButton id='computeSpeciesBatchButton' styleClass="buttonPanel"/>
      </cell>
    </row>
  </Table>

</JPanel>

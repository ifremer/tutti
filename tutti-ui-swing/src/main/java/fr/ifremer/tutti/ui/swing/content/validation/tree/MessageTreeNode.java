package fr.ifremer.tutti.ui.swing.content.validation.tree;

/*
 * #%L
 * Tutti :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import jaxx.runtime.validator.swing.SwingValidatorUtil;
import org.nuiton.validator.NuitonValidatorScope;

import javax.swing.JComponent;

/**
 * Created on 7/9/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.6
 */
public class MessageTreeNode extends TuttiMessageNodeSupport<String> {

    private static final long serialVersionUID = 1L;

    protected NuitonValidatorScope scope;

    public MessageTreeNode(NuitonValidatorScope scope, String message, JComponent editor) {
        super(message, SwingValidatorUtil.getIcon(scope), editor);
        this.scope = scope;
    }

//    public NuitonValidatorScope getScope() {
//        return scope;
//    }

}

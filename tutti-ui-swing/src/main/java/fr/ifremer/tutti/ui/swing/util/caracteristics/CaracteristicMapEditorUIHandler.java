package fr.ifremer.tutti.ui.swing.util.caracteristics;

/*
 * #%L
 * Tutti :: UI
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Functions;
import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import fr.ifremer.tutti.persistence.entities.CaracteristicMap;
import fr.ifremer.tutti.persistence.entities.referential.Caracteristic;
import fr.ifremer.tutti.service.DecoratorService;
import fr.ifremer.tutti.ui.swing.util.TuttiBeanMonitor;
import fr.ifremer.tutti.ui.swing.util.table.AbstractTuttiTableUIHandler;
import jaxx.runtime.swing.editor.bean.BeanFilterableComboBox;
import jaxx.runtime.validator.swing.SwingValidator;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jdesktop.swingx.JXTable;
import org.jdesktop.swingx.table.DefaultTableColumnModelExt;

import javax.swing.JComponent;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import static fr.ifremer.tutti.ui.swing.util.caracteristics.CaracteristicMapCellComponent.CaracteristicMapCellEditor;

/**
 * @author Kevin Morin - kmorin@codelutin.com
 * @since 1.4
 */
public class CaracteristicMapEditorUIHandler
        extends AbstractTuttiTableUIHandler<CaracteristicMapEditorRowModel, CaracteristicMapEditorUIModel, CaracteristicMapEditorUI> {

    private final static Log log =
            LogFactory.getLog(CaracteristicMapEditorUIHandler.class);

    protected CaracteristicMapCellEditor caracteristicMapCellEditor;

    public CaracteristicMapEditorUIHandler() {
        super(CaracteristicMapEditorRowModel.PROPERTY_VALUE);
    }

    //------------------------------------------------------------------------//
    //-- AbstractTuttiTableUIHandler methods                                --//
    //------------------------------------------------------------------------//

    @Override
    public CaracteristicMapEditorTableModel getTableModel() {
        return (CaracteristicMapEditorTableModel) getTable().getModel();
    }

    @Override
    public JXTable getTable() {
        return ui.getCaracteristicMapEditorTable();
    }

    @Override
    protected boolean isRowValid(CaracteristicMapEditorRowModel row) {
        return row.getKey() != null && row.getValue() != null;
    }

    @Override
    protected void onRowModified(int rowIndex, CaracteristicMapEditorRowModel row,
                                 String propertyName, Object oldValue, Object newValue) {
        recomputeRowValidState(row);
        super.onRowModified(rowIndex, row, propertyName, oldValue, newValue);
        saveSelectedRowIfNeeded();
    }

    @Override
    protected void saveSelectedRowIfRequired(TuttiBeanMonitor<CaracteristicMapEditorRowModel> rowMonitor,
                                             CaracteristicMapEditorRowModel row) {
        if (row.isValid()) {
            // there is a valid bean attached to the monitor

            if (rowMonitor.wasModified()) {

                // monitored bean was modified, save it
                if (log.isInfoEnabled()) {
                    log.info("Row " + row + " was modified, will save it");
                }

                saveRow(row);

            }
        }
    }

    @Override
    protected void beforeOpenPopup(int rowIndex, int columnIndex) {
        super.beforeOpenPopup(rowIndex, columnIndex);

        getModel().setRemoveCaracteristicEnabled(rowIndex >= 0);
    }

    //------------------------------------------------------------------------//
    //-- AbstractTuttiUIHandler methods                                     --//
    //------------------------------------------------------------------------//

    @Override
    public void beforeInit(CaracteristicMapEditorUI ui) {
        super.beforeInit(ui);
        CaracteristicMapEditorUIModel model = new CaracteristicMapEditorUIModel();
        ui.setContextValue(model);
    }

    @Override
    public void afterInit(CaracteristicMapEditorUI ui) {
        initUI(ui);

        initBeanFilterableComboBox(getKeyCombo(), new ArrayList<>(), null);
        getModel().setAllAvailableCaracteristics(new ArrayList<>(getDataContext().getCaracteristicWithProtected()));

        JXTable table = getTable();

        // create table column model
        DefaultTableColumnModelExt columnModel =
                new DefaultTableColumnModelExt();

        {

            addColumnToModel(columnModel,
                             null,
                             newTableCellRender(Caracteristic.class, DecoratorService.CARACTERISTIC_WITH_UNIT),
                             CaracteristicMapEditorTableModel.KEY);
        }

        {

            addColumnToModel(columnModel,
                             new CaracteristicValueEditor(getContext()),
                             new CaracteristicValueRenderer(getContext()),
                             CaracteristicMapEditorTableModel.VALUE);
        }

        // create table model
        CaracteristicMapEditorTableModel tableModel =
                new CaracteristicMapEditorTableModel(columnModel);

        table.setModel(tableModel);
        table.setColumnModel(columnModel);

        initTable(table);
    }

    @Override
    protected JComponent getComponentToFocus() {
        return getUI().getNewRowKey();
    }

    @Override
    public void onCloseUI() {
        if (log.isDebugEnabled()) {
            log.debug("closing: " + ui);
        }

        caracteristicMapCellEditor.closeEditor();
    }

    @Override
    public SwingValidator<CaracteristicMapEditorUIModel> getValidator() {
        return null;
    }

    public CaracteristicMapCellEditor getCaracteristicMapCellEditor() {
        return caracteristicMapCellEditor;
    }

    //------------------------------------------------------------------------//
    //-- Cancelable methods                                                 --//
    //------------------------------------------------------------------------//

//    @Override
//    public void cancel() {
//
//        if (log.isDebugEnabled()) {
//            log.debug("Cancel UI " + ui);
//        }
//
//        // close dialog
//        closeUI(ui);
//    }

    //------------------------------------------------------------------------//
    //-- Public methods                                                     --//
    //------------------------------------------------------------------------//

//    /** Adds a row with the parameter selected in the combo box */
//    public void addRow() {
//        BeanFilterableComboBox<Caracteristic> keyCombo = getKeyCombo();
//        Caracteristic selectedItem = (Caracteristic) keyCombo.getSelectedItem();
//        CaracteristicMapEditorTableModel tableModel = getTableModel();
//
//        CaracteristicMapEditorRowModel row = tableModel.createNewRow();
//        row.setKey(selectedItem);
//        getModel().getRows().add(row);
//
//        int rowIndex = tableModel.getRowCount() - 1;
//        tableModel.fireTableRowsInserted(rowIndex, rowIndex);
//
//        keyCombo.getHandler().removeItem(selectedItem);
//
//        CaracteristicMapEditorUIModel model = getModel();
//        model.setModify(true);
//        recomputeRowValidState(row);
//    }

    /**
     * Edit the batch caracteristics
     *
     * @param caracteristicMapColumnRowModel the row to edit
     * @param caracteristicMapCellEditor     the editor
     * @param caracteristicsUsed             the set of the caracteristics used in the other rows.
     *                                       If no caracteristic is set on this row, add automatically these caracteristics with a null value
     */
    public void editBatch(CaracteristicMapColumnRowModel caracteristicMapColumnRowModel,
                          CaracteristicMapCellEditor caracteristicMapCellEditor,
                          Set<Caracteristic> caracteristicsUsed) {

        this.caracteristicMapCellEditor = caracteristicMapCellEditor;

        CaracteristicMapEditorTableModel tableModel = getTableModel();
        CaracteristicMapEditorUIModel model = getModel();

        CaracteristicMap caracteristicMap = caracteristicMapColumnRowModel.getCaracteristics();
        if (MapUtils.isEmpty(caracteristicMap)) {
            caracteristicMap = new CaracteristicMap();
            caracteristicMap.putAll(Maps.asMap(caracteristicsUsed, Functions.constant((Serializable) null)));
        }
        model.setCaracteristicMap(caracteristicMap);

        List<CaracteristicMapEditorRowModel> rows = Lists.newArrayList();
        List<Caracteristic> caracteristics = Lists.newArrayList(caracteristicMap.keySet());

        List<Caracteristic> availableCaracteristics = model.getAvailableCaracteristics();

        for (Caracteristic key : caracteristics) {
            CaracteristicMapEditorRowModel newRow = tableModel.createNewRow();
            newRow.setKey(key);
            newRow.setValue(caracteristicMap.get(key));
            rows.add(newRow);
        }

        model.setRows(rows);

        // add the lengthstep caracteristic in the caracteristics which cannot be added
        caracteristics.add(caracteristicMapColumnRowModel.getLengthStepCaracteristic());

        List<Caracteristic> caracteristicList = Lists.newArrayList();

        for (Caracteristic caracteristic : availableCaracteristics) {
            if (!caracteristics.contains(caracteristic)) {
                caracteristicList.add(caracteristic);
            }
        }

        BeanFilterableComboBox<Caracteristic> keyCombo = getKeyCombo();
        keyCombo.setData(caracteristicList);
        keyCombo.getHandler().reset();
        model.setModify(false);
    }

//    public void removeCaracteristic() {
//        int rowIndex = getTable().getSelectedRow();
//
//        Preconditions.checkState(
//                rowIndex != -1,
//                "Cant remove caracteristic if no caracteristic selected");
//
//        CaracteristicMapEditorRowModel row = getTableModel().getEntry(rowIndex);
//
//        CaracteristicMap caracteristicMap = getModel().getCaracteristicMap();
//        if (caracteristicMap != null) {
//            caracteristicMap.remove(row.getKey());
//        }
//
//        //add the row in the combo
//        BeanFilterableComboBox keyCombo = getKeyCombo();
//        keyCombo.addItem(row.getKey());
//        keyCombo.getHandler().reset();
//
//        // remove the row from the model
//        getModel().getRows().remove(rowIndex);
//
//        // refresh all the table
//        getTableModel().fireTableRowsDeleted(rowIndex, rowIndex);
//
//        getModel().removeRowInError(row);
//    }

//    public void save() {
//
//        if (log.isDebugEnabled()) {
//            log.debug("Save UI " + ui);
//        }
//
//        caracteristicMapCellEditor.validateEdition(getModel());
//
//        closeUI(ui);
//    }

    //------------------------------------------------------------------------//
    //-- Internal methods                                                   --//
    //------------------------------------------------------------------------//

    protected BeanFilterableComboBox<Caracteristic> getKeyCombo() {
        return ui.getNewRowKey();
    }

    protected void saveRow(CaracteristicMapEditorRowModel row) {

        if (row.isValid()) {
            CaracteristicMap caracteristics = getModel().getCaracteristicMap();
            Preconditions.checkNotNull(caracteristics);

            caracteristics.put(row.getKey(), row.getValue());
        }
    }
}

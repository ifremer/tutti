package fr.ifremer.tutti.ui.swing.content.db.actions;

/*
 * #%L
 * Tutti :: UI
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import fr.ifremer.tutti.TuttiConfiguration;
import fr.ifremer.tutti.persistence.ProgressionModel;
import fr.ifremer.tutti.ui.swing.content.actions.AbstractMainUITuttiAction;
import fr.ifremer.tutti.ui.swing.content.MainUIHandler;
import fr.ifremer.tutti.ui.swing.update.TuttiDbUpdaterCallBack;
import fr.ifremer.tutti.ui.swing.update.Updates;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.File;

import static org.nuiton.i18n.I18n.t;

/**
 * To install (or reinstall) a db from last network one.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.4
 */
public class InstallDbAction extends AbstractMainUITuttiAction {

    /** Logger. */
    private static final Log log =
            LogFactory.getLog(InstallDbAction.class);

    protected File backupFile;

    protected boolean doBackup;

    protected String jdbcUrl;

    public InstallDbAction(MainUIHandler handler) {
        super(handler, true);
        setActionDescription(t("tutti.dbManager.action.installDb.tip"));
    }

    @Override
    public boolean prepareAction() throws Exception {
        boolean doAction = super.prepareAction();

        if (doAction) {

            // check db url is reachable
            doAction = getContext().checkUpdateDataReachable(true);
        }

        if (doAction) {

            ProgressionModel progressionModel = new ProgressionModel();
            progressionModel.setTotal(2);
            setProgressionModel(progressionModel);
        }
        return doAction;
    }

    @Override
    public void doAction() {

        ProgressionModel progressionModel = getProgressionModel();

        // ------------------------------------------------------------------ //
        // --- install db                                                     //
        // ------------------------------------------------------------------ //

        TuttiConfiguration config = getConfig();

        File current = config.getDataDirectory();
        String url = config.getUpdateDataUrl();

        if (log.isInfoEnabled()) {
            log.info(String.format("Try to install / update db (current data location: %s), using update url: %s", current, url));
        }

        progressionModel.increments(t("tutti.dbManager.action.upgradeDb.check"));
        TuttiDbUpdaterCallBack callback = new TuttiDbUpdaterCallBack(url, this, progressionModel);
        Updates.doUpdate(config, callback, current);

        Preconditions.checkState(callback.isDbInstalled());

        progressionModel.increments(t("tutti.dbManager.action.upgradeDb.opening"));

        getContext().setDbExist(true);

        // ------------------------------------------------------------------ //
        // --- open db                                                        //
        // ------------------------------------------------------------------ //

        getActionEngine().runInternalAction(getHandler(), OpenDbAction.class);
    }
}

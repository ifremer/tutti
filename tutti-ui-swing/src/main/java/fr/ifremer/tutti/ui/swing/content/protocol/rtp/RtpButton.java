package fr.ifremer.tutti.ui.swing.content.protocol.rtp;

/*
 * #%L
 * Tutti :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2016 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.ui.swing.content.protocol.EditProtocolSpeciesTableModel;
import fr.ifremer.tutti.ui.swing.content.protocol.EditProtocolUI;
import jaxx.runtime.SwingUtil;

import javax.swing.JToggleButton;
import javax.swing.RowSorter;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import static org.nuiton.i18n.I18n.t;

/**
 * Created on 14/01/16.
 *
 * @author Tony Chemit - chemit@codelutin.com
 */
public class RtpButton extends JToggleButton {

    private static final long serialVersionUID = 1L;

    protected final RtpEditorUI popup;

    public RtpButton(final EditProtocolUI ui) {

        setIcon(SwingUtil.createActionIcon("edit-rtp"));
        setText(t("tutti.editRtp.button"));
        setToolTipText(t("tutti.editRtp.button.tip"));

        popup = ui.getRtpEditorUI();

        popup.addWindowListener(new WindowAdapter() {

            @Override
            public void windowOpened(WindowEvent e) {
                setSelected(true);
            }

            @Override
            public void windowClosing(WindowEvent e) {
                setSelected(false);
            }

            @Override
            public void windowClosed(WindowEvent e) {

                setSelected(false);
            }
        });

        addActionListener(e -> {
            if (isSelected()) {
                if (!popup.isVisible()) {
                    SwingUtil.center(ui.getHandler().getContext().getMainUI(), popup);
                    popup.openEditor();
                }
            }
//                else {
//                    popup.closeEditor();
//                }
        });

    }

    public void init(EditProtocolSpeciesTableModel tableModel, RowSorter rowSorter, int rowIndex) {
        popup.setBean(tableModel, rowSorter, rowIndex);
    }

}
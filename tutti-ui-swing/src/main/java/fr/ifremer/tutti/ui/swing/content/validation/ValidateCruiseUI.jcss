/*
 * #%L
 * Tutti :: UI
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

#navigationTitledPanel {
  rightDecoration: {topToolBar};
  border: {null};
  title: "tutti.validateCruise.navigation.title";
}

#topToolBar {
  floatable: false;
  opaque: false;
  borderPainted: false;
}

#exportButton {
  actionIcon: export;
  toolTipText: "tutti.validateCruise.navigation.action.export.all.tip";
  _applicationAction: {fr.ifremer.tutti.ui.swing.content.validation.actions.ExportCruiseValidationMessagesAction.class};
  _help: {"tutti.validateCruise.navigation.action.export.all.help"};
}

#readyToSynchButton {
  actionIcon: time;
  text: "tutti.validateCruise.navigation.action.readyToSynch";
  toolTipText: "tutti.validateCruise.navigation.action.readyToSynch.tip";
  i18nMnemonic: "tutti.validateCruise.navigation.action.readyToSynch.mnemonic";
  enabled: {model.isReadyToSynch()};
  _applicationAction: {fr.ifremer.tutti.ui.swing.content.validation.actions.SaveCruiseToReadyToSynchAction.class};
  _help: {"tutti.validateCruise.navigation.action.readyToSynch.help"};
}

#menuPopup {
  label: "tutti.editAccidentalBatch.title.batchActions";
}

#exportOperationMessagesItem {
  actionIcon: export;
  text: "tutti.validateCruise.navigation.action.export.operation";
  toolTipText: "tutti.validateCruise.navigation.action.export.operation.tip";
  i18nMnemonic: "tutti.validateCruise.navigation.action.export.operation.mnemonic";
  _applicationAction: {fr.ifremer.tutti.ui.swing.content.validation.actions.ExportOperationValidationMessagesAction.class};
  enabled: {model.getSelectedFishingOperation() != null};
  _help: {"tutti.validateCruise.navigation.action.export.operation.help"};
}

#navigation {
  rootVisible: false;
}

#editPanel {
   layout: {editPanelLayout};
}

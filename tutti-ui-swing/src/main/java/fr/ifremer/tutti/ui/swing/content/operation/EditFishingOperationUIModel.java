package fr.ifremer.tutti.ui.swing.content.operation;

/*
 * #%L
 * Tutti :: UI
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Lists;
import fr.ifremer.adagio.core.dao.referential.ObjectTypeCode;
import fr.ifremer.tutti.persistence.entities.CaracteristicMap;
import fr.ifremer.tutti.persistence.entities.TuttiEntities;
import fr.ifremer.tutti.persistence.entities.data.Attachment;
import fr.ifremer.tutti.persistence.entities.data.Cruise;
import fr.ifremer.tutti.persistence.entities.data.FishingOperation;
import fr.ifremer.tutti.persistence.entities.data.FishingOperations;
import fr.ifremer.tutti.persistence.entities.referential.Gear;
import fr.ifremer.tutti.persistence.entities.referential.Person;
import fr.ifremer.tutti.persistence.entities.referential.TuttiLocation;
import fr.ifremer.tutti.persistence.entities.referential.Vessel;
import fr.ifremer.tutti.type.CoordinateEditorType;
import fr.ifremer.tutti.ui.swing.util.AbstractTuttiBeanUIModel;
import fr.ifremer.tutti.ui.swing.util.attachment.AttachmentModelAware;
import fr.ifremer.tutti.util.Distances;
import jaxx.runtime.swing.editor.gis.DmdCoordinate;
import jaxx.runtime.swing.editor.gis.DmsCoordinate;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.nuiton.jaxx.application.swing.tab.TabContentModel;
import org.nuiton.util.DateUtil;
import org.nuiton.util.beans.Binder;
import org.nuiton.util.beans.BinderFactory;

import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import static org.nuiton.i18n.I18n.n;

/**
 * Model for UI {@link EditFishingOperationUI}.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 0.1
 */
public class EditFishingOperationUIModel extends AbstractTuttiBeanUIModel<FishingOperation, EditFishingOperationUIModel>
        implements AttachmentModelAware, TabContentModel, FishingOperation {

    private static final long serialVersionUID = 1L;

    public static final String TITLE = n("tutti.editFishingOperation.tab.general");

    public static final String PROPERTY_PERSISTED = "persisted";

    public static final String PROPERTY_GEAR_SHOOTING_START_LATITUDE_DMS = "gearShootingStartLatitudeDms";

    public static final String PROPERTY_GEAR_SHOOTING_START_LATITUDE_DMS_SIGN = "gearShootingStartLatitudeDmsSign";

    public static final String PROPERTY_GEAR_SHOOTING_START_LATITUDE_DMS_DEGREE = "gearShootingStartLatitudeDmsDegree";

    public static final String PROPERTY_GEAR_SHOOTING_START_LATITUDE_DMS_MINUTE = "gearShootingStartLatitudeDmsMinute";

    public static final String PROPERTY_GEAR_SHOOTING_START_LATITUDE_DMS_SECOND = "gearShootingStartLatitudeDmsSecond";

    public static final String PROPERTY_GEAR_SHOOTING_START_LATITUDE_DMD = "gearShootingStartLatitudeDmd";

    public static final String PROPERTY_GEAR_SHOOTING_START_LATITUDE_DMD_SIGN = "gearShootingStartLatitudeDmdSign";

    public static final String PROPERTY_GEAR_SHOOTING_START_LATITUDE_DMD_DEGREE = "gearShootingStartLatitudeDmdDegree";

    public static final String PROPERTY_GEAR_SHOOTING_START_LATITUDE_DMD_MINUTE = "gearShootingStartLatitudeDmdMinute";

    public static final String PROPERTY_GEAR_SHOOTING_START_LATITUDE_DMD_DECIMAL = "gearShootingStartLatitudeDmdDecimal";

    public static final String PROPERTY_GEAR_SHOOTING_START_LONGITUDE_DMS = "gearShootingStartLongitudeDms";

    public static final String PROPERTY_GEAR_SHOOTING_START_LONGITUDE_DMS_SIGN = "gearShootingStartLongitudeDmsSign";

    public static final String PROPERTY_GEAR_SHOOTING_START_LONGITUDE_DMS_DEGREE = "gearShootingStartLongitudeDmsDegree";

    public static final String PROPERTY_GEAR_SHOOTING_START_LONGITUDE_DMS_MINUTE = "gearShootingStartLongitudeDmsMinute";

    public static final String PROPERTY_GEAR_SHOOTING_START_LONGITUDE_DMS_SECOND = "gearShootingStartLongitudeDmsSecond";

    public static final String PROPERTY_GEAR_SHOOTING_START_LONGITUDE_DMD = "gearShootingStartLongitudeDmd";

    public static final String PROPERTY_GEAR_SHOOTING_START_LONGITUDE_DMD_SIGN = "gearShootingStartLongitudeDmdSign";

    public static final String PROPERTY_GEAR_SHOOTING_START_LONGITUDE_DMD_DEGREE = "gearShootingStartLongitudeDmdDegree";

    public static final String PROPERTY_GEAR_SHOOTING_START_LONGITUDE_DMD_MINUTE = "gearShootingStartLongitudeDmdMinute";

    public static final String PROPERTY_GEAR_SHOOTING_START_LONGITUDE_DMD_DECIMAL = "gearShootingStartLongitudeDmdDecimal";

    public static final String PROPERTY_GEAR_SHOOTING_END_LATITUDE_DMS = "gearShootingEndLatitudeDms";

    public static final String PROPERTY_GEAR_SHOOTING_END_LATITUDE_DMS_SIGN = "gearShootingEndLatitudeDmsSign";

    public static final String PROPERTY_GEAR_SHOOTING_END_LATITUDE_DMS_DEGREE = "gearShootingEndLatitudeDmsDegree";

    public static final String PROPERTY_GEAR_SHOOTING_END_LATITUDE_DMS_MINUTE = "gearShootingEndLatitudeDmsMinute";

    public static final String PROPERTY_GEAR_SHOOTING_END_LATITUDE_DMS_SECOND = "gearShootingEndLatitudeDmsSecond";

    public static final String PROPERTY_GEAR_SHOOTING_END_LATITUDE_DMD = "gearShootingEndLatitudeDmd";

    public static final String PROPERTY_GEAR_SHOOTING_END_LATITUDE_DMD_SIGN = "gearShootingEndLatitudeDmdSign";

    public static final String PROPERTY_GEAR_SHOOTING_END_LATITUDE_DMD_DEGREE = "gearShootingEndLatitudeDmdDegree";

    public static final String PROPERTY_GEAR_SHOOTING_END_LATITUDE_DMD_MINUTE = "gearShootingEndLatitudeDmdMinute";

    public static final String PROPERTY_GEAR_SHOOTING_END_LATITUDE_DMD_DECIMAL = "gearShootingEndLatitudeDmdDecimal";

    public static final String PROPERTY_GEAR_SHOOTING_END_LONGITUDE_DMS = "gearShootingEndLongitudeDms";

    public static final String PROPERTY_GEAR_SHOOTING_END_LONGITUDE_DMS_SIGN = "gearShootingEndLongitudeDmsSign";

    public static final String PROPERTY_GEAR_SHOOTING_END_LONGITUDE_DMS_DEGREE = "gearShootingEndLongitudeDmsDegree";

    public static final String PROPERTY_GEAR_SHOOTING_END_LONGITUDE_DMS_MINUTE = "gearShootingEndLongitudeDmsMinute";

    public static final String PROPERTY_GEAR_SHOOTING_END_LONGITUDE_DMS_SECOND = "gearShootingEndLongitudeDmsSecond";

    public static final String PROPERTY_GEAR_SHOOTING_END_LONGITUDE_DMD = "gearShootingEndLongitudeDmd";

    public static final String PROPERTY_GEAR_SHOOTING_END_LONGITUDE_DMD_SIGN = "gearShootingEndLongitudeDmdSign";

    public static final String PROPERTY_GEAR_SHOOTING_END_LONGITUDE_DMD_DEGREE = "gearShootingEndLongitudeDmdDegree";

    public static final String PROPERTY_GEAR_SHOOTING_END_LONGITUDE_DMD_MINUTE = "gearShootingEndLongitudeDmdMinute";

    public static final String PROPERTY_GEAR_SHOOTING_END_LONGITUDE_DMD_DECIMAL = "gearShootingEndLongitudeDmdDecimal";

    public static final String PROPERTY_VALIDATION_CONTEXT = "validationContext";

    public static final String PROPERTY_SECONDARY_VESSEL_TYPE = "secondaryVesselType";

    public static final String PROPERTY_SECONDARY_VESSEL_TYPE_ALL = "secondaryVesselTypeAll";

    public static final String PROPERTY_SECONDARY_VESSEL_TYPE_SCIENTIFIC = "secondaryVesselTypeScientific";

    public static final String PROPERTY_SECONDARY_VESSEL_TYPE_FISHING = "secondaryVesselTypeFishing";

    public static final String PROPERTY_SECONDARY_VESSEL_TYPE_ONLY_CRUISE = "secondaryVesselTypeOnlyCruise";

    /**
     * Delegate edit object.
     *
     * @since 1.3
     */
    protected final FishingOperation editObject = FishingOperations.newFishingOperation();

    /**
     * To edit gearShootingStartLatitude as dms format.
     *
     * @since 2.8
     */
    protected final DmsCoordinate gearShootingStartLatitudeDms =
            DmsCoordinate.empty();

    /**
     * To edit gearShootingStartLongitude as dms format.
     *
     * @since 2.8
     */
    protected final DmsCoordinate gearShootingStartLongitudeDms =
            DmsCoordinate.empty();

    /**
     * To edit gearShootingEndLatitude as dms format.
     *
     * @since 2.8
     */
    protected final DmsCoordinate gearShootingEndLatitudeDms =
            DmsCoordinate.empty();

    /**
     * To edit gearShootingEndLongitude as dms format.
     *
     * @since 2.8
     */
    protected final DmsCoordinate gearShootingEndLongitudeDms =
            DmsCoordinate.empty();

    /**
     * To edit gearShootingStartLatitude as dmd format.
     *
     * @since 2.8
     */
    protected final DmdCoordinate gearShootingStartLatitudeDmd =
            DmdCoordinate.empty();

    /**
     * To edit gearShootingStartLongitude as dmd format.
     *
     * @since 2.8
     */
    protected final DmdCoordinate gearShootingStartLongitudeDmd =
            DmdCoordinate.empty();

    /**
     * To edit gearShootingEndLatitude as dmd format.
     *
     * @since 2.8
     */
    protected final DmdCoordinate gearShootingEndLatitudeDmd =
            DmdCoordinate.empty();

    /**
     * To edit gearShootingEndLongitude as dmd format.
     *
     * @since 2.8
     */
    protected final DmdCoordinate gearShootingEndLongitudeDmd =
            DmdCoordinate.empty();

    protected int quadrant;

    public int getQuadrant() {
        return quadrant;
    }

    public void setQuadrant(int quadrant) {
        Object oldValue = getQuadrant();
        this.quadrant = quadrant;
        firePropertyChange("quadrant", oldValue, quadrant);
    }

    protected final List<Attachment> attachment = Lists.newArrayList();

    protected FishingOperation fishingOperation;

    protected String validationContext;

    protected CoordinateEditorType coordinateEditorType;

    protected boolean loadingData;

    protected SecondaryVesselTypeEnum secondaryVesselType;

    protected boolean importFromColumnFileEnabled;

    protected static Binder<EditFishingOperationUIModel, FishingOperation> toBeanBinder =
            BinderFactory.newBinder(EditFishingOperationUIModel.class,
                                    FishingOperation.class);

    protected static Binder<FishingOperation, EditFishingOperationUIModel> fromBeanBinder =
            BinderFactory.newBinder(FishingOperation.class, EditFishingOperationUIModel.class);

    public EditFishingOperationUIModel() {
        super(fromBeanBinder, toBeanBinder);
    }

    public boolean isLoadingData() {
        return loadingData;
    }

    public void setLoadingData(boolean loadingData) {
        this.loadingData = loadingData;
    }

    @Override
    public String getTitle() {
        return TITLE;
    }

    @Override
    public String getIcon() {
        return null;
    }

    @Override
    public boolean isCloseable() {
        return false;
    }

    @Override
    public boolean isEmpty() {
        boolean result;

        switch (coordinateEditorType) {

            case DMS:
                result = gearShootingStartLatitudeDms.isNull()
                         && gearShootingStartLongitudeDms.isNull()
                         && gearShootingEndLatitudeDms.isNull()
                         && gearShootingEndLongitudeDms.isNull();
                break;
            case DMD:
                result = gearShootingStartLatitudeDmd.isNull()
                         && gearShootingStartLongitudeDmd.isNull()
                         && gearShootingEndLatitudeDmd.isNull()
                         && gearShootingEndLongitudeDmd.isNull();
                break;
            default:
                result = getGearShootingStartLatitude() == null
                         && getGearShootingStartLongitude() == null
                         && getGearShootingEndLatitude() == null
                         && getGearShootingEndLongitude() == null;
                break;
        }

        return result &&
               StringUtils.isEmpty(getStationNumber())
               && getFishingOperationNumber() == null
               && getStrata() == null
               && getSubStrata() == null
               && getLocation() == null
               && getGearShootingStartDate() == null
               && getGearShootingEndDate() == null
               && !isFishingOperationRectiligne()
               && getTrawlDistance() == null
               && getFishingOperationValid() == null
               && CollectionUtils.isEmpty(getRecorderPerson())
               && getGear() == null
               && StringUtils.isEmpty(getComment())
               && CollectionUtils.isEmpty(getAttachment());
    }

    @Override
    protected FishingOperation newEntity() {
        return fishingOperation;
    }

    public FishingOperation getFishingOperation() {
        return fishingOperation;
    }

    public void setFishingOperation(FishingOperation fishingOperation) {
        this.fishingOperation = fishingOperation;
        firePropertyChange(PROPERTY_PERSISTED, null, isPersisted());
    }

    public boolean isPersisted() {
        return fishingOperation != null && !TuttiEntities.isNew(fishingOperation);
    }

    //------------------------------------------------------------------------//
    //-- Start Latitude ------------------------------------------------------//
    //------------------------------------------------------------------------//

    public DmsCoordinate getGearShootingStartLatitudeDms() {
        return gearShootingStartLatitudeDms;
    }

    public void setGearShootingStartLatitudeDms(Float decimal) {
        DmsCoordinate position =
                DmsCoordinate.valueOf(decimal);
        setGearShootingStartLatitudeDmsSign(position.isSign());
        setGearShootingStartLatitudeDmsDegree(position.getDegree());
        setGearShootingStartLatitudeDmsMinute(position.getMinute());
        setGearShootingStartLatitudeDmsSecond(position.getSecond());
        firePropertyChange(PROPERTY_GEAR_SHOOTING_START_LATITUDE_DMS, null, getGearShootingStartLatitudeDms());
    }

    public boolean isGearShootingStartLatitudeDmsSign() {
        return gearShootingStartLatitudeDms.isSign();
    }

    public void setGearShootingStartLatitudeDmsSign(boolean sign) {
        Object oldValue = isGearShootingStartLatitudeDmsSign();
        gearShootingStartLatitudeDms.setSign(sign);
        firePropertyChange(PROPERTY_GEAR_SHOOTING_START_LATITUDE_DMS_SIGN, oldValue, sign);
    }

    public Integer getGearShootingStartLatitudeDmsDegree() {
        return gearShootingStartLatitudeDms.getDegree();
    }

    public void setGearShootingStartLatitudeDmsDegree(Integer degree) {
        Object oldValue = getGearShootingStartLatitudeDmsDegree();
        if (degree != null) {
            degree = Math.abs(degree);
        }
        gearShootingStartLatitudeDms.setDegree(degree);
        firePropertyChange(PROPERTY_GEAR_SHOOTING_START_LATITUDE_DMS_DEGREE, oldValue, degree);
    }

    public Integer getGearShootingStartLatitudeDmsMinute() {
        return gearShootingStartLatitudeDms.getMinute();
    }

    public void setGearShootingStartLatitudeDmsMinute(Integer minute) {
        Object oldValue = getGearShootingStartLatitudeDmsMinute();
        gearShootingStartLatitudeDms.setMinute(minute);
        firePropertyChange(PROPERTY_GEAR_SHOOTING_START_LATITUDE_DMS_MINUTE, oldValue, minute);
    }

    public Integer getGearShootingStartLatitudeDmsSecond() {
        return gearShootingStartLatitudeDms.getSecond();
    }

    public void setGearShootingStartLatitudeDmsSecond(Integer second) {
        Object oldValue = getGearShootingStartLatitudeDmsSecond();
        gearShootingStartLatitudeDms.setSecond(second);
        firePropertyChange(PROPERTY_GEAR_SHOOTING_START_LATITUDE_DMS_SECOND, oldValue, second);
    }

    public DmdCoordinate getGearShootingStartLatitudeDmd() {
        return gearShootingStartLatitudeDmd;
    }

    public void setGearShootingStartLatitudeDmd(Float decimal) {
        DmdCoordinate position =
                DmdCoordinate.valueOf(decimal);
        setGearShootingStartLatitudeDmdSign(position.isSign());
        setGearShootingStartLatitudeDmdDegree(position.getDegree());
        setGearShootingStartLatitudeDmdMinute(position.getMinute());
        setGearShootingStartLatitudeDmdDecimal(position.getDecimal());
        firePropertyChange(PROPERTY_GEAR_SHOOTING_START_LATITUDE_DMD, null, getGearShootingStartLatitudeDmd());
    }

    public boolean isGearShootingStartLatitudeDmdSign() {
        return gearShootingStartLatitudeDmd.isSign();
    }

    public void setGearShootingStartLatitudeDmdSign(boolean sign) {
        Object oldValue = isGearShootingStartLatitudeDmdSign();
        gearShootingStartLatitudeDmd.setSign(sign);
        firePropertyChange(PROPERTY_GEAR_SHOOTING_START_LATITUDE_DMD_SIGN, oldValue, sign);
    }

    public Integer getGearShootingStartLatitudeDmdDegree() {
        return gearShootingStartLatitudeDmd.getDegree();
    }

    public void setGearShootingStartLatitudeDmdDegree(Integer degree) {
        Object oldValue = getGearShootingStartLatitudeDmdDegree();
        gearShootingStartLatitudeDmd.setDegree(degree);
        firePropertyChange(PROPERTY_GEAR_SHOOTING_START_LATITUDE_DMD_DEGREE, oldValue, degree);
    }

    public Integer getGearShootingStartLatitudeDmdMinute() {
        return gearShootingStartLatitudeDmd.getMinute();
    }

    public void setGearShootingStartLatitudeDmdMinute(Integer minute) {
        Object oldValue = getGearShootingStartLatitudeDmdMinute();
        gearShootingStartLatitudeDmd.setMinute(minute);
        firePropertyChange(PROPERTY_GEAR_SHOOTING_START_LATITUDE_DMD_MINUTE, oldValue, minute);
    }

    public Integer getGearShootingStartLatitudeDmdDecimal() {
        return gearShootingStartLatitudeDmd.getDecimal();
    }

    public void setGearShootingStartLatitudeDmdDecimal(Integer decimal) {
        Object oldValue = getGearShootingStartLatitudeDmdDecimal();
        gearShootingStartLatitudeDmd.setDecimal(decimal);
        firePropertyChange(PROPERTY_GEAR_SHOOTING_START_LATITUDE_DMD_DECIMAL, oldValue, decimal);
    }

    //------------------------------------------------------------------------//
    //-- Start Longitude -----------------------------------------------------//
    //------------------------------------------------------------------------//

    public DmsCoordinate getGearShootingStartLongitudeDms() {
        return gearShootingStartLongitudeDms;
    }

    public void setGearShootingStartLongitudeDms(Float decimal) {
        DmsCoordinate position =
                DmsCoordinate.valueOf(decimal);
        setGearShootingStartLongitudeDmsSign(position.isSign());
        setGearShootingStartLongitudeDmsDegree(position.getDegree());
        setGearShootingStartLongitudeDmsMinute(position.getMinute());
        setGearShootingStartLongitudeDmsSecond(position.getSecond());
        firePropertyChange(PROPERTY_GEAR_SHOOTING_START_LONGITUDE_DMS, null, getGearShootingStartLongitudeDms());
    }

    public boolean isGearShootingStartLongitudeDmsSign() {
        return gearShootingStartLongitudeDms.isSign();
    }

    public void setGearShootingStartLongitudeDmsSign(boolean sign) {
        Object oldValue = isGearShootingStartLongitudeDmsSign();
        gearShootingStartLongitudeDms.setSign(sign);
        firePropertyChange(PROPERTY_GEAR_SHOOTING_START_LONGITUDE_DMS_SIGN, oldValue, sign);
    }

    public Integer getGearShootingStartLongitudeDmsDegree() {
        return gearShootingStartLongitudeDms.getDegree();
    }

    public void setGearShootingStartLongitudeDmsDegree(Integer degree) {
        Object oldValue = getGearShootingStartLongitudeDmsDegree();
        if (degree != null) {
            degree = Math.abs(degree);
        }
        gearShootingStartLongitudeDms.setDegree(degree);
        firePropertyChange(PROPERTY_GEAR_SHOOTING_START_LONGITUDE_DMS_DEGREE, oldValue, degree);
    }

    public Integer getGearShootingStartLongitudeDmsMinute() {
        return gearShootingStartLongitudeDms.getMinute();
    }

    public void setGearShootingStartLongitudeDmsMinute(Integer minute) {
        Object oldValue = getGearShootingStartLongitudeDmsMinute();
        gearShootingStartLongitudeDms.setMinute(minute);
        firePropertyChange(PROPERTY_GEAR_SHOOTING_START_LONGITUDE_DMS_MINUTE, oldValue, minute);
    }

    public Integer getGearShootingStartLongitudeDmsSecond() {
        return gearShootingStartLongitudeDms.getSecond();
    }

    public void setGearShootingStartLongitudeDmsSecond(Integer second) {
        Object oldValue = getGearShootingStartLongitudeDmsSecond();
        gearShootingStartLongitudeDms.setSecond(second);
        firePropertyChange(PROPERTY_GEAR_SHOOTING_START_LONGITUDE_DMS_SECOND, oldValue, second);
    }

    public DmdCoordinate getGearShootingStartLongitudeDmd() {
        return gearShootingStartLongitudeDmd;
    }

    public void setGearShootingStartLongitudeDmd(Float decimal) {
        DmdCoordinate position =
                DmdCoordinate.valueOf(decimal);
        setGearShootingStartLongitudeDmdSign(position.isSign());
        setGearShootingStartLongitudeDmdDegree(position.getDegree());
        setGearShootingStartLongitudeDmdMinute(position.getMinute());
        setGearShootingStartLongitudeDmdDecimal(position.getDecimal());
        firePropertyChange(PROPERTY_GEAR_SHOOTING_START_LONGITUDE_DMD, null, getGearShootingStartLongitudeDmd());
    }

    public boolean isGearShootingStartLongitudeDmdSign() {
        return gearShootingStartLongitudeDmd.isSign();
    }

    public void setGearShootingStartLongitudeDmdSign(boolean sign) {
        Object oldValue = isGearShootingStartLongitudeDmdSign();
        gearShootingStartLongitudeDmd.setSign(sign);
        firePropertyChange(PROPERTY_GEAR_SHOOTING_START_LONGITUDE_DMD_SIGN, oldValue, sign);
    }

    public Integer getGearShootingStartLongitudeDmdDegree() {
        return gearShootingStartLongitudeDmd.getDegree();
    }

    public void setGearShootingStartLongitudeDmdDegree(Integer degree) {
        Object oldValue = getGearShootingStartLongitudeDmdDegree();
        if (degree != null) {
            degree = Math.abs(degree);
        }
        gearShootingStartLongitudeDmd.setDegree(degree);
        firePropertyChange(PROPERTY_GEAR_SHOOTING_START_LONGITUDE_DMD_DEGREE, oldValue, degree);
    }

    public Integer getGearShootingStartLongitudeDmdMinute() {
        return gearShootingStartLongitudeDmd.getMinute();
    }

    public void setGearShootingStartLongitudeDmdMinute(Integer minute) {
        Object oldValue = getGearShootingStartLongitudeDmdMinute();
        gearShootingStartLongitudeDmd.setMinute(minute);
        firePropertyChange(PROPERTY_GEAR_SHOOTING_START_LONGITUDE_DMD_MINUTE, oldValue, minute);
    }

    public Integer getGearShootingStartLongitudeDmdDecimal() {
        return gearShootingStartLongitudeDmd.getDecimal();
    }

    public void setGearShootingStartLongitudeDmdDecimal(Integer minute) {
        Object oldValue = getGearShootingStartLongitudeDmdDecimal();
        gearShootingStartLongitudeDmd.setDecimal(minute);
        firePropertyChange(PROPERTY_GEAR_SHOOTING_START_LONGITUDE_DMD_DECIMAL, oldValue, minute);
    }

    //------------------------------------------------------------------------//
    //-- End Latitude --------------------------------------------------------//
    //------------------------------------------------------------------------//

    public DmsCoordinate getGearShootingEndLatitudeDms() {
        return gearShootingEndLatitudeDms;
    }

    public void setGearShootingEndLatitudeDms(Float decimal) {
        DmsCoordinate position =
                DmsCoordinate.valueOf(decimal);
        setGearShootingEndLatitudeDmsSign(position.isSign());
        setGearShootingEndLatitudeDmsDegree(position.getDegree());
        setGearShootingEndLatitudeDmsMinute(position.getMinute());
        setGearShootingEndLatitudeDmsSecond(position.getSecond());
        firePropertyChange(PROPERTY_GEAR_SHOOTING_END_LATITUDE_DMS, null, getGearShootingEndLatitudeDms());
    }

    public boolean isGearShootingEndLatitudeDmsSign() {
        return gearShootingEndLatitudeDms.isSign();
    }

    public void setGearShootingEndLatitudeDmsSign(boolean sign) {
        Object oldValue = isGearShootingEndLatitudeDmsSign();
        gearShootingEndLatitudeDms.setSign(sign);
        firePropertyChange(PROPERTY_GEAR_SHOOTING_END_LATITUDE_DMS_SIGN, oldValue, sign);
    }

    public Integer getGearShootingEndLatitudeDmsDegree() {
        return gearShootingEndLatitudeDms.getDegree();
    }

    public void setGearShootingEndLatitudeDmsDegree(Integer degree) {
        Object oldValue = getGearShootingEndLatitudeDmsDegree();
        if (degree != null) {
            degree = Math.abs(degree);
        }
        gearShootingEndLatitudeDms.setDegree(degree);
        firePropertyChange(PROPERTY_GEAR_SHOOTING_END_LATITUDE_DMS_DEGREE, oldValue, degree);
    }

    public Integer getGearShootingEndLatitudeDmsMinute() {
        return gearShootingEndLatitudeDms.getMinute();
    }

    public void setGearShootingEndLatitudeDmsMinute(Integer minute) {
        Object oldValue = getGearShootingEndLatitudeDmsMinute();
        gearShootingEndLatitudeDms.setMinute(minute);
        firePropertyChange(PROPERTY_GEAR_SHOOTING_END_LATITUDE_DMS_MINUTE, oldValue, minute);
    }

    public Integer getGearShootingEndLatitudeDmsSecond() {
        return gearShootingEndLatitudeDms.getSecond();
    }

    public void setGearShootingEndLatitudeDmsSecond(Integer second) {
        Object oldValue = getGearShootingEndLatitudeDmsSecond();
        gearShootingEndLatitudeDms.setSecond(second);
        firePropertyChange(PROPERTY_GEAR_SHOOTING_END_LATITUDE_DMS_SECOND, oldValue, second);
    }

    public DmdCoordinate getGearShootingEndLatitudeDmd() {
        return gearShootingEndLatitudeDmd;
    }

    public void setGearShootingEndLatitudeDmd(Float decimal) {
        DmdCoordinate position =
                DmdCoordinate.valueOf(decimal);
        setGearShootingEndLatitudeDmdSign(position.isSign());
        setGearShootingEndLatitudeDmdDegree(position.getDegree());
        setGearShootingEndLatitudeDmdMinute(position.getMinute());
        setGearShootingEndLatitudeDmdDecimal(position.getDecimal());
        firePropertyChange(PROPERTY_GEAR_SHOOTING_END_LATITUDE_DMD, null, getGearShootingEndLatitudeDmd());
    }

    public boolean isGearShootingEndLatitudeDmdSign() {
        return gearShootingEndLatitudeDmd.isSign();
    }

    public void setGearShootingEndLatitudeDmdSign(boolean sign) {
        Object oldValue = isGearShootingEndLatitudeDmdSign();
        gearShootingEndLatitudeDmd.setSign(sign);
        firePropertyChange(PROPERTY_GEAR_SHOOTING_END_LATITUDE_DMD_SIGN, oldValue, sign);
    }

    public Integer getGearShootingEndLatitudeDmdDegree() {
        return gearShootingEndLatitudeDmd.getDegree();
    }

    public void setGearShootingEndLatitudeDmdDegree(Integer degree) {
        Object oldValue = getGearShootingEndLatitudeDmdDegree();
        gearShootingEndLatitudeDmd.setDegree(degree);
        firePropertyChange(PROPERTY_GEAR_SHOOTING_END_LATITUDE_DMD_DEGREE, oldValue, degree);
    }

    public Integer getGearShootingEndLatitudeDmdMinute() {
        return gearShootingEndLatitudeDmd.getMinute();
    }

    public void setGearShootingEndLatitudeDmdMinute(Integer minute) {
        Object oldValue = getGearShootingEndLatitudeDmdMinute();
        gearShootingEndLatitudeDmd.setMinute(minute);
        firePropertyChange(PROPERTY_GEAR_SHOOTING_END_LATITUDE_DMD_MINUTE, oldValue, minute);
    }

    public Integer getGearShootingEndLatitudeDmdDecimal() {
        return gearShootingEndLatitudeDmd.getDecimal();
    }

    public void setGearShootingEndLatitudeDmdDecimal(Integer minute) {
        Object oldValue = getGearShootingEndLatitudeDmdDecimal();
        gearShootingEndLatitudeDmd.setDecimal(minute);
        firePropertyChange(PROPERTY_GEAR_SHOOTING_END_LATITUDE_DMD_DECIMAL, oldValue, minute);
    }

    //------------------------------------------------------------------------//
    //-- End Longitude -------------------------------------------------------//
    //------------------------------------------------------------------------//

    public DmsCoordinate getGearShootingEndLongitudeDms() {
        return gearShootingEndLongitudeDms;
    }

    public void setGearShootingEndLongitudeDms(Float decimal) {
        DmsCoordinate position =
                DmsCoordinate.valueOf(decimal);
        setGearShootingEndLongitudeDmsSign(position.isSign());
        setGearShootingEndLongitudeDmsDegree(position.getDegree());
        setGearShootingEndLongitudeDmsMinute(position.getMinute());
        setGearShootingEndLongitudeDmsSecond(position.getSecond());
        firePropertyChange(PROPERTY_GEAR_SHOOTING_END_LONGITUDE_DMS, null, getGearShootingEndLongitudeDms());
    }

    public boolean isGearShootingEndLongitudeDmsSign() {
        return gearShootingEndLongitudeDms.isSign();
    }

    public void setGearShootingEndLongitudeDmsSign(boolean sign) {
        Object oldValue = isGearShootingEndLongitudeDmsSign();
        gearShootingEndLongitudeDms.setSign(sign);
        firePropertyChange(PROPERTY_GEAR_SHOOTING_END_LONGITUDE_DMS_SIGN, oldValue, sign);
    }

    public Integer getGearShootingEndLongitudeDmsDegree() {
        return gearShootingEndLongitudeDms.getDegree();
    }

    public void setGearShootingEndLongitudeDmsDegree(Integer degree) {
        Object oldValue = getGearShootingEndLongitudeDmsDegree();
        if (degree != null) {
            degree = Math.abs(degree);
        }
        gearShootingEndLongitudeDms.setDegree(degree);
        firePropertyChange(PROPERTY_GEAR_SHOOTING_END_LONGITUDE_DMS_DEGREE, oldValue, degree);
    }

    public Integer getGearShootingEndLongitudeDmsMinute() {
        return gearShootingEndLongitudeDms.getMinute();
    }

    public void setGearShootingEndLongitudeDmsMinute(Integer minute) {
        Object oldValue = getGearShootingEndLongitudeDmsMinute();
        gearShootingEndLongitudeDms.setMinute(minute);
        firePropertyChange(PROPERTY_GEAR_SHOOTING_END_LONGITUDE_DMS_MINUTE, oldValue, minute);
    }

    public Integer getGearShootingEndLongitudeDmsSecond() {
        return gearShootingEndLongitudeDms.getSecond();
    }

    public void setGearShootingEndLongitudeDmsSecond(Integer second) {
        Object oldValue = getGearShootingEndLongitudeDmsSecond();
        gearShootingEndLongitudeDms.setSecond(second);
        firePropertyChange(PROPERTY_GEAR_SHOOTING_END_LONGITUDE_DMS_SECOND, oldValue, second);
    }

    public DmdCoordinate getGearShootingEndLongitudeDmd() {
        return gearShootingEndLongitudeDmd;
    }

    public void setGearShootingEndLongitudeDmd(Float decimal) {
        DmdCoordinate position =
                DmdCoordinate.valueOf(decimal);
        setGearShootingEndLongitudeDmdSign(position.isSign());
        setGearShootingEndLongitudeDmdDegree(position.getDegree());
        setGearShootingEndLongitudeDmdMinute(position.getMinute());
        setGearShootingEndLongitudeDmdDecimal(position.getDecimal());
        firePropertyChange(PROPERTY_GEAR_SHOOTING_END_LONGITUDE_DMD, null, getGearShootingEndLongitudeDmd());
    }

    public boolean isGearShootingEndLongitudeDmdSign() {
        return gearShootingEndLongitudeDmd.isSign();
    }

    public void setGearShootingEndLongitudeDmdSign(boolean sign) {
        Object oldValue = isGearShootingEndLongitudeDmdSign();
        gearShootingEndLongitudeDmd.setSign(sign);
        firePropertyChange(PROPERTY_GEAR_SHOOTING_END_LONGITUDE_DMD_SIGN, oldValue, sign);
    }

    public Integer getGearShootingEndLongitudeDmdDegree() {
        return gearShootingEndLongitudeDmd.getDegree();
    }

    public void setGearShootingEndLongitudeDmdDegree(Integer degree) {
        Object oldValue = gearShootingEndLongitudeDmd.getDegree();
        gearShootingEndLongitudeDmd.setDegree(degree);
        firePropertyChange(PROPERTY_GEAR_SHOOTING_END_LONGITUDE_DMD_DEGREE, oldValue, degree);
    }

    public Integer getGearShootingEndLongitudeDmdMinute() {
        return gearShootingEndLongitudeDmd.getMinute();
    }

    public void setGearShootingEndLongitudeDmdMinute(Integer minute) {
        Object oldValue = gearShootingEndLongitudeDmd.getMinute();
        gearShootingEndLongitudeDmd.setMinute(minute);
        firePropertyChange(PROPERTY_GEAR_SHOOTING_END_LONGITUDE_DMD_MINUTE, oldValue, minute);
    }

    public Integer getGearShootingEndLongitudeDmdDecimal() {
        return gearShootingEndLongitudeDmd.getDecimal();
    }

    public void setGearShootingEndLongitudeDmdDecimal(Integer decimal) {
        Object oldValue = gearShootingEndLongitudeDmd.getDecimal();
        gearShootingEndLongitudeDmd.setDecimal(decimal);
        firePropertyChange(PROPERTY_GEAR_SHOOTING_END_LONGITUDE_DMD_DECIMAL, oldValue, decimal);
    }

    //------------------------------------------------------------------------//
    //-- Start - End Time ----------------------------------------------------//
    //------------------------------------------------------------------------//

    public void setGearShootingStartTime(Date gearShootingStartDate) {
        Date currentGearShootingStartDate = getGearShootingStartDate();
        if (currentGearShootingStartDate != null && gearShootingStartDate != null) {
            Calendar currentCalendar = DateUtil.getDefaultCalendar(currentGearShootingStartDate);
            Calendar timeCalendar = DateUtil.getDefaultCalendar(gearShootingStartDate);
            currentCalendar.set(Calendar.HOUR_OF_DAY, timeCalendar.get(Calendar.HOUR_OF_DAY));
            currentCalendar.set(Calendar.MINUTE, timeCalendar.get(Calendar.MINUTE));
            setGearShootingStartDate(currentCalendar.getTime());
        }
    }

    public void setGearShootingEndTime(Date gearShootingEndDate) {
        Date currentGearShootingEndDate = getGearShootingEndDate();
        if (currentGearShootingEndDate != null && gearShootingEndDate != null) {
            Calendar currentCalendar = DateUtil.getDefaultCalendar(currentGearShootingEndDate);
            Calendar timeCalendar = DateUtil.getDefaultCalendar(gearShootingEndDate);
            currentCalendar.set(Calendar.HOUR_OF_DAY, timeCalendar.get(Calendar.HOUR_OF_DAY));
            currentCalendar.set(Calendar.MINUTE, timeCalendar.get(Calendar.MINUTE));
            setGearShootingEndDate(currentCalendar.getTime());
        }
    }

    public String getValidationContext() {
        return validationContext;
    }

    public void setValidationContext(String validationContext) {
        Object oldValue = getValidationContext();
        this.validationContext = validationContext;
        firePropertyChange(PROPERTY_VALIDATION_CONTEXT, oldValue, validationContext);
    }

    public CoordinateEditorType getCoordinateEditorType() {
        return coordinateEditorType;
    }

    public void setCoordinateEditorType(CoordinateEditorType coordinateEditorType) {
        this.coordinateEditorType = coordinateEditorType;
    }

    public boolean isDateInCruise(Date date) {
        return DateUtil.between(date, getCruise().getBeginDate(), getCruise().getEndDate());
    }

    public boolean isValidDuration() {
        int minutes = DateUtil.getDifferenceInMinutes(getGearShootingStartDate(), getGearShootingEndDate());
        return minutes <= 45 && minutes >= 20;
    }

    public boolean isCoordinateDms() {
        return CoordinateEditorType.DMS.equals(getCoordinateEditorType());
    }

    public boolean isCoordinateDmd() {
        return CoordinateEditorType.DMD.equals(getCoordinateEditorType());
    }

    public void convertGearShootingCoordinatesFromDD() {
        switch (coordinateEditorType) {

            case DMS:
                setGearShootingStartLatitudeDms(getGearShootingStartLatitude());
                setGearShootingStartLongitudeDms(getGearShootingStartLongitude());
                setGearShootingEndLatitudeDms(getGearShootingEndLatitude());
                setGearShootingEndLongitudeDms(getGearShootingEndLongitude());
                break;

            case DMD:
                setGearShootingStartLatitudeDmd(getGearShootingStartLatitude());
                setGearShootingStartLongitudeDmd(getGearShootingStartLongitude());
                setGearShootingEndLatitudeDmd(getGearShootingEndLatitude());
                setGearShootingEndLongitudeDmd(getGearShootingEndLongitude());
                break;
        }
    }

    public void convertGearShootingCoordinatesToDD() {

        switch (coordinateEditorType) {

            case DMS:
                editObject.setGearShootingStartLatitude(
                        gearShootingStartLatitudeDms.toDecimal());

                editObject.setGearShootingStartLongitude(
                        gearShootingStartLongitudeDms.toDecimal());

                editObject.setGearShootingEndLatitude(
                        gearShootingEndLatitudeDms.toDecimal());

                editObject.setGearShootingEndLongitude(
                        gearShootingEndLongitudeDms.toDecimal());
                break;

            case DMD:
                editObject.setGearShootingStartLatitude(
                        gearShootingStartLatitudeDmd.toDecimal());

                editObject.setGearShootingStartLongitude(
                        gearShootingStartLongitudeDmd.toDecimal());

                editObject.setGearShootingEndLatitude(
                        gearShootingEndLatitudeDmd.toDecimal());

                editObject.setGearShootingEndLongitude(
                        gearShootingEndLongitudeDmd.toDecimal());
                break;
        }
    }

    public void computeDistance() {

        convertGearShootingCoordinatesToDD();

        if (getGearShootingStartLatitude() != null && getGearShootingStartLongitude() != null
            && getGearShootingEndLatitude() != null && getGearShootingEndLongitude() != null) {
            Integer distance = Distances.computeDistanceInMeters(
                    getGearShootingStartLatitude(),
                    getGearShootingStartLongitude(),
                    getGearShootingEndLatitude(),
                    getGearShootingEndLongitude());
            setTrawlDistance(distance);
        }
    }

    public SecondaryVesselTypeEnum getSecondaryVesselType() {
        return secondaryVesselType;
    }

    public boolean isSecondaryVesselTypeAll() {
        return SecondaryVesselTypeEnum.ALL.equals(getSecondaryVesselType());
    }

    public boolean isSecondaryVesselTypeScientific() {
        return SecondaryVesselTypeEnum.SCIENTIFIC.equals(getSecondaryVesselType());
    }

    public boolean isSecondaryVesselTypeFishing() {
        return SecondaryVesselTypeEnum.FISHING.equals(getSecondaryVesselType());
    }

    public boolean isSecondaryVesselTypeOnlyCruise() {
        return SecondaryVesselTypeEnum.ONLY_CRUISE.equals(getSecondaryVesselType());
    }

    public void setSecondaryVesselType(SecondaryVesselTypeEnum secondaryVesselType) {
        this.secondaryVesselType = secondaryVesselType;
        firePropertyChange(PROPERTY_SECONDARY_VESSEL_TYPE, null, secondaryVesselType);
        firePropertyChange(PROPERTY_SECONDARY_VESSEL_TYPE_ALL, null, isSecondaryVesselTypeAll());
        firePropertyChange(PROPERTY_SECONDARY_VESSEL_TYPE_SCIENTIFIC, null, isSecondaryVesselTypeScientific());
        firePropertyChange(PROPERTY_SECONDARY_VESSEL_TYPE_FISHING, null, isSecondaryVesselTypeFishing());
    }

    //------------------------------------------------------------------------//
    //-- AttachmentModelAware methods                                       --//
    //------------------------------------------------------------------------//

    @Override
    public ObjectTypeCode getObjectType() {
        return ObjectTypeCode.OPERATION;
    }

    @Override
    public Integer getObjectId() {
        return getIdAsInt();
    }

    @Override
    public List<Attachment> getAttachment() {
        return attachment;
    }

    @Override
    public void addAllAttachment(Collection<Attachment> attachments) {
        this.attachment.addAll(attachments);
        firePropertyChange(PROPERTY_ATTACHMENT, null, getAttachment());
    }

    @Override
    public void addAttachment(Attachment attachment) {
        this.attachment.add(attachment);
        firePropertyChange(PROPERTY_ATTACHMENT, null, getAttachment());
    }

    @Override
    public void removeAllAttachment(Collection<Attachment> attachments) {
        this.attachment.removeAll(attachments);
        firePropertyChange(PROPERTY_ATTACHMENT, null, getAttachment());
    }

    @Override
    public void removeAttachment(Attachment attachment) {
        this.attachment.remove(attachment);
        firePropertyChange(PROPERTY_ATTACHMENT, null, getAttachment());
    }

    //------------------------------------------------------------------------//
    //-- FishingOperation methods                                           --//
    //------------------------------------------------------------------------//

    @Override
    public Cruise getCruise() {
        return editObject.getCruise();
    }

    @Override
    public void setCruise(Cruise cruise) {
        editObject.setCruise(cruise);
    }

    @Override
    public String getStationNumber() {
        return editObject.getStationNumber();
    }

    @Override
    public void setStationNumber(String stationNumber) {
        Object oldValue = getStationNumber();
        editObject.setStationNumber(stationNumber);
        firePropertyChange(PROPERTY_STATION_NUMBER, oldValue, stationNumber);
    }

    @Override
    public Integer getFishingOperationNumber() {
        return editObject.getFishingOperationNumber();
    }

    @Override
    public void setFishingOperationNumber(Integer fishingOperationNumber) {
        Object oldValue = getFishingOperationNumber();
        editObject.setFishingOperationNumber(fishingOperationNumber);
        firePropertyChange(PROPERTY_FISHING_OPERATION_NUMBER, oldValue, fishingOperationNumber);
    }

    @Override
    public TuttiLocation getStrata() {
        return editObject.getStrata();
    }

    @Override
    public void setStrata(TuttiLocation strata) {
        Object oldValue = getStrata();
        editObject.setStrata(strata);
        firePropertyChange(PROPERTY_STRATA, oldValue, strata);
    }

    @Override
    public TuttiLocation getSubStrata() {
        return editObject.getSubStrata();
    }

    @Override
    public void setSubStrata(TuttiLocation subStrata) {
        Object oldValue = getSubStrata();
        editObject.setSubStrata(subStrata);
        firePropertyChange(PROPERTY_SUB_STRATA, oldValue, subStrata);
    }

    @Override
    public Float getGearShootingStartLatitude() {
        return editObject.getGearShootingStartLatitude();
    }

    @Override
    public void setGearShootingStartLatitude(Float gearShootingStartLatitude) {
        Object oldValue = getGearShootingStartLatitude();
        editObject.setGearShootingStartLatitude(gearShootingStartLatitude);
        firePropertyChange(PROPERTY_GEAR_SHOOTING_START_LATITUDE, oldValue, gearShootingStartLatitude);
    }

    @Override
    public Float getGearShootingStartLongitude() {
        return editObject.getGearShootingStartLongitude();
    }

    @Override
    public void setGearShootingStartLongitude(Float gearShootingStartLongitude) {
        Object oldValue = getGearShootingStartLongitude();
        editObject.setGearShootingStartLongitude(gearShootingStartLongitude);
        firePropertyChange(PROPERTY_GEAR_SHOOTING_START_LONGITUDE, oldValue, gearShootingStartLongitude);
    }

    @Override
    public Date getGearShootingStartDate() {
        return editObject.getGearShootingStartDate();
    }

    @Override
    public void setGearShootingStartDate(Date gearShootingStartDate) {
        Object oldValue = getGearShootingStartDate();
        if (gearShootingStartDate != null) {
            Calendar calendar = DateUtil.getDefaultCalendar(gearShootingStartDate);
            calendar.set(Calendar.SECOND, 0);
            editObject.setGearShootingStartDate(calendar.getTime());

        } else {
            editObject.setGearShootingStartDate(null);
        }
        firePropertyChange(PROPERTY_GEAR_SHOOTING_START_DATE, oldValue, getGearShootingStartDate());
    }

    @Override
    public Float getGearShootingEndLatitude() {
        return editObject.getGearShootingEndLatitude();
    }

    @Override
    public void setGearShootingEndLatitude(Float gearShootingEndLatitude) {
        Object oldValue = getGearShootingEndLatitude();
        editObject.setGearShootingEndLatitude(gearShootingEndLatitude);
        firePropertyChange(PROPERTY_GEAR_SHOOTING_END_LATITUDE, oldValue, gearShootingEndLatitude);
    }

    @Override
    public Float getGearShootingEndLongitude() {
        return editObject.getGearShootingEndLongitude();
    }

    @Override
    public void setGearShootingEndLongitude(Float gearShootingEndLongitude) {
        Object oldValue = getGearShootingEndLongitude();
        editObject.setGearShootingEndLongitude(gearShootingEndLongitude);
        firePropertyChange(PROPERTY_GEAR_SHOOTING_END_LONGITUDE, oldValue, gearShootingEndLongitude);
    }

    @Override
    public Date getGearShootingEndDate() {
        return editObject.getGearShootingEndDate();
    }

    @Override
    public void setGearShootingEndDate(Date gearShootingEndDate) {
        Object oldValue = getGearShootingEndDate();
        if (gearShootingEndDate != null) {
            Calendar calendar = DateUtil.getDefaultCalendar(gearShootingEndDate);
            calendar.set(Calendar.SECOND, 0);
            editObject.setGearShootingEndDate(calendar.getTime());

        } else {
            editObject.setGearShootingEndDate(null);
        }
        firePropertyChange(PROPERTY_GEAR_SHOOTING_END_DATE, oldValue, getGearShootingEndDate());
    }

    @Override
    public boolean isFishingOperationRectiligne() {
        return editObject.isFishingOperationRectiligne();
    }

    @Override
    public void setFishingOperationRectiligne(boolean fishingOperationRectiligne) {
        Object oldValue = isFishingOperationRectiligne();
        editObject.setFishingOperationRectiligne(fishingOperationRectiligne);
        firePropertyChange(PROPERTY_FISHING_OPERATION_RECTILIGNE, oldValue, fishingOperationRectiligne);
    }

    @Override
    public Integer getTrawlDistance() {
        return editObject.getTrawlDistance();
    }

    @Override
    public void setTrawlDistance(Integer trawlDistance) {
        Object oldValue = getTrawlDistance();
        editObject.setTrawlDistance(trawlDistance);
        firePropertyChange(PROPERTY_TRAWL_DISTANCE, oldValue, trawlDistance);
    }

    @Override
    public Boolean getFishingOperationValid() {
        return editObject.getFishingOperationValid();
    }

    @Override
    public void setFishingOperationValid(Boolean fishingOperationValid) {
        Object oldValue = getFishingOperationValid();
        editObject.setFishingOperationValid(fishingOperationValid);
        firePropertyChange(PROPERTY_FISHING_OPERATION_VALID, oldValue, fishingOperationValid);
    }

    @Override
    public TuttiLocation getLocation() {
        return editObject.getLocation();
    }

    @Override
    public void setLocation(TuttiLocation location) {
        Object oldValue = getLocation();
        editObject.setLocation(location);
        firePropertyChange(PROPERTY_LOCATION, oldValue, location);
    }

    @Override
    public String getComment() {
        return editObject.getComment();
    }

    @Override
    public void setComment(String comment) {
        Object oldValue = getComment();
        editObject.setComment(comment);
        firePropertyChange(PROPERTY_COMMENT, oldValue, comment);
    }

    @Override
    public String getSynchronizationStatus() {
        return editObject.getSynchronizationStatus();
    }

    @Override
    public void setSynchronizationStatus(String synchronizationStatus) {
        String oldValue = getSynchronizationStatus();
        editObject.setSynchronizationStatus(synchronizationStatus);
        firePropertyChange(PROPERTY_SYNCHRONIZATION_STATUS, oldValue, synchronizationStatus);
    }

    @Override
    public List<Person> getRecorderPerson() {
        return editObject.getRecorderPerson();
    }

    @Override
    public void setRecorderPerson(List<Person> recorderPerson) {
        Object oldValue = null;
        List<Person> oldRecorderPerson = getRecorderPerson();
        if (oldRecorderPerson != null) {
            oldValue = Lists.newArrayList(oldRecorderPerson);
        }
        editObject.setRecorderPerson(Lists.<Person>newArrayList());
        if (recorderPerson != null) {
            getRecorderPerson().addAll(recorderPerson);
        }
        firePropertyChange(PROPERTY_RECORDER_PERSON, oldValue, recorderPerson);
    }

    @Override
    public Gear getGear() {
        return editObject.getGear();
    }

    @Override
    public void setGear(Gear gear) {
        Object oldValue = getGear();
        editObject.setGear(gear);
        firePropertyChange(PROPERTY_GEAR, oldValue, gear);
    }

    @Override
    public Vessel getVessel() {
        return editObject.getVessel();
    }

    @Override
    public void setVessel(Vessel vessel) {
        Object oldValue = getVessel();
        editObject.setVessel(vessel);
        firePropertyChange(PROPERTY_VESSEL, oldValue, vessel);
    }

    @Override
    public String getMultirigAggregation() {
        return editObject.getMultirigAggregation();
    }

    @Override
    public void setMultirigAggregation(String multirigAggregation) {
        Object oldValue = getMultirigAggregation();
        editObject.setMultirigAggregation(multirigAggregation);
        firePropertyChange(PROPERTY_MULTIRIG_AGGREGATION, oldValue, multirigAggregation);
    }

    @Override
    public Vessel getSecondaryVessel(int index) {
        return editObject.getSecondaryVessel(index);
    }

    @Override
    public boolean isSecondaryVesselEmpty() {
        return editObject.isSecondaryVesselEmpty();
    }

    @Override
    public int sizeSecondaryVessel() {
        return editObject.sizeSecondaryVessel();
    }

    @Override
    public void addSecondaryVessel(Vessel secondaryVessel) {
        editObject.addSecondaryVessel(secondaryVessel);
        firePropertyChange(PROPERTY_SECONDARY_VESSEL, null, getSecondaryVessel());
    }

    @Override
    public void addAllSecondaryVessel(Collection<Vessel> secondaryVessel) {
        editObject.addAllSecondaryVessel(secondaryVessel);
        firePropertyChange(PROPERTY_SECONDARY_VESSEL, null, getSecondaryVessel());
    }

    @Override
    public boolean removeSecondaryVessel(Vessel secondaryVessel) {
        boolean result = editObject.removeSecondaryVessel(secondaryVessel);
        firePropertyChange(PROPERTY_SECONDARY_VESSEL, null, getSecondaryVessel());
        return result;
    }

    @Override
    public boolean removeAllSecondaryVessel(Collection<Vessel> secondaryVessel) {
        boolean result = editObject.removeAllSecondaryVessel(secondaryVessel);
        firePropertyChange(PROPERTY_SECONDARY_VESSEL, null, getSecondaryVessel());
        return result;
    }

    @Override
    public boolean containsSecondaryVessel(Vessel secondaryVessel) {
        return editObject.containsSecondaryVessel(secondaryVessel);
    }

    @Override
    public boolean containsAllSecondaryVessel(Collection<Vessel> secondaryVessel) {
        return editObject.containsAllSecondaryVessel(secondaryVessel);
    }

    @Override
    public List<Vessel> getSecondaryVessel() {
        return editObject.getSecondaryVessel();
    }

    @Override
    public void setSecondaryVessel(List<Vessel> secondaryVessel) {
        editObject.setSecondaryVessel(secondaryVessel);
        firePropertyChange(PROPERTY_SECONDARY_VESSEL, null, getSecondaryVessel());
    }

    List<Vessel> allSecondaryVessel;

    List<Vessel> onlyCruisSecondaryVessel;

    List<Vessel> scientificSecondaryVessel;

    List<Vessel> fishingSecondaryVessel;

    public List<Vessel> getAllSecondaryVessel() {
        return allSecondaryVessel;
    }

    public void setAllSecondaryVessel(List<Vessel> allSecondaryVessel) {
        this.allSecondaryVessel = allSecondaryVessel;
    }

    public List<Vessel> getOnlyCruisSecondaryVessel() {
        return onlyCruisSecondaryVessel;
    }

    public void setOnlyCruisSecondaryVessel(List<Vessel> onlyCruisSecondaryVessel) {
        this.onlyCruisSecondaryVessel = onlyCruisSecondaryVessel;
    }

    public List<Vessel> getFishingSecondaryVessel() {
        return fishingSecondaryVessel;
    }

    public void setFishingSecondaryVessel(List<Vessel> fishingSecondaryVessel) {
        this.fishingSecondaryVessel = fishingSecondaryVessel;
    }

    public List<Vessel> getScientificSecondaryVessel() {
        return scientificSecondaryVessel;
    }

    public void setScientificSecondaryVessel(List<Vessel> scientificSecondaryVessel) {
        this.scientificSecondaryVessel = scientificSecondaryVessel;
    }

    @Override
    public boolean isPlanktonObserved() {
        return false;
    }

    @Override
    public void setPlanktonObserved(boolean planktonObserved) {
    }

    @Override
    public CaracteristicMap getVesselUseFeatures() {
        return null;
    }

    @Override
    public void setVesselUseFeatures(CaracteristicMap vesselUseFeatures) {
    }

    @Override
    public CaracteristicMap getGearUseFeatures() {
        return null;
    }

    @Override
    public void setGearUseFeatures(CaracteristicMap gearUseFeatures) {
    }

    @Override
    public boolean isAccidentalObserved() {
        return false;
    }

    @Override
    public void setAccidentalObserved(boolean accidentalObserved) {
    }

    @Override
    public Person getRecorderPerson(int index) {
        return null;
    }

    @Override
    public boolean isRecorderPersonEmpty() {
        return false;
    }

    @Override
    public int sizeRecorderPerson() {
        return 0;
    }

    @Override
    public void addRecorderPerson(Person recorderPerson) {
    }

    @Override
    public void addAllRecorderPerson(Collection<Person> recorderPerson) {
    }

    @Override
    public boolean removeRecorderPerson(Person recorderPerson) {
        return false;
    }

    @Override
    public boolean removeAllRecorderPerson(Collection<Person> recorderPerson) {
        return false;
    }

    @Override
    public boolean containsRecorderPerson(Person recorderPerson) {
        return false;
    }

    @Override
    public boolean containsAllRecorderPerson(Collection<Person> recorderPerson) {
        return false;
    }

    public boolean isImportFromColumnFileEnabled() {
        return importFromColumnFileEnabled;
    }

    public void setImportFromColumnFileEnabled(boolean importFromColumnFileEnabled) {
        this.importFromColumnFileEnabled = importFromColumnFileEnabled;
    }
}

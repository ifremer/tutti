package fr.ifremer.tutti.ui.swing.content.operation.catches.species.edit;

/*
 * #%L
 * Tutti :: UI
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.entities.data.SampleCategory;
import fr.ifremer.tutti.type.WeightUnit;
import fr.ifremer.tutti.ui.swing.TuttiUIContext;
import org.nuiton.jaxx.widgets.number.NumberEditor;
import org.apache.commons.lang3.StringUtils;
import org.nuiton.decorator.Decorator;

import javax.swing.AbstractCellEditor;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.event.AncestorEvent;
import javax.swing.event.AncestorListener;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.io.Serializable;

/**
 * To render and edit a {@link SampleCategory}.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 0.3
 */
public class SampleCategoryComponent {

    public static <C extends Serializable> TableCellRenderer newRender(
            TableCellRenderer renderer,
            Decorator<C> decorator,
            Color computedDataColor,
            WeightUnit weightUnit) {
        return new SampleCategoryRenderer<>(renderer,
                                            decorator,
                                            computedDataColor,
                                            weightUnit);
    }

    public static <C extends Serializable> TableCellEditor newEditor(Decorator<C> decorator,
                                                                     WeightUnit weightUnit) {
        return new SampleCategoryEditor<>(decorator, weightUnit);
    }

    /**
     * SampleCategory cell editor.
     *
     * @author Tony Chemit - chemit@codelutin.com
     * @since 0.3
     */
    public static class SampleCategoryEditor<C extends Serializable> extends AbstractCellEditor
            implements TableCellEditor, FocusListener, AncestorListener {

        private static final long serialVersionUID = 1L;

        protected final NumberEditor numberEditor;

        protected final JPanel editor;

        protected final JLabel editorLabel;

        protected final Decorator<C> categoryDecorator;

        private final WeightUnit weightUnit;

        public SampleCategoryEditor(Decorator<C> categoryDecorator, WeightUnit weightUnit) {
            this.categoryDecorator = categoryDecorator;
            this.weightUnit = weightUnit;
            numberEditor = new NumberEditor();
            numberEditor.getTextField().setHorizontalAlignment(SwingConstants.RIGHT);
            numberEditor.getTextField().setBorder(null);
            numberEditor.getTextField().addFocusListener(this);
            numberEditor.getTextField().addAncestorListener(this);
            numberEditor.setNumberType(Float.class);
            numberEditor.setUseSign(false);
            numberEditor.setNumberPattern(weightUnit.getNumberEditorPattern());
            numberEditor.init();

            editor = new JPanel(new BorderLayout());
            editor.add(BorderLayout.WEST, editorLabel = new JLabel());
            editor.add(BorderLayout.CENTER, numberEditor);
            editor.setOpaque(true);
            editorLabel.setOpaque(true);
        }

        public NumberEditor getNumberEditor() {
            return numberEditor;
        }

        @Override
        public Component getTableCellEditorComponent(JTable table,
                                                     Object value,
                                                     boolean isSelected,
                                                     int row,
                                                     int column) {

            SampleCategory<C> sampleCategory = (SampleCategory<C>) value;

            C categoryValue = sampleCategory == null ? null : sampleCategory.getCategoryValue();
            Float number = sampleCategory == null ? null : sampleCategory.getCategoryWeight();

            numberEditor.setNumberValue(number);

            // Check nullity and set the text that will be selected with the current value
            if (number == null) {
                numberEditor.getModel().setTextValue("");
            } else {
//                numberEditor.setModelText(String.valueOf(number));
                numberEditor.getModel().setTextValue(weightUnit.renderWeight(number));
            }

            String label = sampleCategory == null ? "-" :
                           categoryDecorator.toString(categoryValue);

            if (isSelected) {

                editorLabel.setFont(table.getFont().deriveFont(Font.BOLD));
                editorLabel.setBackground(TuttiUIContext.getApplicationContext().getConfig().getColorSelectedRow());
            } else {
                editorLabel.setFont(table.getFont());
                editorLabel.setBackground(null);
            }
            editorLabel.setText(label);
            return editor;
        }

        @Override
        public Float getCellEditorValue() {
            return (Float) numberEditor.getModel().getNumberValue();
        }

        @Override
        public void focusGained(FocusEvent e) {
            SwingUtilities.invokeLater(() -> {
                numberEditor.getTextField().requestFocus();
                numberEditor.getTextField().selectAll();
            });
        }

        @Override
        public void focusLost(FocusEvent e) {
        }

        @Override
        public void ancestorAdded(AncestorEvent event) {
            SwingUtilities.invokeLater(() -> {
                numberEditor.getTextField().requestFocus();
                numberEditor.getTextField().selectAll();
            });
        }

        @Override
        public void ancestorRemoved(AncestorEvent event) {
        }

        @Override
        public void ancestorMoved(AncestorEvent event) {
        }

        @Override
        public boolean stopCellEditing() {
            boolean result = super.stopCellEditing();
            // Reset previous data to avoid keeping it on other cell edition
            if (result) {
                resetEditor();
            }
            return result;
        }

        @Override
        public void cancelCellEditing() {
            resetEditor();
            super.cancelCellEditing();
        }

        protected void resetEditor() {
            numberEditor.setNumberValue(null);
            // Use empty string, otherwise there is a NPE in NumberEditorHandler
            numberEditor.getModel().setTextValue("");
            editorLabel.setText("-");
        }
    }

    /**
     * To render a {@link SampleCategory} in a table cell.
     *
     * @author Tony Chemit - chemit@codelutin.com
     * @since 0.3
     */
    public static class SampleCategoryRenderer<C extends Serializable> implements TableCellRenderer {

        protected final TableCellRenderer delegate;

        protected final Decorator<C> categoryDecorator;

        protected final Color computedWeightColor;

        protected final WeightUnit weightUnit;

        JPanel editor;

        JLabel editorLabel;

        JLabel editorValue;

        public SampleCategoryRenderer(TableCellRenderer delegate,
                                      Decorator<C> categoryDecorator,
                                      Color computedWeightColor,
                                      WeightUnit weightUnit) {
            this.delegate = delegate;
            this.categoryDecorator = categoryDecorator;
            this.computedWeightColor = computedWeightColor;
            this.weightUnit = weightUnit;

            editor = new JPanel();
            editor.setOpaque(true);
            editorLabel = new JLabel();
            editorValue = new JLabel();
            editor.setLayout(new BorderLayout());
            editor.add(editorLabel, BorderLayout.CENTER);
            editor.add(editorValue, BorderLayout.EAST);
        }

        @Override
        public Component getTableCellRendererComponent(JTable table,
                                                       Object value,
                                                       boolean isSelected,
                                                       boolean hasFocus,
                                                       int row,
                                                       int column) {

            SampleCategory<C> sampleCategory = (SampleCategory<C>) value;

            String prefixText = getPrefixText(sampleCategory);
            editorLabel.setText(prefixText);

            String valueText = getValueText(sampleCategory);

            String valueTextHtml = valueText;

            if (isSelected) {
                valueTextHtml = String.format("<strong>%s</strong>", valueTextHtml);
                editorLabel.setFont(table.getFont().deriveFont(Font.BOLD));
            } else {
                editorLabel.setFont(table.getFont());
            }
            valueTextHtml = String.format("<html>%s</html>", valueTextHtml);

            editorValue.setText(valueTextHtml);

            String tip = null;

            if (StringUtils.isNotBlank(prefixText)) {
                tip = String.format("<span style='white-space: nowrap;'>%s</span> %s", prefixText, valueText);

                if (isSelected) {
                    tip = String.format("<strong>%s</strong>", tip);
                }
                tip = String.format("<html>%s</html>", tip);
            }

            editor.setToolTipText(tip);

            return editor;
        }

        protected String getPrefixText(SampleCategory<C> sampleCategory) {
            String text = null;
            if (sampleCategory != null) {
                C categoryValue = sampleCategory.getCategoryValue();
                if (categoryValue != null) {

                    text = categoryDecorator.toString(categoryValue) + " /";

                    if (sampleCategory.isSubSample()) {
                        text += "/";
                    }
                }
            }
            return text == null ? "" : text;
        }

        protected String getValueText(SampleCategory<C> sampleCategory) {
            String text = "";
            if (sampleCategory != null) {
                C categoryValue = sampleCategory.getCategoryValue();
                if (categoryValue != null) {
                    Float number = sampleCategory.getCategoryWeight();
                    Float computedNumber = sampleCategory.getComputedWeight();

                    if (number != null) {
//                        text += JAXXUtil.getStringValue(number);

                        text += weightUnit.renderWeight(number);

                    } else if (computedNumber != null) {

                        String weightStr = weightUnit.renderWeight(computedNumber);

                        if (sampleCategory.hasOnlyOneFrequency()) {
//                            text += Weights.getWeightStringValue(computedNumber);
                            text += weightStr;

                        } else {
                            String color = Integer.toHexString(computedWeightColor.getRGB()).substring(2);
                            text += "<em style='color: #" + color + "'>" +
//                                    Weights.getWeightStringValue(computedNumber) + "</em>";
                                    weightStr + "</em>";
                        }
                    } else {
                        text += "-";
                    }
                }
            }
            return text;
        }
    }
}

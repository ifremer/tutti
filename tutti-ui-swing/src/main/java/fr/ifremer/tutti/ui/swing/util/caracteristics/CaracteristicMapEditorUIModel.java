package fr.ifremer.tutti.ui.swing.util.caracteristics;

/*
 * #%L
 * Tutti :: UI
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.entities.CaracteristicMap;
import fr.ifremer.tutti.persistence.entities.referential.Caracteristic;
import fr.ifremer.tutti.ui.swing.util.table.AbstractTuttiTableUIModel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * @author Kevin Morin - kmorin@codelutin.com
 * @since 1.4
 */
public class CaracteristicMapEditorUIModel extends AbstractTuttiTableUIModel<Object, CaracteristicMapEditorRowModel, CaracteristicMapEditorUIModel> {

    public static final String PROPERTY_CARACTERISTIC_MAP = "caracteristicMap";

    public static final String PROPERTY_REMOVE_CARACTERISTIC_ENABLED = "removeCaracteristicEnabled";

    private static final long serialVersionUID = 1L;

    /**
     * Original available caracteristics, can contain the default caracteristics
     */
    protected List<Caracteristic> allAvailableCaracteristics;

    /**
     * The available caracteristics without the default caracteristics
     */
    protected List<Caracteristic> availableCaracteristics;

    /**
     * Caracteristics
     *
     * @since 1.0
     */
    protected CaracteristicMap caracteristicMap;

    /**
     * Can user remove a selected caracteristic?
     *
     * @since 1.0
     */
    protected boolean removeCaracteristicEnabled;

    public CaracteristicMapEditorUIModel() {
        super(Object.class, null, null);
    }

    public void setAllAvailableCaracteristics(List<Caracteristic> allAvailableCaracteristics) {
        this.allAvailableCaracteristics = allAvailableCaracteristics;
        this.availableCaracteristics = new ArrayList<>(allAvailableCaracteristics);
    }

    public List<Caracteristic> getAvailableCaracteristics() {
        return availableCaracteristics;
    }

    public void computeAvailableCaracteristics(Collection<Caracteristic> defaultCaracteristics) {
        availableCaracteristics = new ArrayList<>(allAvailableCaracteristics);
        availableCaracteristics.removeAll(defaultCaracteristics);
    }

    public CaracteristicMap getCaracteristicMap() {
        return caracteristicMap;
    }

    public void setCaracteristicMap(CaracteristicMap caracteristicMap) {
        Object oldValue = getCaracteristicMap();
        this.caracteristicMap = caracteristicMap != null ? (CaracteristicMap) caracteristicMap.clone() : null;
        firePropertyChange(PROPERTY_CARACTERISTIC_MAP, oldValue, this.caracteristicMap);
    }

    public boolean isRemoveCaracteristicEnabled() {
        return removeCaracteristicEnabled;
    }

    public void setRemoveCaracteristicEnabled(boolean removeCaracteristicEnabled) {
        Object oldValue = isRemoveCaracteristicEnabled();
        this.removeCaracteristicEnabled = removeCaracteristicEnabled;
        firePropertyChange(PROPERTY_REMOVE_CARACTERISTIC_ENABLED, oldValue, removeCaracteristicEnabled);
    }

    @Override
    protected Object newEntity() {
        return null;
    }
}

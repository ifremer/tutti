/*
 * #%L
 * Tutti :: UI
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

BeanFilterableComboBox {
  showReset: true;
  bean: {model};
}

#selectCruiseTopPanel {
  _help: {"tutti.selectCruise.help"};
}

#warningContainer {
  background: {new java.awt.Color(245, 218, 88)};
  visible: {!model.isValid()};
}

#warningLabel {
  actionIcon: warning;
  border: {new javax.swing.border.EmptyBorder(5, 10, 5, 10)};
  text: "tutti.selectCruise.warn.invalid.selected.data";
}

#programLabel {
  text: "tutti.selectCruise.field.program";
  actionIcon: program;
  toolTipText: "tutti.selectCruise.field.program.tip";
  _help: {"tutti.selectCruise.field.program.help"};
}

#programCombobox {
  property: program;
  selectedItem: {model.getProgram()};
  enabled: {!programCombobox.isEmpty()};
  _validatorLabel: {t("tutti.selectCruise.field.program")};
  _help: {"tutti.selectCruise.field.program.help"};
}

#editProgramButton {
  actionIcon: edit;
  text: "tutti.selectCruise.action.editProgram";
  enabled: {model.getProgram() != null};
  _applicationAction: {fr.ifremer.tutti.ui.swing.content.home.actions.EditProgramAction.class};
  toolTipText: "tutti.selectCruise.action.editProgram.tip";
  _help: {"tutti.selectCruise.action.editProgram.help"};
}

#newProgramButton {
  actionIcon: add;
  text: "tutti.selectCruise.action.newProgram";
  _applicationAction: {fr.ifremer.tutti.ui.swing.content.home.actions.NewProgramAction.class};
  toolTipText: "tutti.selectCruise.action.newProgram.tip";
  _help: {"tutti.selectCruise.action.newProgram.help"};
}

#cruiseLabel {
	text: "tutti.selectCruise.field.cruise";
  labelFor: {cruiseCombobox};
  icon: {TuttiUIUtil.getCruiseIcon(model.getCruise())};
  toolTipText: "tutti.selectCruise.field.cruise.tip";
  _help: {"tutti.selectCruise.field.cruise.help"};
}

#cruiseCombobox {
  property: cruise;
  selectedItem: {model.getCruise()};
  enabled: {!cruiseCombobox.isEmpty()};
  _validatorLabel: {t("tutti.selectCruise.field.cruise")};
  _help: {"tutti.selectCruise.field.cruise.help"};
}

#editCruiseComboBox {
  enabled: {model.isProgramFound() && model.isCruiseFound()};
  _comboboxActions: {Arrays.asList(editCruiseButton, sendCruiseReportButton, exportCruiseForSumatraButton, calcifiedPiecesSamplingReportButton, speciesToConfirmReportForCruiseButton)};
}

#editCruiseButton {
  actionIcon: edit;
  enabled: {model.isCruiseFound()};
  text: "tutti.selectCruise.action.editCruise";
  _applicationAction: {fr.ifremer.tutti.ui.swing.content.home.actions.EditCruiseAction.class};
  toolTipText: "tutti.selectCruise.action.editCruise.tip";
  _help: {"tutti.selectCruise.action.editCruise.help"};
}

#sendCruiseReportButton {
  actionIcon: email;
  text: "tutti.selectCruise.action.sendCruiseReport";
  toolTipText: "tutti.selectCruise.action.sendCruiseReport.tip";
  _applicationAction: {fr.ifremer.tutti.ui.swing.content.home.actions.SendCruiseReportAction.class};
  _help: {"tutti.selectCruise.action.sendCruiseReport.help"};
}

#exportCruiseForSumatraButton {
  actionIcon: export;
  text: "tutti.selectCruise.action.exportCruiseForSumatra";
  toolTipText: "tutti.selectCruise.action.exportCruiseForSumatra.tip";
  _applicationAction: {fr.ifremer.tutti.ui.swing.content.home.actions.ExportCruiseForSumatraAction.class};
  _help: {"tutti.selectCruise.action.exportCruiseForSumatra.help"};
}

#calcifiedPiecesSamplingReportButton {
  actionIcon: report;
  text: "tutti.selectCruise.action.calcifiedPiecesSamplingReport";
  toolTipText: "tutti.selectCruise.action.calcifiedPiecesSamplingReport.tip";
  _applicationAction: {fr.ifremer.tutti.ui.swing.content.home.actions.CalcifiedPiecesSamplingReportAction.class};
  _help: {"tutti.selectCruise.action.calcifiedPiecesSamplingReport.help"};
}

#speciesToConfirmReportForCruiseButton {
  actionIcon: report;
  text: "tutti.selectCruise.action.speciesToConfirmReportForCruise";
  toolTipText: "tutti.selectCruise.action.speciesToConfirmReportForCruise.tip";
  _applicationAction: {fr.ifremer.tutti.ui.swing.content.home.actions.SpeciesToConfirmReportForCruiseAction.class};
  _help: {"tutti.selectCruise.action.speciesToConfirmReportForCruise.help"};
}

#newCruiseButton {
  actionIcon: add;
  text: "tutti.selectCruise.action.newCruise";
  enabled: {model.isProgramFound()};
  _applicationAction: {fr.ifremer.tutti.ui.swing.content.home.actions.NewCruiseAction.class};
  toolTipText: "tutti.selectCruise.action.newCruise.tip";
  _help: {"tutti.selectCruise.action.newCruise.help"};
}

#protocolLabel {
  text: "tutti.selectCruise.field.protocol";
  labelFor: {protocolCombobox};
  actionIcon: protocol;
  toolTipText: "tutti.selectCruise.field.protocol.tip";
  _help: {"tutti.selectCruise.field.protocol.help"};
}

#protocolCombobox {
  property: protocol;
  selectedItem: {model.getProtocol()};
  enabled: {!protocolCombobox.isEmpty()};
  _validatorLabel: {t("tutti.selectCruise.field.protocol")};
  _help: {"tutti.selectCruise.field.protocol.help"};
}

#newProtocolComboBox {
  enabled: {model.isProgramFound()};
  _comboboxActions: {Arrays.asList(newProtocolButton, importProtocolButton)};
}

#editProtocolComboBox {
  enabled: {model.isProtocolFound()};
  _comboboxActions: {Arrays.asList(editProtocolButton, cloneProtocolButton, exportProtocolButton, deleteProtocolButton)};
}

#newProtocolButton {
  actionIcon: add;
  text: "tutti.selectCruise.action.newProtocol";
  _applicationAction: {fr.ifremer.tutti.ui.swing.content.home.actions.NewProtocolAction.class};
  enabled: {model.isProgramFound()};
  toolTipText: "tutti.selectCruise.action.newProtocol.tip";
  _help: {"tutti.selectCruise.action.newProtocol.help"};
}

#importProtocolButton {
  actionIcon: import;
  text: "tutti.selectCruise.action.importProtocol";
  _applicationAction: {fr.ifremer.tutti.ui.swing.content.home.actions.ImportProtocolAction.class};
  enabled: {model.isProgramFound()};
  toolTipText: "tutti.selectCruise.action.importProtocol.tip";
  _help: {"tutti.selectCruise.action.importProtocol.help"};
}

#editProtocolButton {
  actionIcon: edit;
  text: "tutti.selectCruise.action.editProtocol";
  _applicationAction: {fr.ifremer.tutti.ui.swing.content.home.actions.EditProtocolAction.class};
  toolTipText: "tutti.selectCruise.action.editProtocol.tip";
  enabled: {model.isProtocolFound() && model.isProgramFound()};
  _help: {"tutti.selectCruise.action.editProtocol.help"};
}

#cloneProtocolButton {
  actionIcon: copy;
  text: "tutti.selectCruise.action.cloneProtocol";
  _applicationAction: {fr.ifremer.tutti.ui.swing.content.home.actions.CloneProtocolAction.class};
  toolTipText: "tutti.selectCruise.action.cloneProtocol.tip";
  enabled: {model.isProtocolFound() && model.isProgramFound()};
  _help: {"tutti.selectCruise.action.cloneProtocol.help"};
}

#exportProtocolButton {
  actionIcon: export;
  text: "tutti.selectCruise.action.exportProtocol";
  _applicationAction: {fr.ifremer.tutti.ui.swing.content.home.actions.ExportProtocolAction.class};
  toolTipText: "tutti.selectCruise.action.exportProtocol.tip";
  enabled: {model.isProtocolFound()};
  _help: {"tutti.selectCruise.action.exportProtocol.help"};
}

#deleteProtocolButton {
  actionIcon: delete;
  text: "tutti.selectCruise.action.deleteProtocol";
  _applicationAction: {fr.ifremer.tutti.ui.swing.content.home.actions.DeleteProtocolAction.class};
  toolTipText: "tutti.selectCruise.action.deleteProtocol.tip";
  enabled: {model.isProtocolFound()};
  _help: {"tutti.selectCruise.action.deleteProtocol.help"};
}

#editCatchesButton {
  actionIcon: edit;
  text: "tutti.selectCruise.action.editCatches";
  i18nMnemonic: "tutti.selectCruise.action.editCatches.mnemonic";
  _applicationAction: {fr.ifremer.tutti.ui.swing.content.home.actions.EditCatchesAction.class};
  enabled: {model.isValid() && model.isCruiseFound()};
  toolTipText: "tutti.selectCruise.action.editCatches.tip";
  _help: {"tutti.selectCruise.action.editCatches.help"};
}

#validateCatchesButton {
  actionIcon: validate;
  text: "tutti.selectCruise.action.validateCatches";
  toolTipText: "tutti.selectCruise.action.validateCatches.tip";
  i18nMnemonic: "tutti.selectCruise.action.validateCatches.mnemonic";
  _applicationAction: {fr.ifremer.tutti.ui.swing.content.home.actions.ValidateCatchesAction.class};
  enabled: {model.isValid() && model.isCruiseFound()};
  _help: {"tutti.selectCruise.action.validateCatches.help"};
}

package fr.ifremer.tutti.ui.swing.content.operation.fishing;

/*
 * #%L
 * Tutti :: UI
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.nuiton.jaxx.application.swing.table.AbstractApplicationTableModel;
import org.nuiton.jaxx.application.swing.table.ColumnIdentifier;
import org.jdesktop.swingx.table.TableColumnModelExt;

import static org.nuiton.i18n.I18n.n;

/**
 * @author kmorin
 * @since 0.3
 */
public class GearUseFeatureTableModel extends AbstractApplicationTableModel<GearUseFeatureRowModel> {

    private static final long serialVersionUID = 1L;

    public static final ColumnIdentifier<GearUseFeatureRowModel> KEY = ColumnIdentifier.newId(
            GearUseFeatureRowModel.PROPERTY_KEY,
            n("tutti.gearUseFeatureTable.table.header.key"),
            n("tutti.gearUseFeatureTable.table.header.key"));

    public static final ColumnIdentifier<GearUseFeatureRowModel> VALUE = ColumnIdentifier.newId(
            GearUseFeatureRowModel.PROPERTY_VALUE,
            n("tutti.gearUseFeatureTable.table.header.value"),
            n("tutti.gearUseFeatureTable.table.header.value"));

    public GearUseFeatureTableModel(TableColumnModelExt columnModel) {
        super(columnModel, false, false);
        setNoneEditableCols(KEY);
    }

    @Override
    public GearUseFeatureRowModel createNewRow() {
        return new GearUseFeatureRowModel();
    }

}

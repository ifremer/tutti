package fr.ifremer.tutti.ui.swing.util.caracteristics;

/*
 * #%L
 * Tutti :: UI
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import com.google.common.collect.Sets;
import fr.ifremer.tutti.persistence.entities.CaracteristicMap;
import fr.ifremer.tutti.persistence.entities.referential.Caracteristic;
import fr.ifremer.tutti.persistence.entities.referential.CaracteristicQualitativeValue;
import fr.ifremer.tutti.ui.swing.TuttiUIContext;
import fr.ifremer.tutti.ui.swing.util.TuttiUI;
import fr.ifremer.tutti.ui.swing.util.TuttiUIUtil;
import jaxx.runtime.SwingUtil;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.decorator.Decorator;
import org.nuiton.jaxx.application.swing.table.AbstractApplicationTableModel;

import javax.swing.AbstractCellEditor;
import javax.swing.JTable;
import javax.swing.border.LineBorder;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;
import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.Serializable;
import java.util.Collections;
import java.util.Objects;
import java.util.Set;

import static org.nuiton.i18n.I18n.t;

/**
 * @author Kevin Morin - kmorin@codelutin.com
 * @since 1.4
 */
public class CaracteristicMapCellComponent extends DefaultTableCellRenderer {

    private static final long serialVersionUID = 1L;

    /** Logger. */
    private static final Log log =
            LogFactory.getLog(CaracteristicMapCellComponent.class);

    protected Decorator<Caracteristic> caracteristicDecorator;

    protected Decorator<CaracteristicQualitativeValue> valueDecorator;

    protected Color computedDataColor;

    public CaracteristicMapCellComponent(TuttiUIContext context) {
        super();
        this.caracteristicDecorator =
                context.getDecoratorService().getDecoratorByType(Caracteristic.class);
        this.valueDecorator =
                context.getDecoratorService().getDecoratorByType(CaracteristicQualitativeValue.class);
        this.computedDataColor = context.getConfig().getColorComputedWeights();

        setHorizontalAlignment(CENTER);
        setIcon(SwingUtil.createActionIcon("show-frequency"));
    }

    public void setText(CaracteristicMap map) {
        String s = "-";
        if (map != null && map.size() > 0) {
            s = String.valueOf(map.size());
        }
        setText(s);
        Font f = this.getFont();
        f = f.deriveFont(Font.ITALIC);
        setFont(f);
        setForeground(computedDataColor);
    }

    public void setToolTipText(CaracteristicMap map) {
        StringBuilder builder = new StringBuilder();
        if (map != null) {
            builder.append("<html><ul>");
            if (MapUtils.isEmpty(map)) {
                builder.append("<li>");
                builder.append(t("tutti.caracteristicMapEditor.none.tip"));
                builder.append("</li>");
            }
            for (Caracteristic caracteristic : map.keySet()) {
                builder.append("<li>");
                builder.append(caracteristicDecorator.toString(caracteristic));
                builder.append(" : ");
                Serializable bean = map.get(caracteristic);
                if (bean instanceof CaracteristicQualitativeValue) {
                    builder.append(valueDecorator.toString(bean));
                } else {
                    builder.append(bean);
                }
                builder.append("</li>");
            }
            builder.append("</ul></html>");
        }
        setToolTipText(builder.toString());
    }

    public static TableCellRenderer newRender(TuttiUIContext context) {
        return new CaracteristicMapCellRenderer(context);
    }

    public static CaracteristicMapCellEditor newEditor(TuttiUI ui, Set<Caracteristic> caracteristicsToSkip) {
        return new CaracteristicMapCellEditor(ui, caracteristicsToSkip);
    }

    public static class CaracteristicMapCellEditor extends AbstractCellEditor implements TableCellEditor {

        private static final long serialVersionUID = 1L;

        protected final CaracteristicMapCellComponent component;

        protected final TuttiUI ui;

        protected JTable table;

        protected AbstractApplicationTableModel<CaracteristicMapColumnRowModel> tableModel;

        protected CaracteristicMapColumnRowModel editRow;

        protected Set<Caracteristic> caracteristicsUsed;

        protected Set<Caracteristic> caracteristicsToSkip;

        protected Integer rowIndex;

        protected Integer columnIndex;

        public CaracteristicMapCellEditor(TuttiUI ui,
                                          Set<Caracteristic> caracteristicsToSkip) {
            this.ui = ui;
            this.caracteristicsToSkip = Collections.unmodifiableSet(caracteristicsToSkip);
            component = new CaracteristicMapCellComponent(ui.getHandler().getContext());
            component.setBorder(new LineBorder(Color.BLACK));
            component.addKeyListener(new KeyAdapter() {
                @Override
                public void keyReleased(KeyEvent e) {
                    if (e.getKeyCode() == KeyEvent.VK_ENTER ||
                        e.getKeyCode() == KeyEvent.VK_SPACE) {
                        e.consume();
                        startEdit();
                    }
                }
            });

            component.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                    e.consume();
                    startEdit();
                }
            });
        }

        public void setCaracteristicsToSkip(Set<Caracteristic> caracteristicsToSkip) {
            Objects.requireNonNull(caracteristicsToSkip);
            this.caracteristicsToSkip = Collections.unmodifiableSet(caracteristicsToSkip);
        }

        protected void startEdit() {

            Preconditions.checkNotNull(tableModel, "No table model assigned.");

            // open frequency dialog

            Preconditions.checkNotNull(editRow, "No editRow found.");

            if (log.isDebugEnabled()) {
                log.debug("Will edit frequencies for row: " + rowIndex);
            }

            // get the caracteristics set to the other rows
            if (caracteristicsUsed == null) {
                caracteristicsUsed = Sets.newHashSet();
                for (CaracteristicMapColumnRowModel row : tableModel.getRows()) {
                    CaracteristicMap map = row.getCaracteristics();
                    if (map != null) {
                        caracteristicsUsed.addAll(map.keySet());
                    }
                }
            }
            caracteristicsUsed.removeAll(caracteristicsToSkip);

            CaracteristicMapColumnUIHandler handler = (CaracteristicMapColumnUIHandler) ui.getHandler();
            CaracteristicMapEditorUI caracteristicMapEditor = handler.getCaracteristicMapEditor();
            // remove all default caracteristics (caracteristicsToSkip)
            caracteristicMapEditor.getModel().computeAvailableCaracteristics(caracteristicsToSkip);
            caracteristicMapEditor.getHandler().editBatch(editRow, this, caracteristicsUsed);
            handler.showCaracteristicMapEditor(editRow);
        }

        public void validateEdition(CaracteristicMapEditorUIModel caracteristicMapEditorModel) {
            CaracteristicMap map = caracteristicMapEditorModel.getCaracteristicMap();
            component.setText(map);
            component.setToolTipText(map);
            editRow.setCaracteristics(map);
            caracteristicsUsed.addAll(map.keySet());

            int r = rowIndex;
            int c = columnIndex;

            // stop edition
            stopCellEditing();

            // reselect this cell
            TuttiUIUtil.doSelectCell(table, r, c);
            table.requestFocus();
        }

        public void closeEditor() {
            CaracteristicMapColumnUIHandler handler = (CaracteristicMapColumnUIHandler) ui.getHandler();
            handler.hideCaracteristicMapEditor();
        }

        @Override
        public Component getTableCellEditorComponent(JTable table,
                                                     Object value,
                                                     boolean isSelected,
                                                     int row,
                                                     int column) {
            tableModel = (AbstractApplicationTableModel) table.getModel();
            this.table = table;

            rowIndex = row;
            columnIndex = column;

            editRow = tableModel.getEntry(row);

            component.setText(editRow.getCaracteristics());
            component.setToolTipText(editRow.getCaracteristics());

            return component;
        }

        @Override
        public Object getCellEditorValue() {

            Preconditions.checkNotNull(editRow, "No editRow found in editor.");

            return editRow.getCaracteristics();
        }

        @Override
        public void cancelCellEditing() {
            super.cancelCellEditing();
            rowIndex = null;
            columnIndex = null;
            editRow = null;
        }
    }

    public static class CaracteristicMapCellRenderer implements TableCellRenderer {

        protected final CaracteristicMapCellComponent component;

        public CaracteristicMapCellRenderer(TuttiUIContext context) {
            component = new CaracteristicMapCellComponent(context);
        }

        @Override
        public Component getTableCellRendererComponent(JTable table,
                                                       Object value,
                                                       boolean isSelected,
                                                       boolean hasFocus,
                                                       int row,
                                                       int column) {

            CaracteristicMapCellComponent result =
                    (CaracteristicMapCellComponent) component.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);

            boolean editable = table.isCellEditable(row, column);
            result.setEnabled(editable);

            CaracteristicMap map = (CaracteristicMap) value;
            result.setText(map);
            result.setToolTipText(map);

            return result;
        }
    }
}

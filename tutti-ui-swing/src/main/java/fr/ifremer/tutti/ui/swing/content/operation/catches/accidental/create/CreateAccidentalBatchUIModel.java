package fr.ifremer.tutti.ui.swing.content.operation.catches.accidental.create;

/*
 * #%L
 * Tutti :: UI
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.entities.data.AccidentalBatch;
import fr.ifremer.tutti.persistence.entities.data.AccidentalBatchs;
import fr.ifremer.tutti.persistence.entities.referential.Caracteristic;
import fr.ifremer.tutti.persistence.entities.referential.CaracteristicQualitativeValue;
import fr.ifremer.tutti.persistence.entities.referential.Species;
import fr.ifremer.tutti.ui.swing.util.AbstractTuttiBeanUIModel;

/**
 * @author Kevin Morin - kmorin@codelutin.com
 * @since 1.4
 */
public class CreateAccidentalBatchUIModel extends AbstractTuttiBeanUIModel<CreateAccidentalBatchUIModel, CreateAccidentalBatchUIModel> {


    private static final long serialVersionUID = 1L;

    /**
     * Delegate edit object.
     *
     * @since 1.3
     */
    protected final AccidentalBatch editObject =
            AccidentalBatchs.newAccidentalBatch();

    public CreateAccidentalBatchUIModel() {
        super(null, null);
    }

    public Species getSpecies() {
        return editObject.getSpecies();
    }

    public void setSpecies(Species accidentalSpecies) {
        Object oldValue = getSpecies();
        editObject.setSpecies(accidentalSpecies);
        firePropertyChange(AccidentalBatch.PROPERTY_SPECIES, oldValue, accidentalSpecies);
    }

    public CaracteristicQualitativeValue getGender() {
        return editObject.getGender();
    }

    public void setGender(CaracteristicQualitativeValue accidentalGender) {
        Object oldValue = getGender();
        editObject.setGender(accidentalGender);
        firePropertyChange(AccidentalBatch.PROPERTY_GENDER, oldValue, accidentalGender);
    }

    public Float getWeight() {
        return editObject.getWeight();
    }

    public void setWeight(Float individualObservationWeight) {
        Object oldValue = getWeight();
        editObject.setWeight(individualObservationWeight);
        firePropertyChange(AccidentalBatch.PROPERTY_WEIGHT, oldValue, individualObservationWeight);
    }

    public Float getSize() {
        return editObject.getSize();
    }

    public void setSize(Float individualObservationSize) {
        Object oldValue = getSize();
        editObject.setSize(individualObservationSize);
        firePropertyChange(AccidentalBatch.PROPERTY_SIZE, oldValue, individualObservationSize);
    }

    public Caracteristic getLengthStepCaracteristic() {
        return editObject.getLengthStepCaracteristic();
    }

    public void setLengthStepCaracteristic(Caracteristic individualObservationLengthStepCaracteristic) {
        Object oldValue = getLengthStepCaracteristic();
        editObject.setLengthStepCaracteristic(individualObservationLengthStepCaracteristic);
        firePropertyChange(AccidentalBatch.PROPERTY_LENGTH_STEP_CARACTERISTIC, oldValue, individualObservationLengthStepCaracteristic);
    }

    public CaracteristicQualitativeValue getDeadOrAlive() {
        return editObject.getDeadOrAlive();
    }

    public void setDeadOrAlive(CaracteristicQualitativeValue accidentalDeadOrAlive) {
        Object oldValue = getDeadOrAlive();
        editObject.setDeadOrAlive(accidentalDeadOrAlive);
        firePropertyChange(AccidentalBatch.PROPERTY_DEAD_OR_ALIVE, oldValue, accidentalDeadOrAlive);
    }

    @Override
    protected CreateAccidentalBatchUIModel newEntity() {
        return new CreateAccidentalBatchUIModel();
    }

    public void reset() {
        setSpecies(null);
        setGender(null);
        setWeight(null);
        setSize(null);
        setLengthStepCaracteristic(null);
        setDeadOrAlive(null);
    }
}

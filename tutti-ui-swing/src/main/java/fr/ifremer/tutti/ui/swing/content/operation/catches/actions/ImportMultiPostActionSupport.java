package fr.ifremer.tutti.ui.swing.content.operation.catches.actions;

/*
 * #%L
 * Tutti :: UI
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.ProgressionModel;
import fr.ifremer.tutti.persistence.entities.data.FishingOperation;
import fr.ifremer.tutti.service.catches.multipost.MultiPostImportService;
import fr.ifremer.tutti.type.WeightUnit;
import fr.ifremer.tutti.ui.swing.util.actions.LongActionSupport;
import fr.ifremer.tutti.ui.swing.content.operation.FishingOperationsUI;
import fr.ifremer.tutti.ui.swing.content.operation.catches.EditCatchesUI;
import fr.ifremer.tutti.ui.swing.content.operation.catches.EditCatchesUIModel;
import fr.ifremer.tutti.ui.swing.content.operation.catches.MultiPostImportLogDialog;
import fr.ifremer.tutti.ui.swing.content.operation.fishing.actions.EditFishingOperationAction;
import fr.ifremer.tutti.ui.swing.util.AbstractTuttiUIHandler;
import fr.ifremer.tutti.ui.swing.util.TuttiUI;
import org.apache.commons.collections4.MapUtils;
import org.jdesktop.beans.AbstractBean;
import org.nuiton.jaxx.application.swing.AbstractApplicationUIHandler;

import javax.swing.JOptionPane;
import javax.swing.JTextArea;
import java.awt.Dialog;
import java.io.File;
import java.util.Map;

import static org.nuiton.i18n.I18n.t;

/**
 * @author Kevin Morin - kmorin@codelutin.com
 * @since 2.2
 */
public abstract class ImportMultiPostActionSupport<M extends AbstractBean, UI extends TuttiUI<M, ?>, H extends AbstractTuttiUIHandler<M, UI>>
        extends LongActionSupport<M, UI, H> {

    /**
     * Save the modified catch before import ?
     */
    private Boolean doSaveCatches;

    /**
     * Reset modified catches before import ?
     */
    private Boolean doResetCatches;

    /**
     * File to import.
     */
    private File file;

    /**
     * Optional data that were not imported.
     */
    private Map<String, Object> notImportedData;

    protected abstract String getFileExtension();

    protected abstract String getFileExtensionDescription();

    protected abstract String getFileChooserTitle();

    protected abstract String getFileChooserButton();

    protected abstract String getSuccessMessage(File file);

    protected abstract Map<String, Object> importBatches(MultiPostImportService multiPostImportExportService, File file, FishingOperation operation);

    public ImportMultiPostActionSupport(H handler) {
        super(handler, false);
    }

    @Override
    public boolean prepareAction() throws Exception {

        boolean doAction = super.prepareAction();

        if (doAction) {

            doResetCatches = doSaveCatches = false;

            EditCatchesUI catchesUI = getEditCatchUI();
            EditCatchesUIModel model = catchesUI.getModel();

            boolean canSave = model.isModify() && model.isValid();

            if (canSave) {

                // ask user to save before export (otherwise some data can't be export)

                String htmlMessage = String.format(AbstractApplicationUIHandler.CONFIRMATION_FORMAT,
                                                   t("tutti.askToSaveCatch.message"),
                                                   t("tutti.askToSaveCatchBeforeImport.help"));

                int saveResponse = JOptionPane.showOptionDialog(
                        getHandler().getTopestUI(),
                        htmlMessage,
                        t("tutti.askToSaveCatch.title"),
                        JOptionPane.OK_CANCEL_OPTION,
                        JOptionPane.QUESTION_MESSAGE,
                        null,
                        new String[]{t("tutti.option.saveCatch"), t("tutti.option.resetCatch"), t("tutti.option.cancelImport")},
                        t("tutti.option.saveCatch"));

                switch (saveResponse) {
                    case 0:

                        // should save
                        doSaveCatches = true;
                        break;
                    case 1:

                        // should reset
                        doResetCatches = true;
                        break;
                    case 2:

                        // cancel import
                        doAction = false;
                }

            }

            if (doAction) {

                // choose file to import
                file = chooseFile(getFileChooserTitle(),
                                  getFileChooserButton(),
                                  "^.*\\." + getFileExtension(),
                                  getFileExtensionDescription());

                doAction = file != null;

            }

        }

        return doAction;

    }

    @Override
    public final void releaseAction() {
        file = null;
        doSaveCatches = null;
        doResetCatches = null;
        notImportedData = null;
        super.releaseAction();
    }

    @Override
    public final void doAction() throws Exception {

        EditCatchesUI editCatchesUI = getEditCatchUI();
        EditCatchesUIModel editCatchesUIModel = editCatchesUI.getModel();

        FishingOperation operation = editCatchesUIModel.getFishingOperation();

        if (doSaveCatches) {

            setProgressionModel(new ProgressionModel());
            ProgressionModel progressionModel = getProgressionModel();
            progressionModel.setTotal(2);

            progressionModel.increments("Sauvegarde de la capture");

            // save catches before import

            getActionEngine().runInternalAction(editCatchesUI.getHandler(), SaveCatchBatchAction.class);


        } else {

            if (doResetCatches) {

                // reload fishing operation before importing data

                setProgressionModel(new ProgressionModel());
                ProgressionModel progressionModel = getProgressionModel();
                progressionModel.setTotal(2);

                progressionModel.increments("Réinitilisation de la capture");

                editCatchesUI.getHandler().getCatchBatchMonitor().clearModified();

                progressionModel.increments("Lancement de l'import");

            }

        }

        MultiPostImportService multiPostImportExportService = getContext().getMultiPostImportService();

        notImportedData = importBatches(multiPostImportExportService, file, operation);

        FishingOperationsUI parentUI = getUI().getParentContainer(FishingOperationsUI.class);
        EditFishingOperationAction editAction = getActionFactory().createLogicAction(parentUI.getHandler(),
                                                                                     EditFishingOperationAction.class);
        editAction.loadCatchBatch(operation);

    }

    @Override
    public void postSuccessAction() {

        super.postSuccessAction();

        if (MapUtils.isNotEmpty(notImportedData)) {

            String text = buildNotImportedDataReportText(notImportedData);

            if (!text.isEmpty()) {

                MultiPostImportLogDialog dialog = new MultiPostImportLogDialog((Dialog) getContext().getActionUI());

                JTextArea batchList = dialog.getBatchList();
                batchList.setText(text);
                dialog.setSize(400, 300);
                dialog.setLocationRelativeTo(getContext().getMainUI());
                dialog.setVisible(true);

            }


        }

        sendMessage(getSuccessMessage(file));

    }

    protected String buildNotImportedDataReportText(Map<String, Object> notImportedData) {
        throw new IllegalStateException("Override this to use it");
    }

    protected EditCatchesUI getEditCatchUI() {
        return getUI().getParentContainer(EditCatchesUI.class);
    }

    protected void addNotImportedWeightToReport(StringBuilder builder, Float weight, WeightUnit weightUnit, String label) {
        if (weight != null) {
            builder.append("- ").append(t(label, weightUnit.fromEntity(weight), weightUnit.getShortLabel())).append("\n");
        }
    }
}

package fr.ifremer.tutti.ui.swing.content.category;

/*
 * #%L
 * Tutti :: UI
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import fr.ifremer.adagio.core.dao.referential.pmfm.PmfmId;
import fr.ifremer.tutti.persistence.entities.data.SampleCategoryModel;
import fr.ifremer.tutti.persistence.entities.data.SampleCategoryModelEntry;
import fr.ifremer.tutti.persistence.entities.referential.Caracteristic;
import fr.ifremer.tutti.ui.swing.util.table.AbstractTuttiTableUIModel;
import org.nuiton.util.beans.Binder;
import org.nuiton.util.beans.BinderFactory;

import java.util.List;

/**
 * TODO
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.4
 */
public class EditSampleCategoryModelUIModel extends AbstractTuttiTableUIModel<SampleCategoryModel, EditSampleCategoryModelRowModel, EditSampleCategoryModelUIModel> {

    private static final long serialVersionUID = 1L;

    public static final String PROPERTY_CARACTERISTIC_LIST = "caracteristicList";

    public static final String PROPERTY_REMOVE_ENTRY_ENABLED = "removeEntryEnabled";

    public static final String PROPERTY_MOVE_UP_ENTRY_ENABLED = "moveUpEntryEnabled";

    public static final String PROPERTY_MOVE_DOWN_ENTRY_ENABLED = "moveDownEntryEnabled";

    protected final SampleCategoryModelEntry firstCategory;

    protected final List<Caracteristic> caracteristicList;

    protected static Binder<SampleCategoryModel, EditSampleCategoryModelUIModel> fromBeanBinder =
            BinderFactory.newBinder(SampleCategoryModel.class,
                                    EditSampleCategoryModelUIModel.class);

    protected static Binder<EditSampleCategoryModelUIModel, SampleCategoryModel> toBeanBinder =
            BinderFactory.newBinder(EditSampleCategoryModelUIModel.class, SampleCategoryModel.class);

    private boolean removeEntryEnabled;

    private boolean moveUpEntryEnabled;

    private boolean moveDownEntryEnabled;

    public EditSampleCategoryModelUIModel(List<SampleCategoryModelEntry> category, List<Caracteristic> caracteristicList) {

        super(SampleCategoryModel.class, fromBeanBinder, toBeanBinder);

        Preconditions.checkNotNull(caracteristicList, "need a not null caracteristicList");
        Preconditions.checkNotNull(category, "need a not null category list");
        Preconditions.checkArgument(!category.isEmpty(), "need at least one first category (sorted-unsorted)");
        this.caracteristicList = caracteristicList;
        this.firstCategory = category.get(0);
        Preconditions.checkArgument(PmfmId.SORTED_UNSORTED.getValue().equals(firstCategory.getCategoryId()), "first category must be sorted-unsorted caracteristic but was: " + firstCategory.getCategoryId());

        List<EditSampleCategoryModelRowModel> rows = Lists.newArrayList();

        for (SampleCategoryModelEntry sampleCategoryModelEntry : category) {
            caracteristicList.remove(sampleCategoryModelEntry.getCaracteristic());
            if (firstCategory.equals(sampleCategoryModelEntry)) {
                continue;
            }
            EditSampleCategoryModelRowModel row = new EditSampleCategoryModelRowModel();
            row.fromEntity(sampleCategoryModelEntry);
            row.setValid(true);
            rows.add(row);
        }
        setRows(rows);
    }

    @Override
    protected SampleCategoryModel newEntity() {
        List<SampleCategoryModelEntry> entries = Lists.newArrayList(firstCategory);
        int order = 1;
        for (EditSampleCategoryModelRowModel row : getRows()) {

            SampleCategoryModelEntry entry = row.toEntity();
            entry.setOrder(order++);
            entries.add(entry);

        }
        return new SampleCategoryModel(entries);
    }

    public List<Caracteristic> getCaracteristicList() {
        return caracteristicList;
    }

    public boolean isRemoveEntryEnabled() {
        return removeEntryEnabled;
    }

    public void setRemoveEntryEnabled(boolean removeEntryEnabled) {
        boolean oldValue = isRemoveEntryEnabled();
        this.removeEntryEnabled = removeEntryEnabled;
        firePropertyChange(PROPERTY_REMOVE_ENTRY_ENABLED, oldValue, removeEntryEnabled);
    }

    public boolean isMoveUpEntryEnabled() {
        return moveUpEntryEnabled;
    }

    public void setMoveUpEntryEnabled(boolean moveUpEntryEnabled) {
        boolean oldValue = isMoveUpEntryEnabled();
        this.moveUpEntryEnabled = moveUpEntryEnabled;
        firePropertyChange(PROPERTY_MOVE_UP_ENTRY_ENABLED, oldValue, moveUpEntryEnabled);
    }

    public boolean isMoveDownEntryEnabled() {
        return moveDownEntryEnabled;
    }

    public void setMoveDownEntryEnabled(boolean moveDownEntryEnabled) {
        boolean oldValue = isMoveDownEntryEnabled();
        this.moveDownEntryEnabled = moveDownEntryEnabled;
        firePropertyChange(PROPERTY_MOVE_DOWN_ENTRY_ENABLED, oldValue, moveDownEntryEnabled);
    }

    public void addCaracteristic(Caracteristic caracteristic) {
        caracteristicList.add(caracteristic);
        firePropertyChange(PROPERTY_CARACTERISTIC_LIST, null, caracteristicList);
    }

    public void removeCaracteristic(Caracteristic caracteristic) {
        caracteristicList.remove(caracteristic);
        firePropertyChange(PROPERTY_CARACTERISTIC_LIST, null, caracteristicList);
    }
}

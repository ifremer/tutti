package fr.ifremer.tutti.ui.swing.content.category;

/*
 * #%L
 * Tutti :: UI
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.entities.data.SampleCategoryModelEntry;
import fr.ifremer.tutti.persistence.entities.referential.Caracteristic;
import fr.ifremer.tutti.ui.swing.util.AbstractTuttiBeanUIModel;
import org.nuiton.util.beans.Binder;
import org.nuiton.util.beans.BinderFactory;

/**
 * @author Tony Chemit - chemit@codelutin.com
 * @since TODO
 */
public class EditSampleCategoryModelRowModel extends AbstractTuttiBeanUIModel<SampleCategoryModelEntry, EditSampleCategoryModelRowModel> {

    private static final long serialVersionUID = 1L;

    public static final String PROPERTY_CODE = "code";

    public static final String PROPERTY_LABEL = "label";

    public static final String PROPERTY_CARACTERISTIC = "caracteristic";

    protected final SampleCategoryModelEntry editObject = new SampleCategoryModelEntry();

    protected static final Binder<SampleCategoryModelEntry, EditSampleCategoryModelRowModel> fromBeanBinder =
            BinderFactory.newBinder(SampleCategoryModelEntry.class,
                                    EditSampleCategoryModelRowModel.class);

    protected static final Binder<EditSampleCategoryModelRowModel, SampleCategoryModelEntry> toBeanBinder =
            BinderFactory.newBinder(EditSampleCategoryModelRowModel.class,
                                    SampleCategoryModelEntry.class);

    public EditSampleCategoryModelRowModel() {
        super(fromBeanBinder, toBeanBinder);
    }

    public EditSampleCategoryModelRowModel(SampleCategoryModelEntry entry) {
        this();
        fromEntity(entry);
    }

    //------------------------------------------------------------------------//
    //-- AbstractTuttiBeanUIModel                                           --//
    //------------------------------------------------------------------------//

    @Override
    protected SampleCategoryModelEntry newEntity() {
        return new SampleCategoryModelEntry();
    }

    //------------------------------------------------------------------------//
    //-- SampleCategoryModelEntry delegate                                  --//
    //------------------------------------------------------------------------//

    public String getCode() {
        return editObject.getCode();
    }

    public void setCode(String code) {
        String oldValue = getCode();
        editObject.setCode(code);
        firePropertyChange(PROPERTY_CODE, oldValue, code);
    }

    public String getLabel() {
        return editObject.getLabel();
    }

    public void setLabel(String label) {
        String oldValue = getLabel();
        editObject.setLabel(label);
        firePropertyChange(PROPERTY_LABEL, oldValue, label);
    }

    public Integer getCategoryId() {
        return editObject.getCategoryId();
    }

    public Caracteristic getCaracteristic() {
        return editObject.getCaracteristic();
    }

    public void setCaracteristic(Caracteristic caracteristic) {
        Caracteristic oldValue = getCaracteristic();
        editObject.setCaracteristic(caracteristic);
        editObject.setCategoryId(caracteristic.getIdAsInt());
        firePropertyChange(PROPERTY_CARACTERISTIC, oldValue, caracteristic);
    }
}

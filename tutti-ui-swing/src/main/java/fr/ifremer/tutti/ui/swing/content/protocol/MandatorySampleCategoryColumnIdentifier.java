package fr.ifremer.tutti.ui.swing.content.protocol;

/*
 * #%L
 * Tutti :: UI
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.nuiton.jaxx.application.swing.table.ColumnIdentifier;

/**
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.2
 */
public class MandatorySampleCategoryColumnIdentifier extends ColumnIdentifier<EditProtocolSpeciesRowModel> {

    private static final long serialVersionUID = 1L;

    public static MandatorySampleCategoryColumnIdentifier newId(String propertyName,
                                                                Integer sampleCategoryId,
                                                                String headerI18nKey,
                                                                String headerTipI18nKey) {
        return new MandatorySampleCategoryColumnIdentifier(propertyName,
                                                           sampleCategoryId,
                                                           headerI18nKey,
                                                           headerTipI18nKey);
    }

    private final Integer sampleCategoryId;

    MandatorySampleCategoryColumnIdentifier(String propertyName,
                                            Integer sampleCategoryId,
                                            String headerI18nKey,
                                            String headerTipI18nKey) {
        super(propertyName, headerI18nKey, headerTipI18nKey);
        this.sampleCategoryId = sampleCategoryId;
    }

    @Override
    public Object getValue(EditProtocolSpeciesRowModel entry) {
        return entry.containsMandatorySampleCategoryId(sampleCategoryId);
    }

    @Override
    public void setValue(EditProtocolSpeciesRowModel entry, Object value) {
        Boolean toAdd = value == null ? false : Boolean.valueOf(value.toString());
        if (toAdd) {
            if (!entry.containsMandatorySampleCategoryId(sampleCategoryId)) {
                entry.addMandatorySampleCategoryId(sampleCategoryId);
            }
        } else {
            entry.removeMandatorySampleCategoryId(sampleCategoryId);
        }
    }

    public Integer getSampleCategoryId() {
        return sampleCategoryId;
    }

}

package fr.ifremer.tutti.ui.swing.content.operation.catches.individualobservation;

/*
 * #%L
 * Tutti :: UI
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.service.sampling.SamplingCodePrefix;
import fr.ifremer.tutti.ui.swing.util.AbstractTuttiUIHandler;
import jaxx.runtime.SwingUtil;
import jaxx.runtime.validator.swing.SwingValidator;

import javax.swing.JComponent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

/**
 * @author Kevin Morin - kmorin@codelutin.com
 * @since 4.5
 */
public class SampleCodeEditionPopupUIHandler extends AbstractTuttiUIHandler<SampleCodeEditionPopupUIModel, SampleCodeEditionPopupUI> {

    @Override
    public void afterInit(SampleCodeEditionPopupUI ui) {
        initUI(ui);

        ui.getSampleCodeField().getTextField().addKeyListener(new KeyAdapter() {

            @Override
            public void keyReleased(KeyEvent e) {
                super.keyReleased(e);
                if (e.getKeyCode() == KeyEvent.VK_ENTER) {
                    validate();
                }
            }
        });
    }

    @Override
    public void onCloseUI() {
        getUI().dispose();
    }

    @Override
    public SwingValidator<SampleCodeEditionPopupUIModel> getValidator() {
        return ui.getValidator();
    }

    @Override
    protected JComponent getComponentToFocus() {
        return getUI().getSampleCodeField();
    }

    public void open(SamplingCodePrefix prefix, Integer sampleCode) {
        getModel().setSampleCodePrefix(prefix);
        getModel().setSampleCode(sampleCode);
        getModel().setValid(false);
        getUI().pack();
        SwingUtil.center(getContext().getMainUI(), ui);
        getUI().setVisible(true);
    }

    public void validate() {
        if (getValidator().isValid()) {
            getModel().setValid(true);
            onCloseUI();
        }
    }

    public void cancel() {
        getModel().setSampleCode(null);
        onCloseUI();
    }
}

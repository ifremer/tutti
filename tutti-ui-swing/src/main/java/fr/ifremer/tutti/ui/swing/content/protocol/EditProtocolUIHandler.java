package fr.ifremer.tutti.ui.swing.content.protocol;

/*
 * #%L
 * Tutti :: UI
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import com.google.common.base.Predicate;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Multimap;
import com.google.common.collect.Sets;
import fr.ifremer.tutti.persistence.entities.TuttiEntities;
import fr.ifremer.tutti.persistence.entities.data.FishingOperation;
import fr.ifremer.tutti.persistence.entities.data.FishingOperations;
import fr.ifremer.tutti.persistence.entities.data.SampleCategoryModel;
import fr.ifremer.tutti.persistence.entities.data.SampleCategoryModelEntry;
import fr.ifremer.tutti.persistence.entities.protocol.CalcifiedPiecesSamplingDefinition;
import fr.ifremer.tutti.persistence.entities.protocol.CaracteristicMappingRow;
import fr.ifremer.tutti.persistence.entities.protocol.CaracteristicType;
import fr.ifremer.tutti.persistence.entities.protocol.MaturityCaracteristic;
import fr.ifremer.tutti.persistence.entities.protocol.OperationFieldMappingRow;
import fr.ifremer.tutti.persistence.entities.protocol.SpeciesProtocol;
import fr.ifremer.tutti.persistence.entities.protocol.Strata;
import fr.ifremer.tutti.persistence.entities.protocol.SubStrata;
import fr.ifremer.tutti.persistence.entities.protocol.TuttiProtocol;
import fr.ifremer.tutti.persistence.entities.protocol.TuttiProtocols;
import fr.ifremer.tutti.persistence.entities.referential.Caracteristic;
import fr.ifremer.tutti.persistence.entities.referential.CaracteristicQualitativeValue;
import fr.ifremer.tutti.persistence.entities.referential.Species;
import fr.ifremer.tutti.persistence.entities.referential.Speciess;
import fr.ifremer.tutti.persistence.entities.referential.TuttiLocation;
import fr.ifremer.tutti.service.DecoratorService;
import fr.ifremer.tutti.service.TuttiDecorator;
import fr.ifremer.tutti.ui.swing.content.home.actions.CloneProtocolAction;
import fr.ifremer.tutti.ui.swing.content.home.actions.EditProtocolAction;
import fr.ifremer.tutti.ui.swing.content.home.actions.ImportProtocolAction;
import fr.ifremer.tutti.ui.swing.content.operation.catches.species.edit.SpeciesBatchRowHelper;
import fr.ifremer.tutti.ui.swing.content.protocol.calcifiedpiecessampling.CalcifiedPiecesSamplingEditorRowModel;
import fr.ifremer.tutti.ui.swing.content.protocol.calcifiedpiecessampling.CalcifiedPiecesSamplingEditorTableModel;
import fr.ifremer.tutti.ui.swing.content.protocol.calcifiedpiecessampling.CalcifiedPiecesSamplingEditorUI;
import fr.ifremer.tutti.ui.swing.content.protocol.rtp.RtpCellEditor;
import fr.ifremer.tutti.ui.swing.content.protocol.rtp.RtpCellRenderer;
import fr.ifremer.tutti.ui.swing.content.protocol.zones.tree.AvailableStratasTreeModel;
import fr.ifremer.tutti.ui.swing.content.protocol.zones.tree.ZonesTreeModel;
import fr.ifremer.tutti.ui.swing.content.protocol.zones.tree.node.StrataNode;
import fr.ifremer.tutti.ui.swing.util.AbstractTuttiUIHandler;
import fr.ifremer.tutti.ui.swing.util.TuttiUIUtil;
import jaxx.runtime.SwingUtil;
import jaxx.runtime.swing.JAXXWidgetUtil;
import jaxx.runtime.swing.editor.bean.BeanDoubleList;
import jaxx.runtime.swing.editor.bean.BeanDoubleListModel;
import jaxx.runtime.swing.editor.bean.BeanFilterableComboBox;
import jaxx.runtime.swing.editor.bean.BeanUIUtil;
import jaxx.runtime.validator.swing.SwingValidator;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jdesktop.swingx.JXTable;
import org.jdesktop.swingx.autocomplete.ComboBoxCellEditor;
import org.jdesktop.swingx.autocomplete.ObjectToStringConverter;
import org.jdesktop.swingx.decorator.HighlightPredicate;
import org.jdesktop.swingx.decorator.Highlighter;
import org.jdesktop.swingx.table.DefaultTableColumnModelExt;
import org.jdesktop.swingx.table.TableColumnExt;
import org.nuiton.decorator.Decorator;
import org.nuiton.jaxx.application.swing.AbstractApplicationUIHandler;
import org.nuiton.jaxx.application.swing.table.ColumnIdentifier;
import org.nuiton.jaxx.application.swing.util.CloseableUI;

import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JList;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTree;
import javax.swing.ListCellRenderer;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TreeModelEvent;
import javax.swing.event.TreeModelListener;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumnModel;
import java.awt.Color;
import java.awt.Component;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;

import static org.nuiton.i18n.I18n.n;
import static org.nuiton.i18n.I18n.t;


/**
 * @author Tony Chemit - chemit@codelutin.com
 * @since 0.3
 */
public class EditProtocolUIHandler extends AbstractTuttiUIHandler<EditProtocolUIModel, EditProtocolUI> implements CloseableUI {

    /** Logger. */
    private static final Log log = LogFactory.getLog(EditProtocolUIHandler.class);

    public static String getTitle(boolean exist) {

        String result;
        if (exist) {
            result = t("tutti.editProtocol.title.edit.protocol");
        } else {
            result = t("tutti.editProtocol.title.create.protocol");
        }
        return result;
    }

//    protected SelectSpeciesUI dialog;

    protected SampleCategoryModel sampleCategoryModel;

    protected List<BeanDoubleList<Caracteristic>> allDoubleLists;

    public JXTable getSpeciesTable() {
        return ui.getSpeciesTable();
    }

    public JXTable getBenthosTable() {
        return ui.getBenthosTable();
    }

    public JXTable getCaracteristicsMappingTable() {
        return ui.getCaracteristicsMappingTable();
    }

    public JXTable getOperationFieldsMappingTable() {
        return ui.getOperationFieldsMappingTable();
    }

    public EditProtocolSpeciesTableModel getSpeciesTableModel() {
        return (EditProtocolSpeciesTableModel) getSpeciesTable().getModel();
    }

    public EditProtocolSpeciesTableModel getBenthosTableModel() {
        return (EditProtocolSpeciesTableModel) getBenthosTable().getModel();
    }

    public boolean isSpeciesSelected(Object selectedItem) {
        return selectedItem != null && selectedItem instanceof Species;
    }

    public EditProtocolCaracteristicsTableModel getCaracteristicMappingTableModel() {
        return (EditProtocolCaracteristicsTableModel) getCaracteristicsMappingTable().getModel();
    }

    public EditProtocolOperationFieldsTableModel getEditProtocolOperationFieldsMappingTableModel() {
        return (EditProtocolOperationFieldsTableModel) getOperationFieldsMappingTable().getModel();
    }

    //------------------------------------------------------------------------//
    //-- AbstractTuttiUIHandler methods                                     --//
    //------------------------------------------------------------------------//

    @Override
    public SwingValidator<EditProtocolUIModel> getValidator() {
        return ui.getValidator();
    }

    private Multimap<TuttiLocation, TuttiLocation> programStratasAndSubstratas;

    @Override
    public void beforeInit(EditProtocolUI ui) {
        super.beforeInit(ui);

        this.sampleCategoryModel = getContext().getDataContext().getSampleCategoryModel();

        incrementsMessage("Chargement des réferentiels");

        getDataContext().resetValidationDataContext();

        EditProtocolUIModel model = new EditProtocolUIModel(sampleCategoryModel);

        // load cache data

        List<Species> allSpecies = new ArrayList<>(getDataContext().getSpecies());
        model.setAllSpecies(allSpecies);

        Multimap<String, Species> allSpeciesByTaxonId = Speciess.splitByReferenceTaxonId(allSpecies);
        model.setAllSpeciesByTaxonId(allSpeciesByTaxonId);

        List<Species> referentSpecies = getDataContext().getReferentSpecies();
        Map<String, Species> allReferentSpeciesByTaxonId = Speciess.splitReferenceSpeciesByReferenceTaxonId(referentSpecies);
        model.setAllReferentSpeciesByTaxonId(allReferentSpeciesByTaxonId);

        List<Caracteristic> caracteristics = new ArrayList<>(getDataContext().getCaracteristics());
        model.setCaracteristics(caracteristics);

        Map<String, Caracteristic> allCaracteristic = TuttiEntities.splitById(caracteristics);
        model.setAllCaracteristic(allCaracteristic);

        // can't load directly model from database here, since we want to
        // fill only the model with rows (transformed from speciesProtocol)
        // As we still don't have the table model at this point, wait the
        // afterUI method to fill model

        listModelIsModify(model);

        TuttiLocation programZone = getDataContext().getProgram().getZone();

        programStratasAndSubstratas = HashMultimap.create(getPersistenceService().getAllFishingOperationStratasAndSubstratas(programZone.getId()));

        ImmutableMap.Builder<String, String> locationLabelCacheBuilder = ImmutableMap.builder();

        programStratasAndSubstratas.keySet().forEach(strata -> {

            locationLabelCacheBuilder.put(strata.getId(), strata.getLabel());
            programStratasAndSubstratas.get(strata).stream().filter(Objects::nonNull)
                                       .forEach(subStrata -> locationLabelCacheBuilder.put(subStrata.getId(), subStrata.getLabel()));

        });

        ImmutableMap<String, String> locationLabelCache = locationLabelCacheBuilder.build();
        model.setLocationLabelCache(locationLabelCache);

        // Pour lancer la validation et le recalcul des caches de zones après l
        model.addPropertyChangeListener(EditProtocolUIModel.PROPERTY_MODIFYING_ZONES, evt -> {

            boolean newValue = (boolean) evt.getNewValue();
            if (!newValue) {

                if (log.isInfoEnabled()) {
                    log.info("Ask to release zone trees caches and revalidate protocol.");
                }

                EditProtocolUIModel source = (EditProtocolUIModel) evt.getSource();
                boolean stratasAvailable = source.getAvailableStratasTreeModel().isAvailableStratas();
                source.setAvailableStratas(stratasAvailable);

                // and revalidate model,
                getValidator().doValidate();
            }
        });

        this.ui.setContextValue(model);
    }

    @Override
    public void afterInit(EditProtocolUI ui) {

        initUI(this.ui);

        EditProtocolUIModel model = getModel();

        TuttiProtocol protocol;

        if (getContext().isProtocolFilled()) {

            // load existing protocol

            incrementsMessage("Chargement du protocole");

            protocol = getDataContext().getProtocol();

            if (EditProtocolAction.CLEAN_PROTOCOL_ENTRY.getContextValue(this.ui) != null) {

                // clean protocol
                protocol = EditProtocolAction.CLEAN_PROTOCOL_ENTRY.getContextValue(this.ui);

                EditProtocolAction.CLEAN_PROTOCOL_ENTRY.removeContextValue(this.ui);

                model.setCleaned(true);
                this.ui.getSaveWarning().setText(t("tutti.editProtocol.warn.clean"));
            }
            model.fromEntity(protocol);

        } else if ((protocol = ImportProtocolAction.IMPORT_PROTOCOL_ENTRY.getContextValue(this.ui)) != null) {

            incrementsMessage("Import du protocole");

            // import protocol

            ImportProtocolAction.IMPORT_PROTOCOL_ENTRY.removeContextValue(this.ui);

            model.fromEntity(protocol);
            model.setImported(true);
            this.ui.getSaveWarning().setText(t("tutti.editProtocol.warn.import"));

        } else if ((protocol = CloneProtocolAction.CLONE_PROTOCOL_ENTRY.getContextValue(this.ui)) != null) {

            incrementsMessage("Clone du protocole");

            // clone protocol

            CloneProtocolAction.CLONE_PROTOCOL_ENTRY.removeContextValue(this.ui);

            model.fromEntity(protocol);
            model.setCloned(true);
            this.ui.getSaveWarning().setText(t("tutti.editProtocol.warn.clone"));

        } else {

            incrementsMessage("Création d'un nouveau protocol");

            // create new protocol

            if (log.isDebugEnabled()) {
                log.debug("Will create a new protocol");
            }
        }

        if (model.getLengthClassesPmfmId() == null) {
            model.setLengthClassesPmfmId(new ArrayList<>());
        }

        model.setProgramId(getDataContext().getProgramId());

        SwingValidator validator = this.ui.getValidator();
        listenValidatorValid(validator, model);

        registerValidators(validator);

        Collection<Species> referents = model.getAllReferentSpeciesByTaxonId().values();

        initBeanFilterableComboBox(this.ui.getSpeciesComboBox(), new ArrayList<>(referents), null);
        initBeanFilterableComboBox(this.ui.getBenthosComboBox(), new ArrayList<>(referents), null);
        initBeanFilterableComboBox(ui.getCaracteristicMappingComboBox(), new ArrayList<>(model.getCaracteristics()), null);

        List<EditProtocolCaracteristicsRowModel> caracteristicMappingRows;
        List<EditProtocolOperationFieldsRowModel> operationFieldMappingRows;
        List<EditProtocolSpeciesRowModel> speciesRows;
        List<EditProtocolSpeciesRowModel> benthosRows;

        model.addPropertyChangeListener(EditProtocolUIModel.PROPERTY_IMPORT_COLUMNS, evt -> populateImportColumnTableEditors());

        incrementsMessage("Préparation des interfaces graphiques");

        List<CalcifiedPiecesSamplingEditorRowModel> cpsRows = new ArrayList<>();

        // build protocolSpecies and benthos rows
        if (protocol == null) {
            caracteristicMappingRows = new ArrayList<>();
            operationFieldMappingRows = new ArrayList<>();
            speciesRows = new ArrayList<>();
            benthosRows = new ArrayList<>();

        } else {

            caracteristicMappingRows = toProtocolCaracteristicRows(protocol.getCaracteristicMapping());
            operationFieldMappingRows = toProtocolOperationFieldsRows(protocol.getOperationFieldMapping());

            speciesRows = toSpeciesRows(protocol.getSpecies(), cpsRows);
            benthosRows = toSpeciesRows(protocol.getBenthos(), cpsRows);

            if (log.isDebugEnabled()) {
                log.debug("Will edit protocol with " +
                                  speciesRows.size() + " protocolSpecies and " +
                                  benthosRows.size() + " benthos declared.");
            }
        }

        // set to model (will propagate to tableModel)
        // set a copy of the list to refresh the cps table
        cpsRows.sort(CalcifiedPiecesSamplingEditorRowModel.COMPARATOR);
        getModel().setCpsRows(cpsRows);

        model.setSpeciesRow(speciesRows);
        model.setBenthosRow(benthosRows);
        model.setCaracteristicMappingRows(caracteristicMappingRows);
        model.setOperationFieldMappingRows(operationFieldMappingRows);

        this.ui.getSpeciesComboBox().reset();
        this.ui.getBenthosComboBox().reset();
        this.ui.getCaracteristicMappingComboBox().reset();

        {
            // create protocolSpecies table model

            JXTable table = getSpeciesTable();

            DefaultTableColumnModelExt columnModel = new DefaultTableColumnModelExt();

            TableColumnExt speciesColumn = addColumnToModel(columnModel,
                                                            null,
                                                            newTableCellRender(Species.class),
                                                            EditProtocolSpeciesTableModel.SPECIES_ID);
            speciesColumn.setSortable(true);
            DecoratorService.SpeciesDecorator speciesDecorator = new DecoratorService.SpeciesDecorator();
            speciesColumn.putClientProperty(SpeciesBatchRowHelper.SPECIES_DECORATOR, speciesDecorator);
            speciesColumn.setCellRenderer(newTableCellRender(speciesDecorator));

            TableColumnExt speciesSurveyCodeColumn = addColumnToModel(columnModel,
                                                                      null,
                                                                      null,
                                                                      EditProtocolSpeciesTableModel.SURVEY_CODE_ID);
            speciesSurveyCodeColumn.setSortable(true);

            addLengthClassesColumnToModel(columnModel, model.getLengthClassesPmfmId());
            addMaturityColumnToModel(columnModel, model.getMaturityPmfmId());
            addCpsPmfmColumnToModel(columnModel);

            addBooleanColumnToModel(columnModel, EditProtocolSpeciesTableModel.WEIGHT_ENABLED, table);

            for (SampleCategoryModelEntry sampleCategoryModelEntry : sampleCategoryModel.getCategory()) {

                if (sampleCategoryModelEntry.getOrder() == 0) {
                    // first category is not editable
                    continue;
                }

                MandatorySampleCategoryColumnIdentifier identifier = MandatorySampleCategoryColumnIdentifier.newId(
                        EditProtocolSpeciesRowModel.PROPERTY_MANDATORY_SAMPLE_CATEGORY_ID,
                        sampleCategoryModelEntry.getCategoryId(),
                        sampleCategoryModelEntry.getLabel(),
                        sampleCategoryModelEntry.getLabel()
                );

                addBooleanColumnToModel(columnModel, identifier, table);
            }

            addIndividualObservationColumnToModel(columnModel, table);

            addColumnToModel(columnModel,
                             RtpCellEditor.newEditor(ui),
                             new RtpCellRenderer(),
                             EditProtocolSpeciesTableModel.USE_RTP);

            initTable(table,
                      columnModel,
                      speciesColumn,
                      EditProtocolSpeciesTableModel.USE_RTP,
                      speciesRows,
                      e -> {
                          ListSelectionModel source = (ListSelectionModel) e.getSource();
                          getModel().setRemoveSpeciesEnabled(!source.isSelectionEmpty());
                      });
        }

        {
            // create benthos table model

            JXTable table = getBenthosTable();

            DefaultTableColumnModelExt columnModel = new DefaultTableColumnModelExt();

            TableColumnExt speciesColumn = addColumnToModel(columnModel,
                                                            null,
                                                            newTableCellRender(Species.class),
                                                            EditProtocolSpeciesTableModel.SPECIES_ID);
            speciesColumn.setSortable(true);
            DecoratorService.SpeciesDecorator speciesDecorator = new DecoratorService.SpeciesDecorator();
            speciesColumn.putClientProperty(SpeciesBatchRowHelper.SPECIES_DECORATOR, speciesDecorator);
            speciesColumn.setCellRenderer(newTableCellRender(speciesDecorator));

            TableColumnExt speciesSurveyCodeColumn = addColumnToModel(columnModel,
                                                                      null,
                                                                      null,
                                                                      EditProtocolSpeciesTableModel.SURVEY_CODE_ID);
            speciesSurveyCodeColumn.setSortable(true);

            addLengthClassesColumnToModel(columnModel, model.getLengthClassesPmfmId());
            addMaturityColumnToModel(columnModel, model.getMaturityPmfmId());
            addCpsPmfmColumnToModel(columnModel);

            addBooleanColumnToModel(columnModel, EditProtocolSpeciesTableModel.WEIGHT_ENABLED, table);

            for (SampleCategoryModelEntry sampleCategoryModelEntry : sampleCategoryModel.getCategory()) {

                if (sampleCategoryModelEntry.getOrder() == 0) {
                    // first category is not editable
                    continue;
                }

                MandatorySampleCategoryColumnIdentifier identifier = MandatorySampleCategoryColumnIdentifier.newId(
                        EditProtocolSpeciesRowModel.PROPERTY_MANDATORY_SAMPLE_CATEGORY_ID,
                        sampleCategoryModelEntry.getCategoryId(),
                        sampleCategoryModelEntry.getLabel(),
                        sampleCategoryModelEntry.getLabel()
                );

                addBooleanColumnToModel(columnModel, identifier, table);
            }

            addIndividualObservationColumnToModel(columnModel, table);

            addColumnToModel(columnModel,
                             RtpCellEditor.newEditor(ui),
                             new RtpCellRenderer(),
                             EditProtocolSpeciesTableModel.USE_RTP);

            initTable(table,
                      columnModel,
                      speciesColumn,
                      EditProtocolSpeciesTableModel.USE_RTP,
                      benthosRows,
                      e -> {
                          ListSelectionModel source = (ListSelectionModel) e.getSource();
                          getModel().setRemoveBenthosEnabled(!source.isSelectionEmpty());
                      });
        }

        BeanDoubleList<Caracteristic> maturityList = this.ui.getMaturityList();
        model.getMaturityPmfmUsed().attachPredicateToUi(maturityList);

        BeanDoubleList<Caracteristic> lengthClassesList = this.ui.getLengthClassesList();
        model.getLengthStepPmfmUsed().attachPredicateToUi(lengthClassesList);

        allDoubleLists = new ArrayList<>();
        allDoubleLists.add(lengthClassesList);
        allDoubleLists.add(ui.getIndividualObservationList());
        allDoubleLists.add(maturityList);

        initDoubleList(EditProtocolUIModel.PROPERTY_LENGTH_CLASSES_PMFM_ID,
                       lengthClassesList,
                       Lists.newArrayList(model.getCaracteristics()),
                       model.getLengthClassesPmfmId());

        JList<Caracteristic> lengthClassesSelectedList = lengthClassesList.getSelectedList();
        ListCellRenderer<? super Caracteristic> lengthClassesDefaultRenderer = lengthClassesSelectedList.getCellRenderer();
        lengthClassesSelectedList.setCellRenderer(new LengthStepCaracteristicCellRenderer(getConfig().getColorWarningRow(), model.getLengthStepPmfmUsed(), lengthClassesDefaultRenderer));

        initDoubleList(EditProtocolUIModel.PROPERTY_INDIVIDUAL_OBSERVATION_PMFM_ID,
                       this.ui.getIndividualObservationList(),
                       Lists.newArrayList(model.getCaracteristics()),
                       model.getIndividualObservationPmfmId());

        initDoubleList(EditProtocolUIModel.PROPERTY_MATURITY_PMFM_ID,
                       maturityList,
                       model.getCaracteristics().stream().filter(caracteristic -> !caracteristic.isQualitativeValueEmpty()).collect(Collectors.toList()),
                       model.getMaturityPmfmId());

        JList<Caracteristic> maturitySelectedList = maturityList.getSelectedList();
        JMenuItem editMaturity = ui.getEditMaturityCaracteristicAction();
        maturityList.getSelectedListPopup().add(editMaturity);

        // add listener to enable the maturity edition
        maturitySelectedList.addListSelectionListener(e -> {
            boolean editMaturityEnabled = maturitySelectedList.getSelectedIndices().length == 1;
            editMaturity.setEnabled(editMaturityEnabled);
        });

        ListCellRenderer<? super Caracteristic> maturityDefaultRenderer = maturitySelectedList.getCellRenderer();
        maturitySelectedList.setCellRenderer(new MaturityCaracteristicCellRenderer(model.getMaturityPmfmUsed(), maturityDefaultRenderer));


        // update lengthstep and maturity list button states when the user goes back to the caracteristic panel
        // to update the remove state in case the selected maturity has been added or removed from the species or benthos
        ui.getTabPanel().addChangeListener(e -> {
            JTabbedPane source = (JTabbedPane) e.getSource();
            if (ui.getCaracteristicPanel().equals(source.getSelectedComponent())) {
                lengthClassesList.getHandler().recomputeButtonStates();
                maturityList.getHandler().recomputeButtonStates();
            }
        });

        // init caracteristics mappingtable
        {
            JXTable caracteristicsMappingTable = getCaracteristicsMappingTable();

            DefaultTableColumnModelExt columnModel = new DefaultTableColumnModelExt();

            addColumnToModel(columnModel,
                             null,
                             newTableCellRender(Caracteristic.class),
                             EditProtocolCaracteristicsTableModel.PSFM_ID);

            addComboDataColumnToModel(columnModel,
                                      EditProtocolCaracteristicsTableModel.TYPE,
                                      getDecorator(CaracteristicType.class, null),
                                      Lists.newArrayList(CaracteristicType.getTabTypes()));

            addColumnToModel(columnModel,
                             EditProtocolCaracteristicsTableModel.IMPORT_FILE_COLUMN);

            List<Caracteristic> caracteristics = new ArrayList<>(getModel().getCaracteristics());
            EditProtocolCaracteristicsTableModel tableModel = new EditProtocolCaracteristicsTableModel(columnModel, caracteristics);
            caracteristicsMappingTable.setModel(tableModel);
            caracteristicsMappingTable.setColumnModel(columnModel);

            // by default do not authorize to change column orders
            caracteristicsMappingTable.getTableHeader().setReorderingAllowed(false);

            addHighlighters(caracteristicsMappingTable);

            caracteristicsMappingTable.getSelectionModel().addListSelectionListener(e -> {

                int rowIndex = getCaracteristicsMappingTable().getSelectedRow();

                boolean enableRemoveCaracteristicMapping = false;
                boolean enableMoveUpCaracteristicMapping = false;
                boolean enableMoveDownCaracteristicMapping = false;

                if (rowIndex != -1) {

                    // there is a selected row
                    enableRemoveCaracteristicMapping = true;

                    enableMoveUpCaracteristicMapping = rowIndex > 0;

                    enableMoveDownCaracteristicMapping = rowIndex < getCaracteristicsMappingTable().getModel().getRowCount() - 1;
                }
                EditProtocolUIModel model1 = getModel();
                model1.setRemoveCaracteristicMappingEnabled(enableRemoveCaracteristicMapping);
                model1.setMoveUpCaracteristicMappingEnabled(enableMoveUpCaracteristicMapping);
                model1.setMoveDownCaracteristicMappingEnabled(enableMoveDownCaracteristicMapping);

            });

            // always scroll to selected row
            SwingUtil.scrollToTableSelection(caracteristicsMappingTable);

            tableModel.setRows(caracteristicMappingRows);
        }

        // init operation fields table
        {
            JXTable operationFieldsTable = getUI().getOperationFieldsMappingTable();

            DefaultTableColumnModelExt columnModel = new DefaultTableColumnModelExt();

            addColumnToModel(columnModel,
                             null,
                             new DefaultTableCellRenderer() {

                                 @Override
                                 public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
                                     setText(t("tutti.editFishingOperation.field." + value.toString()));
                                     return this;
                                 }
                             },
                             EditProtocolOperationFieldsTableModel.FIELD);

            addColumnToModel(columnModel,
                             EditProtocolOperationFieldsTableModel.IMPORT_FILE_COLUMN);

            EditProtocolOperationFieldsTableModel tableModel = new EditProtocolOperationFieldsTableModel(columnModel);
            operationFieldsTable.setModel(tableModel);
            operationFieldsTable.setColumnModel(columnModel);

            // by default do not authorize to change column orders
            operationFieldsTable.getTableHeader().setReorderingAllowed(false);

            addHighlighters(operationFieldsTable);

            tableModel.setRows(operationFieldMappingRows);

            // always scroll to selected row
            SwingUtil.scrollToTableSelection(operationFieldsTable);
        }

        populateImportColumnTableEditors();

        initZonesTree();

        initAvailableStratasTree();


        // if new protocol can already cancel his creation
        model.setModify(model.isCreate() || model.isCleaned());

        this.ui.getCaracteristicPane().addChangeListener(e -> {
            JTabbedPane tabPanel = (JTabbedPane) e.getSource();
            int selectedIndex = tabPanel.getSelectedIndex();
            BeanDoubleList<Caracteristic> selectedDoubleList;
            log.debug("selected " + selectedIndex);
            switch (selectedIndex) {
                case 0:
                    selectedDoubleList = EditProtocolUIHandler.this.ui.getLengthClassesList();
                    break;

                case 1:
                    selectedDoubleList = EditProtocolUIHandler.this.ui.getIndividualObservationList();
                    break;

                case 2:
                    selectedDoubleList = EditProtocolUIHandler.this.ui.getMaturityList();
                    break;

                default:
                    selectedDoubleList = null;
            }
            if (selectedDoubleList != null) {
                selectedDoubleList.getHandler().refreshFilteredElements();

            } else {
                // if the selected tab is the operation caracteristic mapping tab, then update the combobox
                List<Caracteristic> selectedCaracteristics = new ArrayList<>(getModel().getCaracteristics());
                for (BeanDoubleList<Caracteristic> doubleList : allDoubleLists) {
                    selectedCaracteristics.removeAll(doubleList.getModel().getSelected());
                }
                selectedCaracteristics.removeAll(getModel().getUsedCaracteristics());
                getUI().getCaracteristicMappingComboBox().setData(selectedCaracteristics);
            }
        });

        this.ui.getTabPanel().addChangeListener(e -> {
            JTabbedPane tabPanel = (JTabbedPane) e.getSource();
            int selectedIndex = tabPanel.getSelectedIndex();
            if (log.isDebugEnabled()) {
                log.debug("New tab selected: " + selectedIndex);
            }
            if (selectedIndex == 4) {

                if (log.isDebugEnabled()) {
                    log.debug("Update species list on calcifiedPiecesSamplingEditorUI model.");
                }

                // on met à jour la liste des espèces disponibles dans l'onglet des pièces calcifiées
                CalcifiedPiecesSamplingEditorUI calcifiedPiecesSamplingEditorUI = getUI().getCalcifiedPiecesSamplingEditorUI();

                List<Species> protocolSpecies = new ArrayList<>();

                List<EditProtocolSpeciesRowModel> speciesRows1 = getModel().getSpeciesRow();
                if (speciesRows1 != null) {

                    protocolSpecies.addAll(speciesRows1.stream()
                                                       .filter(EditProtocolSpeciesRowModel::withLengthStepPmfm)
                                                       .map(EditProtocolSpeciesRowModel::toSpeciesWithSurveyCode)
                                                       .collect(Collectors.toList()));

                }

                List<EditProtocolSpeciesRowModel> benthosRows1 = getModel().getBenthosRow();
                if (benthosRows1 != null) {

                    protocolSpecies.addAll(benthosRows1.stream()
                                                       .filter(EditProtocolSpeciesRowModel::withLengthStepPmfm)
                                                       .map(EditProtocolSpeciesRowModel::toSpeciesWithSurveyCode)
                                                       .collect(Collectors.toList()));

                }

                protocolSpecies.removeAll(getModel().getCpsRows()
                                                    .stream()
                                                    .map(CalcifiedPiecesSamplingEditorRowModel::getProtocolSpecies)
                                                    .map(EditProtocolSpeciesRowModel::getSpecies)
                                                    .collect(Collectors.toList()));

                // Si on ne vide pas la liste et que les espèces sont égales (sur l'id), rien n'est changé
                calcifiedPiecesSamplingEditorUI.getSpeciesComboBox().setData(null);
                calcifiedPiecesSamplingEditorUI.getSpeciesComboBox().setData(protocolSpecies);

            }
        });

        model.setVersion(TuttiProtocols.CURRENT_PROTOCOL_VERSION);

    }

    protected void addIndividualObservationColumnToModel(DefaultTableColumnModelExt columnModel, JXTable table) {
        TableCellRenderer renderer = new TableCellRenderer() {

            TableCellRenderer delegate = table.getDefaultRenderer(Boolean.class);

            @Override
            public Component getTableCellRendererComponent(JTable table,
                                                           Object value,
                                                           boolean isSelected,
                                                           boolean hasFocus,
                                                           int row, int column) {
                Component result = delegate.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
                result.setEnabled(table.isCellEditable(row, column));
                return result;
            }
        };
        addColumnToModel(columnModel,
                         table.getDefaultEditor(Boolean.class),
                         renderer,
                         EditProtocolSpeciesTableModel.INDIVIDUAL_OBSERVATION_ENABLED);
    }

    protected void initZonesTree() {

        EditProtocolUIModel model = getModel();

        ZonesTreeModel zonesTreeModel = model.getZonesTreeModel();

        zonesTreeModel.setCreatingModel(true);

        try {

            model.getIncomingZones().forEach(zonesTreeModel::addZone);

        } finally {
            zonesTreeModel.setCreatingModel(false);
        }

        // expand nodes when children are added
        zonesTreeModel.addTreeModelListener(new TreeModelListener() {

            @Override
            public void treeNodesChanged(TreeModelEvent e) {
                model.setModify(true);
            }

            @Override
            public void treeNodesInserted(TreeModelEvent e) {
                getUI().getZoneEditor().getZonesTree().expandPath(e.getTreePath());
            }

            @Override
            public void treeNodesRemoved(TreeModelEvent e) {
            }

            @Override
            public void treeStructureChanged(TreeModelEvent e) {
            }
        });

        initJTree(ui.getZoneEditor().getZonesTree());


    }

    protected void initAvailableStratasTree() {

        EditProtocolUIModel model = getModel();

        Set<String> usedStrataLocations = new LinkedHashSet<>();
        Set<String> usedSubStrataLocations = new LinkedHashSet<>();

        // Récupération des identifiants utilisés dans les zones entrantes
        model.getIncomingZones().forEach(zone -> {

            Collection<Strata> zoneStratas = zone.getStrata();

            Set<String> strataLocations = zoneStratas.stream()
                                                     .map(Strata::getId)
                                                     .collect(Collectors.toSet());
            usedStrataLocations.addAll(strataLocations);

            Set<String> subStrataLocations = zoneStratas.stream()
                                                        .map(Strata::getSubstrata)
                                                        .flatMap(Collection::stream)
                                                        .map(SubStrata::getId)
                                                        .collect(Collectors.toSet());
            usedSubStrataLocations.addAll(subStrataLocations);

        });

        AvailableStratasTreeModel availableStratasTreeModel = model.getAvailableStratasTreeModel();

        availableStratasTreeModel.setCreatingModel(true);

        try {

            programStratasAndSubstratas.keySet().stream().forEach(strataLocation -> {

                String strataId = strataLocation.getId();

                Collection<TuttiLocation> subStrataLocations = new HashSet<>(programStratasAndSubstratas.get(strataLocation));
                subStrataLocations.remove(null);
                List<String> subStratas = subStrataLocations.stream()
                                                            .map(TuttiLocation::getId)
                                                            .filter(id -> !usedSubStrataLocations.contains(id))
                                                            .collect(Collectors.toList());

                if (!(subStratas.isEmpty() && usedStrataLocations.contains(strataLocation.getId()))) {

                    StrataNode strataNode = availableStratasTreeModel.getOrCreateStrataNode(strataId);
                    subStratas.forEach(subStrataId -> availableStratasTreeModel.addSubsStrata(strataNode, subStrataId));

                }

            });

        } finally {

            availableStratasTreeModel.setCreatingModel(false);

        }

        initJTree(ui.getZoneEditor().getAvailableStratasTree());

    }

    @Override
    protected JComponent getComponentToFocus() {
        return getUI().getNameField();
    }

    public List<EditProtocolSpeciesRowModel> toSpeciesRows(List<SpeciesProtocol> speciesProtocols) {
        return toSpeciesRows(speciesProtocols, getModel().getCpsRows());
    }

    public List<EditProtocolSpeciesRowModel> toSpeciesRows(List<SpeciesProtocol> speciesProtocols,
                                                           List<CalcifiedPiecesSamplingEditorRowModel> cpsRows) {
        BeanFilterableComboBox<Species> speciesComboBox = ui.getSpeciesComboBox();
        Preconditions.checkNotNull(speciesComboBox.getData());
        BeanFilterableComboBox<Species> benthosComboBox = ui.getBenthosComboBox();
        Preconditions.checkNotNull(benthosComboBox.getData());

        EditProtocolUIModel model = getModel();

        Map<String, Species> allReferentSpeciesByTaxonId = model.getAllReferentSpeciesByTaxonId();
        Map<String, Caracteristic> allCaracteristic = model.getAllCaracteristic();

        List<Caracteristic> lengthClassesPmfmId = Lists.newArrayList();

        Set<Species> speciesSet = Sets.newHashSet();
        List<EditProtocolSpeciesRowModel> result = Lists.newArrayList();
        if (CollectionUtils.isNotEmpty(speciesProtocols)) {
            for (SpeciesProtocol speciesProtocol : speciesProtocols) {
                Integer taxonId = speciesProtocol.getSpeciesReferenceTaxonId();

                if (taxonId == null) {

                    continue;
                }
                String taxonIdStr = String.valueOf(taxonId);

                // remove all synonyms from available synonym list
                Collection<Species> allSynonyms = model.getAllSynonyms(taxonIdStr);
                model.getAllSynonyms().removeAll(allSynonyms);

                // get species referent taxon
                Species species = allReferentSpeciesByTaxonId.get(taxonIdStr);

                // make sure it exists
                Preconditions.checkNotNull(species, "Espèce inconnue : " + taxonIdStr);
                speciesSet.add(species);

                EditProtocolSpeciesRowModel row = createNewProtocolSpeciesRow();
                row.setSpecies(species);

                String lengthStepPmfmId = speciesProtocol.getLengthStepPmfmId();
                Caracteristic lengthStepPmfm = allCaracteristic.get(lengthStepPmfmId);
                if (lengthStepPmfmId != null &&
                        !lengthClassesPmfmId.contains(lengthStepPmfm) &&
                        !model.containsLengthClassesPmfmId(lengthStepPmfmId)) {
                    if (log.isInfoEnabled()) {
                        log.info("Found a new lengthStep pmfm: " + lengthStepPmfmId);
                    }
                    lengthClassesPmfmId.add(lengthStepPmfm);
                }
                row.setLengthStepPmfm(lengthStepPmfm);

                String maturityPmfmId = speciesProtocol.getMaturityPmfmId();
                Caracteristic maturityPmfm = allCaracteristic.get(maturityPmfmId);
                row.setMaturityPmfm(maturityPmfm);

                row.fromEntity(speciesProtocol);

                // make sure to get a clean copy of the list
                row.setMandatorySampleCategoryId(Lists.newArrayList(speciesProtocol.getMandatorySampleCategoryId()));
                result.add(row);

                // add all the cps rows
                Collection<CalcifiedPiecesSamplingDefinition> cpsDefs = speciesProtocol.getCalcifiedPiecesSamplingDefinition();
                if (cpsDefs != null) {
                    cpsDefs.forEach(def -> {
                        CalcifiedPiecesSamplingEditorRowModel cpsRow = new CalcifiedPiecesSamplingEditorRowModel();
                        cpsRow.fromEntity(def);
                        cpsRow.setProtocolSpecies(row);
                        cpsRow.setValid(true);
                        cpsRows.add(cpsRow);
                    });
                }

            }

            // remove once for all in comboboxes
            speciesComboBox.removeItems(speciesSet);
            benthosComboBox.removeItems(speciesSet);
        }

        if (CollectionUtils.isNotEmpty(lengthClassesPmfmId)) {
            // detect some new length step pmfp to add in protocol

            // add it to model
            model.addAllLengthClassesPmfmId(TuttiEntities.collecIds(lengthClassesPmfmId));

            // add it to ui (no binding here, must do it manually)
            BeanDoubleList<Caracteristic> lengthClassesList = ui.getLengthClassesList();
            lengthClassesList.getModel().addToSelected(lengthClassesPmfmId);
        }
        return result;
    }

    public EditProtocolSpeciesRowModel createNewProtocolSpeciesRow() {

        EditProtocolSpeciesRowModel result = getModel().newRow();

        result.addPropertyChangeListener(EditProtocolSpeciesRowModel.PROPERTY_LENGTH_STEP_PMFM, evt -> {

            Caracteristic newValue = (Caracteristic) evt.getNewValue();
            if (newValue == null) {

                result.setIndividualObservationEnabled(false);

                if (getModel().getCpsRows() != null) {

                    List<CalcifiedPiecesSamplingEditorRowModel> speciesCpsRows =
                            getModel().getCpsRows()
                                      .stream()
                                      .filter(row -> row.getProtocolSpecies().equals(result))
                                      .collect(Collectors.toList());

                    if (!speciesCpsRows.isEmpty()) {
                        String htmlMessage = String.format(
                                AbstractApplicationUIHandler.CONFIRMATION_FORMAT,
                                t("tutti.editProtocol.action.removeLengthStepPmfm.removeCpsRows.message"),
                                t("jaxx.application.common.askBeforeDelete.help"));
                        int i = JOptionPane.showConfirmDialog(
                                getUI(),
                                htmlMessage,
                                t("tutti.editProtocol.action.removeLengthStepPmfm.removeCpsRows.title"),
                                JOptionPane.OK_CANCEL_OPTION,
                                JOptionPane.QUESTION_MESSAGE);

                        boolean deleteCpsRows = i == JOptionPane.OK_OPTION;

                        if (deleteCpsRows) {

                            deleteCpsRows(speciesCpsRows);

                        } else {

                            result.setLengthStepPmfm((Caracteristic) evt.getOldValue());

                        }
                    }
                }
            }
        });
        return result;
    }

    public void deleteCpsRows(Collection<CalcifiedPiecesSamplingEditorRowModel> cpsRowsToDelete) {

        CalcifiedPiecesSamplingEditorUI cpsEditor = getUI().getCalcifiedPiecesSamplingEditorUI();

        List<CalcifiedPiecesSamplingEditorRowModel> cpsRows = getModel().getCpsRows();

        TreeSet<Integer> indexesToDelete =
                new TreeSet<>(cpsRowsToDelete.stream().map(cpsRows::indexOf).collect(Collectors.toSet()));

        cpsRows.removeAll(cpsRowsToDelete);

        CalcifiedPiecesSamplingEditorTableModel cpsTableModel =
                (CalcifiedPiecesSamplingEditorTableModel) cpsEditor.getCpsTable().getModel();
        cpsTableModel.fireTableRowsDeleted(indexesToDelete.first(), indexesToDelete.last());
    }

    public List<EditProtocolCaracteristicsRowModel> toProtocolCaracteristicRows(List<CaracteristicMappingRow> caracteristicMappingRows) {
        BeanFilterableComboBox<Caracteristic> caracteristicMappingComboBox = ui.getCaracteristicMappingComboBox();
        Preconditions.checkNotNull(caracteristicMappingComboBox.getData());

        EditProtocolUIModel model = getModel();

        Set<Caracteristic> caracteristicSet = Sets.newHashSet();
        List<EditProtocolCaracteristicsRowModel> result = Lists.newArrayList();
        if (CollectionUtils.isNotEmpty(caracteristicMappingRows)) {
            for (CaracteristicMappingRow caracteristicMappingRow : caracteristicMappingRows) {
                String pmfmId = caracteristicMappingRow.getPmfmId();

                if (pmfmId == null) {

                    continue;
                }

                // make sure it exists
                caracteristicSet.add(model.getAllCaracteristic().get(pmfmId));

                EditProtocolCaracteristicsRowModel row = createEditProtocolCaracteristicsRowModel();
                row.fromEntity(caracteristicMappingRow);
                row.setValid(row.getType() != null);

                result.add(row);
            }

            caracteristicMappingComboBox.removeItems(caracteristicSet);
        }

        return result;
    }

    public List<EditProtocolOperationFieldsRowModel> toProtocolOperationFieldsRows(Collection<OperationFieldMappingRow> operationFieldMappingRows) {
        List<EditProtocolOperationFieldsRowModel> result = Lists.newArrayList();

        if (operationFieldMappingRows == null) {
            operationFieldMappingRows = new ArrayList<>();
        }
        Map<String, OperationFieldMappingRow> rowsByField = Maps.uniqueIndex(operationFieldMappingRows,
                                                                             OperationFieldMappingRow::getField);

        n("tutti.editFishingOperation.field.gearShootingStartDay");
        n("tutti.editFishingOperation.field.gearShootingStartTime");
        n("tutti.editFishingOperation.field.gearShootingEndDay");
        n("tutti.editFishingOperation.field.gearShootingEndTime");

        List<String> properties = Lists.newArrayList(
                FishingOperation.PROPERTY_STATION_NUMBER,
                FishingOperation.PROPERTY_FISHING_OPERATION_NUMBER,
                FishingOperations.PROPERTY_GEAR_SHOOTING_START_DAY,
                FishingOperations.PROPERTY_GEAR_SHOOTING_START_TIME,
                FishingOperation.PROPERTY_GEAR_SHOOTING_START_LATITUDE,
                FishingOperation.PROPERTY_GEAR_SHOOTING_START_LONGITUDE,
                FishingOperations.PROPERTY_GEAR_SHOOTING_END_DAY,
                FishingOperations.PROPERTY_GEAR_SHOOTING_END_TIME,
                FishingOperation.PROPERTY_GEAR_SHOOTING_END_LATITUDE,
                FishingOperation.PROPERTY_GEAR_SHOOTING_END_LONGITUDE,
                FishingOperation.PROPERTY_FISHING_OPERATION_RECTILIGNE,
                FishingOperation.PROPERTY_TRAWL_DISTANCE,
                FishingOperation.PROPERTY_FISHING_OPERATION_VALID,
                FishingOperation.PROPERTY_MULTIRIG_AGGREGATION,
                FishingOperation.PROPERTY_RECORDER_PERSON,
                FishingOperation.PROPERTY_GEAR,
                FishingOperation.PROPERTY_VESSEL,
                FishingOperation.PROPERTY_STRATA,
                FishingOperation.PROPERTY_SUB_STRATA,
                FishingOperation.PROPERTY_LOCATION,
                FishingOperation.PROPERTY_SECONDARY_VESSEL
        );
        //TODO voir avec Vincent comment on gère le cas où aucun des champs clé n'est renseigné

        for (String property : properties) {
            OperationFieldMappingRow row = rowsByField.get(property);
            EditProtocolOperationFieldsRowModel newRow = createOperationFieldRow(property, row != null ? row.getImportColumn() : null);
            result.add(newRow);
        }
        return result;
    }

    @Override
    public void onCloseUI() {
        if (log.isDebugEnabled()) {
            log.debug("closing: " + ui);
        }
        clearValidators();
        ui.getTabPanel().setSelectedIndex(0);

        closeUI(ui.getRtpEditorUI());
        closeUI(ui.getZoneEditor());

    }

    @Override
    public boolean quitUI() {
        return quitScreen(
                getModel().isValid(),
                getModel().isModify(),
                t("tutti.editProtocol.askCancelEditBeforeLeaving.cancelSaveProtocol"),
                t("tutti.editProtocol.askSaveBeforeLeaving.saveProtocol"),
                ui.getSaveButton().getAction()
        );
    }

    //------------------------------------------------------------------------//
    //-- Public methods                                                     --//
    //------------------------------------------------------------------------//

    public void addDoubleListListeners() {
        EditProtocolUIModel model = getModel();

        for (BeanDoubleList<Caracteristic> list : allDoubleLists) {
            String id = (String) list.getClientProperty("_updateListenerId");
            UpdateSelectedList updateListener = (UpdateSelectedList) list.getClientProperty("_updateListener");
            model.addPropertyChangeListener(id, updateListener);
        }
    }

    public void removeDoubleListListeners() {
        EditProtocolUIModel model = getModel();

        for (BeanDoubleList<Caracteristic> list : allDoubleLists) {
            String id = (String) list.getClientProperty("_updateListenerId");
            UpdateSelectedList updateListener = (UpdateSelectedList) list.getClientProperty("_updateListener");
            model.removePropertyChangeListener(id, updateListener);
        }
    }

//    //FIXME tchemit-2014-01-09 Bad place for this!
//    public Species openSelectOtherSpeciesDialog(String title, List<Species> species) {
//        SelectSpeciesUIModel model = dialog.getModel();
//        model.setSpecies(species);
//        model.setSelectedSpecies(null);
//
//        openDialog(dialog, title, new Dimension(400, 130));
//
//        return model.getSelectedSpecies();
//    }

    public EditProtocolCaracteristicsRowModel createEditProtocolCaracteristicsRowModel() {
        EditProtocolCaracteristicsRowModel newRow = new EditProtocolCaracteristicsRowModel(getModel().getCaracteristics());

        newRow.addPropertyChangeListener(evt -> {
            EditProtocolCaracteristicsRowModel row = (EditProtocolCaracteristicsRowModel) evt.getSource();
            EditProtocolUIModel model = getModel();

            if (EditProtocolCaracteristicsRowModel.PROPERTY_IMPORT_COLUMN.equals(evt.getPropertyName())) {
                String oldValue = (String) evt.getOldValue();
                if (oldValue != null) {
                    model.decNumberOfRows(oldValue);
                }
                String newValue = (String) evt.getNewValue();
                if (newValue != null) {
                    model.incNumberOfRows(newValue);
                }

                recomputeRowsValidState();

            } else {
                row.setValid(isCaracteristicsRowValid(row));
            }

            if (row.isValid()) {
                model.setModify(true);
            }
        });

        return newRow;
    }

    //------------------------------------------------------------------------//
    //-- Internal methods                                                   --//
    //------------------------------------------------------------------------//

    protected void initDoubleList(String propertyId,
                                  BeanDoubleList<Caracteristic> widget,
                                  List<Caracteristic> availableCaracteristics,
                                  List<String> selectedCaracteristics) {

        initBeanList(widget, availableCaracteristics, new ArrayList<>());

        UpdateSelectedList listener = new UpdateSelectedList(widget, getModel().getAllCaracteristic());
        widget.putClientProperty("_updateListener", listener);
        widget.putClientProperty("_updateListenerId", propertyId);
        listener.select(selectedCaracteristics);

        // add a filter to keep only in universe everything except what is selected by other double lists
        List<BeanDoubleList<Caracteristic>> list = new ArrayList<>(allDoubleLists);
        list.remove(widget);
        widget.getHandler().addFilter(new SelectValuePredicate(list));
        widget.getHandler().addFilter(caracteristic -> !getModel().isCaracteristicUsedInMapping(caracteristic));

        widget.getHandler().sortData();
    }

    protected void selectCaracteristics(List<String> ids, JComboBox comboBox) {

        Map<String, Caracteristic> allCaracteristic = getModel().getAllCaracteristic();
        List<Caracteristic> selection = new ArrayList<>();
        if (CollectionUtils.isNotEmpty(ids)) {
            for (String id : ids) {
                selection.add(allCaracteristic.get(id));
            }
        }

        List<Caracteristic> dataToList = new ArrayList<>(selection);

        // add a null value at first position
        if (!dataToList.isEmpty() && dataToList.get(0) != null) {
            dataToList.add(0, null);
        }
        SwingUtil.fillComboBox(comboBox, dataToList, null);
    }

    protected void addLengthClassesColumnToModel(TableColumnModel model, List<String> selectedIds) {

        Decorator<Caracteristic> decorator = getDecorator(Caracteristic.class, null);

        JComboBox<Caracteristic> comboBox = new JComboBox<>();

        getModel().addPropertyChangeListener(EditProtocolUIModel.PROPERTY_LENGTH_CLASSES_PMFM_ID, evt -> {
            List<String> ids = (List<String>) evt.getNewValue();
            selectCaracteristics(ids, comboBox);
        });
        comboBox.setRenderer(newListCellRender(decorator));

        selectCaracteristics(selectedIds, comboBox);
        ObjectToStringConverter converter = BeanUIUtil.newDecoratedObjectToStringConverter(decorator);
        BeanUIUtil.decorate(comboBox, converter);
        ComboBoxCellEditor editor = new ComboBoxCellEditor(comboBox);

        addColumnToModel(model,
                         editor,
                         newTableCellRender(decorator),
                         EditProtocolSpeciesTableModel.LENGTH_STEP_PMFM_ID);
    }

    protected void addMaturityColumnToModel(TableColumnModel model, List<String> selectedIds) {

        Decorator<Caracteristic> decorator = getDecorator(Caracteristic.class, null);

        JComboBox<Caracteristic> comboBox = new JComboBox();

        getModel().addPropertyChangeListener(EditProtocolUIModel.PROPERTY_MATURITY_PMFM_ID, evt -> {
            List<String> ids = (List<String>) evt.getNewValue();
            selectCaracteristics(ids, comboBox);
        });

        comboBox.setRenderer(newListCellRender(decorator));

        selectCaracteristics(selectedIds, comboBox);
        ObjectToStringConverter converter = BeanUIUtil.newDecoratedObjectToStringConverter(decorator);
        BeanUIUtil.decorate(comboBox, converter);
        ComboBoxCellEditor editor = new ComboBoxCellEditor(comboBox);

        addColumnToModel(model,
                         editor,
                         newTableCellRender(decorator),
                         EditProtocolSpeciesTableModel.MATURITY_PMFM_ID);
    }

    protected void addCpsPmfmColumnToModel(TableColumnModel model) {

        Decorator<CaracteristicQualitativeValue> caracteristicQualitativeValueDecorator =
                getDecorator(CaracteristicQualitativeValue.class, null);

        addComboDataColumnToModel(model,
                                  EditProtocolSpeciesTableModel.CALCIFIED_PIECES_SAMPLING_TYPE_PMFM_ID,
                                  caracteristicQualitativeValueDecorator,
                                  getDataContext().getCpsTypeValues());

    }

    protected void initTable(final JXTable table,
                             DefaultTableColumnModelExt columnModel,
                             TableColumnExt speciesColumn,
                             ColumnIdentifier rtpIdentifier,
                             List<EditProtocolSpeciesRowModel> rows,
                             ListSelectionListener selectionListener) {

        EditProtocolSpeciesTableModel tableModel = new EditProtocolSpeciesTableModel(getModel()::newRow, columnModel);
        table.setModel(tableModel);
        table.setColumnModel(columnModel);

        JTableHeader tableHeader = table.getTableHeader();

        // by default do not authorize to change column orders
        tableHeader.setReorderingAllowed(false);

        addHighlighters(table);
        if (rtpIdentifier != null) {
            addRtpHighlighter(table, rtpIdentifier);
        }

        // always scroll to selected row
        SwingUtil.scrollToTableSelection(table);

        // add selection listener
        table.getSelectionModel().addListSelectionListener(selectionListener);

        // when model change, then rebuild the species comparator + set model as modified
        tableModel.addTableModelListener(e -> {
            getModel().setModify(true);

            int type = e.getType();
            if (type == TableModelEvent.DELETE ||
                    type == TableModelEvent.INSERT ||
                    e.getLastRow() == Integer.MAX_VALUE) {

                // get species column
                TableColumnExt tableColumn = (TableColumnExt) table.getColumns().get(0);

                // get column comparator
                TuttiDecorator.TuttiDecoratorComparator<Species> comparator =
                        (TuttiDecorator.TuttiDecoratorComparator<Species>)
                                tableColumn.getComparator();

                // get column comparator
                TuttiDecorator<Species> decorator =
                        SpeciesBatchRowHelper.getSpeciesColumnDecorator(tableColumn);

                boolean comparatorNull = comparator == null;
                if (comparatorNull) {

                    // first time coming here, add the comparator
                    comparator = decorator.getCurrentComparator();
                }

                // init comparator with model species list
                comparator.init(decorator, tableModel.getSpeciesList());

                if (comparatorNull) {

                    // affect it to colum
                    tableColumn.setComparator(comparator);
                }
            }
        });

        // create popup to change species decorator

        SpeciesBatchRowHelper.installSpeciesColumnComparatorPopup(
                table,
                speciesColumn,
                null,
                t("tutti.species.refTaxCode.tip"),
                t("tutti.species.name.tip")
        );

        // at the very end, set rows to model
        tableModel.setRows(rows);
    }

    protected void addRtpHighlighter(JXTable table, ColumnIdentifier identifier) {
        Color cellWithValueColor = getConfig().getColorCellWithValue();

        Highlighter commentHighlighter = TuttiUIUtil.newBackgroundColorHighlighter(
                new HighlightPredicate.AndHighlightPredicate(
                        new HighlightPredicate.IdentifierHighlightPredicate(identifier),
                        // for not null value
                        (renderer, adapter) -> (boolean) adapter.getValue()), cellWithValueColor);
        table.addHighlighter(commentHighlighter);
    }

    protected EditProtocolOperationFieldsRowModel createOperationFieldRow(String property, String column) {
        EditProtocolOperationFieldsRowModel newRow = new EditProtocolOperationFieldsRowModel();
        newRow.addPropertyChangeListener(evt -> {
            EditProtocolUIModel model = getModel();

            //update number of rows by column
            if (EditProtocolOperationFieldsRowModel.PROPERTY_IMPORT_COLUMN.equals(evt.getPropertyName())) {
                String oldValue = (String) evt.getOldValue();
                if (oldValue != null) {
                    model.decNumberOfRows(oldValue);
                }
                String newValue = (String) evt.getNewValue();
                if (newValue != null) {
                    model.incNumberOfRows(newValue);
                }

                recomputeRowsValidState();
            }
        });
        newRow.setValid(true);
        newRow.setField(property);
        newRow.setImportColumn(column);
        newRow.setValid(true);
        newRow.addPropertyChangeListener(evt -> getModel().setModify(true));
        return newRow;
    }

    protected void recomputeRowsValidState() {
        EditProtocolUIModel model = getModel();

        for (EditProtocolCaracteristicsRowModel row : model.getCaracteristicMappingRows()) {
            row.setValid(isCaracteristicsRowValid(row));
        }

        for (EditProtocolOperationFieldsRowModel row : model.getOperationFieldMappingRows()) {
            row.setValid(isOperationFieldsRowValid(row));
        }

        getCaracteristicsMappingTable().repaint();
        getOperationFieldsMappingTable().repaint();
    }

    protected boolean isOperationFieldsRowValid(EditProtocolOperationFieldsRowModel row) {
        EditProtocolUIModel model = getModel();
        String importColumn = row.getImportColumn();
        return importColumn == null || model.numberOfRows(importColumn) < 2;
    }

    protected boolean isCaracteristicsRowValid(EditProtocolCaracteristicsRowModel row) {
        EditProtocolUIModel model = getModel();
        String importColumn = row.getImportColumn();
        return row.getType() != null &&
                (importColumn == null || model.numberOfRows(importColumn) < 2);
    }

    protected void populateImportColumnTableEditors() {
        Collection<String> importColumns = getModel().getImportColumns();
        ArrayList<String> dataToList = new ArrayList<>();

        if (importColumns != null) {
            dataToList.addAll(importColumns);

            // add a null value at first position
            if (!dataToList.isEmpty() && dataToList.get(0) != null) {
                dataToList.add(0, null);
            }

        } else {
            dataToList.add(null);
        }

        populateImportColumnTableEditor(getUI().getCaracteristicsMappingTable(), 2, dataToList);
        populateImportColumnTableEditor(getUI().getOperationFieldsMappingTable(), 1, dataToList);
    }

    protected void populateImportColumnTableEditor(JXTable table, int columnIndex, ArrayList<String> dataToList) {
        JComboBox comboBox = new JComboBox();
        SwingUtil.fillComboBox(comboBox, dataToList, null);

        TableColumnExt col = table.getColumnExt(columnIndex);
        ComboBoxCellEditor editor = new ComboBoxCellEditor(comboBox);
        col.setCellEditor(editor);
    }

    public void permuteCaracteristics(int selectedRow, int newRow) {

        getCaracteristicMappingTableModel().permuteEntry(selectedRow, newRow);
        getCaracteristicsMappingTable().getSelectionModel().setSelectionInterval(newRow, newRow);

        // Recompute
        getModel().setCaracteristicMappingRows(getCaracteristicMappingTableModel().getRows());
        getModel().setModify(true);

    }

    protected static class UpdateSelectedList implements PropertyChangeListener {

        private final BeanDoubleListModel<Caracteristic> model;

        private final Map<String, Caracteristic> caracteristicMap;

        public UpdateSelectedList(BeanDoubleList<Caracteristic> doubleList,
                                  Map<String, Caracteristic> caracteristicMap) {
            this.model = doubleList.getModel();
            this.caracteristicMap = caracteristicMap;
        }

        private boolean valueIsAdjusting;

        @Override
        public void propertyChange(PropertyChangeEvent evt) {

            if (!valueIsAdjusting) {

                valueIsAdjusting = true;

                try {
                    List<String> selectedIds = (List<String>) evt.getNewValue();
                    if (log.isInfoEnabled()) {
                        log.info("[" + evt.getPropertyName() + "] selected ids: " + selectedIds);
                    }

                    select(selectedIds);
                } finally {
                    valueIsAdjusting = false;
                }
            }
        }

        public void select(List<String> selectedIds) {

            List<Caracteristic> selection = Lists.newArrayList();
            if (CollectionUtils.isNotEmpty(selectedIds)) {
                for (String selectedId : selectedIds) {
                    Caracteristic e = caracteristicMap.get(selectedId);
                    Preconditions.checkNotNull(e, "Could not find caracteristic with id: " + selectedId);
                    selection.add(e);
                }
            }
            model.setSelected(selection);
        }
    }

    protected static class SelectValuePredicate implements Predicate<Caracteristic> {

        protected final List<BeanDoubleList<Caracteristic>> lists;

        public SelectValuePredicate(List<BeanDoubleList<Caracteristic>> lists) {
            this.lists = lists;
        }

        @Override
        public boolean apply(Caracteristic input) {

            boolean result = true;
            for (BeanDoubleList<Caracteristic> list : lists) {
                if (list.getModel().getSelected().contains(input)) {
                    result = false;
                    break;
                }
            }
            return result;
        }
    }

    protected void initJTree(JTree tree) {
        tree.getModel().addTreeModelListener(new ExpandOnInsertTreeModelListener(tree));
        JAXXWidgetUtil.expandTree(tree);
    }

    private static class ExpandOnInsertTreeModelListener implements TreeModelListener {

        private final JTree tree;

        private ExpandOnInsertTreeModelListener(JTree tree) {
            this.tree = tree;
        }

        @Override
        public void treeNodesChanged(TreeModelEvent e) {
        }

        @Override
        public void treeNodesInserted(TreeModelEvent e) {
            tree.expandPath(e.getTreePath());
        }

        @Override
        public void treeNodesRemoved(TreeModelEvent e) {
        }

        @Override
        public void treeStructureChanged(TreeModelEvent e) {
        }

    }

    private static class LengthStepCaracteristicCellRenderer implements ListCellRenderer<Caracteristic> {

        private final CaracteristicsCount caracteristicsCount;
        private final Color notRemovableColor;

        private final ListCellRenderer<? super Caracteristic> defaultRenderer;

        LengthStepCaracteristicCellRenderer(Color notRemovableColor, CaracteristicsCount caracteristicsCount, ListCellRenderer<? super Caracteristic> defaultRenderer) {
            this.notRemovableColor = notRemovableColor;
            this.caracteristicsCount = caracteristicsCount;
            this.defaultRenderer = defaultRenderer;
        }

        @Override
        public Component getListCellRendererComponent(JList<? extends Caracteristic> list, Caracteristic value, int index, boolean isSelected, boolean cellHasFocus) {
            Component result = defaultRenderer.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);

            if (isSelected && caracteristicsCount.isCaracteristicUsed(value)) {
                result.setBackground(notRemovableColor.darker());
            }
            return result;
        }
    }

    private class MaturityCaracteristicCellRenderer implements ListCellRenderer<Caracteristic> {

        public static final String TEXT_PATTERN = "<html><body><strong>%s</strong> :<ul><li>%s</li></ul><strong>%s</strong> :<ul><li>%s</li></ul></body></html>";

        private final CaracteristicsCount caracteristicsCount;
        private final ListCellRenderer<? super Caracteristic> defaultRenderer;

        private Color validColor = getConfig().getColorCellWithValue();

        private Color invalidColor = getConfig().getColorRowInvalid();

        private Color notRemovableColor = getConfig().getColorWarningRow();

        MaturityCaracteristicCellRenderer(CaracteristicsCount caracteristicsCount, ListCellRenderer<? super Caracteristic> defaultRenderer) {
            this.caracteristicsCount = caracteristicsCount;
            this.defaultRenderer = defaultRenderer;
        }

        @Override
        public Component getListCellRendererComponent(JList<? extends Caracteristic> list, Caracteristic value, int index, boolean isSelected, boolean cellHasFocus) {
            Component result = defaultRenderer.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);

            Color backgroud;
            if (isSelected && caracteristicsCount.isCaracteristicUsed(value)) {
                backgroud = notRemovableColor;

            } else if (!EditProtocolUIHandler.this.getModel().isMaturityValid(value)) {
                backgroud = invalidColor;
            } else {
                backgroud = validColor;
            }
            if (isSelected) {
                backgroud = backgroud.darker();
            }
            result.setBackground(backgroud);

            if (result instanceof JComponent) {

                ((JComponent) result).setToolTipText(createToolTipText(value));
            }
            return result;
        }

        private String createToolTipText(Caracteristic caracteristic) {
            String tooltip = null;

            if (caracteristic != null && getModel().isMaturityValid(caracteristic.getId())) {

                MaturityCaracteristic maturityCaracteristic = getModel().getMaturityCaracteristic(caracteristic.getId());
                List<String> matureStates = new ArrayList<>();
                List<String> immatureStates = new ArrayList<>();

                caracteristic.getQualitativeValue().forEach(state -> {
                    if (maturityCaracteristic.containsMatureStateIds(state.getId())) {
                        matureStates.add(decorate(state));
                    } else {
                        immatureStates.add(decorate(state));
                    }
                });

                tooltip = String.format(TEXT_PATTERN, t("tutti.editProtocol.field.maturity.immature.tip"), StringUtils.join(immatureStates, "</li><li>"),
                                        t("tutti.editProtocol.field.maturity.mature.tip"), StringUtils.join(matureStates, "</li><li>"));
            }

            return tooltip;
        }

    }
}

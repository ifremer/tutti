package fr.ifremer.tutti.ui.swing.content.program;

/*
 * #%L
 * Tutti :: UI
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Lists;
import fr.ifremer.tutti.persistence.entities.data.Program;
import fr.ifremer.tutti.ui.swing.util.AbstractTuttiUIHandler;
import jaxx.runtime.JAXXUtil;
import jaxx.runtime.validator.swing.SwingValidator;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.jaxx.application.swing.util.CloseableUI;

import javax.swing.JComponent;

import static org.nuiton.i18n.I18n.t;

/**
 * Handler of UI {@link EditProgramUI}.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 0.1
 */
public class EditProgramUIHandler extends AbstractTuttiUIHandler<EditProgramUIModel, EditProgramUI> implements CloseableUI {

    /** Logger. */
    private static final Log log =
            LogFactory.getLog(EditProgramUIHandler.class);

    public static String getTitle(boolean exist) {

        String result;
        if (exist) {
            result = t("tutti.editProgram.title.edit.program");
        } else {
            result = t("tutti.editProgram.title.create.program");
        }
        return result;
    }

    @Override
    public void beforeInit(EditProgramUI ui) {

        super.beforeInit(ui);
        getDataContext().resetValidationDataContext();

        EditProgramUIModel model = new EditProgramUIModel();

        String programId = getContext().getProgramId();
        if (programId == null) {

            if (log.isInfoEnabled()) {
                log.info("Edit new program");
            }
        } else {

            if (log.isInfoEnabled()) {
                log.info("Edit existing program " + programId);
            }
            // load existing program
            Program program = getPersistenceService().getProgram(programId);

            model.fromEntity(program);
        }

        listModelIsModify(model);

        ui.setContextValue(model);
    }

    @Override
    public void afterInit(EditProgramUI ui) {

        initUI(ui);

        EditProgramUIModel model = getModel();
        initBeanFilterableComboBox(ui.getZoneComboBox(),
                                   Lists.newArrayList(getPersistenceService().getAllProgramZone()),
                                   model.getZone());

        SwingValidator validator = ui.getValidator();
        listenValidatorValid(validator, model);

        registerValidators(validator);

        // if new program can already cancel his creation
        model.setModify(model.isCreate());

        //FIXME Binding does not work
        JAXXUtil.applyDataBinding(ui, EditProgramUI.BINDING_RESET_BUTTON_ENABLED);
    }

    @Override
    protected JComponent getComponentToFocus() {
        return getUI().getNameField();
    }

    @Override
    public void onCloseUI() {
        if (log.isDebugEnabled()) {
            log.debug("closing: " + ui);
        }
        clearValidators();
    }

    @Override
    public boolean quitUI() {
        return quitScreen(
                getModel().isValid(),
                getModel().isModify(),
                t("tutti.editProgram.askCancelEditBeforeLeaving.cancelSaveProgram"),
                t("tutti.editProgram.askSaveBeforeLeaving.saveProgram"),
                ui.getSaveButton().getAction()
        );
    }

    @Override
    public SwingValidator<EditProgramUIModel> getValidator() {
        return ui.getValidator();
    }

    public void reloadProgram(Program program) {

        EditProgramUIModel model = getModel();

        model.fromEntity(program);

        getModel().setModify(false);
    }

}

package fr.ifremer.tutti.ui.swing.content.operation.catches.species.edit.actions;

/*
 * #%L
 * Tutti :: UI
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Joiner;
import com.google.common.collect.Lists;
import fr.ifremer.adagio.core.dao.referential.pmfm.PmfmId;
import fr.ifremer.tutti.persistence.entities.data.CatchBatch;
import fr.ifremer.tutti.persistence.entities.data.FishingOperation;
import fr.ifremer.tutti.persistence.entities.data.SampleCategoryModel;
import fr.ifremer.tutti.service.PersistenceService;
import fr.ifremer.tutti.service.pupitri.PupitriImportResult;
import fr.ifremer.tutti.service.pupitri.PupitriImportService;
import fr.ifremer.tutti.ui.swing.content.operation.FishingOperationsUI;
import fr.ifremer.tutti.ui.swing.content.operation.catches.EditCatchesUIModel;
import fr.ifremer.tutti.ui.swing.content.operation.catches.species.edit.ImportPupitriMelagWeightPopupUI;
import fr.ifremer.tutti.ui.swing.content.operation.catches.species.edit.ImportPupitriPopupUI;
import fr.ifremer.tutti.ui.swing.content.operation.catches.species.edit.ImportPupitriPopupUIModel;
import fr.ifremer.tutti.ui.swing.content.operation.catches.species.edit.SpeciesBatchUI;
import fr.ifremer.tutti.ui.swing.content.operation.catches.species.edit.SpeciesBatchUIHandler;
import fr.ifremer.tutti.ui.swing.content.operation.catches.species.edit.SpeciesBatchUIModel;
import fr.ifremer.tutti.ui.swing.content.operation.fishing.actions.EditFishingOperationAction;
import fr.ifremer.tutti.ui.swing.util.AbstractTuttiUIHandler;
import fr.ifremer.tutti.ui.swing.util.TuttiUIUtil;
import fr.ifremer.tutti.ui.swing.util.actions.LongActionSupport;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.jaxx.application.swing.AbstractApplicationUIHandler;

import javax.swing.JOptionPane;
import javax.swing.UIManager;
import java.awt.Component;
import java.io.File;
import java.util.List;
import java.util.Set;

import static org.nuiton.i18n.I18n.t;

/**
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.0
 */
public class ImportPupitriAction extends LongActionSupport<SpeciesBatchUIModel, SpeciesBatchUI, SpeciesBatchUIHandler> {

    /** Logger. */
    private static final Log log = LogFactory.getLog(ImportPupitriAction.class);

    protected File importedTrunkFile;

    protected File importedCarrouselFile;

    protected boolean importMissingBatches;

    protected PersistenceService persistenceService;

    protected EditFishingOperationAction editAction;

    protected PupitriImportResult importResult;

    public ImportPupitriAction(SpeciesBatchUIHandler handler) {
        super(handler, false);
        persistenceService = getContext().getPersistenceService();
    }

    public EditFishingOperationAction getEditAction() {
        if (editAction == null) {
            FishingOperationsUI parentContainer = handler.getParentContainer(FishingOperationsUI.class);
            editAction = getContext().getActionFactory().createLogicAction(parentContainer.getHandler(),
                                                                           EditFishingOperationAction.class);
        }
        return editAction;
    }

    @Override
    public boolean prepareAction() throws Exception {
        boolean result = true;

        // must check that sample category model is compatible for pupitri import
        // means need some categories

        SampleCategoryModel sampleCategoryModel = getDataContext().getSampleCategoryModel();

        List<String> missingCategories = Lists.newArrayList();
        if (!sampleCategoryModel.containsCategoryId(PmfmId.SIZE_CATEGORY.getValue())) {
            missingCategories.add("<li>" + persistenceService.getSizeCategoryCaracteristic().getParameterName() + "</li>");
        }
        if (!sampleCategoryModel.containsCategoryId(PmfmId.SEX.getValue())) {
            missingCategories.add("<li>" + persistenceService.getSexCaracteristic().getParameterName() + "</li>");
        }
        if (!missingCategories.isEmpty()) {
            result = false;
            JOptionPane.showMessageDialog(
                    getContext().getActionUI(),
                    t("tutti.editSpeciesBatch.action.importPupitri.invalidSampleCategoryModel.message", Joiner.on("").join(missingCategories)),
                    t("tutti.editSpeciesBatch.action.importPupitri.invalidSampleCategoryModel.title"),
                    JOptionPane.ERROR_MESSAGE,
                    UIManager.getIcon("error")
            );
        }
        if (result) {

            SpeciesBatchUIModel speciesBatchUIModel = getUI().getModel();
            if (speciesBatchUIModel.getRowCount() > 0) {
                String htmlMessage = String.format(
                        AbstractTuttiUIHandler.CONFIRMATION_FORMAT,
                        t("tutti.editSpeciesBatch.action.importPupitri.existingData.message"),
                        t("tutti.editSpeciesBatch.action.importPupitri.existingData.help"));

                Component dialogParentComponent = getDialogParentComponent();
                int answer = JOptionPane.showConfirmDialog(dialogParentComponent,
                                                           htmlMessage,
                                                           t("tutti.editSpeciesBatch.action.importPupitri.existingData.title"),
                                                           JOptionPane.OK_CANCEL_OPTION,
                                                           JOptionPane.WARNING_MESSAGE);

                result = answer == JOptionPane.OK_OPTION;
            }
        }

        if (result) {

            ImportPupitriPopupUI importPupitriDialog = new ImportPupitriPopupUI(getUI());
            importPupitriDialog.open();

            ImportPupitriPopupUIModel importPupitriDialogModel = importPupitriDialog.getModel();
            importedTrunkFile = importPupitriDialogModel.getTrunkFile();
            importedCarrouselFile = importPupitriDialogModel.getCarrouselFile();
            importMissingBatches = importPupitriDialogModel.isImportMissingBatches();

            result = importedTrunkFile != null && importedCarrouselFile != null;
        }

        if (result) {

            EditCatchesUIModel model = getModel().getCatchesUIModel();

            FishingOperation operation = model.getFishingOperation();

            // import
            PupitriImportService service = getContext().getPupitriImportService();
            importResult = service.readImportPupitri(importedTrunkFile, importedCarrouselFile, operation, importMissingBatches);

            result = importResult.isFishingOperationFound();

            if (!result) {
                displayWarningMessage(
                        t("tutti.editSpeciesBatch.action.importPupitri.no.matching.fishingOperation.title"),
                        "<html><body>" +
                        t("tutti.editSpeciesBatch.action.importPupitri.no.matching.fishingOperation") +
                        "</body></html>"
                );
                sendMessage(t("tutti.editSpeciesBatch.action.importPupitri.no.matching.data"));

            } else if (importResult.isFoundTotalMelag()) {

                ImportPupitriMelagWeightPopupUI importPupitriMelagWeightDialog = new ImportPupitriMelagWeightPopupUI(getUI());
                importPupitriMelagWeightDialog.open(importResult.getMelagTotalWeight());
                float weight = importPupitriMelagWeightDialog.getModel().getTotalMelagWeight();
                importResult.setEditedMelagTotalWeight(weight);
            }
        }

        return result;
    }

    @Override
    public void doAction() throws Exception {

        EditCatchesUIModel model = getModel().getCatchesUIModel();

        FishingOperation operation = model.getFishingOperation();
        CatchBatch catchBatch = model.toEntity();

        // import
        PupitriImportService service = getContext().getPupitriImportService();
        importResult = service.saveImportPupitri(importedTrunkFile,
                                                 importedCarrouselFile,
                                                 operation,
                                                 catchBatch,
                                                 importMissingBatches,
                                                 importResult);

        if (importResult.isFishingOperationFound()) {

            // reload operation
            getEditAction().loadCatchBatch(operation);
        }
    }

    @Override
    public void releaseAction() {
        importedTrunkFile = null;
        importedCarrouselFile = null;
        importMissingBatches = false;
        super.releaseAction();
    }

    @Override
    public void postSuccessAction() {
        super.postSuccessAction();

        if (importResult.isFishingOperationFound()) {

            Set<String> notImportedSpeciesIds = importResult.getNotImportedSpeciesIds();

            int rejectedSpeciesNb = notImportedSpeciesIds.size();

            sendMessage(t("tutti.editSpeciesBatch.action.importPupitri.success",
                          getModel().getRootNumber(), rejectedSpeciesNb));

            if (!notImportedSpeciesIds.isEmpty()) {

                // on affiche la liste des especes non importees

                StringBuilder content = new StringBuilder();

                for (String notImportedSpeciesId : notImportedSpeciesIds) {
                    content.append("<li>").append(notImportedSpeciesId).append("</li>");
                }

                String text =
                        "<html><body>"
                        + t("tutti.editSpeciesBatch.action.importPupitri.speciesNotImported.message",
                            rejectedSpeciesNb, content.toString())
                        + "</body></html>";

                displayWarningMessage(t("tutti.editSpeciesBatch.action.importPupitri.speciesNotImported.title"), text);

            }

            // Ask User to see pupiti report

            String reportAttachmentFilename = importResult.getReportAttachmentFilename();
            String htmlMessage = String.format(
                    AbstractApplicationUIHandler.CONFIRMATION_FORMAT,
                    t("tutti.editSpeciesBatch.action.importPupitri.showReport.message", reportAttachmentFilename),
                    "");

            Component ui = getDialogParentComponent();

            boolean showReport = JOptionPane.showOptionDialog(
                    ui,
                    htmlMessage,
                    t("tutti.editSpeciesBatch.action.importPupitri.showReport.title"),
                    JOptionPane.OK_CANCEL_OPTION,
                    JOptionPane.QUESTION_MESSAGE,
                    null,
                    new Object[]{t("tutti.editSpeciesBatch.action.showReport"), t("tutti.editSpeciesBatch.action.doNotShowReport")}, t("tutti.editSpeciesBatch.action.showReport")) == 0;

            if (showReport) {

                String reportAttachmentId = importResult.getReportAttachmentId();
                File attachmentFile = persistenceService.getAttachmentFile(reportAttachmentId);
                if (log.isInfoEnabled()) {
                    log.info("Open pupitri report at " + attachmentFile);
                }

                TuttiUIUtil.openResource(attachmentFile);

            }
        }
    }
}

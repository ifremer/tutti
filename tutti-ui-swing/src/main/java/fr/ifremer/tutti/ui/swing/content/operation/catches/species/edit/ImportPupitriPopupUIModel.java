package fr.ifremer.tutti.ui.swing.content.operation.catches.species.edit;

/*
 * #%L
 * Tutti :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2016 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.jdesktop.beans.AbstractSerializableBean;

import java.io.File;

/**
 * @author Kevin Morin (Code Lutin)
 * @since 4.3
 */
public class ImportPupitriPopupUIModel extends AbstractSerializableBean {

    public static final String PROPERTY_TRUNK_FILE = "trunkFile";

    public static final String PROPERTY_CARROUSEL_FILE = "carrouselFile";

    public static final String PROPERTY_IMPORT_MISSING_BATCHES = "importMissingBatches";

    protected File trunkFile;

    protected File carrouselFile;

    protected boolean importMissingBatches;

    public File getTrunkFile() {
        return trunkFile;
    }

    public void setTrunkFile(File trunkFile) {
        Object oldValue = getTrunkFile();
        this.trunkFile = trunkFile;
        firePropertyChange(PROPERTY_TRUNK_FILE, oldValue, trunkFile);
    }

    public File getCarrouselFile() {
        return carrouselFile;
    }

    public void setCarrouselFile(File carrouselFile) {
        Object oldValue = getCarrouselFile();
        this.carrouselFile = carrouselFile;
        firePropertyChange(PROPERTY_CARROUSEL_FILE, oldValue, carrouselFile);
    }

    public boolean isImportMissingBatches() {
        return importMissingBatches;
    }

    public void setImportMissingBatches(boolean importMissingBatches) {
        Object oldValue = isImportMissingBatches();
        this.importMissingBatches = importMissingBatches;
        firePropertyChange(PROPERTY_IMPORT_MISSING_BATCHES, oldValue, importMissingBatches);
    }
}

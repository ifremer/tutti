package fr.ifremer.tutti.ui.swing.content.home.actions;

/*
 * #%L
 * Tutti :: UI
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import fr.ifremer.tutti.persistence.entities.protocol.TuttiProtocol;
import fr.ifremer.tutti.service.PersistenceService;
import fr.ifremer.tutti.ui.swing.content.home.SelectCruiseUI;
import fr.ifremer.tutti.ui.swing.content.home.SelectCruiseUIHandler;
import fr.ifremer.tutti.ui.swing.content.home.SelectCruiseUIModel;
import fr.ifremer.tutti.ui.swing.util.actions.LongActionSupport;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.List;

import static org.nuiton.i18n.I18n.t;

/**
 * To delete the selected protocol.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.0
 */
public class DeleteProtocolAction extends LongActionSupport<SelectCruiseUIModel, SelectCruiseUI, SelectCruiseUIHandler> {

    /** Logger. */
    private static final Log log = LogFactory.getLog(EditProtocolAction.class);

    public DeleteProtocolAction(SelectCruiseUIHandler handler) {
        super(handler, false);
    }

    @Override
    public boolean prepareAction() throws Exception {
        boolean result = super.prepareAction();

        TuttiProtocol protocol = getHandler().getModel().getProtocol();

        result &=
                askBeforeDelete(
                        t("tutti.selectCruise.action.deleteProtocol.title"),
                        t("tutti.selectCruise.action.deleteProtocol.message", protocol.getName())
                );

        return result;
    }

    @Override
    public void doAction() {
        TuttiProtocol protocol = getModel().getProtocol();
        Preconditions.checkNotNull(protocol);
        String id = protocol.getId();
        Preconditions.checkNotNull(id);
        if (log.isInfoEnabled()) {
            log.info("Delete protocol: " + id);
        }

        PersistenceService service = getContext().getPersistenceService();
        service.deleteProtocol(id);

        getModel().setProtocol(null);

        List<TuttiProtocol> protocols = getModel().getProtocols();
        protocols.remove(protocol);

        if (log.isInfoEnabled()) {
            log.info("nb protocols: " + protocols.size());
        }
        // reset (will clear combo-box)
        getModel().setProtocols(null);

        // set new list
        getModel().setProtocols(protocols);

    }

    @Override
    public void postSuccessAction() {

        getHandler().resetComboBoxAction(getUI().getEditProtocolComboBox());
        super.postSuccessAction();

    }
}

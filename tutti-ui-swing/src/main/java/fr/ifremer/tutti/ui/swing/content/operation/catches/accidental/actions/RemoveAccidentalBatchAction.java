package fr.ifremer.tutti.ui.swing.content.operation.catches.accidental.actions;

/*
 * #%L
 * Tutti :: UI
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import fr.ifremer.tutti.persistence.TuttiPersistence;
import fr.ifremer.tutti.persistence.entities.TuttiEntities;
import fr.ifremer.tutti.ui.swing.util.actions.LongActionSupport;
import fr.ifremer.tutti.ui.swing.content.operation.catches.accidental.AccidentalBatchRowModel;
import fr.ifremer.tutti.ui.swing.content.operation.catches.accidental.AccidentalBatchTableModel;
import fr.ifremer.tutti.ui.swing.content.operation.catches.accidental.AccidentalBatchUI;
import fr.ifremer.tutti.ui.swing.content.operation.catches.accidental.AccidentalBatchUIHandler;
import fr.ifremer.tutti.ui.swing.content.operation.catches.accidental.AccidentalBatchUIModel;
import fr.ifremer.tutti.ui.swing.util.TuttiUIUtil;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jdesktop.swingx.JXTable;

import javax.swing.JOptionPane;

import static org.nuiton.i18n.I18n.t;

/**
 * @author Kevin Morin - kmorin@codelutin.com
 * @since 1.4
 */
public class RemoveAccidentalBatchAction extends LongActionSupport<AccidentalBatchUIModel, AccidentalBatchUI, AccidentalBatchUIHandler> {

    private static final Log log =
            LogFactory.getLog(RemoveAccidentalBatchAction.class);

    public RemoveAccidentalBatchAction(AccidentalBatchUIHandler handler) {
        super(handler, false);
    }

    int rowIndex;

    @Override
    public boolean prepareAction() throws Exception {
        boolean result = super.prepareAction();

        if (result) {
            int answer = JOptionPane.showConfirmDialog(getContext().getActionUI(),
                                                       t("tutti.editAccidentalBatch.action.removeBatch.confirm.message"),
                                                       t("tutti.editAccidentalBatch.action.removeBatch.confirm.title"),
                                                       JOptionPane.YES_NO_OPTION);
            result = answer == JOptionPane.YES_OPTION;
        }

        return result;
    }

    @Override
    public void doAction() throws Exception {

        JXTable table = handler.getTable();

        rowIndex = table.getSelectedRow();

        Preconditions.checkState(rowIndex != -1,
                                 "Cant remove batch if none is selected");

        AccidentalBatchTableModel tableModel = handler.getTableModel();
        AccidentalBatchRowModel selectedBatch = tableModel.getEntry(rowIndex);

        boolean persisted = !TuttiEntities.isNew(selectedBatch);

        if (persisted) {

            // remove it from db

            String id = selectedBatch.getId();

            if (log.isInfoEnabled()) {
                log.info("Remove accidental with id: " + id);
            }

            TuttiPersistence persistenceService =
                    getContext().getPersistenceService();

            persistenceService.deleteAccidentalBatch(id);
        }
    }

    @Override
    public void postSuccessAction() {
        super.postSuccessAction();

        JXTable table = handler.getTable();

        AccidentalBatchTableModel tableModel = handler.getTableModel();

        tableModel.removeRow(rowIndex);

        TuttiUIUtil.selectFirstCellOnFirstRowAndStopEditing(table);
    }
}

package fr.ifremer.tutti.ui.swing.content.operation.catches;

/*
 * #%L
 * Tutti :: UI
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Sets;
import fr.ifremer.tutti.persistence.entities.referential.Species;
import fr.ifremer.tutti.type.WeightUnit;
import fr.ifremer.tutti.ui.swing.content.operation.catches.species.BenthosBatchUISupportImpl;
import fr.ifremer.tutti.ui.swing.content.operation.catches.species.EditSpeciesBatchPanelUI;
import fr.ifremer.tutti.ui.swing.content.operation.catches.species.SpeciesOrBenthosBatchUISupport;
import fr.ifremer.tutti.ui.swing.util.AbstractTuttiTabContainerUIHandler;
import fr.ifremer.tutti.ui.swing.util.TuttiBeanMonitor;
import jaxx.runtime.swing.CardLayout2Ext;
import jaxx.runtime.validator.swing.SwingValidator;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jdesktop.swingx.JXTitledPanel;
import org.nuiton.jaxx.application.swing.tab.TabContentModel;
import org.nuiton.jaxx.application.swing.tab.TabHandler;

import javax.swing.JComponent;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import java.awt.Component;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Set;

import static org.nuiton.i18n.I18n.n;
import static org.nuiton.i18n.I18n.t;

/**
 * @author Tony Chemit - chemit@codelutin.com
 * @since 0.3
 */
public class EditCatchesUIHandler extends AbstractTuttiTabContainerUIHandler<EditCatchesUIModel, EditCatchesUI>
        implements TabHandler {

    public static final String MAIN_CARD = "main";

    public static final String CREATE_BATCH_CARD = "createBatch";

    public static final String EDIT_CARACTERISTICS_CARD = "caracteristicsCard";

    /** Logger. */
    private static final Log log = LogFactory.getLog(EditCatchesUIHandler.class);

    /**
     * To monitor changes on the incoming fishing operation.
     *
     * @since 0.3
     */
    private TuttiBeanMonitor<EditCatchesUIModel> catchBatchMonitor;

    /**
     * To remove {@code 0} value of total rejected weight.
     *
     * @since 3.5
     */
    private final PropertyChangeListener totalWeightRejectedListener = new PropertyChangeListener() {

        public void propertyChange(PropertyChangeEvent evt) {

            EditCatchesUIModel source = (EditCatchesUIModel) evt.getSource();

            Float totalWeight = (Float) evt.getNewValue();

            if (totalWeight != null && WeightUnit.KG.isZero(totalWeight)) {

                // remove the totalWeight (see https://forge.codelutin.com/issues/5144)
                source.setCatchTotalRejectedWeight(null);

            }
        }
    };

    /**
     * To handle the svg canvas.
     *
     * @since 3.13
     */
    private EditCatchesSvgHandler editCatchesSvgHandler;

    //------------------------------------------------------------------------//
    //-- AbstractTuttiUIHandler methods                                     --//
    //------------------------------------------------------------------------//

    @Override
    public void beforeInit(EditCatchesUI ui) {
        super.beforeInit(ui);

        this.catchBatchMonitor = new TuttiBeanMonitor<EditCatchesUIModel>(
                EditCatchesUIModel.PROPERTY_MARINE_LITTER_TOTAL_WEIGHT,
                EditCatchesUIModel.PROPERTY_SPECIES_TOTAL_SORTED_WEIGHT,
                EditCatchesUIModel.PROPERTY_SPECIES_TOTAL_INERT_WEIGHT,
                EditCatchesUIModel.PROPERTY_SPECIES_TOTAL_LIVING_NOT_ITEMIZED_WEIGHT,
                EditCatchesUIModel.PROPERTY_BENTHOS_TOTAL_SORTED_WEIGHT,
                EditCatchesUIModel.PROPERTY_BENTHOS_TOTAL_INERT_WEIGHT,
                EditCatchesUIModel.PROPERTY_BENTHOS_TOTAL_LIVING_NOT_ITEMIZED_WEIGHT);

        EditCatchesUIModel model = ui.getContextValue(EditCatchesUIModel.class);

        model.setValidationContext(getContext().getValidationContext());

        listModelIsModify(model);

        catchBatchMonitor.setBean(model);

        editCatchesSvgHandler = new EditCatchesSvgHandler(getContext(), ui, model);

    }

    @Override
    public void afterInit(EditCatchesUI ui) {

        initUI(ui);

        EditCatchesUIModel model = getModel();

        changeValidatorContext(model.getValidationContext(), getValidator());
        listenValidationTableHasNoFatalError(getValidator(), model);

        setCustomTab2(0, model,
                      Sets.newHashSet(EditCatchesUIModel.PROPERTY_CATCH_TOTAL_WEIGHT,
                                      EditCatchesUIModel.PROPERTY_CATCH_TOTAL_REJECTED_WEIGHT,
                                      EditCatchesUIModel.PROPERTY_SPECIES_TOTAL_SORTED_WEIGHT,
                                      EditCatchesUIModel.PROPERTY_SPECIES_TOTAL_INERT_WEIGHT,
                                      EditCatchesUIModel.PROPERTY_SPECIES_TOTAL_LIVING_NOT_ITEMIZED_WEIGHT,
                                      EditCatchesUIModel.PROPERTY_BENTHOS_TOTAL_SORTED_WEIGHT,
                                      EditCatchesUIModel.PROPERTY_BENTHOS_TOTAL_INERT_WEIGHT,
                                      EditCatchesUIModel.PROPERTY_BENTHOS_TOTAL_LIVING_NOT_ITEMIZED_WEIGHT,
                                      EditCatchesUIModel.PROPERTY_MARINE_LITTER_TOTAL_WEIGHT));

        setCustomTab(1, ui.getSpeciesTabPanel().getEditBatchesUI().getModel());
        setCustomTab(2, ui.getBenthosTabPanel().getEditBatchesUI().getModel());
        setCustomTab(3, ui.getMarineLitterTabContent().getModel());
        setCustomTab(4, ui.getAccidentalTabContent().getModel());

        getTabPanel().setSelectedIndex(1);
        // when internal tab change, close any attachments popup
        getTabPanel().addChangeListener(e -> closeAttachments());

    }

    public void setCustomTab2(int index, TabContentModel model, Set<String> modifyPropertyNames) {

        JTabbedPane tabPanel = getTabPanel();
        tabPanel.setTabComponentAt(index, CatchCustomTab.newCustomTab(model, this, modifyPropertyNames));

    }

    public void closeAttachments() {
        ui.getCatchesCaracteristicsAttachmentsButton().onCloseUI();
        ui.getSpeciesTabPanel().getEditBatchesUI().getSpeciesBatchAttachmentsButton().onCloseUI();
        ui.getBenthosTabPanel().getEditBatchesUI().getSpeciesBatchAttachmentsButton().onCloseUI();
        ui.getMarineLitterTabContent().getMarineLitterBatchAttachmentsButton().onCloseUI();
        ui.getAccidentalTabContent().getAccidentalBatchAttachmentsButton().onCloseUI();
    }

    @Override
    protected JComponent getComponentToFocus() {
        return null;
    }

    @Override
    public void onCloseUI() {

        if (log.isDebugEnabled()) {
            log.debug("closing: " + ui);
        }
        ui.getCatchesCaracteristicsAttachmentsButton().onCloseUI();
        editCatchesSvgHandler.clearSVG();

        // close batches tabs, then general tab
        closeUI(ui.getSpeciesTabPanel());
        closeUI(ui.getBenthosTabPanel());
        closeUI(ui.getMarineLitterTabContent());
        closeUI(ui.getAccidentalTabContent());
    }

    @Override
    public SwingValidator<EditCatchesUIModel> getValidator() {
        return ui.getValidator();
    }

    @Override
    protected Set<String> getPropertiesToIgnore() {
        Set<String> result = super.getPropertiesToIgnore();
        result.addAll(Sets.newHashSet(
                EditCatchesUIModel.PROPERTY_CATCH_TOTAL_COMPUTED_WEIGHT,
                EditCatchesUIModel.PROPERTY_CATCH_TOTAL_UNSORTED_COMPUTED_WEIGHT,
                EditCatchesUIModel.PROPERTY_CATCH_TOTAL_SORTED_COMPUTED_WEIGHT,
                EditCatchesUIModel.PROPERTY_CATCH_TOTAL_SORTED_SORTED_COMPUTED_WEIGHT,
                EditCatchesUIModel.PROPERTY_CATCH_TOTAL_REJECTED_COMPUTED_WEIGHT,
                EditCatchesUIModel.PROPERTY_SPECIES_TOTAL_COMPUTED_WEIGHT,
                EditCatchesUIModel.PROPERTY_SPECIES_TOTAL_SORTED_COMPUTED_WEIGHT,
                EditCatchesUIModel.PROPERTY_SPECIES_TOTAL_UNSORTED_COMPUTED_WEIGHT,
                EditCatchesUIModel.PROPERTY_SPECIES_TOTAL_SAMPLE_SORTED_COMPUTED_WEIGHT,
                EditCatchesUIModel.PROPERTY_SPECIES_TOTAL_INERT_COMPUTED_WEIGHT,
                EditCatchesUIModel.PROPERTY_SPECIES_TOTAL_LIVING_NOT_ITEMIZED_COMPUTED_WEIGHT,
                EditCatchesUIModel.PROPERTY_BENTHOS_TOTAL_COMPUTED_WEIGHT,
                EditCatchesUIModel.PROPERTY_BENTHOS_TOTAL_SORTED_COMPUTED_WEIGHT,
                EditCatchesUIModel.PROPERTY_BENTHOS_TOTAL_UNSORTED_COMPUTED_WEIGHT,
                EditCatchesUIModel.PROPERTY_BENTHOS_TOTAL_SAMPLE_SORTED_COMPUTED_WEIGHT,
                EditCatchesUIModel.PROPERTY_BENTHOS_TOTAL_INERT_COMPUTED_WEIGHT,
                EditCatchesUIModel.PROPERTY_BENTHOS_TOTAL_LIVING_NOT_ITEMIZED_COMPUTED_WEIGHT,
                EditCatchesUIModel.PROPERTY_SPECIES_DISTINCT_SORTED_SPECIES_COUNT,
                EditCatchesUIModel.PROPERTY_BENTHOS_DISTINCT_SORTED_SPECIES_COUNT,
                EditCatchesUIModel.PROPERTY_SPECIES_DISTINCT_UNSORTED_SPECIES_COUNT,
                EditCatchesUIModel.PROPERTY_BENTHOS_DISTINCT_UNSORTED_SPECIES_COUNT,
                EditCatchesUIModel.PROPERTY_MARINE_LITTER_TOTAL_COMPUTED_WEIGHT,
                EditCatchesUIModel.PROPERTY_ATTACHMENT
        ));
        return result;
    }

    @Override
    public JTabbedPane getTabPanel() {
        return ui.getTabPane();
    }

    @Override
    public boolean onTabChanged(int currentIndex, int newIndex) {

        ui.getCatchesCaracteristicsAttachmentsButton().onCloseUI();
        ui.getComputeSpeciesBatchButton().setVisible(newIndex < 4);
        ui.getCleanSpeciesBatchButton().setVisible(newIndex > 0 && newIndex < 3);

        boolean result = super.onTabChanged(currentIndex, newIndex);

        if (result && currentIndex != newIndex) {

            result = getModel().isDoNotCheckLeavingFrequencyScreen() || blockIfNotLeavingFrequencyScreen();
            if (!result) {
                return false;
            }

            boolean showMainActions;

            if (currentIndex == 0) {
                editCatchesSvgHandler.clearSVG();
            }
            if (newIndex == 0) {

                if (getUI().isVisible()) {

                    editCatchesSvgHandler.initResumeSvg();
                }
                showMainActions = true;

            } else {

                JComponent componentAt = (JComponent) getTabPanel().getComponentAt(newIndex);
                CardLayout2Ext layout = (CardLayout2Ext) componentAt.getLayout();
                String selectedCard = layout.getSelected();
                showMainActions = selectedCard == null
                        || MAIN_CARD.equals(selectedCard)
                        || EditSpeciesBatchPanelUI.EDIT_BATCH_CARD.equals(selectedCard);

            }

            getUI().getCreateFishingOperationActions().setVisible(showMainActions);

        }

        return result;
    }

    @Override
    public boolean onHideTab(int currentIndex, int newIndex) {

        closeAttachments();

        EditCatchesUIModel model = getModel();
        boolean result = blockIfNotLeavingFrequencyScreen();
        if (!result) {
            return false;
        }

        if (model.isModify()) {

            if (model.isValid()) {

                // ask user to save, do not save or cancel action
                int answer = askSaveBeforeLeaving(
                        t("tutti.editCatchBatch.askSaveBeforeLeaving.saveCatchBatch"));
                switch (answer) {
                    case JOptionPane.OK_OPTION:

                        // persist catch batch
                        getContext().getActionEngine().runAction(getUI().getSaveButton());

                        result = true;
                        break;

                    case JOptionPane.NO_OPTION:

                        // won't save modification
                        // so since we will edit a new operation, nothing to do here

                        // persist catch batch
                        getContext().getActionEngine().runAction(getUI().getCancelButton());

                        result = true;
                        break;
                    default:

                        // other case, use cancel action
                        result = false;
                }
            } else {

                // model is not valid, ask user to loose modification or cancel
                result = askCancelEditBeforeLeaving(
                        t("tutti.editCatchBatch.askCancelEditBeforeLeaving.cancelEditCatchBatch"));

                if (result) {

                    // ok will revert any modification
                    getContext().getActionEngine().runAction(ui.getCancelButton());
                }
            }

        } else {

            // model not modify, can change tab
            result = true;
        }

        if (result) {
            editCatchesSvgHandler.clearSVG();
        }

        return result;
    }

    @Override
    public void onShowTab(int currentIndex, int newIndex) {
        registerValidators();
        editCatchesSvgHandler.initResumeSvg();
    }

    @Override
    public boolean removeTab(int i) {
        return false;
    }

    @Override
    public boolean onRemoveTab() {
        return false;
    }

    //------------------------------------------------------------------------//
    //-- Public methods                                                     --//
    //------------------------------------------------------------------------//

    public EditSpeciesBatchPanelUI getEditSpeciesBatchPanelUI(SpeciesOrBenthosBatchUISupport batchUISupport) {
        return batchUISupport instanceof BenthosBatchUISupportImpl ? ui.getBenthosTabPanel() : ui.getSpeciesTabPanel();
    }

    public void uninstallTotalRejectWeightListener() {
        getModel().removePropertyChangeListener(EditCatchesUIModel.PROPERTY_CATCH_TOTAL_REJECTED_WEIGHT, totalWeightRejectedListener);
    }

    public void installTotalRejectWeightListener() {
        getModel().addPropertyChangeListener(EditCatchesUIModel.PROPERTY_CATCH_TOTAL_REJECTED_WEIGHT, totalWeightRejectedListener);
    }

    public TuttiBeanMonitor<EditCatchesUIModel> getCatchBatchMonitor() {
        return catchBatchMonitor;
    }

    protected void registerValidators() {
        registerValidators(getValidator(),
                           ui.getSpeciesTabPanel().getEditBatchesUI().getHandler().getValidator(),
                           ui.getBenthosTabPanel().getEditBatchesUI().getHandler().getValidator(),
                           ui.getMarineLitterTabContent().getHandler().getValidator()
        );
    }

    public void setMarineLitterSelectedCard(String card) {

        JPanel panel = ui.getMarineLitterTabPanel();
        CardLayout2Ext layout = (CardLayout2Ext) panel.getLayout();
        if (!card.equals(layout.getSelected())) {

            layout.setSelected(card);

            boolean showActionPanel;

            if (MAIN_CARD.equals(card)) {

                registerValidators();
                showActionPanel = true;

            } else {

                showActionPanel = false;

                if (CREATE_BATCH_CARD.equals(card)) {

                    registerValidators(ui.getMarineLitterTabCreateBatch().getValidator());
                    String title = n("tutti.createMarineLitterBatch.title");
                    ui.getMarineLitterTabCreateBatchReminderLabel().setTitle(ui.getMarineLitterTabCreateBatchReminderLabel().getTitle() + " - " + t(title));

                }

            }

            getUI().getCreateFishingOperationActions().setVisible(showActionPanel);

        }

    }

    public void setAccidentalSelectedCard(String card) {
        setAccidentalSelectedCard(card, null);
    }

    public void setAccidentalSelectedCard(String card, Species species) {

        JPanel panel = ui.getAccidentalTabPanel();
        CardLayout2Ext layout = (CardLayout2Ext) panel.getLayout();
        if (!card.equals(layout.getSelected())) {

            layout.setSelected(card);

            boolean showActionPanel;

            if (MAIN_CARD.equals(card)) {

                registerValidators();
                showActionPanel = true;

            } else {

                showActionPanel = false;

                JXTitledPanel titlePanel = null;
                String title = "";

                if (CREATE_BATCH_CARD.equals(card)) {

                    registerValidators(ui.getAccidentalTabCreateBatch().getValidator());
                    titlePanel = ui.getAccidentalTabCreateBatchReminderLabel();
                    title = n("tutti.createAccidentalBatch.title");

                } else if (EDIT_CARACTERISTICS_CARD.equals(card)) {

                    titlePanel = ui.getAccidentalCaracteristicMapEditorReminderLabel();
                    title = n("tutti.editCaracteristics.title");

                }

                if (titlePanel != null) {
                    titlePanel.setTitle(ui.getAccidentalTabFishingOperationReminderLabel().getTitle() + " - " + t(title, decorate(species)));
                }

            }

            getUI().getCreateFishingOperationActions().setVisible(showActionPanel);

        }

    }

    private boolean blockIfNotLeavingFrequencyScreen() {

        Component selectedComponent = getUI().getTabPane().getSelectedComponent();

        if (selectedComponent != null) {

            EditSpeciesBatchPanelUI speciesTabPanel = getUI().getSpeciesTabPanel();
            EditSpeciesBatchPanelUI benthosTabPanel = getUI().getBenthosTabPanel();

            if (speciesTabPanel.equals(selectedComponent)) {

                if (EditSpeciesBatchPanelUI.EDIT_FREQUENCY_CARD.equals(speciesTabPanel.getTopPanelLayout().getSelected())) {

                    if (log.isInfoEnabled()) {
                        log.info("Species frequencies screen on, try to leave it.");
                    }
                    boolean canContinue = speciesTabPanel.getEditFrequenciesUI().leaveIfConfirmed();
                    if (!canContinue) {
                        return false;
                    }

                }

            } else if (benthosTabPanel.equals(selectedComponent)) {

                if (EditSpeciesBatchPanelUI.EDIT_FREQUENCY_CARD.equals(benthosTabPanel.getTopPanelLayout().getSelected())) {

                    if (log.isInfoEnabled()) {
                        log.info("Benthos frequencies screen on, try to leave it.");
                    }
                    boolean canContinue = benthosTabPanel.getEditFrequenciesUI().leaveIfConfirmed();
                    if (!canContinue) {
                        return false;
                    }

                }

            }

        }

        return true;

    }

}

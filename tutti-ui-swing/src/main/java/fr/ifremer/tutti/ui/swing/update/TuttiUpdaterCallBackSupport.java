package fr.ifremer.tutti.ui.swing.update;

/*
 * #%L
 * Tutti :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import fr.ifremer.tutti.persistence.ProgressionModel;
import fr.ifremer.tutti.ui.swing.TuttiUIContext;
import fr.ifremer.tutti.ui.swing.util.actions.LongActionSupport;
import fr.ifremer.tutti.ui.swing.update.module.ModuleUpdaterSupport;
import fr.ifremer.tutti.ui.swing.updater.UpdateModule;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.jaxx.application.swing.action.ApplicationActionException;
import org.nuiton.updater.ApplicationInfo;
import org.nuiton.updater.ApplicationUpdaterCallback;

import java.util.Map;
import java.util.Set;

import static org.nuiton.i18n.I18n.t;

/**
 * Created on 1/28/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.12.1
 */
public abstract class TuttiUpdaterCallBackSupport implements ApplicationUpdaterCallback {

    /** Logger. */
    private static final Log log = LogFactory.getLog(TuttiReportUpdaterCallBack.class);

    protected final TuttiUIContext context;

    protected ProgressionModel progressionModel;

    protected final LongActionSupport action;

    protected final String url;

    protected final Map<UpdateModule, ModuleUpdaterSupport> allUpdaters;

    /**
     * Modules to update.
     */
    protected Set<UpdateModule> modulesToUpdate;

    protected boolean applicationUpdated;

    public TuttiUpdaterCallBackSupport(String url, Map<UpdateModule, ModuleUpdaterSupport> allUpdaters, LongActionSupport action,
                                       ProgressionModel progressionModel) {
        this.url = url;
        this.action = action;
        this.allUpdaters = allUpdaters;
        this.context = action.getContext();
        this.progressionModel = progressionModel;
    }

    public void setModulesToUpdate(UpdateModule... modulesToUpdate) {
        this.modulesToUpdate = Sets.newHashSet(modulesToUpdate);
    }

    public boolean isApplicationUpdated() {
        return applicationUpdated;
    }

    public String getUrl() {
        return url;
    }

    public ProgressionModel getProgressionModel() {
        return progressionModel;
    }

    @Override
    public Map<String, ApplicationInfo> updateToDo(Map<String, ApplicationInfo> appToUpdate) {

        Map<String, ApplicationInfo> result = Maps.newHashMap();

        for (UpdateModule updateModule : modulesToUpdate) {
            ModuleUpdaterSupport moduleUpdaterSupport = getModuleUpdater(updateModule);
            ApplicationInfo info = moduleUpdaterSupport.updateToDo(context, appToUpdate);
            if (info != null) {
                result.put(info.name, info);
            }
        }

        return result;

    }

    @Override
    public void startUpdate(ApplicationInfo info) {

        for (UpdateModule updateModule : modulesToUpdate) {

            ModuleUpdaterSupport moduleUpdaterSupport = getModuleUpdater(updateModule);
            if (moduleUpdaterSupport.matchUpdate(info)) {

                String moduleLabel = moduleUpdaterSupport.getLabel();
                String message = t("tutti.applicationUpdater.startUpdate", moduleLabel, info.newVersion);
                if (log.isInfoEnabled()) {
                    log.info(message);
                }
                progressionModel.setMessage(message);

            }

        }

    }

    @Override
    public void updateDone(Map<String, ApplicationInfo> appToUpdate, Map<String, Exception> appUpdateError) {

        boolean doRestart = false;
        for (UpdateModule updateModule : modulesToUpdate) {

            ModuleUpdaterSupport moduleUpdaterSupport = getModuleUpdater(updateModule);
            try {

                boolean updateDone = moduleUpdaterSupport.updateDone(context, appToUpdate, appUpdateError);
                if (updateDone) {
                    doRestart = true;
                }

            } catch (ApplicationUpdateException e) {
                throw ApplicationActionException.propagateError(action, e);
            }
        }

        if (doRestart) {

            applicationUpdated = true;
        }

    }

    @Override
    public void aborted(String propertiesURL, Exception eee) {
        if (log.isErrorEnabled()) {
            log.error("Could not update from " + propertiesURL, eee);
        }
        throw ApplicationActionException.propagateError(action, eee);
    }

    protected ModuleUpdaterSupport getModuleUpdater(UpdateModule updateModule) {
        return allUpdaters.get(updateModule);
    }

}

<!--
  #%L
  Tutti :: UI
  %%
  Copyright (C) 2012 - 2014 Ifremer
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation, either version 3 of the 
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public 
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/gpl-3.0.html>.
  #L%
  -->
<JPanel id='editSpeciesFrequenciesTopPanel' layout='{new BorderLayout()}' decorator='help'
        implements='fr.ifremer.tutti.ui.swing.util.TuttiUI&lt;SpeciesFrequencyUIModel, SpeciesFrequencyUIHandler&gt;'
        onComponentResized="JAXXWidgetUtil.setComponentWidth(lengthStepCaracteristicComboBox, getLengthStepPanelPreferedWidth())  ;">

  <import>
    fr.ifremer.tutti.persistence.entities.data.CopyIndividualObservationMode
    fr.ifremer.tutti.persistence.entities.referential.Caracteristic

    fr.ifremer.tutti.ui.swing.TuttiHelpBroker
    fr.ifremer.tutti.ui.swing.content.operation.catches.species.frequency.FrequencyConfigurationMode
    fr.ifremer.tutti.ui.swing.content.operation.catches.species.frequency.actions.AddSpeciesFrequencyLengthStepCaracteristicAction
    fr.ifremer.tutti.ui.swing.content.operation.catches.species.frequency.actions.CancelEditSpeciesFrequencyAction
    fr.ifremer.tutti.ui.swing.content.operation.catches.species.frequency.actions.GenerateSpeciesFrequencyLengthStepsAction
    fr.ifremer.tutti.ui.swing.content.operation.catches.species.frequency.actions.ResetSpeciesFrequencyAction
    fr.ifremer.tutti.ui.swing.content.operation.catches.species.frequency.actions.SaveAndStaySpeciesFrequencyAction
    fr.ifremer.tutti.ui.swing.content.operation.catches.species.frequency.actions.SaveAndCloseSpeciesFrequencyAction
    fr.ifremer.tutti.ui.swing.content.operation.catches.species.frequency.actions.SaveAndContinueSpeciesFrequencyAction
    fr.ifremer.tutti.ui.swing.content.operation.catches.species.frequency.actions.SearchSpeciesInPrevOrNextCatchesAction;
    fr.ifremer.tutti.ui.swing.util.TuttiUI
    fr.ifremer.tutti.ui.swing.util.TuttiUIUtil
    fr.ifremer.tutti.ui.swing.util.WideDataBeanFilterableComboBox
    fr.ifremer.tutti.ui.swing.util.caracteristics.CaracteristicMapEditorUI
    fr.ifremer.tutti.ui.swing.util.computable.ComputableDataEditor

    jaxx.runtime.swing.CardLayout2Ext
    jaxx.runtime.swing.JAXXWidgetUtil
    jaxx.runtime.swing.editor.bean.BeanFilterableComboBox
    org.nuiton.jaxx.widgets.number.NumberEditor

    org.jdesktop.swingx.JXTable
    org.jdesktop.swingx.JXTitledPanel

    javax.swing.ListSelectionModel
    javax.swing.SwingConstants

    java.awt.Color

    static org.nuiton.i18n.I18n.t
    static jaxx.runtime.SwingUtil.getStringValue
  </import>

  <script><![CDATA[

// Pour savoir sur quel modèle on est (espèce ou benthos)
private String speciesOrBenthosContext;

public SpeciesFrequencyUI(TuttiUI<?,?> parentUI, String speciesOrBenthosContext) {
    TuttiUIUtil.setParentUI(this, parentUI);
    this.speciesOrBenthosContext = speciesOrBenthosContext;
}

public String getSpeciesOrBenthosContext() { return speciesOrBenthosContext; }

protected int getLengthStepPanelPreferedWidth() {
    return getWidth() - frequencyModePanel.getPreferredSize().width - histogramPanel.getPreferredSize().width;
}

public boolean leaveIfConfirmed() { return handler.leaveIfConfirmed(); }

  ]]></script>

  <SpeciesFrequencyUIModel id='model' initializer='getContextValue(SpeciesFrequencyUIModel.class)'/>

  <BeanValidator id='validator' bean='model' uiClass='jaxx.runtime.validator.swing.ui.ImageValidationUI'>
    <field name='lengthStepCaracteristic' component='lengthStepCaracteristicComboBox'/>
    <field name='step' component='stepField'/>
    <field name='rows' component='tableScrollPane'/>
    <field name='totalWeight' component='totalWeightField'/>
    <field name='nonEmptyIndividualObservationRowsInError' component='obsPanel'/>
  </BeanValidator>

  <TuttiHelpBroker id='broker' constructorParams='"tutti.editSpeciesFrequencies.help"'/>

  <CardLayout2Ext id='modeConfigurationLayout' constructorParams='this, "modeConfigurationPanel"'/>

  <CardLayout2Ext id='dataFieldLayout' constructorParams='this, "dataFieldPanel"'/>

  <CardLayout2Ext id='obsPanelLayout' constructorParams='this, "obsPanel"'/>

  <AddSpeciesFrequencyLengthStepCaracteristicAction id="addLengthStepCaracteristicAction" constructorParams="this"/>
  <GenerateSpeciesFrequencyLengthStepsAction id="generateLengthStepsAction" constructorParams="this"/>
  <SaveAndStaySpeciesFrequencyAction id="saveAndStayAction" constructorParams="this"/>
  <SaveAndCloseSpeciesFrequencyAction id="saveAndCloseAction" constructorParams="this"/>
  <SaveAndContinueSpeciesFrequencyAction id="saveAndContinueAction" constructorParams="this"/>

  <SearchSpeciesInPrevOrNextCatchesAction id="searchSpeciesInPrevCatchesAction" constructorParams="this, true"/>
  <SearchSpeciesInPrevOrNextCatchesAction id="searchSpeciesInNextCatchesAction" constructorParams="this, false"/>

  <JToolBar id='speciesFrequencyTabToolBar'>
    <JMenuBar id='menu'>
      <JMenu id='menuAction'>
        <JMenuItem id='searchSpeciesInPrevCatchesButton'/>
        <JMenuItem id='searchSpeciesInNextCatchesButton'/>
        <JMenu id='menuImportMultiPost'>
          <JMenuItem id='importMultiPostFullButton'/>
          <JMenuItem id='importMultiPostOnlyFrequenciesButton'/>
          <JMenuItem id='importMultiPostOnlyIndividualObservationsButton'/>
        </JMenu>
        <JMenu id='menuExportMultiPost'>
          <JMenuItem id='exportMultiPostFullButton'/>
          <JMenuItem id='exportMultiPostOnlyFrequenciesButton'/>
          <JMenuItem id='exportMultiPostOnlyIndividualObservationsButton'/>
        </JMenu>
      </JMenu>
    </JMenuBar>
  </JToolBar>

  <JToolBar id="frequenciesToolBar">
    <!-- use actionperformed otherwise the event is triggered twice when the confirm dialog is shown -->
    <JCheckBox id='copyRtpWeightsCheckBox' onActionPerformed='model.setCopyRtpWeights(copyRtpWeightsCheckBox.isSelected())'/>
    <JButton id='graphAverageWeightButton'/>
  </JToolBar>

  <JToolBar id="copyToSizeToolBar">
    <JLabel id='copyToSizeTableLabel'/>
    <JRadioButton id='copyAllButton'
                  onActionPerformed='handler.setCopyIndividualObservationMode(CopyIndividualObservationMode.ALL)'/>
    <JRadioButton id='copyNothingButton'
                  onActionPerformed='handler.setCopyIndividualObservationMode(CopyIndividualObservationMode.NOTHING)'/>
    <JRadioButton id='copySizesButton'
                  onActionPerformed='handler.setCopyIndividualObservationMode(CopyIndividualObservationMode.SIZE)'/>
  </JToolBar>

  <JPopupMenu id='obsTablePopup'>
    <JMenuItem id='deleteObsRowsMenu'/>
    <JMenuItem id='editSampleCodeMenu'/>
    <JMenuItem id='deleteSampleCodeMenu'/>
  </JPopupMenu>

  <Table id='configurationPanel' fill="both" anchor="west" constraints='BorderLayout.CENTER'>

    <row>

      <cell columns="2">
        <HBox id='frequencyModePanel'
              horizontalAlignment='{SwingConstants.LEFT}'
              verticalAlignment='{SwingConstants.CENTER}'>
          <JRadioButton id='simpleCountingModeButton'
                        onActionPerformed='model.setConfigurationMode(FrequencyConfigurationMode.SIMPLE_COUNTING)'/>
          <JRadioButton id='frequencyModeButton'
                        onActionPerformed='model.setConfigurationMode(FrequencyConfigurationMode.FREQUENCIES)'/>
        </HBox>
      </cell>

      <cell rows="2">
        <!-- length step caracteristic -->
        <Table id="lengthStepCaracteristicPanel" anchor="west" styleClass="visibleIfFrequencies" fill="both">

          <row>
            <cell>
              <JLabel id='lengthStepCaracteristicLabel'/>
            </cell>
            <cell>
              <WideDataBeanFilterableComboBox id='lengthStepCaracteristicComboBox'
                                              constructorParams='this'
                                              genericType='Caracteristic'/>
            </cell>
            <cell>
              <JButton id='addLengthStepCaracteristicButton'/>
            </cell>
          </row>
          <row>
            <cell>
              <JLabel id='stepLabel'/>
            </cell>
            <cell weightx="1" columns="2">
              <NumberEditor id='stepField' constructorParams='this'/>
            </cell>
          </row>

        </Table>
      </cell>

      <cell rows="4">
        <JPanel id="histogramPanel" layout="{new BorderLayout()}" styleClass="visibleIfFrequencies"/>
      </cell>
    </row>

    <row>
      <cell rows="3">
        <VBox id='modePanel' verticalAlignment='{SwingConstants.CENTER}' styleClass="visibleIfFrequencies">
          <JRadioButton id='autoGenModeButton'
                        onActionPerformed='model.setFrequenciesConfigurationMode(FrequencyConfigurationMode.AUTO_GEN)'/>
          <JRadioButton id='rafaleModeButton'
                        onActionPerformed='model.setFrequenciesConfigurationMode(FrequencyConfigurationMode.RAFALE)'/>
        </VBox>
      </cell>

      <cell rows="3">
        <JPanel id='modeConfigurationPanel' styleClass="visibleIfFrequencies">
          <Table  id='autoGenModePanel' constraints='"autoGenMode"' fill="both">
            <!-- Min step-->
            <row>
              <cell anchor='west'>
                <JLabel id='minStepLabel'/>
              </cell>
              <cell weightx='1.0'>
                <NumberEditor id='minStepField' constructorParams='this'/>
              </cell>
            </row>
            <!-- Max step-->
            <row>
              <cell anchor='west'>
                <JLabel id='maxStepLabel'/>
              </cell>
              <cell weightx='1.0'>
                <NumberEditor id='maxStepField' constructorParams='this'/>
              </cell>
            </row>
            <!-- Actions -->
            <row>
              <cell columns='2'>
                <JPanel layout='{new GridLayout(1, 0)}'>
                  <JButton id='generateButton'/>
                </JPanel>
              </cell>
            </row>
          </Table>
          <Table id='rafaleModePanel' constraints='"rafaleMode"' fill="both">
            <!-- Rafale step-->
            <row>
              <cell weightx='1.0'>
                <JLabel id='rafaleStepLabel'/>
              </cell>
            </row>
            <row>
              <cell weightx='1.0'>
                <NumberEditor id='rafaleStepField' constructorParams='this'/>
              </cell>
            </row>
            <row>
              <cell weightx='1.0'>
                <JCheckBox id='addIndividualObservationCheckBox'
                           onItemStateChanged='handler.setBoolean(event, "addIndividualObservationOnRafale")'/>
              </cell>
            </row>
          </Table>
        </JPanel>
      </cell>
    </row>

    <row>
      <cell anchor="center">
        <JLabel id="samplingWarningLabel" styleClass="visibleIfFrequencies"/>
      </cell>
    </row>

    <row>
      <cell anchor="center">
        <JLabel id="samplingResumeLabel" styleClass="visibleIfFrequencies"/>
      </cell>
    </row>

    <row>
      <cell columns="4">
        <JSeparator styleClass="visibleIfFrequencies"/>
      </cell>
    </row>

    <row weighty="1">
      <cell columns="4" weightx="1">
        <JPanel id='dataFieldPanel'>
          <JSplitPane constraints='"lengthCaracteristicPmfm"' id="firstSplitPane">
            <JXTitledPanel id="frequenciesTitlePanel">
              <JPanel id='frequenciesPanel' layout="{new BorderLayout()}">
                <Table constraints='BorderLayout.NORTH'
                       fill="both"
                       id="lengthstepSettingsBlock">
                  <row>
                    <cell anchor="east">
                      <JLabel id='totalNumberLabel'/>
                    </cell>
                    <cell weightx="1">
                      <JTextField id='totalNumberField'/>
                    </cell>
                  </row>
                  <row>
                    <cell>
                      <JLabel id='totalWeightLabel' styleClass='labelWithUnit'/>
                    </cell>
                    <cell weightx="1">
                      <ComputableDataEditor id='totalWeightField' genericType="Float" constructorParams='this'/>
                    </cell>
                  </row>
                </Table>
                <JSplitPane id="secondSplitPane" constraints='BorderLayout.CENTER'>
                  <JScrollPane id='logsScrollPane'>
                    <JXTable id='logsTable'/>
                  </JScrollPane>
                  <JScrollPane id='tableScrollPane'>
                    <JXTable id='table'/>
                  </JScrollPane>
                </JSplitPane>
              </JPanel>
            </JXTitledPanel>
            <JPanel id="obsPanel">
              <JXTitledPanel id="obsTableTitlePanel" constraints="SpeciesFrequencyUIHandler.OBS_TABLE_CARD">
                <JScrollPane>
                  <JXTable id='obsTable'
                           onMouseClicked='handler.autoSelectRowInTable(event, obsTablePopup)'
                           onKeyPressed='handler.openRowMenu(event, obsTablePopup)'/>
                </JScrollPane>
              </JXTitledPanel>
              <JXTitledPanel id='obsCaracteristicMapEditorReminderLabel'
                             constraints='SpeciesFrequencyUIHandler.EDIT_CARACTERISTICS_CARD'>
                <CaracteristicMapEditorUI
                  id='obsCaracteristicCaracteristicMapEditor'
                  constructorParams='this'/>
              </JXTitledPanel>
            </JPanel>
          </JSplitPane>

          <Table id="simpleCountingNumberPanel" constraints='"noLengthCaracteristicPmfm"' fill='horizontal'>
            <row>
              <cell columns='2'>
                <JPanel id='dataInFrequenciesWarningContainer' layout='{new BorderLayout(10, 10)}'>
                  <JLabel id='dataInFrequenciesWarning' constraints='BorderLayout.CENTER'/>
                </JPanel>
              </cell>
            </row>
            <row>
              <cell>
                <JLabel id='simpleCountingNumberLabel'/>
              </cell>
              <cell weightx='1'>
                <NumberEditor id='simpleCountingNumberField' constructorParams='this'/>
              </cell>
            </row>
            <row>
              <cell>
                <JLabel id='simpleCountingWeightLabel'/>
              </cell>
              <cell weightx='1'>
                <NumberEditor id='simpleCountingWeightField' constructorParams='this'/>
              </cell>
            </row>
          </Table>
        </JPanel>
      </cell>
    </row>
  </Table>

  <!-- actions -->
  <JPanel id='actionPanel' layout='{new GridLayout(1, 0)}' constraints='BorderLayout.SOUTH' styleClass="buttonPanel">
    <JButton id='cancelButton'/>
    <JButton id='resetButton'/>
    <JButton id='saveAndStayButton'/>
    <JButton id='saveAndContinueButton'/>
    <JButton id='saveAndCloseButton'/>
  </JPanel>

</JPanel>
package fr.ifremer.tutti.ui.swing.content.protocol.actions;

/*
 * #%L
 * Tutti :: UI
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import fr.ifremer.tutti.persistence.entities.referential.Species;
import fr.ifremer.tutti.persistence.entities.referential.Speciess;
import fr.ifremer.tutti.ui.swing.content.protocol.EditProtocolSpeciesRowModel;
import fr.ifremer.tutti.ui.swing.content.protocol.EditProtocolSpeciesTableModel;
import fr.ifremer.tutti.ui.swing.content.protocol.EditProtocolUI;
import fr.ifremer.tutti.ui.swing.content.protocol.EditProtocolUIHandler;
import fr.ifremer.tutti.ui.swing.content.protocol.EditProtocolUIModel;
import fr.ifremer.tutti.ui.swing.content.protocol.calcifiedpiecessampling.CalcifiedPiecesSamplingEditorRowModel;
import fr.ifremer.tutti.ui.swing.util.actions.LongActionSupport;
import jaxx.runtime.SwingUtil;

import javax.swing.JTable;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

import static org.nuiton.i18n.I18n.t;

/**
 * To remove all the selected protocolSpecies rows from protocol.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.0
 */
public class RemoveSpeciesProtocolAction extends LongActionSupport<EditProtocolUIModel, EditProtocolUI, EditProtocolUIHandler> {

    /**
     * Set of removed protocolSpecies.
     *
     * @since 2.8
     */
    protected Set<Species> removedSpecies;

    /**
     * Set of removed rows.
     *
     * @since 2.8
     */
    protected Set<EditProtocolSpeciesRowModel> removedRows;

    /**
     * calcified pieces sampling rows to delete
     *
     * @since 4.5
     */
    protected Collection<CalcifiedPiecesSamplingEditorRowModel> cpsRowsToDelete;

    public RemoveSpeciesProtocolAction(EditProtocolUIHandler handler) {
        super(handler, false);
    }

    @Override
    public boolean prepareAction() throws Exception {
        boolean result = super.prepareAction();

        if (result) {
            JTable table = handler.getSpeciesTable();

            // need to have a selection
            Preconditions.checkState(!table.getSelectionModel().isSelectionEmpty());

            EditProtocolSpeciesTableModel tableModel =
                    (EditProtocolSpeciesTableModel) table.getModel();

            EditProtocolUIModel model = getModel();

            removedSpecies = Sets.newHashSet();
            removedRows = Sets.newHashSet();

            for (Integer rowIndex : SwingUtil.getSelectedModelRows(table)) {

                // get row to remove
                EditProtocolSpeciesRowModel selectedRow =
                        tableModel.getEntry(rowIndex);

                // re-add all synonym of this taxon to the species / benthos combobox
                Species species = selectedRow.getSpecies();
                removedSpecies.add(species);

                Integer taxonId = species.getReferenceTaxonId();
                List<Species> allSynonyms = Lists.newArrayList(
                        model.getAllSynonyms(String.valueOf(taxonId)));
                allSynonyms.remove(species);
                model.getAllSynonyms().addAll(allSynonyms);

                // mark row to be removed at the very last moment
                removedRows.add(selectedRow);
            }

            List<CalcifiedPiecesSamplingEditorRowModel> cpsRows = getModel().getCpsRows();
            cpsRowsToDelete = cpsRows.stream()
                                     .filter(r -> removedSpecies.contains(r.getProtocolSpecies().getSpecies()))
                                     .collect(Collectors.toList());

            if (!cpsRowsToDelete.isEmpty()) {

                result = askBeforeDelete(t("tutti.editProtocol.action.removeSpeciesProtocol.removeCpsRows.title"),
                                         t("tutti.editProtocol.action.removeSpeciesProtocol.removeCpsRows.message"));

            }
        }

        return result;
    }

    @Override
    public void doAction() throws Exception {

        // reorder the list by name, otherwise,
        // all the species without a reftax code will be at the end
        Collections.sort(getModel().getAllSynonyms(), Speciess.SPECIES_BY_NAME_COMPARATOR);
    }

    @Override
    public void postSuccessAction() {
        super.postSuccessAction();

        // update comboboxes
        getUI().getBenthosComboBox().addItems(removedSpecies);
        getUI().getSpeciesComboBox().addItems(removedSpecies);

        // remove all rows from model
        getModel().getSpeciesRow().removeAll(removedRows);

        // remove the lengthstep pmfm of the rows from the used lengthstep pmfm
        getModel().getLengthStepPmfmUsed().removeCaracteristics(removedRows.stream()
                                                                           .map(EditProtocolSpeciesRowModel::getLengthStepPmfm)
                                                                           .filter(Objects::nonNull)
                                                                           .collect(Collectors.toList()));

        // remove the maturities of the rows from the used maturities
        getModel().getMaturityPmfmUsed().removeCaracteristics(removedRows.stream()
                                                                         .map(EditProtocolSpeciesRowModel::getMaturityPmfm)
                                                                         .filter(Objects::nonNull)
                                                                         .collect(Collectors.toList()));

        // remove the protocolSpecies from the cps table or combobox
        if (!cpsRowsToDelete.isEmpty()) {
            handler.deleteCpsRows(cpsRowsToDelete);
        }

        // fire table data changed
        handler.getSpeciesTableModel().fireTableDataChanged();

        // clear table selection
        handler.getSpeciesTable().clearSelection();

        // notify user
        sendMessage(t("tutti.flash.info.species.remove.from.protocol"));
    }

}

package fr.ifremer.tutti.ui.swing.util.editor;

/*
 * #%L
 * Tutti :: UI
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Lists;
import fr.ifremer.tutti.persistence.entities.TuttiEntities;
import fr.ifremer.tutti.persistence.entities.referential.TuttiLocation;
import fr.ifremer.tutti.service.DecoratorService;
import fr.ifremer.tutti.ui.swing.TuttiUIContext;
import jaxx.runtime.swing.editor.bean.BeanFilterableComboBox;
import org.jdesktop.swingx.renderer.DefaultTableRenderer;
import org.nuiton.decorator.Decorator;
import org.nuiton.decorator.JXPathDecorator;

import javax.swing.DefaultCellEditor;
import javax.swing.DefaultListCellRenderer;
import javax.swing.JComboBox;
import javax.swing.JList;
import javax.swing.JTable;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.MouseEvent;
import java.util.EventObject;
import java.util.List;
import java.util.Map;

/**
 * To edit a {@link TuttiLocation}.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.4
 */
public class TuttiLocationTableCell {

    protected Decorator<TuttiLocation> decorator;

    protected List<TuttiLocation> entities;

    protected Map<String, TuttiLocation> entityMap;

    public TuttiLocationTableCell(TuttiUIContext context) {
        DecoratorService decoratorService = context.getDecoratorService();
        decorator = decoratorService.getDecoratorByType(TuttiLocation.class);

        entities = Lists.newArrayList(context.getPersistenceService().getAllCountry());
        entityMap = TuttiEntities.splitById(entities);
    }

    public TableCellEditor getNewTableCellEditor() {
        return new TuttiLocationTableCellEditor();
    }

    public TableCellRenderer getNewTableCellRenderer() {
        return new TuttiLocationTableCellRenderer();
    }

    protected class TuttiLocationTableCellEditor extends DefaultCellEditor {

        private static final long serialVersionUID = 1L;

        protected TuttiLocationTableCellEditor() {
            super(new JComboBox());

            final BeanFilterableComboBox<TuttiLocation> component = new BeanFilterableComboBox<>();
            component.setI18nPrefix("tutti.property.");
            component.setShowReset(true);
            component.setBeanType(TuttiLocation.class);
            setClickCountToStart(1);

            editorComponent = component;
            delegate = new DefaultCellEditor.EditorDelegate() {
                private static final long serialVersionUID = 1L;

                @Override
                public void setValue(Object value) {
                    if (value != null && String.class.isInstance(value)) {
                        value = entityMap.get(value);
                    }
                    component.setSelectedItem(value);
                }

                @Override
                public Object getCellEditorValue() {
                    String result = null;
                    Object selectedItem = component.getSelectedItem();
                    if (TuttiLocation.class.isInstance(selectedItem)) {
                        TuttiLocation entity = (TuttiLocation) component.getSelectedItem();
                        if (entity != null) {
                            result = entity.getId();
                        }
                    }
                    return result;
                }

                @Override
                public boolean shouldSelectCell(EventObject anEvent) {
                    if (anEvent instanceof MouseEvent) {
                        MouseEvent e = (MouseEvent) anEvent;
                        return e.getID() != MouseEvent.MOUSE_DRAGGED;
                    }
                    return true;
                }

                @Override
                public boolean stopCellEditing() {
                    if (component.isEditable()) {
                        // Commit edited value.
                        component.getCombobox().actionPerformed(
                                new ActionEvent(TuttiLocationTableCellEditor.this, 0, ""));
                    }
                    return super.stopCellEditing();
                }
            };

            component.init((JXPathDecorator<TuttiLocation>) decorator, entities);
        }

        class TuttiLocationListCellRenderer extends DefaultListCellRenderer {

            private static final long serialVersionUID = 1L;

            @Override
            public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
                return super.getListCellRendererComponent(list, decorator.toString(value), index, isSelected, cellHasFocus);
            }
        }
    }

    protected class TuttiLocationTableCellRenderer extends DefaultTableRenderer {

        private static final long serialVersionUID = 1L;

        @Override
        public Component getTableCellRendererComponent(JTable table, Object value,
                                                       boolean isSelected, boolean hasFocus,
                                                       int row, int column) {

            String entityId = String.valueOf(value);
            TuttiLocation entity = entityMap.get(entityId);
            return super.getTableCellRendererComponent(table, decorator.toString(entity), isSelected, hasFocus, row, column);
        }
    }
}

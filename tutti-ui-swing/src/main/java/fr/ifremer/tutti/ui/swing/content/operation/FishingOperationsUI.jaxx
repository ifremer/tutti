<!--
  #%L
  Tutti :: UI
  %%
  Copyright (C) 2012 - 2014 Ifremer
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation, either version 3 of the 
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public 
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/gpl-3.0.html>.
  #L%
  -->
<JPanel id='fishingOperationsTopPanel' layout='{new BorderLayout()}'
        decorator='help'
        implements='fr.ifremer.tutti.ui.swing.util.TuttiUI&lt;FishingOperationsUIModel, FishingOperationsUIHandler&gt;'>

  <import>
    fr.ifremer.tutti.persistence.entities.data.FishingOperation

    fr.ifremer.tutti.ui.swing.TuttiHelpBroker
    fr.ifremer.tutti.ui.swing.TuttiUIContext
    fr.ifremer.tutti.ui.swing.content.operation.catches.EditCatchesUI
    fr.ifremer.tutti.ui.swing.util.TuttiUI
    fr.ifremer.tutti.ui.swing.util.TuttiUIUtil

    jaxx.runtime.swing.editor.bean.BeanFilterableComboBox

    java.awt.GridLayout
    java.awt.Dimension

    static org.nuiton.i18n.I18n.t
  </import>

  <script><![CDATA[
public FishingOperationsUI(TuttiUI parentUI) {
    TuttiUIUtil.setParentUI(this, parentUI);
}
  ]]></script>

  <FishingOperationsUIModel id='model'
                            initializer='getContextValue(FishingOperationsUIModel.class)'/>

  <TuttiHelpBroker id='broker'
                   constructorParams='"tutti.fishingOperations.help"'/>

  <Table fill='both' id='topPanel' constraints='BorderLayout.NORTH'>

    <!-- Cruise fishingOperations -->
    <row>
      <cell anchor='west' weightx='1.0'>
        <BeanFilterableComboBox id='fishingOperationComboBox'
                                constructorParams='this'
                                genericType='FishingOperation'/>
      </cell>
      <cell anchor='east'>
        <JPanel layout="{new GridLayout()}">
          <JButton id='newFishingOperationButton'/>
          <JButton id='deleteFishingOperationButton'/>
        </JPanel>
      </cell>
    </row>
    <row>
      <cell columns='2'>
        <JPanel id='warningContainer' layout='{new BorderLayout(10, 10)}'>
          <JLabel id='warningLabel' constraints='BorderLayout.CENTER'/>
        </JPanel>
      </cell>
    </row>
  </Table>

  <!-- Current selected fishingOperation -->
  <JTabbedPane id='tabPane'>
    <tab id='fishingOperationTab' title='tutti.label.tab.fishingOperation'>
      <EditFishingOperationUI id='fishingOperationTabContent'
                              constructorParams='this'/>
    </tab>
    <tab id='catchesTab' title='tutti.label.tab.catches'>
      <EditCatchesUI id='catchesTabContent' constructorParams='this'/>
    </tab>
  </JTabbedPane>

  <!-- When no fishing operation selected -->
  <JLabel id='noTraitPane' constraints='BorderLayout.CENTER'/>

</JPanel>

package fr.ifremer.tutti.ui.swing.content.operation.catches.species.edit;

/*
 * #%L
 * Tutti :: UI
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Multimap;
import fr.ifremer.tutti.persistence.entities.referential.CaracteristicQualitativeValue;
import fr.ifremer.tutti.persistence.entities.referential.Species;
import fr.ifremer.tutti.type.WeightUnit;
import fr.ifremer.tutti.ui.swing.content.operation.catches.AbstractTuttiBatchUIModel;
import fr.ifremer.tutti.ui.swing.content.operation.catches.EditCatchesUIModel;
import fr.ifremer.tutti.ui.swing.content.operation.catches.species.SpeciesOrBenthosBatchUISupport;
import fr.ifremer.tutti.ui.swing.content.operation.catches.species.frequency.SpeciesFrequencyRowModel;
import fr.ifremer.tutti.ui.swing.util.computable.ComputableData;
import fr.ifremer.tutti.util.Numbers;
import org.apache.commons.collections4.CollectionUtils;
import org.nuiton.jaxx.application.swing.tab.TabContentModel;

import java.util.List;
import java.util.Map;

/**
 * @author Tony Chemit - chemit@codelutin.com
 * @since 0.1
 */
public class SpeciesBatchUIModel extends AbstractTuttiBatchUIModel<SpeciesBatchRowModel, SpeciesBatchUIModel>
        implements TabContentModel, SpeciesSortableRowModel {

    private static final long serialVersionUID = 1L;

    public static final String PROPERTY_SPLIT_BATCH_ENABLED = "splitBatchEnabled";

    public static final String PROPERTY_CHANGE_SAMPLE_CATEGORY_ENABLED = "changeSampleCategoryEnabled";

    public static final String PROPERTY_ADD_SAMPLE_CATEGORY_ENABLED = "addSampleCategoryEnabled";

    public static final String PROPERTY_REMOVE_SUB_BATCH_ENABLED = "removeSubBatchEnabled";

    public static final String PROPERTY_RENAME_BATCH_ENABLED = "renameBatchEnabled";

    public static final String PROPERTY_REMOVE_BATCH_ENABLED = "removeBatchEnabled";

    public static final String PROPERTY_CREATE_MELAG_ENABLED = "createMelagEnabled";

    public static final String PROPERTY_EDIT_FREQUENCIES_ENABLED = "editFrequenciesEnabled";

    public static final String PROPERTY_TABLE_VIEW_MODE = "tableViewMode";

    public static final String PROPERTY_TABLE_VIEW_MODE_ALL = "tableViewModeAll";

    public static final String PROPERTY_TABLE_VIEW_MODE_LEAF = "tableViewModeLeaf";

    public static final String PROPERTY_TABLE_VIEW_MODE_ROOT = "tableViewModeRoot";

    public static final String PROPERTY_ROOT_NUMBER = "rootNumber";

    public static final String PROPERTY_LEAF_NUMBER = "leafNumber";

    /**
     * Can user split a selected species batch ?
     *
     * @since 0.3
     */
    protected boolean splitBatchEnabled;

    /**
     * Can user change a sample category of the selected species batch ?
     *
     * @since 2.6
     */
    protected boolean changeSampleCategoryEnabled;

    /**
     * Can user add a/some missing sample category of the selected species batch ?
     *
     * @since 2.6
     */
    protected boolean addSampleCategoryEnabled;

    /**
     * Can user remove a selected species batch?
     *
     * @since 0.3
     */
    protected boolean removeBatchEnabled;

    /**
     * Can user remove a selected species sub batches?
     *
     * @since 0.3
     */
    protected boolean removeSubBatchEnabled;

    /**
     * Can user create a melag from the selected species?
     *
     * @since 0.3
     */
    protected boolean createMelagEnabled = true;

    /**
     * Can user rename the selected species?
     *
     * @since 0.3
     */
    protected boolean renameBatchEnabled;

    /**
     * Can user edit frequencies for the selected species?
     *
     * @since 2.3
     */
    private boolean editFrequenciesEnabled;

    /**
     * What to show in the table.
     *
     * @since 0.3
     */
    protected TableViewMode tableViewMode;

    /** @since 1.0 */
    protected int rootNumber;

    /** @since 1.0 */
    protected int leafNumber;

    /**
     * How to sort table.
     *
     * @since 3.0
     */
    protected SpeciesSortMode speciesSortMode;

    /**
     * Which decorator index for species column decorate.
     *
     * @since 3.0
     */
    protected int speciesDecoratorContextIndex;

    /**
     * Délégation de différences entre species et benthos.
     *
     * @since 4.5
     */
    protected final SpeciesOrBenthosBatchUISupport speciesOrBenthosBatchUISupport;

    public SpeciesBatchUIModel(SpeciesOrBenthosBatchUISupport speciesOrBenthosBatchUISupport) {
        super(speciesOrBenthosBatchUISupport,
              Iterables.toArray(speciesOrBenthosBatchUISupport.getCatchesUIModelPropertiesMapping().keySet(), String.class));
//              EditCatchesUIModel.PROPERTY_SPECIES_TOTAL_COMPUTED_WEIGHT,
//              EditCatchesUIModel.PROPERTY_SPECIES_TOTAL_SORTED_WEIGHT,
//              EditCatchesUIModel.PROPERTY_SPECIES_TOTAL_UNSORTED_COMPUTED_WEIGHT,
//              EditCatchesUIModel.PROPERTY_SPECIES_TOTAL_SAMPLE_SORTED_COMPUTED_WEIGHT,
//              EditCatchesUIModel.PROPERTY_SPECIES_TOTAL_INERT_WEIGHT,
//              EditCatchesUIModel.PROPERTY_SPECIES_TOTAL_LIVING_NOT_ITEMIZED_WEIGHT);
        this.speciesOrBenthosBatchUISupport = speciesOrBenthosBatchUISupport;
    }

    public SpeciesOrBenthosBatchUISupport getSpeciesOrBenthosBatchUISupport() {
        return speciesOrBenthosBatchUISupport;
    }

    public EditCatchesUIModel getCatchesUIModel() {
        return speciesOrBenthosBatchUISupport.getCatchesUIModel();
    }

    public WeightUnit getWeightUnit() {
        return speciesOrBenthosBatchUISupport.getWeightUnit();
    }

    public boolean canPupitriImport() {
        return speciesOrBenthosBatchUISupport.canPupitriImport();
    }

    public boolean canPsionImport() {
        return speciesOrBenthosBatchUISupport.canPsionImport();
    }

    public boolean canBigfinImport() {
        return speciesOrBenthosBatchUISupport.canBigfinImport();
    }

    public Float getTotalComputedWeight() {
        return speciesOrBenthosBatchUISupport.getTotalComputedWeight();
    }

    public void setTotalComputedWeight(Float totalComputedWeight) {
        speciesOrBenthosBatchUISupport.setTotalComputedWeight(totalComputedWeight);
    }

    public ComputableData<Float> getTotalSortedComputedOrNotWeight() {
        return speciesOrBenthosBatchUISupport.getTotalSortedComputedOrNotWeight();
    }

    public Float getTotalSortedWeight() {
        return speciesOrBenthosBatchUISupport.getTotalSortedWeight();
    }

    public void setTotalSortedWeight(Float totalSortedWeight) {
        speciesOrBenthosBatchUISupport.setTotalSortedWeight(totalSortedWeight);
    }

    public Float getTotalSortedComputedWeight() {
        return speciesOrBenthosBatchUISupport.getTotalSortedComputedWeight();
    }

    public void setTotalSortedComputedWeight(Float totalSortedComputedWeight) {
        speciesOrBenthosBatchUISupport.setTotalSortedComputedWeight(totalSortedComputedWeight);
    }

    public Float getTotalUnsortedComputedWeight() {
        return speciesOrBenthosBatchUISupport.getTotalUnsortedComputedWeight();
    }

    public void setTotalUnsortedComputedWeight(Float totalUnsortedComputedWeight) {
        speciesOrBenthosBatchUISupport.setTotalUnsortedComputedWeight(totalUnsortedComputedWeight);
    }

    public Float getTotalSampleSortedComputedWeight() {
        return speciesOrBenthosBatchUISupport.getTotalSampleSortedComputedWeight();
    }

    public void setTotalSampleSortedComputedWeight(Float totalSampleSortedComputedWeight) {
        speciesOrBenthosBatchUISupport.setTotalSampleSortedComputedWeight(totalSampleSortedComputedWeight);
    }

    public ComputableData<Float> getTotalInertComputedOrNotWeight() {
        return speciesOrBenthosBatchUISupport.getTotalInertComputedOrNotWeight();
    }

    public Float getTotalInertWeight() {
        return speciesOrBenthosBatchUISupport.getTotalInertWeight();
    }

    public void setTotalInertWeight(Float totalInertWeight) {
        speciesOrBenthosBatchUISupport.setTotalInertWeight(totalInertWeight);
    }

    public Float getTotalInertComputedWeight() {
        return speciesOrBenthosBatchUISupport.getTotalInertComputedWeight();
    }

    public void setTotalInertComputedWeight(Float totalInertComputedWeight) {
        speciesOrBenthosBatchUISupport.setTotalInertComputedWeight(totalInertComputedWeight);
    }

    public ComputableData<Float> getTotalLivingNotItemizedComputedOrNotWeight() {
        return speciesOrBenthosBatchUISupport.getTotalLivingNotItemizedComputedOrNotWeight();
    }

    public Float getTotalLivingNotItemizedWeight() {
        return speciesOrBenthosBatchUISupport.getTotalLivingNotItemizedWeight();
    }

    public void setTotalLivingNotItemizedWeight(Float totalLivingNotItemizedWeight) {
        speciesOrBenthosBatchUISupport.setTotalLivingNotItemizedWeight(totalLivingNotItemizedWeight);
    }

    public Float getTotalLivingNotItemizedComputedWeight() {
        return speciesOrBenthosBatchUISupport.getTotalLivingNotItemizedComputedWeight();
    }

    public void setTotalLivingNotItemizedComputedWeight(Float totalLivingNotItemizedComputedWeight) {
        speciesOrBenthosBatchUISupport.setTotalLivingNotItemizedComputedWeight(totalLivingNotItemizedComputedWeight);
    }

    public Integer getDistinctSortedSpeciesCount() {
        return speciesOrBenthosBatchUISupport.getDistinctSortedSpeciesCount();
    }

    public void setDistinctSortedSpeciesCount(Integer distinctSortedSpeciesCount) {
        speciesOrBenthosBatchUISupport.setDistinctSortedSpeciesCount(distinctSortedSpeciesCount);
    }

    public Integer getDistinctUnsortedSpeciesCount() {
        return speciesOrBenthosBatchUISupport.getDistinctUnsortedSpeciesCount();
    }

    public void setDistinctUnsortedSpeciesCount(Integer distinctUnsortedSpeciesCount) {
        speciesOrBenthosBatchUISupport.setDistinctUnsortedSpeciesCount(distinctUnsortedSpeciesCount);
    }

    public void incDistinctSortedSpeciesCount() {
        Integer speciesCount = getDistinctSortedSpeciesCount();
        if (speciesCount == null) {
            speciesCount = 0;
        }
        setDistinctSortedSpeciesCount(speciesCount + 1);
    }

    public void decDistinctSortedSpeciesCount() {
        Integer speciesCount = getDistinctSortedSpeciesCount();
        if (speciesCount != null) {
            setDistinctSortedSpeciesCount(speciesCount - 1);
        }
    }

    public void incDistinctUnsortedSpeciesCount() {
        Integer speciesCount = getDistinctUnsortedSpeciesCount();
        if (speciesCount == null) {
            speciesCount = 0;
        }
        setDistinctUnsortedSpeciesCount(speciesCount + 1);
    }

    public void decDistinctUnsortedSpeciesCount() {
        Integer speciesCount = getDistinctUnsortedSpeciesCount();
        if (speciesCount != null) {
            setDistinctUnsortedSpeciesCount(speciesCount - 1);
        }
    }

    public TableViewMode getTableViewMode() {
        return tableViewMode;
    }

    public void setTableViewMode(TableViewMode tableViewMode) {
        Object oldValue = getTableViewMode();
        this.tableViewMode = tableViewMode;
        firePropertyChange(PROPERTY_TABLE_VIEW_MODE, oldValue, tableViewMode);
        firePropertyChange(PROPERTY_TABLE_VIEW_MODE_ALL, null, isTableViewModeAll());
        firePropertyChange(PROPERTY_TABLE_VIEW_MODE_LEAF, null, isTableViewModeLeaf());
        firePropertyChange(PROPERTY_TABLE_VIEW_MODE_ROOT, null, isTableViewModeRoot());
    }

    public boolean isTableViewModeAll() {
        return TableViewMode.ALL.equals(tableViewMode);
    }

    public boolean isTableViewModeLeaf() {
        return TableViewMode.LEAF.equals(tableViewMode);
    }

    public boolean isTableViewModeRoot() {
        return TableViewMode.ROOT.equals(tableViewMode);
    }

    public Multimap<CaracteristicQualitativeValue, Species> getSpeciesUsed() {
        return catchesUIModel.getSpeciesUsed();
    }

    public int getRootNumber() {
        return rootNumber;
    }

    public void setRootNumber(int rootNumber) {
        Object oldValue = getRootNumber();
        this.rootNumber = rootNumber;
        firePropertyChange(PROPERTY_ROOT_NUMBER, oldValue, rootNumber);
    }

    public int getLeafNumber() {
        return leafNumber;
    }

    public void setLeafNumber(int leafNumber) {
        Object oldValue = getLeafNumber();
        this.leafNumber = leafNumber;
        firePropertyChange(PROPERTY_LEAF_NUMBER, oldValue, leafNumber);
    }

    public boolean isSplitBatchEnabled() {
        return splitBatchEnabled;
    }

    public void setSplitBatchEnabled(boolean splitBatchEnabled) {
        this.splitBatchEnabled = splitBatchEnabled;
        firePropertyChange(PROPERTY_SPLIT_BATCH_ENABLED, null, splitBatchEnabled);
    }

    public boolean isChangeSampleCategoryEnabled() {
        return changeSampleCategoryEnabled;
    }

    public void setChangeSampleCategoryEnabled(boolean changeSampleCategoryEnabled) {
        this.changeSampleCategoryEnabled = changeSampleCategoryEnabled;
        firePropertyChange(PROPERTY_CHANGE_SAMPLE_CATEGORY_ENABLED, null, changeSampleCategoryEnabled);
    }

    public boolean isAddSampleCategoryEnabled() {
        return addSampleCategoryEnabled;
    }

    public void setAddSampleCategoryEnabled(boolean addSampleCategoryEnabled) {
        this.addSampleCategoryEnabled = addSampleCategoryEnabled;
        firePropertyChange(PROPERTY_ADD_SAMPLE_CATEGORY_ENABLED, null, addSampleCategoryEnabled);
    }

    public boolean isRemoveBatchEnabled() {
        return removeBatchEnabled;
    }

    public void setRemoveBatchEnabled(boolean removeBatchEnabled) {
        this.removeBatchEnabled = removeBatchEnabled;
        firePropertyChange(PROPERTY_REMOVE_BATCH_ENABLED, null, removeBatchEnabled);
    }

    public boolean isRemoveSubBatchEnabled() {
        return removeSubBatchEnabled;
    }

    public void setRemoveSubBatchEnabled(boolean removeSubBatchEnabled) {
        this.removeSubBatchEnabled = removeSubBatchEnabled;
        firePropertyChange(PROPERTY_REMOVE_SUB_BATCH_ENABLED, null, removeSubBatchEnabled);
    }

    public boolean isRenameBatchEnabled() {
        return renameBatchEnabled;
    }

    public void setRenameBatchEnabled(boolean renameBatchEnabled) {
        this.renameBatchEnabled = renameBatchEnabled;
        firePropertyChange(PROPERTY_RENAME_BATCH_ENABLED, null, renameBatchEnabled);
    }

    public boolean isCreateMelagEnabled() {
        return createMelagEnabled;
    }

    public void setCreateMelagEnabled(boolean createMelagEnabled) {
        this.createMelagEnabled = createMelagEnabled;
        firePropertyChange(PROPERTY_CREATE_MELAG_ENABLED, null, createMelagEnabled);
    }

    public boolean isEditFrequenciesEnabled() {
        return editFrequenciesEnabled;
    }

    public void setEditFrequenciesEnabled(boolean editFrequenciesEnabled) {
        this.editFrequenciesEnabled = editFrequenciesEnabled;
        firePropertyChange(PROPERTY_EDIT_FREQUENCIES_ENABLED, null, editFrequenciesEnabled);
    }

    //------------------------------------------------------------------------//
    //-- TabContentModel                                                    --//
    //------------------------------------------------------------------------//

    @Override
    public boolean isEmpty() {
        return CollectionUtils.isEmpty(getRows())
                && getTotalSortedWeight() == null
                && getTotalInertWeight() == null
                && getTotalLivingNotItemizedWeight() == null;
    }

    @Override
    public String getTitle() {
        return speciesOrBenthosBatchUISupport.getTitle();
    }

    @Override
    public String getIcon() {
        return null;
    }

    @Override
    public boolean isCloseable() {
        return false;
    }

    public List<SpeciesBatchRowModel> getLeafs(Species species) {
        List<SpeciesBatchRowModel> result = Lists.newArrayList();
        for (SpeciesBatchRowModel rowModel : getRows()) {
            if (rowModel.isBatchRoot()) {
                getLeafs(species, rowModel, result);
            }
        }
        return result;
    }

    public void getLeafs(Species species, SpeciesBatchRowModel rowModel, List<SpeciesBatchRowModel> used) {

        if (rowModel.isBatchLeaf()) {

            if (species.equals(rowModel.getSpecies())) {
                used.add(rowModel);
            }
        } else {
            for (SpeciesBatchRowModel child : rowModel.getChildBatch()) {
                getLeafs(species, child, used);
            }
        }
    }

    public Map<Species, Integer> getSpeciesCount() {
        Map<Species, Integer> result = Maps.newHashMap();
        for (SpeciesBatchRowModel row : getRows()) {
            if (row.isBatchLeaf()) {

                Species species = row.getSpecies();
                Integer count =
                        Numbers.getValueOrComputedValue(result.get(species),
                                                        0);

                List<SpeciesFrequencyRowModel> frequencies = row.getFrequency();
                if (CollectionUtils.isEmpty(frequencies)) {

                    //
                    Integer number = row.getNumber();
                    if (number != null) {
                        count += number;
                    }
                } else {
                    for (SpeciesFrequencyRowModel frequency : frequencies) {
                        Integer number = frequency.getNumber();
                        if (number != null) {
                            count += number;
                        }
                    }
                }
                result.put(species, count);
            }
        }
        return result;
    }

    //------------------------------------------------------------------------//
    //-- SpeciesSortableRowModel                                            --//
    //------------------------------------------------------------------------//

    @Override
    public SpeciesSortMode getSpeciesSortMode() {
        return speciesSortMode;
    }

    @Override
    public void setSpeciesSortMode(SpeciesSortMode speciesSortMode) {
        Object oldValue = getSpeciesSortMode();
        this.speciesSortMode = speciesSortMode;
        firePropertyChange(PROPERTY_SPECIES_SORT_MODE, oldValue, speciesSortMode);
        firePropertyChange(PROPERTY_SPECIES_SORT_MODE_NONE, null, isSpeciesSortModeNone());
        firePropertyChange(PROPERTY_SPECIES_SORT_MODE_ASC, null, isSpeciesSortModeAsc());
        firePropertyChange(PROPERTY_SPECIES_SORT_MODE_DESC, null, isSpeciesSortModeDesc());
    }

    @Override
    public boolean isSpeciesSortModeNone() {
        return SpeciesSortMode.NONE.equals(speciesSortMode);
    }

    @Override
    public boolean isSpeciesSortModeAsc() {
        return SpeciesSortMode.ASC.equals(speciesSortMode);
    }

    @Override
    public boolean isSpeciesSortModeDesc() {
        return SpeciesSortMode.DESC.equals(speciesSortMode);
    }

    @Override
    public int getSpeciesDecoratorContextIndex() {
        return speciesDecoratorContextIndex;
    }

    @Override
    public void setSpeciesDecoratorContextIndex(int speciesDecoratorContextIndex) {
        int oldValue = getSpeciesDecoratorContextIndex();
        this.speciesDecoratorContextIndex = speciesDecoratorContextIndex;
        firePropertyChange(PROPERTY_SPECIES_DECORATOR_CONTEXT_INDEX, oldValue, speciesDecoratorContextIndex);
    }
}


package fr.ifremer.tutti.ui.swing.content.operation.fishing;

/*
 * #%L
 * Tutti :: UI
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.entities.referential.Caracteristic;
import fr.ifremer.tutti.ui.swing.util.AbstractTuttiBeanUIModel;

import java.io.Serializable;

/**
 * @author Kevin Morin - kmorin@codelutin.com
 * @since 1.0
 */
public abstract class AbstractCaracteristicRowModel<RM extends AbstractCaracteristicRowModel<RM>> extends AbstractTuttiBeanUIModel<Serializable, RM> {

    private static final long serialVersionUID = 1L;

    public static final String PROPERTY_KEY = "key";

    public static final String PROPERTY_VALUE = "value";

    protected Caracteristic key;

    protected Serializable value;

    public AbstractCaracteristicRowModel() {
        super(null, null);
    }

//    public AbstractCaracteristicRowModel(Caracteristic key, Serializable value) {
//        this();
//        this.key = key;
//        this.value = value;
//    }

    public Caracteristic getKey() {
        return key;
    }

    public void setKey(Caracteristic key) {
        Object oldValue = getKey();
        this.key = key;
        firePropertyChange(PROPERTY_KEY, oldValue, key);
    }

    public Serializable getValue() {
        return value;
    }

    public void setValue(Serializable value) {
        Object oldValue = getValue();
        this.value = value;
        firePropertyChange(PROPERTY_VALUE, oldValue, value);
    }

    @Override
    protected Serializable newEntity() {
        return null;
    }
}

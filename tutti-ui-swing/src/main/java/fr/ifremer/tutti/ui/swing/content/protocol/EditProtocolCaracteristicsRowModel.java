package fr.ifremer.tutti.ui.swing.content.protocol;

/*
 * #%L
 * Tutti :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.entities.TuttiEntities;
import fr.ifremer.tutti.persistence.entities.protocol.CaracteristicMappingRow;
import fr.ifremer.tutti.persistence.entities.protocol.CaracteristicMappingRowBean;
import fr.ifremer.tutti.persistence.entities.protocol.CaracteristicType;
import fr.ifremer.tutti.persistence.entities.referential.Caracteristic;
import fr.ifremer.tutti.ui.swing.util.AbstractTuttiBeanUIModel;
import org.nuiton.util.beans.Binder;
import org.nuiton.util.beans.BinderFactory;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Kevin Morin (Code Lutin)
 * @since 3.10
 */
public class EditProtocolCaracteristicsRowModel extends AbstractTuttiBeanUIModel<CaracteristicMappingRow, EditProtocolCaracteristicsRowModel> {

    public static final String PROPERTY_PSFM = "psfm";

    public static final String PROPERTY_TYPE = "type";

    public static final String PROPERTY_IMPORT_COLUMN = "importColumn";

    private static final long serialVersionUID = 1L;

    protected Map<String, Caracteristic> caracteristicMap;

    protected Caracteristic psfm;

    protected CaracteristicType type;

    protected String importColumn;

    protected static final Binder<CaracteristicMappingRow, EditProtocolCaracteristicsRowModel> fromBeanBinder =
            BinderFactory.newBinder(CaracteristicMappingRow.class,
                                    EditProtocolCaracteristicsRowModel.class);

    protected static final Binder<EditProtocolCaracteristicsRowModel, CaracteristicMappingRow> toBeanBinder =
            BinderFactory.newBinder(EditProtocolCaracteristicsRowModel.class,
                                    CaracteristicMappingRow.class);

    public EditProtocolCaracteristicsRowModel(Collection<Caracteristic> caracteristics) {
        super(fromBeanBinder, toBeanBinder);
        caracteristicMap = caracteristics != null ? TuttiEntities.splitById(caracteristics) : new HashMap<>();
    }

    public Caracteristic getPsfm() {
        return psfm;
    }

    public void setPsfm(Caracteristic psfm) {
        Object oldValue = getPsfm();
        this.psfm = psfm;
        firePropertyChanged(PROPERTY_PSFM, oldValue, psfm);
    }

    public CaracteristicType getType() {
        return type;
    }

    public void setType(CaracteristicType type) {
        Object oldValue = getType();
        this.type = type;
        firePropertyChanged(PROPERTY_TYPE, oldValue, type);
    }

    public String getImportColumn() {
        return importColumn;
    }

    public void setImportColumn(String importColumn) {
        Object oldValue = getImportColumn();
        this.importColumn = importColumn;
        firePropertyChanged(PROPERTY_IMPORT_COLUMN, oldValue, importColumn);
    }

    public String getPmfmId() {
        return psfm == null ? null : psfm.getId();
    }

    public void setPmfmId(String pmfmId) {
        setPsfm(caracteristicMap.get(pmfmId));
    }

    public String getTab() {
        return type == null ? null : type.name();
    }

    public void setTab(String tab) {
        setType(CaracteristicType.valueOf(tab));
    }

    @Override
    protected CaracteristicMappingRow newEntity() {
        return new CaracteristicMappingRowBean();
    }
}

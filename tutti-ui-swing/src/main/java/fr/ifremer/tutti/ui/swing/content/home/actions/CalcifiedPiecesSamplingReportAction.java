package fr.ifremer.tutti.ui.swing.content.home.actions;

/*
 * #%L
 * Tutti :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2016 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import fr.ifremer.tutti.persistence.ProgressionModel;
import fr.ifremer.tutti.persistence.entities.data.Cruise;
import fr.ifremer.tutti.service.TuttiDataContext;
import fr.ifremer.tutti.service.export.cps.CalcifiedPiecesSamplingExportService;
import fr.ifremer.tutti.service.cruise.CruiseCacheLoader;
import fr.ifremer.tutti.ui.swing.TuttiUIContext;
import fr.ifremer.tutti.ui.swing.content.MainUIHandler;
import fr.ifremer.tutti.ui.swing.content.actions.AbstractMainUITuttiAction;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.util.DateUtil;

import java.io.File;
import java.util.Date;

import static org.nuiton.i18n.I18n.t;

/**
 * @author Kevin Morin (Code Lutin)
 * @since 4.5
 */
public class CalcifiedPiecesSamplingReportAction extends AbstractMainUITuttiAction {

    /** Logger. */
    private static final Log log = LogFactory.getLog(CalcifiedPiecesSamplingReportAction.class);

    protected File file;

    public CalcifiedPiecesSamplingReportAction(MainUIHandler handler) {
        super(handler, false);
    }

    @Override
    public boolean prepareAction() throws Exception {

        boolean doAction = super.prepareAction();

        if (doAction && !getDataContext().isProtocolFilled()) {
            displayErrorMessage(
                    t("tutti.exportCpsCsv.title.missing.protocol"),
                    t("tutti.exportCpsCsv.message.missing.protocol")
            );
            doAction = false;
        }

        if (doAction && !getDataContext().getProtocol().isUseCalcifiedPieceSampling()) {
            displayErrorMessage(
                    t("tutti.exportCpsCsv.title.sampling.notActivated"),
                    t("tutti.exportCpsCsv.message.sampling.notActivated")
            );
            doAction = false;
        }

        if (doAction) {

            String date = DateUtil.formatDate(new Date(), "dd-MM-yyyy");
            String exportFilename = t("tutti.exportCpsCsv.fileName", getDataContext().getCruise().getName(), date);

            // choose file to export
            file = saveFile(
                    exportFilename,
                    "csv",
                    t("tutti.exportCpsCsv.title.choose.exportFile"),
                    t("tutti.exportCpsCsv.action.chooseFile"),
                    "^.+\\.csv$", t("tutti.common.file.csv")
            );
            doAction = file != null;
        }
        return doAction;
    }

    @Override
    public void doAction() throws Exception {

        TuttiDataContext dataContext = getDataContext();
        Cruise cruise = dataContext.getCruise();
        Preconditions.checkNotNull(cruise);
        Preconditions.checkNotNull(file);

        if (log.isInfoEnabled()) {
            log.info("Will export cps for cruise " + cruise.getId() + " to file: " + file);
        }
        ProgressionModel pm = new ProgressionModel();
        setProgressionModel(pm);

        long cruiseFishingOperationIds = dataContext.getCruiseFishingOperationIds().size();
        pm.setTotal((int) (1 + cruiseFishingOperationIds));

        TuttiUIContext context = getContext();
        if (!dataContext.isCruiseCacheLoaded() || !dataContext.isCruiseCacheUpToDate()) {

            // load (or reload) cache
            CruiseCacheLoader cruiseCacheLoader = context.createCruiseCacheLoader(getProgressionModel());
            dataContext.loadCruiseCache(cruiseCacheLoader);
        }


        // export sampling report

        CalcifiedPiecesSamplingExportService service = context.getCalcifiedPiecesSamplingExportService();
        service.exportCruiseCalcifiedPiecesSamplingsReport(file, pm);
    }

    @Override
    public void postSuccessAction() {
        super.postSuccessAction();
        sendMessage(t("tutti.exportCpsCsv.action.success", file));
    }

    @Override
    public void releaseAction() {
        file = null;
        super.releaseAction();
    }

}

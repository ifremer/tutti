package fr.ifremer.tutti.ui.swing.util.table;

/*
 * #%L
 * Tutti :: UI
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import fr.ifremer.tutti.persistence.entities.CaracteristicMap;
import fr.ifremer.tutti.persistence.entities.referential.Caracteristic;
import org.nuiton.jaxx.application.swing.table.ColumnIdentifier;

import java.io.Serializable;

/**
 * To identify a column that represents a caracteristic.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.5
 */
public class CaracteristicColumnIdentifier<R> extends ColumnIdentifier<R> {

    private static final long serialVersionUID = 1L;

    public static <R> CaracteristicColumnIdentifier<R> newCaracteristicId(
            Caracteristic caracteristic,
            String propertyName,
            String headerI18nKey,
            String headerTipI18nKey) {

        return new CaracteristicColumnIdentifier<>(caracteristic, propertyName,
                                                    headerI18nKey,
                                                    headerTipI18nKey
        );
    }

    protected Caracteristic caracteristic;

    protected CaracteristicColumnIdentifier(Caracteristic caracteristic,
                                            String propertyName,
                                            String headerI18nKey,
                                            String headerTipI18nKey) {
        super(propertyName, headerI18nKey, headerTipI18nKey);
        this.caracteristic = caracteristic;
    }

    @Override
    public Object getValue(R entry) {
        CaracteristicMap map = (CaracteristicMap) super.getValue(entry);
        return map.get(caracteristic);
    }

    @Override
    public void setValue(R entry, Object value) {
        CaracteristicMap map = (CaracteristicMap) super.getValue(entry);
        Preconditions.checkNotNull(map, "caracteristicMap (" + getPropertyName() + ") is null in " + entry);
        // do a copy for the old value to be different than the new value
        map = CaracteristicMap.copy(map);
        map.put(caracteristic, (Serializable) value);
        // reset the map in bean to fire property
        super.setValue(entry, map);
    }

    public Caracteristic getCaracteristic() {
        return caracteristic;
    }
}

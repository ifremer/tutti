package fr.ifremer.tutti.ui.swing.content.operation.catches.species.frequency;

/*
 * #%L
 * Tutti :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2016 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.type.WeightUnit;
import org.jdesktop.beans.AbstractSerializableBean;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

import java.util.List;

import static org.nuiton.i18n.I18n.t;

/**
 * Created on 15/04/16.
 *
 * @author Tony Chemit - chemit@codelutin.com
 */
public class AverageWeightsHistogramModel extends AbstractSerializableBean {

    public static final String PROPERTY_TITLE = "title";
    public static final String PROPERTY_LENGTH_STEP_LABEL_WITH_UNIT = "lengthStepLabelWithUnit";
    public static final String PROPERTY_STEP = "step";

    /**
     * To store average weights graph series.
     *
     * @since 4.5
     */
    private final XYSeriesCollection dataset;
    protected final XYSeries series;

    private final WeightUnit frequencyWeightUnit;

    private String title;
    private String lengthStepLabelWithUnit;
    private Float step;

    public AverageWeightsHistogramModel(WeightUnit frequencyWeightUnit) {

        this.frequencyWeightUnit = frequencyWeightUnit;

        this.series = new XYSeries("", true, false);
        this.dataset = new XYSeriesCollection(series);
        this.dataset.setIntervalPositionFactor(0);
        this.dataset.setIntervalWidth(0);

    }

    public WeightUnit getFrequencyWeightUnit() {
        return frequencyWeightUnit;
    }

    public XYSeriesCollection getDataset() {
        return dataset;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {

        title = title + frequencyWeightUnit.decorateLabel(t("tutti.editSpeciesFrequencies.field.graphAverageWeight"));

        Object oldValue = getTitle();
        this.title = title;
        firePropertyChange(PROPERTY_TITLE, oldValue, title);
    }

    public String getLengthStepLabelWithUnit() {
        return lengthStepLabelWithUnit;
    }

    public void setLengthStepLabelWithUnit(String lengthStepLabelWithUnit) {
        Object oldValue = getLengthStepLabelWithUnit();
        this.lengthStepLabelWithUnit = lengthStepLabelWithUnit;
        firePropertyChange(PROPERTY_LENGTH_STEP_LABEL_WITH_UNIT, oldValue, lengthStepLabelWithUnit);
    }

    public Float getStep() {
        return step;
    }

    public void setStep(Float step) {
        Object oldValue = getStep();
        this.step = step;
        firePropertyChange(PROPERTY_STEP, oldValue, step);
    }

    public void reloadRows(List<SpeciesFrequencyRowModel> rows) {

        series.clear();

        if (rows != null) {

            rows.stream().filter(SpeciesFrequencyRowModel::isValid).forEach(this::addOrUpdate);

        }

    }

    public void addOrUpdate(SpeciesFrequencyRowModel row) {

        series.addOrUpdate(row.getLengthStep(), row.computeAverageWeight());

    }

    public void removeValue(Float lengthStep) {
        if (series.indexOf(lengthStep) >= 0) {
            if (series.getItemCount() > 1) {
                series.remove(lengthStep);
            } else {
                series.clear();
            }
        }
    }

}

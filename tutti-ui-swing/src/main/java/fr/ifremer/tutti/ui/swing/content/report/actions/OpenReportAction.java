package fr.ifremer.tutti.ui.swing.content.report.actions;

/*
 * #%L
 * Tutti :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.ui.swing.content.report.ReportUI;
import fr.ifremer.tutti.ui.swing.util.TuttiUIUtil;
import fr.ifremer.tutti.ui.swing.util.actions.SimpleActionSupport;

import java.io.File;

/**
 * Created on 3/11/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.13.2
 */
public class OpenReportAction extends SimpleActionSupport<ReportUI> {

    private static final long serialVersionUID = 1L;

    public OpenReportAction(ReportUI ui) {
        super(ui);
    }

    @Override
    protected void onActionPerformed(ReportUI ui) {

        File outputFile = ui.getModel().getOutputFile();
        TuttiUIUtil.openResource(outputFile);

    }

}
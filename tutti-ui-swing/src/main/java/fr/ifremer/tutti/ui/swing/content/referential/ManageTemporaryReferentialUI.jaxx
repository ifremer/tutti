<!--
  #%L
  Tutti :: UI
  %%
  Copyright (C) 2012 - 2014 Ifremer
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation, either version 3 of the 
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public 
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/gpl-3.0.html>.
  #L%
  -->
<JPanel id='manageTemporaryReferentialTopPanel' layout='{new BorderLayout()}'
        decorator='help'
        implements='fr.ifremer.tutti.ui.swing.util.TuttiUI&lt;ManageTemporaryReferentialUIModel, ManageTemporaryReferentialUIHandler&gt;'>

  <import>

    fr.ifremer.tutti.ui.swing.TuttiHelpBroker
    fr.ifremer.tutti.ui.swing.TuttiUIContext
    fr.ifremer.tutti.ui.swing.util.TuttiUI
    fr.ifremer.tutti.ui.swing.util.TuttiUIUtil

    java.util.Arrays

    static org.nuiton.i18n.I18n.t
  </import>

  <script><![CDATA[

    public ManageTemporaryReferentialUI(TuttiUI parentUI) {
        TuttiUIUtil.setParentUI(this, parentUI);
    }
  ]]></script>

  <ManageTemporaryReferentialUIModel id='model'
                  initializer='getContextValue(ManageTemporaryReferentialUIModel.class)'/>

  <TuttiHelpBroker id='broker'
                   constructorParams='"tutti.manageTemporaryReferential.help"'/>

  <JButton id='exportSpeciesExampleButton'/>
  <JButton id='exportExistingSpeciesButton'/>
  <JButton id='importSpeciesButton'/>
  <JButton id='replaceSpeciesButton'/>
  <JButton id='exportVesselExampleButton'/>
  <JButton id='exportExistingVesselButton'/>
  <JButton id='importVesselButton'/>
  <JButton id='replaceVesselButton'/>
  <JButton id='exportGearExampleButton'/>
  <JButton id='exportExistingGearButton'/>
  <JButton id='importGearButton'/>
  <JButton id='replaceGearButton'/>
  <JButton id='exportPersonExampleButton'/>
  <JButton id='exportExistingPersonButton'/>
  <JButton id='importPersonButton'/>
  <JButton id='replacePersonButton'/>

  <Table constraints='BorderLayout.CENTER'>
    <row>
      <cell>
        <JLabel id='speciesLabel'/>
      </cell>
      <cell>
        <JComboBox id='speciesActionComboBox'/>
      </cell>
    </row>

    <row>
      <cell columns='2' fill='both'>
        <JSeparator/>
      </cell>
    </row>

    <row>
      <cell>
        <JLabel id='vesselLabel'/>
      </cell>
      <cell>
        <JComboBox id='vesselActionComboBox'/>
      </cell>
    </row>

    <row>
      <cell columns='2' fill='both'>
        <JSeparator/>
      </cell>
    </row>

    <row>
      <cell>
        <JLabel id='gearLabel'/>
      </cell>
      <cell>
        <JComboBox id='gearActionComboBox'/>
      </cell>
    </row>

    <row>
      <cell columns='2' fill='both'>
        <JSeparator/>
      </cell>
    </row>

    <row>
      <cell>
        <JLabel id='personLabel'/>
      </cell>
      <cell>
        <JComboBox id='personActionComboBox'/>
      </cell>
    </row>

  </Table>
</JPanel>

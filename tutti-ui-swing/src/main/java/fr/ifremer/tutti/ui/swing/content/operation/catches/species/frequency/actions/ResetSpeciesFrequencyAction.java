package fr.ifremer.tutti.ui.swing.content.operation.catches.species.frequency.actions;

/*
 * #%L
 * Tutti :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.ui.swing.content.operation.catches.species.frequency.SpeciesFrequencyUI;
import fr.ifremer.tutti.ui.swing.content.operation.catches.species.frequency.SpeciesFrequencyUIHandler;
import fr.ifremer.tutti.ui.swing.content.operation.catches.species.frequency.SpeciesFrequencyUIModel;
import fr.ifremer.tutti.ui.swing.util.actions.LongActionSupport;
import org.nuiton.jaxx.application.swing.AbstractApplicationUIHandler;

import javax.swing.JOptionPane;

import static org.nuiton.i18n.I18n.t;

/**
 * Created on 1/1/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.11
 */
public class ResetSpeciesFrequencyAction extends LongActionSupport<SpeciesFrequencyUIModel, SpeciesFrequencyUI, SpeciesFrequencyUIHandler> {


    public ResetSpeciesFrequencyAction(SpeciesFrequencyUIHandler handler) {
        super(handler, false);
    }

    @Override
    public boolean prepareAction() throws Exception {

        boolean doAction = super.prepareAction();

        if (doAction) {

            doAction = false;
            // Ask confirmation to quit screen

            String htmlMessage = String.format(AbstractApplicationUIHandler.CONFIRMATION_FORMAT,
                                               t("tutti.askToResetEditFrequencies.message"),
                                               t("tutti.askToResetEditFrequencies.help"));

            int saveResponse = JOptionPane.showOptionDialog(
                    getHandler().getTopestUI(),
                    htmlMessage,
                    t("tutti.askToResetEditFrequencies.title"),
                    JOptionPane.OK_CANCEL_OPTION,
                    JOptionPane.QUESTION_MESSAGE,
                    null,
                    new String[]{t("tutti.option.continue"), t("tutti.option.cancel")},
                    t("tutti.option.cancel"));

            switch (saveResponse) {
                case 0:

                    // accept
                    doAction = true;
                    break;

            }
        }
        return doAction;

    }

    @Override
    public void doAction() throws Exception {

        SpeciesFrequencyUIModel model = getModel();

        // remove all frequencies
        model.clear();

        // remove all individual observations
        model.getIndividualObservationModel().clear();

    }

}
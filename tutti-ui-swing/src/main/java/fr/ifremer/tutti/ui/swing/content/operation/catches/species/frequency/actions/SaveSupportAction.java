package fr.ifremer.tutti.ui.swing.content.operation.catches.species.frequency.actions;

/*
 * #%L
 * Tutti :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.ui.swing.content.operation.catches.species.frequency.SpeciesFrequencyRowModel;
import fr.ifremer.tutti.ui.swing.content.operation.catches.species.frequency.SpeciesFrequencyUI;
import fr.ifremer.tutti.ui.swing.content.operation.catches.species.frequency.SpeciesFrequencyUIHandler;
import fr.ifremer.tutti.ui.swing.content.operation.catches.species.frequency.SpeciesFrequencyUIModel;
import jaxx.runtime.swing.JTables;
import org.nuiton.jaxx.application.swing.AbstractApplicationUIHandler;

import javax.swing.AbstractAction;
import javax.swing.JOptionPane;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.nuiton.i18n.I18n.t;

/**
 * Created on 1/1/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.11
 */
public abstract class SaveSupportAction extends AbstractAction {

    private static final long serialVersionUID = 1L;

    final SpeciesFrequencyUI ui;

    public SaveSupportAction(SpeciesFrequencyUI ui) {
        this.ui = ui;
    }

    protected boolean canSaveFrequencies() {
        boolean doSave = true;

        SpeciesFrequencyUIModel model = ui.getModel();

        // check for doublon
        // check that we do not have doublon in length
        // see http://forge.codelutin.com/issues/2499
        Set<Float> lengths = new HashSet<>();

        Float doublon = null;
        int index = 0;
        List<SpeciesFrequencyRowModel> rows = model.getRows();
        for (SpeciesFrequencyRowModel row : rows) {
            Float lengthStep = row.getLengthStep();
            if (!lengths.add(lengthStep)) {

                // already exist
                doublon = lengthStep;
                break;
            }
            index++;
        }
        SpeciesFrequencyUIHandler handler = ui.getHandler();

        if (doublon != null) {

            // can't save mensurations (found doublon)
            String message =
                    t("tutti.editSpeciesFrequencies.error.length.doublon",
                      doublon, index + 1);
            handler.getContext().getErrorHelper().showErrorDialog(
                    message);

            // focus to first error row
            JTables.selectFirstCellOnRow(ui.getTable(), index, false);
            doSave = false;
        }

        // ask user if there is some rows we can't save
        // see http://forge.codelutin.com/issues/4046
        if (doSave && model.isSomeRowsWithWeightAndOtherWithout()) {

            // there is some rows with weight and other without
            // ask user what to do

            String htmlMessage = String.format(
                    AbstractApplicationUIHandler.CONFIRMATION_FORMAT,
                    t("tutti.editSpeciesFrequencies.askBeforeSave.message"),
                    t("tutti.editSpeciesFrequencies.askBeforeSave.help"));
            int answer = JOptionPane.showConfirmDialog(
                    handler.getTopestUI(),
                    htmlMessage,
                    t("tutti.editSpeciesFrequencies.askBeforeSave.title"),
                    JOptionPane.YES_NO_OPTION,
                    JOptionPane.QUESTION_MESSAGE);

            switch (answer) {
                case JOptionPane.YES_OPTION:

                    // ok can save
                    break;
                default:

                    // do not save
                    doSave = false;
            }
        }

        return doSave;
    }
}
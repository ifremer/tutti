package fr.ifremer.tutti.ui.swing.content.operation.catches.species.edit;

/*
 * #%L
 * Tutti :: UI
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.entities.data.SpeciesBatch;
import fr.ifremer.tutti.persistence.entities.referential.Species;
import fr.ifremer.tutti.service.DecoratorService;

import static org.nuiton.i18n.I18n.t;

/**
 * To decorate some {@link SpeciesBatch} as a {@link Species}.
 *
 * Used to be able to keep the hole speciesBatch in the cell.
 *
 * @since 2.8
 */
public class SpeciesBatchDecorator<R extends SpeciesBatch> extends DecoratorService.SpeciesFromProtocolDecorator {

    private static final long serialVersionUID = 1L;

    public static <R extends SpeciesBatch> SpeciesBatchDecorator<R> newDecorator() {
        return new SpeciesBatchDecorator<>();
    }

    protected final TuttiDecoratorComparator[] originalComparators;

    public SpeciesBatchDecorator() throws IllegalArgumentException, NullPointerException {
        super();

        int nbContext = getNbContext();
        originalComparators = new TuttiDecoratorComparator[nbContext];

        for (int i = 0; i < nbContext; i++) {
            TuttiDecoratorComparator comparator = (TuttiDecoratorComparator<?>) context.getComparator(0);
            originalComparators[i] = comparator;
            Context context = contexts[i];
            context.setComparator(new SpeciesBatchDecoratorComparator<>(comparator.getExpression()));
        }
    }

    @Override
    public String toString(Object bean) {
        if (bean instanceof SpeciesBatch) {
            bean = ((SpeciesBatch) bean).getSpecies();
        }
        return super.toString(bean);
    }

    @Override
    protected Object getValue(Species bean, String token) {
        Object result = super.getValue(bean, token);
        if (Species.PROPERTY_SURVEY_CODE.equals(token) && result == null) {
            result = bean.getRefTaxCode();
        }
        return result;
    }

    @Override
    protected Object onNullValue(Species bean, String token) {
        Object result;
        if (Species.PROPERTY_SURVEY_CODE.equals(token)) {
            result = t("tutti.propety.no.species.speciesCode");

        } else {
            result = super.onNullValue(bean, token);
        }
        return result;
    }

    public TuttiDecoratorComparator getOriginalComparator() {
        return originalComparators[contextIndex];
    }

    protected int contextIndex = 0;

    @Override
    public void setContextIndex(int index) {
        super.setContextIndex(index);
        this.contextIndex = index;
    }

    public int getContextIndex() {
        return contextIndex;
    }
}

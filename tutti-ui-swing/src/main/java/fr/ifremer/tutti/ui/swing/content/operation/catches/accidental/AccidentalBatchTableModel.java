package fr.ifremer.tutti.ui.swing.content.operation.catches.accidental;

/*
 * #%L
 * Tutti :: UI
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.type.WeightUnit;
import org.jdesktop.swingx.table.TableColumnModelExt;
import org.nuiton.jaxx.application.swing.table.AbstractApplicationTableModel;
import org.nuiton.jaxx.application.swing.table.ColumnIdentifier;

import static org.nuiton.i18n.I18n.n;

/**
 * @author Tony Chemit - chemit@codelutin.com
 * @since 0.2
 */
public class AccidentalBatchTableModel extends AbstractApplicationTableModel<AccidentalBatchRowModel> {

    private static final long serialVersionUID = 1L;

    public static final ColumnIdentifier<AccidentalBatchRowModel> ID = ColumnIdentifier.newId(
            AccidentalBatchRowModel.PROPERTY_ID,
            n("tutti.editAccidentalBatch.table.header.id"),
            n("tutti.editAccidentalBatch.table.header.id.tip"));

    public static final ColumnIdentifier<AccidentalBatchRowModel> SPECIES = ColumnIdentifier.newId(
            AccidentalBatchRowModel.PROPERTY_SPECIES,
            n("tutti.editAccidentalBatch.table.header.species"),
            n("tutti.editAccidentalBatch.table.header.species.tip"));

    public static final ColumnIdentifier<AccidentalBatchRowModel> GENDER = ColumnIdentifier.newId(
            AccidentalBatchRowModel.PROPERTY_GENDER,
            n("tutti.editAccidentalBatch.table.header.gender"),
            n("tutti.editAccidentalBatch.table.header.gender.tip"));

    public static final ColumnIdentifier<AccidentalBatchRowModel> WEIGHT = ColumnIdentifier.newId(
            AccidentalBatchRowModel.PROPERTY_WEIGHT,
            n("tutti.editAccidentalBatch.table.header.weight"),
            n("tutti.editAccidentalBatch.table.header.weight.tip"));

    public static final ColumnIdentifier<AccidentalBatchRowModel> SIZE = ColumnIdentifier.newId(
            AccidentalBatchRowModel.PROPERTY_SIZE,
            n("tutti.editAccidentalBatch.table.header.size"),
            n("tutti.editAccidentalBatch.table.header.size.tip"));

    public static final ColumnIdentifier<AccidentalBatchRowModel> LENGTH_STEP_CARACTERISTIC = ColumnIdentifier.newId(
            AccidentalBatchRowModel.PROPERTY_LENGTH_STEP_CARACTERISTIC,
            n("tutti.editAccidentalBatch.table.header.lengthStepCaracteristic"),
            n("tutti.editAccidentalBatch.table.header.lengthStepCaracteristic.tip"));

    public static final ColumnIdentifier<AccidentalBatchRowModel> DEAD_OR_ALIVE = ColumnIdentifier.newId(
            AccidentalBatchRowModel.PROPERTY_DEAD_OR_ALIVE,
            n("tutti.editAccidentalBatch.table.header.deadOrAlive"),
            n("tutti.editAccidentalBatch.table.header.deadOrAlive.tip"));

//    public static final ColumnIdentifier<AccidentalBatchRowModel> OTHER_CARACTERISTICS = ColumnIdentifier.newId(
//            IndividualObservationBatchRowModel.PROPERTY_CARACTERISTICS,
//            n("tutti.editAccidentalBatch.table.header.otherCaracteristics"),
//            n("tutti.editAccidentalBatch.table.header.otherCaracteristics.tip"));

    public static final ColumnIdentifier<AccidentalBatchRowModel> COMMENT = ColumnIdentifier.newId(
            AccidentalBatchRowModel.PROPERTY_COMMENT,
            n("tutti.editAccidentalBatch.table.header.comment"),
            n("tutti.editAccidentalBatch.table.header.comment.tip"));

    public static final ColumnIdentifier<AccidentalBatchRowModel> ATTACHMENT = ColumnIdentifier.newReadOnlyId(
            AccidentalBatchRowModel.PROPERTY_ATTACHMENT,
            n("tutti.editAccidentalBatch.table.header.file"),
            n("tutti.editAccidentalBatch.table.header.file.tip"));

    /**
     * Weight unit.
     *
     * @since 2.5
     */
    protected final WeightUnit weightUnit;

    public AccidentalBatchTableModel(WeightUnit weightUnit,
                                     TableColumnModelExt columnModel) {
        super(columnModel, false, false);
        this.weightUnit = weightUnit;
        setNoneEditableCols(ID);
    }

    @Override
    public AccidentalBatchRowModel createNewRow() {
        AccidentalBatchRowModel result = new AccidentalBatchRowModel(weightUnit);

        // by default empty row is not valid
        result.setValid(false);
        return result;
    }
}

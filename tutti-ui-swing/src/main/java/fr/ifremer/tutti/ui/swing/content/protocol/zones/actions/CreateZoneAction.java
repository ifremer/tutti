package fr.ifremer.tutti.ui.swing.content.protocol.zones.actions;

/*
 * #%L
 * Tutti :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2016 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.entities.protocol.Zone;
import fr.ifremer.tutti.persistence.entities.protocol.Zones;
import fr.ifremer.tutti.ui.swing.content.protocol.EditProtocolUIModel;
import fr.ifremer.tutti.ui.swing.content.protocol.zones.ZoneEditorUI;
import fr.ifremer.tutti.ui.swing.content.protocol.zones.tree.node.ZoneNode;
import fr.ifremer.tutti.ui.swing.util.actions.SimpleActionSupport;
import org.apache.commons.lang3.StringUtils;

import javax.swing.JOptionPane;
import javax.swing.JTree;
import javax.swing.tree.TreePath;

import java.util.UUID;

import static org.nuiton.i18n.I18n.t;

/**
 * @author Kevin Morin (Code Lutin)
 * @since 4.5
 */
public class CreateZoneAction extends SimpleActionSupport<ZoneEditorUI> {

    public CreateZoneAction(ZoneEditorUI zoneEditorUI) {
        super(zoneEditorUI);
    }

    @Override
    protected void onActionPerformed(ZoneEditorUI zoneEditorUI) {

        String zoneLabel = JOptionPane.showInputDialog(zoneEditorUI,
                                                       t("tutti.zoneEditor.createZone.message"),
                                                       t("tutti.zoneEditor.createZone.title"),
                                                       JOptionPane.QUESTION_MESSAGE);

        if (StringUtils.isNotEmpty(zoneLabel)) {

            Zone zone = Zones.newZone();
            zone.setId(UUID.randomUUID().toString());
            zone.setLabel(zoneLabel);

            EditProtocolUIModel model = zoneEditorUI.getModel();

            ZoneNode zoneNode = model.getZonesTreeModel().addZone(zone);

            //select newly created node
            JTree zonesTree = zoneEditorUI.getZonesTree();
            TreePath path = new TreePath(zoneNode.getPath());
            zonesTree.setSelectionPath(path);
        }
    }
}

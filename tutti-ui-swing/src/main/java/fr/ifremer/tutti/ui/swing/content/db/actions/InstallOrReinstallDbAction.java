package fr.ifremer.tutti.ui.swing.content.db.actions;

/*
 * #%L
 * Tutti :: UI
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.ui.swing.content.actions.AbstractMainUITuttiAction;
import fr.ifremer.tutti.ui.swing.content.MainUIHandler;

/**
 * To install or reinstall a db (will delegate to correct action).
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.4
 */
public class InstallOrReinstallDbAction extends AbstractMainUITuttiAction {

    protected AbstractMainUITuttiAction delegateAction;

    public InstallOrReinstallDbAction(MainUIHandler handler) {
        super(handler, true);
    }

    @Override
    public boolean prepareAction() throws Exception {

        if (getContext().isDbExist()) {

            // reinstall action
            delegateAction = getContext().getActionFactory().createLogicAction(getHandler(), ReinstallDbAction.class);
        } else {

            // install action
            delegateAction = getContext().getActionFactory().createLogicAction(getHandler(), InstallDbAction.class);
        }

        setActionDescription(delegateAction.getActionDescription());

        return delegateAction.prepareAction();
    }

    @Override
    public void doAction() throws Exception {

        getActionEngine().runInternalAction(delegateAction);
    }

    @Override
    protected void releaseAction() {
        delegateAction = null;
        super.releaseAction();
    }

}

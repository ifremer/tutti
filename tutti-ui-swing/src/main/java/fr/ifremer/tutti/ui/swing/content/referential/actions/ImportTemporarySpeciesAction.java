package fr.ifremer.tutti.ui.swing.content.referential.actions;

/*
 * #%L
 * Tutti :: UI
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.entities.referential.Species;
import fr.ifremer.tutti.service.referential.ReferentialImportResult;
import fr.ifremer.tutti.service.referential.ReferentialTemporarySpeciesService;
import fr.ifremer.tutti.ui.swing.content.referential.ManageTemporaryReferentialUIHandler;

import java.io.File;

import static org.nuiton.i18n.I18n.t;

/**
 * Import temporary species referential.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.0
 */
public class ImportTemporarySpeciesAction extends ImportTemporaryActionSupport<Species> {


    public ImportTemporarySpeciesAction(ManageTemporaryReferentialUIHandler handler) {
        super(handler);
    }

    @Override
    protected File chooseImportFile() {

        return chooseFile(
                t("tutti.manageTemporaryReferential.title.choose.importTemporarySpeciesFile"),
                t("tutti.manageTemporaryReferential.action.chooseReferentialSpeciesFile.import"),
                "^.*\\.csv", t("tutti.common.file.csv"));

    }

    @Override
    protected ReferentialImportResult<Species> doImport(File file) {

        ReferentialTemporarySpeciesService service = getContext().getReferentialTemporarySpeciesService();
        return service.importTemporarySpecies(file);

    }

    @Override
    protected void postSuccessAction(File file, ReferentialImportResult<Species> result) {

        int nbRef = getModel().getNbTemporarySpecies();
        getModel().setNbTemporarySpecies(nbRef + result.getNbRefAdded() - result.getNbRefDeleted());

        // reset ui cache
        getDataContext().resetSpecies();
        reloadFishingOperation();

        getHandler().resetComboBoxAction(getUI().getSpeciesActionComboBox());
        String title = t("tutti.manageTemporaryReferential.action.chooseReferentialSpeciesFile.import.dialog.title");
        String message = t("tutti.manageTemporaryReferential.action.chooseReferentialSpeciesFile.import.dialog.message",
                           result.getNbRefAdded(), result.getNbRefUpdated(), result.getNbRefDeleted());
        displayInfoMessage(title, message);

        sendMessage(t("tutti.manageTemporaryReferential.action.chooseReferentialSpeciesFile.import.success", file));

    }
}

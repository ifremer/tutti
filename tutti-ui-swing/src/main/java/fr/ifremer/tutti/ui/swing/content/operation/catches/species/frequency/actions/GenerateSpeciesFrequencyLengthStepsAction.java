package fr.ifremer.tutti.ui.swing.content.operation.catches.species.frequency.actions;

/*
 * #%L
 * Tutti :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.ui.swing.content.operation.catches.species.frequency.SpeciesFrequencyTableModel;
import fr.ifremer.tutti.ui.swing.content.operation.catches.species.frequency.SpeciesFrequencyUI;
import jaxx.runtime.SwingUtil;
import jaxx.runtime.swing.JTables;

import javax.swing.AbstractAction;
import java.awt.event.ActionEvent;

import static org.nuiton.i18n.I18n.t;

/**
 * Created on 1/1/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.11
 */
public class GenerateSpeciesFrequencyLengthStepsAction extends AbstractAction {

    private static final long serialVersionUID = 1L;

    final SpeciesFrequencyUI ui;

    public GenerateSpeciesFrequencyLengthStepsAction(SpeciesFrequencyUI ui) {
        this.ui = ui;

        putValue(NAME, t("tutti.editSpeciesFrequencies.action.generate"));
        putValue(SHORT_DESCRIPTION, t("tutti.editSpeciesFrequencies.action.generate.tip"));
        putValue(MNEMONIC_KEY, (int) SwingUtil.getFirstCharAt(t("tutti.editSpeciesFrequencies.action.generate.mnemonic"), 'Z'));
        putValue(SMALL_ICON, SwingUtil.createActionIcon("generate"));

    }

    @Override
    public void actionPerformed(ActionEvent e) {

//        SpeciesFrequencyUIModel model = ui.getModel();
        SpeciesFrequencyTableModel tableModel = ui.getHandler().getTableModel();
        tableModel.generateRows();

//        Map<Float, SpeciesFrequencyRowModel> rowsByStep = model.getRowCache();
//
//        Float minStep = model.getLengthStep(model.getMinStep());
//        Float maxStep = model.getLengthStep(model.getMaxStep());
//        Caracteristic lengthStepCaracteristic = model.getLengthStepCaracteristic();
//
//        Set<Float> existingKeys = new HashSet<>(rowsByStep.keySet());
//        List<SpeciesFrequencyRowModel> rows = new ArrayList<>(rowsByStep.values());
//
//        for (float i = minStep, step = model.getStep(); i <= maxStep; i = Numbers.getRoundedLengthStep(i + step, true)) {
//
//            if (!existingKeys.contains(i)) {
//
//                // add it
//                SpeciesFrequencyRowModel newRow = tableModel.createNewRow();
//                newRow.setLengthStep(i);
//                newRow.setLengthStepCaracteristic(lengthStepCaracteristic);
//                rows.add(newRow);
//            }
//        }
//        Collections.sort(rows);
//        model.setRows(rows);

        // select first cell in table (see http://forge.codelutin.com/issues/2496)
        JTables.doSelectCell(ui.getTable(), 0, 1);

    }
}

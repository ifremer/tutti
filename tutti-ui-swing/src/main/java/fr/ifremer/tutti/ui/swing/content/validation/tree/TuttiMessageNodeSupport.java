package fr.ifremer.tutti.ui.swing.content.validation.tree;

/*
 * #%L
 * Tutti :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.tree.DefaultMutableTreeNode;

/**
 * Created on 9/24/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.8
 */
public class TuttiMessageNodeSupport<O> extends DefaultMutableTreeNode {

    private static final long serialVersionUID = 1L;

    private final ImageIcon icon;

    private final JComponent editor;

    public TuttiMessageNodeSupport(O userObject, ImageIcon icon, JComponent editor) {
        super(userObject);
        this.icon = icon;
        this.editor = editor;
    }

    public JComponent getEditor() {
        return editor;
    }

    public ImageIcon getIcon() {
        return icon;
    }

    @Override
    public O getUserObject() {
        return (O) super.getUserObject();
    }
}

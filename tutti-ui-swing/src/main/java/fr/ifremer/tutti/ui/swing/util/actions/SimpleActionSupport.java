package fr.ifremer.tutti.ui.swing.util.actions;

/*
 * #%L
 * Tutti :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import jaxx.runtime.JAXXObject;

import javax.swing.AbstractAction;
import javax.swing.SwingUtilities;
import java.awt.event.ActionEvent;

/**
 * Created on 3/7/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.15
 */
public abstract class SimpleActionSupport<UI extends JAXXObject> extends AbstractAction {

    private static final long serialVersionUID = 1L;

    private final UI ui;

    private final boolean invokeLater;

    protected SimpleActionSupport(UI ui) {
        this(ui, false);
    }

    protected SimpleActionSupport(UI ui, boolean invokeLater) {
        this.ui = ui;
        this.invokeLater = invokeLater;
    }

    @Override
    public final void actionPerformed(ActionEvent e) {

        if (invokeLater) {

            SwingUtilities.invokeLater(() -> onActionPerformed(ui));

        } else {

            onActionPerformed(ui);

        }

    }

    protected abstract void onActionPerformed(UI ui);

}

package fr.ifremer.tutti.ui.swing.content.protocol.actions;

/*
 * #%L
 * Tutti :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import com.google.common.collect.Sets;
import fr.ifremer.tutti.persistence.entities.referential.Caracteristic;
import fr.ifremer.tutti.ui.swing.util.actions.LongActionSupport;
import fr.ifremer.tutti.ui.swing.content.protocol.EditProtocolCaracteristicsRowModel;
import fr.ifremer.tutti.ui.swing.content.protocol.EditProtocolCaracteristicsTableModel;
import fr.ifremer.tutti.ui.swing.content.protocol.EditProtocolUI;
import fr.ifremer.tutti.ui.swing.content.protocol.EditProtocolUIHandler;
import fr.ifremer.tutti.ui.swing.content.protocol.EditProtocolUIModel;
import jaxx.runtime.SwingUtil;

import javax.swing.JTable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import static org.nuiton.i18n.I18n.t;

/**
 * @author Kevin Morin (Code Lutin)
 * @since 3.10
 */
public class RemoveCaracteristicMappingAction extends LongActionSupport<EditProtocolUIModel, EditProtocolUI, EditProtocolUIHandler> {


    /**
     * Set of removed caracteristics.
     */
    protected Set<Caracteristic> removedCarateristics;

    /**
     * Set of removed rows.
     */
    protected Set<EditProtocolCaracteristicsRowModel> removedRows;

    protected List<Integer> removedRowIndexes;


    public RemoveCaracteristicMappingAction(EditProtocolUIHandler handler) {
        super(handler, false);
    }

    @Override
    public void doAction() throws Exception {

        JTable table = handler.getCaracteristicsMappingTable();

        // need to have a selection
        Preconditions.checkState(!table.getSelectionModel().isSelectionEmpty());

        EditProtocolCaracteristicsTableModel tableModel =
                (EditProtocolCaracteristicsTableModel) table.getModel();

        removedCarateristics = Sets.newHashSet();
        removedRows = Sets.newHashSet();
        removedRowIndexes = new ArrayList<>();

        for (Integer rowIndex : SwingUtil.getSelectedModelRows(table)) {

            removedRowIndexes.add(rowIndex);

            // get row to remove
            EditProtocolCaracteristicsRowModel selectedRow =
                    tableModel.getEntry(rowIndex);

            // re-add all synonym of this taxon to the species / benthos combobox
            Caracteristic caracteristic = selectedRow.getPsfm();
            removedCarateristics.add(caracteristic);

            // mark row to be removed at the very last moment
            removedRows.add(selectedRow);
        }

        Collections.sort(removedRowIndexes, Collections.reverseOrder());
    }

    @Override
    public void postSuccessAction() {
        super.postSuccessAction();

        // update comboboxes
        getUI().getCaracteristicMappingComboBox().addItems(removedCarateristics);

        // remove all rows from model
        getModel().removeCaracteristicMappingRows(removedRows);

        // fire table data changed
        JTable table = handler.getCaracteristicsMappingTable();
        EditProtocolCaracteristicsTableModel tableModel =
                (EditProtocolCaracteristicsTableModel) table.getModel();

        removedRowIndexes.forEach(tableModel::removeRow);

        // clear table selection
        table.clearSelection();

        // notify user
        sendMessage(t("tutti.flash.info.caracteristicMapping.remove.from.protocol"));
    }
}

package fr.ifremer.tutti.ui.swing.content.operation.catches.actions;

/*
 * #%L
 * Tutti :: UI
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import fr.ifremer.tutti.persistence.entities.data.CatchBatch;
import fr.ifremer.tutti.persistence.entities.data.FishingOperation;
import fr.ifremer.tutti.ui.swing.util.actions.LongActionSupport;
import fr.ifremer.tutti.ui.swing.content.operation.FishingOperationsUI;
import fr.ifremer.tutti.ui.swing.content.operation.catches.EditCatchesUI;
import fr.ifremer.tutti.ui.swing.content.operation.catches.EditCatchesUIHandler;
import fr.ifremer.tutti.ui.swing.content.operation.catches.EditCatchesUIModel;
import fr.ifremer.tutti.ui.swing.content.operation.fishing.actions.EditFishingOperationAction;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * To cancel edit of a {@link CatchBatch}.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.0
 */
public class CancelEditCatchBatchAction extends LongActionSupport<EditCatchesUIModel, EditCatchesUI, EditCatchesUIHandler> {

    /** Logger. */
    private static final Log log =
            LogFactory.getLog(CancelEditCatchBatchAction.class);

//    /**
//     * A flag to update ui after create or save the edit catch batch.
//     *
//     * @since 1.0
//     */
//    protected boolean updateUI;

    protected EditFishingOperationAction editAction;

    public CancelEditCatchBatchAction(EditCatchesUIHandler handler) {
        super(handler, true);
    }

    public EditFishingOperationAction getEditAction() {
        if (editAction == null) {
            FishingOperationsUI parentContainer = handler.getParentContainer(FishingOperationsUI.class);
            editAction = getContext().getActionFactory().createLogicAction(parentContainer.getHandler(),
                                                                           EditFishingOperationAction.class);
        }
        return editAction;
    }

    @Override
    public void doAction() throws Exception {

        // cancel to create a catch batch ?
        // Should never happen ?
        Preconditions.checkState(
                !getModel().isCreate(),
                "Can't cancel edition of a not persisted catchBatch!");

        if (log.isInfoEnabled()) {
            log.info("Can edition of catchBatch (will reload catchBatch)");
        }

        FishingOperation operation = getModel().getFishingOperation();

        getEditAction().loadCatchBatch(operation);
    }

}

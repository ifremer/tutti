package fr.ifremer.tutti.ui.swing.content.operation.catches.marinelitter;

/*
 * #%L
 * Tutti :: UI
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.type.WeightUnit;
import org.jdesktop.swingx.table.TableColumnModelExt;
import org.nuiton.jaxx.application.swing.table.AbstractApplicationTableModel;
import org.nuiton.jaxx.application.swing.table.ColumnIdentifier;

import static org.nuiton.i18n.I18n.n;

/**
 * @author Tony Chemit - chemit@codelutin.com
 * @since 0.2
 */
public class MarineLitterBatchTableModel extends AbstractApplicationTableModel<MarineLitterBatchRowModel> {

    private static final long serialVersionUID = 1L;

    public static final ColumnIdentifier<MarineLitterBatchRowModel> ID = ColumnIdentifier.newId(
            MarineLitterBatchRowModel.PROPERTY_ID,
            n("tutti.editMarineLitterBatch.table.header.id"),
            n("tutti.editMarineLitterBatch.table.header.id.tip"));

    public static final ColumnIdentifier<MarineLitterBatchRowModel> MACRO_WASTE_CATEGORY = ColumnIdentifier.newId(
            MarineLitterBatchRowModel.PROPERTY_MARINE_LITTER_CATEGORY,
            n("tutti.editMarineLitterBatch.table.header.marineLitterCategory"),
            n("tutti.editMarineLitterBatch.table.header.marineLitterCategory.tip"));

    public static final ColumnIdentifier<MarineLitterBatchRowModel> MACRO_WASTE_SIZE_CATEGORY = ColumnIdentifier.newId(
            MarineLitterBatchRowModel.PROPERTY_MARINE_LITTER_SIZE_CATEGORY,
            n("tutti.editMarineLitterBatch.table.header.marineLitterSizeCategory"),
            n("tutti.editMarineLitterBatch.table.header.marineLitterSizeCategory.tip"));

    public static final ColumnIdentifier<MarineLitterBatchRowModel> NUMBER = ColumnIdentifier.newId(
            MarineLitterBatchRowModel.PROPERTY_NUMBER,
            n("tutti.editMarineLitterBatch.table.header.number"),
            n("tutti.editMarineLitterBatch.table.header.number.tip"));

    public static final ColumnIdentifier<MarineLitterBatchRowModel> WEIGHT = ColumnIdentifier.newId(
            MarineLitterBatchRowModel.PROPERTY_WEIGHT,
            n("tutti.editMarineLitterBatch.table.header.weight"),
            n("tutti.editMarineLitterBatch.table.header.weight.tip"));

    public static final ColumnIdentifier<MarineLitterBatchRowModel> COMMENT = ColumnIdentifier.newId(
            MarineLitterBatchRowModel.PROPERTY_COMMENT,
            n("tutti.editMarineLitterBatch.table.header.comment"),
            n("tutti.editMarineLitterBatch.table.header.comment.tip"));

    public static final ColumnIdentifier<MarineLitterBatchRowModel> ATTACHMENT = ColumnIdentifier.newReadOnlyId(
            MarineLitterBatchRowModel.PROPERTY_ATTACHMENT,
            n("tutti.editMarineLitterBatch.table.header.file"),
            n("tutti.editMarineLitterBatch.table.header.file.tip"));

    /**
     * Weight unit.
     *
     * @since 2.5
     */
    protected final WeightUnit weightUnit;

    public MarineLitterBatchTableModel(WeightUnit weightUnit,
                                       TableColumnModelExt columnModel) {
        super(columnModel, false, false);
        this.weightUnit = weightUnit;

        setNoneEditableCols(ID, MACRO_WASTE_CATEGORY, MACRO_WASTE_SIZE_CATEGORY);
    }

    @Override
    public MarineLitterBatchRowModel createNewRow() {
        MarineLitterBatchRowModel result =
                new MarineLitterBatchRowModel(weightUnit);

        // by default empty row is not valid
        result.setValid(false);
        return result;
    }
}

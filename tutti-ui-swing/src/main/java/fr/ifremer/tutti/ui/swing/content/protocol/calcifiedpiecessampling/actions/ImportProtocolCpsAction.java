package fr.ifremer.tutti.ui.swing.content.protocol.calcifiedpiecessampling.actions;

/*
 * #%L
 * Tutti :: UI
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import fr.ifremer.tutti.persistence.entities.protocol.CalcifiedPiecesSamplingDefinition;
import fr.ifremer.tutti.persistence.entities.protocol.SpeciesProtocol;
import fr.ifremer.tutti.persistence.entities.protocol.TuttiProtocol;
import fr.ifremer.tutti.persistence.entities.referential.Species;
import fr.ifremer.tutti.service.DecoratorService;
import fr.ifremer.tutti.service.protocol.ProtocolImportExportService;
import fr.ifremer.tutti.ui.swing.content.protocol.EditProtocolSpeciesRowModel;
import fr.ifremer.tutti.ui.swing.content.protocol.EditProtocolUIModel;
import fr.ifremer.tutti.ui.swing.content.protocol.calcifiedpiecessampling.CalcifiedPiecesSamplingEditorRowModel;
import fr.ifremer.tutti.ui.swing.content.protocol.calcifiedpiecessampling.CalcifiedPiecesSamplingEditorUI;
import fr.ifremer.tutti.ui.swing.content.protocol.calcifiedpiecessampling.CalcifiedPiecesSamplingEditorUIHandler;
import fr.ifremer.tutti.ui.swing.util.actions.LongActionSupport;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

import static org.nuiton.i18n.I18n.t;

/**
 * To import protocol cps.
 *
 * @author Kevin Morin (Code Lutin)
 * @since 4.5
 */
public class ImportProtocolCpsAction extends LongActionSupport<EditProtocolUIModel, CalcifiedPiecesSamplingEditorUI, CalcifiedPiecesSamplingEditorUIHandler> {

    /** Logger. */
    private static final Log log = LogFactory.getLog(ImportProtocolCpsAction.class);

    private File file;

    private TuttiProtocol protocol;

    private Set<Species> notImportedSpecies;

    public ImportProtocolCpsAction(CalcifiedPiecesSamplingEditorUIHandler handler) {
        super(handler, false);
    }

    @Override
    public boolean prepareAction() throws Exception {

        boolean doAction = super.prepareAction();

        if (doAction) {

            // choose file to import
            file = chooseFile(
                    t("tutti.editProtocol.title.choose.cpsImportFile"),
                    t("tutti.editProtocol.action.importProtocolCpsFile"),
                    "^.*\\.csv", t("tutti.common.file.csv")
            );

            doAction = file != null;
        }
        return doAction;
    }

    @Override
    public void doAction() throws Exception {
        Preconditions.checkNotNull(file);
        if (log.isInfoEnabled()) {
            log.info("Will import protocol cps file: " + file);
        }

        EditProtocolUIModel model = getModel();

        // bind to a protocol
        protocol = model.toEntity();

        // import
        ProtocolImportExportService service =
                getContext().getTuttiProtocolImportExportService();

       notImportedSpecies = service.importCalcifiedPiecesSamplings(file,
                                                                   protocol,
                                                                   model.getAllReferentSpeciesByTaxonId());

    }

    @Override
    public void postSuccessAction() {
        super.postSuccessAction();

        // add all the cps rows
        Collection<SpeciesProtocol> allSpeciesProtocol = new ArrayList<>();
        if (!protocol.isSpeciesEmpty()) {
            allSpeciesProtocol.addAll(protocol.getSpecies());
        }
        if (!protocol.isBenthosEmpty()) {
            allSpeciesProtocol.addAll(protocol.getBenthos());
        }

        Collection<EditProtocolSpeciesRowModel> speciesRows = new ArrayList<>();
        speciesRows.addAll(getModel().getSpeciesRow());
        speciesRows.addAll(getModel().getBenthosRow());
        Map<Integer, EditProtocolSpeciesRowModel> rowsByRefTaxId = speciesRows.stream()
                .collect(Collectors.toMap(EditProtocolSpeciesRowModel::getSpeciesReferenceTaxonId,
                                          Function.identity()));

        List<CalcifiedPiecesSamplingEditorRowModel> cpsRows = new ArrayList<>();

        allSpeciesProtocol.forEach(speciesProtocol -> {

            Collection<CalcifiedPiecesSamplingDefinition> cpsDefs = speciesProtocol.getCalcifiedPiecesSamplingDefinition();
            if (cpsDefs != null) {
                cpsDefs.forEach(def -> {
                    CalcifiedPiecesSamplingEditorRowModel cpsRow = new CalcifiedPiecesSamplingEditorRowModel();
                    cpsRow.fromEntity(def);
                    cpsRow.setProtocolSpecies(rowsByRefTaxId.get(speciesProtocol.getSpeciesReferenceTaxonId()));
                    cpsRow.setValid(true);
                    cpsRows.add(cpsRow);
                });
            }
        });

        getModel().setCpsRows(cpsRows);

        if (log.isInfoEnabled()) {
            log.info("not imported cps : " + notImportedSpecies.size() + "\n" + notImportedSpecies);
        }

        sendMessage(t("tutti.flash.info.cps.imported.in.protocol", file));

        if (!notImportedSpecies.isEmpty()) {
            StringBuilder sb = new StringBuilder();
            for (Species sp : notImportedSpecies) {
                sb.append("<li>").append(decorate(sp, DecoratorService.FROM_PROTOCOL)).append("</li>");
            }
            displayWarningMessage(
                    t("tutti.editProtocol.action.importProtocolCps.speciesNotImported.title"),
                    "<html><body>" +
                    t("tutti.editProtocol.action.importProtocolSpecies.speciesNotImported", sb.toString()) +
                    "</body></html>"
            );
        }
    }

    @Override
    public void releaseAction() {
        file = null;
        protocol = null;
        notImportedSpecies = null;
        super.releaseAction();
    }
}

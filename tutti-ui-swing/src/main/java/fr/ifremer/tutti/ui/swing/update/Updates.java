package fr.ifremer.tutti.ui.swing.update;

/*
 * #%L
 * Tutti :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.TuttiConfiguration;
import fr.ifremer.tutti.ui.swing.updater.UpdateModule;
import org.nuiton.updater.ApplicationInfo;
import org.nuiton.updater.ApplicationUpdater;

import java.io.File;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * Created on 1/28/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.12.1
 */
public class Updates {


    public static ApplicationInfo getDatabaseUpdateVersion(TuttiConfiguration config) {

        ApplicationUpdater up = new ApplicationUpdater();

        String updateDataUrl = config.getUpdateDataUrl();
        File dataDirectory = config.getDataDirectory();
        Map<String, ApplicationInfo> dbVersions = up.getVersions(updateDataUrl, dataDirectory);

        return dbVersions.get(UpdateModule.db.name());

    }

    public static <C extends TuttiUpdaterCallBackSupport> void doUpdate(TuttiConfiguration config, C callback, File current) {

        File dest = new File(config.getBasedir(), "NEW");

        ApplicationUpdater up = new ApplicationUpdater();

        String url = callback.getUrl();

        up.update(url,
                  current,
                  dest,
                  false,
                  callback,
                  callback.getProgressionModel());

    }

    public static UpdateModule[] getApplicationModules() {
        Set<UpdateModule> result = new HashSet<>();
        for (UpdateModule updateModule : UpdateModule.values()) {
            if (updateModule.getModuleConfiguration().isApplication()) {
                result.add(updateModule);
            }
        }
        return result.toArray(new UpdateModule[result.size()]);
    }
}

package fr.ifremer.tutti.ui.swing.content.program.actions;

/*
 * #%L
 * Tutti :: UI
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.entities.TuttiEntities;
import fr.ifremer.tutti.persistence.entities.data.Program;
import fr.ifremer.tutti.service.PersistenceService;
import fr.ifremer.tutti.ui.swing.TuttiUIContext;
import fr.ifremer.tutti.ui.swing.util.actions.LongActionSupport;
import fr.ifremer.tutti.ui.swing.content.program.EditProgramUI;
import fr.ifremer.tutti.ui.swing.content.program.EditProgramUIHandler;
import fr.ifremer.tutti.ui.swing.content.program.EditProgramUIModel;

import static org.nuiton.i18n.I18n.t;

/**
 * @author Kevin Morin - kmorin@codelutin.com
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.0
 */
public class SaveProgramAction extends LongActionSupport<EditProgramUIModel, EditProgramUI, EditProgramUIHandler> {

    public SaveProgramAction(EditProgramUIHandler handler) {
        super(handler, true);
    }

    @Override
    public void doAction() throws Exception {
        TuttiUIContext context = getContext();
        PersistenceService persistenceService =
                getContext().getPersistenceService();

        EditProgramUIModel model = getModel();

        Program bean = model.toEntity();

        Program saved;
        if (TuttiEntities.isNew(bean)) {

            saved = persistenceService.createProgram(bean);
            model.setId(saved.getId());
            sendMessage(t("tutti.flash.info.programCreated", bean.getName()));
        } else {
            saved = persistenceService.saveProgram(bean);
            sendMessage(t("tutti.flash.info.programSaved", bean.getName()));
        }

        context.setProgramId(saved.getId());

        model.setModify(false);
    }

    @Override
    public void postSuccessAction() {
        getContext().getMainUI().getHandler().setBodyTitle(
                EditProgramUIHandler.getTitle(true));
    }

}

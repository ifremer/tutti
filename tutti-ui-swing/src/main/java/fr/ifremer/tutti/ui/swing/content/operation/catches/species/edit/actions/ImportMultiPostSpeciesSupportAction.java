package fr.ifremer.tutti.ui.swing.content.operation.catches.species.edit.actions;

/*
 * #%L
 * Tutti :: UI
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.entities.data.CatchBatch;
import fr.ifremer.tutti.persistence.entities.data.FishingOperation;
import fr.ifremer.tutti.persistence.entities.data.SpeciesBatch;
import fr.ifremer.tutti.service.catches.multipost.MultiPostImportService;
import fr.ifremer.tutti.type.WeightUnit;
import fr.ifremer.tutti.ui.swing.content.operation.catches.species.SpeciesOrBenthosBatchUISupport;
import fr.ifremer.tutti.ui.swing.content.operation.catches.actions.ImportMultiPostActionSupport;
import fr.ifremer.tutti.ui.swing.content.operation.catches.species.edit.SpeciesBatchUI;
import fr.ifremer.tutti.ui.swing.content.operation.catches.species.edit.SpeciesBatchUIHandler;
import fr.ifremer.tutti.ui.swing.content.operation.catches.species.edit.SpeciesBatchUIModel;

import java.io.File;
import java.util.Collection;
import java.util.Map;

import static org.nuiton.i18n.I18n.n;

/**
 * @author Tony Chemit - chemit@codelutin.com
 * @since 4.5
 */
public abstract class ImportMultiPostSpeciesSupportAction extends ImportMultiPostActionSupport<SpeciesBatchUIModel, SpeciesBatchUI, SpeciesBatchUIHandler> {

    protected ImportMultiPostSpeciesSupportAction(SpeciesBatchUIHandler handler) {
        super(handler);
    }

    public abstract boolean isImportFrequencies();

    public abstract boolean isImportIndivudalObservations();

    @Override
    protected final Map<String, Object> importBatches(MultiPostImportService multiPostImportExportService, File file, FishingOperation operation) {

        SpeciesOrBenthosBatchUISupport batchUISupport = getModel().getSpeciesOrBenthosBatchUISupport();
        return batchUISupport.importMultiPost(file, operation, isImportFrequencies(), isImportIndivudalObservations());

//        return multiPostImportExportService.importSpecies(file, operation, isImportFrequencies(), isImportIndivudalObservations());

    }

    @Override
    protected final String buildNotImportedDataReportText(Map<String, Object> notImportedData) {

        WeightUnit speciesWeightUnit = getConfig().getSpeciesWeightUnit();

        StringBuilder builder = new StringBuilder();

        Float totalSortedWeight = (Float) notImportedData.get(CatchBatch.PROPERTY_SPECIES_TOTAL_SORTED_WEIGHT);
        addNotImportedWeightToReport(builder, totalSortedWeight, speciesWeightUnit, n("tutti.multiPostImportLog.totalSortedWeight"));

        Float inertWeight = (Float) notImportedData.get(CatchBatch.PROPERTY_SPECIES_TOTAL_INERT_WEIGHT);
        addNotImportedWeightToReport(builder, inertWeight, speciesWeightUnit, n("tutti.multiPostImportLog.inertWeight"));

        Float livingNotItemizedWeight =
                (Float) notImportedData.get(CatchBatch.PROPERTY_SPECIES_TOTAL_LIVING_NOT_ITEMIZED_WEIGHT);
        addNotImportedWeightToReport(builder, livingNotItemizedWeight, speciesWeightUnit, n("tutti.multiPostImportLog.livingNotItemizedWeight"));

        Collection<SpeciesBatch> notImportedSpeciesBatches =
                (Collection<SpeciesBatch>) notImportedData.get(MultiPostImportService.BATCHES_KEY);

        for (SpeciesBatch sb : notImportedSpeciesBatches) {
            builder.append("- ").append(decorate(sb.getSpecies())).append(" / ").append(decorate(sb.getSampleCategoryValue())).append("\n");
        }

        return builder.toString();

    }

}

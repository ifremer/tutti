package fr.ifremer.tutti.ui.swing.update;

/*
 * #%L
 * Tutti :: UI
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ImmutableMap;
import fr.ifremer.tutti.persistence.ProgressionModel;
import fr.ifremer.tutti.ui.swing.util.actions.LongActionSupport;
import fr.ifremer.tutti.ui.swing.update.module.ModuleUpdaterSupport;
import fr.ifremer.tutti.ui.swing.update.module.ReportModuleUpdater;
import fr.ifremer.tutti.ui.swing.updater.UpdateModule;

/**
 * CallBack to update reports.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.6
 */
public class TuttiReportUpdaterCallBack extends TuttiUpdaterCallBackSupport {

    public TuttiReportUpdaterCallBack(String url, LongActionSupport action, ProgressionModel progressionModel) {
        super(url,
              ImmutableMap.<UpdateModule, ModuleUpdaterSupport>of(UpdateModule.report, new ReportModuleUpdater()),
              action,
              progressionModel);
        super.setModulesToUpdate(UpdateModule.report);
    }

    @Override
    public void setModulesToUpdate(UpdateModule... modulesToUpdate) {
        throw new IllegalStateException("You are not allowed to use this method on " + this);
    }

}

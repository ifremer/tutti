package fr.ifremer.tutti.ui.swing.content.operation.catches.species.edit;

/*
 * #%L
 * Tutti :: UI
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.nuiton.jaxx.application.swing.table.ColumnIdentifier;

import java.io.Serializable;

/**
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.2
 */
public class SampleCategoryColumnIdentifier extends ColumnIdentifier<SpeciesBatchRowModel> {

    private static final long serialVersionUID = 1L;

    public static SampleCategoryColumnIdentifier newId(String propertyName,
                                                       Integer sampleCategoryId,
                                                       String headerI18nKey,
                                                       String headerTipI18nKey) {
        return new SampleCategoryColumnIdentifier(propertyName,
                                                  sampleCategoryId,
                                                  headerI18nKey,
                                                  headerTipI18nKey);
    }

    private final Integer sampleCategoryId;

    SampleCategoryColumnIdentifier(String propertyName,
                                   Integer sampleCategoryId,
                                   String headerI18nKey,
                                   String headerTipI18nKey) {
        super(propertyName, headerI18nKey, headerTipI18nKey);
        this.sampleCategoryId = sampleCategoryId;
    }

    @Override
    public Object getValue(SpeciesBatchRowModel entry) {
        return entry.getSampleCategoryById(getSampleCategoryId());
    }

    public void setWeightValue(SpeciesBatchRowModel entry, Object value) {

        entry.setSampleCategoryWeight(getSampleCategoryId(), value);
    }

    public Serializable getCategoryValue(SpeciesBatchRowModel entry) {

        return entry.getSampleCategoryValue(getSampleCategoryId());

    }

    public void setCategoryValue(SpeciesBatchRowModel entry, Serializable value) {

        entry.setSampleCategoryValue(getSampleCategoryId(), value);
    }

    public Integer getSampleCategoryId() {
        return sampleCategoryId;
    }

}

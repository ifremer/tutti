package fr.ifremer.tutti.ui.swing.content.home;

/*
 * #%L
 * Tutti :: UI
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.entities.data.Cruise;
import fr.ifremer.tutti.persistence.entities.data.Cruises;
import fr.ifremer.tutti.persistence.entities.data.Program;
import fr.ifremer.tutti.persistence.entities.protocol.TuttiProtocol;
import fr.ifremer.tutti.ui.swing.util.AbstractTuttiBeanUIModel;

import java.util.List;

/**
 * Model of ui {@link SelectCruiseUI}.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 0.1
 */
public class SelectCruiseUIModel extends AbstractTuttiBeanUIModel<Cruise, SelectCruiseUIModel> {

    private static final long serialVersionUID = 1L;

    public static final String PROPERTY_PROGRAMS = "programs";

    public static final String PROPERTY_PROGRAM = "program";

    public static final String PROPERTY_PROGRAM_VALID = "programValid";

    public static final String PROPERTY_CRUISES = "cruises";

    public static final String PROPERTY_CRUISE = "cruise";

    public static final String PROPERTY_PROTOCOLS = "protocols";

    public static final String PROPERTY_PROTOCOL = "protocol";

    public static final String PROPERTY_PROGRAM_FOUND = "programFound";

    public static final String PROPERTY_CRUISE_FOUND = "cruiseFound";

    public static final String PROPERTY_PROTOCOL_FOUND = "protocolFound";

    protected List<Program> programs;

    protected Program program;

    protected List<Cruise> cruises;

    protected Cruise cruise;

    protected List<TuttiProtocol> protocols;

    protected TuttiProtocol protocol;

    public SelectCruiseUIModel() {
        super(null, null);
    }

    public List<Program> getPrograms() {
        return programs;
    }

    public void setPrograms(List<Program> programs) {
        Object oldValue = getPrograms();
        this.programs = programs;
        firePropertyChange(PROPERTY_PROGRAMS, oldValue, programs);
    }

    public Program getProgram() {
        return program;
    }

    public void setProgram(Program program) {
        Program oldValue = getProgram();
        this.program = program;
        firePropertyChange(PROPERTY_PROGRAM, oldValue, program);
        firePropertyChange(PROPERTY_PROGRAM_FOUND, oldValue != null, program != null);
        firePropertyChange(PROPERTY_PROGRAM_VALID, null, isProgramValid());
    }

    public boolean isProgramFound() {
        return program != null;
    }

    public boolean isProgramValid() {
        return program == null || program.getZone() != null;
    }

    public List<Cruise> getCruises() {
        return cruises;
    }

    public void setCruises(List<Cruise> cruises) {
        Object oldValue = getCruises();
        this.cruises = cruises;
        firePropertyChange(PROPERTY_CRUISES, oldValue, cruises);
    }

    public Cruise getCruise() {
        return cruise;
    }

    public void setCruise(Cruise cruise) {
        Cruise oldValue = getCruise();
        this.cruise = cruise;
        firePropertyChange(PROPERTY_CRUISE, oldValue, cruise);
        firePropertyChange(PROPERTY_CRUISE_FOUND, oldValue != null, cruise != null);
    }

    public boolean isCruiseFound() {
        return cruise != null;
    }

    public List<TuttiProtocol> getProtocols() {
        return protocols;
    }

    public void setProtocols(List<TuttiProtocol> protocols) {
        Object oldValue = getProtocols();
        this.protocols = protocols;
        firePropertyChange(PROPERTY_PROTOCOLS, oldValue, protocols);
    }

    public TuttiProtocol getProtocol() {
        return protocol;
    }

    public void setProtocol(TuttiProtocol protocol) {
        Object oldValue = getProtocol();
        this.protocol = protocol;
        firePropertyChange(PROPERTY_PROTOCOL, oldValue, protocol);
        firePropertyChange(PROPERTY_PROTOCOL_FOUND, oldValue != null, protocol != null);
    }

    public boolean isProtocolFound() {
        return protocol != null;
    }

    @Override
    protected Cruise newEntity() {
        return Cruises.newCruise();
    }
}

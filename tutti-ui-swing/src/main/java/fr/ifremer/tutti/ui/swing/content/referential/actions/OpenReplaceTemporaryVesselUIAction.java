package fr.ifremer.tutti.ui.swing.content.referential.actions;

/*
 * #%L
 * Tutti :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Lists;
import fr.ifremer.tutti.persistence.entities.referential.Vessel;
import fr.ifremer.tutti.service.PersistenceService;
import fr.ifremer.tutti.ui.swing.content.referential.ManageTemporaryReferentialUIHandler;
import fr.ifremer.tutti.ui.swing.content.referential.replace.ReplaceTemporaryVesselUI;
import fr.ifremer.tutti.ui.swing.content.referential.replace.ReplaceTemporaryVesselUIModel;
import jaxx.runtime.context.JAXXInitialContext;

import javax.swing.JButton;
import java.util.List;

import static org.nuiton.i18n.I18n.t;

/**
 * Created on 7/6/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.6
 */
public class OpenReplaceTemporaryVesselUIAction extends AbstractOpenReplaceTemporaryUIAction<Vessel, ReplaceTemporaryVesselUIModel, ReplaceTemporaryVesselUI> {

    public OpenReplaceTemporaryVesselUIAction(ManageTemporaryReferentialUIHandler handler) {
        super(handler);
    }

    @Override
    protected JButton getButton() {
        return getUI().getReplaceVesselButton();
    }

    @Override
    protected String getEntityLabel() {
        return t("tutti.common.referential.vessel");
    }

    @Override
    protected ReplaceTemporaryVesselUIModel createNewModel() {
        return new ReplaceTemporaryVesselUIModel();
    }

    @Override
    protected ReplaceTemporaryVesselUI createUI(JAXXInitialContext ctx) {
        return new ReplaceTemporaryVesselUI(ctx);
    }

    @Override
    protected List<Vessel> getTargetList(PersistenceService persistenceService) {
        return Lists.newArrayList(persistenceService.getAllVessel());
    }

    @Override
    protected List<Vessel> retainTemporaryList(PersistenceService persistenceService, List<Vessel> targetList) {
        return persistenceService.retainTemporaryVesselList(targetList);
    }

    @Override
    public void postSuccessAction() {

        getHandler().resetComboBoxAction(getUI().getVesselActionComboBox());
        super.postSuccessAction();

    }
}
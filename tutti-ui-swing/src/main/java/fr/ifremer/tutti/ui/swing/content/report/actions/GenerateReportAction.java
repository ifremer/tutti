package fr.ifremer.tutti.ui.swing.content.report.actions;

/*
 * #%L
 * Tutti :: UI
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import fr.ifremer.tutti.persistence.ProgressionModel;
import fr.ifremer.tutti.service.report.ReportGenerationRequest;
import fr.ifremer.tutti.service.report.ReportGenerationResult;
import fr.ifremer.tutti.service.report.ReportGenerationService;
import fr.ifremer.tutti.ui.swing.util.actions.LongActionSupport;
import fr.ifremer.tutti.ui.swing.content.report.ReportUI;
import fr.ifremer.tutti.ui.swing.content.report.ReportUIHandler;
import fr.ifremer.tutti.ui.swing.content.report.ReportUIModel;

import static org.nuiton.i18n.I18n.t;

/**
 * To generate the selected report.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.9
 */
public class GenerateReportAction extends LongActionSupport<ReportUIModel, ReportUI, ReportUIHandler> {

    protected ReportGenerationResult reportGenerationResult;

    public GenerateReportAction(ReportUIHandler handler) {
        super(handler, true);
    }

    @Override
    public boolean prepareAction() throws Exception {
        boolean doAction = super.prepareAction();

        if (doAction) {

            getModel().setReportGenerationResult(null);

        }

        return doAction;
    }

    @Override
    public void doAction() throws Exception {
        Preconditions.checkState(getModel().isValid());

        createProgressionModelIfRequired(1);

        ProgressionModel progressionModel = getProgressionModel();

        progressionModel.increments(t("tutti.generateReport.action.computeNbSteps"));

        ReportGenerationRequest reportGenerationRequest = getModel().toEntity();

        ReportGenerationService reportGenerationService = getContext().getReportGenerationService();

        int nbSteps = reportGenerationService.getNbSteps(reportGenerationRequest);
        progressionModel.adaptTotal(nbSteps);

        reportGenerationResult = reportGenerationService.generateReport(reportGenerationRequest, progressionModel);

    }

    @Override
    public void postSuccessAction() {

        super.postSuccessAction();
        getModel().setReportGenerationResult(reportGenerationResult);
        sendMessage(t("tutti.report.generated"));

    }

    @Override
    protected void releaseAction() {
        super.releaseAction();
        reportGenerationResult = null;
    }
}

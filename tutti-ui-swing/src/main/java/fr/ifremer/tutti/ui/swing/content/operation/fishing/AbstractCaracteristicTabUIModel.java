
package fr.ifremer.tutti.ui.swing.content.operation.fishing;

/*
 * #%L
 * Tutti :: UI
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Predicates;
import fr.ifremer.tutti.persistence.entities.CaracteristicMap;
import fr.ifremer.tutti.persistence.entities.data.FishingOperation;
import fr.ifremer.tutti.persistence.entities.referential.Caracteristic;
import fr.ifremer.tutti.ui.swing.util.AbstractTuttiBeanUIModel;
import fr.ifremer.tutti.ui.swing.util.table.AbstractTuttiTableUIModel;
import org.nuiton.jaxx.application.swing.tab.TabContentModel;

import java.io.Serializable;
import java.util.List;

/**
 * @author Kevin Morin - kmorin@codelutin.com
 * @since 1.0
 */
public abstract class AbstractCaracteristicTabUIModel<RM extends AbstractTuttiBeanUIModel<Serializable, RM>, M extends AbstractCaracteristicTabUIModel<RM, M>>
        extends AbstractTuttiTableUIModel<FishingOperation, RM, M> implements TabContentModel {

    public static final String PROPERTY_CARACTERISTIC_MAP = "caracteristicMap";

    public static final String PROPERTY_REMOVE_CARACTERISTIC_ENABLED = "removeCaracteristicEnabled";

    public static final String PROPERTY_AVAILABLE_CARACTERISTICS = "availableCaracteristics";

    private static final long serialVersionUID = 1L;

    protected List<Caracteristic> availableCaracteristics;

    /**
     * Caracteristics
     *
     * @since 1.0
     */
    protected CaracteristicMap caracteristicMap;

    /**
     * Can user remove a selected caracteristic?
     *
     * @since 1.0
     */
    protected boolean removeCaracteristicEnabled;

    public AbstractCaracteristicTabUIModel() {
        super(FishingOperation.class, null, null);
    }

    public List<Caracteristic> getAvailableCaracteristics() {
        return availableCaracteristics;
    }

    public void setAvailableCaracteristics(List<Caracteristic> availableCaracteristics) {
        Object oldValue = getAvailableCaracteristics();
        this.availableCaracteristics = availableCaracteristics;
        firePropertyChange(PROPERTY_AVAILABLE_CARACTERISTICS, oldValue, availableCaracteristics);
    }

    public CaracteristicMap getCaracteristicMap() {
        return caracteristicMap;
    }

    public void setCaracteristicMap(CaracteristicMap caracteristicMap) {
        Object oldValue = getCaracteristicMap();
        this.caracteristicMap = caracteristicMap != null ? (CaracteristicMap) caracteristicMap.clone() : null;
        firePropertyChange(PROPERTY_CARACTERISTIC_MAP, oldValue, this.caracteristicMap);
    }

    public boolean isRemoveCaracteristicEnabled() {
        return removeCaracteristicEnabled;
    }

    public void setRemoveCaracteristicEnabled(boolean removeCaracteristicEnabled) {
        Object oldValue = isRemoveCaracteristicEnabled();
        this.removeCaracteristicEnabled = removeCaracteristicEnabled;
        firePropertyChange(PROPERTY_REMOVE_CARACTERISTIC_ENABLED, oldValue, removeCaracteristicEnabled);
    }

    @Override
    public boolean isEmpty() {
        return caracteristicMap == null || caracteristicMap.values().stream().allMatch(Predicates.isNull()::apply);
    }

    @Override
    public String getIcon() {
        return null;
    }

    @Override
    protected FishingOperation newEntity() {
        return null;
    }

    @Override
    public boolean isCloseable() {
        return false;
    }
}

package fr.ifremer.tutti.ui.swing.content.operation.catches.species.edit;

/*
 * #%L
 * Tutti :: UI
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.ui.swing.util.AbstractTuttiUIHandler;
import fr.ifremer.tutti.ui.swing.util.TuttiUIUtil;
import jaxx.runtime.SwingUtil;
import jaxx.runtime.validator.swing.SwingValidator;

import javax.swing.JComponent;

/**
 * @author Kevin Morin - kmorin@codelutin.com
 * @since 4.3
 */
public class ImportPupitriMelagWeightPopupUIHandler extends AbstractTuttiUIHandler<ImportPupitriMelagWeightPopupUIModel, ImportPupitriMelagWeightPopupUI> {

    @Override
    public void beforeInit(ImportPupitriMelagWeightPopupUI ui) {
        super.beforeInit(ui);
        ui.setContextValue(new ImportPupitriMelagWeightPopupUIModel());
    }

    @Override
    public void afterInit(ImportPupitriMelagWeightPopupUI ui) {
        initUI(ui);
        ui.pack();
        ui.setResizable(true);
    }

    @Override
    protected JComponent getComponentToFocus() {
        return ui.getValidateButton();
    }

    @Override
    public void onCloseUI() {
        getModel().setTotalMelagWeight(null);
        getModel().setComputedTotalMelagWeight(null);
    }

    @Override
    public SwingValidator<ImportPupitriMelagWeightPopupUIModel> getValidator() {
        return null;
    }

    public void open(float initialWeight) {
        ui.getModel().setComputedTotalMelagWeight(initialWeight);

        SwingUtil.center(TuttiUIUtil.getApplicationContext(ui).getMainUI(), ui);
        ui.setVisible(true);
    }

}

package fr.ifremer.tutti.ui.swing.content.cruise;

/*
 * #%L
 * Tutti :: UI
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Lists;
import fr.ifremer.adagio.core.dao.referential.ObjectTypeCode;
import fr.ifremer.tutti.persistence.entities.data.Attachment;
import fr.ifremer.tutti.persistence.entities.data.Cruise;
import fr.ifremer.tutti.persistence.entities.data.Cruises;
import fr.ifremer.tutti.persistence.entities.data.Program;
import fr.ifremer.tutti.persistence.entities.referential.GearWithOriginalRankOrder;
import fr.ifremer.tutti.persistence.entities.referential.Person;
import fr.ifremer.tutti.persistence.entities.referential.TuttiLocation;
import fr.ifremer.tutti.persistence.entities.referential.Vessel;
import fr.ifremer.tutti.ui.swing.util.AbstractTuttiBeanUIModel;
import fr.ifremer.tutti.ui.swing.util.attachment.AttachmentModelAware;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.nuiton.util.DateUtil;
import org.nuiton.util.beans.Binder;
import org.nuiton.util.beans.BinderFactory;

import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.List;

/**
 * Model of UI {@link EditCruiseUI}.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 0.1
 */
public class EditCruiseUIModel extends AbstractTuttiBeanUIModel<Cruise, EditCruiseUIModel> implements AttachmentModelAware, Cruise {

    private static final long serialVersionUID = 1L;

    public static final String PROPERTY_CAN_GENERATE_NAME = "canGenerateName";

    public static final String PROPERTY_CAN_SHOW_GEAR_CARACTERISTIC = "canShowGearCatacteristic";

    public static final String PROPERTY_CAN_EDIT_GEAR_CARACTERISTIC = "canEditGearCatacteristic";

    public static final String PROPERTY_VESSEL_TYPE = "vesselType";

    public static final String PROPERTY_VESSEL_TYPE_ALL = "vesselTypeAll";

    public static final String PROPERTY_VESSEL_TYPE_SCIENTIFIC = "vesselTypeScientific";

    public static final String PROPERTY_VESSEL_TYPE_FISHING = "vesselTypeFishing";

    public static final String PROPERTY_VALIDATION_CONTEXT = "validationContext";

    /**
     * Delegate edit object.
     *
     * @since 1.3
     */
    protected final Cruise editObject = Cruises.newCruise();

    /**
     * Type of vessel.
     *
     * @see VesselTypeEnum
     * @since 1.0
     */
    protected VesselTypeEnum vesselType;

    /**
     * Can edit caracteristics of selected gear (for any temporary gear or professional one)
     * <strong>only for a none modified cruise.</strong>?
     *
     * @since 2.6
     */
    protected boolean canEditGearCatacteristic;

    /**
     * Can show caracteristics of selected gear? Any time so should be keep it?
     *
     * @since 2.6
     */
    protected boolean canShowGearCatacteristic;

    /**
     * To switch validation context (edit or validate).
     *
     * @since 3.6
     */
    protected String validationContext;

    protected final List<Attachment> attachment = Lists.newArrayList();

    protected static Binder<EditCruiseUIModel, Cruise> toBeanBinder =
            BinderFactory.newBinder(EditCruiseUIModel.class,
                                    Cruise.class);

    protected static Binder<Cruise, EditCruiseUIModel> fromBeanBinder =
            BinderFactory.newBinder(Cruise.class, EditCruiseUIModel.class);

    public EditCruiseUIModel() {
        super(fromBeanBinder, toBeanBinder);
    }

    @Override
    protected Cruise newEntity() {
        return Cruises.newCruise();
    }

    public boolean isCanEditGearCatacteristic() {
        return canEditGearCatacteristic;
    }

    public void setCanEditGearCatacteristic(boolean canEditGearCatacteristic) {
        this.canEditGearCatacteristic = canEditGearCatacteristic;
        firePropertyChange(PROPERTY_CAN_EDIT_GEAR_CARACTERISTIC, null, canEditGearCatacteristic);
    }

    public boolean isCanShowGearCatacteristic() {
        return canShowGearCatacteristic;
    }

    public void setCanShowGearCatacteristic(boolean canShowGearCatacteristic) {
        this.canShowGearCatacteristic = canShowGearCatacteristic;
        firePropertyChange(PROPERTY_CAN_SHOW_GEAR_CARACTERISTIC, null, canShowGearCatacteristic);
    }

    public VesselTypeEnum getVesselType() {
        return vesselType;
    }

    public void setVesselType(VesselTypeEnum vesselType) {
        boolean oldAll = isVesselTypeAll();
        boolean oldScientific = isVesselTypeScientific();
        boolean oldFishing = isVesselTypeFishing();
        Object oldValue = getVesselType();
        this.vesselType = vesselType;
        firePropertyChange(PROPERTY_VESSEL_TYPE, oldValue, vesselType);
        firePropertyChange(PROPERTY_VESSEL_TYPE_ALL, oldAll, isVesselTypeAll());
        firePropertyChange(PROPERTY_VESSEL_TYPE_SCIENTIFIC, oldScientific, isVesselTypeScientific());
        firePropertyChange(PROPERTY_VESSEL_TYPE_FISHING, oldFishing, isVesselTypeFishing());
    }

    public boolean isVesselTypeAll() {
        return VesselTypeEnum.ALL.equals(vesselType);
    }

    public boolean isVesselTypeScientific() {
        return VesselTypeEnum.SCIENTIFIC.equals(vesselType);
    }

    public boolean isVesselTypeFishing() {
        return VesselTypeEnum.FISHING.equals(vesselType);
    }

    public boolean isCanGenerateName() {
        return getProgram() != null && getBeginDate() != null;
    }

    public static final String CRUISE_NAME_FORMAT = "%1$s_%2$s";

    public String getGeneratedCampaignName() {
        int year;
        if (getBeginDate() == null) {
            year = 0;
        } else {
            Calendar c = Calendar.getInstance();
            c.setTime(getBeginDate());
            year = c.get(Calendar.YEAR);
        }
        String result = String.format(CRUISE_NAME_FORMAT, getProgram().getName(), year);
        if (StringUtils.isNotEmpty(getSurveyPart())) {
            result += "_" + getSurveyPart();
        }
        return result;
    }

    public String getValidationContext() {
        return validationContext;
    }

    public void setValidationContext(String validationContext) {
        Object oldValue = getValidationContext();
        this.validationContext = validationContext;
        firePropertyChange(PROPERTY_VALIDATION_CONTEXT, oldValue, validationContext);
    }

    //------------------------------------------------------------------------//
    //-- AttachmentModelAware methods                                       --//
    //------------------------------------------------------------------------//

    @Override
    public ObjectTypeCode getObjectType() {
        return ObjectTypeCode.SCIENTIFIC_CRUISE;
    }

    @Override
    public Integer getObjectId() {
        return getIdAsInt();
    }

    @Override
    public List<Attachment> getAttachment() {
        return attachment;
    }

    @Override
    public void addAllAttachment(Collection<Attachment> attachments) {
        this.attachment.addAll(attachments);
        firePropertyChange(PROPERTY_ATTACHMENT, null, getAttachment());
    }

    @Override
    public void addAttachment(Attachment attachment) {
        this.attachment.add(attachment);
        firePropertyChange(PROPERTY_ATTACHMENT, null, getAttachment());
    }

    @Override
    public void removeAllAttachment(Collection<Attachment> attachments) {
        this.attachment.removeAll(attachments);
        firePropertyChange(PROPERTY_ATTACHMENT, null, getAttachment());
    }

    @Override
    public void removeAttachment(Attachment attachment) {
        this.attachment.remove(attachment);
        firePropertyChange(PROPERTY_ATTACHMENT, null, getAttachment());
    }

    //------------------------------------------------------------------------//
    //-- Cruise methods                                                     --//
    //------------------------------------------------------------------------//

    @Override
    public Program getProgram() {
        return editObject.getProgram();
    }

    @Override
    public void setProgram(Program program) {
        Object oldValue = getProgram();
        editObject.setProgram(program);
        firePropertyChange(PROPERTY_PROGRAM, oldValue, program);
        firePropertyChange(PROPERTY_CAN_GENERATE_NAME, null, isCanGenerateName());
    }

    @Override
    public String getName() {
        return editObject.getName();
    }

    @Override
    public void setName(String name) {
        Object oldValue = getName();
        editObject.setName(name);
        firePropertyChange(PROPERTY_NAME, oldValue, name);
    }

    @Override
    public String getComment() {
        return editObject.getComment();
    }

    @Override
    public void setComment(String comment) {
        Object oldValue = getComment();
        editObject.setComment(comment);
        firePropertyChange(PROPERTY_COMMENT, oldValue, comment);
    }

    @Override
    public TuttiLocation getDepartureLocation() {
        return editObject.getDepartureLocation();
    }

    @Override
    public void setDepartureLocation(TuttiLocation departureLocation) {
        Object oldValue = getDepartureLocation();
        editObject.setDepartureLocation(departureLocation);
        firePropertyChange(PROPERTY_DEPARTURE_LOCATION, oldValue, departureLocation);
    }

    @Override
    public TuttiLocation getReturnLocation() {
        return editObject.getReturnLocation();
    }

    @Override
    public void setReturnLocation(TuttiLocation returnLocation) {
        Object oldValue = getReturnLocation();
        editObject.setReturnLocation(returnLocation);
        firePropertyChange(PROPERTY_RETURN_LOCATION, oldValue, returnLocation);
    }

    @Override
    public String getSurveyPart() {
        return editObject.getSurveyPart();
    }

    @Override
    public void setSurveyPart(String surveyPart) {
        Object oldValue = getSurveyPart();
        editObject.setSurveyPart(surveyPart);
        firePropertyChange(PROPERTY_SURVEY_PART, oldValue, surveyPart);
        firePropertyChange(PROPERTY_CAN_GENERATE_NAME, null, isCanGenerateName());
    }

    @Override
    public Integer getMultirigNumber() {
        return editObject.getMultirigNumber();
    }

    @Override
    public void setMultirigNumber(Integer multirigNumber) {
        Object oldValue = getMultirigNumber();
        editObject.setMultirigNumber(multirigNumber);
        firePropertyChange(PROPERTY_MULTIRIG_NUMBER, oldValue, multirigNumber);
    }

    @Override
    public Date getBeginDate() {
        return editObject.getBeginDate();
    }

    @Override
    public void setBeginDate(Date beginDate) {
        Date oldValue = getBeginDate();
        Date newDate = beginDate;
        if (oldValue != null && beginDate != null) {
            // reapply time from previous date
            newDate = applyTime(beginDate, oldValue);
        }
        setBeginDate0(newDate);
    }

    @Override
    public Date getEndDate() {
        return editObject.getEndDate();
    }

    @Override
    public void setEndDate(Date endDate) {
        Date oldValue = getEndDate();
        Date newDate = endDate;
        if (oldValue != null && endDate != null) {
            // reapply time from previous date
            newDate = applyTime(endDate, oldValue);
        }
        setEndDate0(newDate);
    }

    @Override
    public Vessel getVessel() {
        return editObject.getVessel();
    }

    @Override
    public void setVessel(Vessel vessel) {
        Object oldValue = getVessel();
        editObject.setVessel(vessel);
        firePropertyChange(PROPERTY_VESSEL, oldValue, vessel);
    }

    @Override
    public String getSynchronizationStatus() {
        return editObject.getSynchronizationStatus();
    }

    @Override
    public void setSynchronizationStatus(String synchronizationStatus) {
        String oldValue = getSynchronizationStatus();
        editObject.setSynchronizationStatus(synchronizationStatus);
        firePropertyChange(PROPERTY_SYNCHRONIZATION_STATUS, oldValue, synchronizationStatus);
    }

    @Override
    public List<GearWithOriginalRankOrder> getGear() {
        return editObject.getGear();
    }

    @Override
    public void setGear(List<GearWithOriginalRankOrder> gear) {
        editObject.setGear(gear);
        firePropertyChange(PROPERTY_GEAR, null, gear);
    }

    @Override
    public List<Person> getHeadOfMission() {
        return editObject.getHeadOfMission();
    }

    @Override
    public void setHeadOfMission(List<Person> headOfMission) {
        editObject.setHeadOfMission(headOfMission);
        firePropertyChange(PROPERTY_HEAD_OF_MISSION, null, headOfMission);
    }

    @Override
    public List<Person> getHeadOfSortRoom() {
        return editObject.getHeadOfSortRoom();
    }

    @Override
    public void setHeadOfSortRoom(List<Person> headOfSortRoom) {
        editObject.setHeadOfSortRoom(headOfSortRoom);
        firePropertyChange(PROPERTY_HEAD_OF_SORT_ROOM, null, headOfSortRoom);
    }

    @Override
    public GearWithOriginalRankOrder getGear(int index) {
        return editObject.getGear(index);
    }

    @Override
    public boolean isGearEmpty() {
        return editObject.isGearEmpty();
    }

    @Override
    public int sizeGear() {
        return editObject.sizeGear();
    }

    @Override
    public void addGear(GearWithOriginalRankOrder gear) {
        editObject.addGear(gear);
    }

    @Override
    public void addAllGear(Collection<GearWithOriginalRankOrder> gear) {
        editObject.addAllGear(gear);
    }

    @Override
    public boolean removeGear(GearWithOriginalRankOrder gear) {
        return editObject.removeGear(gear);
    }

    @Override
    public boolean removeAllGear(Collection<GearWithOriginalRankOrder> gear) {
        return editObject.removeAllGear(gear);
    }

    @Override
    public boolean containsGear(GearWithOriginalRankOrder gear) {
        return editObject.containsGear(gear);
    }

    @Override
    public boolean containsAllGear(Collection<GearWithOriginalRankOrder> gear) {
        return editObject.containsAllGear(gear);
    }

    @Override
    public Person getHeadOfMission(int index) {
        return null;
    }

    @Override
    public boolean isHeadOfMissionEmpty() {
        return false;
    }

    @Override
    public int sizeHeadOfMission() {
        return 0;
    }

    @Override
    public void addHeadOfMission(Person headOfMission) {
    }

    @Override
    public void addAllHeadOfMission(Collection<Person> headOfMission) {
    }

    @Override
    public boolean removeHeadOfMission(Person headOfMission) {
        return false;
    }

    @Override
    public boolean removeAllHeadOfMission(Collection<Person> headOfMission) {
        return false;
    }

    @Override
    public boolean containsHeadOfMission(Person headOfMission) {
        return false;
    }

    @Override
    public boolean containsAllHeadOfMission(Collection<Person> headOfMission) {
        return false;
    }

    @Override
    public Person getHeadOfSortRoom(int index) {
        return null;
    }

    @Override
    public boolean isHeadOfSortRoomEmpty() {
        return false;
    }

    @Override
    public int sizeHeadOfSortRoom() {
        return 0;
    }

    @Override
    public void addHeadOfSortRoom(Person headOfSortRoom) {
    }

    @Override
    public void addAllHeadOfSortRoom(Collection<Person> headOfSortRoom) {
    }

    @Override
    public boolean removeHeadOfSortRoom(Person headOfSortRoom) {
        return false;
    }

    @Override
    public boolean removeAllHeadOfSortRoom(Collection<Person> headOfSortRoom) {
        return false;
    }

    @Override
    public boolean containsHeadOfSortRoom(Person headOfSortRoom) {
        return false;
    }

    @Override
    public boolean containsAllHeadOfSortRoom(Collection<Person> headOfSortRoom) {
        return false;
    }

    //------------------------------------------------------------------------//
    //-- Start - End Time ----------------------------------------------------//
    //------------------------------------------------------------------------//

    public void setBeginTime(Date beginTime) {
        Date oldValue = getBeginDate();
        if (oldValue != null && beginTime != null) {
            // apply time to previous date
            Date newDate = applyTime(oldValue, beginTime);
            setBeginDate0(newDate);
        }
    }

    public void setEndTime(Date endTime) {
        Date oldValue = getEndDate();
        if (oldValue != null && endTime != null) {
            // apply time to previous date
            Date newDate = applyTime(oldValue, endTime);
            setEndDate0(newDate);
        }
    }

    //------------------------------------------------------------------------//
    //-- Protected methods ---------------------------------------------------//
    //------------------------------------------------------------------------//

    protected Date applyTime(Date date, Date timeDate) {

        Calendar cal = DateUtils.toCalendar(timeDate);
        Date newDate = DateUtils.setHours(date, cal.get(Calendar.HOUR_OF_DAY));
        newDate = DateUtils.setMinutes(newDate, cal.get(Calendar.MINUTE));

        return newDate;
    }

    protected void setBeginDate0(Date beginDate) {
        Date oldValue = getBeginDate();
        if (beginDate != null) {
            Calendar calendar = DateUtil.getDefaultCalendar(beginDate);
            calendar.set(Calendar.SECOND, 0);
            editObject.setBeginDate(calendar.getTime());

        } else {
            editObject.setBeginDate(null);
        }
        firePropertyChange(PROPERTY_BEGIN_DATE, oldValue, getBeginDate());
        firePropertyChange(PROPERTY_CAN_GENERATE_NAME, null, isCanGenerateName());
    }

    protected void setEndDate0(Date endDate) {
        Object oldValue = getEndDate();
        if (endDate != null) {
            Calendar calendar = DateUtil.getDefaultCalendar(endDate);
            calendar.set(Calendar.SECOND, 0);
            editObject.setEndDate(calendar.getTime());

        } else {
            editObject.setEndDate(null);
        }
        firePropertyChange(PROPERTY_END_DATE, oldValue, getEndDate());
    }
}

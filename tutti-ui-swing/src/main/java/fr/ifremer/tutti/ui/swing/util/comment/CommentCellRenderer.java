package fr.ifremer.tutti.ui.swing.util.comment;

/*
 * #%L
 * Tutti :: UI
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import jaxx.runtime.SwingUtil;
import org.apache.commons.lang3.StringUtils;

import javax.swing.JComponent;
import javax.swing.JTable;
import javax.swing.UIManager;
import javax.swing.table.DefaultTableCellRenderer;
import java.awt.Font;

import static org.nuiton.i18n.I18n.n;
import static org.nuiton.i18n.I18n.t;

/**
 * To render a comment in a table cell.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.1
 */
public class CommentCellRenderer extends DefaultTableCellRenderer {

    public static final String TEXT_PATTERN = "<html><body>%s</body></html>";

    private static final long serialVersionUID = 1L;

    private final String noneText;

    protected Font defaulfFont;

    protected Font selectedFont;


    public static CommentCellRenderer newRender() {
        return new CommentCellRenderer();
    }

    protected CommentCellRenderer() {
        setHorizontalAlignment(CENTER);
        setIcon(SwingUtil.createActionIcon("edit-comment"));
        this.noneText = n("tutti.commentEditor.none.tip");
    }

    @Override
    protected void setValue(Object value) {
        // do nothing
    }

    @Override
    public JComponent getTableCellRendererComponent(JTable table,
                                                    Object value,
                                                    boolean isSelected,
                                                    boolean hasFocus,
                                                    int row,
                                                    int column) {

        String comment = (String) value;

        String toolTipTextValue;

        if (StringUtils.isEmpty(comment)) {

            // use HTML to show the tooltip in italic
            toolTipTextValue = "<i>" + t(noneText) + "</i>";


        } else {

            // use html to display the tooltip on several lines
            toolTipTextValue = String.valueOf(value).replace("\n", "<br/>");

        }
        boolean editable = table.isCellEditable(row, column);
        toolTipTextValue = String.format(TEXT_PATTERN, toolTipTextValue);
        setEnabled(editable);
        setToolTipText(toolTipTextValue);
        setBackground(null);

        if (defaulfFont == null) {
            defaulfFont = UIManager.getFont("Table.font");
            selectedFont = defaulfFont.deriveFont(Font.BOLD);
        }

        if (isSelected) {
            setFont(selectedFont);
        } else {
            setFont(defaulfFont);
        }

        return this;
    }
}

package fr.ifremer.tutti.ui.swing.content.operation.catches.species.edit.actions;

/*
 * #%L
 * Tutti :: UI
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.entities.data.CatchBatch;
import fr.ifremer.tutti.persistence.entities.data.FishingOperation;
import fr.ifremer.tutti.service.PersistenceService;
import fr.ifremer.tutti.service.bigfin.BigfinImportResult;
import fr.ifremer.tutti.service.bigfin.BigfinImportService;
import fr.ifremer.tutti.ui.swing.content.operation.FishingOperationsUI;
import fr.ifremer.tutti.ui.swing.content.operation.catches.EditCatchesUIModel;
import fr.ifremer.tutti.ui.swing.content.operation.catches.species.edit.SpeciesBatchUI;
import fr.ifremer.tutti.ui.swing.content.operation.catches.species.edit.SpeciesBatchUIHandler;
import fr.ifremer.tutti.ui.swing.content.operation.catches.species.edit.SpeciesBatchUIModel;
import fr.ifremer.tutti.ui.swing.content.operation.fishing.actions.EditFishingOperationAction;
import fr.ifremer.tutti.ui.swing.util.actions.LongActionSupport;

import java.io.File;

import static org.nuiton.i18n.I18n.t;

/**
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.0
 */
public class ImportBigfinAction extends LongActionSupport<SpeciesBatchUIModel, SpeciesBatchUI, SpeciesBatchUIHandler> {

    protected File importedBigfinFile;

    protected PersistenceService persistenceService;

    protected EditFishingOperationAction editAction;

    protected BigfinImportResult importResult;

    public ImportBigfinAction(SpeciesBatchUIHandler handler) {
        super(handler, false);
        persistenceService = getContext().getPersistenceService();
    }


    public EditFishingOperationAction getEditAction() {
        if (editAction == null) {
            FishingOperationsUI parentContainer = handler.getParentContainer(FishingOperationsUI.class);
            editAction = getContext().getActionFactory().createLogicAction(parentContainer.getHandler(),
                                                                           EditFishingOperationAction.class);
        }
        return editAction;
    }

    @Override
    public boolean prepareAction() throws Exception {
        boolean result = super.prepareAction();

        if (result) {
            // choose file to import
            importedBigfinFile = chooseFile(
                    t("tutti.editSpeciesBatch.action.title.choose.importBigfinFile"),
                    t("tutti.editSpeciesBatch.action.chooseBigfinFile.import"),
                    "^.*\\.csv", t("tutti.common.file.csv"));

            result = importedBigfinFile != null;
        }
        return result;
    }

    @Override
    public void doAction() throws Exception {
        BigfinImportService importService = getContext().getTuttiBigfinImportService();

        EditCatchesUIModel model = getModel().getCatchesUIModel();

        FishingOperation operation = model.getFishingOperation();
        CatchBatch catchBatch = model.toEntity();

        // import
        importResult = importService.importFile(importedBigfinFile, operation, catchBatch);

        if (importResult.isDone()) {

            // reload operation
            getEditAction().loadCatchBatch(operation);
        }
    }

    @Override
    public void releaseAction() {
        super.releaseAction();
        importedBigfinFile = null;
    }

    @Override
    public void postSuccessAction() {
        super.postSuccessAction();

        if (importResult.isDone()) {

            sendMessage(t("tutti.editSpeciesBatch.action.importBigfin.success",
                          importResult.getNbFrequenciesImported(),
                          importResult.getNbFrequenciesDeleted()));

            if (!importResult.getWarnings().isEmpty()) {
                StringBuilder sb = new StringBuilder();
                for (String s : importResult.getWarnings()) {
                    sb.append("<li>").append(s).append("</li>");
                }
                displayWarningMessage(
                        t("tutti.editSpeciesBatch.action.importBigfin.errors.fishingOperation.title"),
                        "<html><body>" +
                        t("tutti.editSpeciesBatch.action.importBigfin.errors.fishingOperation", sb.toString()) +
                        "</body></html>"
                );
            }

        } else {

            StringBuilder sb = new StringBuilder();
            for (String s : importResult.getErrors()) {
                sb.append("<li>").append(s).append("</li>");
            }
            displayWarningMessage(
                    t("tutti.editSpeciesBatch.action.importBigfin.fatal.fishingOperation.title"),
                    "<html><body>" +
                    t("tutti.editSpeciesBatch.action.importBigfin.fatal.fishingOperation", sb.toString()) +
                    "</body></html>"
            );
            sendMessage(t("tutti.editSpeciesBatch.action.importBigfin.fatal.data"));
        }
    }
}

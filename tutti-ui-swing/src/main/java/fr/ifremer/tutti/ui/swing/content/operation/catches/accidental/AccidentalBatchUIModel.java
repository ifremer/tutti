package fr.ifremer.tutti.ui.swing.content.operation.catches.accidental;

/*
 * #%L
 * Tutti :: UI
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.ui.swing.content.operation.catches.AbstractTuttiBatchUIModel;
import fr.ifremer.tutti.ui.swing.content.operation.catches.EditCatchesUIModel;
import org.nuiton.jaxx.application.swing.tab.TabContentModel;
import org.apache.commons.collections4.CollectionUtils;

import static org.nuiton.i18n.I18n.n;

/**
 * @author Tony Chemit - chemit@codelutin.com
 * @since 0.2
 */
public class AccidentalBatchUIModel extends AbstractTuttiBatchUIModel<AccidentalBatchRowModel, AccidentalBatchUIModel>
        implements TabContentModel {

    private static final long serialVersionUID = 1L;

    public static final String PROPERTY_REMOVE_BATCH_ENABLED = "removeBatchEnabled";

    /** Can user remove a selected batch? */
    protected boolean removeBatchEnabled;

    public AccidentalBatchUIModel(EditCatchesUIModel catchesUIModel) {
        super(catchesUIModel);
    }

    public boolean isRemoveBatchEnabled() {
        return removeBatchEnabled;
    }

    public void setRemoveBatchEnabled(boolean removeBatchEnabled) {
        this.removeBatchEnabled = removeBatchEnabled;
        firePropertyChange(PROPERTY_REMOVE_BATCH_ENABLED, null, removeBatchEnabled);
    }

    //------------------------------------------------------------------------//
    //-- TabContentModel                                                    --//
    //------------------------------------------------------------------------//

    @Override
    public boolean isEmpty() {
        boolean result = true;
        if (CollectionUtils.isNotEmpty(getRows())) {

            // check if every line is not valid
            for (AccidentalBatchRowModel row : rows) {
                if (row.isValid()) {

                    // found a valid row so not empty
                    result = false;
                    break;
                }
            }
        }
        return result;
    }

    @Override
    public String getTitle() {
        return n("tutti.label.tab.accidental");
    }

    @Override
    public String getIcon() {
        return null;
    }

    @Override
    public boolean isCloseable() {
        return false;
    }

}

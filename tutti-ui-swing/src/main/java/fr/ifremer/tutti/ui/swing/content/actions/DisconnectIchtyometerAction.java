package fr.ifremer.tutti.ui.swing.content.actions;

/*
 * #%L
 * Tutti :: UI
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.ichtyometer.feed.IchtyometerFeedReader;
import fr.ifremer.tutti.ui.swing.content.MainUIHandler;
import org.apache.commons.io.IOUtils;

import static org.nuiton.i18n.I18n.t;

/**
 * Close the connection to ichtyometer.
 *
 * Created on 1/29/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.1
 */
public class DisconnectIchtyometerAction extends AbstractMainUITuttiAction {

    public DisconnectIchtyometerAction(MainUIHandler handler) {
        super(handler, false);
    }

    @Override
    public void doAction() throws Exception {

        IchtyometerFeedReader ichtyometerReader = getContext().getIchtyometerReader();

        // there is a feed reader, stop it now
        IOUtils.closeQuietly(ichtyometerReader);
        getContext().setIchtyometerReader(null);

        sendMessage(t("tutti.ichtyometer.connection.stop", ichtyometerReader.getClientName()));
    }
}

package fr.ifremer.tutti.ui.swing.content.operation.catches.species.frequency;

/*
 * #%L
 * Tutti :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2016 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.entities.data.CopyIndividualObservationMode;
import fr.ifremer.tutti.type.WeightUnit;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.Objects;

/**
 * Pour générer ce qui doit être recopié depuis une observation individuelle vers les mensurations lors d'une
 * modification d'une observation individuelle.
 *
 * Created on 15/04/16.
 *
 * @author Tony Chemit - chemit@codelutin.com
 */
public class IndividualObservationToFrequencyEngine {

    /** Logger. */
    private static final Log log = LogFactory.getLog(IndividualObservationToFrequencyEngine.class);

    /**
     * Le modèle du tableau des mensurations.
     */
    private final SpeciesFrequencyTableModel frequencyTableModel;

    private final WeightUnit individualObservationWeightUnit;
    private final WeightUnit frequencyWeightUnit;

    public IndividualObservationToFrequencyEngine(SpeciesFrequencyUIModel speciesFrequencyUIModel) {
        this.individualObservationWeightUnit = speciesFrequencyUIModel.getIndividualObservationWeightUnit();
        this.frequencyWeightUnit = speciesFrequencyUIModel.getWeightUnit();
        this.frequencyTableModel = speciesFrequencyUIModel.getFrequencyTableModel();
    }

    public boolean computeFrequencyUpdate(CopyIndividualObservationMode copyIndividualObservationMode,
                                          IndividualObservationBatchRowState oldState,
                                          IndividualObservationBatchRowState newState) {

        switch (copyIndividualObservationMode) {

            case ALL:
                computeFrequencyUpdateForSizeAndWeight(oldState, newState);

                break;

            case SIZE:
                computeFrequencyUpdateForSizeOnly(oldState, newState);

                break;

            case NOTHING:

                break;

            default:
                throw new IllegalStateException("Can't come here");

        }

        return oldState.isValid() != newState.isValid();

    }

    public void computeFrequencyUpdateForSizeOnly(IndividualObservationBatchRowState oldState, IndividualObservationBatchRowState newState) {

        Objects.requireNonNull(oldState);
        Objects.requireNonNull(newState);

        Float oldSize = oldState.getSize();
        Float newSize = newState.getSize();
        boolean sizeChanged = !Objects.equals(oldSize, newSize);

        if (sizeChanged) {

            Float decrementSize = null;
            Float incrementSize = null;

            if (oldState.isValid()) {
                decrementSize = oldSize;
            }

            if (newState.isValid()) {
                incrementSize = newSize;
            }

            apply(decrementSize, incrementSize, null, null);

        }

    }

    public void computeFrequencyUpdateForSizeAndWeight(IndividualObservationBatchRowState oldState, IndividualObservationBatchRowState newState) {

        Objects.requireNonNull(oldState);
        Objects.requireNonNull(newState);

        Float newSize = newState.getSize();
        Float oldSize = oldState.getSize();
        boolean sizeChanged = !Objects.equals(oldSize, newSize);

        Float newWeight = newState.getWeight();
        Float oldWeight = oldState.getWeight();
        boolean weightChanged = !Objects.equals(oldWeight, newWeight);

        if (!sizeChanged && !weightChanged) {

            // ce cas limite peut arrive quand on passe de null à null
            return;

        }

        boolean oldStateValid = oldState.isValid();
        boolean newStateValid = newState.isValid();

        Float decrementSize = null;
        Float incrementSize = null;

        Pair<Float, Float> substractWeight = null;
        Pair<Float, Float> addWeight = null;

        if (sizeChanged) {

            if (oldStateValid) {

                decrementSize = oldSize;
                substractWeight = Pair.of(oldSize, oldWeight);

            }

            if (newStateValid) {

                incrementSize = newSize;
                addWeight = Pair.of(newSize, newWeight);

            }

        }

        if (weightChanged) {

            // la classe de taille n'a pas changée, on calcule le delta de poids à ajouter ou supprimer sur la classe de taille

            Float weightToAdd;

            if (oldStateValid && newStateValid) {

                weightToAdd = newWeight - oldWeight;

            } else if (oldStateValid) {

                decrementSize = oldSize;
                weightToAdd = -oldWeight;

            } else if (newStateValid) {

                incrementSize = newSize;
                weightToAdd = newWeight;

            } else {

                weightToAdd = 0f;

            }

            if (individualObservationWeightUnit.isNotNullNorZero(weightToAdd)) {

                if (individualObservationWeightUnit.isGreaterThanZero(weightToAdd)) {

                    // Ajout
                    addWeight = Pair.of(newSize, weightToAdd);

                } else {

                    // Suppression
                    substractWeight = Pair.of(newSize, -weightToAdd);

                }

            }

        }

        apply(decrementSize, incrementSize, substractWeight, addWeight);

    }

    private void apply(Float lengthStepToDecrement, Float lengthStepToIncrement, Pair<Float, Float> substractWeight, Pair<Float, Float> addWeight) {

        if (lengthStepToDecrement != null) {

            if (log.isInfoEnabled()) {
                log.info("Decrements frequency rows number for length class: " + lengthStepToDecrement);
            }
            frequencyTableModel.decrementFrequencyRowsNumbers(lengthStepToDecrement);

        }

        if (lengthStepToIncrement != null) {

            if (log.isInfoEnabled()) {
                log.info("Increments frequency rows number for length class: " + lengthStepToIncrement);
            }
            frequencyTableModel.incrementFrequencyRowsNumbers(lengthStepToIncrement);

        }

        if (substractWeight != null) {

            float weight = substractWeight.getValue();
            float weightToRemove = frequencyTableModel.convertWeightFromIndividualObservation(weight);
            Float lengthClass = substractWeight.getKey();
            if (log.isInfoEnabled()) {
                log.info("Substract weight: " + weightToRemove + frequencyWeightUnit.getShortLabel() + " for length class: " + lengthClass);
            }
            frequencyTableModel.removeWeightToFrequencyRow(lengthClass, weightToRemove);

        }

        if (addWeight != null) {

            float weight = addWeight.getValue();
            float weightToAdd = frequencyTableModel.convertWeightFromIndividualObservation(weight);
            Float lengthClass = addWeight.getKey();
            if (log.isInfoEnabled()) {
                log.info("Add weight: " + weightToAdd + frequencyWeightUnit.getShortLabel() + " for length class: " + lengthClass);
            }
            frequencyTableModel.addWeightToFrequencyRow(lengthClass, weightToAdd);

        }

    }

}

package fr.ifremer.tutti.ui.swing.content.db.actions;

/*
 * #%L
 * Tutti :: UI
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.TuttiConfiguration;
import fr.ifremer.tutti.persistence.ProgressionModel;
import fr.ifremer.tutti.ui.swing.TuttiUIContext;
import fr.ifremer.tutti.ui.swing.content.actions.AbstractMainUITuttiAction;
import fr.ifremer.tutti.ui.swing.content.MainUIHandler;
import fr.ifremer.tutti.ui.swing.update.TuttiDbUpdaterCallBack;
import fr.ifremer.tutti.ui.swing.update.Updates;
import fr.ifremer.tutti.ui.swing.util.AbstractTuttiUIHandler;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.updater.ApplicationInfo;

import javax.swing.JOptionPane;
import java.io.File;

import static org.nuiton.i18n.I18n.t;

/**
 * To update - install database.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.0
 */
public class UpdateDbAction extends AbstractMainUITuttiAction {

    /** Logger. */
    private static final Log log = LogFactory.getLog(UpdateDbAction.class);

    protected ApplicationInfo updateDbVersion;

    public UpdateDbAction(MainUIHandler handler) {
        super(handler, true);
        setActionDescription(t("tutti.dbManager.action.upgradeDb.tip"));
    }

    @Override
    public boolean prepareAction() throws Exception {
        boolean doAction = super.prepareAction();

        updateDbVersion = null;

        if (doAction) {
            // check db url is reachable
            doAction = getContext().checkUpdateDataReachable(true);
        }

        if (doAction) {

            // get the next db version
            updateDbVersion = Updates.getDatabaseUpdateVersion(getConfig());

            if (getContext().isDbExist() &&
                updateDbVersion != null &&
                updateDbVersion.newVersion != null) {

                // ask user if it wants to do the update
                String htmlMessage = String.format(
                        AbstractTuttiUIHandler.CONFIRMATION_FORMAT,
                        t("tutti.dbManager.updatedb.found", updateDbVersion.newVersion),
                        t("tutti.common.askBeforeUpdate.help"));
                int i = JOptionPane.showConfirmDialog(
                        getHandler().getUI(),
                        htmlMessage,
                        t("tutti.dbManager.title.confirm.updatedb"),
                        JOptionPane.OK_CANCEL_OPTION,
                        JOptionPane.QUESTION_MESSAGE);

                doAction = i == JOptionPane.OK_OPTION;
            }
        }
        return doAction;
    }

    @Override
    public void doAction() {
        TuttiUIContext context = getContext();
        TuttiConfiguration config = getConfig();

        File current = config.getDataDirectory();
        String url = config.getUpdateDataUrl();

        if (log.isInfoEnabled()) {
            log.info(String.format("Try to install / update db (current data location: %s), using update url: %s", current, url));
        }

//        File dest = new File(config.getBasedir(), "NEW");
        ProgressionModel progressionModel = new ProgressionModel();
        context.getActionUI().getModel().setProgressionModel(progressionModel);
        progressionModel.setMessage(t("tutti.dbManager.action.upgradeDb.check"));

        TuttiDbUpdaterCallBack callback = new TuttiDbUpdaterCallBack(url, this, progressionModel);

        Updates.doUpdate(config, callback, current);

//        ApplicationUpdater up = new ApplicationUpdater();
//
//        up.update(url,
//                  current,
//                  dest,
//                  false,
//                  callback,
//                  progressionModel);

        if (callback.isDbUpdated()) {

            sendMessage(t("tutti.dbManager.action.upgradeDb.done", updateDbVersion.newVersion));

        } else {
            sendMessage(t("tutti.dbManager.action.upgradeDb.upToDate"));
        }
    }

    @Override
    public void postSuccessAction() {
        handler.reloadDbManagerText();
        super.postSuccessAction();
    }

    @Override
    public void postFailedAction(Throwable error) {
        handler.reloadDbManagerText();
        super.postFailedAction(error);
    }
}

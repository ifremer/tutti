package fr.ifremer.tutti.ui.swing.content.cruise.actions;

/*
 * #%L
 * Tutti :: UI
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Sets;
import fr.ifremer.tutti.persistence.entities.TuttiEntities;
import fr.ifremer.tutti.persistence.entities.data.Cruise;
import fr.ifremer.tutti.persistence.entities.referential.Gear;
import fr.ifremer.tutti.persistence.entities.referential.GearWithOriginalRankOrder;
import fr.ifremer.tutti.persistence.entities.referential.GearWithOriginalRankOrders;
import fr.ifremer.tutti.service.PersistenceService;
import fr.ifremer.tutti.ui.swing.TuttiUIContext;
import fr.ifremer.tutti.ui.swing.content.cruise.EditCruiseUI;
import fr.ifremer.tutti.ui.swing.content.cruise.EditCruiseUIHandler;
import fr.ifremer.tutti.ui.swing.content.cruise.EditCruiseUIModel;
import fr.ifremer.tutti.ui.swing.util.AbstractTuttiUIHandler;
import fr.ifremer.tutti.ui.swing.util.TuttiUIUtil;
import fr.ifremer.tutti.ui.swing.util.actions.LongActionSupport;
import org.apache.commons.lang3.ObjectUtils;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import java.util.Set;

import static org.nuiton.i18n.I18n.t;

/**
 * Saves a cruise
 *
 * @author Kevin Morin - kmorin@codelutin.com
 * @since 1.0
 */
public class SaveCruiseAction extends LongActionSupport<EditCruiseUIModel, EditCruiseUI, EditCruiseUIHandler> {

    protected PersistenceService persistenceService;

    protected boolean updateVessel;

    protected boolean updateGear;

    public SaveCruiseAction(EditCruiseUIHandler handler) {
        super(handler, true);
        persistenceService = getContext().getPersistenceService();
    }

    @Override
    public boolean prepareAction() throws Exception {
        boolean result = super.prepareAction();

        Cruise bean = getModel().toEntity();
        if (!TuttiEntities.isNew(bean)) {
            Cruise oldCruise = persistenceService.getCruise(bean.getIdAsInt());

            // check vessel has not changed
            if (ObjectUtils.notEqual(oldCruise.getVessel(), bean.getVessel())) {

                // if so ask user confirmation
                String htmlMessage = String.format(
                        AbstractTuttiUIHandler.CONFIRMATION_FORMAT,
                        t("tutti.editCruise.action.save.vesselChanged.message"),
                        t("tutti.editCruise.action.save.vesselChanged.help"));

                int answer = JOptionPane.showConfirmDialog(getContext().getActionUI(),
                                                           htmlMessage,
                                                           t("tutti.editCruise.action.save.vesselChanged.title"),
                                                           JOptionPane.OK_CANCEL_OPTION,
                                                           JOptionPane.WARNING_MESSAGE);

                result = updateVessel = answer == JOptionPane.OK_OPTION;
            }

            if (result) {

                if (ObjectUtils.notEqual(oldCruise.getGear(), bean.getGear())) {

                    // get all gears to remove

                    Set<Gear> gearsToRemove =
                            Sets.<Gear>newHashSet(oldCruise.getGear());

                    for (GearWithOriginalRankOrder gearWithOriginalRankOrder : bean.getGear()) {
                        GearWithOriginalRankOrder g =
                                GearWithOriginalRankOrders.newGearWithOriginalRankOrder(
                                        gearWithOriginalRankOrder.getIdAsInt(),
                                        gearWithOriginalRankOrder.getOriginalRankOrder()
                                );
                        gearsToRemove.remove(g);
                    }

                    // check they are not used by any operation of the cruise
                    boolean obsoleteGearIsUsed = persistenceService.isOperationUseGears(
                            bean.getIdAsInt(), gearsToRemove);

                    if (obsoleteGearIsUsed) {

                        // not possible, can't removed used gears

                        String message = t("tutti.persistence.cruise.gearUsedInOperations.error");
                        displayWarningMessage(
                                t("tutti.editCruise.action.save.gearChanged.title"),
                                "<html><body>" + message + "</body></html>"
                        );
                        result = false;
                    } else {

                        // if so ask user confirmation
                        String htmlMessage = String.format(
                                AbstractTuttiUIHandler.CONFIRMATION_FORMAT,
                                t("tutti.editCruise.action.save.gearChanged.message"),
                                t("tutti.editCruise.action.save.gearChanged.help"));

                        int answer = JOptionPane.showConfirmDialog(getContext().getActionUI(),
                                                                   htmlMessage,
                                                                   t("tutti.editCruise.action.save.gearChanged.title"),
                                                                   JOptionPane.OK_CANCEL_OPTION,
                                                                   JOptionPane.WARNING_MESSAGE);

                        result = updateGear = answer == JOptionPane.OK_OPTION;
                    }
                }
            }
        }

        return result;
    }

    @Override
    public void doAction() throws Exception {
        TuttiUIContext context = getContext();
        EditCruiseUIModel model = getModel();

        Cruise bean = model.toEntity();

        Cruise saved;
        boolean createCruise = TuttiEntities.isNew(bean);
        if (createCruise) {

            saved = persistenceService.createCruise(bean);
            model.setId(saved.getId());
            sendMessage(t("tutti.flash.info.cruiseCreated", bean.getName()));
        } else {
            saved = persistenceService.saveCruise(bean, updateVessel, updateGear);
            sendMessage(t("tutti.flash.info.cruiseSaved", bean.getName()));
        }

        context.setProgramId(saved.getProgram().getId());
        context.setCruiseId(saved.getIdAsInt());

        if (!createCruise) {

            // reload cruise
            getDataContext().reloadCruise();
        }

        getDataContext().closeCruiseCache();

        // update originalRankOrder for all gears of the cruise
        for (GearWithOriginalRankOrder gear : model.getGear()) {
            gear.setOriginalRankOrder(gear.getRankOrder());
        }
        // update SynchronizationStatus
        model.setSynchronizationStatus(saved.getSynchronizationStatus());
        model.setModify(false);
    }

    @Override
    public void postSuccessAction() {

        getContext().getMainUI().getHandler().setBodyTitle(EditCruiseUIHandler.getTitle(true));

        ImageIcon icon = TuttiUIUtil.getCruiseIcon(getModel());
        getContext().getMainUI().getBody().setLeftDecoration(new JLabel(icon));
    }
}

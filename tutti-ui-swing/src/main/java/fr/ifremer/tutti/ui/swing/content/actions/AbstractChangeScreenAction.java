package fr.ifremer.tutti.ui.swing.content.actions;

/*
 * #%L
 * Tutti :: UI
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.ProgressionModel;
import fr.ifremer.tutti.ui.swing.TuttiScreen;
import fr.ifremer.tutti.ui.swing.TuttiUIContext;
import fr.ifremer.tutti.ui.swing.content.MainUIHandler;
import jaxx.runtime.SwingUtil;
import jaxx.runtime.context.JAXXContextEntryDef;

import java.beans.PropertyVetoException;

import static org.nuiton.i18n.I18n.t;

/**
 * Action to change the screen.
 *
 * Will just check that the current screen can be quit via
 * the {@link MainUIHandler#quitCurrentScreen()}.
 *
 * @author Kevin Morin - kmorin@codelutin.com
 * @since 1.0
 */
public abstract class AbstractChangeScreenAction extends AbstractMainUITuttiAction {

    /**
     * Context entry to keep previous screen.
     *
     * @since 1.1
     */
    protected static final JAXXContextEntryDef<TuttiScreen> PREVIOUS_SCREEN =
            SwingUtil.newContextEntryDef("previousScreen", TuttiScreen.class);

    /**
     * Screen where to go.
     *
     * @since 1.0
     */
    protected TuttiScreen screen;

    /**
     * Flag to skip the check of current screen.
     *
     * @since 1.1
     */
    protected boolean skipCheckCurrentScreen;

    protected AbstractChangeScreenAction(MainUIHandler handler, boolean hideBody, TuttiScreen screen) {
        super(handler, hideBody);
        this.screen = screen;
    }

    public void setSkipCheckCurrentScreen(boolean skipCheckCurrentScreen) {
        this.skipCheckCurrentScreen = skipCheckCurrentScreen;
    }

    protected void setScreen(TuttiScreen screen) {
        this.screen = screen;
    }

    @Override
    public boolean prepareAction() throws Exception {
        boolean result = super.prepareAction();
        result &= skipCheckCurrentScreen || getHandler().quitCurrentScreen();
        return result;
    }

    @Override
    public void doAction() throws Exception {

        TuttiUIContext context = getContext();

        TuttiScreen previousScreen = context.getScreen();
        if (getUI() != null) {
            if (previousScreen == null) {
                PREVIOUS_SCREEN.removeContextValue(getUI());
            } else {
                PREVIOUS_SCREEN.setContextValue(getUI(), previousScreen);
            }
        }

        // clean current screen
        context.setScreen(null);

        // change screen
        context.setScreen(screen);
    }

    @Override
    public void postFailedAction(Throwable error) {
        if (error != null && !(error instanceof PropertyVetoException)) {

            getContext().setFallBackScreen();
        }
    }

    protected void loadReferantials(boolean createProgressionModel) {


        ProgressionModel progressionModel;

        if (createProgressionModel) {

            progressionModel = new ProgressionModel();
            progressionModel.setTotal(5);
            setProgressionModel(progressionModel);

        } else {

            progressionModel = getProgressionModel();

        }

        progressionModel.increments(t("tutti.openScreen.step.loading.allGear"));
        getContext().getPersistenceService().getAllGear();

        progressionModel.increments(t("tutti.openScreen.step.loading.allPerson"));
        getContext().getPersistenceService().getAllPerson();

        progressionModel.increments(t("tutti.openScreen.step.loading.allSpecies"));
        getContext().getPersistenceService().getAllSpecies();

        progressionModel.increments(t("tutti.openScreen.step.loading.allVessel"));
        getContext().getPersistenceService().getAllVessel();

        progressionModel.increments(t("tutti.openScreen.step.loading.ui"));

    }

    protected void loadReferantialsWithObsoletes(boolean createProgressionModel) {

        ProgressionModel progressionModel;

        if (createProgressionModel) {

            progressionModel = new ProgressionModel();
            progressionModel.setTotal(9);
            setProgressionModel(progressionModel);

        } else {

            progressionModel = getProgressionModel();

        }

        progressionModel.increments(t("tutti.openScreen.step.loading.allGear"));
        getContext().getPersistenceService().getAllGear();

        progressionModel.increments(t("tutti.openScreen.step.loading.allGearWithObsoletes"));
        getContext().getPersistenceService().getAllGearWithObsoletes();

        progressionModel.increments(t("tutti.openScreen.step.loading.allPerson"));
        getContext().getPersistenceService().getAllPerson();

        progressionModel.increments(t("tutti.openScreen.step.loading.allPersonWithObsoletes"));
        getContext().getPersistenceService().getAllPersonWithObsoletes();

        progressionModel.increments(t("tutti.openScreen.step.loading.allSpecies"));
        getContext().getPersistenceService().getAllSpecies();

        progressionModel.increments(t("tutti.openScreen.step.loading.allSpeciesWithObsoletes"));
        getContext().getPersistenceService().getAllReferentSpeciesWithObsoletes();

        progressionModel.increments(t("tutti.openScreen.step.loading.allVessel"));
        getContext().getPersistenceService().getAllVessel();

        progressionModel.increments(t("tutti.openScreen.step.loading.allVesselWithObsoletes"));
        getContext().getPersistenceService().getAllVesselWithObsoletes();

        progressionModel.increments(t("tutti.openScreen.step.loading.ui"));

    }

}

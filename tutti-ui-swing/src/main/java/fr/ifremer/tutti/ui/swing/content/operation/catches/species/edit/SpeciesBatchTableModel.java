package fr.ifremer.tutti.ui.swing.content.operation.catches.species.edit;

/*
 * #%L
 * Tutti :: UI
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import fr.ifremer.tutti.persistence.entities.data.SampleCategory;
import fr.ifremer.tutti.persistence.entities.data.SampleCategoryModel;
import fr.ifremer.tutti.persistence.entities.referential.Species;
import fr.ifremer.tutti.type.WeightUnit;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jdesktop.swingx.table.TableColumnModelExt;
import org.nuiton.jaxx.application.swing.table.AbstractApplicationTableModel;
import org.nuiton.jaxx.application.swing.table.ColumnIdentifier;

import javax.swing.table.TableColumn;
import java.io.Serializable;
import java.util.Enumeration;
import java.util.List;
import java.util.Set;

import static org.nuiton.i18n.I18n.n;

/**
 * @author Tony Chemit - chemit@codelutin.com
 * @since 0.1
 */
public class SpeciesBatchTableModel extends AbstractApplicationTableModel<SpeciesBatchRowModel> {

    private static final long serialVersionUID = 1L;

    /** Logger. */
    private static final Log log = LogFactory.getLog(SpeciesBatchTableModel.class);

    public static final ColumnIdentifier<SpeciesBatchRowModel> ID = ColumnIdentifier.newId(
            SpeciesBatchRowModel.PROPERTY_ID,
            n("tutti.editSpeciesBatch.table.header.id"),
            n("tutti.editSpeciesBatch.table.header.id.tip"));

    public static final ColumnIdentifier<SpeciesBatchRowModel> SPECIES = ColumnIdentifier.newId(
            SpeciesBatchRowModel.PROPERTY_SPECIES_ROW,
            n("tutti.editSpeciesBatch.table.header.species"),
            n("tutti.editSpeciesBatch.table.header.species.tip"));

    public static final ColumnIdentifier<SpeciesBatchRowModel> WEIGHT = ColumnIdentifier.newId(
            SpeciesBatchRowModel.PROPERTY_COMPUTED_WEIGHT,
            n("tutti.editSpeciesBatch.table.header.weight"),
            n("tutti.editSpeciesBatch.table.header.weight.tip"));

    public static final ColumnIdentifier<SpeciesBatchRowModel> COMPUTED_NUMBER = ColumnIdentifier.newId(
            SpeciesBatchRowModel.PROPERTY_COMPUTED_NUMBER,
            n("tutti.editSpeciesBatch.table.header.computedNumber"),
            n("tutti.editSpeciesBatch.table.header.computedNumber.tip"));

    public static final ColumnIdentifier<SpeciesBatchRowModel> COMMENT = ColumnIdentifier.newId(
            SpeciesBatchRowModel.PROPERTY_COMMENT,
            n("tutti.editSpeciesBatch.table.header.comment"),
            n("tutti.editSpeciesBatch.table.header.comment.tip"));

    public static final ColumnIdentifier<SpeciesBatchRowModel> ATTACHMENT = ColumnIdentifier.newReadOnlyId(
            SpeciesBatchRowModel.PROPERTY_ATTACHMENT,
            n("tutti.editSpeciesBatch.table.header.file"),
            n("tutti.editSpeciesBatch.table.header.file.tip"));

    public static final ColumnIdentifier<SpeciesBatchRowModel> SPECIES_TO_CONFIRM = ColumnIdentifier.newId(
            SpeciesBatchRowModel.PROPERTY_SPECIES_TO_CONFIRM,
            "", /* I18n n'est pas capable de rendre une traduction vide et on le veut (voir https://forge.codelutin.com/issues/6067) */
            n("tutti.editSpeciesBatch.table.header.toConfirm.tip"));

    /**
     * Columns for the frequency edition.
     *
     * @since 0.2
     */
    protected final Set<ColumnIdentifier<SpeciesBatchRowModel>> frequencyCols;

    /**
     * Columns implied in the sample category definition.
     *
     * @since 0.2
     */
    protected final Set<SampleCategoryColumnIdentifier> sampleCols;

    /**
     * Sample categories model.
     *
     * @since 2.4
     */
    protected final SampleCategoryModel sampleCategoryModel;

    /**
     * Weight unit.
     *
     * @since 2.5
     */
    protected final WeightUnit weightUnit;

    public SpeciesBatchTableModel(WeightUnit weightUnit,
                                  SampleCategoryModel sampleCategoryModel,
                                  TableColumnModelExt columnModel) {
        super(columnModel, false, false);
        this.weightUnit = weightUnit;
        this.sampleCategoryModel = sampleCategoryModel;
        setNoneEditableCols(ID, SPECIES);

        frequencyCols = Sets.newHashSet();
        frequencyCols.add(COMPUTED_NUMBER);
        frequencyCols.add(WEIGHT);

        Set<SampleCategoryColumnIdentifier> sampleCols = Sets.newLinkedHashSet();

        Enumeration<TableColumn> columns = columnModel.getColumns();
        while (columns.hasMoreElements()) {
            TableColumn tableColumn = columns.nextElement();
            Object identifier = tableColumn.getIdentifier();
            if (identifier instanceof SampleCategoryColumnIdentifier) {
                sampleCols.add((SampleCategoryColumnIdentifier) identifier);
            }
        }
        this.sampleCols = ImmutableSet.copyOf(sampleCols);
    }

    public Set<SampleCategoryColumnIdentifier> getSampleCols() {
        return sampleCols;
    }

    @Override
    protected void collectShell(SpeciesBatchRowModel row,
                                Set<SpeciesBatchRowModel> collectedRows) {
        super.collectShell(row, collectedRows);

        if (!row.isChildBatchsEmpty()) {
            for (SpeciesBatchRowModel child : row.getChildBatch()) {
                collectShell(child, collectedRows);
            }
        }
    }

    @Override
    public SpeciesBatchRowModel createNewRow() {
        SpeciesBatchRowModel result =
                new SpeciesBatchRowModel(weightUnit, sampleCategoryModel);

        // by default empty row is not valid
        result.setValid(false);
        return result;
    }

    @Override
    public void setValueAt(Object aValue,
                           int rowIndex,
                           int columnIndex,
                           ColumnIdentifier<SpeciesBatchRowModel> propertyName,
                           SpeciesBatchRowModel entry) {
        if (sampleCols.contains(propertyName)) {

            SampleCategoryColumnIdentifier sampleCategoryColumnIdentifier = (SampleCategoryColumnIdentifier) propertyName;
            sampleCategoryColumnIdentifier.setWeightValue(entry, aValue);

            // must find out first ancestor with this category
            Integer sampleCategoryId = sampleCategoryColumnIdentifier.getSampleCategoryId();
            if (log.isDebugEnabled()) {
                log.debug("Sample category: " + sampleCategoryId + " modified at row: " + rowIndex);
            }
            SampleCategory<?> sampleCategory = entry.getSampleCategoryById(sampleCategoryId);
            SpeciesBatchRowModel firstAncestor = entry.getFirstAncestor(sampleCategory);

            int firstRowIndex = getRowIndex(firstAncestor);
            if (log.isDebugEnabled()) {
                log.debug("First ancestor row: " + firstRowIndex);
            }

            // save this row and his shell
            updateShell(firstAncestor, columnIndex);

        } else {
            super.setValueAt(aValue, rowIndex, columnIndex, propertyName, entry);
        }
    }

    @Override
    protected boolean isCellEditable(int rowIndex,
                                     int columnIndex,
                                     ColumnIdentifier<SpeciesBatchRowModel> propertyName) {

        boolean result = super.isCellEditable(rowIndex,
                                              columnIndex,
                                              propertyName);
        if (result) {

            if (frequencyCols.contains(propertyName)) {

                // must have filled a species to edit this column
                SpeciesBatchRowModel entry = getEntry(rowIndex);
                result = entry.isBatchLeaf();

            } else if (sampleCols.contains(propertyName)) {

                // can only edit if a category value is setted
                SpeciesBatchRowModel entry = getEntry(rowIndex);

                // check from protocol what is possible ?
                Species species = entry.getSpecies();

                if (species == null) {

                    // no species, can not edit
                    result = false;
                } else {

                    // protocol authorize it

                    // final test: can edit only if sample category is setted
                    SampleCategory<?> value =
                            (SampleCategory<?>) propertyName.getValue(entry);

                    result = value.getCategoryValue() != null;

                }
            }
        }
        return result;
    }

    /**
     * Return the next editable row index for frequency from the given
     * {@code rowIndex}, or {@code null} if none found.
     *
     * @param rowIndex the starting index where to look
     * @return the next editable row index for frequency from the given {@code rowIndex}, or {@code null} if none found.
     * @since 2.5
     */
    public Integer getNextEditableFrequencyRow(Integer rowIndex) {
        Integer result = null;

        for (int i = rowIndex, max = getRowCount(); i < max; i++) {
            SpeciesBatchRowModel entry = getEntry(i);
            if (entry.isBatchLeaf()) {
                result = i;
                break;
            }
        }
        return result;
    }

    /**
     * Return the index of the next row that can be inserted as a child of the
     * given row.
     *
     * @param row the parent row
     * @return the index of the next row that can be inserted as a child of the
     * given row
     * @since 2.7
     */
    public final int getNextChildRowIndex(SpeciesBatchRowModel row) {

        // get row index of the given row
        int parentRowIndex = getRowIndex(row);

        // must find it
        Preconditions.checkState(getRowIndex(row) != -1);

        int result = parentRowIndex;

        if (!row.isChildBatchsEmpty()) {

            // take his last child
            SpeciesBatchRowModel lastChild =
                    row.getChildBatch().get(row.sizeChildBatchs() - 1);

            // get the shell of this child (including himself)
            Set<SpeciesBatchRowModel> childs = Sets.newHashSet();
            childs.add(lastChild);
            lastChild.collectShell(childs);

            // get row with max index
            for (SpeciesBatchRowModel child : childs) {
                int childRowIndex = getRowIndex(child);
                result = Math.max(childRowIndex, result);
            }
        }

        // result is the nex row
        result++;

        return result;
    }

    /**
     * Return the sample category id of a column or {@code null} if not on a
     * sample category column.
     *
     * @param columnIndex index of the column to look at
     * @return the sample category id of a column or {@code null} if not on a
     * sample category column.
     * @since 2.6
     */
    public Integer getSampleCategoryId(int columnIndex) {
        Integer result = null;
        ColumnIdentifier<SpeciesBatchRowModel> identifier = getIdentifier(columnIndex);
        if (sampleCols.contains(identifier)) {
            SampleCategoryColumnIdentifier sampleId = (SampleCategoryColumnIdentifier) identifier;
            result = sampleId.getSampleCategoryId();
        }
        return result;
    }

    /**
     * Update the sample category value of the given {@code row}.
     *
     * @param row         the row to walk through
     * @param columnIndex index of the column where the sample category is
     * @param newValue    new sample category value to set
     * @return the old category value replaced by the new one
     * @since 2.6
     */
    public Serializable updateSampleCategorieValue(SpeciesBatchRowModel row,
                                                   int columnIndex,
                                                   Serializable newValue) {

        SampleCategoryColumnIdentifier identifier = (SampleCategoryColumnIdentifier) getIdentifier(columnIndex);

        Serializable oldValue = identifier.getCategoryValue(row);

        // set category value
        identifier.setCategoryValue(row, newValue);

        // update row shell
        updateShell(row, columnIndex);

        return oldValue;

    }

    /**
     * Return previous sibling row, or {@code null} if not exist.
     *
     * @param row where to start
     * @return the previous sibling row, or {@code null} if not exist.
     * @since 2.6
     */
    public SpeciesBatchRowModel getPreviousSibling(SpeciesBatchRowModel row) {
        SpeciesBatchRowModel result = null;
        SpeciesBatchRowModel parentBatch = row.getParentBatch();
        if (parentBatch != null) {
            List<SpeciesBatchRowModel> childBatch = parentBatch.getChildBatch();
            int i = childBatch.indexOf(row);
            if (i > 0) {
                result = childBatch.get(i - 1);
            }
        }
        return result;
    }

    /**
     * Update all the given cells for the  given column.
     *
     * @param shell       shell to update
     * @param columnIndex the column index to update
     * @since 2.6
     */
    public void updateShell(Set<SpeciesBatchRowModel> shell, int columnIndex) {
        for (SpeciesBatchRowModel batchRowModel : shell) {
            int currentRowIndex = getRowIndex(batchRowModel);
            if (log.isDebugEnabled()) {
                log.debug("Update shell row: " + currentRowIndex);
            }
            fireTableCellUpdated(currentRowIndex, columnIndex);
        }
    }

    /**
     * Return the list of used species in the table (used to fill the
     * comparator cache for species sort)
     *
     * @return the list of used species in the table.
     * @since 2.8
     */
    public List<Species> getSpeciesList() {
        List<Species> result = Lists.newArrayList();
        for (SpeciesBatchRowModel row : rows) {
            if (row.isBatchRoot()) {
                result.add(row.getSpecies());
            }
        }
        return result;
    }

    /**
     * Update all the cells of the given row shell for a given column.
     *
     * @param entry       the first row to update
     * @param columnIndex the column index to update
     * @since 2.6
     */
    protected void updateShell(SpeciesBatchRowModel entry, int columnIndex) {

        Set<SpeciesBatchRowModel> shell = Sets.newHashSet();
        entry.collectShell(shell);
        shell.add(entry);
        updateShell(shell, columnIndex);
    }
}

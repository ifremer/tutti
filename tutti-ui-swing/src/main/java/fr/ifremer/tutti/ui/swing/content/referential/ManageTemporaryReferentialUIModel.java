package fr.ifremer.tutti.ui.swing.content.referential;

/*
 * #%L
 * Tutti :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.jdesktop.beans.AbstractSerializableBean;

/**
 * @author Kevin Morin (Code Lutin)
 * @since 3.8
 */
public class ManageTemporaryReferentialUIModel extends AbstractSerializableBean {

    public static final String PROPERTY_NB_TEMPORARY_SPECIES = "nbTemporarySpecies";

    public static final String PROPERTY_NB_TEMPORARY_VESSELS = "nbTemporaryVessels";

    public static final String PROPERTY_NB_TEMPORARY_GEARS = "nbTemporaryGears";

    public static final String PROPERTY_NB_TEMPORARY_PERSONS = "nbTemporaryPersons";

    private static final long serialVersionUID = 1L;

    protected int nbTemporarySpecies;

    protected int nbTemporaryVessels;

    protected int nbTemporaryGears;

    protected int nbTemporaryPersons;

    public int getNbTemporarySpecies() {
        return nbTemporarySpecies;
    }

    public void setNbTemporarySpecies(int nbTemporarySpecies) {
        Integer oldValue = getNbTemporarySpecies();
        this.nbTemporarySpecies = nbTemporarySpecies;
        firePropertyChange(PROPERTY_NB_TEMPORARY_SPECIES, oldValue, nbTemporarySpecies);
    }

    public int getNbTemporaryVessels() {
        return nbTemporaryVessels;
    }

    public void setNbTemporaryVessels(int nbTemporaryVessels) {
        Integer oldValue = getNbTemporaryVessels();
        this.nbTemporaryVessels = nbTemporaryVessels;
        firePropertyChange(PROPERTY_NB_TEMPORARY_VESSELS, oldValue, nbTemporaryVessels);
    }

    public int getNbTemporaryGears() {
        return nbTemporaryGears;
    }

    public void setNbTemporaryGears(int nbTemporaryGears) {
        Integer oldValue = getNbTemporaryGears();
        this.nbTemporaryGears = nbTemporaryGears;
        firePropertyChange(PROPERTY_NB_TEMPORARY_GEARS, oldValue, nbTemporaryGears);
    }

    public int getNbTemporaryPersons() {
        return nbTemporaryPersons;
    }

    public void setNbTemporaryPersons(int nbTemporaryPersons) {
        Integer oldValue = getNbTemporaryPersons();
        this.nbTemporaryPersons = nbTemporaryPersons;
        firePropertyChange(PROPERTY_NB_TEMPORARY_PERSONS, oldValue, nbTemporaryPersons);
    }
}

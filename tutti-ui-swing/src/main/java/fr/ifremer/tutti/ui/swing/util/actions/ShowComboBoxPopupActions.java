package fr.ifremer.tutti.ui.swing.util.actions;

/*
 * #%L
 * Tutti :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import javax.swing.JComboBox;
import java.util.TimerTask;

/**
 * Created on 4/4/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.14.4
 */
public class ShowComboBoxPopupActions extends TimerTask {

    private final JComboBox comboBox;

    public ShowComboBoxPopupActions(JComboBox comboBox) {
        this.comboBox = comboBox;
    }

    @Override
    public void run() {

        if (comboBox.isShowing()) {
            comboBox.showPopup();
        }
    }

}

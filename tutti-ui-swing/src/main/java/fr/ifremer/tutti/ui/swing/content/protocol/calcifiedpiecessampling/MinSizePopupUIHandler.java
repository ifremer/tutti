package fr.ifremer.tutti.ui.swing.content.protocol.calcifiedpiecessampling;

/*
 * #%L
 * Tutti :: UI
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.ui.swing.util.AbstractTuttiUIHandler;
import jaxx.runtime.SwingUtil;
import jaxx.runtime.validator.swing.SwingValidator;

import javax.swing.JComponent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import static org.nuiton.i18n.I18n.t;

/**
 * @author Kevin Morin - kmorin@codelutin.com
 * @since 4.5
 */
public class MinSizePopupUIHandler extends AbstractTuttiUIHandler<MinSizePopupUIModel, MinSizePopupUI> {

    @Override
    public void afterInit(MinSizePopupUI ui) {
        initUI(ui);

        ui.getMinSizeField().getTextField().addKeyListener(new KeyAdapter() {

            @Override
            public void keyReleased(KeyEvent e) {
                super.keyReleased(e);
                if (e.getKeyCode() == KeyEvent.VK_ENTER) {
                    validate();
                }
            }
        });
    }

    @Override
    public void onCloseUI() {
        getUI().dispose();
    }

    @Override
    public SwingValidator<MinSizePopupUIModel> getValidator() {
        return getUI().getValidator();
    }

    @Override
    protected JComponent getComponentToFocus() {
        return getUI().getMinSizeField();
    }

    public void open(int minMinSize, Integer maxMinSize) {
        getModel().setMinMinSize(minMinSize);
        getModel().setMaxMinSize(maxMinSize);
        getModel().setValid(false);

        String message;
        if (maxMinSize != null) {
            message = t("tutti.cpsEditor.dialog.minSize.message", minMinSize, maxMinSize);

        } else {
            message = t("tutti.cpsEditor.dialog.minSize.message.infinite", minMinSize);
        }
        getUI().getMessage().setText(message);

        getUI().pack();
        SwingUtil.center(getContext().getMainUI(), ui);
        getUI().setVisible(true);
    }

    public void validate() {
        if (getValidator().isValid()) {
            getModel().setValid(true);
            onCloseUI();
        }
    }

    public void cancel() {
        getModel().setMinSize(null);
        onCloseUI();
    }
}

package fr.ifremer.tutti.ui.swing.content.protocol.calcifiedpiecessampling;

/*
 * #%L
 * Tutti :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2016 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.entities.protocol.CalcifiedPiecesSamplingDefinition;
import fr.ifremer.tutti.persistence.entities.protocol.CalcifiedPiecesSamplingDefinitions;
import fr.ifremer.tutti.persistence.entities.referential.Species;
import fr.ifremer.tutti.service.DecoratorService;
import fr.ifremer.tutti.ui.swing.content.protocol.EditProtocolSpeciesRowModel;
import fr.ifremer.tutti.ui.swing.util.AbstractTuttiBeanUIModel;
import org.nuiton.util.beans.Binder;
import org.nuiton.util.beans.BinderFactory;

import java.util.Comparator;

/**
 * @author Kevin Morin (Code Lutin)
 * @since 4.5
 */
public class CalcifiedPiecesSamplingEditorRowModel extends AbstractTuttiBeanUIModel<CalcifiedPiecesSamplingDefinition,
                                                                                    CalcifiedPiecesSamplingEditorRowModel>
                                                   implements CalcifiedPiecesSamplingDefinition {

    public static final String PROPERTY_PROTOCOL_SPECIES = "protocolSpecies";

    public static final Comparator<CalcifiedPiecesSamplingEditorRowModel> COMPARATOR =
            new Comparator<CalcifiedPiecesSamplingEditorRowModel>() {

        private final DecoratorService.SpeciesDecoratorWithSurveyCode speciesDecorator =
                new DecoratorService.SpeciesDecoratorWithSurveyCode();

        @Override
        public int compare(CalcifiedPiecesSamplingEditorRowModel o1, CalcifiedPiecesSamplingEditorRowModel o2) {

            boolean o1null = o1 == null || o1.getProtocolSpecies() == null;
            boolean o2null = o2 == null || o2.getProtocolSpecies() == null;

            if (o1null) {
                return o2null ? 0 : -1;
            }
            if (o2null) {
                return 1;
            }

            Species species1 = o1.getProtocolSpecies().getSpecies();
            Species species2 = o2.getProtocolSpecies().getSpecies();
            if (!(species1.equals(species2))) {
                return speciesDecorator.toString(species1).compareTo(speciesDecorator.toString(species2));
            }

            boolean withMaturity = o1.getMaturity() != null;

            if (withMaturity && !o1.getMaturity().equals(o2.getMaturity())) {
                return o1.getMaturity() ? 1 : -1;
            }

            return o1.getMinSize() - o2.getMinSize();
        }
    };

    /**
     * Delegate edit object.
     *
     * @since 4.5
     */
    protected final CalcifiedPiecesSamplingDefinition editObject =
            CalcifiedPiecesSamplingDefinitions.newCalcifiedPiecesSamplingDefinition();

    protected EditProtocolSpeciesRowModel protocolSpecies;

    protected static final Binder<CalcifiedPiecesSamplingDefinition, CalcifiedPiecesSamplingEditorRowModel> fromBeanBinder =
            BinderFactory.newBinder(CalcifiedPiecesSamplingDefinition.class,
                                    CalcifiedPiecesSamplingEditorRowModel.class);

    protected static final Binder<CalcifiedPiecesSamplingEditorRowModel, CalcifiedPiecesSamplingDefinition> toBeanBinder =
            BinderFactory.newBinder(CalcifiedPiecesSamplingEditorRowModel.class,
                                    CalcifiedPiecesSamplingDefinition.class);

    public CalcifiedPiecesSamplingEditorRowModel() {
        super(fromBeanBinder, toBeanBinder);
    }

    @Override
    public Integer getMaxSize() {
        return editObject.getMaxSize();
    }

    @Override
    public void setMaxSize(Integer maxSize) {
        Object oldValue = getMaxSize();
        editObject.setMaxSize(maxSize);
        firePropertyChange(PROPERTY_MAX_SIZE, oldValue, maxSize);
    }

    @Override
    public int getMinSize() {
        return editObject.getMinSize();
    }

    @Override
    public void setMinSize(int minSize) {
        Object oldValue = getMinSize();
        editObject.setMinSize(minSize);
        firePropertyChange(PROPERTY_MIN_SIZE, oldValue, minSize);
    }

    public void setMinSize(Integer minSize) {
        if (minSize != null) {
            setMinSize(minSize.intValue());
        } else {
            firePropertyChange(PROPERTY_MIN_SIZE, getMinSize(), null);
        }
    }

    public EditProtocolSpeciesRowModel getProtocolSpecies() {
        return protocolSpecies;
    }

    public void setProtocolSpecies(EditProtocolSpeciesRowModel protocolSpecies) {
        Object oldValue = getProtocolSpecies();
        this.protocolSpecies = protocolSpecies;
        firePropertyChange(PROPERTY_PROTOCOL_SPECIES, oldValue, protocolSpecies);
    }

    @Override
    public Boolean getMaturity() {
        return editObject.getMaturity();
    }

    @Override
    public void setMaturity(Boolean maturity) {
        Object oldValue = getMaturity();
        editObject.setMaturity(maturity);
        firePropertyChange(PROPERTY_MATURITY, oldValue, maturity);
    }

    @Override
    public Integer getOperationLimitation() {
        return editObject.getOperationLimitation();
    }

    @Override
    public void setOperationLimitation(Integer operationLimitation) {
        Object oldValue = getOperationLimitation();
        editObject.setOperationLimitation(operationLimitation);
        firePropertyChange(PROPERTY_OPERATION_LIMITATION, oldValue, operationLimitation);
    }

    @Override
    public Integer getMaxByLenghtStep() {
        return editObject.getMaxByLenghtStep();
    }

    @Override
    public void setMaxByLenghtStep(Integer maxByLenghtStep) {
        Object oldValue = getMaxByLenghtStep();
        editObject.setMaxByLenghtStep(maxByLenghtStep);
        firePropertyChange(PROPERTY_MAX_BY_LENGHT_STEP, oldValue, maxByLenghtStep);
    }

    @Override
    public int getSamplingInterval() {
        return editObject.getSamplingInterval();
    }

    @Override
    public void setSamplingInterval(int samplingInterval) {
        Object oldValue = getSamplingInterval();
        editObject.setSamplingInterval(samplingInterval);
        firePropertyChange(PROPERTY_SAMPLING_INTERVAL, oldValue, samplingInterval);
    }

    public void setSamplingInterval(Integer samplingInterval) {
        if (samplingInterval != null) {
            setMinSize(samplingInterval.intValue());
        } else {
            firePropertyChange(PROPERTY_SAMPLING_INTERVAL, getSamplingInterval(), null);
        }
    }

    @Override
    public boolean isSex() {
        return editObject.isSex();
    }

    @Override
    public void setSex(boolean sex) {
        Object oldValue = isSex();
        editObject.setSex(sex);
        firePropertyChange(PROPERTY_SEX, oldValue, sex);
    }

    @Override
    public Integer getZoneLimitation() {
        return editObject.getZoneLimitation();
    }

    @Override
    public void setZoneLimitation(Integer zoneLimitation) {
        Object oldValue = getZoneLimitation();
        editObject.setZoneLimitation(zoneLimitation);
        firePropertyChange(PROPERTY_ZONE_LIMITATION, oldValue, zoneLimitation);
    }

    @Override
    protected CalcifiedPiecesSamplingDefinition newEntity() {
        return CalcifiedPiecesSamplingDefinitions.newCalcifiedPiecesSamplingDefinition();
    }
}

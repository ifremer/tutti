/*
 * #%L
 * Tutti :: UI
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

ComputableDataEditor {
  autoPopup: {handler.getConfig().isAutoPopupNumberEditor()};
  showPopupButton: {handler.getConfig().isShowNumberEditorButton()};
  showReset: true;
  computedDataColor: {handler.getConfig().getColorComputedWeights()};
}

#editMarineLitterBatchTopPanel {
  _help: {"tutti.editMarineLitterBatch.help"};
}

#marineLitterBatchTabToolBar {
  floatable: false;
  opaque: true;
  borderPainted: false;
}

#menu {
  border:{null};
  background:{javax.swing.UIManager.getColor("MenuBar.background")};
}

#menuAction {
  opaque: true;
  text: "tutti.toolbar.menu.action";
  toolTipText: "tutti.toolbar.menu.action.tip";
  i18nMnemonic: "tutti.toolbar.menu.action.mnemonic";
}

#marineLitterBatchAttachmentsButton {
  toolTipText: "tutti.editMarineLitterBatch.action.attachments.tip";
  focusPainted: false;
}

#marineLitterTotalWeightLabel {
  text: "tutti.editMarineLitterBatch.field.marineLitterTotalWeight";
  toolTipText: "tutti.editMarineLitterBatch.field.marineLitterTotalWeight.tip";
  labelFor: {marineLitterTotalWeightField};
  _help: {"tutti.editMarineLitterBatch.field.marineLitterTotalWeight.help"};
}

#marineLitterTotalWeightField {
  bean: {model.getMarineLitterTotalComputedOrNotWeight()};
  numberType: {Float.class};
  numberValue: {model.getMarineLitterTotalWeight()};
  _help: {"tutti.editMarineLitterBatch.field.marineLitterTotalWeight.help"};
}

#tablePopup {
  label: "tutti.editMarineLitterBatch.title.batchActions";
}

#table {
  selectionMode: {ListSelectionModel.SINGLE_SELECTION};
  selectionForeground: {Color.BLACK};
  sortable: false;
}

#importMultiPostButton {
  actionIcon: import;
  text: "tutti.editMarineLitterBatch.action.importMultiPost";
  toolTipText: "tutti.editMarineLitterBatch.action.importMultiPost.tip";
  i18nMnemonic: "tutti.editMarineLitterBatch.action.importMultiPost.mnemonic";
  _applicationAction: {fr.ifremer.tutti.ui.swing.content.operation.catches.marinelitter.actions.ImportMultiPostMarineLitterAction.class};
  _help: {"tutti.editMarineLitterBatch.action.importMultiPost.help"};
}

#exportMultiPostButton {
  actionIcon: export;
  text: "tutti.editMarineLitterBatch.action.exportMultiPost";
  toolTipText: "tutti.editMarineLitterBatch.action.exportMultiPost.tip";
  i18nMnemonic: "tutti.editMarineLitterBatch.action.exportMultiPost.mnemonic";
  _applicationAction: {fr.ifremer.tutti.ui.swing.content.operation.catches.marinelitter.actions.ExportMultiPostMarineLitterAction.class};
  _help: {"tutti.editMarineLitterBatch.action.exportMultiPost.help"};
}

#createMarineLitterBatchButton {
  actionIcon: batch-create;
  text: "tutti.editMarineLitterBatch.action.createBatch";
  toolTipText: "tutti.editMarineLitterBatch.action.createBatch.tip";
  i18nMnemonic: "tutti.editMarineLitterBatch.action.createBatch.mnemonic";
  _simpleAction: {fr.ifremer.tutti.ui.swing.content.operation.catches.marinelitter.actions.OpenCreateMarineLitterBatchUIAction.class};
  _help: {"tutti.editMarineLitterBatch.action.createBatch.help"};
}

#removeMarineLitterBatchMenu {
  actionIcon: batch-delete;
  text: "tutti.editMarineLitterBatch.action.removeBatch";
  toolTipText: "tutti.editMarineLitterBatch.action.removeBatch.tip";
  i18nMnemonic: "tutti.editMarineLitterBatch.action.removeBatch.mnemonic";
  _applicationAction: {fr.ifremer.tutti.ui.swing.content.operation.catches.marinelitter.actions.RemoveMarineLitterBatchAction.class};
  enabled: {model.isRemoveBatchEnabled()};
  _help: {"tutti.editMarineLitterBatch.action.removeBatch.help"};
}

package fr.ifremer.tutti.ui.swing.update.actions;

/*
 * #%L
 * Tutti :: UI
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.TuttiConfiguration;
import fr.ifremer.tutti.persistence.ProgressionModel;
import fr.ifremer.tutti.ui.swing.RunTutti;
import fr.ifremer.tutti.ui.swing.TuttiUIContext;
import fr.ifremer.tutti.ui.swing.content.actions.AbstractMainUITuttiAction;
import fr.ifremer.tutti.ui.swing.content.actions.CloseApplicationAction;
import fr.ifremer.tutti.ui.swing.content.MainUIHandler;
import fr.ifremer.tutti.ui.swing.update.TuttiApplicationUpdaterCallBack;
import fr.ifremer.tutti.ui.swing.update.Updates;
import fr.ifremer.tutti.ui.swing.updater.UpdateModule;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.updater.ApplicationUpdater;

import java.io.File;

import static org.nuiton.i18n.I18n.t;

/**
 * To update jre / i18n or tutti using the {@link ApplicationUpdater} mecanism.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.0
 */
public class UpdateApplicationAction extends AbstractMainUITuttiAction {

    /** Logger. */
    private static final Log log = LogFactory.getLog(UpdateApplicationAction.class);

    public UpdateApplicationAction(MainUIHandler handler) {
        super(handler, true);
        setActionDescription(t("tutti.main.action.updateApplication.tip"));
        modulesToUpdate = Updates.getApplicationModules();
    }

    protected UpdateModule[] modulesToUpdate;

    protected boolean reload;

    @Override
    public boolean prepareAction() throws Exception {
        boolean doAction = super.prepareAction();

        if (doAction) {
            // check application url is reachable
            TuttiUIContext context = getContext();
            doAction = context.checkUpdateApplicationReachable(true);
        }
        return doAction;
    }

    @Override
    public void releaseAction() {
        super.releaseAction();
        modulesToUpdate = Updates.getApplicationModules();
    }

    @Override
    public void doAction() throws Exception {

        reload = false;

        TuttiUIContext context = getContext();
        TuttiConfiguration config = getConfig();

        File current = config.getBasedir();
        if (current == null || !current.exists()) {

            // can not update application
            if (log.isWarnEnabled()) {
                log.warn("No application base directory defined, skip updates.");
            }
        } else {

            String url = config.getUpdateApplicationUrl();

            if (log.isInfoEnabled()) {
                log.info(String.format("Try to update jre, i18N, help or tutti (current application location: %s), using update url: %s", current, url));
            }

            ProgressionModel progressionModel = new ProgressionModel();
            context.getActionUI().getModel().setProgressionModel(progressionModel);
            progressionModel.setMessage(t("tutti.updateApplication.checkUpdates"));

            TuttiApplicationUpdaterCallBack callback = new TuttiApplicationUpdaterCallBack(url, this, progressionModel);

            callback.setModulesToUpdate(modulesToUpdate);

            Updates.doUpdate(config, callback, current);

            if (callback.isApplicationUpdated()) {

                reload = true;

            } else {

                sendMessage(t("tutti.updateApplication.noUpdate"));
            }
        }
    }

    public void setModulesToUpdate(UpdateModule... modulesToUpdate) {
        this.modulesToUpdate = modulesToUpdate;
    }

    @Override
    public void postSuccessAction() {
        super.postSuccessAction();

        if (reload) {
            // wait 1 second to be sure action ui is up
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                if (log.isWarnEnabled()) {
                    log.warn("Could not wait 1 second...", e);
                }
            }

            // tell user restart will be done

            getHandler().showSuccessMessage(t("tutti.updateApplication.title.success"),
                                            t("tutti.updateApplication.message.success"));

            CloseApplicationAction action = getContext().getActionFactory().createLogicAction(
                    getHandler(), CloseApplicationAction.class);
            action.setExitCode(RunTutti.RESTART_EXIT_CODE);
            getActionEngine().runAction(action);
        }
    }

    public boolean isReload() {
        return reload;
    }
}

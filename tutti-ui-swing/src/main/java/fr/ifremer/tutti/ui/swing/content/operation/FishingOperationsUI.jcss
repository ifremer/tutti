/*
 * #%L
 * Tutti :: UI
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

BeanFilterableComboBox {
  showReset: true;
  bean: {model};
}

#fishingOperationsTopPanel {
  _help: {"tutti.fishingOperations.help"};
}

#newFishingOperationButton {
  actionIcon: add;
  toolTipText: "tutti.fishingOperations.action.newFishingOperation.tip";
  i18nMnemonic: "tutti.fishingOperations.action.newFishingOperation.mnemonic";
  preferredSize: {new Dimension(75,12)};
  _applicationAction: {fr.ifremer.tutti.ui.swing.content.operation.fishing.actions.NewFishingOperationAction.class};
  _help: {"tutti.fishingOperations.action.newFishingOperation.help"};
}

#deleteFishingOperationButton {
  actionIcon: delete;
  enabled:{model.getSelectedFishingOperation() != null};
  toolTipText: "tutti.fishingOperations.action.deleteFishingOperation.tip";
  preferredSize: {new Dimension(75,12)};
  _applicationAction: {fr.ifremer.tutti.ui.swing.content.operation.fishing.actions.DeleteFishingOperationAction.class};
  _help: {"tutti.fishingOperations.action.deleteFishingOperation.help"};
}

#fishingOperationComboBox {
  property: selectedFishingOperation;
  selectedItem: {model.getSelectedFishingOperation()};
  _help: {"tutti.fishingOperations.field.fishingOperation.help"};
}

#warningContainer {
  background: {new java.awt.Color(245, 218, 88)};
  visible: {!model.isCatchEnabled()};
}

#warningLabel {
  actionIcon: warning;
  border: {new javax.swing.border.EmptyBorder(5, 10, 5, 10)};
  /*text: {handler.getCatchWarningLabel(model.isCatchEnabled(), model.isCatchNotFound(), model.isSampleCatchModelValid())};*/
  text: {model.getValidationErrorMessage()};
}

#catchesTab {
  enabled: {model.getSelectedFishingOperation() != null && model.isCatchEnabled()};
}

#noTraitPane {
  text: "tutti.fishingOperations.info.no.fishingOperation.selected";
  horizontalAlignment: {JLabel.CENTER};
}

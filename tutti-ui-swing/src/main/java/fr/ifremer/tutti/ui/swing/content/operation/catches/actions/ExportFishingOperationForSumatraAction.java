package fr.ifremer.tutti.ui.swing.content.operation.catches.actions;

/*
 * #%L
 * Tutti :: UI
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.ProgressionModel;
import fr.ifremer.tutti.persistence.entities.data.Cruise;
import fr.ifremer.tutti.persistence.entities.data.FishingOperation;
import fr.ifremer.tutti.service.export.sumatra.CatchesSumatraExportService;
import fr.ifremer.tutti.service.export.sumatra.SumatraExportResult;
import fr.ifremer.tutti.ui.swing.content.operation.catches.EditCatchesUI;
import fr.ifremer.tutti.ui.swing.content.operation.catches.EditCatchesUIHandler;
import fr.ifremer.tutti.ui.swing.content.operation.catches.EditCatchesUIModel;
import fr.ifremer.tutti.ui.swing.util.actions.LongActionSupport;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.util.DateUtil;

import java.io.File;
import java.util.Objects;

import static org.nuiton.i18n.I18n.t;

/**
 * Created on 10/1/13.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.7
 */
public class ExportFishingOperationForSumatraAction extends LongActionSupport<EditCatchesUIModel, EditCatchesUI, EditCatchesUIHandler> {

    /** Logger. */
    private static final Log log = LogFactory.getLog(ExportFishingOperationForSumatraAction.class);

    private static final String EXPORT_FILE_NAME = "sumatra_%s_%s_%s_%s"; // sumatra_nom campagne_n° station_n° trait_date début trait.csv

    protected File file;

    protected SumatraExportResult sumatraExportResult;

    public ExportFishingOperationForSumatraAction(EditCatchesUIHandler handler) {
        super(handler, true);
    }

    @Override
    public boolean prepareAction() throws Exception {

        boolean doAction = super.prepareAction();

        if (getModel().isModify()) {

            displayWarningMessage(
                    t("tutti.exportFishingOperationCsv.title.model.modified"),
                    t("tutti.exportFishingOperationCsv.message.model.modified")
            );
            doAction = false;
        }

        if (doAction) {

            if (!getDataContext().isProtocolFilled()) {
                displayWarningMessage(
                        t("tutti.exportFishingOperationCsv.title.missing.protocol"),
                        t("tutti.exportFishingOperationCsv.message.missing.protocol")
                );
            }
        }

        if (doAction) {

            FishingOperation fishingOperation = getModel().getFishingOperation();

            String date = DateUtil.formatDate(fishingOperation.getGearShootingStartDate(), "dd-MM-yyyy");
            String exportFilename = String.format(EXPORT_FILE_NAME,
                                                  getDataContext().getCruise().getName(),
                                                  fishingOperation.getStationNumber(),
                                                  fishingOperation.getFishingOperationNumber(),
                                                  date);

            // choose file to export
            file = saveFile(
                    exportFilename,
                    "csv",
                    t("tutti.exportFishingOperationCsv.title.choose.exportFile"),
                    t("tutti.exportFishingOperationCsv.action.chooseFile"),
                    "^.+\\.csv$", t("tutti.common.file.csv")
            );
            doAction = file != null;
        }
        return doAction;
    }

    @Override
    public void releaseAction() {
        file = null;
        sumatraExportResult = null;
        super.releaseAction();
    }

    @Override
    public void doAction() throws Exception {
        Cruise cruise = getDataContext().getCruise();
        FishingOperation fishingOperation = getModel().getFishingOperation();
        Objects.requireNonNull(cruise);
        Objects.requireNonNull(fishingOperation);
        Objects.requireNonNull(file);

        if (log.isInfoEnabled()) {
            log.info("Will export fishingOperation " + cruise.getId() + "-" + fishingOperation.getStationNumber() +
                             " to file: " + file);
        }

        ProgressionModel progressionModel = new ProgressionModel();
        progressionModel.setTotal(3); // loading cruise + loading fishing operationIds + export
        setProgressionModel(progressionModel);

        // export catches
        CatchesSumatraExportService service = getContext().getCatchesSumatraExportService();

        sumatraExportResult = service.exportFishingOperationForSumatra(file, cruise.getIdAsInt(), fishingOperation.getIdAsInt(), progressionModel);

    }

    @Override
    public void postSuccessAction() {
        super.postSuccessAction();

        if (sumatraExportResult.withBadSpecies()) {

            StringBuilder badSpeciesList = new StringBuilder();
            for (String s : sumatraExportResult.getBadSpecies()) {
                badSpeciesList.append("<li>").append(s);
            }

            displayWarningMessage(
                    t("tutti.exportFishingOperationCsv.title.badSpecies"),
                    t("tutti.exportFishingOperationCsv.message.badSpecies", badSpeciesList.toString()));
        }

        if (sumatraExportResult.withBadBenthos()) {

            StringBuilder badBenthosList = new StringBuilder();
            for (String s : sumatraExportResult.getBadBenthos()) {
                badBenthosList.append("<li>").append(s);
            }

            displayWarningMessage(
                    t("tutti.exportFishingOperationCsv.title.badBenthos"),
                    t("tutti.exportFishingOperationCsv.message.badBenthos", badBenthosList.toString()));
        }

        sendMessage(t("tutti.exportFishingOperationCsv.action.success", file));
    }
}

package fr.ifremer.tutti.ui.swing.content.actions;

/*
 * #%L
 * Tutti :: UI
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.ichtyometer.IchtyometerClient;
import fr.ifremer.tutti.ichtyometer.LocalDeviceNotFoundException;
import fr.ifremer.tutti.ichtyometer.RemoteDeviceChooser;
import fr.ifremer.tutti.ichtyometer.RemoteDeviceNotFoundException;
import fr.ifremer.tutti.ichtyometer.RemoteDeviceServiceNotFoundException;
import fr.ifremer.tutti.ichtyometer.feed.IchtyometerFeedReader;
import fr.ifremer.tutti.ui.swing.content.MainUIHandler;
import org.nuiton.jaxx.application.ApplicationBusinessException;

import javax.swing.JOptionPane;

import static org.nuiton.i18n.I18n.t;

/**
 * Establish a connection to an ichtyometer.
 *
 * Created on 1/29/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.1
 */
public class ConnectIchtyometerAction extends AbstractMainUITuttiAction {

    public ConnectIchtyometerAction(MainUIHandler handler) {
        super(handler, false);
    }

    @Override
    public void doAction() throws Exception {

        IchtyometerClient client = new IchtyometerClient(getConfig().getIchtyometerMaximumNumberOfAttemptToConnect());

        RemoteDeviceChooser remoteDeviceChooser = remoteDeviceNames -> {

            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                // Don't care
            }
            return (String) JOptionPane.showInputDialog(
                    getContext().getActionUI(),
                    t("tutti.ichtyometer.choose.remote.device.found"),
                    t("tutti.ichtyometer.title.choose.remote.device"),
                    JOptionPane.QUESTION_MESSAGE,
                    null,
                    remoteDeviceNames.toArray(new String[remoteDeviceNames.size()]),
                    null
            );
        };
        try {
            client.open(remoteDeviceChooser, getConfig().isFullBluetoothScan());
        } catch (LocalDeviceNotFoundException e) {
            throw new ApplicationBusinessException(t("tutti.ichtyometer.error.no.local.device"));
        } catch (RemoteDeviceNotFoundException e) {
            throw new ApplicationBusinessException(t("tutti.ichtyometer.error.no.remote.device"));
        } catch (RemoteDeviceServiceNotFoundException e) {
            throw new ApplicationBusinessException(t("tutti.ichtyometer.error.no.remote.device.service"));
        }
        IchtyometerFeedReader ichtyometerReader = new IchtyometerFeedReader();
        ichtyometerReader.start(client);

        getContext().setIchtyometerReader(ichtyometerReader);
    }

    @Override
    public void postSuccessAction() {
        super.postSuccessAction();

        IchtyometerFeedReader ichtyometerReader = getContext().getIchtyometerReader();
        String clientName = ichtyometerReader.getClientName();
        sendMessage(t("tutti.ichtyometer.connection.establish", clientName));

        displayInfoMessage(
                t("tutti.ichtyometer.connection.establish.title"),
                t("tutti.ichtyometer.connection.establish.message", clientName)
        );
    }
}

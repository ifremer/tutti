package fr.ifremer.tutti.ui.swing.content.operation.fishing.actions;

/*
 * #%L
 * Tutti :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.entities.data.FishingOperation;
import fr.ifremer.tutti.persistence.entities.data.FishingOperations;
import fr.ifremer.tutti.service.operationimport.FishingOperationImportService;
import fr.ifremer.tutti.service.operationimport.ImportFromColumnFileFishingOperationNotFoundException;
import fr.ifremer.tutti.service.operationimport.ImportFromColumnFileInvalidRowException;
import fr.ifremer.tutti.service.operationimport.ImportFromColumnFileMissingHeaderException;
import fr.ifremer.tutti.ui.swing.util.actions.LongActionSupport;
import fr.ifremer.tutti.ui.swing.content.operation.EditFishingOperationUI;
import fr.ifremer.tutti.ui.swing.content.operation.EditFishingOperationUIHandler;
import fr.ifremer.tutti.ui.swing.content.operation.EditFishingOperationUIModel;
import fr.ifremer.tutti.ui.swing.content.protocol.actions.LoadProtocolImportColumnsAction;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.csv.AbstractImportErrorInfo;
import org.nuiton.jaxx.application.ApplicationBusinessException;
import org.nuiton.util.beans.Binder;
import org.nuiton.util.beans.BinderFactory;

import java.io.File;
import java.io.IOException;

import static org.nuiton.i18n.I18n.t;

/**
 * @author Kevin Morin (Code Lutin)
 * @since 3.10
 */
public class ImportFishingOperationCaracteristicsFromColumnFileAction extends LongActionSupport<EditFishingOperationUIModel, EditFishingOperationUI, EditFishingOperationUIHandler> {

    private static final Log log = LogFactory.getLog(LoadProtocolImportColumnsAction.class);

    /**
     * File to import.
     */
    protected File importFile;

    /**
     * Result fishing operation after import to merge back to ui model.
     */
    protected FishingOperation fishingOperationToMerge;

    public ImportFishingOperationCaracteristicsFromColumnFileAction(EditFishingOperationUIHandler handler) {
        super(handler, true);
    }

    @Override
    public boolean prepareAction() throws Exception {
        boolean result = super.prepareAction() && getModel().isImportFromColumnFileEnabled();

        if (result) {
            importFile = chooseFile(t("tutti.editFishingOperation.action.title.choose.importColumnsFile"),
                                    t("tutti.editFishingOperation.action.chooseColumnsFile.import"),
                                    "^.*\\.csv", t("tutti.common.file.csv"));

            result = importFile != null;
        }

        return result;
    }

    @Override
    public void doAction() throws ImportFromColumnFileInvalidRowException, ImportFromColumnFileFishingOperationNotFoundException, ImportFromColumnFileMissingHeaderException, IOException {

        EditFishingOperationUIModel model = getModel();

        // Create a copy of the ui model to use in the import
        // this bean will be merged back after import into ui model
        // use the binder, cause EditFishingOperationUIModel.newEntity() returns always the same instance
        fishingOperationToMerge = FishingOperations.newFishingOperation();
        Binder<EditFishingOperationUIModel, FishingOperation> toBeanBinder =
                BinderFactory.newBinder(EditFishingOperationUIModel.class, FishingOperation.class);
        toBeanBinder.copy(model, fishingOperationToMerge);

        FishingOperationImportService importService = getContext().getFishingOperationImportService();

        String errorMessage = null;
        try {
            importService.importCaracteristicsFromColumnFile(importFile, fishingOperationToMerge);

        } catch (ImportFromColumnFileInvalidRowException error) {
            // matching row is not valid

            errorMessage = "";
            for (AbstractImportErrorInfo<FishingOperation> errorInfo : error.getErrors()) {
                Throwable errorInfoCause = errorInfo.getCause();
                if (log.isErrorEnabled()) {
                    log.error(errorInfoCause.getLocalizedMessage());
                }
                errorMessage += "<li>" + errorInfoCause.getLocalizedMessage().replaceAll("\\s+", " ") + "</li>";
            }

        } catch (ImportFromColumnFileFishingOperationNotFoundException error) {
            // no matching fishing operation
            errorMessage = "<li>" + t("tutti.editFishingOperation.action.importColumns.operationNotFound") + "</li>";

        } catch (ImportFromColumnFileMissingHeaderException error) {
            errorMessage = t("tutti.editFishingOperation.action.importColumns.missingHeader", error.getImportColumn());
        }

        if (errorMessage != null) {
            throw new ApplicationBusinessException(t("tutti.editFishingOperation.action.importColumns.error", errorMessage));
        }
    }

    @Override
    public void postSuccessAction() {

        super.postSuccessAction();

        // Merge result to ui model
        EditFishingOperationUI ui = getUI();
        EditFishingOperationUIModel model = getModel();

        model.fromEntity(fishingOperationToMerge);

        //FIXME Find out why ?
        //these 2 do not refresh correctly
        ui.getRecorderPersonList().getHandler().setSelected(fishingOperationToMerge.getRecorderPerson());
        ui.getSecondaryVesselList().getHandler().setSelected(fishingOperationToMerge.getSecondaryVessel());

        ui.getGearUseFeatureTabContent().getHandler().mergeCaracteristics(fishingOperationToMerge);
        ui.getVesselUseFeatureTabContent().getHandler().mergeCaracteristics(fishingOperationToMerge);

        sendMessage(t("tutti.editFishingOperation.action.importColumns.success"));

    }

}

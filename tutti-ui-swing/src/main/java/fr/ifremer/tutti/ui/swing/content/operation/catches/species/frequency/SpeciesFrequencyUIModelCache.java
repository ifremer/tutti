package fr.ifremer.tutti.ui.swing.content.operation.catches.species.frequency;

/*
 * #%L
 * Tutti :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Sets;
import org.apache.commons.lang3.mutable.MutableInt;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

/**
 * Put here all possible caches used by model.
 *
 * Created on 1/1/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.11
 */
public class SpeciesFrequencyUIModelCache implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * Rows with a filled weight.
     *
     * @since 3.0
     */
    protected final Set<SpeciesFrequencyRowModel> withWeightRows = Sets.newHashSet();

    /**
     * Number of rows for each lengthstep (keys are a round value (*10) of the real float step, values are number of such steps.
     *
     * The cache is used to check if there is no a doublon of step in the rows.
     *
     * @since 3.10
     */
    protected final Map<Integer, MutableInt> nbOfRowsByLengthStep = new TreeMap<>();

    private final Map<Float, SpeciesFrequencyRowModel> rowCache = new TreeMap<>();

    public void loadCache(List<SpeciesFrequencyRowModel> rows) {

        withWeightRows.clear();
        nbOfRowsByLengthStep.clear();
        rowCache.clear();

        for (SpeciesFrequencyRowModel row : rows) {

            Float lengthStep = row.getLengthStep();
            if (lengthStep != null) {
                rowCache.put(lengthStep, row);
                incNumberOfRows(lengthStep);
            }

            updateRowWithWeight(row);

        }

    }

    public void updateRowWithWeight(SpeciesFrequencyRowModel row) {

        if (row.getWeight() == null) {
            withWeightRows.remove(row);
        } else {
            withWeightRows.add(row);
        }
    }

    public int getNbRowsWithWeight() {
        return withWeightRows.size();
    }

    public int numberOfRows(float lengthStep) {
        MutableInt mutableInt = getNbRowsByLengthStep(lengthStep);
        return mutableInt.intValue();
    }

    public void incNumberOfRows(float lengthStep) {
        MutableInt mutableInt = getNbRowsByLengthStep(lengthStep);
        mutableInt.increment();
    }

    public void decNumberOfRows(float lengthStep) {
        MutableInt mutableInt = getNbRowsByLengthStep(lengthStep);
        mutableInt.decrement();
    }

    protected MutableInt getNbRowsByLengthStep(float lengthStep) {

        //convert the lengthStep into millimeter to avoid float inprecision in map equality
        int mmLengthStep = Math.round(lengthStep * 10);
        MutableInt mutableInt = nbOfRowsByLengthStep.get(mmLengthStep);
        if (mutableInt == null) {
            mutableInt = new MutableInt(0);
            nbOfRowsByLengthStep.put(mmLengthStep, mutableInt);
        }
        return mutableInt;

    }

    public void addLengthStep(SpeciesFrequencyRowModel row) {

        Float lengthStep = row.getLengthStep();
        rowCache.put(lengthStep, row);
        incNumberOfRows(lengthStep);

    }

    public void removeLengthStep(Float oldValue) {

        rowCache.remove(oldValue);
        decNumberOfRows(oldValue);

    }

    public Map<Float, SpeciesFrequencyRowModel> getRowCache() {
        return rowCache;
    }


    public Float computeTotalWeight() {
        float result = 0f;
        for (SpeciesFrequencyRowModel row : withWeightRows) {
            if (!row.isValid()) {
                continue;
            }
            result += row.getWeight();
        }
        return result;
    }
}

package fr.ifremer.tutti.ui.swing.util.attachment;

/*
 * #%L
 * Tutti :: UI
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.adagio.core.dao.referential.ObjectTypeCode;
import fr.ifremer.tutti.persistence.entities.data.Attachment;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;

/**
 * To place on model wich supports attachments.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.0.2
 */
public interface AttachmentModelAware extends Serializable {

    String PROPERTY_ATTACHMENT = "attachment";

    String PROPERTY_OBJECT_ID = "objectId";

    ObjectTypeCode getObjectType();

    Integer getObjectId();

    List<Attachment> getAttachment();

    void addAllAttachment(Collection<Attachment> attachments);

    void addAttachment(Attachment attachment);

    void removeAllAttachment(Collection<Attachment> attachments);

    void removeAttachment(Attachment attachment);

//  TODO use it only when jaxx can parse java 8
//    default boolean isPersisted() {
//        return getObjectId() != null;
//    }

    boolean isCreate();
}

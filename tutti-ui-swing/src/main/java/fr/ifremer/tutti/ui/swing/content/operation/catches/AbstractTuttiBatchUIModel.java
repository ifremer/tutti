package fr.ifremer.tutti.ui.swing.content.operation.catches;

/*
 * #%L
 * Tutti :: UI
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ImmutableMap;
import fr.ifremer.tutti.persistence.entities.data.FishingOperation;
import fr.ifremer.tutti.ui.swing.content.operation.catches.species.SpeciesOrBenthosBatchUISupport;
import fr.ifremer.tutti.ui.swing.util.AbstractTuttiBeanUIModel;
import fr.ifremer.tutti.ui.swing.util.table.AbstractTuttiTableUIModel;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Map;

/**
 * Abstract model for ui in batch tabs.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 0.3
 */
public abstract class AbstractTuttiBatchUIModel<R extends AbstractTuttiBeanUIModel, B extends AbstractTuttiBatchUIModel<R, B>> extends AbstractTuttiTableUIModel<FishingOperation, R, B> {

    /** Logger. */
    private static final Log log = LogFactory.getLog(AbstractTuttiBatchUIModel.class);

    private static final long serialVersionUID = 1L;

//    /**
//     * Fishing Operation to prapagate to operations model.
//     *
//     * @since 0.3
//     */
//    protected final Set<String> propagateProperties;

    /**
     * Editing fishing operations model.
     *
     * @since 0.3
     */
    protected final EditCatchesUIModel catchesUIModel;

    protected AbstractTuttiBatchUIModel(EditCatchesUIModel catchesUIModel, String... properties) {
        super(FishingOperation.class, null, null);
        this.catchesUIModel = catchesUIModel;
        ImmutableMap.Builder<String, String> builder = ImmutableMap.<String, String>builder();
        for (String property : properties) {
            builder.put(property, property);
        }
        catchesUIModel.addPropertyChangeListener(new ForwardPropertyChangeListener(builder.build()));

    }

    public AbstractTuttiBatchUIModel(SpeciesOrBenthosBatchUISupport batchUISupport, String... properties) {
        super(FishingOperation.class, null, null);
        this.catchesUIModel = batchUISupport.getCatchesUIModel();

        ImmutableMap.Builder<String, String> builder = ImmutableMap.<String, String>builder();
        for (String property : properties) {
            builder.put(property, batchUISupport.getCatchesUIModelPropertiesMapping().get(property));
        }
        catchesUIModel.addPropertyChangeListener(new ForwardPropertyChangeListener(builder.build()));
    }

    public final FishingOperation getFishingOperation() {
        return catchesUIModel == null ? null : catchesUIModel.getFishingOperation();
    }

    @Override
    protected FishingOperation newEntity() {
        return null;
    }

    //FIXME Use the one from JavaBeanObject ... later
    private class ForwardPropertyChangeListener implements PropertyChangeListener {

        protected final Map<String, String> propagatePropertiesMapping;

        private ForwardPropertyChangeListener(Map<String, String> propagatePropertiesMapping) {
            this.propagatePropertiesMapping = propagatePropertiesMapping;
        }

        @Override
        public void propertyChange(PropertyChangeEvent evt) {
            String propertyName = evt.getPropertyName();

            if (propagatePropertiesMapping.containsKey(propertyName)) {
                if (log.isDebugEnabled()) {
                    log.debug("Propagates property [" + propertyName + "] change from catch model to " + AbstractTuttiBatchUIModel.this.getClass().getSimpleName());
                }
                String translatedPropertyName = propagatePropertiesMapping.get(propertyName);
                firePropertyChange(translatedPropertyName, evt.getOldValue(), evt.getNewValue());
                EditCatchesUIModel source = (EditCatchesUIModel) evt.getSource();
                if (!source.isLoadingData() && !isModify()) {
                    setModify(true);
                }
            }

            if (AbstractTuttiBeanUIModel.PROPERTY_MODIFY.equals(propertyName)) {

                Boolean newValue = (Boolean) evt.getNewValue();
                if (newValue != null && !newValue) {
                    setModify(false);
                }

            }
        }
    }
}

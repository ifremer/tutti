package fr.ifremer.tutti.ui.swing.content.operation.catches.species.frequency.actions;

/*
 * #%L
 * Tutti :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.entities.data.FishingOperation;
import fr.ifremer.tutti.ui.swing.content.operation.FishingOperationsUI;
import fr.ifremer.tutti.ui.swing.content.operation.FishingOperationsUIModel;
import fr.ifremer.tutti.ui.swing.content.operation.catches.EditCatchesUI;
import fr.ifremer.tutti.ui.swing.content.operation.catches.species.EditSpeciesBatchPanelUI;
import fr.ifremer.tutti.ui.swing.content.operation.catches.species.SpeciesOrBenthosBatchUISupport;
import fr.ifremer.tutti.ui.swing.content.operation.catches.species.edit.SpeciesBatchUI;
import fr.ifremer.tutti.ui.swing.content.operation.catches.species.frequency.SpeciesFrequencyUI;
import fr.ifremer.tutti.ui.swing.content.operation.catches.species.frequency.SpeciesFrequencyUIHandler;
import fr.ifremer.tutti.ui.swing.content.operation.catches.species.frequency.SpeciesFrequencyUIModel;
import fr.ifremer.tutti.ui.swing.content.operation.fishing.actions.EditFishingOperationAction;
import java.awt.event.ActionEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.List;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import javax.swing.JOptionPane;
import jaxx.runtime.SwingUtil;
import org.jdesktop.swingx.JXTable;

import static org.nuiton.i18n.I18n.t;

/**
 * Created on 28/07/2016
 *
 * @author Benjamin POUSSIN - poussin@codelutin.com
 * @since 4.6
 */
public class SearchSpeciesInPrevOrNextCatchesAction extends SaveSupportAction {

    /** Logger. */
    private static final Log log = LogFactory.getLog(SearchSpeciesInPrevOrNextCatchesAction.class);

    protected boolean prev;
    protected Map operationIdAndBatchId;
    protected SpeciesFrequencyUIHandler handler;


    public SearchSpeciesInPrevOrNextCatchesAction(SpeciesFrequencyUI ui, boolean prev) {
        super(ui);
        this.prev = prev;
        handler = ui.getHandler();

        if (prev) {
            putValue(NAME, t("tutti.editSpeciesFrequencies.action.searchSpeciesInPrevCatches"));
            putValue(SHORT_DESCRIPTION, t("tutti.editSpeciesFrequencies.action.searchSpeciesInPrevCatches.tip"));
            putValue(MNEMONIC_KEY, (int) SwingUtil.getFirstCharAt(t("tutti.editSpeciesFrequencies.action.searchSpeciesInPrevCatches.mnemonic"), 'Z'));
//        putValue(SMALL_ICON, SwingUtil.createActionIcon("checkSave"));
        } else {
            putValue(NAME, t("tutti.editSpeciesFrequencies.action.searchSpeciesInNextCatches"));
            putValue(SHORT_DESCRIPTION, t("tutti.editSpeciesFrequencies.action.searchSpeciesInNextCatches.tip"));
            putValue(MNEMONIC_KEY, (int) SwingUtil.getFirstCharAt(t("tutti.editSpeciesFrequencies.action.searchSpeciesInNextCatches.mnemonic"), 'Z'));
//        putValue(SMALL_ICON, SwingUtil.createActionIcon("checkSave"));
        }

    }

    @Override
    public void actionPerformed(ActionEvent e) {

        operationIdAndBatchId = search();
        if (operationIdAndBatchId != null) {
            if (checkSave()) {
                move();
            }
        } else {
            JOptionPane.showOptionDialog(handler.getTopestUI(),
                    t("tutti.informCantFindBatchWithSpecies.message"),
                    t("tutti.informCantFindBatchWithSpecies.title"),
                    JOptionPane.OK_OPTION,
                    JOptionPane.INFORMATION_MESSAGE,
                    null,
                    new String[]{t("tutti.option.ok")},
                    t("tutti.option.ok"));
        }
    }

    protected Map search() {
        Map result;

        SpeciesFrequencyUIModel model = handler.getModel();
        int fishingOperationId = model.getFishingOperation().getIdAsInt();
        int taxonId = model.getBatch().getSpecies().getReferenceTaxonId();

        log.debug(String.format("looking for fishingOperationId: %s, taxonId: %s", fishingOperationId, taxonId));
        if (prev) {
            result =
                    handler.getPersistenceService().getPrevOperationNameAndBatchId(
                            fishingOperationId, taxonId);
        } else {
            result =
                    handler.getPersistenceService().getNextOperationNameAndBatchId(
                            fishingOperationId, taxonId);
        }

        log.debug(String.format("search result: %s", result));
        return result;
    }

    protected boolean checkSave() {
        boolean doAction = true;
        if (handler.getModel().isModify()) {

            // Ask confirmation to quit screen
            doAction = handler.askCancelEditBeforeLeaving();
            if (doAction) {
                // to prevent ask again user, when FishingOperation combobox change
                // if combo had used vetoable and not propertyChange to check
                // modification and cancel action, this method would not be necessary
                handler.getModel().setModify(false);
            }
        }
        return doAction;
    }

    public void move() {

        EditSpeciesBatchPanelUI oldBatchUI = handler.getUI().getParentContainer(EditSpeciesBatchPanelUI.class);
        SpeciesOrBenthosBatchUISupport sbbs = oldBatchUI.getModel().getSpeciesOrBenthosBatchUISupport();

        FishingOperationsUI foUI = handler.getUI().getParentContainer(FishingOperationsUI.class);
        FishingOperationsUIModel foModel = foUI.getModel();
        List<FishingOperation> list = foModel.getFishingOperation();
        Optional<FishingOperation> op = list.stream().filter((FishingOperation o) -> {
            return Objects.equals(o.getIdAsInt(), operationIdAndBatchId.get("operationId"));
        }).findFirst();

        // reposition screen on batch table, must be done after load
        EditFishingOperationAction changeAction = foUI.getHandler().getEditFishingOperationAction();
        changeAction.addPropertyChangeListener(changeAction.PROPERTY_DONE, new PropertyChangeListener() {
            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                changeAction.removePropertyChangeListener(changeAction.PROPERTY_DONE, this);
                
                // go to batch table
                foUI.getTabPane().setSelectedIndex(1);
                EditCatchesUI catchUI = foUI.getCatchesTabContent();
                EditSpeciesBatchPanelUI batchUI = catchUI.getEditSpeciesBatchPanelUI(sbbs);
                // select table row
                SpeciesBatchUI speciesUI = batchUI.getEditBatchesUI();
                JXTable table = speciesUI.getTable();
                String batchId = String.valueOf(operationIdAndBatchId.get("batchId"));
                for (int i = 0, max = table.getRowCount(); i < max; i++) {
                    Object value = table.getValueAt(i, 1);
                    if (Objects.equals(value, batchId)) {
                        table.setRowSelectionInterval(i, i);
                        break;
                    }
                }
            }
        });

        log.debug(String.format("switch to operation: %s", op));
        foModel.setSelectedFishingOperation(op.get());

    }
}
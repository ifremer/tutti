package fr.ifremer.tutti.ui.swing.content.operation.catches.actions;

/*
 * #%L
 * Tutti :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.entities.data.CatchBatch;
import fr.ifremer.tutti.persistence.entities.data.FishingOperation;
import fr.ifremer.tutti.service.catches.multipost.MultiPostImportService;
import fr.ifremer.tutti.type.WeightUnit;
import fr.ifremer.tutti.ui.swing.content.operation.catches.EditCatchesUI;
import fr.ifremer.tutti.ui.swing.content.operation.catches.EditCatchesUIHandler;
import fr.ifremer.tutti.ui.swing.content.operation.catches.EditCatchesUIModel;

import java.io.File;
import java.util.Map;

import static org.nuiton.i18n.I18n.n;
import static org.nuiton.i18n.I18n.t;

/**
 * Created on 11/15/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.9
 */
public class ImportMultiPostCatchAction extends ImportMultiPostActionSupport<EditCatchesUIModel, EditCatchesUI, EditCatchesUIHandler> {

    public ImportMultiPostCatchAction(EditCatchesUIHandler handler) {
        super(handler);
    }

    @Override
    protected String getFileExtension() {
        return "tuttiCatch";
    }

    @Override
    protected String getFileExtensionDescription() {
        return t("tutti.common.file.tuttiCatch");
    }

    @Override
    protected String getFileChooserTitle() {
        return t("tutti.editCatchBatch.action.importMultiPost.sourceFile.title");
    }

    @Override
    protected String getFileChooserButton() {
        return t("tutti.editCatchBatch.action.importMultiPost.sourceFile.button");
    }

    @Override
    protected String getSuccessMessage(File file) {
        return t("tutti.editCatchBatch.action.importMultiPost.success", file);
    }

    @Override
    protected Map<String, Object> importBatches(MultiPostImportService multiPostImportExportService, File file, FishingOperation operation) {

        return multiPostImportExportService.importCatchBatch(file, operation);

    }

    @Override
    protected String buildNotImportedDataReportText(Map<String, Object> notImportedData) {

        WeightUnit catchWeightUnit = WeightUnit.KG;
        WeightUnit speciesWeightUnit = getConfig().getSpeciesWeightUnit();
        WeightUnit benthosWeightUnit = getConfig().getBenthosWeightUnit();
        WeightUnit marineLitterWeightUnit = getConfig().getMarineLitterWeightUnit();

        StringBuilder builder = new StringBuilder();

        Float catchTotalWeight = (Float) notImportedData.get(CatchBatch.PROPERTY_CATCH_TOTAL_WEIGHT);
        addNotImportedWeightToReport(builder, catchTotalWeight, catchWeightUnit, n("tutti.multiPostImportLog.catchTotalWeight"));

        Float catchTotalRejectedWeight = (Float) notImportedData.get(CatchBatch.PROPERTY_CATCH_TOTAL_REJECTED_WEIGHT);
        addNotImportedWeightToReport(builder, catchTotalRejectedWeight, catchWeightUnit, n("tutti.multiPostImportLog.catchTotalRejectedWeight"));

        Float speciesTotalSortedWeight = (Float) notImportedData.get(CatchBatch.PROPERTY_SPECIES_TOTAL_SORTED_WEIGHT);
        addNotImportedWeightToReport(builder, speciesTotalSortedWeight, speciesWeightUnit, n("tutti.multiPostImportLog.speciesTotalSortedWeight"));

        Float benthosTotalSortedWeight = (Float) notImportedData.get(CatchBatch.PROPERTY_BENTHOS_TOTAL_SORTED_WEIGHT);
        addNotImportedWeightToReport(builder, benthosTotalSortedWeight, benthosWeightUnit, n("tutti.multiPostImportLog.benthosTotalSortedWeight"));

        Float marineLitterTotalWeight = (Float) notImportedData.get(CatchBatch.PROPERTY_MARINE_LITTER_TOTAL_WEIGHT);
        addNotImportedWeightToReport(builder, marineLitterTotalWeight, marineLitterWeightUnit, n("tutti.multiPostImportLog.marineLitterTotalWeight"));

        return builder.toString();

    }

    @Override
    protected EditCatchesUI getEditCatchUI() {
        return getUI();
    }
}
package fr.ifremer.tutti.ui.swing.content.program;

/*
 * #%L
 * Tutti :: UI
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.entities.data.Program;
import fr.ifremer.tutti.persistence.entities.data.Programs;
import fr.ifremer.tutti.persistence.entities.referential.TuttiLocation;
import fr.ifremer.tutti.ui.swing.util.AbstractTuttiBeanUIModel;
import org.nuiton.util.beans.Binder;
import org.nuiton.util.beans.BinderFactory;

/**
 * Bean to edit a program.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 0.1
 */
public class EditProgramUIModel extends AbstractTuttiBeanUIModel<Program, EditProgramUIModel> implements Program {

    private static final long serialVersionUID = 1L;

    /**
     * Delegate edit object.
     *
     * @since 1.3
     */
    protected final Program editObject = Programs.newProgram();

    protected static Binder<EditProgramUIModel, Program> toBeanBinder =
            BinderFactory.newBinder(EditProgramUIModel.class,
                                    Program.class);

    protected static Binder<Program, EditProgramUIModel> fromBeanBinder =
            BinderFactory.newBinder(Program.class, EditProgramUIModel.class);


    public EditProgramUIModel() {
        super(fromBeanBinder, toBeanBinder);
    }

    @Override
    protected Program newEntity() {
        return Programs.newProgram();
    }

    //------------------------------------------------------------------------//
    //-- Program methods                                                    --//
    //------------------------------------------------------------------------//

    @Override
    public String getName() {
        return editObject.getName();
    }

    @Override
    public void setName(String name) {
        Object oldValue = getName();
        editObject.setName(name);
        firePropertyChange(PROPERTY_NAME, oldValue, name);
    }

    @Override
    public String getDescription() {
        return editObject.getDescription();
    }

    @Override
    public void setDescription(String description) {
        Object oldValue = getDescription();
        editObject.setDescription(description);
        firePropertyChange(PROPERTY_DESCRIPTION, oldValue, description);
    }

    @Override
    public TuttiLocation getZone() {
        return editObject.getZone();
    }

    @Override
    public void setZone(TuttiLocation zone) {
        Object oldValue = getZone();
        editObject.setZone(zone);
        firePropertyChange(PROPERTY_ZONE, oldValue, zone);
    }
}

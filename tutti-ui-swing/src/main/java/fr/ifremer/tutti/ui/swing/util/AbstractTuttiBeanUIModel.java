package fr.ifremer.tutti.ui.swing.util;

/*
 * #%L
 * Tutti :: UI
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.entities.TuttiEntity;
import org.jdesktop.beans.AbstractSerializableBean;
import org.nuiton.jaxx.application.bean.JavaBeanObject;
import org.nuiton.util.CollectionUtil;
import org.nuiton.util.beans.Binder;

import java.util.Collection;
import java.util.List;

/**
 * Abstract UI model to edit a bean.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 0.1
 */
public abstract class AbstractTuttiBeanUIModel<E, B extends AbstractTuttiBeanUIModel<E, B>> extends AbstractSerializableBean implements TuttiEntity, JavaBeanObject {

    private static final long serialVersionUID = 1L;

    public static final String PROPERTY_MODIFY = "modify";

    public static final String PROPERTY_VALID = "valid";

    protected String id;

    protected boolean modify;

    protected boolean valid;

    private final Binder<E, B> fromBeanBinder;

    private final Binder<B, E> toBeanBinder;

    protected AbstractTuttiBeanUIModel(Binder<E, B> fromBeanBinder,
                                       Binder<B, E> toBeanBinder) {
        this.fromBeanBinder = fromBeanBinder;
        this.toBeanBinder = toBeanBinder;
    }

    public void fromEntity(E entity) {
        fromBean(entity);
    }

    public E toEntity() {
        return toBean();
    }

    public final void fromBean(E bean) {
        fromBeanBinder.copy(bean, (B) this);
    }

    public final E toBean() {
        E result = newEntity();
        toBeanBinder.copy((B) this, result);
        return result;
    }

    protected abstract E newEntity();

    public boolean isModify() {
        return modify;
    }

    public void setModify(boolean modify) {
        Object oldValue = isModify();
        this.modify = modify;
        firePropertyChange(PROPERTY_MODIFY, oldValue, modify);
    }

    public boolean isValid() {
        return valid;
    }

    public void setValid(boolean valid) {
        Object oldValue = isValid();
        this.valid = valid;
        firePropertyChange(PROPERTY_VALID, oldValue, valid);
    }

    public boolean isCreate() {
        return id == null;
    }

    //------------------------------------------------------------------------//
    //-- TuttiEntity methods                                                --//
    //------------------------------------------------------------------------//

    @Override
    public Integer getIdAsInt() {
        return id == null ? null : Integer.valueOf(id);
    }

    @Override
    public void setId(Integer id) {
        if (id == null) {
            this.id = null;
        } else {
            this.id = id.toString();
        }
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public void setId(String id) {
        Object oldValue = getId();
        this.id = id;
        firePropertyChange(PROPERTY_ID, oldValue, id);
    }

    //------------------------------------------------------------------------//
    //-- PropagatePropertyChangeListener methods                            --//
    //------------------------------------------------------------------------//

    @Override
    public void firePropertyChanged(String propertyName,
                                    Object oldValue,
                                    Object newValue) {
        firePropertyChange(propertyName, oldValue, newValue);
    }

    protected <B> B getChild(Collection<B> child, int index) {
        return CollectionUtil.getOrNull(child, index);
    }

    protected <B> B getChild(List<B> child, int index) {
        return CollectionUtil.getOrNull(child, index);
    }

}

package fr.ifremer.tutti.ui.swing.content.cruise.actions;

/*
 * #%L
 * Tutti :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.entities.referential.Caracteristic;
import fr.ifremer.tutti.ui.swing.content.cruise.GearCaracteristicsEditorRowModel;
import fr.ifremer.tutti.ui.swing.content.cruise.GearCaracteristicsEditorTableModel;
import fr.ifremer.tutti.ui.swing.content.cruise.GearCaracteristicsEditorUI;
import fr.ifremer.tutti.ui.swing.content.cruise.GearCaracteristicsEditorUIModel;
import fr.ifremer.tutti.ui.swing.util.actions.SimpleActionSupport;
import jaxx.runtime.swing.editor.bean.BeanFilterableComboBox;

/**
 * Created on 3/7/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.15
 */
public class GearCaracteristicAddRowAction extends SimpleActionSupport<GearCaracteristicsEditorUI> {

    private static final long serialVersionUID = 1L;

    public GearCaracteristicAddRowAction(GearCaracteristicsEditorUI ui) {
        super(ui);
    }

    @Override
    protected void onActionPerformed(GearCaracteristicsEditorUI ui) {

        BeanFilterableComboBox<Caracteristic> keyCombo = ui.getNewRowKey();

        Caracteristic selectedItem = (Caracteristic) keyCombo.getSelectedItem();

        GearCaracteristicsEditorTableModel tableModel = ui.getHandler().getTableModel();
        GearCaracteristicsEditorUIModel model = ui.getModel();

        GearCaracteristicsEditorRowModel row = tableModel.createNewRow();
        row.setKey(selectedItem);
        model.getRows().add(row);

        int rowIndex = tableModel.getRowCount() - 1;
        tableModel.fireTableRowsInserted(rowIndex, rowIndex);

        keyCombo.removeItem(selectedItem);


        row.setValid(false);
        model.addRowInError(row);

        model.setModify(true);

    }

}
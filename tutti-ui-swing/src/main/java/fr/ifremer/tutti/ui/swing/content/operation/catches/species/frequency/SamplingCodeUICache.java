package fr.ifremer.tutti.ui.swing.content.operation.catches.species.frequency;

/*
 * #%L
 * Tutti :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2016 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.entities.referential.Species;
import fr.ifremer.tutti.service.sampling.IndividualObservationSamplingCacheRequest;
import fr.ifremer.tutti.service.sampling.SamplingCodeCache;
import fr.ifremer.tutti.service.sampling.SamplingCodePrefix;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.Closeable;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.TreeSet;

import static fr.ifremer.tutti.persistence.service.TuttiPersistenceServiceLocator.getPersistenceService;

/**
 * Pour gérer le cache des codes de prélèvements.
 *
 * Created on 23/04/16.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 4.5
 */
public class SamplingCodeUICache implements Closeable {

    /** Logger. */
    private static final Log log = LogFactory.getLog(SamplingCodeUICache.class);

    private final SpeciesFrequencyUIModel uiModel;

    private final Integer cruiseId;

    private final SamplingCodeCache samplingCodeCache;

    /**
     * Contient les codes de prélèvements qu'on sait non utilisables.
     *
     * Au chargement de l'écran, on remplit cet ensemble avec les codes des observations individuelles du lot.
     */
    private final Set<Integer> samplingCodesNotAvailable = new TreeSet<>();
    /**
     * Contient les codes de prélèvements qu'on sait utilisables.
     *
     * Dès qu'un code de prélèvement est ajouté ou modifié dans l'écran, on l'ajoute ici.
     */
    private final Set<Integer> samplingCodesAvailable = new TreeSet<>();

    /**
     * La code référent du taxon du lot en cours d'édition.
     */
    private Integer speciesReferenceTaxonId;

    public SamplingCodeUICache(SamplingCodeCache samplingCodeCache, SpeciesFrequencyUIModel uiModel, Integer cruiseId) {
        this.samplingCodeCache = samplingCodeCache;
        this.uiModel = uiModel;
        this.cruiseId = cruiseId;
    }

    public void init(Species species, List<IndividualObservationBatchRowModel> individualObservations, boolean addToCache) {

        Objects.requireNonNull(species);
        Objects.requireNonNull(individualObservations);

        this.speciesReferenceTaxonId = species.getReferenceTaxonId();

        this.samplingCodesAvailable.clear();
        this.samplingCodesNotAvailable.clear();

        if (addToCache) {

            addIndividualObservations(individualObservations);

        } else {

            individualObservations.stream()
                                  .filter(individualObservationRow -> individualObservationRow.getSamplingCode() != null)
                                  .forEach(individualObservationRow -> addSamplingCodeNotAvailable(individualObservationRow.getSamplingCode()));

        }
    }

    @Override
    public void close() {
        this.samplingCodesNotAvailable.clear();
        this.samplingCodesAvailable.clear();
        this.speciesReferenceTaxonId = null;
    }

    public void addIndividualObservations(Collection<IndividualObservationBatchRowModel> individualObservations) {

        Objects.requireNonNull(individualObservations);

        individualObservations.stream()
                              .filter(IndividualObservationBatchRowModel::withSamplingCode)
                              .forEach(row -> {

                                  IndividualObservationSamplingCacheRequest samplingCacheRequest = uiModel.toSamplingCacheRequest(row);
                                  addSampling(samplingCacheRequest);

                              });

    }

    public void removeIndividualObservations(Collection<IndividualObservationBatchRowModel> individualObservations) {

        Objects.requireNonNull(individualObservations);

        individualObservations.stream()
                              .filter(IndividualObservationBatchRowModel::withSamplingCode)
                              .forEach(row -> {

                                  IndividualObservationSamplingCacheRequest samplingCacheRequest = uiModel.toSamplingCacheRequest(row);
                                  removeSampling(samplingCacheRequest);

                              });

    }

    public boolean canUseSamplingCode(Integer sampleCode) {

        if (isSamplingCodeNotAvailable(sampleCode)) {

            // le code n'est pas disponible (on le sait depuis le cache de l'écran)
            if (log.isDebugEnabled()) {
                log.debug("Sampling code " + sampleCode + " is known as not available from cache. Can't use it.");
            }
            return false;
        }

        if (isSamplingCodeAvailable(sampleCode)) {

            // le code est pas disponible (on le sait depuis le cache de l'écran)
            if (log.isDebugEnabled()) {
                log.debug("Sampling code " + sampleCode + " is known as available from cache. Can use it.");
            }
            return true;
        }

        // on demande en base si le code est disponible

        String samplingCodeSuffix = uiModel.getIndividualObservationModel().getSamplingCodePrefix().toSpeciesOnlySamplingCode(sampleCode);
        boolean samplingCodeAvailable = getPersistenceService().isSamplingCodeAvailable(cruiseId,
                                                                                        speciesReferenceTaxonId,
                                                                                        samplingCodeSuffix);

        if (log.isDebugEnabled()) {
            if (samplingCodeAvailable) {
                log.debug("Sampling code " + sampleCode + " is known as available from database. Can use it.");
            } else {
                log.debug("Sampling code " + sampleCode + " is known as not available from database. Can't use it.");
            }
        }

        return samplingCodeAvailable;

    }

    public int getNextSamplingCodeId() {

        int nextSamplingCodeId = samplingCodeCache.getNextSamplingCodeId(speciesReferenceTaxonId);

        if (log.isInfoEnabled()) {
            log.info("Generated sampling code: " + nextSamplingCodeId);
        }
        return nextSamplingCodeId;

    }

    public void addSampling(IndividualObservationSamplingCacheRequest samplingCacheRequest) {

        Objects.requireNonNull(samplingCacheRequest);

        String samplingCode = samplingCacheRequest.getSamplingCode();
        Objects.requireNonNull(samplingCode);

        samplingCodeCache.addSamplingCode(speciesReferenceTaxonId, samplingCode);

        // Le code n'est plus utilisable
        addSamplingCodeNotAvailable(samplingCode);

    }

    public void removeSampling(IndividualObservationSamplingCacheRequest samplingCacheRequest) {

        Objects.requireNonNull(samplingCacheRequest);

        String samplingCode = samplingCacheRequest.getSamplingCode();
        Objects.requireNonNull(samplingCode);

        samplingCodeCache.removeSamplingCode(speciesReferenceTaxonId, samplingCode);

        // Le code est réutilisable
        addSamplingCodeAvailable(samplingCode);

    }

    private boolean isSamplingCodeNotAvailable(int samplingCode) {
        return samplingCodesNotAvailable.contains(samplingCode);
    }

    private boolean isSamplingCodeAvailable(int samplingCode) {
        return samplingCodesAvailable.contains(samplingCode);
    }

    private void addSamplingCodeAvailable(String samplingCode) {

        int samplingCodeNumber = SamplingCodePrefix.extractSamplingCodeIdFromSamplingCode(samplingCode);
        if (log.isDebugEnabled()) {
            log.debug(String.format("Make samplingCode: %s (%d) available", samplingCode, samplingCodeNumber));
        }
        samplingCodesNotAvailable.remove(samplingCodeNumber);
        samplingCodesAvailable.add(samplingCodeNumber);

    }

    private void addSamplingCodeNotAvailable(String samplingCode) {

        int samplingCodeNumber = SamplingCodePrefix.extractSamplingCodeIdFromSamplingCode(samplingCode);
        if (log.isDebugEnabled()) {
            log.debug(String.format("Make samplingCode: %s (%d) not available", samplingCode, samplingCodeNumber));
        }
        samplingCodesNotAvailable.add(samplingCodeNumber);
        samplingCodesAvailable.remove(samplingCodeNumber);

    }

}

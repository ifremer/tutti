/*
 * #%L
 * Tutti :: UI
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

BeanFilterableComboBox {
  showReset: true;
  bean: {model};
}

JTextField {
  _selectOnFocus: {true};
}

#editProgramTopPanel {
  _help: {"tutti.editProgram.help"};
}

#nameLabel {
  text: "tutti.editProgram.field.name";
  labelFor: {nameField};
  toolTipText: "tutti.editProgram.field.name.tip";
  _help: {"tutti.editProgram.field.name.help"};
}

#nameField {
  text: {model.getName()};
  _validatorLabel: {t("tutti.editProgram.field.name")};
  _help: {"tutti.editProgram.field.name.help"};
}

#descriptionPane {
  border: {BorderFactory.createTitledBorder(t("tutti.editProgram.field.description"))};
  toolTipText: "tutti.editProgram.field.description.tip";
  _help: {"tutti.editProgram.field.description.help"};
}

#descriptionField {
  text: {getStringValue(model.getDescription())};
  _validatorLabel: {t("tutti.editProgram.field.description")};
  _help: {"tutti.editProgram.field.description.help"};
}

#zoneLabel {
  text: "tutti.editProgram.field.zone";
  labelFor: {zoneComboBox};
  toolTipText: "tutti.editProgram.field.zone.tip";
  _help: {"tutti.editProgram.field.zone.help"};
}

#zoneComboBox {
  property: zone;
  selectedItem: {model.getZone()};
  _validatorLabel: {t("tutti.editProgram.field.zone")};
  _help: {"tutti.editProgram.field.zone.help"};
}

#saveButton {
  actionIcon: save;
  text: "tutti.editProgram.action.saveProgram";
  toolTipText: "tutti.editProgram.action.saveProgram.tip";
  i18nMnemonic: "tutti.editProgram.action.saveProgram.mnemonic";
  _applicationAction: {fr.ifremer.tutti.ui.swing.content.program.actions.SaveProgramAction.class};
  enabled: {model.isModify() && model.isValid()};
  _help: {"tutti.editProgram.action.saveProgram.help"};
}

#resetButton {
  actionIcon: reset;
  text: "tutti.editProgram.action.resetProgram";
  toolTipText: "tutti.editProgram.action.resetProgram.tip";
  i18nMnemonic: "tutti.editProgram.action.resetProgram.mnemonic";
  _applicationAction: {fr.ifremer.tutti.ui.swing.content.program.actions.ResetProgramAction.class};
  enabled: {model.isModify() && !model.isCreate()};
  _help: {"tutti.editProgram.action.reset.help"};
}

#closeButton {
  actionIcon: cancel;
  text: "tutti.editProgram.action.closeEditProgram";
  toolTipText: "tutti.editProgram.action.closeEditProgram.tip";
  i18nMnemonic: "tutti.editProgram.action.closeEditProgram.mnemonic";
  _applicationAction: {fr.ifremer.tutti.ui.swing.content.actions.OpenHomeScreenAction.class};
  _help: {"tutti.editProgram.action.cancelProgram.help"};
}

package fr.ifremer.tutti.ui.swing.content.operation.catches.actions;

/*
 * #%L
 * Tutti :: UI
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.entities.data.Cruise;
import fr.ifremer.tutti.persistence.entities.data.FishingOperation;
import fr.ifremer.tutti.service.export.pdf.CatchesPdfExportService;
import fr.ifremer.tutti.ui.swing.content.operation.catches.EditCatchesUI;
import fr.ifremer.tutti.ui.swing.content.operation.catches.EditCatchesUIHandler;
import fr.ifremer.tutti.ui.swing.content.operation.catches.EditCatchesUIModel;
import fr.ifremer.tutti.ui.swing.util.actions.LongActionSupport;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.File;
import java.util.Objects;

import static org.nuiton.i18n.I18n.t;

/**
 * Opens a file chooser, exports the cruise catches into the selected file and open the default email editor.
 *
 * @author Kevin Morin - kmorin@codelutin.com
 * @since 1.0
 */
public class ExportFishingOperationReportAction extends LongActionSupport<EditCatchesUIModel, EditCatchesUI, EditCatchesUIHandler> {

    /** Logger. */
    private static final Log log = LogFactory.getLog(ExportFishingOperationReportAction.class);

    protected File file;

    public ExportFishingOperationReportAction(EditCatchesUIHandler handler) {
        super(handler, true);
    }

    @Override
    public boolean prepareAction() throws Exception {

        boolean doAction = super.prepareAction();

        if (getModel().isModify()) {

            displayWarningMessage(
                    t("tutti.exportFishingOperationReport.title.model.modified"),
                    t("tutti.exportFishingOperationReport.message.model.modified")
            );
            doAction = false;
        }

        if (doAction) {

            Cruise cruise = getDataContext().getCruise();
            FishingOperation fishingOperation = getModel().getFishingOperation();
            // choose file to export
            file = saveFile(
                    "exportFishingOperation-" + cruise.getName() + "-" + fishingOperation.getStationNumber(),
                    "pdf",
                    t("tutti.exportFishingOperationReport.title.choose.exportFile"),
                    t("tutti.exportFishingOperationReport.action.chooseFile"),
                    "^.+\\.pdf$", t("tutti.common.file.pdf")
            );
            doAction = file != null;
        }
        return doAction;
    }

    @Override
    public void releaseAction() {
        file = null;
        super.releaseAction();
    }

    @Override
    public void doAction() throws Exception {
        Cruise cruise = getDataContext().getCruise();
        FishingOperation fishingOperation = getModel().getFishingOperation();
        Objects.requireNonNull(cruise);
        Objects.requireNonNull(fishingOperation);
        Objects.requireNonNull(file);

        if (log.isInfoEnabled()) {
            log.info("Will export fishingOperation " + cruise.getId() + "-" + fishingOperation.getStationNumber() + " to file: " + file);
        }

        // export catches
        CatchesPdfExportService service = getContext().getGeneratePDFService();
        service.generateFishingOperationPDFFile(file, fishingOperation.getIdAsInt(), getConfig().getI18nLocale());

    }

    @Override
    public void postSuccessAction() {
        super.postSuccessAction();
        sendMessage(t("tutti.exportFishingOperationReport.action.success", file));
    }
}

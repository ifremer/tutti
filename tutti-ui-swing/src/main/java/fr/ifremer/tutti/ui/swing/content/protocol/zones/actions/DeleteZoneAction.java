package fr.ifremer.tutti.ui.swing.content.protocol.zones.actions;

/*
 * #%L
 * Tutti :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2016 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.ui.swing.content.protocol.EditProtocolUIModel;
import fr.ifremer.tutti.ui.swing.content.protocol.zones.ZoneEditorUI;
import fr.ifremer.tutti.ui.swing.content.protocol.zones.tree.node.StrataNode;
import fr.ifremer.tutti.ui.swing.content.protocol.zones.tree.node.ZoneNode;
import fr.ifremer.tutti.ui.swing.util.actions.SimpleActionSupport;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.swing.JOptionPane;
import javax.swing.JTree;
import javax.swing.tree.TreePath;
import java.util.Enumeration;

import static org.nuiton.i18n.I18n.t;

/**
 * @author Kevin Morin (Code Lutin)
 * @since 4.5
 */
public class DeleteZoneAction extends SimpleActionSupport<ZoneEditorUI> {

    /** Logger. */
    private static final Log log = LogFactory.getLog(DeleteZoneAction.class);

    public DeleteZoneAction(ZoneEditorUI zoneEditorUI) {
        super(zoneEditorUI);
    }

    @Override
    protected void onActionPerformed(ZoneEditorUI zoneEditorUI) {

        JTree zonesTree = zoneEditorUI.getZonesTree();

        if (zonesTree.getSelectionCount() == 1) {

            TreePath selectedPath = zonesTree.getSelectionPath();
            ZoneNode zoneNode = (ZoneNode) selectedPath.getPathComponent(1);

            String zoneLabel = zoneNode.getUserObject();

            int confirmDeletion = JOptionPane.showConfirmDialog(zoneEditorUI,
                                                                t("tutti.zoneEditor.deleteZone.message", zoneLabel),
                                                                t("tutti.zoneEditor.deleteZone.title"),
                                                                JOptionPane.YES_NO_OPTION,
                                                                JOptionPane.QUESTION_MESSAGE);

            if (confirmDeletion == JOptionPane.YES_OPTION) {


                EditProtocolUIModel model = zoneEditorUI.getModel();

                Enumeration children = zoneNode.children();
                while (children.hasMoreElements()) {

                    StrataNode strateNode = (StrataNode) children.nextElement();
                    if (log.isInfoEnabled()) {
                        log.info("Move back strata: " + strateNode + " to available universe.");
                    }
                    model.unselectStrataNode(strateNode);

                }

                model.getZonesTreeModel().removeNodeFromParent(zoneNode);

            }
        }
    }
}

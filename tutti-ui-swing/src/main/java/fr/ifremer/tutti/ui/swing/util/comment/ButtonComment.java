package fr.ifremer.tutti.ui.swing.util.comment;

/*
 * #%L
 * Tutti :: UI
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.entities.CommentAware;
import fr.ifremer.tutti.ui.swing.TuttiUIContext;
import jaxx.runtime.SwingUtil;

import javax.swing.JToggleButton;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.Point;
import java.awt.event.HierarchyBoundsAdapter;
import java.awt.event.HierarchyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import static org.nuiton.i18n.I18n.t;

/**
 * A toggleButton to show (or hide) comment editor.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.1
 */
public class ButtonComment extends JToggleButton {

    private static final long serialVersionUID = 1L;

    protected final CommentEditorUI popup;

    protected Point popupPosition = null;

    protected boolean popupMoving;

    public ButtonComment(TuttiUIContext context,
                         CommentAware model) {

        setIcon(SwingUtil.createActionIcon("edit-comment"));
        setToolTipText(t("tutti.commentEditor.action.tip"));

        popup = new CommentEditorUI(context);

        popup.addWindowListener(new WindowAdapter() {

            @Override
            public void windowOpened(WindowEvent e) {
                setSelected(true);
            }

            @Override
            public void windowClosing(WindowEvent e) {
                setSelected(false);
            }

            @Override
            public void windowClosed(WindowEvent e) {
                setSelected(false);
            }
        });

        addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                if (isSelected()) {
                    popup.openEditor(ButtonComment.this);
                } else {
                    popup.closeEditor();
                }
            }
        });

        addHierarchyBoundsListener(new HierarchyBoundsAdapter() {

            @Override
            public void ancestorMoved(HierarchyEvent e) {
                if (popup.isShowing()) {

                    // place dialog just under the button
                    Point point = new Point(getLocationOnScreen());
                    point.translate(-popup.getWidth() + getWidth(), getHeight());
                    popupMoving = true;
                    try {
                        popup.setLocation(point);
                    } finally {
                        popupMoving = false;
                    }
                }
            }
        });
        setBean(model);
    }

    public void init() {
        popup.getHandler().init();
    }

    public void init(CommentAware model) {
        setBean(model);
        init();
    }

    public void onCloseUI() {
        setSelected(false);
    }

    public CommentAware getBean() {
        return popup.getBean();
    }

    protected void setBean(CommentAware model) {
        popup.setBean(model);
        init();

    }

}

package fr.ifremer.tutti.ui.swing.content.cruise.actions;

/*
 * #%L
 * Tutti :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import fr.ifremer.tutti.persistence.entities.data.Cruise;
import fr.ifremer.tutti.ui.swing.util.actions.LongActionSupport;
import fr.ifremer.tutti.ui.swing.content.cruise.EditCruiseUI;
import fr.ifremer.tutti.ui.swing.content.cruise.EditCruiseUIHandler;
import fr.ifremer.tutti.ui.swing.content.cruise.EditCruiseUIModel;

import static org.nuiton.i18n.I18n.t;

/**
 * Created on 7/10/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.6
 */
public class ResetCruiseAction extends LongActionSupport<EditCruiseUIModel, EditCruiseUI, EditCruiseUIHandler> {

    public ResetCruiseAction(EditCruiseUIHandler handler) {
        super(handler, true);
    }

    @Override
    public void doAction() throws Exception {

        Preconditions.checkState(getDataContext().isCruiseFilled());

        Cruise cruise = getDataContext().getCruise();

        getHandler().reloadCruise(cruise);

        getModel().setModify(false);
    }

    @Override
    public void postSuccessAction() {

        sendMessage(t("tutti.resetCruise.action.cruiseReloaded", decorate(getDataContext().getCruise())));
    }
}
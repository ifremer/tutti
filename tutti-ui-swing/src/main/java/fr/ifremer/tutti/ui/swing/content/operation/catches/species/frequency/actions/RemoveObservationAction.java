package fr.ifremer.tutti.ui.swing.content.operation.catches.species.frequency.actions;

/*
 * #%L
 * Tutti :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2016 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.ui.swing.content.operation.catches.species.frequency.IndividualObservationBatchRowModel;
import fr.ifremer.tutti.ui.swing.content.operation.catches.species.frequency.IndividualObservationBatchTableModel;
import fr.ifremer.tutti.ui.swing.content.operation.catches.species.frequency.SpeciesFrequencyTableModel;
import fr.ifremer.tutti.ui.swing.content.operation.catches.species.frequency.SpeciesFrequencyUI;
import fr.ifremer.tutti.ui.swing.content.operation.catches.species.frequency.SpeciesFrequencyUIHandler;
import fr.ifremer.tutti.ui.swing.content.operation.catches.species.frequency.SpeciesFrequencyUIModel;
import fr.ifremer.tutti.ui.swing.util.actions.SimpleActionSupport;
import org.jdesktop.swingx.JXTable;

import javax.swing.JOptionPane;
import java.util.Collection;
import java.util.HashSet;

import static org.nuiton.i18n.I18n.t;

/**
 * @author Kevin Morin (Code Lutin)
 * @since 4.5
 */
public class RemoveObservationAction extends SimpleActionSupport<SpeciesFrequencyUI> {

    public RemoveObservationAction(SpeciesFrequencyUI speciesFrequencyUI) {
        super(speciesFrequencyUI, false);
    }

    @Override
    protected void onActionPerformed(SpeciesFrequencyUI ui) {
        JXTable obsTable = ui.getObsTable();
        IndividualObservationBatchTableModel obsTableModel =
                (IndividualObservationBatchTableModel) obsTable.getModel();

        int[] selectedRowIndexes = obsTable.getSelectedRows();
        Collection<IndividualObservationBatchRowModel> rowsToDelete = new HashSet<>();

        for (int selectedRowIndex : selectedRowIndexes) {
            rowsToDelete.add(obsTableModel.getRows().get(selectedRowIndex));
        }

        String message;
        if (rowsToDelete.size() == 1) {
            message = t("tutti.editSpeciesFrequencies.action.removeObservation.confirm.message");
        } else {
            message = t("tutti.editSpeciesFrequencies.action.removeObservations.confirm.message", rowsToDelete.size());
        }

        int answer = JOptionPane.showConfirmDialog(ui,
                                                   message,
                                                   t("tutti.editSpeciesFrequencies.action.removeObservations.confirm.title"),
                                                   JOptionPane.YES_NO_OPTION);

        if (answer == JOptionPane.YES_OPTION) {

            SpeciesFrequencyUIModel model = ui.getModel();
            SpeciesFrequencyUIHandler handler = ui.getHandler();

            SpeciesFrequencyTableModel frequencyTableModel = handler.getTableModel();
            
            // Attention, on ne traite ici que les observations individuelles qui ont une taille
            rowsToDelete.stream().filter(IndividualObservationBatchRowModel::withSize).forEach(row -> {

                // doit-on enlever le poids dans les mensurations ?
                boolean removeWeight = model.isCopyIndividualObservationAll() && row.withWeight();

                // doit-on enlever une  classe de taille dans les mensurations ?
                boolean removeSize = removeWeight || model.isCopyIndividualObservationSize();

                if (removeWeight) {
                    float weightToRemove = frequencyTableModel.convertWeightFromIndividualObservation(row.getWeight());
                    frequencyTableModel.removeWeightToFrequencyRow(row.getSize(), weightToRemove);
                }
                if (removeSize) {
                    frequencyTableModel.decrementFrequencyRowsNumbers(row.getSize());
                }

            });

            model.getIndividualObservationModel().removeIndividualObservations(rowsToDelete);

            model.setModify(true);
        }
    }
}

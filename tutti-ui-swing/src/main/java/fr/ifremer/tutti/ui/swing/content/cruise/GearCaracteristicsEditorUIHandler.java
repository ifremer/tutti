package fr.ifremer.tutti.ui.swing.content.cruise;

/*
 * #%L
 * Tutti :: UI
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import fr.ifremer.tutti.persistence.entities.CaracteristicMap;
import fr.ifremer.tutti.persistence.entities.referential.Caracteristic;
import fr.ifremer.tutti.persistence.entities.referential.Gear;
import fr.ifremer.tutti.service.DecoratorService;
import fr.ifremer.tutti.ui.swing.util.TuttiBeanMonitor;
import fr.ifremer.tutti.ui.swing.util.caracteristics.CaracteristicValueEditor;
import fr.ifremer.tutti.ui.swing.util.caracteristics.CaracteristicValueRenderer;
import fr.ifremer.tutti.ui.swing.util.table.AbstractTuttiTableUIHandler;
import jaxx.runtime.swing.editor.bean.BeanFilterableComboBox;
import jaxx.runtime.validator.swing.SwingValidator;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jdesktop.swingx.JXTable;
import org.jdesktop.swingx.table.DefaultTableColumnModelExt;

import javax.swing.JComponent;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Kevin Morin - kmorin@codelutin.com
 * @since 2.1
 */
public class GearCaracteristicsEditorUIHandler
        extends AbstractTuttiTableUIHandler<GearCaracteristicsEditorRowModel, GearCaracteristicsEditorUIModel, GearCaracteristicsEditorUI> {

    private final static Log log =
            LogFactory.getLog(GearCaracteristicsEditorUIHandler.class);

    public GearCaracteristicsEditorUIHandler() {
        super(GearCaracteristicsEditorRowModel.PROPERTY_VALUE);
    }

    @Override
    public GearCaracteristicsEditorTableModel getTableModel() {
        return (GearCaracteristicsEditorTableModel) getTable().getModel();
    }

    @Override
    public JXTable getTable() {
        return ui.getGearCaracteristicsEditorTable();
    }

    @Override
    protected boolean isRowValid(GearCaracteristicsEditorRowModel row) {
        return row.getKey() != null && row.getValue() != null;
    }

    @Override
    protected void onRowModified(int rowIndex, GearCaracteristicsEditorRowModel row,
                                 String propertyName, Object oldValue, Object newValue) {
        recomputeRowValidState(row);
        super.onRowModified(rowIndex, row, propertyName, oldValue, newValue);
        saveSelectedRowIfNeeded();
    }

    @Override
    protected void onAfterSelectedRowChanged(int oldRowIndex, GearCaracteristicsEditorRowModel oldRow, int newRowIndex, GearCaracteristicsEditorRowModel newRow) {
        super.onAfterSelectedRowChanged(oldRowIndex, oldRow, newRowIndex, newRow);
        if (newRow != null) {
            recomputeRowValidState(newRow);
        }
//        getModel().setRemoveCaracteristicEnabled(newRowIndex >= 0);
    }

    @Override
    protected void beforeOpenPopup(int rowIndex, int columnIndex) {
        super.beforeOpenPopup(rowIndex, columnIndex);

        getModel().setRemoveCaracteristicEnabled(rowIndex != -1);
    }

    @Override
    protected void saveSelectedRowIfRequired(TuttiBeanMonitor<GearCaracteristicsEditorRowModel> rowMonitor,
                                             GearCaracteristicsEditorRowModel row) {
        if (row.isValid()) {
            // there is a valid bean attached to the monitor

            if (rowMonitor.wasModified()) {

                // monitored bean was modified, save it
                if (log.isDebugEnabled()) {
                    log.debug("Row " + row + " was modified, will save it");
                }

                saveRow(row);

            }
        }
    }

    @Override
    public void beforeInit(GearCaracteristicsEditorUI ui) {
        super.beforeInit(ui);
        GearCaracteristicsEditorUIModel model =
                new GearCaracteristicsEditorUIModel();
        ui.setContextValue(model);

        model.addPropertyChangeListener(GearCaracteristicsEditorUIModel.PROPERTY_GEAR, evt -> editGear((Gear) evt.getNewValue()));

    }

    @Override
    public void afterInit(GearCaracteristicsEditorUI ui) {
        initUI(ui);

        initBeanFilterableComboBox(getKeyCombo(), Lists.<Caracteristic>newArrayList(), null);
        getModel().setAvailableCaracteristics(getDataContext().getCaracteristics());

        JXTable table = getTable();

        // create table column model
        DefaultTableColumnModelExt columnModel =
                new DefaultTableColumnModelExt();

        {

            addColumnToModel(columnModel,
                             null,
                             newTableCellRender(Caracteristic.class, DecoratorService.CARACTERISTIC_WITH_UNIT),
                             GearCaracteristicsEditorTableModel.KEY);
        }

        {

            addColumnToModel(columnModel,
                             new CaracteristicValueEditor(getContext()),
                             new CaracteristicValueRenderer(getContext()),
                             GearCaracteristicsEditorTableModel.VALUE);
        }

        // create table model
        GearCaracteristicsEditorTableModel tableModel =
                new GearCaracteristicsEditorTableModel(columnModel);

        table.setModel(tableModel);
        table.setColumnModel(columnModel);

        initTable(table);
    }

    @Override
    protected JComponent getComponentToFocus() {
        return getUI().getNewRowKey();
    }

    @Override
    public void onCloseUI() {
        if (log.isDebugEnabled()) {
            log.debug("closing: " + ui);
        }

        EditCruiseUI ui = getUI().getParentContainer(EditCruiseUI.class);
        ui.getMainPanelLayout().setSelected(EditCruiseUIHandler.CRUISE_CARD);

        // when canceling always invalid model (in that way)
        getModel().setValid(false);
        getModel().setGear(null);
    }

    @Override
    public SwingValidator<GearCaracteristicsEditorUIModel> getValidator() {
        return null;
    }

//    @Override
//    public void cancel() {
//
//        if (log.isDebugEnabled()) {
//            log.debug("Cancel UI " + ui);
//        }
//
//        // close dialog
//        closeUI(ui);
//    }

    protected BeanFilterableComboBox<Caracteristic> getKeyCombo() {
        return ui.getNewRowKey();
    }

    protected void saveRow(GearCaracteristicsEditorRowModel row) {

        if (row.isValid()) {
            CaracteristicMap caracteristics = getModel().getCaracteristicMap();
            Preconditions.checkNotNull(caracteristics);

            caracteristics.put(row.getKey(), row.getValue());
        }
    }

    //------------------------------------------------------------------------//
    //-- Public methods                                                     --//
    //------------------------------------------------------------------------//

    /** Adds a row with the parameter selected in the combo box */
//    public void addRow() {
//        BeanFilterableComboBox<Caracteristic> keyCombo = getKeyCombo();
//        Caracteristic selectedItem = (Caracteristic) keyCombo.getSelectedItem();
//        GearCaracteristicsEditorTableModel tableModel = getTableModel();
//
//        GearCaracteristicsEditorRowModel row = tableModel.createNewRow();
//        row.setKey(selectedItem);
////        tableModel.addNewRow(row);
//        getModel().getRows().add(row);
//
//        int rowIndex = tableModel.getRowCount() - 1;
//        tableModel.fireTableRowsInserted(rowIndex, rowIndex);
//
//        keyCombo.getHandler().removeItem(selectedItem);
//
//        GearCaracteristicsEditorUIModel model = getModel();
//        model.setModify(true);
//        recomputeRowValidState(row);
//    }

//    public void removeCaracteristic() {
//        int rowIndex = getTable().getSelectedRow();
//
//        Preconditions.checkState(
//                rowIndex != -1,
//                "Cant remove caracteristic if no caracteristic selected");
//
//        GearCaracteristicsEditorRowModel row = getTableModel().getEntry(rowIndex);
//
//        CaracteristicMap caracteristicMap = getModel().getCaracteristicMap();
//        if (caracteristicMap != null) {
//            caracteristicMap.remove(row.getKey());
//        }
//
//        //add the row in the combo
//        BeanFilterableComboBox keyCombo = getKeyCombo();
//        keyCombo.addItem(row.getKey());
////        selectFirstInCombo(keyCombo);
//        keyCombo.getHandler().reset();
//
//        // remove the row from the model
//        getModel().getRows().remove(rowIndex);
//
//        // refresh all the table
//        getTableModel().fireTableRowsDeleted(rowIndex, rowIndex);
//
//        getModel().removeRowInError(row);
//    }

//    public void save() {
//
//        if (log.isDebugEnabled()) {
//            log.debug("Save UI " + ui);
//        }
//
//        Gear gear = getModel().getGear();
//        gear.setCaracteristics((CaracteristicMap) getModel().getCaracteristicMap().clone());
//        getPersistenceService().saveGearCaracteristics(gear, getDataContext().getCruise());
//
//        closeUI(ui);
//    }
    protected void editGear(Gear gear) {

        if (gear != null) {
            GearCaracteristicsEditorTableModel tableModel = getTableModel();
            GearCaracteristicsEditorUIModel model = getModel();

            CaracteristicMap caracteristicMap = gear.getCaracteristics();
            if (caracteristicMap == null) {
                caracteristicMap = new CaracteristicMap();
            }
            // always clear caracteristic map before all
            model.getCaracteristicMap().clear();
            model.getCaracteristicMap().putAll(caracteristicMap);

            List<GearCaracteristicsEditorRowModel> rows = Lists.newArrayList();
            List<Caracteristic> caracteristics = Lists.newArrayList(caracteristicMap.keySet());

            List<Caracteristic> availableCaracteristics = model.getAvailableCaracteristics();

            for (Caracteristic key : caracteristics) {
                GearCaracteristicsEditorRowModel newRow = tableModel.createNewRow();
                newRow.setKey(key);
                newRow.setValue(caracteristicMap.get(key));
                rows.add(newRow);
            }

            model.setRows(rows);

            List<Caracteristic> caracteristicList = Lists.newArrayList();

            caracteristicList.addAll(availableCaracteristics
                                             .stream()
                                             .filter(caracteristic -> !caracteristics.contains(caracteristic))
                                             .collect(Collectors.toList()));

            BeanFilterableComboBox<Caracteristic> keyCombo = getKeyCombo();
            keyCombo.setData(caracteristicList);
            //        selectFirstInCombo(keyCombo);
            keyCombo.getHandler().reset();
            model.setModify(false);

        } else {
            cleanrRowMonitor();
            getModel().setRows(Lists.<GearCaracteristicsEditorRowModel>newArrayList());
            getTable().clearSelection();
        }
    }
}

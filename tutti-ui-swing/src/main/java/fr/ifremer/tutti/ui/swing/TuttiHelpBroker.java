package fr.ifremer.tutti.ui.swing;

/*
 * #%L
 * Tutti :: UI
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import jaxx.runtime.JAXXObject;
import jaxx.runtime.SwingUtil;
import jaxx.runtime.awt.visitor.BuildTreeVisitor;
import jaxx.runtime.awt.visitor.ComponentTreeNode;
import jaxx.runtime.awt.visitor.GetCompopentAtPointVisitor;
import jaxx.runtime.swing.help.JAXXHelpBroker;
import jaxx.runtime.swing.help.JAXXHelpUI;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.swing.AbstractButton;
import java.awt.Component;
import java.awt.Point;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;

/**
 * Help broker.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.1
 */
public class TuttiHelpBroker extends JAXXHelpBroker {

    /** Logger */
    private static final Log log = LogFactory.getLog(TuttiHelpBroker.class);

    public TuttiHelpBroker(String defaultID) {
        super("tutti", "help",
              defaultID,
              TuttiUIContext.getApplicationContext());
    }

    @Override
    public void prepareUI(JAXXObject c) {

        Preconditions.checkNotNull(c, "parameter c can not be null!");

        // l'ui doit avoir un boutton showHelp
        AbstractButton help = getShowHelpButton(c);

        if (help != null) {

            // attach context to button
            if (log.isDebugEnabled()) {
                log.debug("attach context to showhelp button " + c);
            }
            help.putClientProperty(JAXX_CONTEXT_ENTRY, c);

            // add tracking action
            ActionListener listener = getShowHelpAction();
            if (log.isDebugEnabled()) {
                log.debug("adding tracking action " + listener);
            }
            help.addActionListener(listener);

            if (log.isDebugEnabled()) {
                log.debug("done for " + c);
            }
        }
    }

    @Override
    public String findHelpId(Component comp) {

        if (comp == null) {
            comp = TuttiUIContext.getApplicationContext().getMainUI();
        }
        JAXXHelpUI parentContainer = SwingUtil.getParent(comp, JAXXHelpUI.class);

        String result;
        if (parentContainer != null && this != parentContainer.getBroker()) {

            JAXXHelpBroker broker = parentContainer.getBroker();
            result = broker.findHelpId(comp);
        } else {
            result = super.findHelpId(comp);
        }

        if (result == null) {
            result = "tutti.index.help";
        }

        return result;
    }

}

package fr.ifremer.tutti.ui.swing.content.operation.catches.species.split;

/*
 * #%L
 * Tutti :: UI
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.entities.data.SampleCategoryModel;
import fr.ifremer.tutti.persistence.entities.data.SampleCategoryModelEntry;
import fr.ifremer.tutti.type.WeightUnit;
import fr.ifremer.tutti.ui.swing.content.operation.catches.species.SelectedCategoryAble;
import fr.ifremer.tutti.ui.swing.content.operation.catches.species.SpeciesOrBenthosBatchUISupport;
import fr.ifremer.tutti.ui.swing.content.operation.catches.species.edit.SpeciesBatchRowModel;
import fr.ifremer.tutti.ui.swing.util.table.AbstractTuttiTableUIModel;

import java.util.List;

/**
 * Model of {@link SplitSpeciesBatchUI}.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 0.3
 */
public class SplitSpeciesBatchUIModel
        extends AbstractTuttiTableUIModel<SpeciesBatchRowModel, SplitSpeciesBatchRowModel, SplitSpeciesBatchUIModel> implements SelectedCategoryAble {

    private static final long serialVersionUID = 1L;

    public static final String PROPERTY_CATEGORY = "category";

    public static final String PROPERTY_SELECTED_CATEGORY = "selectedCategory";

    public static final String PROPERTY_BATCH_WEIGHT = "batchWeight";

    public static final String PROPERTY_SAMPLE_WEIGHT = "sampleWeight";

    public static final String PROPERTY_SPLIT_MODE = "splitMode";

    /**
     * Batch which fires the editor.
     *
     * @since 0.3
     */
    protected SpeciesBatchRowModel batch;

    /**
     * Sample categories.
     *
     * @since 0.3
     */
    protected List<SampleCategoryModelEntry> category;

    /**
     * Selected Sample category.
     *
     * @since 0.3
     */
    protected SampleCategoryModelEntry selectedCategory;

    /**
     * Sample weight of split batches.
     *
     * @since 0.3
     */
    protected Float sampleWeight;

    /**
     * Model of sample categories.
     *
     * @since 2.4
     */
    protected SampleCategoryModel sampleCategoryModel;

    /**
     * Is ui in split mode ?
     *
     * @since 2.6
     */
    protected boolean splitMode = true;

    private final SpeciesOrBenthosBatchUISupport speciesOrBenthosBatchUISupport;

    public SplitSpeciesBatchUIModel(SpeciesOrBenthosBatchUISupport speciesOrBenthosBatchUISupport,
                                    SampleCategoryModel sampleCategoryModel) {
        super(SpeciesBatchRowModel.class, null, null);
        this.speciesOrBenthosBatchUISupport = speciesOrBenthosBatchUISupport;
        this.sampleCategoryModel = sampleCategoryModel;
    }

    @Override
    protected SpeciesBatchRowModel newEntity() {
        return new SpeciesBatchRowModel(getWeightUnit(), sampleCategoryModel);
    }

    public SpeciesOrBenthosBatchUISupport getSpeciesOrBenthosBatchUISupport() {
        return speciesOrBenthosBatchUISupport;
    }

    public SpeciesBatchRowModel getBatch() {
        return batch;
    }

    public void setBatch(SpeciesBatchRowModel batch) {
        Object oldWeight = getBatchWeight();

        this.batch = batch;
        firePropertyChange(PROPERTY_BATCH_WEIGHT, oldWeight, getBatchWeight());
    }

    public List<SampleCategoryModelEntry> getCategory() {
        return category;
    }

    public void setCategory(List<SampleCategoryModelEntry> category) {
        Object oldValue = getCategory();
        this.category = category;
        firePropertyChange(PROPERTY_CATEGORY, oldValue, category);
    }

    public SampleCategoryModelEntry getSelectedCategory() {
        return selectedCategory;
    }

    public void setSelectedCategory(SampleCategoryModelEntry selectedCategory) {
        Object oldValue = getSelectedCategory();
        this.selectedCategory = selectedCategory;
        firePropertyChange(PROPERTY_SELECTED_CATEGORY, oldValue, selectedCategory);
    }

    public Float getBatchWeight() {
        return batch == null ? null : batch.getFinestCategory().getCategoryWeight();
    }

    public Float getSampleWeight() {
        return sampleWeight;
    }

    public void setSampleWeight(Float sampleWeight) {
        Object oldValue = getSampleWeight();
        this.sampleWeight = sampleWeight;
        firePropertyChange(PROPERTY_SAMPLE_WEIGHT, oldValue, sampleWeight);
    }

    public SampleCategoryModel getSampleCategoryModel() {
        return sampleCategoryModel;
    }

    public boolean isSplitMode() {
        return splitMode;
    }

    public void setSplitMode(boolean splitMode) {
        Object oldValue = isSplitMode();
        this.splitMode = splitMode;
        firePropertyChange(PROPERTY_SPLIT_MODE, oldValue, splitMode);
    }

    public WeightUnit getWeightUnit() {
        return speciesOrBenthosBatchUISupport.getWeightUnit();
    }
}

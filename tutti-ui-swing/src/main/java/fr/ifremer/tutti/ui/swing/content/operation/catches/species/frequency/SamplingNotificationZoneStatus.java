package fr.ifremer.tutti.ui.swing.content.operation.catches.species.frequency;

/*
 * #%L
 * Tutti :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2016 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

/**
 * Pour définir les différents états de la zone de notification des prélèvements.
 *
 * Created on 19/04/16.
 *
 * @author Tony Chemit - chemit@codelutin.com
 */
public enum SamplingNotificationZoneStatus {
    DISABLED, /* Not using algorithm */
    NOT_USING_SAMPLING, /* Selected indivdual observation is declared in algorithm, but not requiring sampling (max are 0) */
    NEED_SAMPLING, /* Selected indivdual observation need sampling */
    NO_SAMPLING_REQUIRED, /* Selected indivdual observation use sampling, but not for this one */
    COUNT_ATTAINED, /* Selected individual observation has attained one of his total count */
    NONE /* Nothing in special */
}

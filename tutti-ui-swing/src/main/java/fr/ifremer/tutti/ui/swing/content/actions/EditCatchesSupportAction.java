package fr.ifremer.tutti.ui.swing.content.actions;

/*
 * #%L
 * Tutti :: UI
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import fr.ifremer.tutti.persistence.ProgressionModel;
import fr.ifremer.tutti.persistence.entities.data.FishingOperation;
import fr.ifremer.tutti.service.TuttiDataContext;
import fr.ifremer.tutti.service.ValidationService;
import fr.ifremer.tutti.service.cruise.CruiseCacheLoader;
import fr.ifremer.tutti.ui.swing.TuttiScreen;
import fr.ifremer.tutti.ui.swing.TuttiUIContext;
import fr.ifremer.tutti.ui.swing.content.MainUIHandler;
import fr.ifremer.tutti.ui.swing.content.operation.EditFishingOperationUI;
import fr.ifremer.tutti.ui.swing.content.operation.FishingOperationsUI;
import jaxx.runtime.swing.editor.bean.BeanFilterableComboBox;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.swing.JComponent;
import javax.swing.SwingUtilities;

/**
 * Opens the operations edition screen to edit the selected operations.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.0
 */
public abstract class EditCatchesSupportAction extends AbstractChangeScreenAction {

    /** Logger. */
    private static final Log log = LogFactory.getLog(EditCatchesSupportAction.class);

    public EditCatchesSupportAction(MainUIHandler handler) {
        super(handler, true, TuttiScreen.EDIT_FISHING_OPERATION);
    }

    protected abstract boolean isLoadReferential();

    protected boolean loadReferential;
    protected boolean loadCruiseCache;

    @Override
    public boolean prepareAction() throws Exception {
        boolean doAction = super.prepareAction();

        TuttiDataContext dataContext = getDataContext();
        if (doAction) {

            loadReferential = isLoadReferential();
            loadCruiseCache = !(dataContext.isCruiseCacheLoaded() && dataContext.isCruiseCacheUpToDate());

            int totalSteps = 1;

            if (loadReferential) {
                totalSteps += 5;
            }

            // chargement des utilisateurs
            totalSteps++;

            // chargement de la série de campagne
            totalSteps++;

            // chargement de la campagne
            totalSteps++;

            // chargement des navires
            totalSteps++;

            // chargement des engins
            totalSteps++;

            // chargement des caractéristiques
            totalSteps++;

            if (loadCruiseCache) {

                // Calcul des étapes (nb de traits dans la campagne)
                long cruiseFishingOperationIds = getDataContext().getCruiseFishingOperationIds().stream().count();
                totalSteps += cruiseFishingOperationIds + 1;

                if (log.isInfoEnabled()) {
                    log.info("Found " + cruiseFishingOperationIds + " fishing operations to load in sampling cache.");
                }
            }

            if (totalSteps > 1) {
                ProgressionModel progressionModel = new ProgressionModel();
                progressionModel.setTotal(1);
                setProgressionModel(progressionModel);
                progressionModel.adaptTotal(totalSteps);
            }

        }
        return doAction;
    }

    @Override
    public void doAction() throws Exception {
        TuttiUIContext context = getContext();
        Preconditions.checkState(context.isCruiseFilled());
        Integer cruiseId = context.getCruiseId();
        if (log.isInfoEnabled()) {
            log.info("Edit operations of cruise: " + cruiseId);
        }
        context.setValidationContext(ValidationService.VALIDATION_CONTEXT_EDIT);
        if (loadReferential) {

            loadReferantials(false);

        }
        ProgressionModel progressionModel = getProgressionModel();

        progressionModel.increments("Chargement des utilisateurs");
        getDataContext().getPersons();

        progressionModel.increments("Chargement de la série de campagne");
        getDataContext().getProgram();

        progressionModel.increments("Chargement de la campagne");
        getDataContext().getCruise();

        progressionModel.increments("Chargement des navires");
        getDataContext().getScientificVessels();
        getDataContext().getFishingVessels();

        progressionModel.increments("Chargement des engins");
        getDataContext().getScientificGears();
        getDataContext().getFishingGears();

        progressionModel.increments("Chargement des caractéristiques");
        getDataContext().getCaracteristics();

        if (loadCruiseCache) {



            CruiseCacheLoader cruiseCacheLoader = context.createCruiseCacheLoader(progressionModel);
            getDataContext().loadCruiseCache(cruiseCacheLoader);

            progressionModel.increments("Chargement de l'interface graphique");

        }
        super.doAction();
    }

    @Override
    public void postSuccessAction() {
        super.postSuccessAction();

        SwingUtilities.invokeLater(
                () -> {

                    FishingOperationsUI currentBody = (FishingOperationsUI) getHandler().getCurrentBody();

                    BeanFilterableComboBox<FishingOperation> comboBox = currentBody.getFishingOperationComboBox();
                    if (!comboBox.isEmpty()) {
                        FishingOperation selectedOperation = comboBox.getData().get(0);
                        currentBody.getModel().setSelectedFishingOperation(selectedOperation);
                    }

                    EditFishingOperationUI fishingOperationTabContent = currentBody.getFishingOperationTabContent();

                    JComponent componentToFocus = fishingOperationTabContent.getHandler().getComponentToFocus();
                    componentToFocus.requestFocusInWindow();

                }
        );

    }

}

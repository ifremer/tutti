<!--
  #%L
  Tutti :: UI
  %%
  Copyright (C) 2012 - 2014 Ifremer
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation, either version 3 of the 
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public 
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/gpl-3.0.html>.
  #L%
  -->
<JPanel id='genericFormatImportTopPanel' layout='{new BorderLayout()}' decorator='help'
        implements='fr.ifremer.tutti.ui.swing.util.TuttiUI&lt;GenericFormatExportUIModel, GenericFormatExportUIHandler&gt;'>

  <import>
    fr.ifremer.tutti.persistence.entities.data.Program

    fr.ifremer.tutti.ui.swing.TuttiHelpBroker

    fr.ifremer.tutti.ui.swing.util.TuttiUI
    fr.ifremer.tutti.ui.swing.util.TuttiUIUtil
    fr.ifremer.tutti.ui.swing.content.genericformat.tree.DataSelectTreeModel
    fr.ifremer.tutti.ui.swing.content.genericformat.tree.ExportDataSelectTreeCellRenderer

    jaxx.runtime.swing.editor.bean.BeanFilterableComboBox

    javax.swing.tree.DefaultTreeModel

    static org.nuiton.i18n.I18n.t
    static jaxx.runtime.SwingUtil.getStringValue
  </import>

  <script><![CDATA[

      public GenericFormatExportUI(TuttiUI parentUI) {
          TuttiUIUtil.setParentUI(this, parentUI);
      }
  ]]></script>

  <GenericFormatExportUIModel id='model' initializer='getContextValue(GenericFormatExportUIModel.class)'/>

  <DataSelectTreeModel id='treeModel'/>

  <TuttiHelpBroker id='broker' constructorParams='"tutti.genericFormatimport.help"'/>

  <BeanValidator id='validator' bean='model' context='edit' uiClass='jaxx.runtime.validator.swing.ui.ImageValidationUI'>
    <field name='program' component='programComboBox'/>
  </BeanValidator>

  <Table fill='both' constraints='BorderLayout.NORTH'>

    <!-- select program -->
    <row>
      <cell>
        <JLabel id='programLabel'/>
      </cell>
      <cell>
        <BeanFilterableComboBox id='programComboBox' constructorParams='this' genericType='Program'/>
      </cell>
    </row>

    <!-- select data -->
    <row>
      <cell weightx='1' weighty='1' columns='2'>
        <JPanel layout='{new BorderLayout()}'>
          <JScrollPane id='dataSelectionPane' constraints='BorderLayout.CENTER'>
            <JTree id='dataSelectionTree'/>
          </JScrollPane>
          <JPanel id='exportOptions' layout='{new BorderLayout()}' constraints='BorderLayout.EAST'>
            <JPanel layout='{new GridLayout(0, 1)}' constraints='BorderLayout.NORTH'>
              <JCheckBox id='exportAttachmentsCheckBox' onItemStateChanged='handler.setBoolean(event, "exportAttachments")'/>
              <JCheckBox id='exportSpeciesCheckBox' onItemStateChanged='handler.setBoolean(event, "exportSpecies")'/>
              <JCheckBox id='exportBenthosCheckBox' onItemStateChanged='handler.setBoolean(event, "exportBenthos")'/>
              <JCheckBox id='exportMarineLitterCheckBox' onItemStateChanged='handler.setBoolean(event, "exportMarineLitter")'/>
              <JCheckBox id='exportAccidentalCatchCheckBox' onItemStateChanged='handler.setBoolean(event, "exportAccidentalCatch")'/>
              <JCheckBox id='exportIndividualObservationCheckBox' onItemStateChanged='handler.setBoolean(event, "exportIndividualObservation")'/>
            </JPanel>
          </JPanel>

          <JToolBar id='dataSelectionTreeHeader' constraints='BorderLayout.SOUTH'>
            <JButton id='unfoldAllButton'/>
            <JButton id='foldAllButton'/>
            <JButton id='selectAllButton'/>
            <JButton id='unselectAllButton'/>
          </JToolBar>

        </JPanel>
      </cell>
    </row>

    <row>
      <cell fill='both' columns='2'>
        <JPanel layout='{new GridLayout(1, 0)}' styleClass="buttonPanel">
          <JButton id='closeButton'/>
          <JButton id='exportButton'/>
        </JPanel>
      </cell>
    </row>

  </Table>

</JPanel>

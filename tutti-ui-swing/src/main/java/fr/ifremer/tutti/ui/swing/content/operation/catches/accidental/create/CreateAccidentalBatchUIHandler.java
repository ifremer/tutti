package fr.ifremer.tutti.ui.swing.content.operation.catches.accidental.create;

/*
 * #%L
 * Tutti :: UI
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Lists;
import fr.ifremer.tutti.ui.swing.content.operation.catches.EditCatchesUI;
import fr.ifremer.tutti.ui.swing.content.operation.catches.EditCatchesUIHandler;
import fr.ifremer.tutti.ui.swing.util.AbstractTuttiUIHandler;
import jaxx.runtime.validator.swing.SwingValidator;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.swing.JComponent;

/**
 * @author Kevin Morin - kmorin@codelutin.com
 * @since 1.4
 */
public class CreateAccidentalBatchUIHandler extends AbstractTuttiUIHandler<CreateAccidentalBatchUIModel, CreateAccidentalBatchUI> {

    private static final Log log =
            LogFactory.getLog(CreateAccidentalBatchUIHandler.class);

    //------------------------------------------------------------------------//
    //-- AbstractTuttiUIHandler methods                                     --//
    //------------------------------------------------------------------------//

    @Override
    public void beforeInit(CreateAccidentalBatchUI ui) {
        super.beforeInit(ui);
        CreateAccidentalBatchUIModel model = new CreateAccidentalBatchUIModel();
        ui.setContextValue(model);
        listModelIsModify(model);
    }

    @Override
    public void afterInit(CreateAccidentalBatchUI ui) {

        initUI(ui);

        initBeanFilterableComboBox(ui.getAccidentalSpeciesComboBox(),
                                   Lists.newArrayList(getDataContext().getReferentSpecies()),
                                   null);

        initBeanFilterableComboBox(ui.getAccidentalGenderComboBox(),
                                   Lists.newArrayList(getDataContext().getGenderValues()),
                                   null);

        initBeanFilterableComboBox(ui.getAccidentalLengthStepCaracteristicComboBox(),
                                   Lists.newArrayList(getDataContext().getLengthStepCaracteristics()),
                                   null);

        initBeanFilterableComboBox(ui.getAccidentalDeadOrAliveComboBox(),
                                   Lists.newArrayList(getDataContext().getDeadOrAliveValues()),
                                   null);

        listenValidatorValid(ui.getValidator(), getModel());
    }

    @Override
    protected JComponent getComponentToFocus() {
        return getUI().getAccidentalSpeciesComboBox();
    }

    @Override
    public void onCloseUI() {

        if (log.isDebugEnabled()) {
            log.debug("closing: " + ui);
        }

        // evict model from validator
        ui.getValidator().setBean(null);

        // when canceling always invalid model
        getModel().setValid(false);

        EditCatchesUI parent = getParentContainer(EditCatchesUI.class);
        parent.getHandler().setAccidentalSelectedCard(EditCatchesUIHandler.MAIN_CARD);
    }

    @Override
    public SwingValidator<CreateAccidentalBatchUIModel> getValidator() {
        return ui.getValidator();
    }

    //------------------------------------------------------------------------//
    //-- Public methods                                                     --//
    //------------------------------------------------------------------------//

    public void openUI() {

        CreateAccidentalBatchUIModel model = getModel();

        // connect model to validator
        ui.getValidator().setBean(model);

        model.reset();

    }

}

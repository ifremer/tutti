package fr.ifremer.tutti.ui.swing.content.operation.catches.accidental;

/*
 * #%L
 * Tutti :: UI
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import fr.ifremer.tutti.persistence.entities.TuttiEntities;
import fr.ifremer.tutti.persistence.entities.data.AccidentalBatch;
import fr.ifremer.tutti.persistence.entities.data.Attachment;
import fr.ifremer.tutti.persistence.entities.data.FishingOperation;
import fr.ifremer.tutti.persistence.entities.referential.Caracteristic;
import fr.ifremer.tutti.persistence.entities.referential.CaracteristicQualitativeValue;
import fr.ifremer.tutti.persistence.entities.referential.Species;
import fr.ifremer.tutti.service.DecoratorService;
import fr.ifremer.tutti.type.WeightUnit;
import fr.ifremer.tutti.ui.swing.content.operation.catches.AbstractTuttiBatchTableUIHandler;
import fr.ifremer.tutti.ui.swing.content.operation.catches.EditCatchesUI;
import fr.ifremer.tutti.ui.swing.content.operation.catches.EditCatchesUIHandler;
import fr.ifremer.tutti.ui.swing.content.operation.catches.EditCatchesUIModel;
import fr.ifremer.tutti.ui.swing.content.operation.catches.accidental.create.CreateAccidentalBatchUIModel;
import fr.ifremer.tutti.ui.swing.content.operation.catches.species.frequency.IndividualObservationBatchTableModel;
import fr.ifremer.tutti.ui.swing.util.TuttiBeanMonitor;
import fr.ifremer.tutti.ui.swing.util.TuttiUI;
import fr.ifremer.tutti.ui.swing.util.TuttiUIUtil;
import fr.ifremer.tutti.ui.swing.util.attachment.AttachmentCellEditor;
import fr.ifremer.tutti.ui.swing.util.attachment.AttachmentCellRenderer;
import fr.ifremer.tutti.ui.swing.util.caracteristics.CaracteristicMapCellComponent;
import fr.ifremer.tutti.ui.swing.util.caracteristics.CaracteristicMapColumnRowModel;
import fr.ifremer.tutti.ui.swing.util.caracteristics.CaracteristicMapColumnUIHandler;
import fr.ifremer.tutti.ui.swing.util.caracteristics.CaracteristicMapEditorUI;
import fr.ifremer.tutti.ui.swing.util.comment.CommentCellEditor;
import fr.ifremer.tutti.ui.swing.util.comment.CommentCellRenderer;
import jaxx.runtime.validator.swing.SwingValidator;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jdesktop.swingx.JXTable;
import org.jdesktop.swingx.table.DefaultTableColumnModelExt;
import org.nuiton.decorator.Decorator;
import org.nuiton.jaxx.application.swing.table.ColumnIdentifier;
import org.nuiton.validator.NuitonValidatorResult;

import javax.swing.JComponent;
import java.util.List;

/**
 * @author Tony Chemit - chemit@codelutin.com
 * @since 0.2
 */
public class AccidentalBatchUIHandler
        extends AbstractTuttiBatchTableUIHandler<AccidentalBatchRowModel, AccidentalBatchUIModel, AccidentalBatchTableModel, AccidentalBatchUI>
        implements CaracteristicMapColumnUIHandler {

    /** Logger. */
    private static final Log log =
            LogFactory.getLog(AccidentalBatchUIHandler.class);

    /**
     * Weight unit.
     *
     * @since 2.5
     */
    protected WeightUnit weightUnit;

    public AccidentalBatchUIHandler() {
        super(
                AccidentalBatchRowModel.PROPERTY_SPECIES,
                AccidentalBatchRowModel.PROPERTY_GENDER,
                AccidentalBatchRowModel.PROPERTY_WEIGHT,
                AccidentalBatchRowModel.PROPERTY_SIZE,
                AccidentalBatchRowModel.PROPERTY_LENGTH_STEP_CARACTERISTIC,
                AccidentalBatchRowModel.PROPERTY_DEAD_OR_ALIVE,
                AccidentalBatchRowModel.PROPERTY_CARACTERISTICS,
                AccidentalBatchRowModel.PROPERTY_COMMENT,
                AccidentalBatchRowModel.PROPERTY_ATTACHMENT);
    }

    //------------------------------------------------------------------------//
    //-- AbstractTuttiBatchTableUIHandler methods                           --//
    //------------------------------------------------------------------------//

    @Override
    protected ColumnIdentifier<AccidentalBatchRowModel> getCommentIdentifier() {
        return AccidentalBatchTableModel.COMMENT;
    }

    @Override
    protected ColumnIdentifier<AccidentalBatchRowModel> getAttachementIdentifier() {
        return AccidentalBatchTableModel.ATTACHMENT;
    }

    @Override
    public void selectFishingOperation(FishingOperation bean) {

        boolean empty = bean == null;

        AccidentalBatchUIModel model = getModel();

        List<AccidentalBatchRowModel> rows;

        if (empty) {
            rows = null;
        } else {

            if (log.isDebugEnabled()) {
                log.debug("Get accidental batch for fishingOperation: " +
                          bean.getId());
            }
            rows = Lists.newArrayList();

            if (!TuttiEntities.isNew(bean)) {

                // get all marine litter root
                List<AccidentalBatch> batches =
                        getPersistenceService().getAllAccidentalBatch(bean.getIdAsInt());

                for (AccidentalBatch aBatch : batches) {
                    AccidentalBatchRowModel entry = loadBatch(aBatch);
                    rows.add(entry);
                }
            }
        }
        model.setRows(rows);
    }

    //------------------------------------------------------------------------//
    //-- AbstractTuttiTableUIHandler methods                                --//
    //------------------------------------------------------------------------//

    @Override
    protected void beforeOpenPopup(int rowIndex, int columnIndex) {
        super.beforeOpenPopup(rowIndex, columnIndex);

        boolean enableRemove = false;

        if (rowIndex != -1) {

            // there is a selected row
            enableRemove = true;
        }
        AccidentalBatchUIModel model = getModel();
        model.setRemoveBatchEnabled(enableRemove);
    }

    @Override
    public AccidentalBatchTableModel getTableModel() {
        return (AccidentalBatchTableModel) getTable().getModel();
    }

    @Override
    public JXTable getTable() {
        return ui.getTable();
    }

    @Override
    protected boolean isRowValid(AccidentalBatchRowModel row) {
        AccidentalBatch batch = row.toEntity();
        NuitonValidatorResult validator =
                getValidationService().validateEditAccidentalBatch(batch);
        return !validator.hasErrorMessagess();
    }

    @Override
    protected void saveSelectedRowIfRequired(TuttiBeanMonitor<AccidentalBatchRowModel> rowMonitor,
                                             AccidentalBatchRowModel row) {

        if (row != null && row.isValid() && rowMonitor.wasModified()) {

            // monitored bean was modified, save it
            if (log.isInfoEnabled()) {
                log.info("Row " + row + " was modified, will save it");
            }

            String title = buildReminderLabelTitle(row.getSpecies(),
                                                   null,
                                                   "Sauvegarde de la capture accidentelle : ",
                                                   "Ligne :" + (getTableModel().getRowIndex(row) + 1));

            showInformationMessage(title);

            rowMonitor.setBean(null);
            saveRow(row);
            rowMonitor.setBean(row);

            // clear modified flag on the monitor
            rowMonitor.clearModified();
        }
    }

    //------------------------------------------------------------------------//
    //-- AbstractTuttiUIHandler methods                                     --//
    //------------------------------------------------------------------------//

    @Override
    public SwingValidator<AccidentalBatchUIModel> getValidator() {
        return null;
    }

    @Override
    public void beforeInit(AccidentalBatchUI ui) {
        super.beforeInit(ui);

        weightUnit = getConfig().getAccidentalCatchWeightUnit();

        if (log.isDebugEnabled()) {
            log.debug("beforeInit: " + ui);
        }

        EditCatchesUIModel catchesUIModel =
                ui.getContextValue(EditCatchesUIModel.class);

        AccidentalBatchUIModel model = new AccidentalBatchUIModel(catchesUIModel);
        ui.setContextValue(model);
    }

    @Override
    public void afterInit(AccidentalBatchUI ui) {

        if (log.isDebugEnabled()) {
            log.debug("afterInit: " + ui);
        }

        initUI(ui);

        JXTable table = getTable();

        // create table column model
        DefaultTableColumnModelExt columnModel =
                new DefaultTableColumnModelExt();

        Decorator<CaracteristicQualitativeValue> caracteristicQualitativeValueDecorator =
                getDecorator(CaracteristicQualitativeValue.class, null);

        {
            // Id column

            addIdColumnToModel(columnModel, AccidentalBatchTableModel.ID, table);

        }

        {
            // Species column

            Decorator<Species> speciesDecorator = getDecorator(
                    Species.class, DecoratorService.FROM_PROTOCOL);
            addComboDataColumnToModel(columnModel,
                                      AccidentalBatchTableModel.SPECIES,
                                      speciesDecorator,
                                      getDataContext().getReferentSpecies());

        }

        { // Gender caracteristic column

            addComboDataColumnToModel(columnModel,
                                      AccidentalBatchTableModel.GENDER,
                                      caracteristicQualitativeValueDecorator,
                                      getDataContext().getGenderValues());

        }

        { // Weight column

            addFloatColumnToModel(columnModel,
                                  AccidentalBatchTableModel.WEIGHT,
                                  weightUnit,
                                  table);
        }

        { // Size column

            addFloatColumnToModel(columnModel,
                                  AccidentalBatchTableModel.SIZE,
                                  TuttiUI.DECIMAL3_PATTERN,
                                  table);
        }

        { // Length step caracteristic column
            Decorator<Caracteristic> caracteristicDecorator =
                    getDecorator(Caracteristic.class, null);
            addComboDataColumnToModel(columnModel,
                                      AccidentalBatchTableModel.LENGTH_STEP_CARACTERISTIC,
                                      caracteristicDecorator,
                                      getDataContext().getLengthStepCaracteristics());

        }

        { // Dead or alive caracteristic column

            addComboDataColumnToModel(columnModel,
                                      AccidentalBatchTableModel.DEAD_OR_ALIVE,
                                      caracteristicQualitativeValueDecorator,
                                      getDataContext().getDeadOrAliveValues());

        }

        { // Other caracteristics column

            addColumnToModel(columnModel,
                             CaracteristicMapCellComponent.newEditor(ui, Sets.<Caracteristic>newHashSet()),
                             CaracteristicMapCellComponent.newRender(getContext()),
                             IndividualObservationBatchTableModel.OTHER_CARACTERISTICS);

        }

        { // Comment column

            addColumnToModel(columnModel,
                             CommentCellEditor.newEditor(ui),
                             CommentCellRenderer.newRender(),
                             AccidentalBatchTableModel.COMMENT);
        }

        { // File column

            Decorator<Attachment> decorator = getDecorator(Attachment.class, null);
            addColumnToModel(columnModel,
                             AttachmentCellEditor.newEditor(ui),
                             AttachmentCellRenderer.newRender(decorator),
                             AccidentalBatchTableModel.ATTACHMENT);
        }

        // create table model
        AccidentalBatchTableModel tableModel =
                new AccidentalBatchTableModel(weightUnit, columnModel);

        table.setModel(tableModel);
        table.setColumnModel(columnModel);

        initBatchTable(table, columnModel, tableModel);
    }

    @Override
    protected JComponent getComponentToFocus() {
        return getUI().getTable();
    }

    @Override
    public void onCloseUI() {
        if (log.isDebugEnabled()) {
            log.debug("closing: " + ui);
        }
        ui.getAccidentalBatchAttachmentsButton().onCloseUI();
    }

    @Override
    public CaracteristicMapEditorUI getCaracteristicMapEditor() {
        EditCatchesUI parent = getParentContainer(EditCatchesUI.class);
        return parent.getAccidentalCaracteristicMapEditor();
    }

    @Override
    public void showCaracteristicMapEditor(CaracteristicMapColumnRowModel editRow) {
        EditCatchesUI parent = getParentContainer(EditCatchesUI.class);
        parent.getHandler().setAccidentalSelectedCard(EditCatchesUIHandler.EDIT_CARACTERISTICS_CARD, editRow.getSpecies());
    }

    @Override
    public void hideCaracteristicMapEditor() {
        EditCatchesUI parent = getParentContainer(EditCatchesUI.class);
        parent.getHandler().setAccidentalSelectedCard(EditCatchesUIHandler.MAIN_CARD);
    }

    //------------------------------------------------------------------------//
    //-- Public methods                                                     --//
    //------------------------------------------------------------------------//

    public void addBatch(CreateAccidentalBatchUIModel model) {
        if (model.isValid()) {

            AccidentalBatchTableModel tableModel = getTableModel();

            AccidentalBatchRowModel newRow = tableModel.createNewRow();
            newRow.setSpecies(model.getSpecies());
            newRow.setGender(model.getGender());
            newRow.setWeight(model.getWeight());
            newRow.setSize(model.getSize());
            newRow.setLengthStepCaracteristic(model.getLengthStepCaracteristic());
            newRow.setDeadOrAlive(model.getDeadOrAlive());

            recomputeRowValidState(newRow);

            saveRow(newRow);

            tableModel.addNewRow(newRow);
            TuttiUIUtil.selectFirstCellOnLastRow(getTable());
        }
    }

    //------------------------------------------------------------------------//
    //-- Internal methods                                                   --//
    //------------------------------------------------------------------------//

    protected AccidentalBatchRowModel loadBatch(AccidentalBatch aBatch) {

        AccidentalBatchRowModel newRow =
                new AccidentalBatchRowModel(weightUnit, aBatch);

        List<Attachment> attachments =
                getPersistenceService().getAllAttachments(newRow.getObjectType(),
                                                          newRow.getObjectId());

        newRow.addAllAttachment(attachments);

        return newRow;
    }

    protected void saveRow(AccidentalBatchRowModel row) {

        AccidentalBatch entityToSave = row.toEntity();

        FishingOperation fishingOperation = getModel().getFishingOperation();
        entityToSave.setFishingOperation(fishingOperation);
        if (log.isDebugEnabled()) {
            log.debug("Selected fishingOperation: " + fishingOperation.getId());
        }

        if (TuttiEntities.isNew(entityToSave)) {

            getPersistenceService().createAccidentalBatch(entityToSave);
            row.setId(entityToSave.getId());
        } else {
            getPersistenceService().saveAccidentalBatch(entityToSave);
        }

        fireBatchSaved(row);
    }
}

<!--
  #%L
  Tutti :: UI
  %%
  Copyright (C) 2012 - 2014 Ifremer
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation, either version 3 of the 
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public 
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/gpl-3.0.html>.
  #L%
  -->
<JDialog id='rtpEditorDialog' layout='{new BorderLayout()}' modal="true"
         implements='fr.ifremer.tutti.ui.swing.util.TuttiUI&lt;RtpEditorUIModel, RtpEditorUIHandler&gt;'>

  <import>

    fr.ifremer.tutti.ui.swing.TuttiHelpBroker
    fr.ifremer.tutti.ui.swing.TuttiUIContext
    fr.ifremer.tutti.ui.swing.content.protocol.EditProtocolSpeciesTableModel
    fr.ifremer.tutti.ui.swing.util.TuttiUI
    fr.ifremer.tutti.ui.swing.util.TuttiUIUtil

    org.jdesktop.swingx.JXTitledPanel
    org.nuiton.jaxx.widgets.number.NumberEditor

    javax.swing.JComponent
    javax.swing.RowSorter
  </import>

  <script><![CDATA[

public RtpEditorUI(TuttiUI<?,?> parentUI) {
    super(parentUI.getHandler().getContext().getMainUI());
    TuttiUIUtil.setParentUI(this, parentUI);
}

public void setBean(EditProtocolSpeciesTableModel tableModel, RowSorter rowSorter, int row) {
    handler.setBean(tableModel, rowSorter, row);
}

public void openEditor() { handler.openEditor(); }

public void closeEditor() { handler.closeEditor(); }

  ]]></script>

  <RtpEditorUIModel id="model" initializer='getContextValue(RtpEditorUIModel.class)'/>

  <TuttiHelpBroker id='broker' constructorParams='"tutti.editRtp.help"'/>

  <BeanValidator id='validator' bean='model' uiClass='jaxx.runtime.validator.swing.ui.ImageValidationUI'>
    <field name='rtpMaleA' component='rtpMaleAField'/>
    <field name='rtpMaleB' component='rtpMaleBField'/>
    <field name='rtpFemaleA' component='rtpFemaleAField'/>
    <field name='rtpFemaleB' component='rtpFemaleBField'/>
    <field name='rtpUndefinedA' component='rtpUndefinedAField'/>
    <field name='rtpUndefinedB' component='rtpUndefinedBField'/>
  </BeanValidator>

  <JToolBar id='headerToolBar'>
    <JButton id="previousRowButton"/>
    <JButton id="nextRowButton"/>
  </JToolBar>

  <JXTitledPanel id="bodyPanel" constraints='BorderLayout.CENTER'>
    <JPanel layout='{new BorderLayout()}'>
      <JPanel id='rtpFormulaLabelContainer' layout='{new BorderLayout(10, 10)}'
              constraints='BorderLayout.NORTH'>
              <JLabel id="rtpFormulaLabel" constraints='BorderLayout.CENTER'/>
      </JPanel>

      <JPanel layout="{new GridLayout(3, 1)}" constraints='BorderLayout.CENTER'>
        <JPanel id="malePanel" layout="{new GridLayout(2, 1)}" border='{new TitledBorder(t("tutti.editRtp.malePanel"))}'>
          <JPanel layout="{new GridLayout(1, 2)}">
            <Table id='maleAPanel' fill='both'>
              <row>
                <cell>
                  <JLabel id='rtpMaleALabel'/>
                </cell>
                <cell weightx='1'>
                  <NumberEditor id='rtpMaleAField' styleClass="aEditor"/>
                </cell>
              </row>
            </Table>
            <Table id='maleBPanel' fill='both'>
              <row>
                <cell>
                  <JLabel id='rtpMaleBLabel'/>
                </cell>
                <cell weightx='1'>
                  <NumberEditor id='rtpMaleBField' styleClass="bEditor"/>
                </cell>
              </row>
            </Table>
          </JPanel>
          <JButton id="copyValuesButton" />
        </JPanel>

        <JPanel id="femalePanel" layout="{new GridLayout(1, 2)}"
                border='{new TitledBorder(t("tutti.editRtp.femalePanel"))}'>
          <Table id='femaleAPanel' fill='both'>
            <row>
              <cell>
                <JLabel id='rtpFemaleALabel'/>
              </cell>
              <cell weightx='1'>
                <NumberEditor id='rtpFemaleAField' styleClass="aEditor"/>
              </cell>
            </row>
          </Table>
          <Table id='femaleBPanel' fill='both'>
            <row>
              <cell>
                <JLabel id='rtpFemaleBLabel'/>
              </cell>
              <cell weightx='1'>
                <NumberEditor id='rtpFemaleBField' styleClass="bEditor"/>
              </cell>
            </row>
          </Table>
        </JPanel>

        <JPanel id="undefinedPanel" layout="{new GridLayout(1, 2)}"
                border='{new TitledBorder(t("tutti.editRtp.undefinedPanel"))}'>
          <Table id='undefinedAPanel' fill='both'>
            <row>
              <cell>
                <JLabel id='rtpUndefinedALabel'/>
              </cell>
              <cell weightx='1'>
                <NumberEditor id='rtpUndefinedAField' styleClass="aEditor"/>
              </cell>
            </row>
          </Table>
          <Table id='undefinedBPanel' fill='both'>
            <row>
              <cell>
                <JLabel id='rtpUndefinedBLabel'/>
              </cell>
              <cell weightx='1'>
                <NumberEditor id='rtpUndefinedBField' styleClass="bEditor"/>
              </cell>
            </row>
          </Table>
        </JPanel>
      </JPanel>
    </JPanel>

  </JXTitledPanel>

  <JPanel id="actions" constraints='BorderLayout.SOUTH' layout="{new GridLayout(1, 2)}">
    <JButton id='closeButton'/>
    <JButton id='saveButton'/>
  </JPanel>

</JDialog>

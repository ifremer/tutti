package fr.ifremer.tutti.ui.swing.content.operation.catches.accidental.actions;

/*
 * #%L
 * Tutti :: UI
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.entities.data.FishingOperation;
import fr.ifremer.tutti.service.catches.multipost.MultiPostImportService;
import fr.ifremer.tutti.ui.swing.content.operation.catches.actions.ImportMultiPostActionSupport;
import fr.ifremer.tutti.ui.swing.content.operation.catches.accidental.AccidentalBatchUI;
import fr.ifremer.tutti.ui.swing.content.operation.catches.accidental.AccidentalBatchUIHandler;
import fr.ifremer.tutti.ui.swing.content.operation.catches.accidental.AccidentalBatchUIModel;

import java.io.File;
import java.util.Map;

import static org.nuiton.i18n.I18n.t;

/**
 * @author Kevin Morin - kmorin@codelutin.com
 * @since 2.2
 */
public class ImportMultiPostAccidentalAction
        extends ImportMultiPostActionSupport<AccidentalBatchUIModel, AccidentalBatchUI, AccidentalBatchUIHandler> {

    public ImportMultiPostAccidentalAction(AccidentalBatchUIHandler handler) {
        super(handler);
    }

    @Override
    protected String getFileExtension() {
        return "tuttiAccidental";
    }

    @Override
    protected String getFileExtensionDescription() {
        return t("tutti.common.file.tuttiAccidental");
    }

    @Override
    protected String getFileChooserTitle() {
        return t("tutti.editAccidentalBatch.action.importMultiPost.sourceFile.title");
    }

    @Override
    protected String getFileChooserButton() {
        return t("tutti.editAccidentalBatch.action.importMultiPost.sourceFile.button");
    }

    @Override
    protected String getSuccessMessage(File file) {
        return t("tutti.editAccidentalBatch.action.importMultiPost.success", file);
    }

    @Override
    protected Map<String, Object> importBatches(MultiPostImportService multiPostImportExportService, File file, FishingOperation operation) {

        multiPostImportExportService.importAccidentalCatches(file, operation);
        return null;

    }

}

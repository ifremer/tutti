package fr.ifremer.tutti.ui.swing.content.operation.catches.species.frequency;

/*
 * #%L
 * Tutti :: UI
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Lists;
import com.google.common.collect.Ordering;
import fr.ifremer.tutti.persistence.entities.data.SpeciesBatch;
import fr.ifremer.tutti.persistence.entities.data.SpeciesBatchFrequency;
import fr.ifremer.tutti.persistence.entities.data.SpeciesBatchFrequencys;
import fr.ifremer.tutti.persistence.entities.referential.Caracteristic;
import fr.ifremer.tutti.type.WeightUnit;
import fr.ifremer.tutti.ui.swing.util.AbstractTuttiBeanUIModel;
import org.nuiton.util.beans.Binder;
import org.nuiton.util.beans.BinderFactory;

import java.util.List;

/**
 * Represents a batch frequency row.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 0.2
 */
public class SpeciesFrequencyRowModel extends AbstractTuttiBeanUIModel<SpeciesBatchFrequency, SpeciesFrequencyRowModel> implements SpeciesBatchFrequency, Comparable<SpeciesFrequencyRowModel> {

    private static final long serialVersionUID = 1L;

    public static final String PROPERTY_LENGTH_STEP = "lengthStep";

    public static final String PROPERTY_NUMBER = "number";

    public static final String PROPERTY_WEIGHT = "weight";

    public static final String PROPERTY_RTP_COMPUTED_WEIGHT = "rtpComputedWeight";

    public static final String PROPERTY_LENGHT_STEP_CARACTERISTIC = "lengthStepCaracteristic";

    /**
     * Length step.
     *
     * @since 0.2
     */
    protected Float lengthStep;

    /**
     * Count of fishes for this lengthStep.
     *
     * @since 0.2
     */
    protected Integer number;

    /**
     * Weight of fishes observed.
     *
     * @since 4.5
     */
    protected Float weight;

    /**
     * Weight of fishes computed with RTPs.
     *
     * @since 4.5
     */
    protected Float rtpComputedWeight;

    /**
     * Length step caracteristic.
     *
     * @since 0.3
     */
    protected Caracteristic lengthStepCaracteristic;

    /**
     * Weight unit.
     *
     * @since 2.5
     */
    protected final WeightUnit weightUnit;

    protected static final Binder<SpeciesBatchFrequency, SpeciesFrequencyRowModel> fromBeanBinder =
            BinderFactory.newBinder(SpeciesBatchFrequency.class,
                                    SpeciesFrequencyRowModel.class);

    protected static final Binder<SpeciesFrequencyRowModel, SpeciesBatchFrequency> toBeanBinder =
            BinderFactory.newBinder(SpeciesFrequencyRowModel.class,
                                    SpeciesBatchFrequency.class);

    private static final Ordering<Float> ordering = Ordering.natural().nullsFirst();

    //------------------------------------------------------------------------//
    //-- Factory methods                                                    --//
    //------------------------------------------------------------------------//

    public static List<SpeciesFrequencyRowModel> fromEntity(WeightUnit weightUnit,
                                                            List<SpeciesBatchFrequency> entities) {
        List<SpeciesFrequencyRowModel> result = Lists.newArrayList();
        for (SpeciesBatchFrequency entity : entities) {

            SpeciesFrequencyRowModel row =
                    new SpeciesFrequencyRowModel(weightUnit);
            row.fromEntity(entity);
            result.add(row);
        }
        return result;
    }

    public static List<SpeciesBatchFrequency> toEntity(List<SpeciesFrequencyRowModel> rows,
                                                       SpeciesBatch batch) {
        List<SpeciesBatchFrequency> result = Lists.newArrayList();
        for (SpeciesFrequencyRowModel row : rows) {

            SpeciesBatchFrequency entity = row.toEntity();
            entity.setBatch(batch);
            result.add(entity);
        }
        return result;
    }

    public SpeciesFrequencyRowModel(WeightUnit weightUnit) {
        super(fromBeanBinder, toBeanBinder);
        this.weightUnit = weightUnit;
    }

    //------------------------------------------------------------------------//
    //-- AbstractTuttiBeanUIModel                                           --//
    //------------------------------------------------------------------------//

    @Override
    protected SpeciesBatchFrequency newEntity() {
        return SpeciesBatchFrequencys.newSpeciesBatchFrequency();
    }

    @Override
    public void fromEntity(SpeciesBatchFrequency entity) {
        super.fromEntity(entity);

        // convert weight
        setWeight(weightUnit.fromEntity(getWeight()));
    }

    @Override
    public SpeciesBatchFrequency toEntity() {
        SpeciesBatchFrequency result = super.toEntity();

        // convert weight
        result.setWeight(weightUnit.toEntity(getWeight()));
        return result;
    }

    //------------------------------------------------------------------------//
    //-- SpeciesBatchFrequency                                              --//
    //------------------------------------------------------------------------//

    @Override
    public Float getLengthStep() {
        return lengthStep;
    }

    @Override
    public void setLengthStep(Float lengthStep) {
        Object oldValue = getLengthStep();
        this.lengthStep = lengthStep;
        firePropertyChange(PROPERTY_LENGTH_STEP, oldValue, lengthStep);
    }

    @Override
    public Integer getNumber() {
        return number;
    }

    @Override
    public void setNumber(Integer number) {
        Object oldValue = getNumber();
        this.number = number;
        firePropertyChange(PROPERTY_NUMBER, oldValue, number);
    }

    public boolean withNumber() {
        return number != null && number > 0;
    }

    public void incNumber() {
        if (number == null) {
            number = 0;
        }
        setNumber(number + 1);
    }

    public void decNumber() {
        decNumber(1);
    }

    public void decNumber(int nb) {
        if (number != null && number >= nb) {
            setNumber(number - nb);
        }
    }

    @Override
    public Float getWeight() {
        return weight;
    }

    @Override
    public void setWeight(Float weight) {
        Object oldValue = getWeight();
        this.weight = weight;
        firePropertyChange(PROPERTY_WEIGHT, oldValue, weight);
    }

    public boolean withWeight() {
        Float weight = getWeight();
        return weightUnit.isNotNullNorZero(weight);
    }

    /**
     * @param weightToAdd weight (cannot be negative) to add
     */
    public void addToWeight(float weightToAdd) {
        if (weightUnit.isSmallerThanZero(weightToAdd)) {
            throw new IllegalArgumentException("you must add a positive weight");
        }
        if (weight == null) {
            weight = 0f;
        }
        setWeight(weightUnit.round(weight + weightToAdd));
    }

    /**
     * @param weightToRemove weight (cannot be negative) to remove
     */
    public void removeFromWeight(float weightToRemove) {
        if (weightUnit.isSmallerThanZero(weightToRemove)) {
            throw new IllegalArgumentException("you must remove a positive weight");
        }
        if (weight == null) {
            weight = 0f;
        }
        if (weightUnit.isSmallerThan(weight, weightToRemove)) {
            throw new IllegalArgumentException("the weight to remove cannot be greater than the initial weight");
        }
        setWeight(weightUnit.round(weight - weightToRemove));
    }

    public Float getRtpComputedWeight() {
        return rtpComputedWeight;
    }

    public void setRtpComputedWeight(Float rtpComputedWeight) {
        Object oldValue = getRtpComputedWeight();
        this.rtpComputedWeight = rtpComputedWeight;
        firePropertyChange(PROPERTY_RTP_COMPUTED_WEIGHT, oldValue, rtpComputedWeight);
    }

    public boolean withRtpComputedWeight() {
        return rtpComputedWeight != null;
    }

    @Override
    public Caracteristic getLengthStepCaracteristic() {
        return lengthStepCaracteristic;
    }

    @Override
    public void setLengthStepCaracteristic(Caracteristic lengthStepCaracteristic) {
        Object oldValue = getLengthStepCaracteristic();
        this.lengthStepCaracteristic = lengthStepCaracteristic;
        firePropertyChange(PROPERTY_LENGHT_STEP_CARACTERISTIC, oldValue, lengthStepCaracteristic);
    }

    @Override
    public SpeciesBatch getBatch() {
        return null;
    }

    @Override
    public void setBatch(SpeciesBatch batch) {
    }

    @Override
    public int compareTo(SpeciesFrequencyRowModel o) {
        return ordering.compare(lengthStep, o.lengthStep);
    }

    @Override
    public Integer getRankOrder() {
        return null;
    }

    @Override
    public void setRankOrder(Integer rankOrder) {
    }

    @Override
    public boolean isBenthosFrequencyBatch() {
        return false;
    }

    @Override
    public void setBenthosFrequencyBatch(boolean benthosFrequencyBatch) {
        //Never!
    }

    public boolean isEmpty() {
        return lengthStep == null && (weight == null || number == null);
    }

    public Float computeAverageWeight() {
        Float averageWeight = null;
        if (withWeight() && withNumber()) {
            averageWeight = weight / number;
        }
        return averageWeight;
    }

    public void copy(SpeciesFrequencyRowModel source) {
        setLengthStepCaracteristic(source.getLengthStepCaracteristic());
        setLengthStep(source.getLengthStep());
        setNumber(source.getNumber());
        setWeight(source.getWeight());
    }

    public boolean withLengthStep() {
        return lengthStep != null;
    }
}

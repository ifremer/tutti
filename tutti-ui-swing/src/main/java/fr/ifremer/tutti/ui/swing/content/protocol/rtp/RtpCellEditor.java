package fr.ifremer.tutti.ui.swing.content.protocol.rtp;

/*
 * #%L
 * Tutti :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2016 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.ui.swing.content.protocol.EditProtocolSpeciesTableModel;
import fr.ifremer.tutti.ui.swing.content.protocol.EditProtocolUI;

import javax.swing.AbstractCellEditor;
import javax.swing.JTable;
import javax.swing.border.LineBorder;
import javax.swing.event.CellEditorListener;
import javax.swing.event.ChangeEvent;
import javax.swing.table.TableCellEditor;
import java.awt.Color;
import java.awt.Component;
import java.util.EventObject;

/**
 * Created on 14/01/16.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 4.4
 */
public class RtpCellEditor extends AbstractCellEditor implements TableCellEditor {

    private static final long serialVersionUID = 1L;

    public static TableCellEditor newEditor(EditProtocolUI ui) {
        return new RtpCellEditor(ui);
    }

    protected final RtpButton editorButton;

    public RtpCellEditor(EditProtocolUI context) {

        editorButton = new RtpButton(context);
        editorButton.setBorder(new LineBorder(Color.BLACK));
        addCellEditorListener(new CellEditorListener() {
            @Override
            public void editingStopped(ChangeEvent e) {
                editorButton.setSelected(false);
            }

            @Override
            public void editingCanceled(ChangeEvent e) {
                editorButton.setSelected(false);
            }
        });
    }

    @Override
    public Component getTableCellEditorComponent(JTable table,
                                                 Object value,
                                                 boolean isSelected,
                                                 int row,
                                                 int column) {

        EditProtocolSpeciesTableModel tableModel = (EditProtocolSpeciesTableModel) table.getModel();
        editorButton.init(tableModel, table.getRowSorter(), row);
        return editorButton;

    }

    @Override
    public boolean shouldSelectCell(EventObject anEvent) {
        return false;
    }

    @Override
    public Object getCellEditorValue() {

        return true;
    }

}

package fr.ifremer.tutti.ui.swing.content.protocol.actions;

/*
 * #%L
 * Tutti :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.service.protocol.ProtocolCaracteristicsImportExportService;
import fr.ifremer.tutti.ui.swing.content.protocol.EditProtocolUI;
import fr.ifremer.tutti.ui.swing.content.protocol.EditProtocolUIHandler;
import fr.ifremer.tutti.ui.swing.content.protocol.EditProtocolUIModel;
import fr.ifremer.tutti.ui.swing.util.actions.LongActionSupport;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.File;
import java.util.List;

import static org.nuiton.i18n.I18n.t;

/**
 * @author Kevin Morin (Code Lutin)
 * @since 3.10
 */
public class LoadProtocolImportColumnsAction extends LongActionSupport<EditProtocolUIModel, EditProtocolUI, EditProtocolUIHandler> {

    /** Logger. */
    private static final Log log = LogFactory.getLog(LoadProtocolImportColumnsAction.class);

    protected File columnsFile;

    public LoadProtocolImportColumnsAction(EditProtocolUIHandler handler) {
        super(handler, false);
    }

    @Override
    public boolean prepareAction() throws Exception {
        boolean result = super.prepareAction();

        if (result) {
            columnsFile = chooseFile(t("tutti.editProtocol.action.title.choose.importColumnsFile"),
                                     t("tutti.editProtocol.action.chooseColumnsFile.import"),
                                     "^.*\\.csv", t("tutti.common.file.csv"));

            result = columnsFile != null;
        }

        return result;
    }

    @Override
    public void doAction() throws Exception {

        ProtocolCaracteristicsImportExportService service = getContext().getProtocolCaracteristicsImportExportService();
        List<String> columns = service.loadProtocolCaracteristicsImportColumns(columnsFile);
        if (log.isInfoEnabled()) {
            log.info("Detected columns: "+columns);
        }
        getModel().setImportColumns(columns);

    }

    @Override
    public void postSuccessAction() {
        super.postSuccessAction();
        sendMessage(t("tutti.editProtocol.action.loadImportColumns.success", getModel().sizeImportColumns()));
    }
}

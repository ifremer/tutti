package fr.ifremer.tutti.ui.swing.content.operation.catches.actions;

/*
 * #%L
 * Tutti :: UI
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.ProgressionModel;
import fr.ifremer.tutti.persistence.entities.data.FishingOperation;
import fr.ifremer.tutti.service.DecoratorService;
import fr.ifremer.tutti.service.catches.multipost.MultiPostExportService;
import fr.ifremer.tutti.ui.swing.content.operation.catches.EditCatchesUI;
import fr.ifremer.tutti.ui.swing.content.operation.catches.EditCatchesUIModel;
import fr.ifremer.tutti.ui.swing.util.AbstractTuttiUIHandler;
import fr.ifremer.tutti.ui.swing.util.TuttiUI;
import fr.ifremer.tutti.ui.swing.util.actions.LongActionSupport;
import org.jdesktop.beans.AbstractBean;
import org.nuiton.jaxx.application.swing.AbstractApplicationUIHandler;

import javax.swing.JOptionPane;
import java.io.File;

import static org.nuiton.i18n.I18n.t;

/**
 * @author Kevin Morin - kmorin@codelutin.com
 * @since 2.2
 */
public abstract class ExportMultiPostActionSupport<M extends AbstractBean, UI extends TuttiUI<M, ?>, H extends AbstractTuttiUIHandler<M, UI>>
        extends LongActionSupport<M, UI, H> {

    /**
     * Do save the catches before export.
     */
    private Boolean doSaveBeforeExport;

    /**
     * File to export.
     */
    private File file;

    /**
     * Fishing operation to export.
     */
    private FishingOperation fishingOperation;

    protected abstract String getFileExtension();

    protected abstract String getFileExtensionDescription();

    protected abstract String getFileChooserTitle();

    protected abstract String getFileChooserButton();

    protected abstract String getSuccessMessage(File file);

    protected abstract void doExport(MultiPostExportService multiPostImportExportService, File file, FishingOperation fishingOperation);

    public ExportMultiPostActionSupport(H handler) {
        super(handler, false);
    }

    protected EditCatchesUI getEditCatchUI() {
        return getUI().getParentContainer(EditCatchesUI.class);
    }

    /**
     * Demande si il faut sauvegarder les données de l'écran avant de lancer l'export.
     *
     * @return {@code null} si pas de sauvegarde à faire,
     * {@code 0} si on doit effectuer la sauvegarde avant l'export, {@code 1} pour ne rien faire avant l'export, {@code 2} pour annuler l'action
     * @since 4.5
     */
    protected Integer askToSaveBeforeExport() {

        EditCatchesUIModel model = getEditCatchUI().getModel();

        Integer doSaveBeforeExportResponse = null;

        boolean canSave = model.isModify() && model.isValid();
        if (canSave) {

            String htmlMessage = String.format(
                    AbstractApplicationUIHandler.CONFIRMATION_FORMAT,
                    t("tutti.askToSaveCatch.message"),
                    t("tutti.askToSaveCatchBeforeExport.help"));

            doSaveBeforeExportResponse = JOptionPane.showOptionDialog(getHandler().getTopestUI(),
                                                                      htmlMessage,
                                                                      t("tutti.askToSaveCatch.title"),
                                                                      JOptionPane.OK_CANCEL_OPTION,
                                                                      JOptionPane.QUESTION_MESSAGE,
                                                                      null,
                                                                      new String[]{t("tutti.option.saveCatch"), t("tutti.option.notSaveCatch"), t("tutti.option.cancelExport")},
                                                                      t("tutti.option.saveCatch"));


        }

        return doSaveBeforeExportResponse;

    }

    @Override
    public final boolean prepareAction() throws Exception {

        boolean doAction = super.prepareAction();

        if (doAction) {

            fishingOperation = getEditCatchUI().getModel().getFishingOperation();

            Integer doSaveBeforeExportResponse = askToSaveBeforeExport();

            doSaveBeforeExport = false;

            if (doSaveBeforeExportResponse != null) {

                switch (doSaveBeforeExportResponse) {
                    case 0:

                        // should save
                        doSaveBeforeExport = true;
                        break;
                    case 1:

                        // nothing to do before export
                        break;
                    case 2:

                        // cancel import
                        doAction = false;
                        break;
                }

            }

            if (doAction) {

                String extension = getFileExtension();

                // choose file to export
                file = saveFile(decorate(fishingOperation, DecoratorService.FILE_NAME_COMPATIBLE),
                                extension,
                                getFileChooserTitle(),
                                getFileChooserButton(),
                                "^.*\\." + extension,
                                getFileExtensionDescription());

                doAction = file != null;

            }

        }

        return doAction;

    }

    protected void saveBeforeExport() {

        getProgressionModel().increments("Sauvegarde de la capture");

        // save catches before export

        EditCatchesUI catchesUI = getEditCatchUI();
        getActionEngine().runInternalAction(catchesUI.getHandler(), SaveCatchBatchAction.class);

    }

    @Override
    public final void doAction() throws Exception {

        if (doSaveBeforeExport) {

            setProgressionModel(new ProgressionModel());
            ProgressionModel progressionModel = getProgressionModel();
            progressionModel.setTotal(2);

            saveBeforeExport();

            progressionModel.increments("Lancement de l'export");

        }

        // do the export

        MultiPostExportService multiPostImportExportService = getContext().getMultiPostExportService();

        doExport(multiPostImportExportService, file, fishingOperation);

    }

    @Override
    public final void releaseAction() {
        file = null;
        fishingOperation = null;
        doSaveBeforeExport = null;
        super.releaseAction();
    }

    @Override
    public final void postSuccessAction() {
        super.postSuccessAction();
        sendMessage(getSuccessMessage(file));
    }

}

package fr.ifremer.tutti.ui.swing.content.validation;

/*
 * #%L
 * Tutti :: UI
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ImmutableSet;
import fr.ifremer.tutti.persistence.entities.data.Cruise;
import fr.ifremer.tutti.persistence.entities.data.Cruises;
import fr.ifremer.tutti.persistence.entities.data.FishingOperation;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jdesktop.beans.AbstractSerializableBean;
import org.nuiton.validator.NuitonValidatorResult;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

/**
 * @author Kevin Morin - kmorin@codelutin.com
 * @since 1.4
 */
public class ValidateCruiseUIModel extends AbstractSerializableBean {

    private static final long serialVersionUID = 1L;

    /** Logger. */
    private static final Log log = LogFactory.getLog(ValidateCruiseUIModel.class);

    public static final String PROPERTY_SELECTED_FISHING_OPERATION = "selectedFishingOperation";

    public static final String PROPERTY_SELECTED_CRUISE = "selectedCruise";

    public static final String PROPERTY_READY_TO_SYNCH = "readyToSynch";

    protected FishingOperation selectedFishingOperation;

    protected final LinkedHashMap<FishingOperation, NuitonValidatorResult> validator = new LinkedHashMap<>();

    protected NuitonValidatorResult cruiseValidatorResult;

    protected boolean readyToSynch;

    protected Cruise cruise;

    private Cruise selectedCruise;
    private Object lastSelectedObject;

    public FishingOperation getSelectedFishingOperation() {
        return selectedFishingOperation;
    }

    public void setSelectedFishingOperation(FishingOperation selectedFishingOperation) {
        this.selectedFishingOperation = selectedFishingOperation;
        this.lastSelectedObject= selectedFishingOperation;
        firePropertyChange(PROPERTY_SELECTED_FISHING_OPERATION, null/** always force to propagate the event **/, selectedFishingOperation);
    }

    public Cruise getSelectedCruise() {
        return selectedCruise;
    }

    public void setSelectedCruise(Cruise selectedCruise) {
        this.selectedCruise = selectedCruise;
        this.lastSelectedObject= selectedCruise;
        firePropertyChange(PROPERTY_SELECTED_CRUISE, null /** always force to propagate the event **/, selectedCruise);
    }

    public Object getLastSelectedObject() {
        return lastSelectedObject;
    }

    public Map<FishingOperation, NuitonValidatorResult> getValidator() {
        return new HashMap<>(validator);
    }

    public Set<FishingOperation> getFishingOperations() {
        return ImmutableSet.copyOf(validator.keySet());
    }

    public NuitonValidatorResult getValidatorResult(FishingOperation fishingOperation) {
        return validator.get(fishingOperation);
    }

    public NuitonValidatorResult getCruiseValidatorResult() {
        return cruiseValidatorResult;
    }

    public void setCruiseValidatorResult(NuitonValidatorResult cruiseValidatorResult) {
        this.cruiseValidatorResult = cruiseValidatorResult;
    }

    public void addFishingOperationValidatorResults(Map<FishingOperation, NuitonValidatorResult> validatorResultMap) {
        for (Map.Entry<FishingOperation, NuitonValidatorResult> entry : validatorResultMap.entrySet()) {
            this.validator.put(entry.getKey(), entry.getValue());
        }
    }

    public void addValidatorResult(FishingOperation fishingOperation, NuitonValidatorResult validatorResult) {
        validator.put(fishingOperation, validatorResult);
    }

    public void computeReadyToSynch() {
//        boolean result = Cruises.isDirty(cruise);
        boolean result = true;
//        if (result) {
        if (cruiseValidatorResult != null) {
            if (cruiseValidatorResult.hasErrorMessagess()) {
                result = false;
                if (log.isInfoEnabled()) {
                    log.info("there is some errors in cruise: " + cruiseValidatorResult.getFieldsForError());
                }
            }
            if (cruiseValidatorResult.hasFatalMessages()) {
                result = false;
                if (log.isInfoEnabled()) {
                    log.info("there is some fatal errors in cruise: " + cruiseValidatorResult.getFieldsForFatal());
                }
            }
        }

        for (NuitonValidatorResult nuitonValidatorResult : validator.values()) {
            if (nuitonValidatorResult.hasErrorMessagess()) {
                result = false;
                if (log.isInfoEnabled()) {
                    log.info("there is some errors in fishing operation: " + nuitonValidatorResult.getFieldsForError());
                }
                break;
            }
            if (nuitonValidatorResult.hasFatalMessages()) {
                result = false;
                if (log.isInfoEnabled()) {
                    log.info("there is some fatal errors in fishing operation: " + nuitonValidatorResult.getFieldsForFatal());
                }
                break;
            }
        }
//        } else {
//            if (log.isInfoEnabled()) {
//                log.info("Cruise is not dirty");
//            }
//        }

        if (result) {

            result = Cruises.isDirty(cruise);

        }

        if (log.isInfoEnabled()) {
            log.info("New readyToSynch value: " + result);
        }
        setReadyToSynch(result);
    }

    public boolean isReadyToSynch() {
        return readyToSynch;
    }

    public void setReadyToSynch(boolean readyToSynch) {
        this.readyToSynch = readyToSynch;
        firePropertyChange(PROPERTY_READY_TO_SYNCH, null /*force to fire the changes*/, readyToSynch);
    }

    public void setCruise(Cruise cruise) {
        this.cruise = cruise;
        computeReadyToSynch();
    }

    public Cruise getCruise() {
        return cruise;
    }
}

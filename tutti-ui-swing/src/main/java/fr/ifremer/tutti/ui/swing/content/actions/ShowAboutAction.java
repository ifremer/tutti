package fr.ifremer.tutti.ui.swing.content.actions;

/*
 * #%L
 * Tutti :: UI
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Joiner;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import fr.ifremer.tutti.TuttiConfiguration;
import fr.ifremer.tutti.ui.swing.TuttiUIContext;
import fr.ifremer.tutti.ui.swing.content.MainUIHandler;
import fr.ifremer.tutti.ui.swing.content.db.actions.UpdateDbAction;
import fr.ifremer.tutti.ui.swing.update.actions.UpdateApplicationAction;
import fr.ifremer.tutti.ui.swing.update.actions.UpdateReportAction;
import fr.ifremer.tutti.ui.swing.updater.UpdateModule;
import fr.ifremer.tutti.ui.swing.util.TuttiUIUtil;
import fr.ifremer.tutti.ui.swing.util.actions.LongActionSupport;
import jaxx.runtime.swing.AboutPanel;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.updater.ApplicationInfo;
import org.nuiton.updater.ApplicationUpdater;

import javax.swing.JEditorPane;
import javax.swing.JScrollPane;
import javax.swing.event.HyperlinkEvent;
import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

import static org.nuiton.i18n.I18n.t;

/**
 * To show about panel.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.2
 */
public class ShowAboutAction extends AbstractMainUITuttiAction {

    /** Logger. */
    private static final Log log = LogFactory.getLog(ShowAboutAction.class);

    protected AboutPanel about;

    public ShowAboutAction(MainUIHandler handler) {
        super(handler, false);
    }

    protected boolean canUpdateApplication;

    protected boolean canUpdateData;

    @Override
    public boolean prepareAction() throws Exception {
        boolean doAction = super.prepareAction();

        if (doAction) {
            // check db url is reachable
            TuttiUIContext context = getContext();
            canUpdateApplication = context.checkUpdateApplicationReachable(false);
            canUpdateData = context.checkUpdateDataReachable(false);

        }

        return doAction;
    }

    @Override
    public void postSuccessAction() {
        super.postSuccessAction();

        about.showInDialog(getUI(), true);

        // register on swing session
        getContext().addInSwingSession(about, true);
    }

    @Override
    public void doAction() throws Exception {

        about = null;

        String iconPath = "/icons/allegro_about.png";
        String name = "tutti";
        String licensePath = "META-INF/" + name + "-LICENSE.txt";
        String thirdPartyPath = "META-INF/" + name + "-THIRD-PARTY.txt";

        TuttiConfiguration config = getConfig();

        about = new AboutPanel();
        about.setTitle(t("tutti.about.title"));
        about.setAboutText(t("tutti.about.message"));

        int currentYear = Calendar.getInstance().get(Calendar.YEAR);
        int inceptionYear = config.getInceptionYear();
        String years;
        if (currentYear != inceptionYear) {
            years = inceptionYear + "-" + currentYear;
        } else {
            years = inceptionYear + "";
        }

        about.setBottomText(t("tutti.about.bottomText",
                              config.getOrganizationName(),
                              years,
                              config.getVersion()));
        about.setIconPath(iconPath);
        about.setLicenseFile(licensePath);
        about.setThirdpartyFile(thirdPartyPath);
        about.buildTopPanel();

        //
        // translate tab
        //

        addTranslatePane(config);

        if (canUpdateApplication || canUpdateData) {

            //
            // update tab
            //

            addUpdateTab(config);

        }
        about.init();
    }

    protected void addTranslatePane(TuttiConfiguration config) throws MalformedURLException {

        JScrollPane translatePane = new JScrollPane();
        JEditorPane translateArea = new JEditorPane();
        translateArea.setContentType("text/html");
        translateArea.setEditable(false);
        if (translateArea.getFont() != null) {
            translateArea.setFont(translateArea.getFont().deriveFont((float) 11));
        }

        translateArea.setBorder(null);
        File csvFile = new File(config.getI18nDirectory(), "tutti-i18n.csv");
        String translateText = t("tutti.about.translate.content", csvFile.toURI().toURL());
        translateArea.setText(translateText);
        translatePane.getViewport().add(translateArea);
        translateArea.addHyperlinkListener(e -> {
            if (HyperlinkEvent.EventType.ACTIVATED == e.getEventType()) {
                URL url = e.getURL();
                if (log.isInfoEnabled()) {
                    log.info("edit url: " + url);
                }
                TuttiUIUtil.openLink(url);
            }
        });

        about.getTabs().add(t("tutti.about.translate.title"), translatePane);

    }

    protected void addUpdate(String url, UpdateModule type,
                             Map<String, ApplicationInfo> source,
                             Map<UpdateModule, ApplicationInfo> target) {

        ApplicationInfo info = source.get(type.name().toLowerCase());
        if (info == null) {

            if (log.isWarnEnabled()) {
                log.warn("Can not find information about module " + type + " by update provider " + url);
            }
        } else {

            target.put(type, info);
        }
        
    }

    protected void addUpdateTab(TuttiConfiguration config) {

        // create final update map
        Map<UpdateModule, ApplicationInfo> versions = getModules(config);

        JScrollPane updatePane = new JScrollPane();
        JEditorPane updateArea = new JEditorPane();
        updateArea.setContentType("text/html");
        updateArea.setEditable(false);
        if (updateArea.getFont() != null) {
            updateArea.setFont(updateArea.getFont().deriveFont((float) 11));
        }
        updateArea.setBorder(null);

        List<String> params = Lists.newArrayList();
        for (Map.Entry<UpdateModule, ApplicationInfo> entry : versions.entrySet()) {
            UpdateModule appName = entry.getKey();
            ApplicationInfo info = entry.getValue();
            String oldVersion = info.oldVersion;
            String newVersion = info.newVersion;
            String appLabel = getModuleLabel(appName);

            String message;
            if (newVersion == null) {

                // no update
                message = t("tutti.about.update.app.noup.detail", appLabel, oldVersion);
                params.add(message);
            } else {
                // update exists

                message = t("tutti.about.update.app.up.detail", appLabel, oldVersion, newVersion, appName);
                params.add(message);

            }
            if (log.isInfoEnabled()) {
                log.info(message);
            }
        }

        String urlApplication = config.getUpdateApplicationUrl();
        String urlData = config.getUpdateDataUrl();

        String updateText = t("tutti.about.update.content", urlApplication, urlData, Joiner.on("\n").join(params));
        updateArea.setText(updateText);
        updatePane.getViewport().add(updateArea);
        updateArea.addHyperlinkListener(e -> {
            if (HyperlinkEvent.EventType.ACTIVATED == e.getEventType()) {
                URL url = e.getURL();

                if (url != null) {

                    TuttiUIUtil.openLink(url);
                } else {
                    String appType = e.getDescription();

                    onUpdateLinkClicked(appType);
                }
            }
        });
        about.getTabs().add(t("tutti.about.update.title"), updatePane);
    }

    protected Map<UpdateModule, ApplicationInfo> getModules(TuttiConfiguration config) {

        File current = config.getBasedir();
        String urlApplication = config.getUpdateApplicationUrl();
        String urlData = config.getUpdateDataUrl();

        ApplicationUpdater up = new ApplicationUpdater();

        Map<UpdateModule, ApplicationInfo> versions = Maps.newLinkedHashMap();

        if (canUpdateApplication) {

            // get application updates
            Map<String, ApplicationInfo> applicationVersions = up.getVersions(urlApplication, current);

            addUpdate(urlApplication, UpdateModule.jre, applicationVersions, versions);
            addUpdate(urlApplication, UpdateModule.launcher, applicationVersions, versions);
            addUpdate(urlApplication, UpdateModule.tutti, applicationVersions, versions);
            addUpdate(urlApplication, UpdateModule.i18n, applicationVersions, versions);
            addUpdate(urlApplication, UpdateModule.help, applicationVersions, versions);
            addUpdate(urlApplication, UpdateModule.ichtyometer, applicationVersions, versions);
        }

        if (canUpdateData) {

            // get report updates
            Map<String, ApplicationInfo> reportVersions = up.getVersions(urlData, current);
            addUpdate(urlData, UpdateModule.report, reportVersions, versions);

            // get db updates
            Map<String, ApplicationInfo> dbVersions = up.getVersions(urlData, config.getDataDirectory());
            addUpdate(urlData, UpdateModule.db, dbVersions, versions);
        }

        return versions;

    }

    protected void onUpdateLinkClicked(String appType) {

        if (log.isInfoEnabled()) {
            log.info("Open update url for module: " + appType);
        }

        UpdateModule updateModuleToUpdate = UpdateModule.valueOf(appType);

        LongActionSupport action;

        switch (updateModuleToUpdate) {

            case db: {
                action = getContext().getActionFactory().createLogicAction(getHandler(), UpdateDbAction.class);
                if (!getContext().isDbExist()) {

                    // install db
                    action.setActionDescription(t("tutti.dbManager.action.installDb.tip"));
                }
            }
            break;

            case report: {
                action = getContext().getActionFactory().createLogicAction(getHandler(), UpdateReportAction.class);
            }
            break;


            default: {
                UpdateApplicationAction logicAction = getContext().getActionFactory().createLogicAction(getHandler(), UpdateApplicationAction.class);
                logicAction.setModulesToUpdate(updateModuleToUpdate);
                String label = getModuleLabel(updateModuleToUpdate);
                logicAction.setActionDescription(t("tutti.main.action.updateSpecificApplication.tip", label));
                action = logicAction;
            }

        }

        // close this dialog
        getActionEngine().runAction(about.getClose());

        // do update
        getActionEngine().runAction(action);

    }

    protected String getModuleLabel(UpdateModule moduleName) {
        String i18nKey = "tutti.update." + moduleName.name().toLowerCase();
        return t(i18nKey);
    }

}

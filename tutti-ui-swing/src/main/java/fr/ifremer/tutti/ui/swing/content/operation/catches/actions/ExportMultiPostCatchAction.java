package fr.ifremer.tutti.ui.swing.content.operation.catches.actions;

/*
 * #%L
 * Tutti :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.entities.data.FishingOperation;
import fr.ifremer.tutti.service.catches.multipost.MultiPostExportService;
import fr.ifremer.tutti.ui.swing.content.operation.catches.EditCatchesUI;
import fr.ifremer.tutti.ui.swing.content.operation.catches.EditCatchesUIHandler;
import fr.ifremer.tutti.ui.swing.content.operation.catches.EditCatchesUIModel;

import java.io.File;

import static org.nuiton.i18n.I18n.t;

/**
 * Created on 11/15/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.9
 */
public class ExportMultiPostCatchAction extends ExportMultiPostActionSupport<EditCatchesUIModel, EditCatchesUI, EditCatchesUIHandler> {

    public ExportMultiPostCatchAction(EditCatchesUIHandler handler) {
        super(handler);
    }

    @Override
    protected EditCatchesUI getEditCatchUI() {
        return getUI();
    }

    @Override
    protected String getFileExtension() {
        return "tuttiCatch";
    }

    @Override
    protected String getFileExtensionDescription() {
        return t("tutti.common.file.tuttiCatch");
    }

    @Override
    protected String getFileChooserTitle() {
        return t("tutti.editCatchBatch.action.exportMultiPost.destinationFile.title");
    }

    @Override
    protected String getFileChooserButton() {
        return t("tutti.editCatchBatch.action.exportMultiPost.destinationFile.button");
    }

    @Override
    protected String getSuccessMessage(File file) {
        return t("tutti.editCatchBatch.action.exportMultiPost.success", file);
    }

    @Override
    protected void doExport(MultiPostExportService multiPostImportExportService, File file, FishingOperation fishingOperation) {

        multiPostImportExportService.exportCatchBatch(file, fishingOperation);

    }

}
/*
 * #%L
 * Tutti :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

BeanFilterableComboBox {
  showReset: true;
  bean: {model};
}

JToolBar {
  borderPainted: false;
  floatable: false;
  opaque: true;
}

#genericFormatExportTopPanel {
  _help: {"tutti.genericFormatExport.help"};
}

#exportOptions {
  border: {BorderFactory.createTitledBorder(t("tutti.genericFormatExport.exportOptions.legend"))};
}

#dataSelectionPane {
  border: {BorderFactory.createTitledBorder(t("tutti.genericFormatExport.selectData.legend"))};
  columnHeaderView: {dataSelectionTreeHeader};
}

#dataSelectionTree {
  model: {treeModel};
  rootVisible: false;
  scrollsOnExpand: true;
  editable: false;
  toggleClickCount: 2;
  cellRenderer: {new ExportDataSelectTreeCellRenderer()};
}

#programLabel {
  text: "tutti.genericFormatExport.field.program";
  labelFor: {programComboBox};
  toolTipText: "tutti.genericFormatExport.field.program.tip";
  _help: {"tutti.genericFormatExport.field.program.help"};
}

#programComboBox {
  property: program;
  selectedItem: {model.getProgram()};
  _validatorLabel: {t("tutti.genericFormatExport.field.program")};
  _help: {"tutti.genericFormatExport.field.program.help"};
}

#exportAttachmentsCheckBox {
  text: "tutti.genericFormatExport.field.exportAttachments";
  selected: {model.isExportAttachments()};
  toolTipText: "tutti.genericFormatExport.field.exportAttachments.tip";
  _help: {"tutti.genericFormatExport.field.exportAttachments.help"};
}

#exportSpeciesCheckBox {
  text: "tutti.genericFormatExport.field.exportSpecies";
  selected: {model.isExportSpecies()};
  toolTipText: "tutti.genericFormatExport.field.exportSpecies.tip";
  _help: {"tutti.genericFormatExport.field.exportSpecies.help"};
}

#exportBenthosCheckBox {
  text: "tutti.genericFormatExport.field.exportBenthos";
  selected: {model.isExportBenthos()};
  toolTipText: "tutti.genericFormatExport.field.exportBenthos.tip";
  _help: {"tutti.genericFormatExport.field.exportBenthos.help"};
}

#exportMarineLitterCheckBox {
  text: "tutti.genericFormatExport.field.exportMarineLitter";
  selected: {model.isExportMarineLitter()};
  toolTipText: "tutti.genericFormatExport.field.exportMarineLitter.tip";
  _help: {"tutti.genericFormatExport.field.exportMarineLitter.help"};
}

#exportAccidentalCatchCheckBox {
  text: "tutti.genericFormatExport.field.exportAccidentalCatch";
  selected: {model.isExportAccidentalCatch()};
  toolTipText: "tutti.genericFormatExport.field.exportAccidentalCatch.tip";
  _help: {"tutti.genericFormatExport.field.exportAccidentalCatch.help"};
}

#exportIndividualObservationCheckBox {
  text: "tutti.genericFormatExport.field.exportIndividualObservation";
  selected: {model.isExportIndividualObservation()};
  toolTipText: "tutti.genericFormatExport.field.exportIndividualObservation.tip";
  _help: {"tutti.genericFormatExport.field.exportIndividualObservation.help"};
}

#exportButton {
  actionIcon: export;
  text: "tutti.genericFormatExport.action.export";
  toolTipText: "tutti.genericFormatExport.action.export.tip";
  i18nMnemonic: "tutti.genericFormatExport.action.export.mnemonic";
  _applicationAction: {fr.ifremer.tutti.ui.swing.content.genericformat.actions.GenericFormatExportAction.class};
  enabled: {model.isCanExport()};
  _help: {"tutti.genericFormatExport.action.export.help"};
}

#closeButton {
  actionIcon: cancel;
  text: "tutti.genericFormatExport.action.closeGenericFormatExport";
  toolTipText: "tutti.genericFormatExport.action.closeGenericFormatExport.tip";
  i18nMnemonic: "tutti.genericFormatExport.action.closeGenericFormatExport.mnemonic";
  _applicationAction: {fr.ifremer.tutti.ui.swing.content.actions.OpenHomeScreenAction.class};
  _help: {"tutti.genericFormatExport.action.closeGenericFormatExport.help"};
}

#selectAllButton {
  actionIcon: select;
  toolTipText: "tutti.genericFormatExport.action.selectAll.tip";
  enabled: {model.getProgram() != null};
  _simpleAction: {fr.ifremer.tutti.ui.swing.content.genericformat.actions.ExportSelectAllDataAction.class};
  _help: {"tutti.genericFormatExport.action.selectAll.help"};
}

#unselectAllButton {
  actionIcon: unselect;
  toolTipText: "tutti.genericFormatExport.action.unselectAll.tip";
  enabled: {model.getProgram() != null};
  _simpleAction: {fr.ifremer.tutti.ui.swing.content.genericformat.actions.ExportUnselectAllDataAction.class};
  _help: {"tutti.genericFormatExport.action.unselectAll.help"};
}

#foldAllButton {
  actionIcon: collapse;
  toolTipText: "tutti.genericFormatExport.action.foldAll.tip";
  enabled: {model.getProgram() != null};
  _simpleAction: {fr.ifremer.tutti.ui.swing.content.genericformat.actions.ExportFoldAllDataAction.class};
  _help: {"tutti.genericFormatExport.action.foldAll.help"};
}

#unfoldAllButton {
  actionIcon: expand;
  toolTipText: "tutti.genericFormatExport.action.unfoldAll.tip";
  enabled: {model.getProgram() != null};
  _simpleAction: {fr.ifremer.tutti.ui.swing.content.genericformat.actions.ExportUnfoldAllDataAction.class};
  _help: {"tutti.genericFormatExport.action.unfoldAll.help"};
}


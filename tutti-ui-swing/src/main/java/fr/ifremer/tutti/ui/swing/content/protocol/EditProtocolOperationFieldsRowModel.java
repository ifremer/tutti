package fr.ifremer.tutti.ui.swing.content.protocol;

/*
 * #%L
 * Tutti :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.entities.protocol.OperationFieldMappingRow;
import fr.ifremer.tutti.persistence.entities.protocol.OperationFieldMappingRowBean;
import fr.ifremer.tutti.ui.swing.util.AbstractTuttiBeanUIModel;
import org.nuiton.util.beans.Binder;
import org.nuiton.util.beans.BinderFactory;

/**
 * @author Kevin Morin (Code Lutin)
 * @since 3.10
 */
public class EditProtocolOperationFieldsRowModel extends AbstractTuttiBeanUIModel<OperationFieldMappingRow, EditProtocolOperationFieldsRowModel> {

    public static final String PROPERTY_FIELD = "field";

    public static final String PROPERTY_IMPORT_COLUMN = "importColumn";

    private static final long serialVersionUID = 1L;

    protected String field;

    protected String importColumn;

    protected static final Binder<OperationFieldMappingRow, EditProtocolOperationFieldsRowModel> fromBeanBinder =
            BinderFactory.newBinder(OperationFieldMappingRow.class,
                                    EditProtocolOperationFieldsRowModel.class);

    protected static final Binder<EditProtocolOperationFieldsRowModel, OperationFieldMappingRow> toBeanBinder =
            BinderFactory.newBinder(EditProtocolOperationFieldsRowModel.class,
                                    OperationFieldMappingRow.class);

    public EditProtocolOperationFieldsRowModel() {
        super(fromBeanBinder, toBeanBinder);
    }

    public String getField() {
        return field;
    }

    public void setField(String field) {
        Object oldValue = getField();
        this.field = field;
        firePropertyChanged(PROPERTY_FIELD, oldValue, field);
    }

    public String getImportColumn() {
        return importColumn;
    }

    public void setImportColumn(String importColumn) {
        Object oldValue = getImportColumn();
        this.importColumn = importColumn;
        firePropertyChanged(PROPERTY_IMPORT_COLUMN, oldValue, importColumn);
    }

    @Override
    protected OperationFieldMappingRow newEntity() {
        return new OperationFieldMappingRowBean();
    }
}

package fr.ifremer.tutti.ui.swing.content.actions;

/*
 * #%L
 * Tutti :: UI
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.ui.swing.TuttiUIContext;
import fr.ifremer.tutti.ui.swing.content.MainUIHandler;
import fr.ifremer.tutti.ui.swing.content.db.actions.ImportDbAction;
import fr.ifremer.tutti.ui.swing.content.db.actions.InstallDbAction;
import fr.ifremer.tutti.ui.swing.content.db.actions.OpenDbAction;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.jaxx.application.ApplicationIOUtil;

import java.io.File;

import static org.nuiton.i18n.I18n.t;

/**
 * Start action.
 *
 * If there is a start action file, then load it and run inside actions, otherwise
 * just starts normal ui action (open db if exists, or go to manage db screen).
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.4
 */
public class StartAction extends AbstractMainUITuttiAction {

    /** Logger. */
    private static final Log log = LogFactory.getLog(StartAction.class);

    protected AbstractMainUITuttiAction delegateAction;

    public StartAction(MainUIHandler handler) {
        super(handler, true);
    }

    @Override
    public boolean prepareAction() throws Exception {

        File actionfile = getConfig().getStartActionFile();
        boolean doAction = actionfile.exists();

        if (doAction) {

            try {
                // get action to execute
                String content = ApplicationIOUtil.readContent(actionfile, t("tutti.error.read.startActionFile", actionfile));
                content = content.trim();

                if (InstallDbAction.class.getName().equals(content)) {

                    // install db
                    delegateAction = getContext().getActionFactory().createLogicAction(handler, InstallDbAction.class);

                    if (log.isInfoEnabled()) {
                        log.info("Found install db action");
                    }
                    deleteOldDatabaseDirectory();

                } else if (content.startsWith(ImportDbAction.class.getName())) {

                    // import db
                    ImportDbAction action = getContext().getActionFactory().createLogicAction(handler, ImportDbAction.class);
                    File importFile = new File(content.substring(ImportDbAction.class.getName().length() + 1));
                    action.setImportFile(importFile);
                    delegateAction = action;
                    if (log.isInfoEnabled()) {
                        log.info("Found import db action (with file " + importFile + ")");
                    }
                    deleteOldDatabaseDirectory();
                } else {
                    if (log.isWarnEnabled()) {
                        log.warn("Unknown start action: " + content);
                    }
                    doAction = false;
                }

            } finally {

                // delete start action file
                ApplicationIOUtil.deleteFile(
                        actionfile,
                        t("tutti.error.delete.startActionFile", actionfile));
            }
        }

        if (!doAction) {

            // no start action, normal start

            TuttiUIContext context = getContext();

            if (context.isDbLoaded()) {

                // db already opened (happens when reloading ui)
                // just go to select cruise screen
                OpenHomeScreenAction action = getContext().getActionFactory().createLogicAction(handler, OpenHomeScreenAction.class);
                action.setSkipCheckCurrentScreen(true);
                action.setActionDescription(getUI().getMenuActionSelectCruise().getToolTipText());
                delegateAction = action;

            } else {

                if (context.isDbExist()) {

                    // open tutti db (using a fake button to have simple api)
                    OpenDbAction action = getContext().getActionFactory().createLogicAction(handler, OpenDbAction.class);
                    action.setSkipCheckCurrentScreen(true);
                    action.setUpdateReferentiel(true);
                    delegateAction = action;

                } else {

                    // clean db context
                    context.clearDbContext();

                    // go to manage db screen (to install db)
                    OpenDbScreenAction action = getContext().getActionFactory().createLogicAction(handler, OpenDbScreenAction.class);
                    action.setSkipCheckCurrentScreen(true);
                    delegateAction = action;
                }
            }
        }

        setActionDescription(delegateAction.getActionDescription());
        doAction = delegateAction.prepareAction();
        return doAction;
    }

    @Override
    public void doAction() throws Exception {

        getActionEngine().runInternalAction(delegateAction);
    }

    @Override
    protected void releaseAction() {
        delegateAction = null;
        super.releaseAction();
    }

    protected void deleteOldDatabaseDirectory() {
        File dbDirectory = getConfig().getDbDirectory();
        if (dbDirectory.exists()) {

            // delete it before install
            if (log.isInfoEnabled()) {
                log.info("Delete previous database directory: " + dbDirectory);
            }
            ApplicationIOUtil.deleteDirectory(dbDirectory, "Could not delete old db directory");
        }
    }
}

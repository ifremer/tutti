/*
 * #%L
 * Tutti :: UI
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

BeanFilterableComboBox {
  showReset: true;
}

#newRowKey {
  property: selectedCaracteristic;
  enabled: {!newRowKey.isEmpty()};
}

#addRow {
  actionIcon: add;
  enabled: {newRowKey.getSelectedItem() != null};
  _simpleAction: {fr.ifremer.tutti.ui.swing.content.operation.fishing.actions.VesselUseFeatureAddRowAction.class};
}

#vesselUseFeatureTable {
  selectionMode: {ListSelectionModel.SINGLE_SELECTION};
  selectionBackground: {null};
  selectionForeground: {Color.BLACK};
  sortable: false;
}

#removeCaracteristicMenu {
  actionIcon: batch-delete;
  text: "tutti.vesselUseFeatureTable.action.removeCaracteristic";
  toolTipText: "tutti.vesselUseFeatureTable.action.removeCaracteristic.tip";
  i18nMnemonic: "tutti.vesselUseFeatureTable.action.removeCaracteristic.mnemonic";
  enabled: {model.isRemoveCaracteristicEnabled()};
  _simpleAction: {fr.ifremer.tutti.ui.swing.content.operation.fishing.actions.VesselUseFeatureRemoveRowAction.class};
}

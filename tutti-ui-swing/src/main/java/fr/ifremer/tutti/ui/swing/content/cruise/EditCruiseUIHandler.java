package fr.ifremer.tutti.ui.swing.content.cruise;

/*
 * #%L
 * Tutti :: UI
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Predicate;
import com.google.common.collect.Lists;
import fr.ifremer.tutti.persistence.entities.data.Attachment;
import fr.ifremer.tutti.persistence.entities.data.Cruise;
import fr.ifremer.tutti.persistence.entities.data.Program;
import fr.ifremer.tutti.persistence.entities.referential.Gear;
import fr.ifremer.tutti.persistence.entities.referential.GearWithOriginalRankOrder;
import fr.ifremer.tutti.persistence.entities.referential.GearWithOriginalRankOrders;
import fr.ifremer.tutti.persistence.entities.referential.Gears;
import fr.ifremer.tutti.persistence.entities.referential.Person;
import fr.ifremer.tutti.persistence.entities.referential.Vessel;
import fr.ifremer.tutti.service.DecoratorService;
import fr.ifremer.tutti.service.PersistenceService;
import fr.ifremer.tutti.ui.swing.util.AbstractTuttiUIHandler;
import jaxx.runtime.JAXXUtil;
import jaxx.runtime.context.JAXXContextEntryDef;
import jaxx.runtime.swing.editor.bean.BeanDoubleListModel;
import jaxx.runtime.validator.swing.SwingValidator;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.jaxx.application.swing.util.CloseableUI;

import javax.swing.JComponent;
import javax.swing.JList;
import javax.swing.JPopupMenu;
import javax.swing.JSeparator;
import javax.swing.SwingUtilities;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.List;
import java.util.Set;

import static org.nuiton.i18n.I18n.t;

/**
 * Handler of UI {@link EditCruiseUI}.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 0.1
 */
public class EditCruiseUIHandler extends AbstractTuttiUIHandler<EditCruiseUIModel, EditCruiseUI> implements CloseableUI {

    /** Logger. */
    private static final Log log = LogFactory.getLog(EditCruiseUIHandler.class);

    public static final String CRUISE_CARD = "cruise";

    public static final String GEAR_CARACTERISTICS_CARD = "gearCaracteristics";

    public static String getTitle(boolean exist) {

        String result;
        if (exist) {
            result = t("tutti.editCruise.title.edit.cruise");
        } else {
            result = t("tutti.editCruise.title.create.cruise");
        }
        return result;
    }

    /**
     * The gear selected popup grab from the double list.
     *
     * @since 2.6
     */
    protected JPopupMenu gearPopupMenu;

    @Override
    public void beforeInit(EditCruiseUI ui) {

        super.beforeInit(ui);

        getDataContext().resetValidationDataContext();

        EditCruiseUIModel model = new EditCruiseUIModel();
        model.setSynchronizationStatus("DIRTY");
        model.setValidationContext(getContext().getValidationContext());

        if (getContext().isCruiseFilled()) {

            // load existing cruise
            Cruise cruise = getDataContext().getCruise();

            model.fromEntity(cruise);

            if (!model.isGearEmpty()) {

                // load gear caracteristics + set originalRankOrder
                for (GearWithOriginalRankOrder gear : model.getGear()) {

                    // original rank order is the incoming rank order
                    gear.setOriginalRankOrder(gear.getRankOrder());
                }
            }

            // load attachments
            List<Attachment> attachments = getPersistenceService().getAllAttachments(
                    model.getObjectType(), model.getObjectId());
            model.addAllAttachment(attachments);

            if (log.isDebugEnabled()) {
                log.debug("Will edit cruise: " + cruise.getId());
            }
        } else {

            // create new cruise
            Program program = getDataContext().getProgram();
            model.setProgram(program);
            model.setMultirigNumber(1);


            if (log.isDebugEnabled()) {
                log.debug("Will create a new cruise from program: " + program);
            }
        }
        listModelIsModify(model);
        this.ui.setContextValue(model);
    }

    @Override
    public void afterInit(EditCruiseUI ui) {

        initUI(this.ui);

        // init date by hand (otherwise databinding in beforeInit method has already set the
        // date in time editor, but the editor is still not init, so won't work)
        ui.getBeginTimeField().setDate(getModel().getBeginDate());
        ui.getEndTimeField().setDate(getModel().getEndDate());

        PersistenceService persistenceService = getPersistenceService();

        List<Person> users = persistenceService.getAllPerson();

        EditCruiseUIModel model = getModel();

        List<Vessel> scientificVesselList = getDataContext().getScientificVessels();
        List<Vessel> fishingVesselList = getDataContext().getFishingVessels();
        List<Vessel> allVesselList = Lists.newArrayList();
        allVesselList.addAll(scientificVesselList);
        allVesselList.addAll(fishingVesselList);

        List<Gear> scientificGearList = getDataContext().getScientificGears();
        List<Gear> fishingGearList = getDataContext().getFishingGears();
        List<Gear> allGearList = Lists.newArrayList();
        allGearList.addAll(scientificGearList);
        allGearList.addAll(fishingGearList);

        // Change vessel list and gear list when vessel type changes
        model.addPropertyChangeListener(EditCruiseUIModel.PROPERTY_VESSEL_TYPE, evt -> {

            VesselTypeEnum vesselType = (VesselTypeEnum) evt.getNewValue();

            Predicate<Gear> gearPredicate = null;

            List<Vessel> allVesselList1 = Lists.newArrayList();


            switch (vesselType) {
                case FISHING:
                    gearPredicate = Gears.IS_FISHING_GEAR;
                    allVesselList1.addAll(getDataContext().getFishingVessels());
                    break;

                case SCIENTIFIC:
                    gearPredicate = Gears.IS_SCIENTIFIC_GEAR;

                    allVesselList1.addAll(getDataContext().getScientificVessels());
                    break;
                default: {
                    allVesselList1.addAll(getDataContext().getScientificVessels());
                    allVesselList1.addAll(getDataContext().getFishingVessels());
                }
            }

            EditCruiseUIHandler.this.ui.getGearList().getHandler().clearFilters();
            EditCruiseUIHandler.this.ui.getVesselComboBox().setData(null);
            EditCruiseUIHandler.this.ui.getVesselComboBox().setData(allVesselList1);

            if (gearPredicate != null) {
                EditCruiseUIHandler.this.ui.getGearList().getHandler().addFilter(gearPredicate);
            }

        });

        initBeanFilterableComboBox(this.ui.getProgramComboBox(),
                                   Lists.newArrayList(persistenceService.getAllProgram()),
                                   model.getProgram(),
                                   DecoratorService.ONLY_NAME);

        initBeanFilterableComboBox(this.ui.getDepartureLocationComboBox(),
                                   Lists.newArrayList(persistenceService.getAllHarbour()),
                                   model.getDepartureLocation());

        initBeanFilterableComboBox(this.ui.getReturnLocationComboBox(),
                                   Lists.newArrayList(persistenceService.getAllHarbour()),
                                   model.getReturnLocation());

        initBeanFilterableComboBox(this.ui.getVesselComboBox(),
                                   allVesselList,
                                   model.getVessel());

        this.ui.getGearList().setModel(new BeanDoubleListModel<Gear>() {

            private static final long serialVersionUID = 1L;

            private boolean valueAdjusting;

            @Override
            public void moveUpSelected(Gear item) {
                super.moveUpSelected(item);
                rebuildRankOrder();
            }

            @Override
            public void moveDownSelected(Gear item) {
                super.moveDownSelected(item);
                rebuildRankOrder();
            }

            @Override
            public void setSelected(List<Gear> selected) {
                valueAdjusting = true;
                try {
                    super.setSelected(selected);
                } finally {
                    valueAdjusting = false;
                }
                rebuildRankOrder();
            }

            @Override
            public void addToSelected(Gear item) {

                // always use a copy (with original rank order)
                GearWithOriginalRankOrder target =
                        GearWithOriginalRankOrders.newGearWithOriginalRankOrder(item);

                Short originalRankOrder = null;

                if (item instanceof GearWithOriginalRankOrder) {
                    // take the incoming original rank order
                    originalRankOrder = ((GearWithOriginalRankOrder) item).getOriginalRankOrder();
                }
                if (originalRankOrder == null) {
                    originalRankOrder = 0;
                }
                target.setOriginalRankOrder(originalRankOrder);
                super.addToSelected(target);
                rebuildRankOrder();
            }

            @Override
            public void addToSelected(List<Gear> items) {
                valueAdjusting = true;
                try {
                    for (Gear item : items) {
                        addToSelected(item);
                    }
                } finally {
                    valueAdjusting = false;
                }
                rebuildRankOrder();
            }

            @Override
            public void removeFromSelected(Gear item) {
                super.removeFromSelected(item);
                rebuildRankOrder();
            }

            @Override
            public void removeFromSelected(List<Gear> items) {
                valueAdjusting = true;
                try {
                    for (Gear item : items) {
                        removeFromSelected(item);
                    }
                } finally {
                    valueAdjusting = false;
                }
                rebuildRankOrder();
            }

            protected void rebuildRankOrder() {
                if (!valueAdjusting) {
                    int index = 1;
                    for (Gear gear : getSelected()) {
                        gear.setRankOrder((short) (index++));
                    }
                }
                selectedModel.refresh();
            }
        });
        initBeanList(this.ui.getGearList(),
                     allGearList,
                     (List) model.getGear(),
                     getDecorator(Gear.class, DecoratorService.GEAR_WITH_RANK_ORDER));

        // add more actions on selected gear popup
        gearPopupMenu = this.ui.getGearList().getSelectedList().getComponentPopupMenu();
        this.ui.getGearList().getSelectedList().setComponentPopupMenu(null);

        gearPopupMenu.add(new JSeparator(), 0);
        gearPopupMenu.add(this.ui.getViewGearCaracteristicsItem(), 0);
        gearPopupMenu.add(this.ui.getEditGearCaracteristicsItem(), 0);

        initBeanList(this.ui.getHeadOfMissionList(),
                     users,
                     model.getHeadOfMission());

        initBeanList(this.ui.getHeadOfSortRoomList(),
                     users,
                     model.getHeadOfSortRoom());

        this.ui.getGearList().getSelectedList().addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {

                if (SwingUtilities.isRightMouseButton(e)) {
                    JList jList = (JList) e.getSource();
                    Point point = e.getPoint();
                    int selectedIndex = jList.locationToIndex(point);
                    Rectangle rect = jList.getCellBounds(selectedIndex, selectedIndex);
                    if (rect.contains(point)) {
                        jList.setSelectedIndex(selectedIndex); //select the item
                    } else {
                        jList.clearSelection();
                    }

                    GearWithOriginalRankOrder gear = (GearWithOriginalRankOrder) jList.getSelectedValue();
                    updateGearActionsAndShowPoup(gear, jList, e.getX(), e.getY());

                }
            }
        });

        this.ui.getGearList().getSelectedList().addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
                if (e.getKeyCode() == KeyEvent.VK_CONTEXT_MENU) {

                    JList source = (JList) e.getSource();

                    // get the lowest selected row
                    int[] selectedRows = source.getSelectedIndices();
                    int lowestRow = -1;
                    for (int row : selectedRows) {
                        lowestRow = Math.max(lowestRow, row);
                    }
                    Rectangle r = source.getCellBounds(lowestRow, lowestRow);

                    // get the point in the middle lower of the cell
                    Point p = new Point(r.x + r.width / 2, r.y + r.height);

                    Object[] gears = source.getSelectedValues();
                    GearWithOriginalRankOrder gear = gears != null && gears.length == 1 ? (GearWithOriginalRankOrder) gears[0] : null;
                    updateGearActionsAndShowPoup(gear, source, p.x, p.y);
                }
            }
        });

        SwingValidator validator = this.ui.getValidator();
        changeValidatorContext(model.getValidationContext(), ui.getValidator());

        listenValidatorValid(validator, model);

        model.setVesselType(VesselTypeEnum.SCIENTIFIC);

        // if new fishingOperation can already cancel his creation
        model.setModify(model.isCreate());

        registerValidators(validator);

        //FIXME Binding does not work
        JAXXUtil.applyDataBinding(ui, EditCruiseUI.BINDING_RESET_BUTTON_ENABLED);
    }

    @Override
    protected JComponent getComponentToFocus() {
        return getUI().getProgramComboBox();
    }

    @Override
    protected Set<String> getPropertiesToIgnore() {
        Set<String> result = super.getPropertiesToIgnore();
        result.add(EditCruiseUIModel.PROPERTY_ATTACHMENT);
        result.add(EditCruiseUIModel.PROPERTY_VESSEL_TYPE);
        result.add(EditCruiseUIModel.PROPERTY_VESSEL_TYPE_ALL);
        result.add(EditCruiseUIModel.PROPERTY_VESSEL_TYPE_SCIENTIFIC);
        result.add(EditCruiseUIModel.PROPERTY_VESSEL_TYPE_FISHING);
        result.add(EditCruiseUIModel.PROPERTY_CAN_EDIT_GEAR_CARACTERISTIC);
        result.add(EditCruiseUIModel.PROPERTY_CAN_SHOW_GEAR_CARACTERISTIC);
        return result;
    }

    @Override
    public void onCloseUI() {
        if (log.isDebugEnabled()) {
            log.debug("closing: " + ui);
        }
        clearValidators();

        ui.getAttachmentsButton().onCloseUI();
    }

    @Override
    public boolean quitUI() {
        return quitScreen(
                getModel().isValid(),
                getModel().isModify(),
                t("tutti.editCruise.askCancelEditBeforeLeaving.cancelSaveCruise"),
                t("tutti.editCruise.askSaveBeforeLeaving.saveCruise"),
                ui.getSaveButton().getAction()
        );
    }

    @Override
    public SwingValidator<EditCruiseUIModel> getValidator() {
        return ui.getValidator();
    }

//    public void generateCampaignName() {
//
//        EditCruiseUIModel model = getModel();
//        String name = model.getGeneratedCampaignName();
//        model.setName(name);
//    }

    public static final JAXXContextEntryDef<GearWithOriginalRankOrder> GEAR_EDIT_CONTEXT =
            JAXXUtil.newContextEntryDef("editGear", GearWithOriginalRankOrder.class);

    protected void updateGearActionsAndShowPoup(GearWithOriginalRankOrder gear,
                                                JComponent source,
                                                int x,
                                                int y) {
        boolean editMenuEnabled = false;
        boolean viewMenuEnabled = false;
        if (gear != null) {
            editMenuEnabled = !getModel().isCreate() &&
                              !getModel().isModify() &&
                              (!gear.isScientificGear() ||
                               Gears.isTemporary(gear));
            viewMenuEnabled = true;
            GEAR_EDIT_CONTEXT.setContextValue(ui, gear);
        }
        getModel().setCanEditGearCatacteristic(editMenuEnabled);
        getModel().setCanShowGearCatacteristic(viewMenuEnabled);

        gearPopupMenu.show(source, x, y); //and show the menu
    }

    public void reloadCruise(Cruise cruise) {

        EditCruiseUIModel model = getModel();

        model.fromEntity(cruise);

        if (!model.isGearEmpty()) {

            // load gear caracteristics + set originalRankOrder
            for (GearWithOriginalRankOrder gear : model.getGear()) {

                // original rank order is the incoming rank order
                gear.setOriginalRankOrder(gear.getRankOrder());
            }
        }

        // load attachments
        List<Attachment> attachments = getPersistenceService().getAllAttachments(
                model.getObjectType(), model.getObjectId());
        model.addAllAttachment(attachments);

        ui.getHeadOfMissionList().getModel().setSelected(model.getHeadOfMission());
        ui.getHeadOfSortRoomList().getModel().setSelected(model.getHeadOfSortRoom());
        ui.getGearList().getModel().setSelected((List) model.getGear());

        getModel().setModify(false);

    }
}

package fr.ifremer.tutti.ui.swing.content.db;

/*
 * #%L
 * Tutti :: UI
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Maps;
import fr.ifremer.tutti.TuttiConfiguration;
import fr.ifremer.tutti.ui.swing.TuttiUIContext;
import fr.ifremer.tutti.ui.swing.update.Updates;
import fr.ifremer.tutti.ui.swing.util.AbstractTuttiUIHandler;
import jaxx.runtime.validator.swing.SwingValidator;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.updater.ApplicationInfo;
import org.nuiton.version.Version;

import javax.swing.JComponent;
import javax.swing.SwingUtilities;
import java.util.Map;

import static org.nuiton.i18n.I18n.n;
import static org.nuiton.i18n.I18n.t;

/**
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.0
 */
public class DbManagerUIHandler extends AbstractTuttiUIHandler<TuttiUIContext, DbManagerUI> {

    /** Logger. */
    private static final Log log = LogFactory.getLog(DbManagerUIHandler.class);

    public void updateMessage() {

        boolean dbExist = getContext().isDbExist();
        boolean dbLoaded = getContext().isDbLoaded();


        if (log.isInfoEnabled()) {
            log.info("Rebuild information text... (dbExist?" + dbExist + "/ dbLoaded?" + dbLoaded + ")");
        }

        String message;

        if (dbExist) {

            TuttiConfiguration config = getConfig();

            String jdbcUrl = config.getJdbcUrl();

            Map<String, String> caracteristics = Maps.newLinkedHashMap();

            String title;

            caracteristics.put(t("tutti.dbManager.caracteristic.url"), jdbcUrl);

            if (dbLoaded) {

                Version dbVersion = getContext().getPersistenceService().getSchemaVersion();

                // db loaded
                title = n("tutti.dbManager.info.db.loaded");

                caracteristics.put(t("tutti.dbManager.caracteristic.schemaVersion"), dbVersion.toString());

            } else {

                // no db loaded
                title = n("tutti.dbManager.info.no.db.loaded");
            }

            // get referential version

            if (getContext().checkUpdateDataReachable(false)) {
//                String urlDb = config.getUpdateDataUrl();

                ApplicationInfo updateDbVersion = Updates.getDatabaseUpdateVersion(getConfig());

                String currentReferentialVersion = updateDbVersion.oldVersion;
                String newReferentialVersion = updateDbVersion.newVersion;
                caracteristics.put(t("tutti.dbManager.caracteristic.referentialVersion"), currentReferentialVersion);
                if (newReferentialVersion != null) {
                    caracteristics.put(t("tutti.dbManager.caracteristic.lastReferentialVersion"), newReferentialVersion);
                }
            }

            StringBuilder caracteristicsToString = new StringBuilder("<ul>");
            for (Map.Entry<String, String> entry : caracteristics.entrySet()) {
                caracteristicsToString.append("<li>");
                caracteristicsToString.append(entry.getKey());
                caracteristicsToString.append(" : <strong>");
                caracteristicsToString.append(entry.getValue());
                caracteristicsToString.append("</strong></li>");
            }
            caracteristicsToString.append("</ul>");

            message = t(title, caracteristicsToString);

        } else {

            // db does not exist
            message = t("tutti.dbManager.info.no.db.exist");
        }
        String result = "<html><body>" + message + "</body></html>";
        getUI().getInformationArea().setText(result);
    }

    @Override
    public void beforeInit(DbManagerUI ui) {
        super.beforeInit(ui);
        ui.setContextValue(getContext());
    }

    @Override
    public void afterInit(DbManagerUI ui) {

        initUI(ui);

        getModel().addPropertyChangeListener(TuttiUIContext.PROPERTY_DB_EXIST, evt -> {

            Boolean dbExist = (Boolean) evt.getNewValue();
            String mnemonic;
            if (dbExist) {
                mnemonic = t("tutti.dbManager.action.reinstallDb.mnemonic");
            } else {
                mnemonic = t("tutti.dbManager.action.installDb.mnemonic");
            }
            if (StringUtils.isNotBlank(mnemonic)) {
                DbManagerUIHandler.this.ui.getInstallOrReinstallDbButton().setMnemonic(mnemonic.charAt(0));
            }

        });

        updateMessage();

        SwingUtilities.invokeLater(
                () -> getContext().getMainUI().getBody().repaint()
        );
    }

    @Override
    protected JComponent getComponentToFocus() {
        return getUI().getUpgradeDbButton();
    }

    @Override
    public void onCloseUI() {
    }

    @Override
    public SwingValidator<TuttiUIContext> getValidator() {
        return null;
    }

    public String getInstallButtonText(boolean dbExist) {
        String result;
        if (dbExist) {
            result = t("tutti.dbManager.action.reinstallDb");
        } else {
            result = t("tutti.dbManager.action.installDb");
        }
        return result;
    }

    public String getInstallButtonTip(boolean dbExist) {
        String result;
        if (dbExist) {
            result = t("tutti.dbManager.action.reinstallDb.tip");
        } else {
            result = t("tutti.dbManager.action.installDb.tip");
        }
        return result;
    }
}

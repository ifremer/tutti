package fr.ifremer.tutti.ui.swing.content.protocol.zones.tree.node;

/*
 * #%L
 * Tutti :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2016 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.entities.protocol.SubStrata;
import fr.ifremer.tutti.persistence.entities.protocol.SubStratas;

/**
 * Pour définir une sous-strate contenue dans une strate.
 *
 * Le parent d'un tel nœud est toujours une strate.
 *
 * À noter que ce type de nœud est toujours une feuille.
 *
 * @author Kevin Morin (Code Lutin)
 * @author Tony Chemit - chemit@codelutin.com
 * @since 4.5
 */
public class SubStrataNode extends ZoneEditorNodeSupport {

    public SubStrataNode(String id, String label) {
        super(id, label, false);
    }

    @Override
    public StrataNode getParent() {
        return (StrataNode) super.getParent();
    }

    public SubStrata toBean() {

        SubStrata strata = SubStratas.newSubStrata();
        strata.setId(getId());
        return strata;

    }
}

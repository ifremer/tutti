package fr.ifremer.tutti.ui.swing.util.attachment.actions;

/*
 * #%L
 * Tutti :: UI
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.ui.swing.util.actions.LongActionSupport;
import fr.ifremer.tutti.ui.swing.util.attachment.AttachmentEditorUI;
import fr.ifremer.tutti.ui.swing.util.attachment.AttachmentItem;
import fr.ifremer.tutti.ui.swing.util.attachment.AttachmentItemHandler;
import fr.ifremer.tutti.ui.swing.util.attachment.AttachmentItemModel;
import org.nuiton.jaxx.application.ApplicationIOUtil;

import java.io.File;

import static org.nuiton.i18n.I18n.t;

/**
 * To persist a attachment.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.4
 */
public class SaveAttachmentAction extends LongActionSupport<AttachmentItemModel, AttachmentItem, AttachmentItemHandler> {

    protected File file;

    protected File attachmentFile;

    public SaveAttachmentAction(AttachmentItemHandler handler) {
        super(handler, false);
    }

    @Override
    public boolean prepareAction() throws Exception {
        boolean doAction = super.prepareAction();
        if (doAction) {

            AttachmentItemModel model = getModel();

            if (model.isCreate()) {
                attachmentFile = new File(model.getPath());
            } else {
                attachmentFile = getContext().getPersistenceService().getAttachmentFile(model.getId());
            }

            file = saveFile(
                    ApplicationIOUtil.getBaseName(model.getName()),
                    ApplicationIOUtil.getExtension(attachmentFile.getName()),
                    t("tutti.attachmentEditor.saveAttachment.title"),
                    t("tutti.attachmentEditor.saveAttachment.button"));
            doAction = file != null;
        }
        return doAction;
    }

    @Override
    public void doAction() throws Exception {

        AttachmentEditorUI upperUI = getUI().getParentContainer(AttachmentEditorUI.class);

        boolean hackDialog = upperUI.isAlwaysOnTop();
        if (hackDialog) {
            upperUI.setAlwaysOnTop(false);
        }
        try {

            ApplicationIOUtil.copyFile(attachmentFile, file,
                                       t("tutti.attachmentEditor.saveAttachment.error.message", attachmentFile, file.getName()));
            sendMessage(
                    t("tutti.attachmentEditor.saveAttachment.success.message", file.getName()));

        } finally {
            if (hackDialog) {
                upperUI.setAlwaysOnTop(true);
            }
        }
    }

    @Override
    public void releaseAction() {
        super.releaseAction();
        attachmentFile = null;
        file = null;
    }
}

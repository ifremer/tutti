package fr.ifremer.tutti.ui.swing.content.operation.catches.species.split;

/*
 * #%L
 * Tutti :: UI
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.ui.swing.content.operation.catches.species.SelectedCategoryAble;
import org.jdesktop.swingx.table.TableColumnModelExt;
import org.nuiton.jaxx.application.swing.table.AbstractApplicationTableModel;
import org.nuiton.jaxx.application.swing.table.ColumnIdentifier;

import java.util.List;
import java.util.Optional;

import static org.nuiton.i18n.I18n.n;

/**
 * Table model of sample categories values.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 0.3
 */
public class SplitSpeciesBatchTableModel
        extends AbstractApplicationTableModel<SplitSpeciesBatchRowModel> {

    private static final long serialVersionUID = 1L;

    public static final ColumnIdentifier<SplitSpeciesBatchRowModel> SELECTED = ColumnIdentifier.newId(
            SplitSpeciesBatchRowModel.PROPERTY_SELECTED,
            n("tutti.splitSpeciesBatch.table.header.selected"),
            n("tutti.splitSpeciesBatch.table.header.selected"));

    public static final ColumnIdentifier<SplitSpeciesBatchRowModel> EDITABLE_CATEGORY_VALUE = ColumnIdentifier.newId(
            SplitSpeciesBatchRowModel.PROPERTY_CATEGORY_VALUE,
            n("tutti.splitSpeciesBatch.table.header.category"),
            n("tutti.splitSpeciesBatch.table.header.category"));

    public static final ColumnIdentifier<SplitSpeciesBatchRowModel> READ_ONLY_CATEGORY_VALUE = ColumnIdentifier.newId(
            SplitSpeciesBatchRowModel.PROPERTY_CATEGORY_VALUE,
            n("tutti.splitSpeciesBatch.table.header.category"),
            n("tutti.splitSpeciesBatch.table.header.category"));

    public static final ColumnIdentifier<SplitSpeciesBatchRowModel> WEIGHT = ColumnIdentifier.newId(
            SplitSpeciesBatchRowModel.PROPERTY_WEIGHT,
            n("tutti.splitSpeciesBatch.table.header.weight"),
            n("tutti.splitSpeciesBatch.table.header.weight"));

    private final SelectedCategoryAble uiModel;

    /**
     * Is the table is in split mode?
     *
     * @since 2.6
     */
    private final boolean splitMode;

    public SplitSpeciesBatchTableModel(TableColumnModelExt columnModel,
                                       SelectedCategoryAble uiModel,
                                       boolean createEmptyRowIsEmpty,
                                       boolean splitMode) {
        super(columnModel, createEmptyRowIsEmpty, createEmptyRowIsEmpty);
        this.uiModel = uiModel;
        this.splitMode = splitMode;
        setNoneEditableCols(READ_ONLY_CATEGORY_VALUE);
    }

    @Override
    public SplitSpeciesBatchRowModel createNewRow() {
        SplitSpeciesBatchRowModel result = new SplitSpeciesBatchRowModel();
        result.setCategoryType(uiModel.getSelectedCategory());
        result.setValid(false);
        result.setEditable(true);
        return result;
    }

    @Override
    protected boolean isCellEditable(int rowIndex,
                                     int columnIndex,
                                     ColumnIdentifier<SplitSpeciesBatchRowModel> propertyName) {

        boolean result = super.isCellEditable(rowIndex,
                                              columnIndex,
                                              propertyName);
        if (result && !splitMode) {
            // if editable and on edit mode, use editable property on row
            SplitSpeciesBatchRowModel entry = getEntry(rowIndex);
            result = entry.isEditable();
        }
        return result;
    }

    public Optional<Float> getTotalWeight() {

        boolean found = false;
        float result = 0f;
        List<SplitSpeciesBatchRowModel> rows = getRows();
        for (SplitSpeciesBatchRowModel row : rows) {
            if (row.isSelected()) {
                Float weight = row.getWeight();
                if (weight != null) {
                    found = true;
                    result += weight;
                }
            }
        }
        return Optional.ofNullable(found ? result : null);
        
    }
}

package fr.ifremer.tutti.ui.swing.util;

/*
 * #%L
 * Tutti :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2016 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.MoreObjects;
import com.google.common.base.Optional;
import fr.ifremer.tutti.TuttiConfiguration;
import fr.ifremer.tutti.util.BeepFrequency;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.jaxx.application.ApplicationTechnicalException;

import java.io.Closeable;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

/**
 * Created on 20/01/16.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 4.4
 */
public class SoundEngine implements Closeable {

    /** Logger. */
    private static final Log log = LogFactory.getLog(SoundEngine.class);

    protected final TuttiConfiguration configuration;
    protected final Thread thread;
    protected final LinkedBlockingQueue<Object> soundsToPlay;
    protected boolean stop;

    public SoundEngine(TuttiConfiguration configuration) {
        this.configuration = configuration;
        this.thread = new Thread(new SoundEngineRunnable(), toString());
        this.soundsToPlay = new LinkedBlockingQueue<>();
        if (log.isInfoEnabled()) {
            log.info("Starting sound engine thread: " + thread);
        }
    }

    public void open() {
        thread.start();
    }

    public synchronized void beepOnExternalDeviceDataReception(String unit, float aroundLengthStep) {

        if (configuration.isExternalDevicesDataReceptionBeepEnabled()) {
            soundsToPlay.add(configuration.getExternalDevicesDataReceptionBeepFrequency());
        }

        if (configuration.isExternalDevicesVoiceEnabled()) {
            soundsToPlay.add(new Measure(unit, aroundLengthStep));
        }

    }

    public synchronized void beepOnExternalDeviceErrorReception() {

        if (configuration.isExternalDevicesErrorReceptionBeepEnabled()) {
            for (int i = 0; i < 3; i++) {
                soundsToPlay.add(configuration.getExternalDevicesDataReceptionBeepFrequency());
            }
        }

    }

    @Override
    public synchronized void close() {

        if (log.isInfoEnabled()) {
            log.info("Stopping sound engine thread: " + thread);
        }
        stop = true;
        thread.interrupt();
    }

    protected class Measure {

        protected final String unit;
        protected final float aroundLengthStep;

        protected Measure(String unit, float aroundLengthStep) {
            this.unit = unit;
            this.aroundLengthStep = aroundLengthStep;
        }

        @Override
        public String toString() {
            return MoreObjects.toStringHelper(this)
                    .add("unit", unit)
                    .add("aroundLengthStep", aroundLengthStep)
                    .toString();
        }
    }

    protected class SoundEngineRunnable implements Runnable {

        @Override
        public void run() {

            while (true) {

                try {
                    Object sound = soundsToPlay.poll(1, TimeUnit.SECONDS);

                    if (sound != null) {

                        if (sound instanceof Measure) {

                            Measure measure = (Measure) sound;

                            if (log.isInfoEnabled()) {
                                log.info("New Measure to say: " + measure);
                            }
                            String unit;
                            if (configuration.isExternalDevicesReadsUnit()) {
                                unit = measure.unit;
                            } else {
                                unit = null;
                            }
                            SoundUtil.readNumber(measure.aroundLengthStep, Optional.fromNullable(unit));

                        } else if (sound instanceof BeepFrequency) {
                            SoundUtil.beep(configuration.getExternalDevicesErrorReceptionBeepFrequency());
                        }

                    }

                    if (stop) {
                        break;
                    }

                } catch (InterruptedException e) {

                    if (!stop) {
                        throw new ApplicationTechnicalException("Could not get measure to say", e);
                    }

                }


            }

        }
    }

}

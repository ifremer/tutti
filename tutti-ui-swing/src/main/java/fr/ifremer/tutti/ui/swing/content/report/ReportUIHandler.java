package fr.ifremer.tutti.ui.swing.content.report;

/*
 * #%L
 * Tutti :: UI
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Lists;
import fr.ifremer.tutti.persistence.entities.data.FishingOperation;
import fr.ifremer.tutti.service.report.ReportGenerationService;
import fr.ifremer.tutti.ui.swing.util.AbstractTuttiUIHandler;
import jaxx.runtime.validator.swing.SwingValidator;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.jaxx.application.swing.util.CloseableUI;

import javax.swing.JComponent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.util.List;
import java.util.Set;

/**
 * Handler of {@link ReportUI}.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.9
 */
public class ReportUIHandler extends AbstractTuttiUIHandler<ReportUIModel, ReportUI> implements CloseableUI {

    /** Logger. */
    private static final Log log = LogFactory.getLog(ReportUIHandler.class);

    @Override
    public void beforeInit(ReportUI ui) {

        super.beforeInit(ui);

        ReportGenerationService reportService = getContext().getReportGenerationService();

        ReportUIModel model = new ReportUIModel();

        // set programId
        String programId = getDataContext().getProgramId();
        model.setProgramId(programId);

        // set cruiseId
        Integer cruiseId = getDataContext().getCruiseId();
        model.setCruiseId(cruiseId);

        // get all report availables
        List<File> reports = reportService.getAvailableReports();
        model.setReports(reports);

        // get all fishing operation
        List<FishingOperation> fishingOperations =
                getContext().getPersistenceService().getAllFishingOperation(cruiseId);
        model.setFishingOperations(fishingOperations);

        listModelIsModify(model);

        model.addPropertyChangeListener(new PropertyChangeListener() {

            Set<String> removeReportProperties= ImmutableSet.of(
                    ReportUIModel.PROPERTY_FISHING_OPERATION,
                    ReportUIModel.PROPERTY_REPORT
            );

            @Override
            public void propertyChange(PropertyChangeEvent evt) {

                ReportUIModel source = (ReportUIModel) evt.getSource();

                if (source .isReportDone() && removeReportProperties.contains(evt.getPropertyName())) {

                    // there was a result and input has changed, remove result
                    source.setReportGenerationResult(null);

                }

            }
        });
        ui.setContextValue(model);
    }

    @Override
    public void afterInit(ReportUI ui) {

        initUI(ui);

        ReportUIModel model = getModel();

        initBeanFilterableComboBox(ui.getReportComboBox(),
                                   Lists.newArrayList(model.getReports()),
                                   model.getReport());

        initBeanFilterableComboBox(ui.getFishingOperationComboBox(),
                                   Lists.newArrayList(model.getFishingOperations()),
                                   model.getFishingOperation());

        SwingValidator validator = ui.getValidator();
        listenValidatorValid(validator, model);

        registerValidators(validator);
    }

    @Override
    protected JComponent getComponentToFocus() {
        return getUI().getFishingOperationComboBox();
    }

    @Override
    public void onCloseUI() {
        if (log.isDebugEnabled()) {
            log.debug("closing: " + ui);
        }
        clearValidators();
    }

    @Override
    public boolean quitUI() {
        return true;
    }

    @Override
    public SwingValidator<ReportUIModel> getValidator() {
        return ui.getValidator();
    }

}

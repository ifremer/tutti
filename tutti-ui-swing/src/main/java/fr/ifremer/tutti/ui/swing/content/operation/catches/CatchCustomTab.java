package fr.ifremer.tutti.ui.swing.content.operation.catches;

/*
 * #%L
 * Tutti :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.ui.swing.util.AbstractTuttiBeanUIModel;
import org.nuiton.jaxx.application.ApplicationTechnicalException;
import org.nuiton.jaxx.application.swing.tab.CustomTab;
import org.nuiton.jaxx.application.swing.tab.TabContainerHandler;
import org.nuiton.jaxx.application.swing.tab.TabContentModel;
import org.nuiton.util.beans.BeanMonitor;
import org.nuiton.util.beans.BeanUtil;

import javax.swing.UIManager;
import java.awt.Font;
import java.util.Set;

import static org.nuiton.i18n.I18n.t;

/**
 * Une amélioration de CustomTab pour ne pas utiliser la propriété modify d'un modèle mais un sous ensemble de ses
 * propriétés.
 *
 * De plus on utilise un monitor donc quand plus rien n'est modifié, on peut aussi le savoir.
 *
 * TODO Il faudrait utiliser cet objet sur tous les onglets une fois un gros travail de nettoyage des listeners effectué
 * TODO car là c'est un peu chaotique et le monitor devient rapidemment inconsitent.
 *
 * Created on 3/12/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.13.3
 */
public class CatchCustomTab extends CustomTab {

    private static final long serialVersionUID = 1L;

    private final Set<String> modifyPropertyNames;

    public static CatchCustomTab newCustomTab(TabContentModel model, TabContainerHandler handler, Set<String> modifyPropertyNames) {

        CatchCustomTab customTab = new CatchCustomTab(model, handler, modifyPropertyNames);
        customTab.init();
        return customTab;

    }

    protected transient BeanMonitor monitor;

    protected final Font defaultFont;

    protected void init() {

        // init first monitor
        // so monitor will be notify before later listener
        monitor = new BeanMonitor(modifyPropertyNames.toArray(new String[modifyPropertyNames.size()]));
        monitor.setBean(model);

        // listen to the model
        try {
            BeanUtil.addPropertyChangeListener(evt -> onPropertyChanged(evt.getPropertyName(), evt.getNewValue()), this.model);
        } catch (Exception e) {
            throw new ApplicationTechnicalException("Could not init listener", e);
        }

    }

    void onPropertyChanged(String propertyName, Object value) {

        if (propertyName.equals(AbstractTuttiBeanUIModel.PROPERTY_MODIFY)) {

            Boolean newValue = (Boolean) value;
            if (newValue != null && !newValue) {

                // model no more modified, reset the monitor
                monitor.clearModified();
                updateTitle2(monitor.wasModified(), model.isEmpty());

            }

        } else {

            if (modifyPropertyNames.contains(propertyName)) {

                updateTitle2(monitor.wasModified(), model.isEmpty());

            }

        }

    }

    CatchCustomTab(TabContentModel model, TabContainerHandler handler, Set<String> modifyPropertyNames) {
        super(model, handler);
        this.modifyPropertyNames = modifyPropertyNames;
        this.defaultFont = UIManager.getDefaults().getFont("Label.font");
    }

    @Override
    protected void updateTitle() {
        // bad method (fire before monitor is notified + fired from constructor, bad, bad, bad!)
    }

    protected void updateTitle2(boolean modelModify, boolean modelEmpty) {

        String titleValue = t(model.getTitle());

        int style;
        if (modelModify) {
            style = Font.BOLD;
            titleValue += "*";

        } else if (modelEmpty) {
            style = Font.ITALIC;

        } else {
            style = Font.PLAIN;
        }

        title.setText(titleValue);
        title.setFont(defaultFont.deriveFont(style));

    }

}

package fr.ifremer.tutti.ui.swing.content.operation.catches.species.edit.actions;

/*
 * #%L
 * Tutti :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.adagio.core.dao.referential.pmfm.PmfmId;
import fr.ifremer.adagio.core.dao.referential.pmfm.QualitativeValueId;
import fr.ifremer.tutti.persistence.entities.data.SampleCategoryModel;
import fr.ifremer.tutti.persistence.entities.data.SampleCategoryModelEntry;
import fr.ifremer.tutti.persistence.entities.referential.Caracteristic;
import fr.ifremer.tutti.persistence.entities.referential.CaracteristicQualitativeValue;
import fr.ifremer.tutti.persistence.entities.referential.CaracteristicQualitativeValues;
import fr.ifremer.tutti.service.DecoratorService;
import fr.ifremer.tutti.ui.swing.content.operation.catches.EditCatchesUIModel;
import fr.ifremer.tutti.ui.swing.content.operation.catches.species.edit.SpeciesBatchRowModel;
import fr.ifremer.tutti.ui.swing.content.operation.catches.species.edit.SpeciesBatchTableModel;
import fr.ifremer.tutti.ui.swing.content.operation.catches.species.edit.SpeciesBatchUI;
import fr.ifremer.tutti.ui.swing.content.operation.catches.species.edit.SpeciesBatchUIHandler;
import fr.ifremer.tutti.ui.swing.content.operation.catches.species.edit.SpeciesBatchUIModel;
import fr.ifremer.tutti.ui.swing.util.TuttiUI;
import fr.ifremer.tutti.ui.swing.util.actions.SimpleActionSupport;
import jaxx.runtime.SwingUtil;
import jaxx.runtime.swing.renderer.DecoratorListCellRenderer;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.jaxx.widgets.number.NumberEditor;

import javax.swing.JComboBox;
import javax.swing.JOptionPane;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import static org.nuiton.i18n.I18n.t;

/**
 * Created on 3/7/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.15
 */
public class ChangeSpeciesBatchSampleCategoryAction extends SimpleActionSupport<SpeciesBatchUI> {

    /** Logger. */
    private static final Log log = LogFactory.getLog(ChangeSpeciesBatchSampleCategoryAction.class);

    private static final long serialVersionUID = -6540241422935319461L;

    public ChangeSpeciesBatchSampleCategoryAction(SpeciesBatchUI ui) {
        super(ui);
    }

    @Override
    protected void onActionPerformed(SpeciesBatchUI ui) {

        SpeciesBatchUIHandler handler = ui.getHandler();

        // get table model
        SpeciesBatchTableModel tableModel = handler.getTableModel();

        // get selected row
        int rowIndex = SwingUtil.getSelectedModelRow(ui.getTable());

        // get selected column
        int columnIndex = SwingUtil.getSelectedModelColumn(ui.getTable());

        // get selected row
        SpeciesBatchRowModel selectedRow = tableModel.getEntry(rowIndex);

        SampleCategoryModel sampleCategoryModel = handler.getSampleCategoryModel();

        // get his sample category
        Integer sampleCategoryId = tableModel.getSampleCategoryId(columnIndex);
        SampleCategoryModelEntry sampleCategoryEntry = sampleCategoryModel.getCategoryById(sampleCategoryId);
        Caracteristic caracteristic = sampleCategoryEntry.getCaracteristic();

        // get the first ancestor row using this category
        SpeciesBatchRowModel firstAncestorRow = selectedRow.getFirstAncestor(sampleCategoryId);

        // get used values
        Set<Serializable> usedValues = handler.getSampleUsedValues(firstAncestorRow, sampleCategoryId);

        // get the new selected value for this category
        Serializable selectedItem;

        String categoryDecorated = handler.getDecorator(caracteristic.getClass(), DecoratorService.CARACTERISTIC_PARAMETER_ONLY).toString(caracteristic);
        String dialogTitle = t("tutti.editSpeciesBatch.title.changeSampleCategoryValue", categoryDecorated);

        boolean numericType = caracteristic.isNumericType();

        if (numericType) {

            // open a simple number editor
            NumberEditor editor = new NumberEditor();
            editor.setNumberPattern(TuttiUI.DECIMAL3_PATTERN);
            editor.setUseSign(false);
            editor.setShowPopupButton(false);
            editor.setShowReset(false);

            int response = JOptionPane.showConfirmDialog(ui, editor, dialogTitle, JOptionPane.OK_CANCEL_OPTION);

            if (response == JOptionPane.OK_OPTION) {
                selectedItem = editor.getModel();
            } else {
                // user cancel selection
                selectedItem = null;
            }

            if (usedValues.contains(selectedItem)) {

                // impossible de choisir cette valeur (déjà utilisée)
                JOptionPane.showMessageDialog(
                        handler.getTopestUI(),
                        t("tutti.editSpeciesBatch.error.sampleCategoryValue.notAvailable", selectedItem, categoryDecorated));
                selectedItem = null;
            }

        } else {

            // open a combobox to select new value

            List<CaracteristicQualitativeValue> availableValues = new ArrayList<>(caracteristic.getQualitativeValue());

            boolean firstCategory = sampleCategoryModel.getFirstCategoryId().equals(sampleCategoryId);

            if (firstCategory) {

                // remove the unsorted qualitative value
                CaracteristicQualitativeValues.removeQualitativeValue(availableValues, handler.getQualitative_unsorted_id());
            }

            availableValues.removeAll(usedValues);

            JComboBox editor = new JComboBox();
            editor.setRenderer(new DecoratorListCellRenderer(handler.getDecorator(CaracteristicQualitativeValue.class, null)));
            SwingUtil.fillComboBox(editor, availableValues, availableValues.get(0));

            int response = JOptionPane.showConfirmDialog(handler.getTopestUI(), editor, dialogTitle, JOptionPane.OK_CANCEL_OPTION);

            if (response == JOptionPane.OK_OPTION) {
                selectedItem = (CaracteristicQualitativeValue) editor.getSelectedItem();
            } else {
                // user cancel selection
                selectedItem = null;
            }
        }

        if (log.isDebugEnabled()) {
            log.debug("New selected category: " + selectedItem);
        }

        if (selectedItem != null) {

            // update rows values
            Serializable oldValue = tableModel.updateSampleCategorieValue(firstAncestorRow, columnIndex, selectedItem);

            // save the first ancestor row with the modified category value
            handler.saveRow(firstAncestorRow);

            SpeciesBatchUIModel model = ui.getModel();

            // if the user modifies the sorted/unsorted category, update the count of species batches for the categories
            // cf https://forge.codelutin.com/issues/8255
            if (PmfmId.SORTED_UNSORTED.getValue().equals(sampleCategoryId)) {

                // if the new value is sorted, the previous value was unsorted
                if (QualitativeValueId.SORTED_VRAC.getValue().equals(((CaracteristicQualitativeValue) selectedItem).getIdAsInt())) {
                    model.decDistinctUnsortedSpeciesCount();
                    model.incDistinctSortedSpeciesCount();

                } else { // if the new value is unsorted, the previous value was sorted
                    model.decDistinctSortedSpeciesCount();
                    model.incDistinctUnsortedSpeciesCount();
                }
            }

            if (!numericType) {

                // update speciesUsed cache
                EditCatchesUIModel createBatchModel = model.getCatchesUIModel();
                createBatchModel.replaceCaracteristicValue(firstAncestorRow.getSpecies(), (CaracteristicQualitativeValue) oldValue, (CaracteristicQualitativeValue) selectedItem);

            }

        }

    }
}

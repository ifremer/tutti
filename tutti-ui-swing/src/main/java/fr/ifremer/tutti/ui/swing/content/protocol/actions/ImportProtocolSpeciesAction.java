package fr.ifremer.tutti.ui.swing.content.protocol.actions;

/*
 * #%L
 * Tutti :: UI
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import fr.ifremer.tutti.persistence.entities.protocol.SpeciesProtocol;
import fr.ifremer.tutti.persistence.entities.protocol.TuttiProtocol;
import fr.ifremer.tutti.persistence.entities.referential.Species;
import fr.ifremer.tutti.service.DecoratorService;
import fr.ifremer.tutti.service.protocol.ProtocolImportExportService;
import fr.ifremer.tutti.ui.swing.util.actions.LongActionSupport;
import fr.ifremer.tutti.ui.swing.content.protocol.EditProtocolSpeciesRowModel;
import fr.ifremer.tutti.ui.swing.content.protocol.EditProtocolUI;
import fr.ifremer.tutti.ui.swing.content.protocol.EditProtocolUIHandler;
import fr.ifremer.tutti.ui.swing.content.protocol.EditProtocolUIModel;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.File;
import java.util.List;

import static org.nuiton.i18n.I18n.t;

/**
 * To import protocol species.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.0
 */
public class ImportProtocolSpeciesAction extends LongActionSupport<EditProtocolUIModel, EditProtocolUI, EditProtocolUIHandler> {

    /** Logger. */
    private static final Log log =
            LogFactory.getLog(ImportProtocolSpeciesAction.class);

    private File file;

    private TuttiProtocol protocol;

    private List<Species> notImportedSpecies;

    public ImportProtocolSpeciesAction(EditProtocolUIHandler handler) {
        super(handler, false);
    }

    @Override
    public boolean prepareAction() throws Exception {

        boolean doAction = super.prepareAction();

        if (doAction) {

            // choose file to import
            file = chooseFile(
                    t("tutti.editProtocol.title.choose.speciesImportFile"),
                    t("tutti.editProtocol.action.importProtocolSpeciesFile"),
                    "^.*\\.csv", t("tutti.common.file.csv")
            );

            doAction = file != null;
        }
        return doAction;
    }

    @Override
    public void doAction() throws Exception {
        Preconditions.checkNotNull(file);
        if (log.isInfoEnabled()) {
            log.info("Will import protocol species file: " + file);
        }

        EditProtocolUIModel model = getModel();

        // bind to a protocol
        protocol = model.toEntity();

        // import
        ProtocolImportExportService service =
                getContext().getTuttiProtocolImportExportService();

        notImportedSpecies = service.importProtocolSpecies(file,
                                                           protocol,
                                                           model.getAllCaracteristic(),
                                                           model.getAllReferentSpeciesByTaxonId());
    }

    @Override
    public void postSuccessAction() {
        super.postSuccessAction();

        // build rows from imported+merged protocol
        // (will also remove all synonyms of species referent used)
        List<EditProtocolSpeciesRowModel> rows =
                handler.toSpeciesRows(protocol.getSpecies());

        // update species comboBox
        getUI().getSpeciesComboBox().getHandler().reset();

        // update benthos comboBox
        getUI().getBenthosComboBox().getHandler().reset();

        // update rows in model
        getModel().setSpeciesRow(rows);

        getHandler().getSpeciesTableModel().setRows(rows);

        int nbSynonym = 0;
        for (SpeciesProtocol speciesProtocol : protocol.getSpecies()) {
            if (!speciesProtocol.isMadeFromAReferentTaxon()) {
                nbSynonym++;
            }
        }

        String message;
        switch (nbSynonym) {
            case 0:
                message = t("tutti.flash.info.species.imported.in.protocol",
                            file);
                break;

            case 1:
                message = t("tutti.flash.info.species.imported.in.protocol.oneReplaced",
                            file);
                break;

            default:
                message = t("tutti.flash.info.species.imported.in.protocol.severalReplaced",
                            file, nbSynonym);
        }
        sendMessage(message);

        if (!notImportedSpecies.isEmpty()) {
            StringBuilder sb = new StringBuilder();
            for (Species sp : notImportedSpecies) {
                sb.append("<li>").append(decorate(sp, DecoratorService.FROM_PROTOCOL)).append("</li>");
            }
            displayWarningMessage(
                    t("tutti.editProtocol.action.importProtocolSpecies.speciesInBenthos.title"),
                    "<html><body>" +
                    t("tutti.editProtocol.action.importProtocolSpecies.speciesInBenthos", sb.toString()) +
                    "</body></html>"
            );
        }
    }

    @Override
    public void releaseAction() {
        file = null;
        protocol = null;
        notImportedSpecies = null;
        super.releaseAction();
    }
}

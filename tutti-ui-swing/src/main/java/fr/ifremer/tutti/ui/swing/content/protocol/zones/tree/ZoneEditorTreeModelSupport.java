package fr.ifremer.tutti.ui.swing.content.protocol.zones.tree;

/*
 * #%L
 * Tutti :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2016 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */


import com.google.common.collect.ImmutableMap;
import fr.ifremer.tutti.ui.swing.content.protocol.zones.tree.node.StrataNode;
import fr.ifremer.tutti.ui.swing.content.protocol.zones.tree.node.SubStrataNode;
import fr.ifremer.tutti.ui.swing.content.protocol.zones.tree.node.ZoneEditorNodeSupport;
import fr.ifremer.tutti.ui.swing.content.protocol.zones.tree.node.ZoneNode;
import org.apache.commons.collections4.EnumerationUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.swing.event.TreeModelEvent;
import javax.swing.tree.DefaultTreeModel;
import java.util.LinkedHashSet;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;

/**
 * @author Kevin Morin (Code Lutin)
 * @author Tony Chemit - chemit@codelutin.com
 * @since 4.5
 */
public abstract class ZoneEditorTreeModelSupport extends DefaultTreeModel {

    /** Logger. */
    private static final Log log = LogFactory.getLog(ZoneEditorTreeModelSupport.class);

    /**
     * Un drapeau pour bloquer les listeners lors de la création du modèle.
     */
    private boolean creatingModel;

    /**
     * Un cache de libellés de locations par leur identifiant.
     */
    private ImmutableMap<String, String> locationLabelCache;

    /**
     * L'ensemble des nœuds inserés pendant que le modèle est en mode création. On doit relancer alors les fire à la fin.
     */
    private final Set<TreeModelEvent> nodeInserted = new LinkedHashSet<>();

    public ZoneEditorTreeModelSupport(String rootLabel) {
        super(new ZoneNode("No fucking way! I am not a zone", rootLabel));
    }

    public void setCreatingModel(boolean creatingModel) {
        this.creatingModel = creatingModel;
        if (creatingModel) {
            nodeInserted.clear();
        } else {
            try {
                nodeInserted.forEach(treeModelEvent -> fireTreeNodesInserted(
                        treeModelEvent.getSource(), treeModelEvent.getPath(), treeModelEvent.getChildIndices(), treeModelEvent.getChildren()
                ));
            } finally {
                nodeInserted.clear();
            }
        }
    }

    public void addNode(ZoneEditorNodeSupport parent, ZoneEditorNodeSupport newChild) {
        int index = parent.getFutureNodePosition(newChild);
        insertNodeInto(newChild, parent, index);
    }

    @Override
    public ZoneNode getRoot() {
        return (ZoneNode) super.getRoot();
    }

    @Override
    protected void fireTreeNodesChanged(Object source, Object[] path, int[] childIndices, Object[] children) {
        if (creatingModel) {
            return;
        }
        super.fireTreeNodesChanged(source, path, childIndices, children);
    }

    @Override
    protected void fireTreeNodesInserted(Object source, Object[] path, int[] childIndices, Object[] children) {
        if (creatingModel) {
            nodeInserted.add(new TreeModelEvent(source, path, childIndices, children));
            return;
        }
        super.fireTreeNodesInserted(source, path, childIndices, children);
    }

    @Override
    protected void fireTreeNodesRemoved(Object source, Object[] path, int[] childIndices, Object[] children) {
        if (creatingModel) {
            return;
        }
        super.fireTreeNodesRemoved(source, path, childIndices, children);
    }

    @Override
    protected void fireTreeStructureChanged(Object source, Object[] path, int[] childIndices, Object[] children) {
        if (creatingModel) {
            return;
        }
        super.fireTreeStructureChanged(source, path, childIndices, children);
    }

    protected String getLocationLabel(String locationId) {
        String locationLabel = locationLabelCache.get(locationId);
        Objects.requireNonNull(locationLabel, "Libellé de location non trouvé pour l'identifiant: " + locationId);
        return locationLabel;
    }

    public void setLocationLabelCache(ImmutableMap<String, String> locationLabelCache) {
        this.locationLabelCache = locationLabelCache;
    }

    public StrataNode addStrata(ZoneNode zoneNode, String strataId) {

        String strataLabel = getLocationLabel(strataId);
        if (log.isInfoEnabled()) {
            log.info("Add strata: " + strataLabel + " to zone node: " + zoneNode);
        }

        StrataNode strataNode = new StrataNode(strataId, strataLabel);
        addNode(zoneNode, strataNode);
        return strataNode;
    }

    public SubStrataNode addSubsStrata(StrataNode strataNode, String subStrataId) {

        String subStrataLabel = getLocationLabel(subStrataId);
        if (log.isInfoEnabled()) {
            log.info("Add subStrata: " + subStrataLabel + " to strata node: " + strataNode);
        }

        SubStrataNode subStrataNode = new SubStrataNode(subStrataId, subStrataLabel);
        addNode(strataNode, subStrataNode);
        return subStrataNode;

    }

    public StrataNode getOrCreateStrataNode(ZoneNode zoneNode, String strataId) {

        StrataNode strataNode;

        Optional<StrataNode> optionalStrataNode = zoneNode.tryFindStrataNode(strataId);

        if (optionalStrataNode.isPresent()) {

            // la strate existe déjà dans la zone, on la récupère
            strataNode = optionalStrataNode.get();

        } else {

            // la strate n'existe pas, on la crée et l'ajoute à la zone
            // ajout du nœud dans l'arbre
            strataNode = addStrata(zoneNode, strataId);

        }

        return strataNode;

    }

    public void moveSubStratas(StrataNode sourceNode, StrataNode targetNode) {

        EnumerationUtils.toList(sourceNode.children())
                        .forEach(subStrataNode -> {
                            removeNodeFromParent(subStrataNode);
                            addNode(targetNode, subStrataNode);
                        });

    }

}

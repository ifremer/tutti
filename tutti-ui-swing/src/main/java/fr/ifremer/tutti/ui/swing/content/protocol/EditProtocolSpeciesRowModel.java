package fr.ifremer.tutti.ui.swing.content.protocol;

/*
 * #%L
 * Tutti :: UI
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.entities.protocol.CalcifiedPiecesSamplingDefinition;
import fr.ifremer.tutti.persistence.entities.protocol.Rtp;
import fr.ifremer.tutti.persistence.entities.protocol.SpeciesProtocol;
import fr.ifremer.tutti.persistence.entities.protocol.SpeciesProtocols;
import fr.ifremer.tutti.persistence.entities.referential.Caracteristic;
import fr.ifremer.tutti.persistence.entities.referential.CaracteristicQualitativeValue;
import fr.ifremer.tutti.persistence.entities.referential.Species;
import fr.ifremer.tutti.persistence.entities.referential.Speciess;
import fr.ifremer.tutti.ui.swing.util.AbstractTuttiBeanUIModel;
import org.nuiton.util.beans.Binder;
import org.nuiton.util.beans.BinderFactory;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

/**
 * @author kmorin
 * @since 0.3
 */
public class EditProtocolSpeciesRowModel extends AbstractTuttiBeanUIModel<SpeciesProtocol, EditProtocolSpeciesRowModel> implements SpeciesProtocol {

    private static final long serialVersionUID = 1L;

    public static final String PROPERTY_SPECIES_SURVEY_CODE = "speciesSurveyCode";

    public static final String PROPERTY_SPECIES = "species";

    public static final String PROPERTY_LENGTH_STEP_PMFM = "lengthStepPmfm";

    public static final String PROPERTY_WEIGHT_ENABLED = "weightEnabled";

    public static final String PROPERTY_MATURITY_PMFM = "maturityPmfm";

    public static final String PROPERTY_CALCIFIED_PIECES_SAMPLING_TYPE = "calcifiedPiecesSamplingType";

    public static final String PROPERTY_USE_RTP = "useRtp";

    protected Species species;

    protected String speciesSurveyCode;

    protected Caracteristic lengthStepPmfm;

    protected boolean weightEnabled;

//    protected boolean countIfNoFrequencyEnabled;
//
//    protected boolean calcifySampleEnabled;

    protected List<Integer> mandatorySampleCategoryId;

    protected boolean individualObservationEnabled;

    protected Caracteristic maturityPmfm;

    protected CaracteristicQualitativeValue calcifiedPiecesSamplingType;

    protected Rtp rtpMale;

    protected Rtp rtpFemale;

    protected Rtp rtpUndefined;

    Collection<CalcifiedPiecesSamplingDefinition> calcifiedPiecesSamplingDefinition;

    protected static final Binder<SpeciesProtocol, EditProtocolSpeciesRowModel> fromBeanBinder =
            BinderFactory.newBinder(SpeciesProtocol.class,
                    EditProtocolSpeciesRowModel.class);

    protected static final Binder<EditProtocolSpeciesRowModel, SpeciesProtocol> toBeanBinder =
            BinderFactory.newBinder(EditProtocolSpeciesRowModel.class,
                    SpeciesProtocol.class);

    public EditProtocolSpeciesRowModel() {
        super(fromBeanBinder, toBeanBinder);
    }

    @Override
    public Integer getSpeciesReferenceTaxonId() {
        return species.getReferenceTaxonId();
    }

    @Override
    public void setSpeciesReferenceTaxonId(Integer speciesReferenceTaxonId) {
    }

    public Species getSpecies() {
        return species;
    }

    public void setSpecies(Species species) {
        Object oldValue = getSpecies();
        this.species = species;
        firePropertyChange(PROPERTY_SPECIES, oldValue, species);
    }

    @Override
    public String getSpeciesSurveyCode() {
        return speciesSurveyCode;
    }

    @Override
    public void setSpeciesSurveyCode(String speciesSurveyCode) {
        Object oldValue = getSpeciesSurveyCode();
        this.speciesSurveyCode = speciesSurveyCode;
        firePropertyChange(PROPERTY_SPECIES_SURVEY_CODE, oldValue, speciesSurveyCode);
    }

    @Override
    public String getLengthStepPmfmId() {
        return lengthStepPmfm != null ? lengthStepPmfm.getId() : null;
    }

    @Override
    public void setLengthStepPmfmId(String lengthStepPmfmId) {
    }

    @Override
    public boolean withLengthStepPmfm() {
        return lengthStepPmfm != null;
    }

    @Override
    public boolean withMaturityPmfm() {
        return maturityPmfm != null;
    }

    @Override
    public boolean withCalcifiedPiecesSamplingType() {
        return calcifiedPiecesSamplingType != null;
    }

    public Caracteristic getLengthStepPmfm() {
        return lengthStepPmfm;
    }

    public void setLengthStepPmfm(Caracteristic lengthStepPmfm) {
        Object oldValue = getLengthStepPmfm();
        this.lengthStepPmfm = lengthStepPmfm;
        firePropertyChange(PROPERTY_LENGTH_STEP_PMFM, oldValue, lengthStepPmfm);
    }

    @Override
    public boolean isWeightEnabled() {
        return weightEnabled;
    }

    @Override
    public void setWeightEnabled(boolean weightEnabled) {
        Object oldValue = isWeightEnabled();
        this.weightEnabled = weightEnabled;
        firePropertyChange(PROPERTY_WEIGHT_ENABLED, oldValue, weightEnabled);
    }

    @Override
    public Float getLengthStep() {
        return null;
    }

    @Override
    public void setLengthStep(Float lengthStep) {
    }

    @Override
    public boolean isMadeFromAReferentTaxon() {
        return false;
    }

    @Override
    public void setMadeFromAReferentTaxon(boolean madeFromAReferentTaxon) {
    }

    @Override
    protected SpeciesProtocol newEntity() {
        return SpeciesProtocols.newSpeciesProtocol();
    }

    @Override
    public Integer getMandatorySampleCategoryId(int index) {
        return getChild(mandatorySampleCategoryId, index);
    }

    @Override
    public boolean isMandatorySampleCategoryIdEmpty() {
        return mandatorySampleCategoryId == null || mandatorySampleCategoryId.isEmpty();
    }

    @Override
    public int sizeMandatorySampleCategoryId() {
        return mandatorySampleCategoryId == null ? 0 : mandatorySampleCategoryId.size();
    }

    @Override
    public void addMandatorySampleCategoryId(Integer mandatorySampleCategoryId) {
        getMandatorySampleCategoryId().add(mandatorySampleCategoryId);
    }

    @Override
    public void addAllMandatorySampleCategoryId(Collection<Integer> mandatorySampleCategoryId) {
        getMandatorySampleCategoryId().addAll(mandatorySampleCategoryId);
    }

    @Override
    public boolean removeMandatorySampleCategoryId(Integer mandatorySampleCategoryId) {
        return getMandatorySampleCategoryId().remove(mandatorySampleCategoryId);
    }

    @Override
    public boolean removeAllMandatorySampleCategoryId(Collection<Integer> mandatorySampleCategoryId) {
        return getMandatorySampleCategoryId().removeAll(mandatorySampleCategoryId);
    }

    @Override
    public boolean containsMandatorySampleCategoryId(Integer mandatorySampleCategoryId) {
        return getMandatorySampleCategoryId().contains(mandatorySampleCategoryId);
    }

    @Override
    public boolean containsAllMandatorySampleCategoryId(Collection<Integer> mandatorySampleCategoryId) {
        return getMandatorySampleCategoryId().containsAll(mandatorySampleCategoryId);
    }

    @Override
    public List<Integer> getMandatorySampleCategoryId() {
        return mandatorySampleCategoryId;
    }

    @Override
    public void setMandatorySampleCategoryId(List<Integer> mandatorySampleCategoryId) {
        this.mandatorySampleCategoryId = mandatorySampleCategoryId;
        firePropertyChange(PROPERTY_MANDATORY_SAMPLE_CATEGORY_ID, null, mandatorySampleCategoryId);
    }

    @Override
    public boolean isIndividualObservationEnabled() {
        return individualObservationEnabled;
    }

    @Override
    public void setIndividualObservationEnabled(boolean individualObservationEnabled) {
        Object oldValue = isIndividualObservationEnabled();
        this.individualObservationEnabled = individualObservationEnabled;
        firePropertyChange(PROPERTY_INDIVIDUAL_OBSERVATION_ENABLED, oldValue, individualObservationEnabled);
    }

    @Override
    public String getMaturityPmfmId() {
        return maturityPmfm != null ? maturityPmfm.getId() : null;
    }

    @Override
    public void setMaturityPmfmId(String maturityPmfmId) {
    }

    public Caracteristic getMaturityPmfm() {
        return maturityPmfm;
    }

    public void setMaturityPmfm(Caracteristic maturityPmfm) {
        Object oldValue = getMaturityPmfm();
        this.maturityPmfm = maturityPmfm;
        firePropertyChange(PROPERTY_MATURITY_PMFM, oldValue, maturityPmfm);
    }

    @Override
    public CaracteristicQualitativeValue getCalcifiedPiecesSamplingType() {
        return calcifiedPiecesSamplingType;
    }

    @Override
    public void setCalcifiedPiecesSamplingType(CaracteristicQualitativeValue calcifiedPiecesSamplingType) {
        Object oldValue = getCalcifiedPiecesSamplingType();
        this.calcifiedPiecesSamplingType = calcifiedPiecesSamplingType;
        firePropertyChange(PROPERTY_CALCIFIED_PIECES_SAMPLING_TYPE, oldValue, calcifiedPiecesSamplingType);
    }

    public boolean isUseRtp() {
        return withRtpFemale() && withRtpMale() && withRtpUndefined();
    }

    public void setUseRtp(boolean useRtp) {
//        Object oldValue = isUseRtp();
//        this.useRtp = useRtp;
        // Normalement pas utilisé
        firePropertyChange(PROPERTY_USE_RTP, null, useRtp);
    }

    @Override
    public Rtp getRtpMale() {
        return rtpMale;
    }

    @Override
    public void setRtpMale(Rtp rtpMale) {
        Object oldValue = getRtpMale();
        this.rtpMale = rtpMale;
        firePropertyChange(PROPERTY_RTP_MALE, oldValue, rtpMale);
    }

    @Override
    public Rtp getRtpFemale() {
        return rtpFemale;
    }

    @Override
    public void setRtpFemale(Rtp rtpFemale) {
        Object oldValue = getRtpFemale();
        this.rtpFemale = rtpFemale;
        firePropertyChange(PROPERTY_RTP_FEMALE, oldValue, rtpFemale);
    }

    @Override
    public Rtp getRtpUndefined() {
        return rtpUndefined;
    }

    public void setRtpUndefined(Rtp rtpUndefined) {
        Object oldValue = getRtpUndefined();
        this.rtpUndefined = rtpUndefined;
        firePropertyChange(PROPERTY_RTP_UNDEFINED, oldValue, rtpUndefined);
    }

    @Override
    public CalcifiedPiecesSamplingDefinition getCalcifiedPiecesSamplingDefinition(int index) {
        return getChild(calcifiedPiecesSamplingDefinition, index);
    }

    @Override
    public boolean isCalcifiedPiecesSamplingDefinitionEmpty() {
        return calcifiedPiecesSamplingDefinition == null || calcifiedPiecesSamplingDefinition.isEmpty();
    }

    @Override
    public int sizeCalcifiedPiecesSamplingDefinition() {
        return calcifiedPiecesSamplingDefinition == null ? 0 : calcifiedPiecesSamplingDefinition.size();
    }

    @Override
    public void addCalcifiedPiecesSamplingDefinition(CalcifiedPiecesSamplingDefinition calcifiedPiecesSamplingDefinition) {
        getCalcifiedPiecesSamplingDefinition().add(calcifiedPiecesSamplingDefinition);
    }

    @Override
    public void addAllCalcifiedPiecesSamplingDefinition(Collection<CalcifiedPiecesSamplingDefinition> calcifiedPiecesSamplingDefinition) {
        getCalcifiedPiecesSamplingDefinition().addAll(calcifiedPiecesSamplingDefinition);
    }

    @Override
    public boolean removeCalcifiedPiecesSamplingDefinition(CalcifiedPiecesSamplingDefinition calcifiedPiecesSamplingDefinition) {
        return getCalcifiedPiecesSamplingDefinition().remove(calcifiedPiecesSamplingDefinition);
    }

    @Override
    public boolean removeAllCalcifiedPiecesSamplingDefinition(Collection<CalcifiedPiecesSamplingDefinition> calcifiedPiecesSamplingDefinition) {
        return getCalcifiedPiecesSamplingDefinition().removeAll(calcifiedPiecesSamplingDefinition);
    }

    @Override
    public boolean containsCalcifiedPiecesSamplingDefinition(CalcifiedPiecesSamplingDefinition calcifiedPiecesSamplingDefinition) {
        return getCalcifiedPiecesSamplingDefinition().contains(calcifiedPiecesSamplingDefinition);
    }

    @Override
    public boolean containsAllCalcifiedPiecesSamplingDefinition(Collection<CalcifiedPiecesSamplingDefinition> calcifiedPiecesSamplingDefinition) {
        return getCalcifiedPiecesSamplingDefinition().containsAll(calcifiedPiecesSamplingDefinition);
    }

    @Override
    public Collection<CalcifiedPiecesSamplingDefinition> getCalcifiedPiecesSamplingDefinition() {
        if (calcifiedPiecesSamplingDefinition == null) {
            calcifiedPiecesSamplingDefinition = new LinkedList<>();
        }
        return calcifiedPiecesSamplingDefinition;
    }

    @Override
    public void setCalcifiedPiecesSamplingDefinition(Collection<CalcifiedPiecesSamplingDefinition> calcifiedPiecesSamplingDefinition) {
        this.calcifiedPiecesSamplingDefinition = calcifiedPiecesSamplingDefinition;
    }

    @Override
    public boolean withRtpMale() {
        return rtpMale != null;
    }

    @Override
    public boolean withRtpFemale() {
        return rtpFemale != null;
    }

    @Override
    public boolean withRtpUndefined() {
        return rtpUndefined != null;
    }

    public Species toSpeciesWithSurveyCode() {
        Species source = getSpecies();
        Species target = Speciess.newSpecies(source);
        target.setSurveyCode(getSpeciesSurveyCode());
        return target;
    }
}

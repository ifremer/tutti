@echo off

rem move embedded launcher file to tutti root
verify on
move /Y tutti\launcher launcher
del /F /Q launcher\\*.sh

move /Y launcher\tutti.exe tutti.exe
move /Y launcher\tutti.bat tutti.bat
move /Y launcher\launcher.bat launcher.jar
move /Y launcher\\README README
start tutti.exe

@echo off

echo !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
echo !                                                                         !
echo ! THE LAUNCH AND UPDATE PROCESSES HAVE BEEN CHANGED                       !
echo ! PLEASE EXECUTE 'tutti.exe'                                              !
echo !                                                                         !
echo !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
echo !                                                                         !
echo ! LES PROCESSUS DE DEMARRAGE ET MISE A JOUR ONT CHANGES                   !
echo ! VEUILLEZ EXECUTER 'tutti.exe'                                           !
echo !                                                                         !
echo !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
pause
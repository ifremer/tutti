#!/bin/bash

echo "Move embedded script file to tutti root"
mv -f tutti/launcher .
rm launcher/*.exe
rm launcher/*.bat
mv launcher/*.sh .
mv launcher/*.jar .
mv launcher/README .
chmod +x *.sh
./tutti.sh $*

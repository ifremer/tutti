#!/bin/bash

TUTTI_BASEDIR=$(pwd)
TUTTI_HOME=$TUTTI_BASEDIR/tutti
JAVA_HOME=$TUTTI_BASEDIR/jre
JAVA_COMMAND=$JAVA_HOME/bin/java

echo "Allegro Campaign basedir:  $TUTTI_BASEDIR"

$JAVA_COMMAND -jar launcher.jar --db-sanity true $*

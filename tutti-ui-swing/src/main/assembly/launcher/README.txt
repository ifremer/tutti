Pour démarrer Allegro Campagne Saisie
-------------------------------------

# Sous windows

tutti.exe

ou

tutti.bat

# Sous Linux

./tutti.sh

Pour nettoyer la base de donnée
-------------------------------

Si vous rencontrez des problèmes avec la base de données, vous pouvez tenter de lancer cette commande

# Sous windows

tutti-sanity.bat

# Sous Linux

./tutti-sanity.sh

Consulter l'aide
----------------

L'aide en ligne est consultable dans l'application ou bien sur le site

http://tutti.codelutin.com/v/latest/help/fr/index.html

Le manuel d'utilisation (Guide_utilisation_Allegro_Campagne.pdf) est aussi
disponible dans dans le dossier help.

@echo off
verify on
cd /d %~dp0%
set BACKUP_DATE=~~BACKUP_DATE~~
if not exist NEW\\jre goto launcher
  set /p oldVersion=<jre\\version.appup
  set /p newVersion=<NEW\\jre\\version.appup
  set backupdir=OLD\\jre-%oldVersion%-%BACKUP_DATE%
  echo Update jre version %oldVersion% to %newVersion% old jre keep in "%backupdir%"
  if not exist OLD mkdir OLD
  move /Y jre "%backupdir%"
  move /Y NEW\\jre jre

:launcher
if not exist NEW\\launcher goto end
  set /p oldVersion=<launcher\\version.appup
  set /p newVersion=<NEW\\launcher\\version.appup
  set backupdir=OLD\\launcher-%oldVersion%-%BACKUP_DATE%
  echo Update launcher version %oldVersion% to %newVersion% old launcher keep in "%backupdir%"
  if not exist OLD mkdir OLD
  move /Y tutti.exe launcher
  move /Y tutti*.bat launcher
  move /Y launcher.jar launcher
  move /Y launcher "%backupdir%"

  move /Y NEW\\launcher launcher
  del /F /Q launcher\\*.sh
  move /Y launcher\\*.exe .
  move /Y launcher\\*.bat .
  move /Y launcher\\launcher.jar .
  move /Y launcher\\*.txt .

:end
if exist NEW\\jre rmdir /S /Q NEW\\jre
if exist NEW\\launcher rmdir /S /Q NEW\\launcher
start tutti.exe
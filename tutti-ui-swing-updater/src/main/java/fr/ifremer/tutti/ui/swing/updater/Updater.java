package fr.ifremer.tutti.ui.swing.updater;

/*
 * #%L
 * Tutti :: UI Updater
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import javax.swing.JOptionPane;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author Ludovic Pecquot (ludovic.pecquot@e-is.pro)
 * @author tony Chemit (chemit@codelutin.com)
 * @since 3.12
 */
public class Updater {

    public static final int NORMAL_EXIT_CODE = 0;

    public static final int ERROR_EXIT_CODE = 1;

    public static final int RUNTIME_UPDATE_EXIT_CODE = 90;

    private final UpdaterFileSystemPathes pathHelper;

    public static void main(String... args) {

        Updater updater = new Updater();
        int exitCode = updater.execute();
        System.exit(exitCode);

    }

    public Updater() {

        // Get the current directory where application has been launched
        Path baseDir = Paths.get(System.getProperty("user.dir"));
        if (!baseDir.isAbsolute()) {
            baseDir = baseDir.toFile().getAbsoluteFile().toPath();
        }
        System.out.println(String.format("Basedir: %s", baseDir));
        pathHelper = new UpdaterFileSystemPathes(baseDir);

    }

    public int execute() {

        System.out.println("updater started at " + new Date().toString());

        int exitCode;

        try {

            // before trying to update runtime modules
            beforeUpdateRuntimeModules();

            // update runtime modules
            boolean runtimeUpdate = updateRuntimeModules();

            if (runtimeUpdate) {

                // there is some runtime updates
                afterUpdateRuntimeModules();

                exitCode = RUNTIME_UPDATE_EXIT_CODE;

            } else {

                // update application modules
                updateNoneRuntimeModules();

                // clean files
                cleanFiles();

                exitCode = NORMAL_EXIT_CODE;

            }

        } catch (Exception ex) {

            ex.printStackTrace();
            exitCode = ERROR_EXIT_CODE;

        }

        System.out.println("updater ended   at " + new Date().toString() + " with exit code: " + exitCode);

        return exitCode;

    }

    protected void beforeUpdateRuntimeModules() throws IOException {

        Path runtimeUpdater = pathHelper.getUpdaterScriptPath();
        Files.deleteIfExists(runtimeUpdater);

    }

    protected boolean updateRuntimeModules() throws Exception {

        boolean mustUpdateRuntime = false;

        for (UpdateModule updateModule : UpdateModule.values()) {

            UpdateModuleConfiguration moduleConfiguration = updateModule.getModuleConfiguration();
            if (moduleConfiguration.isManageByUpdater() && moduleConfiguration.isRuntime()) {

                boolean updateFound = updateRuntimeModule(updateModule);

                if (updateFound) {
                    mustUpdateRuntime = true;
                }

            }

        }

        return mustUpdateRuntime;

    }

    protected void afterUpdateRuntimeModules() throws IOException {

        Path runtimeUpdater = pathHelper.getUpdaterScriptPath();

        URL resource = getClass().getResource("/" + runtimeUpdater.getFileName());

        try (InputStream stream = resource.openStream()) {

            Path tempFile = Files.createTempFile(runtimeUpdater.getFileName().toFile().getName(), null);
            Files.copy(stream, tempFile, StandardCopyOption.REPLACE_EXISTING);
            tempFile.toFile().deleteOnExit();

            Charset charset = Charset.forName("UTF-8");
            String content = new String(Files.readAllBytes(tempFile), charset);

            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd_HH_mm_ss");
            content = content.replaceFirst("~~BACKUP_DATE~~", dateFormat.format(new Date()));

            Files.write(runtimeUpdater, content.getBytes());

        }

        pathHelper.makeExecutable(runtimeUpdater);

        String message = String.format("Une mise à jour de l'exécutable est disponible." +
                                       "\nVous devez lancer le script '%s' en double cliquant dessus dans un explorateur de fichier." +
                                       "\nLe script se trouve dans le dossier %s.",
                                       runtimeUpdater.getFileName(),
                                       runtimeUpdater.toFile().getParentFile());
        System.out.println(message);
        JOptionPane.showMessageDialog(null, message);

    }

    protected void updateNoneRuntimeModules() throws IOException {

        for (UpdateModule updateModule : UpdateModule.values()) {

            UpdateModuleConfiguration moduleConfiguration = updateModule.getModuleConfiguration();

            if (moduleConfiguration.isManageByUpdater() && !moduleConfiguration.isRuntime()) {

                updateNoneRuntimeModule(updateModule);

            }
        }

    }

    protected boolean updateRuntimeModule(UpdateModule updateModule) throws IOException {

        boolean moduleExist = pathHelper.isModuleExists(updateModule);
        boolean updateModuleExist = pathHelper.isUpdateModuleExists(updateModule);

        String oldVersion = moduleExist ? pathHelper.getModuleVersion(updateModule) : "None";

        String moduleNameStr = updateModule.getModuleLoggerName();

        System.out.println(String.format("%s Current version: %s", moduleNameStr, oldVersion));

        if (updateModuleExist) {

            String newVersion = pathHelper.getUpdateModuleVersion(updateModule);

            System.out.println(String.format("%s New version detected %s", moduleNameStr, newVersion));

            // Remove older backup
            pathHelper.removeOlderBackup(updateModule);

        } else {

            System.out.println(String.format("%s No update found", moduleNameStr));

        }

        return updateModuleExist;

    }

    protected void updateNoneRuntimeModule(UpdateModule updateModule) throws IOException {

        boolean moduleExist = pathHelper.isModuleExists(updateModule);
        boolean updateModuleExist = pathHelper.isUpdateModuleExists(updateModule);

        String oldVersion = moduleExist ? pathHelper.getModuleVersion(updateModule) : "None";

        String moduleNameStr = updateModule.getModuleLoggerName();

        System.out.println(String.format("%s Current version: %s", moduleNameStr, oldVersion));

        if (updateModuleExist) {

            String newVersion = pathHelper.getUpdateModuleVersion(updateModule);

            System.out.println(String.format("%s New version detected %s", moduleNameStr, newVersion));

            pathHelper.removeOlderBackup(updateModule);

            if (moduleExist) {

                // Backup existing module
                pathHelper.backupModule(updateModule, oldVersion);

            }

            // Installing new module
            System.out.println(String.format("%s Install new version %s", moduleNameStr, newVersion));
            Path modulePath = pathHelper.getModulePath(updateModule);
            Path moduleNewPath = pathHelper.getUpdateModulePath(updateModule);

            try {

                pathHelper.move(moduleNewPath, modulePath);

            } catch (IOException e) {

                String message = String.format("La mise à jour du module '%s' a échouée.\nVeuillez contacter un administrateur.", updateModule);
                System.out.println(message);
                JOptionPane.showMessageDialog(null, message);

                e.printStackTrace();

            }

        } else {

            System.out.println(String.format("%s No update found", moduleNameStr));

        }

    }

    protected void cleanFiles() throws IOException {

        pathHelper.cleanObsoleteFiles();
        DeleteHelper.deleteDirectory(pathHelper.getUpdateDirectory());

    }

}

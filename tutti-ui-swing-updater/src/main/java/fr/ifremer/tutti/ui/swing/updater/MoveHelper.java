package fr.ifremer.tutti.ui.swing.updater;

/*
 * #%L
 * Tutti :: UI Updater
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;

/**
 * Created on 4/15/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.13.8
 */
public class MoveHelper {


    public static void move(Path sourceDirectory, Path targetDirectory, boolean dryRun) throws IOException {

        MoveTreeVisitor copyVisitor = new MoveTreeVisitor(sourceDirectory, targetDirectory, dryRun);
        Files.walkFileTree(sourceDirectory, copyVisitor);

    }

    static class MoveTreeVisitor extends SimpleFileVisitor<Path> {

        final Path sourceDirectory;

        final int sourcePathCount;

        final Path targetDirectory;

        final boolean dryRun;

        MoveTreeVisitor(Path sourceDirectory, Path targetDirectory, boolean dryRun) {
            this.sourceDirectory = sourceDirectory;
            this.targetDirectory = targetDirectory;
            this.dryRun = dryRun;
            this.sourcePathCount = sourceDirectory.getNameCount();
        }

        @Override
        public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) throws IOException {

            Path target;

            if (sourceDirectory.equals(dir)) {

                target = targetDirectory;

            } else {

                Path sourceRelativize = dir.subpath(sourcePathCount, dir.getNameCount());
                target = targetDirectory.resolve(sourceRelativize).toAbsolutePath();

            }

            System.out.println(String.format("Create directory: %s", target));
            if (!dryRun) {
                Files.createDirectories(target);
            }

            return FileVisitResult.CONTINUE;

        }

        @Override
        public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {

            Path subPath = file.subpath(sourcePathCount, file.getNameCount());
            Path target = targetDirectory.resolve(subPath).normalize().toAbsolutePath();

            System.out.println(String.format("Copy file from %s to %s", file, target));
            if (!dryRun) {
                Files.move(file, target);
            }

            return FileVisitResult.CONTINUE;

        }

        @Override
        public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {

            System.out.println(String.format("Delete directory: %s", dir));
            if (!dryRun) {
                Files.delete(dir);
            }

            return FileVisitResult.CONTINUE;

        }

    }

}

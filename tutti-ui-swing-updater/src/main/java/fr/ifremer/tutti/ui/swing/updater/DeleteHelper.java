package fr.ifremer.tutti.ui.swing.updater;

/*
 * #%L
 * Tutti :: UI Updater
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.PathMatcher;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;

/**
 * Created on 1/26/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.12
 */
public class DeleteHelper {

    public static void deleteDirectories(Path path, String glob) throws IOException {

        PathMatcher matcher = path.getFileSystem().getPathMatcher("glob:" + glob);
        if (Files.isDirectory(path)) {

            DeleteDirectories deleteDirectories = new DeleteDirectories(matcher);
            Files.walkFileTree(path, deleteDirectories);
        }

    }

    public static void deleteFiles(Path path, String glob) throws IOException {
        if (Files.isDirectory(path)) {

            PathMatcher matcher = path.getFileSystem().getPathMatcher("glob:" + glob);
            DeleteFiles deleteFiles = new DeleteFiles(matcher);
            Files.walkFileTree(path, deleteFiles);
        }

    }

    public static void deleteDirectory(Path path) throws IOException {
        if (Files.isDirectory(path)) {
            DeleteDirectory deleteDirectory = new DeleteDirectory();
            Files.walkFileTree(path, deleteDirectory);
        }
    }

    public static void deleteDirectoryOnExit(Path path) throws IOException {
        if (Files.isDirectory(path)) {
            DeleteDirectoryOnExit deleteDirectory = new DeleteDirectoryOnExit();
            Files.walkFileTree(path, deleteDirectory);
        }
    }

    /**
     * To delete the given directory.
     */
    public static class DeleteDirectory extends SimpleFileVisitor<Path> {

        @Override
        public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
            Files.deleteIfExists(file);
            return FileVisitResult.CONTINUE;
        }

        @Override
        public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
            System.out.println("Delete directory: " + dir);
            Files.deleteIfExists(dir);
            return FileVisitResult.CONTINUE;
        }

    }

    /**
     * To delete the given directory on exit.
     */
    public static class DeleteDirectoryOnExit extends SimpleFileVisitor<Path> {

        @Override
        public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) throws IOException {
            System.out.println("Delete directory on exit: " + dir);
            dir.toFile().deleteOnExit();
            return FileVisitResult.CONTINUE;
        }

        @Override
        public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
            file.toFile().deleteOnExit();
            return FileVisitResult.CONTINUE;
        }

    }

    /**
     * To delete all files that are matching the given matcher.
     */
    public static class DeleteFiles extends SimpleFileVisitor<Path> {

        private final PathMatcher matcher;

        protected DeleteFiles(PathMatcher matcher) {
            this.matcher = matcher;
        }

        @Override
        public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {

            // If the file name matches the glob or if no matcher, delete the file
            if (matcher.matches(file.getFileName())) {
                System.out.println("Delete file: " + file);
                Files.deleteIfExists(file);
            }
            return FileVisitResult.CONTINUE;
        }

    }

    /**
     * To delete all directories which names are matching the given matcher.
     */
    public static class DeleteDirectories extends SimpleFileVisitor<Path> {

        private final PathMatcher matcher;

        protected DeleteDirectories(PathMatcher matcher) {
            this.matcher = matcher;
        }

        private Path deleteDir;

        @Override
        public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) throws IOException {

            if (deleteDir == null) {
                if (matcher.matches(dir.getFileName())) {
                    System.out.println("Delete directory: " + dir);
                    deleteDir = dir;
                }
            }
            return FileVisitResult.CONTINUE;

        }

        @Override
        public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {

            if (deleteDir != null) {
                Files.deleteIfExists(file);
            }
            return FileVisitResult.CONTINUE;

        }

        @Override
        public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {

            if (deleteDir != null) {

                Files.deleteIfExists(dir);

                if (dir.equals(deleteDir)) {
                    deleteDir = null;
                }

            }

            return FileVisitResult.CONTINUE;

        }

    }
}

package fr.ifremer.tutti.ui.swing.updater;

/*
 * #%L
 * Tutti :: UI Updater
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.nio.file.attribute.PosixFilePermission;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created on 1/27/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.13
 */
public class UpdaterFileSystemPathes {

    private static final String BACKUP_DIRECTORY_NAME = "OLD";

    private static final String UPDATE_DIRECTORY_NAME = "NEW";

    private static final String LAUNCHER_DIRECTORY_NAME = "launcher";

    private static final String EMBEDDED_DIRECTORY_NAME = "embedded";

    private static final String UPDATE_RUNTIME_CMD = "update_runtime";

    private static final String BATCH_WINDOWS_EXTENSION = ".bat";

    private static final String EXE_WINDOWS_EXTENSION = ".exe";

    private static final String BATCH_UNIX_EXTENSION = ".sh";

    private static final String VERSION_FILENAME = "version.appup";

    private final Path baseDir;

    private final boolean windowsOS;

    private final String backupDate;

    public UpdaterFileSystemPathes(Path baseDir) {
        this.baseDir = baseDir;
        this.windowsOS = System.getProperty("os.name").startsWith("Windows");
        this.backupDate = new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
    }

    public Path getUpdateDirectory() {
        return baseDir.resolve(UPDATE_DIRECTORY_NAME);
    }

    public Path getUpdateModulePath(UpdateModule updateModule) {
        return getUpdateDirectory().resolve(updateModule.name());
    }

    public boolean isUpdateModuleExists(UpdateModule updateModule) {

        Path updateModulePath = getUpdateModulePath(updateModule);
        return Files.isDirectory(updateModulePath);

    }

    public String getUpdateModuleVersion(UpdateModule updateModule) throws IOException {

        Path updateModulePath = getUpdateModulePath(updateModule);
        return getVersion(updateModulePath);

    }

    public Path getModulePath(UpdateModule updateModule) {
        return baseDir.resolve(updateModule.name());
    }

    public boolean isModuleExists(UpdateModule updateModule) {

        Path modulePath = getModulePath(updateModule);
        return Files.isDirectory(modulePath);

    }

    public String getModuleVersion(UpdateModule updateModule) throws IOException {

        Path modulePath = getModulePath(updateModule);
        return getVersion(modulePath);

    }

    public Path getUpdaterScriptPath() {

        String scriptFilename = UPDATE_RUNTIME_CMD + (windowsOS ? BATCH_WINDOWS_EXTENSION : BATCH_UNIX_EXTENSION);
        return baseDir.resolve(scriptFilename);

    }

    public void cleanObsoleteFiles() throws IOException {

        Path applicationDirectoryPath = getModulePath(UpdateModule.tutti);

        if (windowsOS) {

            // Delete linux files
            DeleteHelper.deleteFiles(baseDir, "*" + BATCH_UNIX_EXTENSION);

        } else {

            // Delete Windows files
            DeleteHelper.deleteFiles(baseDir, "*" + BATCH_WINDOWS_EXTENSION);
            DeleteHelper.deleteFiles(baseDir, "*" + EXE_WINDOWS_EXTENSION);

        }

        // Delete embedded files
        DeleteHelper.deleteFiles(applicationDirectoryPath, "*" + BATCH_UNIX_EXTENSION);
        DeleteHelper.deleteFiles(applicationDirectoryPath, "*" + BATCH_WINDOWS_EXTENSION);
        DeleteHelper.deleteFiles(applicationDirectoryPath, "*" + EXE_WINDOWS_EXTENSION);
        DeleteHelper.deleteDirectory(applicationDirectoryPath.resolve(LAUNCHER_DIRECTORY_NAME));
        DeleteHelper.deleteDirectory(applicationDirectoryPath.resolve(EMBEDDED_DIRECTORY_NAME));

    }

    public void makeExecutable(Path path) throws IOException {

        if (!windowsOS) {

            Set<PosixFilePermission> perms = new HashSet<>();
            //add owners permission
            perms.add(PosixFilePermission.OWNER_READ);
            perms.add(PosixFilePermission.OWNER_WRITE);
            perms.add(PosixFilePermission.OWNER_EXECUTE);
            //add group permissions
            perms.add(PosixFilePermission.GROUP_READ);
            perms.add(PosixFilePermission.GROUP_WRITE);
            perms.add(PosixFilePermission.GROUP_EXECUTE);
            //add others permissions
            perms.add(PosixFilePermission.OTHERS_READ);
            perms.add(PosixFilePermission.OTHERS_EXECUTE);

            Files.setPosixFilePermissions(path, perms);

        }

    }

    public void removeOlderBackup(UpdateModule updateModule) throws IOException {

        String moduleName = updateModule.name();

        Path backupDirectory = getBackupDirectory();

        // Remove older backup
        System.out.println(String.format("%s Clean backup directory %s", updateModule.getModuleLoggerName(), backupDirectory + File.separator + moduleName + "-*"));
        DeleteHelper.deleteDirectories(backupDirectory, moduleName + "-*");

    }

    public void backupModule(UpdateModule updateModule, String version) throws IOException {

        Path modulePath = getModulePath(updateModule);

        String moduleName = updateModule.name();

        Path backupDirectory = getBackupDirectory();

        Path backupModulePath = backupDirectory.resolve(String.format("%s-%s-%s", moduleName, version, backupDate));

        System.out.println(String.format("%s Backup old version %s from %s to %s", updateModule.getModuleLoggerName(), version, modulePath, backupModulePath));
        move(modulePath, backupModulePath);

    }

    public void move(Path source, Path target) throws IOException {

        Path absoluteSourcePath = source.toAbsolutePath();
        Path absoluteTargetPath = target.toAbsolutePath();

        try {

            System.out.println(String.format("Try to move from %s to %s", absoluteSourcePath, absoluteTargetPath));
            Files.move(absoluteSourcePath, absoluteTargetPath, StandardCopyOption.ATOMIC_MOVE);

        } catch (IOException e) {

            // copy atomic impossible
            System.out.println(String.format("Try fallback install (copy then delete, atomic move is not possible from %s to %s)", source, target));

            MoveHelper.move(absoluteSourcePath, absoluteTargetPath, false);

        }

    }

    private Path getBackupDirectory() throws IOException {

        Path backupDirectory = baseDir.resolve(BACKUP_DIRECTORY_NAME);
        if (!Files.isDirectory(backupDirectory)) {
            Files.createDirectory(backupDirectory);
        }

        return backupDirectory;

    }

    private String getVersion(Path path) throws IOException {

        // Return the version of a module from version.appup file
        Path versionFile = path.resolve(VERSION_FILENAME);
        List<String> lines = Files.readAllLines(versionFile, StandardCharsets.UTF_8);
        if (lines == null || lines.isEmpty()) {
            throw new IOException(versionFile.toString() + " is empty");
        }
        return lines.get(0);
    }

}

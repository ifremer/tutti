package fr.ifremer.tutti.service.catches.multipost.csv;

/*
 * #%L
 * Tutti :: Service
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.Date;

/**
 * @author Kevin Morin - kmorin@codelutin.com
 * @since 2.3.1
 */
public abstract class AbstractFishingOperationRow {

    public static final String STATION_NUMBER = "stationNumber";

    public static final String OPERATION_NUMBER = "operationNumber";

    public static final String MULTIRIG_AGGREGATION = "multirigAggregation";

    public static final String DATE = "date";

    protected String stationNumber;

    protected Integer operationNumber;

    protected String multirigAggregation;

    protected Date date;

    public String getStationNumber() {
        return stationNumber;
    }

    public void setStationNumber(String stationNumber) {
        this.stationNumber = stationNumber;
    }

    public Integer getOperationNumber() {
        return operationNumber;
    }

    public void setOperationNumber(Integer operationNumber) {
        this.operationNumber = operationNumber;
    }

    public String getMultirigAggregation() {
        return multirigAggregation;
    }

    public void setMultirigAggregation(String multirigAggregation) {
        this.multirigAggregation = multirigAggregation;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}

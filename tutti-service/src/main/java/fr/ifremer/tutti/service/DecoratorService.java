package fr.ifremer.tutti.service;

/*
 * #%L
 * Tutti :: Service
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Sets;
import fr.ifremer.tutti.LabelAware;
import fr.ifremer.tutti.persistence.entities.data.Attachment;
import fr.ifremer.tutti.persistence.entities.data.Cruise;
import fr.ifremer.tutti.persistence.entities.data.FishingOperation;
import fr.ifremer.tutti.persistence.entities.data.Program;
import fr.ifremer.tutti.persistence.entities.data.SampleCategoryModelEntry;
import fr.ifremer.tutti.persistence.entities.protocol.CaracteristicType;
import fr.ifremer.tutti.persistence.entities.protocol.Rtp;
import fr.ifremer.tutti.persistence.entities.protocol.SpeciesProtocol;
import fr.ifremer.tutti.persistence.entities.protocol.TuttiProtocol;
import fr.ifremer.tutti.persistence.entities.protocol.Zone;
import fr.ifremer.tutti.persistence.entities.referential.Caracteristic;
import fr.ifremer.tutti.persistence.entities.referential.CaracteristicQualitativeValue;
import fr.ifremer.tutti.persistence.entities.referential.Gear;
import fr.ifremer.tutti.persistence.entities.referential.Person;
import fr.ifremer.tutti.persistence.entities.referential.Species;
import fr.ifremer.tutti.persistence.entities.referential.TuttiLocation;
import fr.ifremer.tutti.persistence.entities.referential.Vessel;
import org.apache.commons.lang3.StringUtils;
import org.nuiton.decorator.Decorator;
import org.nuiton.decorator.DecoratorProvider;

import java.io.File;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Comparator;

import static org.nuiton.i18n.I18n.n;
import static org.nuiton.i18n.I18n.t;

/**
 * Tutti decorator service.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 0.1
 */
public class DecoratorService extends AbstractTuttiService {

    public static final String CARACTERISTIC_WITH_UNIT = "withUnit";

    public static final String CARACTERISTIC_PARAMETER_ONLY_WITH_UNIT = "parameterOnlyWithUnit";

    public static final String CARACTERISTIC_PARAMETER_ONLY = "parameterOnly";

    public static final String WITH_SURVEY_CODE = "withSurveyCode";

    public static final String WITH_SURVEY_CODE_NO_NAME = "withSurveyCodeNoName";

    public static final String FROM_PROTOCOL = "fromProtocol";

    public static final String GEAR_WITH_RANK_ORDER = "gearWithrankOrder";

    public static final String ONLY_NAME = "onlyName";

    public static final String FILE_NAME_COMPATIBLE = "fileNameCompatible";

    public static final String SPACE_EVERY_3_DIGIT = "spaceEvery3Digit";

    public static final String MATURITY = "maturity";

    public static final String NULL_INFINITE = "nullInfinite";

    public static final String SEPARATOR = "#";

    public static final Comparator<FishingOperation> FISHING_OPERATION_COMPARATOR_BY_GEAR_SHOOTING_START_DATE = (o1, o2) -> o1.getGearShootingStartDate().compareTo(o2.getGearShootingStartDate());


    /** Delegate decorator provider. */
    protected DecoratorProvider decoratorProvider;

    public <O> Decorator<O> getDecorator(O object) {
        return decoratorProvider.getDecorator(object);
    }

    public <O> Decorator<O> getDecorator(O object, String name) {
        return decoratorProvider.getDecorator(object, name);
    }

    public <O> Decorator<O> getDecoratorByType(Class<O> type) {
        return decoratorProvider.getDecoratorByType(type);
    }

    public <O> Decorator<O> getDecoratorByType(Class<O> type, String name) {
        return decoratorProvider.getDecoratorByType(type, name);
    }

    @Override
    public void setServiceContext(TuttiServiceContext context) {
        super.setServiceContext(context);

        decoratorProvider = new DecoratorProvider() {
            @Override
            protected void loadDecorators() {

                registerTuttiDecorator(SampleCategoryModelEntry.class, "${label}$s", SEPARATOR, " - ");
                registerTuttiDecorator(TuttiLocation.class, "${label}$s#${name}$s", SEPARATOR, " - ");
                registerTuttiDecorator(Cruise.class, "${name}$s", SEPARATOR, " - ");
                registerTuttiDecorator(TuttiProtocol.class, "${name}$s", SEPARATOR, " - ");
                registerTuttiDecorator(Rtp.class, "${a}$s#${b}$s", SEPARATOR, " - ");
                registerTuttiDecorator(Zone.class, "${label}$s", SEPARATOR, " - ");
                registerDecorator(new FishingOperationDecorator());
                registerTuttiDecorator(FishingOperation.class, FILE_NAME_COMPATIBLE, "${stationNumber}$s#${fishingOperationNumber}$s#${multirigAggregation}$s#${gearShootingStartDate}$td%4$tm%4$tY", SEPARATOR, "-");
                registerTuttiDecorator(Gear.class, "${label}$s#${name}$s", SEPARATOR, " - ");
                registerTuttiDecorator(Gear.class, GEAR_WITH_RANK_ORDER, "${rankOrder}$d#${label}$s#${name}$s", SEPARATOR, " - ");
                registerTuttiDecorator(Person.class, "${firstName}$s#${lastName}$s#${department}$s", SEPARATOR, " ");
                registerTuttiDecorator(Caracteristic.class, "${parameterName}$s#${matrixName}$s#${fractionName}$s#${methodName}$s", SEPARATOR, " - ");
                registerTuttiDecorator(Caracteristic.class, CARACTERISTIC_PARAMETER_ONLY, "${parameterName}$s", SEPARATOR, " - ");
                registerDecorator(CARACTERISTIC_PARAMETER_ONLY_WITH_UNIT, new SimpleCaracteristicDecorator("${parameterName}$s"));

                registerTuttiDecorator(CaracteristicQualitativeValue.class, "${description}$s", SEPARATOR, " - ");
                registerDecorator(new Decorator<CaracteristicType>(CaracteristicType.class) {
                    @Override
                    public String toString(Object bean) {
                        return bean == null ? "" : t("tutti.caracteristicType." + bean.toString());
                    }
                });
                registerTuttiDecorator(SpeciesProtocol.class, "${speciesReferenceTaxonId}", SEPARATOR, " - ");
                registerTuttiDecorator(Attachment.class, "${name}$s", SEPARATOR, " - ");
                registerTuttiDecorator(LabelAware.class, "${label}$s", SEPARATOR, " - ");
                registerTuttiDecorator(File.class, "${name}$s", SEPARATOR, " - ");
                registerDecorator(new Decorator<Float>(Float.class) {
                    private static final long serialVersionUID = 1L;

                    @Override
                    public String toString(Object bean) {
                        return bean == null ? "" : String.valueOf(bean);
                    }
                });
                registerDecorator(new Decorator<Number>(Number.class) {
                    private static final long serialVersionUID = 1L;

                    @Override
                    public String toString(Object bean) {
                        return bean == null ? "" : String.valueOf(bean);
                    }
                });
                registerDecorator(SPACE_EVERY_3_DIGIT, new SpaceEvery3DigitDecorator());
                registerDecorator(NULL_INFINITE, new Decorator<Integer>(Integer.class) {
                    private static final long serialVersionUID = 1L;

                    @Override
                    public String toString(Object bean) {
                        return bean == null ? t("tutti.decorator.null.infinite") : String.valueOf(bean);
                    }
                });
                registerDecorator(MATURITY, new MaturityDecorator());
                registerDecorator(new VesselDecorator());
                registerDecorator(new ProgramDecorator());
                registerTuttiDecorator(Program.class, ONLY_NAME, "${name}$s", SEPARATOR, " - ");

                registerDecorator(new SpeciesDecorator());
                registerDecorator(WITH_SURVEY_CODE, new SpeciesDecoratorWithSurveyCode(true));
                registerDecorator(WITH_SURVEY_CODE_NO_NAME, new SpeciesDecoratorWithSurveyCode(false));
                registerDecorator(FROM_PROTOCOL, new SpeciesFromProtocolDecorator());
                registerDecorator(CARACTERISTIC_WITH_UNIT, new SimpleCaracteristicDecorator("${parameterName}$s#${matrixName}$s#${fractionName}$s#${methodName}$s"));

                TuttiDecorator<FishingOperation> fishingOperationTuttiDecorator = (TuttiDecorator<FishingOperation>) getDecoratorByType(FishingOperation.class);
                fishingOperationTuttiDecorator.setSortOnlyOnSelectedContext(true);
                fishingOperationTuttiDecorator.setSortOnlyOnSelectedContextTokens(Sets.newHashSet(FishingOperation.PROPERTY_FISHING_OPERATION_NUMBER));
            }

            public void registerTuttiDecorator(Class<?> klass, String expression, String separator, String separatorReplacement) {
                super.registerDecorator(TuttiDecorator.newDecorator(klass, expression, separator, separatorReplacement));
            }

            public void registerTuttiDecorator(Class<?> klass, String name, String expression, String separator, String separatorReplacement) {
                super.registerDecorator(name, TuttiDecorator.newDecorator(klass, expression, separator, separatorReplacement));
            }
        };
    }

    static {
        n("tutti.property.protocol");
        n("tutti.property.label");
        n("tutti.property.name");
        n("tutti.property.person");
        n("tutti.property.firstName");
        n("tutti.property.lastName");
        n("tutti.property.department");
        n("tutti.property.stationNumber");
        n("tutti.property.fishingOperationNumber");
        n("tutti.property.gearShootingStartDate");
        n("tutti.property.internationalRegistrationCode");
        n("tutti.property.date");
        n("tutti.property.program");
        n("tutti.property.parameterName");
        n("tutti.property.matrixName");
        n("tutti.property.fractionName");
        n("tutti.property.methodName");
        n("tutti.property.cruise");
        n("tutti.property.fishingOperation");
        n("tutti.property.fishingOperationLocation");
        n("tutti.property.tuttiLocation");
        n("tutti.property.zone");
        n("tutti.property.vessel");
        n("tutti.property.country");
        n("tutti.property.gear");
        n("tutti.property.user");
        n("tutti.property.strata");
        n("tutti.property.refTaxCode");
        n("tutti.property.surveyCode");
        n("tutti.property.rankOrder");
        n("tutti.property.species");
        n("tutti.property.genusSpecies");
        n("tutti.property.sortedUnsortedCategory");
        n("tutti.property.marineLitterCategory");
        n("tutti.property.attachment");
        n("tutti.property.multirigAggregation");
        n("tutti.property.caracteristic");
        n("tutti.caracteristicType.GEAR_USE_FEATURE");
        n("tutti.caracteristicType.INDIVIDUAL_OBSERVATION");
        n("tutti.caracteristicType.lengthStep");
        n("tutti.caracteristicType.VESSEL_USE_FEATURE");
    }

    public static class SpeciesDecorator extends TuttiDecorator<Species> implements Cloneable {

        private static final long serialVersionUID = 1L;

        public SpeciesDecorator() throws IllegalArgumentException, NullPointerException {
            super(Species.class, "${refTaxCode}$s#${name}$s", DecoratorService.SEPARATOR, " - ");
        }

        @Override
        protected Object onNullValue(Species bean, String token) {
            Object result = null;
            if (Species.PROPERTY_REF_TAX_CODE.equals(token)) {
                result = t("tutti.propety.no.species.speciesCode");
            }
            return result;
        }
    }

    public static class SpeciesDecoratorWithSurveyCode extends TuttiDecorator<Species> implements Cloneable {

        private static final long serialVersionUID = 1L;

        public SpeciesDecoratorWithSurveyCode() throws IllegalArgumentException, NullPointerException {
            this(true);
        }

        public SpeciesDecoratorWithSurveyCode(boolean showName) throws IllegalArgumentException, NullPointerException {
            super(Species.class, showName ? "${surveyCode}$s#${name}$s" : "${surveyCode}$s", DecoratorService.SEPARATOR, " - ");
        }

        @Override
        protected Object onNullValue(Species bean, String token) {
            Object result = null;
            if (Species.PROPERTY_SURVEY_CODE.equals(token)) {

                // try with refTaxCode
                result = bean.getRefTaxCode();

                if (result == null) {

                    result = t("tutti.propety.no.species.speciesCode");
                }
            }
            return result;
        }
    }

    public static class SimpleCaracteristicDecorator extends TuttiDecorator<Caracteristic> implements Cloneable {

        private static final long serialVersionUID = 1L;

        public SimpleCaracteristicDecorator(String firstPart) throws IllegalArgumentException, NullPointerException {
            super(Caracteristic.class, firstPart + "#${unit}$s", DecoratorService.SEPARATOR, " ");
        }

        @Override
        protected Object getValue(Caracteristic bean, String token) {
            Object result = super.getValue(bean, token);

            if (Caracteristic.PROPERTY_UNIT.equals(token) &&
                StringUtils.isNotBlank((String) result)) {
                result = "(" + result + ")";
            }
            return result;
        }
    }

    public static class SpeciesFromProtocolDecorator extends TuttiDecorator<Species> implements Cloneable {

        private static final long serialVersionUID = 1L;

        public SpeciesFromProtocolDecorator() throws IllegalArgumentException, NullPointerException {
            super(Species.class, "${surveyCode}$s#${name}$s", DecoratorService.SEPARATOR, " - ");
        }

        @Override
        protected Object getValue(Species bean, String token) {
            Object result = super.getValue(bean, token);
            if (Species.PROPERTY_SURVEY_CODE.equals(token) && result == null) {
                result = bean.getRefTaxCode();
            }
            return result;
        }

        @Override
        protected Object onNullValue(Species bean, String token) {
            Object result;
            if (Species.PROPERTY_SURVEY_CODE.equals(token)) {
                result = t("tutti.propety.no.species.speciesCode");

            } else {
                result = super.onNullValue(bean, token);
            }
            return result;
        }
    }

    public static class ProgramDecorator extends TuttiDecorator<Program> implements Cloneable {

        private static final long serialVersionUID = 1L;

        public ProgramDecorator() throws IllegalArgumentException, NullPointerException {
            super(Program.class, "${name}$s#${zone}$s", DecoratorService.SEPARATOR, " - ");
        }

        @Override
        protected Object getValue(Program bean, String token) {
            Object result = super.getValue(bean, token);
            if (Program.PROPERTY_ZONE.equals(token) && result != null) {
                result = ((TuttiLocation) result).getLabel();
            }
            return result;
        }

        @Override
        protected Object onNullValue(Program bean, String token) {
            Object result = null;
            if (Program.PROPERTY_ZONE.equals(token)) {
                result = t("tutti.propety.no.zone");
            }
            return result;
        }
    }

    public static class VesselDecorator extends TuttiDecorator<Vessel> implements Cloneable {

        private static final long serialVersionUID = 1L;

        public VesselDecorator() throws IllegalArgumentException, NullPointerException {
            super(Vessel.class, "${internationalRegistrationCode}$s#${name}$s", DecoratorService.SEPARATOR, " - ");
        }

        @Override
        protected Object getValue(Vessel bean, String token) {
            // handle the null internationalRegistrationCode here,
            // otherwise this will be considered null during sorting
            Object result = super.getValue(bean, token);
            if (Vessel.PROPERTY_INTERNATIONAL_REGISTRATION_CODE.equals(token) && result == null) {
                // use national registration code
                result = t("tutti.propety.vessel.nation.registrationCode", bean.getRegistrationCode());
            }
            return result;
        }

        @Override
        protected Object onNullValue(Vessel bean, String token) {
            Object result = null;
            if (Vessel.PROPERTY_NAME.equals(token)) {
                result = t("tutti.propety.no.vessel.name");
            }
            return result;
        }
    }

    public static class FishingOperationDecorator extends TuttiDecorator<FishingOperation> implements Cloneable {

        private static final long serialVersionUID = 1L;

        public FishingOperationDecorator() throws IllegalArgumentException, NullPointerException {
            super(FishingOperation.class, "${stationNumber}$s#${fishingOperationNumber}$d#${multirigAggregation}$s#${gearShootingStartDate}$td/%4$tm/%4$tY", SEPARATOR, " - ");
        }

        @Override
        protected Comparator<FishingOperation> getComparator(int pos) {
            Comparator<FishingOperation> comparator;
            if (pos == 3) {
                comparator = FISHING_OPERATION_COMPARATOR_BY_GEAR_SHOOTING_START_DATE;
            } else {
                comparator = super.getComparator(pos);
            }
            return comparator;
        }

        @Override
        protected Object onNullValue(FishingOperation bean, String token) {
            Object result = null;
            if (Vessel.PROPERTY_NAME.equals(token)) {
                result = t("tutti.propety.no.vessel.name");
            }
            return result;
        }
    }

    public static class SpaceEvery3DigitDecorator extends Decorator<String> implements Cloneable {

        DecimalFormat df;

        public SpaceEvery3DigitDecorator() throws NullPointerException {
            super(String.class);
            DecimalFormatSymbols symbols = new DecimalFormatSymbols();
            symbols.setGroupingSeparator(' '); //espace insécable
            df = new DecimalFormat("###,###", symbols);
        }

        @Override
        public String toString(Object bean) {
            String s = (String) bean;
            try {
                int i = Integer.parseInt(s);
                return df.format(i);

            } catch (NumberFormatException e) {
                return "";
            }
        }
    }

    public static class MaturityDecorator extends Decorator<Boolean> {

        protected MaturityDecorator() {
            super(Boolean.class);
        }

        @Override
        public String toString(Object bean) {
            String result;

            if (bean == null) {
                result = "";

            } else {

                Boolean maturity = (Boolean) bean;
                result = maturity ? t("tutti.maturity.mature") : t("tutti.maturity.immature");
            }

            return result;
        }
    }
}

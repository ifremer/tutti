package fr.ifremer.tutti.service.referential.consumer;

/*
 * #%L
 * Tutti :: Service
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.entities.referential.Person;
import fr.ifremer.tutti.persistence.entities.referential.Persons;
import fr.ifremer.tutti.service.DecoratorService;
import fr.ifremer.tutti.service.PersistenceService;
import fr.ifremer.tutti.service.csv.CsvComsumer;
import fr.ifremer.tutti.service.referential.ReferentialImportRequest;
import fr.ifremer.tutti.service.referential.csv.PersonModel;
import fr.ifremer.tutti.service.referential.csv.PersonRow;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.csv.ImportRow;
import org.nuiton.jaxx.application.ApplicationBusinessException;

import java.nio.file.Path;

import static org.nuiton.i18n.I18n.t;

/**
 * Created on 2/11/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.14
 */
public class CsvConsumerForTemporaryPerson extends CsvComsumer<PersonRow, PersonModel> {

    /** Logger. */
    private static final Log log = LogFactory.getLog(CsvConsumerForTemporaryPerson.class);

    public CsvConsumerForTemporaryPerson(Path file, char separator, boolean reportError) {
        super(file, PersonModel.forImport(separator), reportError);
    }

    public void checkRow(ImportRow<PersonRow> row,
                         PersistenceService persistenceService,
                         DecoratorService decoratorService,
                         ReferentialImportRequest<Person, Integer> requestResult) {

        if (row.isValid()) {

            PersonRow bean = row.getBean();

            Integer id = bean.getIdAsInt();
            boolean delete = BooleanUtils.isTrue(bean.getToDelete());

            if (id == null) {

                // Ajout
                checkAdd(bean, requestResult);

            } else {

                // Mise à jour ou Suppression

                Person person = requestResult.getExistingEntityById(id);

                if (person == null) {
                    throw new ApplicationBusinessException(t("tutti.service.referential.import.person.error.notExistingId", id));
                }

                if (delete) {

                    // Suppression
                    checkDelete(bean, person, persistenceService, decoratorService, requestResult);

                } else {

                    // Mise à jour
                    checkUpdate(bean, person, requestResult);

                }
            }

        }

        reportError(row);

    }

    public void checkRowForGenericFormatImport(ImportRow<PersonRow> row, ReferentialImportRequest<Person, Integer> requestResult) {

        PersonRow bean = row.getBean();
        String name = bean.getFullName();

        if (row.isValid()) {

            Integer id = bean.getIdAsInt();

            if (id == null) {

                addCheckError(row, new ApplicationBusinessException(t("tutti.service.referential.import.person.error.noId")));

            } else if (!Persons.isTemporaryId(id)) {

                addCheckError(row, new ApplicationBusinessException(t("tutti.service.referential.import.person.error.idNotTemporary", id)));

            } else if (requestResult.isIdAlreadyAdded(id)) {

                addCheckError(row, new ApplicationBusinessException(t("tutti.service.referential.import.person.error.id.alreaydAdded", id)));

            }

            if (StringUtils.isBlank(name)) {

                addCheckError(row, new ApplicationBusinessException(t("tutti.service.referential.import.person.error.noName")));

            } else if (requestResult.isNaturalIdAlreadyAdded(name)) {

                addCheckError(row, new ApplicationBusinessException(t("tutti.service.referential.import.person.error.name.alreaydAdded", name)));

            }

        }

        reportError(row);

        if (row.isValid()) {

            Person entity = bean.toEntity();
            boolean toAdd = requestResult.addExistingNaturalId(name);
            if (toAdd) {

                if (log.isInfoEnabled()) {
                    log.info("Will add person with name: " + name);
                }
                requestResult.addEntityToAdd(entity);

            } else {

                if (log.isInfoEnabled()) {
                    log.info("Will link person with name: " + name);
                }
                requestResult.addEntityToLink(entity);
            }

        }

    }

    protected void checkAdd(PersonRow bean, ReferentialImportRequest<Person, Integer> requestResult) {

        String name = bean.getFullName();
        boolean delete = BooleanUtils.isTrue(bean.getToDelete());

        if (delete) {
            throw new ApplicationBusinessException(t("tutti.service.referential.import.person.error.cannotDeleteWithoutId"));
        }

        if (StringUtils.isBlank(name)) {
            throw new ApplicationBusinessException(t("tutti.service.referential.import.person.error.noName"));
        }

        if (!requestResult.addExistingNaturalId(name)) {
            throw new ApplicationBusinessException(t("tutti.service.referential.import.person.error.existingName", name));
        }

        if (log.isInfoEnabled()) {
            log.info("Will add person with name: " + name);
        }

        requestResult.addEntityToAdd(bean.toEntity());

    }

    protected void checkDelete(PersonRow bean, Person person, PersistenceService persistenceService,
                               DecoratorService decoratorService,
                               ReferentialImportRequest<Person, Integer> requestResult) {

        Integer id = bean.getIdAsInt();
        String name = bean.getFullName();

        if (persistenceService.isTemporaryPersonUsed(id)) {

            String personRef = id + " : " + decoratorService.getDecoratorByType(Person.class).toString(person);
            throw new ApplicationBusinessException(t("tutti.service.referential.import.person.error.used", personRef));
        }

        if (log.isInfoEnabled()) {
            log.info("Will delete person with name: " + name);
        }

        requestResult.addIdToDelete(id);
        requestResult.removeExistingNaturalId(name);

    }

    protected void checkUpdate(PersonRow bean, Person person, ReferentialImportRequest<Person, Integer> requestResult) {

        Integer id = bean.getIdAsInt();
        String name = bean.getFullName();

        if (StringUtils.isBlank(name)) {
            throw new ApplicationBusinessException(t("tutti.service.referential.import.person.error.noName", id));
        }

        String previousFullName = Persons.getFullName(person);
        if (!previousFullName.equals(name) && !requestResult.addExistingNaturalId(name)) {
            throw new ApplicationBusinessException(t("tutti.service.referential.import.person.error.existingName", name));
        }

        if (log.isInfoEnabled()) {
            log.info("Will update person with name: " + name);
        }

        requestResult.addEntityToUpdate(bean.toEntity());

    }

}
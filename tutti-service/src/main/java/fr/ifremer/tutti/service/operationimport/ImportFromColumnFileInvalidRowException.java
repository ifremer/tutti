package fr.ifremer.tutti.service.operationimport;

/*
 * #%L
 * Tutti :: Service
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.entities.data.FishingOperation;
import org.nuiton.csv.AbstractImportErrorInfo;

import java.util.Set;

/**
 * When the row to match in fishing operation import from column file is not valid.
 *
 * Created on 12/11/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.10
 */
public class ImportFromColumnFileInvalidRowException extends Exception {

    private static final long serialVersionUID = 1L;

    private final Set<AbstractImportErrorInfo<FishingOperation>> errors;

    public ImportFromColumnFileInvalidRowException(Set<AbstractImportErrorInfo<FishingOperation>> errors) {
        this.errors = errors;
    }

    public Set<AbstractImportErrorInfo<FishingOperation>> getErrors() {
        return errors;
    }
}

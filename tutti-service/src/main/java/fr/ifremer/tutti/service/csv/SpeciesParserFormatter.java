package fr.ifremer.tutti.service.csv;

/*
 * #%L
 * Tutti :: Service
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.entities.referential.Species;
import fr.ifremer.tutti.persistence.entities.referential.Speciess;
import fr.ifremer.tutti.service.PersistenceService;
import org.nuiton.csv.ImportRuntimeException;

import java.util.List;
import java.util.Map;

/**
 * Created on 2/14/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.14
 */
public class SpeciesParserFormatter extends EntityParserFormatterSupport<Species> {

    public static SpeciesParserFormatter newFormatter() {
        return new SpeciesParserFormatter(false, null, null);
    }

    public static SpeciesParserFormatter newTechnicalFormatter() {
        return new SpeciesParserFormatter(true, null, null);
    }

    public static SpeciesParserFormatter newParser(PersistenceService persistenceService, Map<Integer, Integer> referenceTaxonIdTranslationMap) {
        return new SpeciesParserFormatter(true, persistenceService, referenceTaxonIdTranslationMap);
    }

    private final PersistenceService persistenceService;

    private final Map<Integer, Integer> referenceTaxonIdTranslationMap;

    protected SpeciesParserFormatter(boolean technical, PersistenceService persistenceService, Map<Integer, Integer> referenceTaxonIdTranslationMap) {
        super("", technical, Species.class);
        this.persistenceService = persistenceService;
        this.referenceTaxonIdTranslationMap = referenceTaxonIdTranslationMap;
    }

    @Override
    protected List<Species> getEntities() {
        return persistenceService.getAllReferentSpecies();
    }

    @Override
    protected List<Species> getEntitiesWithObsoletes() {
        return persistenceService.getAllReferentSpeciesWithObsoletes();
    }

    protected Map<String, Species> getEntitiesById() {
        if (entitiesById == null) {

            List<Species> entities = getEntities();
            entitiesById = Speciess.splitReferenceSpeciesByReferenceTaxonId(entities);

        }
        return entitiesById;
    }

    @Override
    protected String formatBusiness(Species value) {
        return value.getName();
    }

    @Override
    protected String formatTechnical(Species value) {
        return Speciess.GET_REFERECE_TAXON_ID.apply(value);
    }

    @Override
    protected Species parseNotBlankValue(String value) {

        Integer referenceTaxonId;
        try {
            referenceTaxonId = Integer.valueOf(value);
        } catch (NumberFormatException e) {
            throw new ImportRuntimeException("Le format du Code taxon n'est pas valide, cela devrait être un entier: " + value);
        }

        if (referenceTaxonIdTranslationMap.containsKey(referenceTaxonId)) {
            value = String.valueOf(referenceTaxonIdTranslationMap.get(referenceTaxonId));
        }
        return super.parseNotBlankValue(value);

    }

}

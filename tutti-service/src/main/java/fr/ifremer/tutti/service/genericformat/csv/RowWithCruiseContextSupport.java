package fr.ifremer.tutti.service.genericformat.csv;

/*
 * #%L
 * Tutti :: Service
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.entities.data.Cruise;
import fr.ifremer.tutti.persistence.entities.data.Cruises;
import fr.ifremer.tutti.persistence.entities.data.Program;

import java.io.Serializable;
import java.util.Date;

/**
 * Created on 2/20/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.14
 */
public class RowWithCruiseContextSupport implements Serializable {

    private static final long serialVersionUID = 1L;

    private Cruise cruise;

    public void forImport() {
        setCruise(Cruises.newCruise());
    }

    public void setCruise(Cruise cruise) {
        this.cruise = cruise;
    }

    public void setSurveyPart(String surveyPart) {
        cruise.setSurveyPart(surveyPart);
    }

    public void setBeginDate(Date beginDate) {
        cruise.setBeginDate(beginDate);
    }

    public void setProgram(Program program) {
        cruise.setProgram(program);
    }

    public Cruise getCruise() {
        return cruise;
    }

    public Date getBeginDate() {
        return cruise.getBeginDate();
    }

    public Program getProgram() {
        return cruise.getProgram();
    }

    public String getSurveyPart() {
        return cruise.getSurveyPart();
    }

}

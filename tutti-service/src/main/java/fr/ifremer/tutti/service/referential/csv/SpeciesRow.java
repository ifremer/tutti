package fr.ifremer.tutti.service.referential.csv;

/*
 * #%L
 * Tutti :: Service
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import fr.ifremer.tutti.persistence.entities.referential.Species;
import fr.ifremer.tutti.persistence.entities.referential.Speciess;

/**
 * @author Kevin Morin (Code Lutin)
 * @since 3.8
 */
public class SpeciesRow {

    public static final String PROPERTY_ID = "id";

    public static final String PROPERTY_REFERENCE_TAXON_ID = "referenceTaxonId";

    public static final String PROPERTY_NAME = "name";

    public static final String PROPERTY_TO_DELETE = "toDelete";

    protected String id;

    protected Integer referenceTaxonId;

    protected String name;

    protected Boolean toDelete;

    public SpeciesRow() {
        super();
    }

    public SpeciesRow(Species species) {
        super();
        Preconditions.checkNotNull(species);
        setId(species.getId());
        setReferenceTaxonId(species.getReferenceTaxonId());

        setName(species.getName());
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getReferenceTaxonId() {
        return referenceTaxonId;
    }

    public void setReferenceTaxonId(Integer referenceTaxonId) {
        this.referenceTaxonId = referenceTaxonId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getToDelete() {
        return toDelete;
    }

    public void setToDelete(Boolean toDelete) {
        this.toDelete = toDelete;
    }

    public Species toEntity(Integer referenceTaxonId) {
        Species species = Speciess.newSpecies();
        species.setId(getId());
        species.setName(getName());
        species.setReferenceTaxonId(referenceTaxonId);
        return species;
    }

    public Integer getIdAsInt() {
        Integer idAsInt = null;
        if (id != null) {
            idAsInt = Integer.valueOf(id);
        }
        return idAsInt;
    }

}

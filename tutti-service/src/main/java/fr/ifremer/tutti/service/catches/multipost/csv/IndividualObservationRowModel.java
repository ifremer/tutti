package fr.ifremer.tutti.service.catches.multipost.csv;

/*
 * #%L
 * Tutti :: Service
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.entities.data.CopyIndividualObservationMode;
import fr.ifremer.tutti.persistence.entities.referential.Caracteristic;
import fr.ifremer.tutti.persistence.entities.referential.Species;
import fr.ifremer.tutti.service.catches.multipost.MultiPostConstants;
import fr.ifremer.tutti.service.csv.AbstractTuttiImportExportModel;
import fr.ifremer.tutti.service.csv.TuttiCsvUtil;

import java.util.List;

/**
 * Model of a individual observation export.
 *
 * @author Kevin Morin - kmorin@codelutin.com
 * @since 2.2
 */
public class IndividualObservationRowModel extends AbstractTuttiImportExportModel<IndividualObservationRow> {

    private IndividualObservationRowModel(List<Species> species, List<Caracteristic> lengthStepCaracteristics) {
        super(MultiPostConstants.CSV_SEPARATOR);

        newColumnForImportExport(IndividualObservationRow.BATCH_ID);
        newColumnForImportExport(IndividualObservationRow.SPECIES_BATCH_ID);

        newColumnForExport(IndividualObservationRow.SPECIES, TuttiCsvUtil.SPECIES_TECHNICAL_FORMATTER);
        newSpeciesForeignKeyColumn(IndividualObservationRow.SPECIES, species);

        newColumnForImportExport(IndividualObservationRow.WEIGHT, TuttiCsvUtil.WEIGHT_PARSER_FORMATTER);

        newColumnForImportExport(IndividualObservationRow.SIZE, TuttiCsvUtil.FLOAT);

        newColumnForExport(IndividualObservationRow.LENGTH_STEP_CARACTERISTIC, TuttiCsvUtil.CARACTERISTIC_TECHNICAL_FORMATTER);
        newForeignKeyColumn(IndividualObservationRow.LENGTH_STEP_CARACTERISTIC, Caracteristic.class, lengthStepCaracteristics);

        newColumnForImportExport(IndividualObservationRow.COMMENT);

        newColumnForImportExport(IndividualObservationRow.SAMPLING_CODE);

        newColumnForImportExport(IndividualObservationRow.COPY_INDIVIDUAL_OBSERVATION_MODE, TuttiCsvUtil.newEnumByNameParserFormatter(CopyIndividualObservationMode.class));

    }

    public static IndividualObservationRowModel forExport() {
        return new IndividualObservationRowModel(null, null);
    }

    public static IndividualObservationRowModel forImport(List<Species> species, List<Caracteristic> lengthStepCaracteristics) {
        return new IndividualObservationRowModel(species, lengthStepCaracteristics);
    }

    @Override
    public IndividualObservationRow newEmptyInstance() {
        return new IndividualObservationRow();
    }
}

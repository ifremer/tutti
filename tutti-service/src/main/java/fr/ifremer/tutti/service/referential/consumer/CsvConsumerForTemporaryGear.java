package fr.ifremer.tutti.service.referential.consumer;

/*
 * #%L
 * Tutti :: Service
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.entities.referential.Gear;
import fr.ifremer.tutti.persistence.entities.referential.Gears;
import fr.ifremer.tutti.service.DecoratorService;
import fr.ifremer.tutti.service.PersistenceService;
import fr.ifremer.tutti.service.csv.CsvComsumer;
import fr.ifremer.tutti.service.referential.ReferentialImportRequest;
import fr.ifremer.tutti.service.referential.csv.GearModel;
import fr.ifremer.tutti.service.referential.csv.GearRow;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.csv.ImportRow;
import org.nuiton.jaxx.application.ApplicationBusinessException;

import java.nio.file.Path;

import static org.nuiton.i18n.I18n.t;

/**
 * Created on 2/11/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.14
 */
public class CsvConsumerForTemporaryGear extends CsvComsumer<GearRow, GearModel> {

    /** Logger. */
    private static final Log log = LogFactory.getLog(CsvConsumerForTemporaryGear.class);

    public CsvConsumerForTemporaryGear(Path file, char separator, boolean failFast) {
        super(file, GearModel.forImport(separator), failFast);
    }

    public void checkRow(ImportRow<GearRow> row,
                         PersistenceService persistenceService,
                         DecoratorService decoratorService,
                         ReferentialImportRequest<Gear, Integer> requestResult) {

        if (row.isValid()) {

            GearRow bean = row.getBean();

            Integer id = bean.getIdAsInt();
            boolean delete = BooleanUtils.isTrue(bean.getToDelete());

            if (id == null) {

                // Ajout
                checkAdd(bean, requestResult);

            } else {

                // Mise à jour ou Suppression

                Gear gear = requestResult.getExistingEntityById(id);

                if (gear == null) {
                    throw new ApplicationBusinessException(t("tutti.service.referential.import.gear.error.notExistingId", id));
                }

                if (delete) {

                    // Suppression
                    checkDelete(bean, gear, persistenceService, decoratorService, requestResult);

                } else {

                    // Mise à jour
                    checkUpdate(bean, gear, requestResult);

                }
            }

        }

        reportError(row);

    }

    public void checkRowForGenericFormatImport(ImportRow<GearRow> row, ReferentialImportRequest<Gear, Integer> requestResult) {

        GearRow bean = row.getBean();
        String name = bean.getName();

        if (row.isValid()) {

            Integer id = bean.getIdAsInt();

            if (id == null) {

                // need an id. This id will be used in generic format import (but won't be necessary the one in db)
                addCheckError(row, new ApplicationBusinessException(t("tutti.service.referential.import.gear.error.noId")));

            } else if (!Gears.isTemporaryId(id)) {

                addCheckError(row, new ApplicationBusinessException(t("tutti.service.referential.import.gear.error.idNotTemporary", id)));

            }else if (requestResult.isIdAlreadyAdded(id)) {

                addCheckError(row, new ApplicationBusinessException(t("tutti.service.referential.import.gear.error.id.alreaydAdded", id)));

            }

            if (StringUtils.isBlank(name)) {

                addCheckError(row, new ApplicationBusinessException(t("tutti.service.referential.import.gear.error.noName")));

            } else if (requestResult.isNaturalIdAlreadyAdded(name)) {

                addCheckError(row, new ApplicationBusinessException(t("tutti.service.referential.import.gear.error.name.alreaydAdded", name)));

            }

        }

        reportError(row);

        if (row.isValid()) {

            Gear entity = bean.toEntity();
            boolean toAdd = requestResult.addExistingNaturalId(name);
            if (toAdd) {

                if (log.isInfoEnabled()) {
                    log.info("Will add gear with name: " + name);
                }
                requestResult.addEntityToAdd(entity);

            } else {

                if (log.isInfoEnabled()) {
                    log.info("Will link gear with name: " + name);
                }
                requestResult.addEntityToLink(entity);

            }

        }

    }

    protected void checkAdd(GearRow bean, ReferentialImportRequest<Gear, Integer> requestResult) {

        String name = bean.getName();
        boolean delete = BooleanUtils.isTrue(bean.getToDelete());

        if (delete) {
            throw new ApplicationBusinessException(t("tutti.service.referential.import.gear.error.cannotDeleteWithoutId"));
        }

        if (StringUtils.isBlank(name)) {
            throw new ApplicationBusinessException(t("tutti.service.referential.import.gear.error.noName"));
        }

        if (!requestResult.addExistingNaturalId(name)) {
            throw new ApplicationBusinessException(t("tutti.service.referential.import.gear.error.existingName", name));
        }

        if (log.isInfoEnabled()) {
            log.info("Will add gear with name: " + name);
        }

        requestResult.addEntityToAdd(bean.toEntity());

    }

    protected void checkDelete(GearRow bean, Gear gear, PersistenceService persistenceService,
                               DecoratorService decoratorService,
                               ReferentialImportRequest<Gear, Integer> requestResult) {

        Integer id = bean.getIdAsInt();
        String name = bean.getName();

        if (persistenceService.isTemporaryGearUsed(id)) {

            String gearRef = id + " : " + decoratorService.getDecoratorByType(Gear.class).toString(gear);
            throw new ApplicationBusinessException(t("tutti.service.referential.import.gear.error.used", gearRef));
        }

        requestResult.addIdToDelete(id);
        requestResult.removeExistingNaturalId(name);

        if (log.isInfoEnabled()) {
            log.info("Will delete gear with name: " + name);
        }

        requestResult.addEntityToAdd(bean.toEntity());

    }

    protected void checkUpdate(GearRow bean, Gear gear, ReferentialImportRequest<Gear, Integer> requestResult) {

        Integer id = bean.getIdAsInt();
        String name = bean.getName();

        if (StringUtils.isBlank(name)) {
            throw new ApplicationBusinessException(t("tutti.service.referential.import.gear.error.noName", id));
        }

        if (!name.equals(gear.getName()) && !requestResult.addExistingNaturalId(name)) {
            throw new ApplicationBusinessException(t("tutti.service.referential.import.gear.error.existingName", name));
        }

        if (log.isInfoEnabled()) {
            log.info("Will update gear with name: " + name);
        }

        requestResult.addEntityToUpdate(bean.toEntity());

    }
}
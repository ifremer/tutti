package fr.ifremer.tutti.service.csv;

/*
 * #%L
 * Tutti :: Service
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Charsets;
import com.google.common.base.Joiner;
import com.google.common.collect.ImmutableMap;
import com.google.common.io.Files;
import org.apache.commons.io.IOUtils;
import org.nuiton.csv.AbstractImportErrorInfo;
import org.nuiton.csv.Import2;
import org.nuiton.csv.ImportConf;
import org.nuiton.csv.ImportRow;
import org.nuiton.csv.ImportableColumn;
import org.nuiton.jaxx.application.ApplicationBusinessException;
import org.nuiton.jaxx.application.ApplicationTechnicalException;

import java.io.BufferedReader;
import java.io.Closeable;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Path;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import static org.nuiton.i18n.I18n.t;

/**
 * Created on 2/11/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.14
 */
public abstract class CsvComsumer<O, M extends AbstractTuttiImportExportModel<O>> implements Closeable, Iterable<ImportRow<O>> {

    private final BufferedReader reader;

    private final Import2<O> importer;

    private final boolean failFast;

    private final Map<Long, Set<String>> rowsInError;

    public CsvComsumer(Path file, M model, boolean failFast) {
        this.failFast = failFast;
        this.rowsInError = new LinkedHashMap<>();
        try {
            this.reader = Files.newReader(file.toFile(), Charsets.UTF_8);
        } catch (FileNotFoundException e) {
            // should never happen
            throw new ApplicationTechnicalException("file not found " + file, e);
        }

        ImportConf importConf = new ImportConf();
        importConf.setStrictMode(failFast);
        this.importer = Import2.newImport(importConf, model, reader);

    }

    @Override
    public Iterator<ImportRow<O>> iterator() {
        return this.importer.iterator();
    }

    @Override
    public void close() throws IOException {
        IOUtils.closeQuietly(reader);
        IOUtils.closeQuietly(importer);
    }

    public void addCheckError(ImportRow<O> row, Exception e) {

        row.addError(new CheckImportErrorInfo<>(row, null, e));

    }

    public String rowErrorsToExceptionMessage(ImportRow<O> bean) {

        Set<String> errors = new HashSet<>();
        for (AbstractImportErrorInfo<O> errorInfo : bean.getErrors()) {

            Throwable cause = errorInfo.getCause();

            if (errorInfo.getField() == null) {

                errors.add(cause.getMessage());

            } else {

                errors.add(t("tutti.csv.import.error.on.field", errorInfo.getField().getHeaderName(), cause.getMessage()));
            }

        }

        return t("tutti.csv.import.error.on.row", bean.getLineNumber(), Joiner.on("\n").join(errors));

    }


    public Set<String> rowErrorsToMessage(ImportRow<O> bean) {

        Set<String> errors = new HashSet<>();
        for (AbstractImportErrorInfo<O> errorInfo : bean.getErrors()) {

            Throwable cause = errorInfo.getCause();

            if (errorInfo.getField() == null) {

                errors.add(cause.getMessage());

            } else {

                errors.add(t("tutti.csv.import.error.on.field", errorInfo.getField().getHeaderName(), cause.getMessage()));
            }

        }

        return errors;

    }

    public int getNbRowsInErrors() {
        return rowsInError.size();
    }

    public Map<Long, Set<String>> getRowsInError() {
        return ImmutableMap.copyOf(rowsInError);
    }

    protected void reportError(ImportRow<O> row) {

        if (!row.isValid()) {


            if (failFast) {

                String message = rowErrorsToExceptionMessage(row);
                throw new ApplicationBusinessException(message);

            } else {

                rowsInError.put(row.getLineNumber(), rowErrorsToMessage(row));

            }

        }

    }

    public static class CheckImportErrorInfo<E> extends AbstractImportErrorInfo<E> {

        public CheckImportErrorInfo(ImportRow<E> row, ImportableColumn<E, Object> field, Throwable cause) {
            super(row, field, cause);
        }

    }
}

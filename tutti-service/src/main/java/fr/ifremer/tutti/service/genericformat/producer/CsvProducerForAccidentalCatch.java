package fr.ifremer.tutti.service.genericformat.producer;

/*
 * #%L
 * Tutti :: Service
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.entities.CaracteristicMap;
import fr.ifremer.tutti.persistence.entities.data.AccidentalBatch;
import fr.ifremer.tutti.persistence.entities.referential.Caracteristic;
import fr.ifremer.tutti.service.csv.CsvProducer;
import fr.ifremer.tutti.service.genericformat.GenericFormatExportOperationContext;
import fr.ifremer.tutti.service.genericformat.csv.AccidentalCatchModel;
import fr.ifremer.tutti.service.genericformat.csv.AccidentalCatchRow;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.MapUtils;

import java.io.Serializable;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created on 2/6/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.13
 */
public class CsvProducerForAccidentalCatch extends CsvProducer<AccidentalCatchRow, AccidentalCatchModel> {

    public CsvProducerForAccidentalCatch(Path file, AccidentalCatchModel model) {
        super(file, model);
    }

    public List<AccidentalCatchRow> getDataToExport(GenericFormatExportOperationContext operationExportContext) {

        List<AccidentalCatchRow> rows = new ArrayList<>();

        List<AccidentalBatch> accidentalBatches = operationExportContext.getAccidentalBatches();

        if (CollectionUtils.isNotEmpty(accidentalBatches)) {
            for (AccidentalBatch accidentalBatch : accidentalBatches) {

                addAccidentalBatch(operationExportContext, rows, accidentalBatch);

            }

        }

        return rows;

    }

    protected void addAccidentalBatch(GenericFormatExportOperationContext operationExportContext, List<AccidentalCatchRow> rows, AccidentalBatch accidentalBatch) {

        addCaracteristicRow(operationExportContext,
                            rows,
                            accidentalBatch,
                            operationExportContext.getDeadOrAliveCaracteristic(),
                            accidentalBatch.getDeadOrAlive());

        addCaracteristicRow(operationExportContext,
                            rows,
                            accidentalBatch,
                            operationExportContext.getGenderCaracteristic(),
                            accidentalBatch.getGender());

        addCaracteristicRow(operationExportContext,
                            rows,
                            accidentalBatch,
                            operationExportContext.getWeightMeasuredCaracteristic(),
                            accidentalBatch.getWeight());

        if (accidentalBatch.getLengthStepCaracteristic() != null) {
            addCaracteristicRow(operationExportContext,
                                rows,
                                accidentalBatch,
                                operationExportContext.getPmfmIdCaracteristic(),
                                accidentalBatch.getLengthStepCaracteristic().getIdAsInt());

            addCaracteristicRow(operationExportContext,
                                rows,
                                accidentalBatch,
                                accidentalBatch.getLengthStepCaracteristic(),
                                accidentalBatch.getSize());
        }

        CaracteristicMap caracteristics = accidentalBatch.getCaracteristics();
        if (MapUtils.isNotEmpty(caracteristics)) {
            for (Map.Entry<Caracteristic, Serializable> entry : caracteristics.entrySet()) {
                addCaracteristicRow(operationExportContext,
                                    rows,
                                    accidentalBatch,
                                    entry.getKey(),
                                    entry.getValue());
            }
        }

    }

    protected void addCaracteristicRow(GenericFormatExportOperationContext operationExportContext,
                                       List<AccidentalCatchRow> rows,
                                       AccidentalBatch accidentalBatch,
                                       Caracteristic caracteristic,
                                       Serializable caracteristicValue) {
        if (caracteristicValue != null) {

            AccidentalCatchRow row = new AccidentalCatchRow();
            row.setCruise(operationExportContext.getCruise());
            row.setFishingOperation(operationExportContext.getOperation());

            row.setComment(accidentalBatch.getComment());
            row.setSpecies(accidentalBatch.getSpecies());
            row.setBatchId(accidentalBatch.getIdAsInt());

            row.setCaracteristic(caracteristic);
            row.setCaracteristicValue(caracteristicValue);
            rows.add(row);

        }
    }

}

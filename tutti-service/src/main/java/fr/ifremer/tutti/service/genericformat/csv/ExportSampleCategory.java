package fr.ifremer.tutti.service.genericformat.csv;

/*
 * #%L
 * Tutti :: Service
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.entities.data.SampleCategory;
import fr.ifremer.tutti.persistence.entities.referential.Caracteristic;

import java.io.Serializable;

/**
 * Export sample category (extends {@link SampleCategory} to some computed
 * stuff.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.0
 */
public class ExportSampleCategory<S extends Serializable> extends SampleCategory<S> {

    private static final long serialVersionUID = 1L;

    public static final String PROPERTY_CATEGORY_CARACTERISTIC = "categoryCaracteristic";

    public static final String PROPERTY_NUMBER = "number";

    public static final String PROPERTY_COMPUTED_WEIGHT = "computedWeight";

    public static final String PROPERTY_COMPUTED_NUMBER = "computedNumber";

    public static final String PROPERTY_WEIGHT_OR_VOL_TYPE = "weightOrVolType";

    public static final String PROPERTY_RANK_ORDER = "rankOrder";

    public static final String PROPERTY_SAMPLE_WEIGHT = "sampleWeight";

    public static final String PROPERTY_BATCH_ID = "batchId";

    public static final String PROPERTY_COMMENT  = "comment";

    private Integer batchId;

    private Integer rankOrder;

    private String weightOrVolType;

    private Float sampleWeight;

    private Float sampleComputedWeight;

    private String comment;

    public String getWeightOrVolType() {
        return weightOrVolType;
    }

    public Integer getBatchId() {
        return batchId;
    }

    public void setBatchId(Integer batchId) {
        this.batchId = batchId;
    }

    public void setWeightOrVolType(String weightOrVolType) {
        this.weightOrVolType = weightOrVolType;
    }

    public Integer getRankOrder() {
        return rankOrder;
    }

    public void setRankOrder(Integer rankOrder) {
        this.rankOrder = rankOrder;
    }

    public Float getSampleWeight() {
        return sampleWeight;
    }

    public void setSampleWeight(Float sampleWeight) {
        this.sampleWeight = sampleWeight;
    }

    public Float getSampleComputedWeight() {
        return sampleComputedWeight;
    }

    public void setSampleComputedWeight(Float sampleComputedWeight) {
        this.sampleComputedWeight = sampleComputedWeight;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Caracteristic getCategoryCaracteristic() {
        return categoryDef.getCaracteristic();
    }

    public boolean isFilled() {
        return getCategoryValue() != null;
    }

}

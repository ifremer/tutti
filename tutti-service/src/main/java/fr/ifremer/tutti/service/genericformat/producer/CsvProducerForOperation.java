package fr.ifremer.tutti.service.genericformat.producer;

/*
 * #%L
 * Tutti :: Service
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.entities.data.CatchBatch;
import fr.ifremer.tutti.persistence.entities.data.Cruise;
import fr.ifremer.tutti.persistence.entities.data.FishingOperation;
import fr.ifremer.tutti.service.csv.CsvProducer;
import fr.ifremer.tutti.service.genericformat.csv.OperationModel;
import fr.ifremer.tutti.service.genericformat.csv.OperationRow;

import java.nio.file.Path;

/**
 * Created on 2/6/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.13
 */
public class CsvProducerForOperation extends CsvProducer<OperationRow, OperationModel> {

    public CsvProducerForOperation(Path file, OperationModel model) {
        super(file, model);
    }

    public OperationRow getDataToExport(Cruise cruise, FishingOperation operation, CatchBatch catchBatch) {

        OperationRow row = new OperationRow();
        row.setCruise(cruise);
        row.setFishingOperation(operation);
        row.setCatchBatch(catchBatch);

        return row;

    }

}

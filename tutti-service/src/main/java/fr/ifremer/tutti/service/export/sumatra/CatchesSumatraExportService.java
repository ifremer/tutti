package fr.ifremer.tutti.service.export.sumatra;

/*
 * #%L
 * Tutti :: Service
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Charsets;
import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import com.google.common.io.Files;
import fr.ifremer.tutti.persistence.ProgressionModel;
import fr.ifremer.tutti.persistence.entities.data.Cruise;
import fr.ifremer.tutti.persistence.entities.data.FishingOperation;
import fr.ifremer.tutti.persistence.entities.protocol.TuttiProtocol;
import fr.ifremer.tutti.persistence.entities.referential.Species;
import fr.ifremer.tutti.persistence.entities.referential.TaxonCache;
import fr.ifremer.tutti.persistence.entities.referential.TaxonCaches;
import fr.ifremer.tutti.service.AbstractTuttiService;
import fr.ifremer.tutti.service.PersistenceService;
import fr.ifremer.tutti.service.TuttiServiceContext;
import fr.ifremer.tutti.service.catches.WeightComputingService;
import fr.ifremer.tutti.service.export.ExportBatchEntry;
import fr.ifremer.tutti.service.export.ExportCatchContext;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.csv.Export;
import org.nuiton.jaxx.application.ApplicationTechnicalException;

import java.io.BufferedWriter;
import java.io.File;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.nuiton.i18n.I18n.t;

/**
 * @author Kevin Morin - kmorin@codelutin.com
 * @since 2.0
 */
public class CatchesSumatraExportService extends AbstractTuttiService {

    private static final Log log =
            LogFactory.getLog(CatchesSumatraExportService.class);

    protected PersistenceService persistenceService;

    protected WeightComputingService weightComputingService;

    @Override
    public void setServiceContext(TuttiServiceContext context) {
        super.setServiceContext(context);
        persistenceService = getService(PersistenceService.class);
        weightComputingService = getService(WeightComputingService.class);
    }

    /**
     * Export selected cruise with the csv sumatra format.
     *
     * @param file     where to generate report
     * @param cruiseId id of the cruise to export
     * @return export result
     * @since 2.0
     */
    public SumatraExportResult exportCruiseForSumatra(File file,
                                                      Integer cruiseId,
                                                      ProgressionModel progressionModel) {

        Preconditions.checkNotNull(cruiseId, "Cannot export a null cruise");
        Preconditions.checkNotNull(file, "Cannot export to a null file");

        if (log.isInfoEnabled()) {
            log.info("Will export cruise " + cruiseId + " to file: " + file);
        }

        progressionModel.increments(t("tutti.service.sumatra.export.step.load.cruise", cruiseId));

        Cruise cruise = persistenceService.getCruise(cruiseId);
        Preconditions.checkNotNull(cruise, "Cruise [" + cruiseId + "] not found");

        progressionModel.increments(t("tutti.service.sumatra.export.step.load.fishingOperationIds"));

        List<Integer> operations =
                persistenceService.getAllFishingOperationIds(cruiseId);

        if (log.isInfoEnabled()) {
            log.info(operations.size() + " operations found for cruise: " + cruiseId);
        }

        progressionModel.adaptTotal(operations.size() + 3);

        SumatraExportResult result = new SumatraExportResult();

        prepareOperationsAndExport(file, operations, progressionModel, result);

        return result;
    }

    /**
     * Export selected fishing operation with the csv sumatra format.
     *
     * @param file               where to generate report
     * @param cruiseId           id of the cruise to export
     * @param fishingOperationId id of the fishing operation to export
     * @return export result
     * @since 2.7
     */
    public SumatraExportResult exportFishingOperationForSumatra(File file,
                                                                Integer cruiseId,
                                                                Integer fishingOperationId,
                                                                ProgressionModel progressionModel) {

        Preconditions.checkNotNull(file, "Cannot export to a null file");
        Preconditions.checkNotNull(cruiseId, "Cannot export a null cruise");
        Preconditions.checkNotNull(fishingOperationId, "Cannot export a null fishing operation");

        if (log.isInfoEnabled()) {
            log.info("Will export fishing operation " +
                     fishingOperationId + " to file: " + file);
        }

        progressionModel.increments(t("tutti.service.sumatra.export.step.load.cruise", cruiseId));

        Cruise cruise = persistenceService.getCruise(cruiseId);
        Preconditions.checkNotNull(cruise, "Cruise [" + cruiseId + "] not found");

        List<Integer> operations = Lists.newArrayList(fishingOperationId);

        SumatraExportResult result = new SumatraExportResult();

        prepareOperationsAndExport(file, operations, progressionModel, result);

        return result;
    }

    protected void prepareOperationsAndExport(File file,
                                              List<Integer> operations,
                                              ProgressionModel progressionModel,
                                              SumatraExportResult result) {

        List<CatchRow> rows = Lists.newArrayList();

        CatchRowModel csvModel =
                new CatchRowModel(context.getConfig().getCsvSeparator());

        if (operations != null) {

            TuttiProtocol protocol = context.getDataContext().getProtocol();

            TaxonCache speciesCache = TaxonCaches.createSpeciesCacheWithoutVernacularCode(persistenceService, protocol);
            TaxonCache benthosCache = TaxonCaches.createBenthosCacheWithoutVernacularCode(persistenceService, protocol);

            for (Integer operationId : operations) {

                progressionModel.increments(t("tutti.service.sumatra.export.step.load.fishingOperation", operationId));
                if (!persistenceService.isFishingOperationWithCatchBatch(operationId)) {

                    if (log.isWarnEnabled()) {
                        log.warn("No catch for operation with id: " + operationId);
                    }
                    break;
                }

                prepareFishingOperation(speciesCache, benthosCache, csvModel, rows, operationId, result);

            }
        }

        progressionModel.increments(t("tutti.service.sumatra.export.step.export", file));

        BufferedWriter writer = null;
        try {
            writer = Files.newWriter(file, Charsets.UTF_8);
            Export export = Export.newExport(csvModel, rows);
            export.write(writer);
            writer.close();

        } catch (Exception e) {
            throw new ApplicationTechnicalException(t("tutti.service.sumatra.export.error", file), e);
        } finally {
            IOUtils.closeQuietly(writer);
        }
    }

    protected void prepareFishingOperation(TaxonCache speciesCache,
                                           TaxonCache benthosCache,
                                           CatchRowModel csvModel,
                                           List<CatchRow> rows,
                                           Integer operationId,
                                           SumatraExportResult result) {

        ExportCatchContext exportContext = ExportCatchContext.newExportContext(
                persistenceService,
                weightComputingService,
                operationId,
                true);

        FishingOperation operation = exportContext.getFishingOperation();

        // Species
        if (exportContext.withSpeciesBatches()) {

            List<ExportBatchEntry> entries = exportContext.getSpeciesBatchEntry(true);
            Set<String> badSpecies = prepareFishingOperationSpecies(speciesCache, entries, csvModel, rows, operation);
            result.setBadSpecies(badSpecies);

        }

        // Benthos
        if (exportContext.withBenthosBatches()) {

            List<ExportBatchEntry> entries = exportContext.getBenthosBatchEntry(true);
            Set<String> badBenthos = prepareFishingOperationBenthos(benthosCache, entries, csvModel, rows, operation);
            result.setBadBenthos(badBenthos);

        }
    }

    protected Set<String> prepareFishingOperationSpecies(TaxonCache taxonCache,
                                                         List<ExportBatchEntry> entries,
                                                         CatchRowModel csvModel,
                                                         List<CatchRow> rows,
                                                         FishingOperation operation) {

        Set<String> badSpecies = new HashSet<>();

        for (ExportBatchEntry entry : entries) {

            Species species = entry.getBatch().getSpecies();

            // load species codes
            taxonCache.load(species);

            if (StringUtils.isBlank(species.getSurveyCode())) {
                badSpecies.add(species.getName());
            }

            CatchRow row = csvModel.newRow(operation, entry);

            rows.add(row);

        }

        return badSpecies;

    }

    protected Set<String> prepareFishingOperationBenthos(TaxonCache taxonCache,
                                                         List<ExportBatchEntry> entries,
                                                         CatchRowModel csvModel,
                                                         List<CatchRow> rows,
                                                         FishingOperation operation) {

        Set<String> badBenthos = new HashSet<>();

        for (ExportBatchEntry entry : entries) {

            Species species = entry.getBatch().getSpecies();

            // load species codes
            taxonCache.load(species);
            if (StringUtils.isBlank(species.getSurveyCode())) {
                badBenthos.add(species.getName());
            }

            CatchRow row = csvModel.newRow(operation, entry);
            rows.add(row);

        }

        return badBenthos;

    }


}

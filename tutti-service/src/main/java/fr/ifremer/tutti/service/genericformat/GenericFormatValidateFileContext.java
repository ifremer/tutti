package fr.ifremer.tutti.service.genericformat;

/*
 * #%L
 * Tutti :: Service
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.ProgressionModel;
import fr.ifremer.tutti.persistence.entities.data.CatchBatch;
import fr.ifremer.tutti.persistence.entities.data.Cruise;
import fr.ifremer.tutti.persistence.entities.data.Cruises;
import fr.ifremer.tutti.persistence.entities.data.FishingOperation;
import fr.ifremer.tutti.persistence.entities.data.FishingOperations;
import fr.ifremer.tutti.persistence.model.CruiseDataModel;
import fr.ifremer.tutti.persistence.model.OperationDataModel;
import fr.ifremer.tutti.persistence.model.ProgramDataModel;
import fr.ifremer.tutti.service.PersistenceService;
import fr.ifremer.tutti.service.genericformat.importactions.RestoreAfterValidateAction;
import org.nuiton.decorator.Decorator;

import java.util.LinkedHashSet;
import java.util.Set;

/**
 * Created on 2/24/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.14
 */
public class GenericFormatValidateFileContext extends GenericFormatContextSupport {

    private final RestoreAfterValidateAction closeAction;

    public GenericFormatValidateFileContext(GenericFormatImportRequest importRequest,
                                            ProgressionModel progressionModel,
                                            PersistenceService persistenceService,
                                            Decorator<Cruise> cruiseDecorator,
                                            Decorator<FishingOperation> fishingOperationDecorator) {

        super(importRequest, progressionModel, persistenceService, cruiseDecorator, fishingOperationDecorator);
        this.closeAction = new RestoreAfterValidateAction(this, persistenceService);

    }

    @Override
    protected void onClose() {

        closeAction.execute();

    }

    public ProgramDataModel toDataModel() {

        final Set<CruiseDataModel> importedCruises = new LinkedHashSet<>();

        doActionOnSortedCruiseContexts((cruiseContext, progressionModel) -> {

            Cruise cruise = cruiseContext.getCruise();

            CruiseDataModel existingCruiseData = cruiseContext.getExistingCruiseData();

            Set<OperationDataModel> operations = new LinkedHashSet<>();

            for (GenericFormatImportOperationContext fishingOperationContext : cruiseContext.orderedFishingOperationContexts()) {
                FishingOperation fishingOperation = fishingOperationContext.getFishingOperation();

                // Add a natural Id as id (used in import to find which cruise to import)
                String naturalId = FishingOperations.getNaturalId(fishingOperation);
                fishingOperation.setId(naturalId);
                OperationDataModel operation = new OperationDataModel(fishingOperation);
                fishingOperation.setId((String) null);

                OperationDataModel existingFishingOperationData = fishingOperationContext.getExistingFishingOperationData();
                if (existingFishingOperationData != null) {
                    operation.setOptionalId(existingFishingOperationData.getId());
                }

                operations.add(operation);
            }

            // Add a natural Id as id (used in import to find which cruise to import)
            String naturalId = Cruises.getNaturalId(cruise);
            cruise.setId(naturalId);
            CruiseDataModel cruiseModel = new CruiseDataModel(cruise, operations);
            cruise.setId((String) null);

            if (existingCruiseData != null) {
                cruiseModel.setOptionalId(existingCruiseData.getId());
            }

            importedCruises.add(cruiseModel);

        });
        return new ProgramDataModel(getImportRequest().getProgram(), importedCruises);

    }

    public void addImportedFishingOperation(FishingOperation fishingOperation, CatchBatch catchBatch) {

        // add a temporary id to simulate persist behaviour
        fishingOperation.setId(getNextFishingOperationId());

        // add a temporary id to simulate persist behaviour
        catchBatch.setId(getNextCatchBatchId());

        super.addImportedFishingOperation(fishingOperation, catchBatch);

    }

    private int fishinOperationId = -1;

    private int catchBatchId = -1;

    private int getNextFishingOperationId() {
        return fishinOperationId--;
    }

    private int getNextCatchBatchId() {
        return catchBatchId--;
    }

}

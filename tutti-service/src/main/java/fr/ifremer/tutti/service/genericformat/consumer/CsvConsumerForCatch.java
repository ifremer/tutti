package fr.ifremer.tutti.service.genericformat.consumer;

/*
 * #%L
 * Tutti :: Service
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Predicate;
import fr.ifremer.adagio.core.dao.referential.pmfm.QualitativeValueId;
import fr.ifremer.tutti.persistence.entities.data.SampleCategoryModel;
import fr.ifremer.tutti.persistence.entities.data.SpeciesBatch;
import fr.ifremer.tutti.persistence.entities.data.SpeciesBatchFrequency;
import fr.ifremer.tutti.persistence.entities.data.SpeciesBatchFrequencys;
import fr.ifremer.tutti.persistence.entities.data.SpeciesBatchs;
import fr.ifremer.tutti.persistence.entities.referential.CaracteristicQualitativeValue;
import fr.ifremer.tutti.persistence.entities.referential.Species;
import fr.ifremer.tutti.service.csv.CsvComsumer;
import fr.ifremer.tutti.service.genericformat.GenericFormatContextSupport;
import fr.ifremer.tutti.service.genericformat.GenericFormatImportEntityParserFactory;
import fr.ifremer.tutti.service.genericformat.GenericFormatImportOperationContext;
import fr.ifremer.tutti.service.genericformat.csv.CatchModel;
import fr.ifremer.tutti.service.genericformat.csv.CatchRow;
import fr.ifremer.tutti.service.genericformat.csv.ExportSampleCategory;
import fr.ifremer.tutti.type.WeightUnit;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.csv.ImportRow;

import java.nio.file.Path;

/**
 * Created on 2/11/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.14
 */
public class CsvConsumerForCatch extends CsvComsumer<CatchRow, CatchModel> {

    /** Logger. */
    private static final Log log = LogFactory.getLog(CsvConsumerForCatch.class);

    private final Predicate<CatchRow> catchRowVracPredicate;

    public CsvConsumerForCatch(Path file, char separator, SampleCategoryModel sampleCategoryModel, GenericFormatImportEntityParserFactory parserFactory, boolean reportError) {

        super(file, CatchModel.forImport(separator, sampleCategoryModel, parserFactory), reportError);

        this.catchRowVracPredicate = input -> {
            ExportSampleCategory exportSampleCategory = input.getSampleCategory().get(0);
            return QualitativeValueId.SORTED_VRAC.getValue().equals(((CaracteristicQualitativeValue) exportSampleCategory.getCategoryValue()).getIdAsInt());
        };

    }

    public GenericFormatImportOperationContext validateRow(ImportRow<CatchRow> row, GenericFormatContextSupport importContext) {

        GenericFormatImportOperationContext operationContext = importContext.getValidationHelper().getFishingOperationContext(this, row, importContext);

        if (operationContext != null) {

            //TODO checks!
            CatchRow bean = row.getBean();

            for (ExportSampleCategory sampleCategory : bean.getSampleCategory()) {

                if (sampleCategory.isFilled()) {

                    if (sampleCategory.getCategoryValue() == null) {
                        //TODO
                    }
                }
            }

            if (bean.withFrequency()) {

                // Mandatory batchNumber (see https://forge.codelutin.com/issues/7058)

                if (bean.getBatchNumber() == null) {

                    addCheckError(row, new MissingBatchNumberForFrequencyException(operationContext.getFishingOperation()));

                }

                if (bean.getFrequencyWeight() != null && WeightUnit.KG.isZero(bean.getFrequencyWeight())) {

                    // Cant have a Weight 0.0
                    addCheckError(row, new FrequencyWeigthNullValueException(operationContext.getFishingOperation()));
                }
            }

        }

        reportError(row);

        return operationContext;

    }

    public void prepareRowForPersist(GenericFormatImportOperationContext operationContext, ImportRow<CatchRow> row) {

        CatchRow bean = row.getBean();

        boolean vrac = catchRowVracPredicate.apply(bean);
        bean.setVrac(vrac);

        boolean withFrequency = bean.withFrequency();

        if (bean.isBenthos()) {

            prepareBenthosRowForPersist(operationContext, bean, withFrequency);

        } else {

            prepareSpeciesRowForPersist(operationContext, bean, withFrequency);

        }

    }

    protected void prepareSpeciesRowForPersist(GenericFormatImportOperationContext operationContext, CatchRow bean, boolean withFrequency) {

        Species species = bean.getSpecies();
        boolean vrac = bean.isVrac();

        SpeciesBatch batch = operationContext.getSpeciesBatch(vrac, species.getReferenceTaxonId());

        if (batch == null) {

            // create root batch

            batch = SpeciesBatchs.newSpeciesBatch();
            batch.setId(getNextBatchId());
            batch.setSpecies(species);
            batch.setSpeciesToConfirm(bean.isSpeciesToConfirm());
            batch.setFishingOperation(operationContext.getFishingOperation());

            if (log.isInfoEnabled()) {
                log.info("Create species root batch (" + (vrac ? "VRAC" : "HORS VRAC") + ") batch species: " + species.getName() + " for " + operationContext.getFishingOperationLabel());
            }

            operationContext.addSpeciesBatch(vrac, batch);

        }

        batch = fillBatchCategories(operationContext, batch, bean);

        if (withFrequency) {

            SpeciesBatchFrequency frequency = SpeciesBatchFrequencys.newSpeciesBatchFrequency();
            frequency.setBatch(batch);
            frequency.setLengthStepCaracteristic(bean.getFrequencyLengthStepCaracteristic());
            frequency.setLengthStep(bean.getFrequencyLengthStep());
            frequency.setWeight(bean.getFrequencyWeight());
            frequency.setNumber(bean.getBatchNumber());

            if (log.isInfoEnabled()) {
                log.info("Create species frequency " + frequency.getNumber() + " for batch: " + batch.getSpecies().getName() + " - " + batch.getSampleCategoryId() + " - " + batch.getSampleCategoryValue());
            }

            operationContext.addSpeciesFrequency(batch, frequency);

        } else {

            batch.setNumber(bean.getBatchNumber());

        }
    }

    protected void prepareBenthosRowForPersist(GenericFormatImportOperationContext operationContext, CatchRow bean, boolean withFrequency) {

        Species species = bean.getSpecies();
        boolean vrac = bean.isVrac();

        SpeciesBatch batch = operationContext.getBenthosBatch(vrac, species.getReferenceTaxonId());

        if (batch == null) {

            // create root batch

            batch = SpeciesBatchs.newBenthosBatch();
            batch.setId(getNextBatchId());
            batch.setSpecies(species);
            batch.setSpeciesToConfirm(bean.isSpeciesToConfirm());
            batch.setFishingOperation(operationContext.getFishingOperation());

            if (log.isInfoEnabled()) {
                log.info("Create benthos root batch (" + (vrac ? "VRAC" : "HORS VRAC") + ") batch species: " + species.getName() + " for " + operationContext.getFishingOperationLabel());
            }

            operationContext.addBenthosBatch(vrac, batch);

        }

        batch = fillBatchCategories(operationContext, batch, bean);

        if (withFrequency) {

            SpeciesBatchFrequency frequency = SpeciesBatchFrequencys.newBenthosBatchFrequency();
            frequency.setBatch(batch);
            frequency.setLengthStepCaracteristic(bean.getFrequencyLengthStepCaracteristic());
            frequency.setLengthStep(bean.getFrequencyLengthStep());
            frequency.setWeight(bean.getFrequencyWeight());
            frequency.setNumber(bean.getBatchNumber());

            if (log.isInfoEnabled()) {
                log.info("Create benthos frequency " + frequency.getNumber() + " for batch: " + batch.getSpecies().getName() + " - " + batch.getSampleCategoryId() + " - " + batch.getSampleCategoryValue());
            }

            operationContext.addBenthosFrequency(batch, frequency);

        } else {

            batch.setNumber(bean.getBatchNumber());

        }

    }

    protected SpeciesBatch fillBatchCategories(GenericFormatImportOperationContext operationContext, SpeciesBatch batch, CatchRow bean) {

        for (ExportSampleCategory exportSampleCategory : bean.getFilledSampleCategories()) {
            batch = fillBatchCategories(operationContext, batch, exportSampleCategory);
        }

        return batch;

    }

    protected SpeciesBatch fillBatchCategories(GenericFormatImportOperationContext operationContext, SpeciesBatch batch, ExportSampleCategory sampleCategory) {

        SpeciesBatch result = null;

        if (batch.getSampleCategoryId() == null) {

            // category not filled in batch, fill it from the incoming category
            batch.setSampleCategoryId(sampleCategory.getCategoryId());
            batch.setSampleCategoryValue(sampleCategory.getCategoryValue());
            batch.setSampleCategoryWeight(sampleCategory.getCategoryWeight());
            batch.setWeight(sampleCategory.getSampleWeight());
            batch.setRankOrder(sampleCategory.getRankOrder());
            batch.setComment(sampleCategory.getComment());
            result = batch;
            operationContext.registerBatchObjectId(batch.getIdAsInt(), sampleCategory.getBatchId());

        } else {

            if (batch.getSampleCategoryId().equals(sampleCategory.getCategoryId()) &&
                    batch.getSampleCategoryValue().equals(sampleCategory.getCategoryValue())) {

                result = batch;

            } else if (!batch.isChildBatchsEmpty()) {

                // got some childs, try to find a matching one
                for (SpeciesBatch childBatch : batch.getChildBatchs()) {

                    if (childBatch.getSampleCategoryId().equals(sampleCategory.getCategoryId()) &&
                            childBatch.getSampleCategoryValue().equals(sampleCategory.getCategoryValue())) {

                        // matching child
                        result = childBatch;
                        break;
                    }
                }

            }

            if (result == null) {

                // add a child
                result = SpeciesBatchs.createNewChild(batch);
                result.setId(getNextBatchId());
                result.setRankOrder(sampleCategory.getRankOrder());
                result.setSampleCategoryId(sampleCategory.getCategoryId());
                result.setSampleCategoryValue(sampleCategory.getCategoryValue());
                result.setSampleCategoryWeight(sampleCategory.getCategoryWeight());
                result.setWeight(sampleCategory.getSampleWeight());
                result.setComment(sampleCategory.getComment());
                operationContext.registerBatchObjectId(result.getIdAsInt(), sampleCategory.getBatchId());
                if (log.isInfoEnabled()) {
                    log.info("Create child batch for batch: " + batch.getSpecies().getName() + " - " + batch.getSampleCategoryId() + " - " + batch.getSampleCategoryValue());
                }

            }

        }

        return result;

    }

    private int batchId;

    private int getNextBatchId() {
        return batchId++;
    }

}
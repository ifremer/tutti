package fr.ifremer.tutti.service.genericformat;

/*
 * #%L
 * Tutti :: Service
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Joiner;
import org.nuiton.jaxx.application.ApplicationBusinessException;

import java.util.Set;

import static org.nuiton.i18n.I18n.t;

/**
 * Created on 2/24/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.14
 */
public class GenericFormatArchiveInvalidLayoutException extends ApplicationBusinessException {

    private static final long serialVersionUID = 1L;

    private final GenericFormatArchive archive;

    private final Set<String> errors;

    public GenericFormatArchiveInvalidLayoutException(GenericFormatArchive archive, Set<String> errors) {
        super(t("tutti.service.genericFormat.importError.archiveNotSane", Joiner.on("\n").join(errors)));
        this.archive = archive;
        this.errors = errors;
    }

    public GenericFormatArchive getArchive() {
        return archive;
    }

    public Set<String> getErrors() {
        return errors;
    }

}

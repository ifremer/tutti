package fr.ifremer.tutti.service.genericformat.csv;

/*
 * #%L
 * Tutti :: Service
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.entities.data.Cruise;
import fr.ifremer.tutti.persistence.entities.data.FishingOperation;
import fr.ifremer.tutti.persistence.entities.data.SampleCategoryModel;
import fr.ifremer.tutti.persistence.entities.data.SampleCategoryModelEntry;
import fr.ifremer.tutti.persistence.entities.data.SpeciesBatch;
import fr.ifremer.tutti.persistence.entities.referential.Caracteristic;
import fr.ifremer.tutti.service.csv.AbstractTuttiImportExportModel;
import fr.ifremer.tutti.service.csv.CaracteristicValueParserFormatter;
import fr.ifremer.tutti.service.csv.TuttiCsvUtil;
import fr.ifremer.tutti.service.genericformat.GenericFormatImportEntityParserFactory;
import org.apache.commons.beanutils.PropertyUtils;
import org.nuiton.csv.ValueSetter;

import java.io.Serializable;

/**
 * Model of a catch export.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.3
 */
public class CatchModel extends AbstractTuttiImportExportModel<CatchRow> {

    public static final String WEIGHT_OR_VOL_TYPE = "Poids";

    public static CatchModel forExport(char separator, SampleCategoryModel sampleCategoryModel) {

        CatchModel exportModel = new CatchModel(separator);
        exportModel.forExport(sampleCategoryModel);
        return exportModel;

    }

    public static CatchModel forImport(char separator, SampleCategoryModel sampleCategoryModel, GenericFormatImportEntityParserFactory parserFactory) {

        CatchModel importModel = new CatchModel(separator);
        importModel.forImport(sampleCategoryModel, parserFactory);
        return importModel;

    }

    @Override
    public CatchRow newEmptyInstance() {

        CatchRow row = CatchRow.newEmptyInstance();
        row.forImport();
        return row;

    }

    protected CatchModel(char separator) {
        super(separator);
    }

    protected void forExport(SampleCategoryModel sampleCategoryModel) {

        newColumnForExport("Annee", Cruise.PROPERTY_BEGIN_DATE, TuttiCsvUtil.YEAR);
        newColumnForExport("Serie", Cruise.PROPERTY_PROGRAM, TuttiCsvUtil.PROGRAM_FORMATTER);
        newColumnForExport("Serie_Partielle", Cruise.PROPERTY_SURVEY_PART);

        newColumnForExport("Code_Station", FishingOperation.PROPERTY_STATION_NUMBER);
        newColumnForExport("Id_Operation", FishingOperation.PROPERTY_FISHING_OPERATION_NUMBER, TuttiCsvUtil.INTEGER);
        newColumnForExport("Poche", FishingOperation.PROPERTY_MULTIRIG_AGGREGATION);

        newColumnForExport("Code_Taxon", SpeciesBatch.PROPERTY_SPECIES, TuttiCsvUtil.SPECIES_TECHNICAL_FORMATTER);
        newColumnForExport("Code_Espece_Campagne", SpeciesBatch.PROPERTY_SPECIES, TuttiCsvUtil.SPECIES_SURVEY_CODE_FORMATTER);
        newColumnForExport("Nom_Scientifique", SpeciesBatch.PROPERTY_SPECIES, TuttiCsvUtil.SPECIES_FORMATTER);
        newColumnForExport("Benthos", CatchRow.PROPERTY_BENTHOS, TuttiCsvUtil.BOOLEAN);
        newColumnForExport("Lot_A_Confirmer", CatchRow.PROPERTY_SPECIES_TO_CONFIRM, TuttiCsvUtil.BOOLEAN);

        for (SampleCategoryModelEntry entry : sampleCategoryModel.getCategory()) {

            String headerPrefix = entry.getCode();
            int categoryOrder = entry.getOrder();
            String collectionName = CatchRow.SAMPLE_CATEGORY;
            newIndexNullableColumnForExport(headerPrefix, collectionName, categoryOrder, ExportSampleCategory.PROPERTY_CATEGORY_VALUE, TuttiCsvUtil.CARACTERISTIC_VALUE_FORMATTER);
            newIndexNullableColumnForExport("Num_Ordre_" + headerPrefix + "_H2", collectionName, categoryOrder, ExportSampleCategory.PROPERTY_RANK_ORDER, TuttiCsvUtil.PRIMITIVE_INTEGER);
            newIndexNullableColumnForExport("Tot_" + headerPrefix, collectionName, categoryOrder, ExportSampleCategory.PROPERTY_CATEGORY_WEIGHT, TuttiCsvUtil.FLOAT);
            newIndexNullableColumnForExport("Ech_" + headerPrefix, collectionName, categoryOrder, ExportSampleCategory.PROPERTY_SAMPLE_WEIGHT, TuttiCsvUtil.FLOAT);
            newIndexNullableColumnForExport("Type_Volume_Poids_" + headerPrefix, collectionName, categoryOrder, ExportSampleCategory.PROPERTY_WEIGHT_OR_VOL_TYPE);
            newNullableColumnForExport("Unite_Volume_Poids_" + headerPrefix, CatchRow.BATCH_WEIGHT_UNIT);
            newIndexNullableColumnForExport("Commentaire_" + headerPrefix, collectionName, categoryOrder, ExportSampleCategory.PROPERTY_COMMENT, TuttiCsvUtil.COMMENT_PARSER_FORMATTER);

        }

        // mensuration

        newColumnForExport("Code_Longueur", CatchRow.FREQUENCY_LENGTH_STEP_CARACTERISTIC, TuttiCsvUtil.CARACTERISTIC_TECHNICAL_FORMATTER);
        newColumnForExport("Libelle_Longueur", CatchRow.FREQUENCY_LENGTH_STEP_CARACTERISTIC, TuttiCsvUtil.CARACTERISTIC_FORMATTER);
        newColumnForExport("Taille", CatchRow.FREQUENCY_LENGTH_STEP, TuttiCsvUtil.FLOAT);
        newColumnForExport("NumOrdre_Taille_H2", CatchRow.FREQUENCY_RANK_ORDER, TuttiCsvUtil.INTEGER);
        newColumnForExport("Poids_Classe_Taille", CatchRow.FREQUENCY_WEIGHT, TuttiCsvUtil.FLOAT);
        newNullableColumnForExport("Unite_Taille", CatchRow.FREQUENCY_LENGTH_STEP_CARACTERISTIC + "." + Caracteristic.PROPERTY_UNIT);
        newNullableColumnForExport("Precision_Mesure", CatchRow.FREQUENCY_LENGTH_STEP_CARACTERISTIC + "." + Caracteristic.PROPERTY_PRECISION, TuttiCsvUtil.FLOAT);
        newColumnForExport("Nbr", CatchRow.BATCH_NUMBER, TuttiCsvUtil.INTEGER);
        newColumnForExport("Nbr_Calcule", CatchRow.BATCH_NUMBER_COMPUTED, TuttiCsvUtil.BOOLEAN);

        newColumnForExport("Poids_Reference", CatchRow.REFERENCE_WEIGHT, TuttiCsvUtil.PRIMITIVE_FLOAT);
        newColumnForExport("Coef_Elev_Espece_Capture", CatchRow.RAISING_FACTOR, TuttiCsvUtil.PRIMITIVE_FLOAT);
        newColumnForExport("Coef_Final_Elevation", CatchRow.FINAL_RAISING_FACTOR, TuttiCsvUtil.FLOAT);

        newColumnForExport("Serie_Id", Cruise.PROPERTY_PROGRAM, TuttiCsvUtil.PROGRAM_TECHNICAL_FORMATTER);
        for (SampleCategoryModelEntry entry : sampleCategoryModel.getCategory()) {

            String headerPrefix = entry.getCode();
            int categoryOrder = entry.getOrder();
            String collectionName = CatchRow.SAMPLE_CATEGORY;

            newIndexNullableColumnForExport(headerPrefix + "_Id", collectionName, categoryOrder, ExportSampleCategory.PROPERTY_CATEGORY_VALUE, TuttiCsvUtil.CARACTERISTIC_VALUE_TECHNICAL_FORMATTER);
            newIndexNullableColumnForExport(headerPrefix + "_Lot_Id", collectionName, categoryOrder, ExportSampleCategory.PROPERTY_BATCH_ID, TuttiCsvUtil.INTEGER);

        }

    }

    protected void forImport(SampleCategoryModel sampleCategoryModel, GenericFormatImportEntityParserFactory parserFactory) {

        newMandatoryColumn("Annee", Cruise.PROPERTY_BEGIN_DATE, TuttiCsvUtil.YEAR);
        newIgnoredColumn("Serie");
        newMandatoryColumn("Serie_Partielle", Cruise.PROPERTY_SURVEY_PART);

        newMandatoryColumn("Code_Station", FishingOperation.PROPERTY_STATION_NUMBER);
        newMandatoryColumn("Id_Operation", FishingOperation.PROPERTY_FISHING_OPERATION_NUMBER, TuttiCsvUtil.INTEGER);
        newMandatoryColumn("Poche", FishingOperation.PROPERTY_MULTIRIG_AGGREGATION);

        newMandatoryColumn("Code_Taxon", SpeciesBatch.PROPERTY_SPECIES, parserFactory.getSpeciesParser());
        newIgnoredColumn("Code_Espece_Campagne");
        newIgnoredColumn("Nom_Scientifique");
        newMandatoryColumn("Benthos", CatchRow.PROPERTY_BENTHOS, TuttiCsvUtil.BOOLEAN);
        newMandatoryColumn("Lot_A_Confirmer", CatchRow.PROPERTY_SPECIES_TO_CONFIRM, TuttiCsvUtil.BOOLEAN);

        for (SampleCategoryModelEntry entry : sampleCategoryModel.getCategory()) {

            String headerPrefix = entry.getCode();

            newIgnoredColumn(headerPrefix);
            newMandatoryColumn("Num_Ordre_" + headerPrefix + "_H2", TuttiCsvUtil.PRIMITIVE_INTEGER, new SampleCategoryValueSetter<>(entry, ExportSampleCategory.PROPERTY_RANK_ORDER));
            newMandatoryColumn("Tot_" + headerPrefix, TuttiCsvUtil.FLOAT, new SampleCategoryValueSetter<>(entry, ExportSampleCategory.PROPERTY_CATEGORY_WEIGHT));
            newMandatoryColumn("Ech_" + headerPrefix, TuttiCsvUtil.FLOAT, new SampleCategoryValueSetter<>(entry, ExportSampleCategory.PROPERTY_SAMPLE_WEIGHT));

            newIgnoredColumn("Type_Volume_Poids_" + headerPrefix);
            newIgnoredColumn("Unite_Volume_Poids_" + headerPrefix);

            newMandatoryColumn("Commentaire_" + headerPrefix, TuttiCsvUtil.COMMENT_PARSER_FORMATTER, new SampleCategoryValueSetter<>(entry, ExportSampleCategory.PROPERTY_COMMENT));

        }

        // mensuration

        newMandatoryColumn("Code_Longueur", CatchRow.FREQUENCY_LENGTH_STEP_CARACTERISTIC, parserFactory.getCaracteristicParser());
        newIgnoredColumn("Libelle_Longueur");
        newMandatoryColumn("Taille", CatchRow.FREQUENCY_LENGTH_STEP, TuttiCsvUtil.FLOAT);
        newMandatoryColumn("NumOrdre_Taille_H2", CatchRow.FREQUENCY_RANK_ORDER, TuttiCsvUtil.INTEGER);
        newMandatoryColumn("Poids_Classe_Taille", CatchRow.FREQUENCY_WEIGHT, TuttiCsvUtil.FLOAT);
        newIgnoredColumn("Unite_Taille");
        newIgnoredColumn("Precision_Mesure");
        newMandatoryColumn("Nbr", CatchRow.BATCH_NUMBER, TuttiCsvUtil.INTEGER);
        newMandatoryColumn("Nbr_Calcule", CatchRow.BATCH_NUMBER_COMPUTED, TuttiCsvUtil.BOOLEAN);

        newIgnoredColumn("Poids_Reference");
        newIgnoredColumn("Coef_Elev_Espece_Capture");
        newIgnoredColumn("Coef_Final_Elevation");

        newMandatoryColumn("Serie_Id", Cruise.PROPERTY_PROGRAM, parserFactory.getProgramParser());
        for (SampleCategoryModelEntry entry : sampleCategoryModel.getCategory()) {

            String headerPrefix = entry.getCode();
            newMandatoryColumn(headerPrefix + "_Id", CaracteristicValueParserFormatter.newParser(entry.getCaracteristic()), new SampleCategoryValueSetter<>(entry, ExportSampleCategory.PROPERTY_CATEGORY_VALUE));
            newMandatoryColumn(headerPrefix + "_Lot_Id", TuttiCsvUtil.INTEGER, new SampleCategoryValueSetter<>(entry, ExportSampleCategory.PROPERTY_BATCH_ID));
        }

    }

    private static class SampleCategoryValueSetter<O> implements ValueSetter<CatchRow, O> {

        private final SampleCategoryModelEntry sampleCategoryModelEntry;

        private final String propertyName;

        public SampleCategoryValueSetter(SampleCategoryModelEntry sampleCategoryModelEntry, String propertyName) {
            this.sampleCategoryModelEntry = sampleCategoryModelEntry;
            this.propertyName = propertyName;
        }

        @Override
        public void set(CatchRow row, O value) throws Exception {

            ExportSampleCategory sampleCategory = row.getSampleCategory(sampleCategoryModelEntry);
            PropertyUtils.setProperty(sampleCategory, propertyName, value);

        }
    }

}

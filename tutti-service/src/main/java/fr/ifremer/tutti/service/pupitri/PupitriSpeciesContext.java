package fr.ifremer.tutti.service.pupitri;

/*
 * #%L
 * Tutti :: Service
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ImmutableSet;
import fr.ifremer.tutti.persistence.entities.referential.Species;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.TreeMap;

/**
 * Pour aggreger toutes les lignes lors d'un import pupitri ayant la même tuple (espece - sorted).
 *
 * @author Kevin Morin - kmorin@codelutin.com
 * @since 1.2
 */
public class PupitriSpeciesContext implements Serializable {

    private static final long serialVersionUID = 1L;

    private static final String MELAG_META_SPECIES = "MELA-NGE";

    private static final String MELAG_2_META_SPECIES = "MELANGE";

    /**
     * All MELAG species.
     *
     * @since 3.7
     */
    private static final Set<String> MELAG_SPECIES = ImmutableSet.of(MELAG_META_SPECIES, MELAG_2_META_SPECIES);

    private static final Set<Signs> DEFAULT_SIGNS = ImmutableSet.of(Signs.DEFAULT);

    private static final Set<Signs> UNSORTED_SIGNS = ImmutableSet.of(Signs.UNSORTED);

    private static final Set<Signs> SEX_SIGNS = ImmutableSet.of(Signs.DEFAULT, Signs.MALE, Signs.FEMALE);

    private static final Set<Signs> SIZE_SIGNS = ImmutableSet.of(Signs.SMALL, Signs.MEDIUM, Signs.BIG);

    /**
     * Espece du lot.
     */
    protected final Species species;

    /**
     * Is the species is a sorted (VRAC) or unsorted (HORS-VRAC)?
     */
    protected final boolean sorted;

    /**
     * If the species was involved in the melag, fill this flag.
     *
     * If species has also a {@link Signs#BIG} weight, then the melga weight is imported as a {@link Signs#SMALL} batch.
     * Otherwise using the {@link Signs#DEFAULT} sign.
     *
     * We will look after this sign while crzating species batch to import.
     */
    protected Signs melagElevatedSign;

    /**
     * Tous les signes utilisés par cette espèce/sorted lors de l'import.
     *
     * @since 3.11
     */
    protected final TreeMap<Signs, PupitriSignContext> signs;

    /**
     * Pour créer les lots fils manquants (see http://forge.codelutin.com/issues/6116)
     * @since 4.3
     */
    protected final boolean createMissingSigns;

    public PupitriSpeciesContext(Species species, boolean createMissingSigns, boolean sorted) {
        this.species = species;
        this.sorted = sorted;
        this.signs = new TreeMap<>();
        this.createMissingSigns = createMissingSigns;
    }

    public Species getSpecies() {
        return species;
    }

    public boolean isSorted() {
        return sorted;
    }

    public boolean isAddMelagComment(Signs sign) {
        return melagElevatedSign != null && melagElevatedSign.equals(sign);
    }

    public boolean isForMelag() {
        return signs.containsKey(Signs.MELAG);
    }

    public boolean isMelagMetaSpecies() {

        return MELAG_SPECIES.contains(species.getSurveyCode());

    }

    public boolean containsSign(Signs signs) {
        return this.signs.containsKey(signs);
    }

    public Set<Signs> getSigns() {
        return ImmutableSet.copyOf(signs.keySet());
    }

    public float getWeight(Signs signs) {
        PupitriSignContext signContext = getSignContext(signs);
        return signContext.getWeight();

    }

    public float getTotalWeight() {

        float totalWeight = 0f;
        for (PupitriSignContext signContext : signs.values()) {
            float weight = signContext.getWeight();
            totalWeight += weight;
        }

        return totalWeight;

    }

    public void addToSignContext(Signs sign, BoxType boxType, Float weight) {

        PupitriSignContext signContext = getOrCreateSignContext(sign);
        signContext.addWeight(weight);
        switch (boxType) {
            case SMALL:
                signContext.incrementNbSmallBox();
                break;
            case BIG:
                signContext.incrementNbBigBox();
                break;
        }

        // cf #6116
        if (createMissingSigns) {
            switch (sign) {
                case MALE:
                    getOrCreateSignContext(Signs.FEMALE);
                    getOrCreateSignContext(Signs.DEFAULT);
                    break;
                case FEMALE:
                    getOrCreateSignContext(Signs.MALE);
                    getOrCreateSignContext(Signs.DEFAULT);
                    break;

                case SMALL:
                    getOrCreateSignContext(Signs.BIG);
                    break;
                case BIG:
                    getOrCreateSignContext(Signs.SMALL);
                    break;
            }
        }
    }

    public void setMelagElevatedWeight(Signs melagElevatedSign, Float weight) {

        PupitriSignContext melagContext = getSignContext(Signs.MELAG);
        PupitriSignContext pupitriSignContext = getOrCreateSignContext(melagElevatedSign);
        pupitriSignContext.addWeight(weight);
        //FIXME Check this???
        pupitriSignContext.addNbBoxs(melagContext);

        signs.remove(Signs.MELAG);
        this.melagElevatedSign = melagElevatedSign;

    }

    public void moveMelagToDefaultSign() {

        PupitriSignContext melagContext = getSignContext(Signs.MELAG);

        PupitriSignContext defaultSignContext = getOrCreateSignContext(Signs.DEFAULT);
        defaultSignContext.addWeight(melagContext.getWeight());
        //FIXME Check this???
        defaultSignContext.addNbBoxs(melagContext);
        signs.remove(Signs.MELAG);

    }

    /**
     * Check that the given species catch can be split.
     * For the moment accept for a same catch :
     * <ul>
     * <li>Signs.DEFAULT, Signs.UNSORTED, Signs.MALE, Signs.FEMALE</li>
     * <li>Signs.SMALL, Signs.MEDIUM, Signs.BIG</li>
     * </ul>
     * See http://forge.codelutin.com/issues/3898
     *
     * @return {@code true} if species catch is safe, {@code false} otherwise.
     * @since 3.0-rc-2
     */
    public boolean isSplitSpecies() {

        Set<Signs> allSigns = new HashSet<>(this.signs.keySet());

        boolean result;

        if (DEFAULT_SIGNS.equals(allSigns)) {

            // only a default sign, no split
            result = false;
        } else if (UNSORTED_SIGNS.equals(allSigns)) {

            // only a unsorted sign, no split
            result = false;
        } else {

            // remove all sex signs
            boolean contains = allSigns.removeAll(SEX_SIGNS);
            if (contains) {

                // check there only sex signs
                result = allSigns.isEmpty();
            } else {

                // remove all size signs
                contains = allSigns.removeAll(SIZE_SIGNS);
                if (contains) {

                    // check there only size signs
                    result = allSigns.isEmpty();
                } else {

                    // in all other cases, not a safe
                    result = false;
                }
            }
        }
        return result;
    }

    @Override
    public int hashCode() {
        int speciesHashCode = species != null ? species.hashCode() : 0;
        int sortedHashCode = sorted ? 0x55555555 : 0x2AAAAAAA;
        return speciesHashCode ^ sortedHashCode;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final PupitriSpeciesContext other = (PupitriSpeciesContext) obj;
        if (this.species != other.species && (this.species == null || !this.species.equals(other.species))) {
            return false;
        }
        if (this.sorted != other.sorted) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("species", species.getSurveyCode())
                .append("sorted", sorted)
                .append("signs", signs)
                .toString();
    }

    PupitriSignContext getSignContext(Signs sign) {
        return signs.get(sign);
    }

    private PupitriSignContext getOrCreateSignContext(Signs sign) {

        PupitriSignContext signContext = getSignContext(sign);
        if (signContext == null) {
            signContext = new PupitriSignContext(sign);
            signs.put(sign, signContext);
        }
        return signContext;
    }

}

package fr.ifremer.tutti.service.genericformat.exportactions;

/*
 * #%L
 * Tutti :: Service
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.adagio.core.dao.referential.ObjectTypeCode;
import fr.ifremer.tutti.persistence.entities.data.Attachment;
import fr.ifremer.tutti.persistence.entities.data.CatchBatch;
import fr.ifremer.tutti.persistence.entities.data.Cruise;
import fr.ifremer.tutti.persistence.entities.data.FishingOperation;
import fr.ifremer.tutti.service.PersistenceService;
import fr.ifremer.tutti.service.genericformat.GenericFormatExportContext;
import fr.ifremer.tutti.service.genericformat.GenericFormatExportOperationContext;
import fr.ifremer.tutti.service.genericformat.csv.AttachmentRow;
import fr.ifremer.tutti.service.genericformat.csv.OperationRow;
import fr.ifremer.tutti.service.genericformat.producer.CsvProducerForAttachment;

import java.util.ArrayList;
import java.util.List;

/**
 * Created on 3/28/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.14.3
 */
public class CreateOperationRowsAction extends ExportFishingOperationActionSupport {

    private final PersistenceService persistenceService;

    public CreateOperationRowsAction(PersistenceService persistenceService) {
        this.persistenceService = persistenceService;
    }

    @Override
    public boolean doExecute(GenericFormatExportContext exportContext, GenericFormatExportOperationContext operationContext) {
        return true;
    }

    @Override
    public void execute(GenericFormatExportContext exportContext, GenericFormatExportOperationContext operationContext) {

        FishingOperation operation = operationContext.getOperation();
        Cruise cruise = operationContext.getCruise();
        CatchBatch catchBatch = operationContext.getCatchBatch();

        boolean exportSpecies = exportContext.isExportSpecies();
        boolean exportBenthos = exportContext.isExportBenthos();
        boolean exportMarineLitter = exportContext.isExportMarineLitter();

        OperationRow operationRow = exportContext.getProducerForOperation().getDataToExport(cruise, operation, catchBatch);

        operationRow.computeCatchValues(exportSpecies, exportBenthos, exportMarineLitter);

        operationContext.setOperationRow(operationRow);

        if (exportContext.isExportAttachments()) {

            List<AttachmentRow> attachmentRows = getAttachmentRows(exportContext, operation, catchBatch);
            operationContext.addAttachmentRows(attachmentRows);

        }

    }

    protected List<AttachmentRow> getAttachmentRows(GenericFormatExportContext exportContext, FishingOperation operation, CatchBatch catchBatch) {

        List<AttachmentRow> attachmentRows = new ArrayList<>();

        CsvProducerForAttachment producerForAttachment = exportContext.getProducerForAttachment();
        {

            List<Attachment> attachments = persistenceService.getAllAttachments(ObjectTypeCode.OPERATION, operation.getIdAsInt());
            producerForAttachment.addAttachments(attachments, attachmentRows);

        }

        if (catchBatch != null) {

            List<Attachment> attachments = persistenceService.getAllAttachments(ObjectTypeCode.CATCH_BATCH, catchBatch.getIdAsInt());
            producerForAttachment.addAttachments(attachments, attachmentRows);

        }

        return attachmentRows;

    }

}

package fr.ifremer.tutti.service.csv;

/*
 * #%L
 * Tutti :: Service
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.entities.TuttiEntity;
import org.nuiton.csv.ImportRuntimeException;

import static org.nuiton.i18n.I18n.t;

/**
 * Created on 2/14/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.14
 */
public class EntityNotFoundException extends ImportRuntimeException {

    private static final long serialVersionUID = -1039504487290085439L;

    private final Class<? extends TuttiEntity> entityType;

    private final String id;

    public EntityNotFoundException(Class<? extends TuttiEntity> entityType, String id) {
        this.entityType = entityType;
        this.id = id;
    }

    public Class<? extends TuttiEntity> getEntityType() {
        return entityType;
    }

    public String getId() {
        return id;
    }

    @Override
    public String getMessage() {
        return t("tutti.service.csv.parse.foreignEntityNotFound", entityType, id);
    }
}

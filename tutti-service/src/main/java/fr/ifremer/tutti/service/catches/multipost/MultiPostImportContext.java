package fr.ifremer.tutti.service.catches.multipost;

/*
 * #%L
 * Tutti :: Service
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2016 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.entities.data.FishingOperation;
import fr.ifremer.tutti.persistence.entities.referential.Caracteristic;
import fr.ifremer.tutti.persistence.entities.referential.Species;
import fr.ifremer.tutti.service.DecoratorService;
import fr.ifremer.tutti.service.TuttiDataContext;
import fr.ifremer.tutti.service.TuttiServiceContext;
import fr.ifremer.tutti.service.catches.multipost.csv.AbstractFishingOperationRow;
import fr.ifremer.tutti.service.catches.multipost.csv.AbstractFishingOperationRowModel;
import org.apache.commons.lang3.ObjectUtils;
import org.nuiton.csv.Import;
import org.nuiton.decorator.Decorator;
import org.nuiton.jaxx.application.ApplicationBusinessException;
import org.nuiton.jaxx.application.ApplicationIOUtil;
import org.nuiton.util.DateUtil;

import java.io.Closeable;
import java.io.File;
import java.io.IOException;
import java.io.Reader;
import java.nio.file.Files;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import static org.nuiton.i18n.I18n.n;
import static org.nuiton.i18n.I18n.t;

/**
 * Created on 11/03/16.
 *
 * @author Tony Chemit - chemit@codelutin.com
 */
public class MultiPostImportContext implements MultiPostConstants, Closeable {

    private final File file;
    private final TuttiServiceContext context;
    private final FishingOperation operation;
    protected List<Species> speciesList;
    protected List<Caracteristic> caracteristics;
    protected List<Caracteristic> caracteristicsWithProtected;
    private File explodedDirectory;

    public MultiPostImportContext(TuttiServiceContext context, File file, FishingOperation operation) {
        this.file = file;
        this.context = context;
        this.operation = operation;
    }

    public FishingOperation getOperation() {
        return operation;
    }

    public File getExplodedDirectory() {

        if (explodedDirectory == null) {

            explodedDirectory = ApplicationIOUtil.explodeZip(
                    context.getConfig().getTmpDirectory(),
                    file,
                    n("tutti.service.multipost.uncompress.error"));

        }
        return explodedDirectory;

    }

    public List<Species> getSpeciesList() {

        if (speciesList == null) {
            TuttiDataContext dataContext = context.getDataContext();
            speciesList = dataContext.getReferentSpecies();
        }

        return speciesList;
    }

    public List<Caracteristic> getCaracteristics() {

        if (caracteristics == null) {
            TuttiDataContext dataContext = context.getDataContext();
            caracteristics = dataContext.getCaracteristics();
        }

        return caracteristics;
    }

    public List<Caracteristic> getCaracteristicsWithProtected() {

        if (caracteristicsWithProtected == null) {
            TuttiDataContext dataContext = context.getDataContext();
            caracteristicsWithProtected = dataContext.getCaracteristicWithProtected();
        }

        return caracteristicsWithProtected;
    }

    public Reader newFileReader(String fileName) throws IOException {
        return Files.newBufferedReader(new File(getExplodedDirectory(), fileName).toPath());
    }

    public File newFile(String fileName) {
        return new File(getExplodedDirectory(), fileName);
    }

    @Override
    public void close() throws IOException {

        // FIXME Delete exploded directory

    }

    public <FO extends AbstractFishingOperationRow> void checkSameOperation(String fileName,
                                                                            AbstractFishingOperationRowModel<FO> fishingOperationRowModel) throws IOException {

        try (Reader reader = Files.newBufferedReader(new File(getExplodedDirectory(), fileName).toPath())) {

            try (Import<FO> importer = Import.newImport(fishingOperationRowModel, reader)) {

                Iterator<FO> iterator = importer.iterator();
                if (iterator.hasNext()) {
                    FO row = iterator.next();
                    Date rowDate = DateUtil.getDay(row.getDate());
                    Date operationDate = DateUtil.getDay(operation.getGearShootingStartDate());
                    if (ObjectUtils.notEqual(row.getStationNumber(), operation.getStationNumber())
                            || ObjectUtils.notEqual(row.getOperationNumber(), operation.getFishingOperationNumber())
                            || ObjectUtils.notEqual(row.getMultirigAggregation(), operation.getMultirigAggregation())
                            || ObjectUtils.notEqual(rowDate, operationDate)) {

                        Decorator<FishingOperation> fishingOperationDecorator = context.getService(DecoratorService.class).getDecoratorByType(FishingOperation.class);
                        throw new ApplicationBusinessException(t("tutti.service.multipost.import.wrongOperation.error",
                                                                 fishingOperationDecorator.toString(operation)
                        ));
                    }
                }

            }
        }
    }
}

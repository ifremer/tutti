package fr.ifremer.tutti.service.catches.multipost.csv;

/*
 * #%L
 * Tutti :: Service
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.entities.referential.CaracteristicQualitativeValue;

import java.io.Serializable;

/**
 * @author Kevin Morin - kmorin@codelutin.com
 * @since 2.2
 */
public class MarineLitterRow implements Serializable {

    public static final String BATCH_ID = "batchId";
    public static final String CATEGORY = "category";
    public static final String SIZE_CATEGORY = "sizeCategory";
    public static final String NUMBER = "number";
    public static final String WEIGHT = "weight";
    public static final String COMMENT = "comment";
    private static final long serialVersionUID = 1L;
    protected String batchId;

    protected CaracteristicQualitativeValue category;

    protected CaracteristicQualitativeValue sizeCategory;

    protected Integer number;

    protected Float weight;

    protected String comment;

    public String getBatchId() {
        return batchId;
    }

    public void setBatchId(String batchId) {
        this.batchId = batchId;
    }

    public CaracteristicQualitativeValue getCategory() {
        return category;
    }

    public void setCategory(CaracteristicQualitativeValue category) {
        this.category = category;
    }

    public CaracteristicQualitativeValue getSizeCategory() {
        return sizeCategory;
    }

    public void setSizeCategory(CaracteristicQualitativeValue sizeCategory) {
        this.sizeCategory = sizeCategory;
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public Float getWeight() {
        return weight;
    }

    public void setWeight(Float weight) {
        this.weight = weight;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
}

package fr.ifremer.tutti.service.pupitri.csv;

/*
 * #%L
 * Tutti :: Service
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.entities.data.FishingOperation;
import fr.ifremer.tutti.service.pupitri.BoxType;
import fr.ifremer.tutti.service.pupitri.Directions;
import fr.ifremer.tutti.service.pupitri.Signs;
import org.apache.commons.lang3.time.DateUtils;

import java.io.Serializable;
import java.util.Date;

/**
 * @author Kevin Morin - kmorin@codelutin.com
 * @since 1.2
 */
public class CarrouselRow implements Serializable {

    private static final long serialVersionUID = 1L;

    public static final String PROPERTY_FILE_ORIGIN = "fileOrigin";

    public static final String PROPERTY_DATE = "date";

    public static final String PROPERTY_TIME = "time";

    public static final String PROPERTY_BALANCE_ID = "balanceId";

    public static final String PROPERTY_TO_CONFIRM = "toConfirm";

    public static final String PROPERTY_OPERATION_CODE = "operationCode";

    public static final String PROPERTY_RIG_NUMBER = "rigNumber";

    public static final String PROPERTY_BOX_TYPE = "boxType";

    public static final String PROPERTY_SPECIES_ID = "speciesId";

    public static final String PROPERTY_SIGN = "sign";

    public static final String PROPERTY_DIRECTION = "direction";

    public static final String PROPERTY_WEIGHT = "weight";

    protected String operationCode;

    protected Date date;

    protected String speciesId;

    protected Signs sign;

    protected Directions direction;

    protected Float weight;

    protected BoxType boxType;

    public boolean acceptOperation(FishingOperation operation) {
        return operationCode.equals(operation.getStationNumber())
               && DateUtils.isSameDay(date, operation.getGearShootingStartDate());
    }

    public String getOperationCode() {
        return operationCode;
    }

    public void setOperationCode(String operationCode) {
        this.operationCode = operationCode;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getSpeciesId() {
        return speciesId;
    }

    public void setSpeciesId(String speciesId) {
        this.speciesId = speciesId;
    }

    public Signs getSign() {
        return sign;
    }

    public void setSign(Signs sign) {
        if (Signs.UNSORTED.equals(sign)) {

            // Not use this sign (see https://forge.codelutin.com/issues/5085)
            sign = Signs.DEFAULT;
        }
        this.sign = sign;
    }

    public Directions getDirection() {
        return direction;
    }

    public void setDirection(Directions direction) {
        this.direction = direction;
    }

    public Float getWeight() {
        return weight;
    }

    public void setWeight(Float weight) {
        this.weight = weight;
    }

    public boolean isSorted() {

        return Directions.VAT == direction;

    }

    public BoxType getBoxType() {
        return boxType;
    }

    public void setBoxType(BoxType boxType) {
        this.boxType = boxType;
    }
}

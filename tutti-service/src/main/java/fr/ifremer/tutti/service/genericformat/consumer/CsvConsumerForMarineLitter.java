package fr.ifremer.tutti.service.genericformat.consumer;

/*
 * #%L
 * Tutti :: Service
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.entities.data.MarineLitterBatch;
import fr.ifremer.tutti.service.csv.CsvComsumer;
import fr.ifremer.tutti.service.genericformat.GenericFormatContextSupport;
import fr.ifremer.tutti.service.genericformat.GenericFormatImportEntityParserFactory;
import fr.ifremer.tutti.service.genericformat.GenericFormatImportOperationContext;
import fr.ifremer.tutti.service.genericformat.csv.MarineLitterModel;
import fr.ifremer.tutti.service.genericformat.csv.MarineLitterRow;
import org.nuiton.csv.ImportRow;

import java.nio.file.Path;

/**
 * Created on 2/11/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.14
 */
public class CsvConsumerForMarineLitter extends CsvComsumer<MarineLitterRow, MarineLitterModel> {

    public CsvConsumerForMarineLitter(Path file, char separator, GenericFormatImportEntityParserFactory parserFactory, boolean reportError) {
        super(file, MarineLitterModel.forImport(separator, parserFactory), reportError);
    }

    public GenericFormatImportOperationContext validateRow(ImportRow<MarineLitterRow> row, GenericFormatContextSupport importContext) {

        GenericFormatImportOperationContext operationContext = importContext.getValidationHelper().getFishingOperationContext(this, row, importContext);

        if (operationContext != null) {

            //TODO checks!
            MarineLitterRow bean = row.getBean();

        }

        reportError(row);

        return operationContext;

    }

    public void prepareRowForPersist(GenericFormatImportOperationContext operationContext, ImportRow<MarineLitterRow> row) {

        MarineLitterRow bean = row.getBean();
        MarineLitterBatch marineLitterBatch = bean.getMarineLitterBatch();
        marineLitterBatch.setFishingOperation(operationContext.getFishingOperation());
        operationContext.addMarineLitterBatch(marineLitterBatch);

    }

}
package fr.ifremer.tutti.service.genericformat.csv;

/*
 * #%L
 * Tutti :: Service
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.adagio.core.dao.referential.ObjectTypeCode;
import fr.ifremer.tutti.persistence.entities.data.Attachment;
import fr.ifremer.tutti.service.csv.AbstractTuttiImportExportModel;
import fr.ifremer.tutti.service.csv.TuttiCsvUtil;
import fr.ifremer.tutti.service.genericformat.GenericFormatImportEntityParserFactory;

/**
 * Created on 3/26/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.14.3
 */
public class AttachmentModel extends AbstractTuttiImportExportModel<AttachmentRow> {

    public static AttachmentModel forExport(char separator) {

        AttachmentModel exportModel = new AttachmentModel(separator);
        exportModel.forExport();
        return exportModel;

    }

    public static AttachmentModel forImport(char separator, GenericFormatImportEntityParserFactory parserFactory) {

        AttachmentModel importModel = new AttachmentModel(separator);
        importModel.forImport(parserFactory);
        return importModel;

    }

    @Override
    public AttachmentRow newEmptyInstance() {

        return AttachmentRow.newEmptyInstance();

    }

    protected AttachmentModel(char separator) {
        super(separator);
    }

    protected void forExport() {

        newColumnForExport("Object_Id", Attachment.PROPERTY_OBJECT_ID, TuttiCsvUtil.PRIMITIVE_INTEGER);
        newColumnForExport("Object_Type", Attachment.PROPERTY_OBJECT_TYPE, TuttiCsvUtil.newEnumByNameParserFormatter(ObjectTypeCode.class));
        newColumnForExport("Name", Attachment.PROPERTY_NAME);
        newColumnForExport("Comment", Attachment.PROPERTY_COMMENT);
        newColumnForExport("Path", Attachment.PROPERTY_PATH);

    }

    protected void forImport(GenericFormatImportEntityParserFactory parserFactory) {

        newMandatoryColumn("Object_Id", Attachment.PROPERTY_OBJECT_ID, TuttiCsvUtil.PRIMITIVE_INTEGER);
        newMandatoryColumn("Object_Type", Attachment.PROPERTY_OBJECT_TYPE, TuttiCsvUtil.newEnumByNameParserFormatter(ObjectTypeCode.class));
        newMandatoryColumn("Name", Attachment.PROPERTY_NAME);
        newMandatoryColumn("Comment", Attachment.PROPERTY_COMMENT);
        newMandatoryColumn("Path", Attachment.PROPERTY_PATH);

    }

}

package fr.ifremer.tutti.service.sampling;

/*
 * #%L
 * Tutti :: Service
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2016 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.MoreObjects;
import com.google.common.base.Preconditions;
import fr.ifremer.tutti.persistence.entities.referential.CaracteristicQualitativeValue;
import fr.ifremer.tutti.persistence.entities.referential.Species;
import org.apache.commons.lang3.StringUtils;

import java.io.Closeable;
import java.io.Serializable;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.TreeMap;

/**
 * Created on 19/03/16.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 4.5
 */
class CruiseSamplingInternalCache implements Closeable {

    private static final String KEY_SEPARATOR = "#";

    public static String createSamplingKey(Species species, CaracteristicQualitativeValue gender, Boolean maturity, int lengthStep) {
        Objects.requireNonNull(species);
        return species.getReferenceTaxonId() + KEY_SEPARATOR + (gender == null ? "" : gender.getId()) +
                KEY_SEPARATOR + (maturity == null ? "" : maturity.toString()) + KEY_SEPARATOR + lengthStep;
    }

    public static String addPrefixKey(Serializable prefix, String key) {
        Objects.requireNonNull(prefix);
        Objects.requireNonNull(key);
        return prefix + KEY_SEPARATOR + key;
    }

    private final Map<String, SamplingData> data = new TreeMap<>();

    public SamplingData getSamplingData(String samplingKey) {
        Objects.requireNonNull(samplingKey);
        return data.get(samplingKey);
    }

    public SamplingData getOrCreateSamplingData(String samplingKey) {
        Objects.requireNonNull(samplingKey);
        return data.computeIfAbsent(samplingKey, s -> new SamplingData());
    }

    @Override
    public void close() {
        data.clear();
    }

    public int size() {
        return data.size();
    }

    /**
     * Get the number of samplings by lengthstep for a species, maturity and gender, for the lengthsteps between min size and max size
     *
     * @return a map of the number of samplings for each lengthstep in millimeters
     */
    public List<CacheExtractedKey> getSamplingNumbers(Map<String, Species> speciesById, Map<Integer, CaracteristicQualitativeValue> sexQualitativeValues) {

        List<CacheExtractedKey> result = new LinkedList<>();

        for (Map.Entry<String, SamplingData> entry : data.entrySet()) {

            SamplingData samplingData = entry.getValue();

            if (samplingData.withSampling()) {

                String[] keyTokens = entry.getKey().split(KEY_SEPARATOR);

                CacheExtractedKey key = new CacheExtractedKey();

                int i = 0;
                String nextToken = keyTokens[i++];

                Species species = speciesById.get(nextToken);
                key.setSpecies(species);

                nextToken = keyTokens[i++];
                if (!StringUtils.isEmpty(nextToken)) {
                    Integer sexId = Integer.parseInt(nextToken);
                    CaracteristicQualitativeValue sexQualitativeValue = sexQualitativeValues.get(sexId);
                    key.setSex(sexQualitativeValue);
                }

                nextToken = keyTokens[i++];
                if (!StringUtils.isEmpty(nextToken)) {
                    Boolean maturity = Boolean.parseBoolean(nextToken);
                    key.setMaturity(maturity);
                }

                nextToken = keyTokens[i];
                if (!StringUtils.isEmpty(nextToken)) {
                    int lengthStep = Integer.parseInt(nextToken);
                    key.setLengthStep(lengthStep);
                }

                key.setSamplingNb(samplingData.getSamplingCount());

                result.add(key);
            }

        }

        return result;

    }

    public SamplingData addOneIndividualObservation(String key) {
        SamplingData samplingData = getOrCreateSamplingData(key);
        samplingData.individualObservationCount++;
        return samplingData;
    }

    public SamplingData removeOneIndividualObservation(String key) {
        SamplingData samplingData = getSamplingData(key);

        Objects.requireNonNull(samplingData, "[" + key + "] You cannot decrement a individual observation if the sampling data does not exist");
        Preconditions.checkState(samplingData.withIndividualObservation(), "[" + key + "] You cannot decrement the observation count if there is no observation");
        samplingData.individualObservationCount--;
        return samplingData;
    }

    public SamplingData addOneSampling(String key) {
        //FIXME Normalement on ne doit pas créer ici car si on ajoute un prélèvement c'est qu'il y a déjà au préalable une observation
        SamplingData samplingData = getOrCreateSamplingData(key);
        samplingData.samplingCount++;
        return samplingData;
    }

    public SamplingData removeOneSampling(String key) {
        SamplingData samplingData = getSamplingData(key);
        Objects.requireNonNull(samplingData, "[" + key + "] You cannot decrement a sampling count if the sampling data does not exist");
        Preconditions.checkState(samplingData.withSampling(), "[" + key + "] You cannot decrement a sampling count if there is no sampling");
        samplingData.samplingCount--;
        return samplingData;
    }

    public Set<String> getKeys() {
        return Collections.unmodifiableSet(data.keySet());
    }

    public void remove(String key, int individualObservationCountToRemove, int samplingCountToRemove) {

        SamplingData samplingData = getSamplingData(key);
        Objects.requireNonNull(samplingData, "[" + key + "] not found.");

        int individualObservationCount = samplingData.getIndividualObservationCount();
        Preconditions.checkState(individualObservationCount >= individualObservationCountToRemove, "[" + key + "] You cannot decrement " + individualObservationCountToRemove + " individual observation(s), you just have " + individualObservationCount);
        samplingData.individualObservationCount -= individualObservationCountToRemove;

        int samplingCount = samplingData.getSamplingCount();
        Preconditions.checkState(samplingCount >= samplingCountToRemove, "[" + key + "] You cannot decrement " + samplingCountToRemove + " sampling(s), you just have " + samplingCount);
        samplingData.samplingCount -= samplingCountToRemove;

    }

    public void cleanEmptyEntries() {
        Iterator<Map.Entry<String, SamplingData>> iterator = data.entrySet().iterator();
        while (iterator.hasNext()) {
            Map.Entry<String, SamplingData> entry = iterator.next();
            SamplingData samplingData = entry.getValue();
            if (!(samplingData.withSampling() || samplingData.withIndividualObservation())) {
                iterator.remove();
            }
        }
    }

    private static final String TO_STRING_VERBOSE_FORMAT = "\n%1$-40s → %2$s";

    public String toStringVerbose() {
        StringBuilder stringBuilder = new StringBuilder(size() +" entries.");
        data.entrySet().forEach(entry -> stringBuilder.append(String.format(TO_STRING_VERBOSE_FORMAT, entry.getKey(), entry.getValue())));
        return stringBuilder.toString();
    }

    class SamplingData {

        /**
         * Nombre d'observations individuelles.
         */
        private int individualObservationCount;
        /**
         * Nombre de prélèvements.
         */
        private int samplingCount;

        public int getIndividualObservationCount() {
            return individualObservationCount;
        }

        public boolean withSampling() {
            return samplingCount > 0;
        }

        public boolean withIndividualObservation() {
            return individualObservationCount > 0;
        }

        public int getSamplingCount() {
            return samplingCount;
        }

        @Override
        public String toString() {
            return MoreObjects.toStringHelper(SamplingData.class)
                              .add("individualObservationCount", individualObservationCount)
                              .add("samplingCount", samplingCount)
                              .toString();
        }
    }

}

package fr.ifremer.tutti.service.bigfin.csv;

/*
 * #%L
 * Tutti :: Service
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.service.bigfin.signs.Sex;
import fr.ifremer.tutti.service.bigfin.signs.Size;
import fr.ifremer.tutti.service.bigfin.signs.VracHorsVrac;

import java.util.Date;

/**
 * @author Kevin Morin (Code Lutin)
 * @since 3.8
 */
public class BigfinDataRow {

    public static final String PROPERTY_RECORD_ID = "recordId";

    public static final String PROPERTY_DATE = "date";

    public static final String PROPERTY_STATION = "station";

    public static final String PROPERTY_SPECIES_OR_SPECIES_BATCH = "speciesOrSpeciesBatch";

    public static final String PROPERTY_LENGTH = "length";

    public static final String PROPERTY_WEIGHT = "weight";

    public static final String PROPERTY_SIZE = "size";

    public static final String PROPERTY_SEX = "sex";

    public static final String PROPERTY_VRAC_HORS_VRAC = "vracHorsVrac";

    protected String recordId;

    protected Date date;

    protected String station;

    protected SpeciesOrSpeciesBatch speciesOrSpeciesBatch;

    protected float length;

    protected Float weight;

    protected Size size;

    protected Sex sex;

    protected VracHorsVrac vracHorsVrac;

    public String getRecordId() {
        return recordId;
    }

    public void setRecordId(String recordId) {
        this.recordId = recordId;
    }

    public float getLength() {
        return length;
    }

    public void setLength(float length) {
        this.length = length;
    }

    public Float getWeight() {
        return weight;
    }

    public void setWeight(Float weight) {
        this.weight = weight;
    }

    public SpeciesOrSpeciesBatch getSpeciesOrSpeciesBatch() {
        return speciesOrSpeciesBatch;
    }

    public void setSpeciesOrSpeciesBatch(SpeciesOrSpeciesBatch speciesOrSpeciesBatch) {
        this.speciesOrSpeciesBatch = speciesOrSpeciesBatch;
    }

    public String getStation() {
        return station;
    }

    public void setStation(String station) {
        this.station = station;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Size getSize() {
        return size;
    }

    public void setSize(Size size) {
        this.size = size;
    }

    public Sex getSex() {
        return sex;
    }

    public void setSex(Sex sex) {
        this.sex = sex;
    }

    public VracHorsVrac getVracHorsVrac() {
        return vracHorsVrac;
    }

    public void setVracHorsVrac(VracHorsVrac vracHorsVrac) {
        this.vracHorsVrac = vracHorsVrac;
    }
}

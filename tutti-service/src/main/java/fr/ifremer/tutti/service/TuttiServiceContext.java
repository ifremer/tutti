package fr.ifremer.tutti.service;

/*
 * #%L
 * Tutti :: Service
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import fr.ifremer.tutti.TuttiConfiguration;
import fr.ifremer.tutti.persistence.entities.data.SampleCategoryModel;
import org.apache.commons.io.IOUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.jaxx.application.ApplicationTechnicalException;

import java.io.Closeable;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.util.Date;
import java.util.UUID;
import java.util.concurrent.ExecutionException;

import static org.nuiton.i18n.I18n.t;

/**
 * Tutti application context.
 *
 * This context is load at startup time and close when application dies.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 0.1
 */
public class TuttiServiceContext implements Closeable {

    /** Logger. */
    private static final Log log =
            LogFactory.getLog(TuttiServiceContext.class);

    protected final TuttiConfiguration config;

    protected final TuttiDataContext dataContext;

    protected final LoadingCache<Class<? extends TuttiService>, TuttiService> services;

    public TuttiServiceContext(TuttiConfiguration config) {
        this(config, new TuttiDataContext());
    }

    public TuttiServiceContext(TuttiConfiguration config, TuttiDataContext dataContext) {
        this.config = config;
        this.dataContext = dataContext;
        // add datacontext in shared valueStack
        TuttiValidationDataContextSupport.setValidationContext(dataContext.getValidationContext(), false);
        this.services = CacheBuilder.newBuilder().build(new CacheLoader<Class<? extends TuttiService>, TuttiService>() {
            @Override
            public TuttiService load(Class<? extends TuttiService> key) throws Exception {
                Preconditions.checkNotNull(key);
                Constructor<? extends TuttiService> constructor = key.getConstructor();
                Preconditions.checkNotNull(constructor);
                TuttiService service = constructor.newInstance();
                if (log.isDebugEnabled()) {
                    log.debug("New service " + service);
                }
                service.setServiceContext(TuttiServiceContext.this);
                return service;
            }
        });
    }

    public TuttiConfiguration getConfig() {
        return config;
    }

    public TuttiDataContext getDataContext() {
        return dataContext;
    }

    public Date currentDate() {
        return new Date();
    }

    public <S extends TuttiService> S getService(Class<S> serviceType) {
        if (serviceType == null) {
            return null;
        }
        try {
            return (S) services.get(serviceType);
        } catch (ExecutionException e) {
            throw new ApplicationTechnicalException(t("tutti.service.context.serviceInstanciation.error", serviceType), e);
        }
    }

    public <S extends TuttiService> S reloadService(Class<S> serviceType) {
        S service = (S) services.getIfPresent(serviceType);
        if (service != null) {
            if (log.isDebugEnabled()) {
                log.debug("Close service " + service);
            }
            IOUtils.closeQuietly(service);
        }
        services.invalidate(serviceType);
        return getService(serviceType);
    }

    @Override
    public void close() throws IOException {

        for (TuttiService service : services.asMap().values()) {
            if (log.isDebugEnabled()) {
                log.debug("Close service " + service);
            }
            IOUtils.closeQuietly(service);
        }
        services.invalidateAll();
    }

    public SampleCategoryModel getSampleCategoryModel() {
        SampleCategoryModel result = getDataContext().getSampleCategoryModel();
        Preconditions.checkNotNull(result, "Need a not null sample category model");
        return result;
    }

    public String generateId(Class<?> type) {
        return UUID.randomUUID().toString();
    }
}

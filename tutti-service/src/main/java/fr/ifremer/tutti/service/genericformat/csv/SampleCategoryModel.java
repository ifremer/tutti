package fr.ifremer.tutti.service.genericformat.csv;

/*
 * #%L
 * Tutti :: Service
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.entities.data.SampleCategoryModelEntry;
import fr.ifremer.tutti.service.csv.AbstractTuttiImportExportModel;
import fr.ifremer.tutti.service.csv.TuttiCsvUtil;
import fr.ifremer.tutti.service.genericformat.GenericFormatImportEntityParserFactory;

/**
 * Created on 2/8/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.13
 */
public class SampleCategoryModel extends AbstractTuttiImportExportModel<SampleCategoryRow> {

    public static SampleCategoryModel forExport(char separator) {

        SampleCategoryModel exportModel = new SampleCategoryModel(separator);
        exportModel.forExport();
        return exportModel;

    }

    public static SampleCategoryModel forImport(char separator, GenericFormatImportEntityParserFactory parserFactory) {

        SampleCategoryModel importModel = new SampleCategoryModel(separator);
        importModel.forImport(parserFactory);
        return importModel;

    }

    @Override
    public SampleCategoryRow newEmptyInstance() {

        SampleCategoryRow row = new SampleCategoryRow();
        row.setSampleCategoryModelEntry(new SampleCategoryModelEntry());
        return row;

    }

    protected SampleCategoryModel(char separator) {
        super(separator);
    }

    protected void forExport() {

        newColumnForExport(SampleCategoryRow.PROPERTY_CODE);
        newColumnForExport(SampleCategoryRow.PROPERTY_ORDER, TuttiCsvUtil.INTEGER);
        newColumnForExport(SampleCategoryRow.PROPERTY_CARACTERISTIC, TuttiCsvUtil.CARACTERISTIC_TECHNICAL_FORMATTER);

    }

    protected void forImport(GenericFormatImportEntityParserFactory parserFactory) {

        newMandatoryColumn(SampleCategoryRow.PROPERTY_CODE);
        newMandatoryColumn(SampleCategoryRow.PROPERTY_ORDER, TuttiCsvUtil.INTEGER);
        newMandatoryColumn(SampleCategoryRow.PROPERTY_CARACTERISTIC, parserFactory.getCaracteristicForSampleCategoryParser());

    }

}
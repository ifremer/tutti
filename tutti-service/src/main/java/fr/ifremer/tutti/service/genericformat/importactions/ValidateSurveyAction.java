package fr.ifremer.tutti.service.genericformat.importactions;

/*
 * #%L
 * Tutti :: Service
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.entities.data.Cruise;
import fr.ifremer.tutti.persistence.entities.data.FishingOperation;
import fr.ifremer.tutti.persistence.model.CruiseDataModel;
import fr.ifremer.tutti.service.genericformat.GenericFormatContextSupport;
import fr.ifremer.tutti.service.genericformat.GenericFormatCsvFileResult;
import fr.ifremer.tutti.service.genericformat.GenericformatImportPersistenceHelper;
import fr.ifremer.tutti.service.genericformat.consumer.CsvConsumerForSurvey;
import fr.ifremer.tutti.service.genericformat.csv.SurveyRow;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.csv.ImportRow;
import org.nuiton.csv.ImportRuntimeException;
import org.nuiton.jaxx.application.ApplicationTechnicalException;

import java.io.IOException;
import java.util.Set;

import static org.nuiton.i18n.I18n.t;

/**
 * Created on 3/3/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.14
 */
public class ValidateSurveyAction extends ImportActionSupport {

    /** Logger. */
    private static final Log log = LogFactory.getLog(ValidateSurveyAction.class);

    private static int cruiseId = -1;

    private final GenericformatImportPersistenceHelper persistenceHelper;

    public ValidateSurveyAction(GenericFormatContextSupport importContext, GenericformatImportPersistenceHelper persistenceHelper) {
        super(importContext);
        this.persistenceHelper = persistenceHelper;
    }

    @Override
    protected boolean canExecute() {
        return importContext.isTechnicalFilesValid();
    }

    @Override
    protected void doExecute() {

        if (log.isInfoEnabled()) {
            log.info("Validate survey.csv file.");
        }

        int maximumRowsInErrorPerFile = importContext.getImportRequest().getMaximumRowsInErrorPerFile();

        GenericFormatCsvFileResult importFileResult = importContext.getSurveyFileResult();
        try (CsvConsumerForSurvey consumer = importContext.loadSurveys(false)) {
            for (ImportRow<SurveyRow> row : consumer) {

                importContext.increments(t("tutti.service.genericFormat.validate.cruises", row.getLineNumber()));

                consumer.validateRow(row, importContext);
                consumer.prepareRowForPersist(row);

                Cruise cruise = row.getBean().getCruise();

                CruiseDataModel existingCruiseData = importContext.getImportRequest().getExistingCruiseData(cruise);

                Set<FishingOperation> existingFishingOperations;

                if (existingCruiseData == null) {

                    existingFishingOperations = null;

                    // add a temporary id to simulate persist behaviour
                    cruise.setId(getNextCruiseId());

                } else {

                    existingFishingOperations = persistenceHelper.getFishingOperations(existingCruiseData.getIdAsInt());

                }

                importContext.addImportedCruise(cruise, null, existingCruiseData, existingFishingOperations);

                if (consumer.getNbRowsInErrors() > maximumRowsInErrorPerFile) {
                    if (log.isWarnEnabled()) {
                        log.warn("Too much errors, stop validating this file.");
                    }
                    break;
                }

            }

            flushConsumer(consumer, importFileResult);

        } catch (IOException e) {
            throw new ApplicationTechnicalException("Could not close survey.csv file", e);
        } catch (ImportRuntimeException e) {

            importFileResult.addGlobalError(e.getMessage());

        }

    }

    private static int getNextCruiseId() {
        return cruiseId--;
    }
}

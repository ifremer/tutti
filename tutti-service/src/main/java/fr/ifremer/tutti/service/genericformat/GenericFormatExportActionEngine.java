package fr.ifremer.tutti.service.genericformat;

/*
 * #%L
 * Tutti :: Service
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.entities.data.Cruise;
import fr.ifremer.tutti.service.PersistenceService;
import fr.ifremer.tutti.service.TuttiServiceContext;
import fr.ifremer.tutti.service.genericformat.exportactions.CheckDataAction;
import fr.ifremer.tutti.service.genericformat.exportactions.CreateAccidentalCatchRowsAction;
import fr.ifremer.tutti.service.genericformat.exportactions.CreateCatchBatchRowsAction;
import fr.ifremer.tutti.service.genericformat.exportactions.CreateIndividualObservationRowsAction;
import fr.ifremer.tutti.service.genericformat.exportactions.CreateMarineLitterRowsAction;
import fr.ifremer.tutti.service.genericformat.exportactions.CreateOperationRowsAction;
import fr.ifremer.tutti.service.genericformat.exportactions.CreateParameterRowsAction;
import fr.ifremer.tutti.service.genericformat.exportactions.ExportCruiseActionSupport;
import fr.ifremer.tutti.service.genericformat.exportactions.ExportFishingOperationActionSupport;
import fr.ifremer.tutti.service.genericformat.exportactions.ExportGearCaracteristicAction;
import fr.ifremer.tutti.service.genericformat.exportactions.ExportProtocolAction;
import fr.ifremer.tutti.service.genericformat.exportactions.ExportReferentialGearAction;
import fr.ifremer.tutti.service.genericformat.exportactions.ExportReferentialPersonAction;
import fr.ifremer.tutti.service.genericformat.exportactions.ExportReferentialSpeciesAction;
import fr.ifremer.tutti.service.genericformat.exportactions.ExportReferentialVesselAction;
import fr.ifremer.tutti.service.genericformat.exportactions.ExportSampleCategoryModelAction;
import fr.ifremer.tutti.service.genericformat.exportactions.ExportSpeciesUsedAction;
import fr.ifremer.tutti.service.genericformat.exportactions.ExportSurveyAction;
import fr.ifremer.tutti.service.genericformat.exportactions.ExportTechnicalActionSupport;
import fr.ifremer.tutti.service.genericformat.exportactions.FinalizeExportAction;
import fr.ifremer.tutti.service.genericformat.exportactions.FlushOperationAction;
import fr.ifremer.tutti.service.genericformat.exportactions.GenerateExportArchiveAction;
import fr.ifremer.tutti.service.genericformat.exportactions.LoadDataAction;
import fr.ifremer.tutti.service.protocol.ProtocolImportExportService;
import fr.ifremer.tutti.service.referential.ReferentialTemporaryGearService;
import fr.ifremer.tutti.service.referential.ReferentialTemporaryPersonService;
import fr.ifremer.tutti.service.referential.ReferentialTemporarySpeciesService;
import fr.ifremer.tutti.service.referential.ReferentialTemporaryVesselService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * Created on 3/28/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.14.3
 */
public class GenericFormatExportActionEngine {

    /** Logger. */
    private static final Log log = LogFactory.getLog(GenericFormatExportActionEngine.class);

    private final List<ExportCruiseActionSupport> cruiseActions;

    private final List<ExportFishingOperationActionSupport> fishingOperationsActions;

    private final List<ExportTechnicalActionSupport> loadActions;

    private final List<ExportTechnicalActionSupport> technicalActions;

    public GenericFormatExportActionEngine(TuttiServiceContext serviceContext) {

        this.loadActions = createLoadActions(serviceContext);
        this.cruiseActions = createCruiseActions(serviceContext);
        this.fishingOperationsActions = createFishingOperationActions(serviceContext);
        this.technicalActions = createTechnicalActions(serviceContext);

    }

    public void executeLoadActions(GenericFormatExportContext exportContext) {

        for (ExportTechnicalActionSupport action : loadActions) {

            if (log.isInfoEnabled()) {
                log.info("Execute " + action.getClass().getSimpleName());
            }

            action.execute(exportContext);

        }

    }

    public void executeCruiseActions(GenericFormatExportContext exportContext, Cruise cruise) {

        for (ExportCruiseActionSupport action : cruiseActions) {

            if (log.isInfoEnabled()) {
                log.info("Execute " + action.getClass().getSimpleName() + " on cruise " + cruise.getId());
            }
            action.execute(exportContext, cruise);

        }

    }

    public void executeOperationActions(GenericFormatExportContext exportContext, GenericFormatExportOperationContext operationContext) {

        for (ExportFishingOperationActionSupport action : fishingOperationsActions) {

            if (action.doExecute(exportContext, operationContext)) {

                if (log.isInfoEnabled()) {
                    log.info("Execute " + action.getClass().getSimpleName() + " on operation " + operationContext.getOperation().getId());
                }
                action.execute(exportContext, operationContext);

            } else {

                if (log.isInfoEnabled()) {
                    log.info("Skip    " + action.getClass().getSimpleName() + " on operation " + operationContext.getOperation().getId());
                }

            }

        }

    }

    public void executeTechnicalActions(GenericFormatExportContext exportContext) {

        for (ExportTechnicalActionSupport action : technicalActions) {

            if (log.isInfoEnabled()) {
                log.info("Execute " + action.getClass().getSimpleName());
            }

            action.execute(exportContext);

        }

    }

    private List<ExportTechnicalActionSupport> createLoadActions(TuttiServiceContext serviceContext) {

        List<ExportTechnicalActionSupport> actions = new ArrayList<>();

        actions.add(new LoadDataAction(serviceContext.getService(PersistenceService.class)));
        actions.add(new CheckDataAction(serviceContext.getService(GenericFormatCheckDataService.class)));

        return actions;

    }


    private List<ExportCruiseActionSupport> createCruiseActions(TuttiServiceContext serviceContext) {

        PersistenceService persistenceService = serviceContext.getService(PersistenceService.class);
        String countryId = serviceContext.getConfig().getExportCountryId();

        List<ExportCruiseActionSupport> actions = new ArrayList<>();

        actions.add(new ExportSurveyAction(persistenceService, countryId));
        actions.add(new ExportGearCaracteristicAction(persistenceService));

        return actions;

    }

    private List<ExportFishingOperationActionSupport> createFishingOperationActions(TuttiServiceContext serviceContext) {

        PersistenceService persistenceService = serviceContext.getService(PersistenceService.class);

        List<ExportFishingOperationActionSupport> actions = new ArrayList<>();

        actions.add(new CreateOperationRowsAction(persistenceService));
        actions.add(new CreateParameterRowsAction());
        actions.add(new CreateCatchBatchRowsAction(persistenceService));
        actions.add(new CreateMarineLitterRowsAction(persistenceService));
        actions.add(new CreateIndividualObservationRowsAction(persistenceService));
        actions.add(new CreateAccidentalCatchRowsAction(persistenceService));
        actions.add(new FlushOperationAction());

        return actions;

    }

    private List<ExportTechnicalActionSupport> createTechnicalActions(TuttiServiceContext serviceContext) {

        List<ExportTechnicalActionSupport> actions = new ArrayList<>();

        actions.add(new ExportProtocolAction(serviceContext.getService(ProtocolImportExportService.class)));
        actions.add(new ExportSampleCategoryModelAction());
        actions.add(new ExportSpeciesUsedAction());
        actions.add(new ExportReferentialGearAction(serviceContext.getService(ReferentialTemporaryGearService.class)));
        actions.add(new ExportReferentialPersonAction(serviceContext.getService(ReferentialTemporaryPersonService.class)));
        actions.add(new ExportReferentialSpeciesAction(serviceContext.getService(ReferentialTemporarySpeciesService.class)));
        actions.add(new ExportReferentialVesselAction(serviceContext.getService(ReferentialTemporaryVesselService.class)));
        actions.add(new FinalizeExportAction());
        actions.add(new GenerateExportArchiveAction());

        return actions;

    }

}

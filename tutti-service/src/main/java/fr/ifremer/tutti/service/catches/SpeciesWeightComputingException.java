package fr.ifremer.tutti.service.catches;

/*
 * #%L
 * Tutti :: Service
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2016 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.entities.data.SpeciesBatch;
import fr.ifremer.tutti.type.WeightUnit;

import static org.nuiton.i18n.I18n.t;

/**
 * Created on 13/04/16.
 *
 * @author Tony Chemit - chemit@codelutin.com
 */
public class SpeciesWeightComputingException extends TuttiWeightComputingException {

    public static SpeciesWeightComputingException forValidationMessage(WeightUnit weightUnit,
                                                                       String message,
                                                                       String species,
                                                                       String categoryLabel,
                                                                       String categoryValue,
                                                                       Float weight,
                                                                       Float sampleCategoryWeight,
                                                                       int thisIndex) {

        String errorMessage = t(message,
                                species,
                                categoryLabel,
                                categoryValue,
                                weightUnit.renderFromEntityWithShortLabel(weight),
                                weightUnit.renderFromEntityWithShortLabel(sampleCategoryWeight)
        );
        return new SpeciesWeightComputingException(errorMessage, SpeciesBatch.PROPERTY_WEIGHT, thisIndex);

    }

    public static SpeciesWeightComputingException forIncoherentTotalSorted(WeightUnit weightUnit,
                                                                           Float speciesTotalSortedWeight,
                                                                           Float speciesTotalComputedSortedWeight) {

        String errorMessage = t("tutti.service.operations.computeWeights.error.species.incoherentTotalSorted",
                                weightUnit.renderFromEntityWithShortLabel(speciesTotalSortedWeight),
                                weightUnit.renderFromEntityWithShortLabel(speciesTotalComputedSortedWeight));
        return new SpeciesWeightComputingException(errorMessage);

    }

    public static SpeciesWeightComputingException forIncoherentParentCategoryWeight(WeightUnit weightUnit,
                                                                                    String species,
                                                                                    String categoryLabel,
                                                                                    String categoryValue,
                                                                                    Float categoryWeight,
                                                                                    Float sum,
                                                                                    int thisIndex) {

        String errorMessage = t("tutti.service.operations.computeWeights.error.species.incoherentParentCategoryWeight",
                                species,
                                categoryLabel,
                                categoryValue,
                                weightUnit.renderFromEntityWithShortLabel(categoryWeight),
                                weightUnit.renderFromEntityWithShortLabel(sum));
        return new SpeciesWeightComputingException(errorMessage, SpeciesBatch.PROPERTY_SAMPLE_CATEGORY_WEIGHT, thisIndex);

    }

    public static SpeciesWeightComputingException forIncoherentRowWeightFrequency(WeightUnit weightUnit,
                                                                                  String species,
                                                                                  String categoryLabel,
                                                                                  String categoryValue,
                                                                                  Float frequencyWeight,
                                                                                  Float rowWeight,
                                                                                  int thisIndex) {

        String errorMessage = t("tutti.service.operations.computeWeights.error.species.incoherentRowWeightFrequency",
                                species,
                                categoryLabel,
                                categoryValue,
                                weightUnit.renderFromEntityWithShortLabel(frequencyWeight),
                                weightUnit.renderFromEntityWithShortLabel(rowWeight));
        return new SpeciesWeightComputingException(errorMessage, SpeciesBatch.PROPERTY_WEIGHT, thisIndex);

    }

    public static SpeciesWeightComputingException forIncoherentCategoryWeight(WeightUnit weightUnit,
                                                                              String species,
                                                                              String categoryLabel,
                                                                              String categoryValue,
                                                                              Float frequencyWeight,
                                                                              Float categoryWeight,
                                                                              int thisIndex) {

        String errorMessage = t("tutti.service.operations.computeWeights.error.species.incoherentCategoryWeight",
                                species,
                                categoryLabel,
                                categoryValue,
                                weightUnit.renderFromEntityWithShortLabel(frequencyWeight),
                                weightUnit.renderFromEntityWithShortLabel(categoryWeight));
        return new SpeciesWeightComputingException(errorMessage, SpeciesBatch.PROPERTY_SAMPLE_CATEGORY_WEIGHT, thisIndex);

    }

    public static SpeciesWeightComputingException forNoWeight(String species,
                                                              String categoryLabel,
                                                              String categoryValue,
                                                              int thisIndex) {

        String errorMessage = t("tutti.service.operations.computeWeights.error.species.noWeight",
                                species,
                                categoryLabel,
                                categoryValue);
        return new SpeciesWeightComputingException(errorMessage, SpeciesBatch.PROPERTY_SAMPLE_CATEGORY_WEIGHT, thisIndex);

    }


    private SpeciesWeightComputingException(String message) {
        super(message);
    }

    private SpeciesWeightComputingException(String message, String property, int index) {
        super(message, property, index);
    }
}

package fr.ifremer.tutti.service.genericformat;

/*
 * #%L
 * Tutti :: Service
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.entities.data.Program;
import fr.ifremer.tutti.persistence.entities.referential.Caracteristic;
import fr.ifremer.tutti.service.PersistenceService;
import fr.ifremer.tutti.service.csv.CaracteristicParserFormatter;
import fr.ifremer.tutti.service.csv.CaracteristicValueParserFormatter;
import fr.ifremer.tutti.service.csv.FishingOperationLocationParserFormatter;
import fr.ifremer.tutti.service.csv.FishingOperationStrataParserFormatter;
import fr.ifremer.tutti.service.csv.FishingOperationSubStrataParserFormatter;
import fr.ifremer.tutti.service.csv.GearListParserFormatter;
import fr.ifremer.tutti.service.csv.GearParserFormatter;
import fr.ifremer.tutti.service.csv.HarbourParserFormatter;
import fr.ifremer.tutti.service.csv.PersonListParserFormatter;
import fr.ifremer.tutti.service.csv.PersonParserFormatter;
import fr.ifremer.tutti.service.csv.ProgramParserFormatter;
import fr.ifremer.tutti.service.csv.SpeciesParserFormatter;
import fr.ifremer.tutti.service.csv.VesselListParserFormatter;
import fr.ifremer.tutti.service.csv.VesselParserFormatter;

import java.util.List;

/**
 * Created on 2/15/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.14
 */
public class GenericFormatImportEntityParserFactory {

    private final PersistenceService persistenceService;

    private final GenericFormatContextSupport importContext;

    private ProgramParserFormatter programParser;

    private VesselParserFormatter vesselParser;

    private HarbourParserFormatter harbourParser;

    private GearParserFormatter gearParser;

    private GearListParserFormatter gearListParser;

    private PersonListParserFormatter personListParser;

    private CaracteristicParserFormatter caracteristicParser;

    private CaracteristicParserFormatter caracteristicWithProtectedParser;

    private CaracteristicParserFormatter caracteristicForSampleCategoryParser;

    private FishingOperationStrataParserFormatter fishingOperationStrataParser;

    private FishingOperationSubStrataParserFormatter fishingOperationSubStrataParser;

    private FishingOperationLocationParserFormatter fishingOperationLocationParser;

    private VesselListParserFormatter vesselListParser;

    private SpeciesParserFormatter speciesParser;

    private CaracteristicValueParserFormatter marineLitterCategoryValueParser;

    private CaracteristicValueParserFormatter marineLitterSizeCategoryValueParser;

    public GenericFormatImportEntityParserFactory(PersistenceService persistenceService,
                                                  GenericFormatContextSupport importContext) {
        this.persistenceService = persistenceService;
        this.importContext = importContext;
    }

    protected boolean isAuthorizeObsoleteReferentials() {
        return importContext.getImportRequest().isAuthorizeObsoleteReferentials();
    }

    public VesselParserFormatter getVesselParser() {
        if (vesselParser == null) {
            vesselParser = VesselParserFormatter.newParser(persistenceService, importContext.getReferentialTemporaryVesselFileResult().getIdTranslationMap());
            vesselParser.setAuthorizeObsoleteReferentials(isAuthorizeObsoleteReferentials());
        }
        return vesselParser;
    }

    public HarbourParserFormatter getHarbourParser() {
        if (harbourParser == null) {
            harbourParser = HarbourParserFormatter.newParser(persistenceService);
            harbourParser.setAuthorizeObsoleteReferentials(isAuthorizeObsoleteReferentials());
        }
        return harbourParser;
    }

    public GearParserFormatter getGearParser() {
        if (gearParser == null) {
            gearParser = GearParserFormatter.newParser(persistenceService, importContext.getReferentialTemporaryGearFileResult().getIdTranslationMap());
            gearParser.setAuthorizeObsoleteReferentials(isAuthorizeObsoleteReferentials());
        }
        return gearParser;
    }

    public GearListParserFormatter getGearListParser() {
        if (gearListParser == null) {
            GearParserFormatter delegateParserFormatter = getGearParser();
            gearListParser = GearListParserFormatter.newParser(delegateParserFormatter);
        }
        return gearListParser;
    }

    public PersonListParserFormatter getPersonListParser() {
        if (personListParser == null) {
            PersonParserFormatter delegateParserFormatter = PersonParserFormatter.newParser(persistenceService, importContext.getReferentialTemporaryPersonFileResult().getIdTranslationMap());
            delegateParserFormatter.setAuthorizeObsoleteReferentials(isAuthorizeObsoleteReferentials());
            personListParser = PersonListParserFormatter.newParser(delegateParserFormatter);
        }
        return personListParser;
    }

    public ProgramParserFormatter getProgramParser() {
        if (programParser == null) {
            programParser = ProgramParserFormatter.newParser(persistenceService);
            programParser.setAuthorizeObsoleteReferentials(isAuthorizeObsoleteReferentials());
        }
        return programParser;
    }

    public CaracteristicParserFormatter getCaracteristicParser() {
        if (caracteristicParser == null) {
            List<Caracteristic> caracteristics = persistenceService.getAllCaracteristic();
            caracteristicParser = CaracteristicParserFormatter.newParser(caracteristics);
            caracteristicParser.setAuthorizeObsoleteReferentials(isAuthorizeObsoleteReferentials());
        }
        return caracteristicParser;
    }

    public CaracteristicParserFormatter getCaracteristicWithProtectedParser() {
        if (caracteristicWithProtectedParser == null) {
            List<Caracteristic> caracteristics = persistenceService.getAllCaracteristicWithProtected();
            caracteristicWithProtectedParser = CaracteristicParserFormatter.newParser(caracteristics);
            caracteristicWithProtectedParser.setAuthorizeObsoleteReferentials(isAuthorizeObsoleteReferentials());
        }
        return caracteristicWithProtectedParser;
    }

    public CaracteristicParserFormatter getCaracteristicForSampleCategoryParser() {
        if (caracteristicForSampleCategoryParser == null) {
            List<Caracteristic> caracteristics = persistenceService.getAllCaracteristicForSampleCategory();
            caracteristicForSampleCategoryParser = CaracteristicParserFormatter.newParser(caracteristics);
            caracteristicForSampleCategoryParser.setAuthorizeObsoleteReferentials(isAuthorizeObsoleteReferentials());
        }
        return caracteristicForSampleCategoryParser;
    }

    public FishingOperationStrataParserFormatter getFishingOperationStrataParser() {
        if (fishingOperationStrataParser == null) {
            Program program = importContext.getImportRequest().getProgram();
            fishingOperationStrataParser = FishingOperationStrataParserFormatter.newParser(persistenceService, program.getZone().getId());
            fishingOperationStrataParser.setAuthorizeObsoleteReferentials(isAuthorizeObsoleteReferentials());
        }
        return fishingOperationStrataParser;
    }

    public FishingOperationSubStrataParserFormatter getFishingOperationSubStrataParser() {
        if (fishingOperationSubStrataParser == null) {
            Program program = importContext.getImportRequest().getProgram();
            fishingOperationSubStrataParser = FishingOperationSubStrataParserFormatter.newParser(persistenceService, program.getZone().getId());
            fishingOperationSubStrataParser.setAuthorizeObsoleteReferentials(isAuthorizeObsoleteReferentials());
        }
        return fishingOperationSubStrataParser;
    }

    public FishingOperationLocationParserFormatter getFishingOperationLocationParser() {
        if (fishingOperationLocationParser == null) {
            Program program = importContext.getImportRequest().getProgram();
            fishingOperationLocationParser = FishingOperationLocationParserFormatter.newParser(persistenceService, program.getZone().getId());
            fishingOperationLocationParser.setAuthorizeObsoleteReferentials(isAuthorizeObsoleteReferentials());
        }
        return fishingOperationLocationParser;
    }

    public VesselListParserFormatter getVesselListParser() {
        if (vesselListParser == null) {
            vesselListParser = VesselListParserFormatter.newParser(getVesselParser());
        }
        return vesselListParser;
    }

    public SpeciesParserFormatter getSpeciesParser() {
        if (speciesParser == null) {
            speciesParser = SpeciesParserFormatter.newParser(persistenceService, importContext.getReferentialTemporarySpeciesFileResult().getReferenceTaxonIdTranslationMap());
            speciesParser.setAuthorizeObsoleteReferentials(isAuthorizeObsoleteReferentials());
        }
        return speciesParser;
    }

    public CaracteristicValueParserFormatter getMarineLitterCategoryValueParser() {
        if (marineLitterCategoryValueParser == null) {
            marineLitterCategoryValueParser = CaracteristicValueParserFormatter.newParser(persistenceService.getMarineLitterCategoryCaracteristic());
        }
        return marineLitterCategoryValueParser;
    }

    public CaracteristicValueParserFormatter getMarineLitterSizeCategoryValueParser() {
        if (marineLitterSizeCategoryValueParser == null) {
            marineLitterSizeCategoryValueParser = CaracteristicValueParserFormatter.newParser(persistenceService.getMarineLitterSizeCategoryCaracteristic());
        }
        return marineLitterSizeCategoryValueParser;
    }
}

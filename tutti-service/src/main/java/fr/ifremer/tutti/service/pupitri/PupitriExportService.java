package fr.ifremer.tutti.service.pupitri;

/*
 * #%L
 * Tutti :: Service
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Charsets;
import com.google.common.collect.Lists;
import fr.ifremer.tutti.persistence.entities.referential.Species;
import fr.ifremer.tutti.service.AbstractTuttiService;
import fr.ifremer.tutti.service.pupitri.csv.SpeciesRow;
import fr.ifremer.tutti.service.pupitri.csv.SpeciesRowModel;
import org.apache.commons.collections4.CollectionUtils;
import org.nuiton.csv.Export;
import org.nuiton.jaxx.application.ApplicationTechnicalException;

import java.io.File;
import java.util.List;

import static org.nuiton.i18n.I18n.t;

/**
 * Created on 11/20/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.10
 */
public class PupitriExportService extends AbstractTuttiService {


    public void exportSpecies(List<Species> species, File target) {

        SpeciesRowModel speciesCsvModel = new SpeciesRowModel(';');

        List<SpeciesRow> rows = Lists.newArrayList();

        if (CollectionUtils.isNotEmpty(species)) {
            rows = Lists.transform(species, SpeciesRow::new);
        }

        try {
            Export.exportToFile(speciesCsvModel,
                                rows,
                                target,
                                Charsets.UTF_8,
                                false);

        } catch (Exception e) {
            throw new ApplicationTechnicalException(
                    t("tutti.service.pupitri.export.species.error", target), e);
        }

    }
}



package fr.ifremer.tutti.service.bigfin.signs;

/*
 * #%L
 * Tutti :: Service
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Function;
import fr.ifremer.adagio.core.dao.referential.pmfm.PmfmId;
import fr.ifremer.adagio.core.dao.referential.pmfm.QualitativeValueId;
import fr.ifremer.tutti.persistence.entities.referential.Caracteristic;
import fr.ifremer.tutti.persistence.entities.referential.CaracteristicQualitativeValue;
import fr.ifremer.tutti.persistence.entities.referential.CaracteristicQualitativeValues;
import fr.ifremer.tutti.service.bigfin.csv.BigfinDataRow;

import java.util.Map;

/**
 * Created on 2/3/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.13
 */
public enum Size implements Sign {

    //    classe de taille, 1 = petit ; 2 = gros ; 0 = pas de classe de taille (saisie libre donc risque fort de mauvaise saisie)
    NOT_SIZED("0") {
        @Override
        public Integer getCategory() {
            return PmfmId.SIZE_CATEGORY.getValue();
        }

        @Override
        public Integer getQualitativeValueId() {
            return QualitativeValueId.UNSORTED.getValue();
        }

        @Override
        public boolean isNullEquivalent(int parentSignChildrenNb) {
            return true;
        }

    },
    SMALL("1") {
        @Override
        public Integer getCategory() {
            return PmfmId.SIZE_CATEGORY.getValue();
        }

        @Override
        public Integer getQualitativeValueId() {
            return QualitativeValueId.SIZE_SMALL.getValue();
        }

        @Override
        public boolean isNullEquivalent(int parentSignChildrenNb) {
            return false;
        }
    },
    BIG("2") {
        @Override
        public Integer getCategory() {
            return PmfmId.SIZE_CATEGORY.getValue();
        }

        @Override
        public Integer getQualitativeValueId() {
            return QualitativeValueId.SIZE_BIG.getValue();
        }

        @Override
        public boolean isNullEquivalent(int parentSignChildrenNb) {
            return false;
        }
    };

    private String sign;

    Size(String sign) {
        this.sign = sign;
    }

    @Override
    public String getSign() {
        return sign;
    }

    @Override
    public void registerSign(Caracteristic caracteristic, Map<Sign, CaracteristicQualitativeValue> map) {
        Integer valueId = getQualitativeValueId();
        CaracteristicQualitativeValue result = CaracteristicQualitativeValues.getQualitativeValue(caracteristic, valueId);
        map.put(this, result);
    }

    public static Size getValue(String sign) {
        Size result = null;
        for (Size s : values()) {
            if (s.sign.equals(sign)) {
                result = s;
                break;
            }
        }
        return result;
    }

    public static Function<BigfinDataRow, Sign> newExtractValueFunction() {
        return BigfinDataRow::getSize;
    }

}

package fr.ifremer.tutti.service.catches;

/*
 * #%L
 * Tutti :: Service
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2016 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.entities.data.MarineLitterBatch;
import fr.ifremer.tutti.type.WeightUnit;

import static org.nuiton.i18n.I18n.t;

/**
 * Created on 13/04/16.
 *
 * @author Tony Chemit - chemit@codelutin.com
 */
public class MarineLitterWeightComputingException extends TuttiWeightComputingException {

    public static MarineLitterWeightComputingException forNoWeight(int index) {

        String errorMessage = t("tutti.validator.warning.marineLitter.weight.required");
        return new MarineLitterWeightComputingException(errorMessage, MarineLitterBatch.PROPERTY_WEIGHT, index);

    }

    public static MarineLitterWeightComputingException forIncoherentTotal(WeightUnit weightUnit,
                                                                          Float marineLitterTotalWeight,
                                                                          Float marineLitterTotalComputedWeight) {

        String errorMessage = t("tutti.service.operations.computeWeights.error.marineLitter.incoherentTotal",
                                weightUnit.renderFromEntityWithShortLabel(marineLitterTotalWeight) ,
                                weightUnit.renderFromEntityWithShortLabel(marineLitterTotalComputedWeight) );
        return new MarineLitterWeightComputingException(errorMessage);

    }

    private MarineLitterWeightComputingException(String message) {
        super(message);
    }

    private MarineLitterWeightComputingException(String message, String property, int index) {
        super(message, property, index);
    }
}

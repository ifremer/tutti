package fr.ifremer.tutti.service.pupitri.csv;

/*
 * #%L
 * Tutti :: Service
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.service.csv.TuttiCsvUtil;
import fr.ifremer.tutti.service.csv.ImportModelWithHeader;
import fr.ifremer.tutti.service.pupitri.Directions;
import org.nuiton.csv.Common;

/**
 * @author Kevin Morin - kmorin@codelutin.com
 * @since 1.2
 */
public class TrunkRowModel extends ImportModelWithHeader<TrunkRow> {

    public TrunkRowModel(char separator) {
        super(separator);

        newMandatoryColumn(TrunkRow.PROPERTY_OPERATION_CODE);
        newMandatoryColumn(TrunkRow.PROPERTY_DATE, new Common.DateValue("dd/MM/yy"));
        newMandatoryColumn(TrunkRow.PROPERTY_DIRECTION,
                           TuttiCsvUtil.newEnumByNameParserFormatter(Directions.class));
        newMandatoryColumn(TrunkRow.PROPERTY_WEIGHT,
                           TuttiCsvUtil.WEIGHT_PARSER_FORMATTER);

        newIgnoredColumn(TrunkRow.PROPERTY_SIGN);
        newIgnoredColumn(TrunkRow.PROPERTY_FILE_ORIGIN);
        newIgnoredColumn(TrunkRow.PROPERTY_RIG_NUMBER);
        newIgnoredColumn(TrunkRow.PROPERTY_TIME);
        newIgnoredColumn(TrunkRow.PROPERTY_BALANCE_ID);
    }

    @Override
    public String[] getHeader() {
        return new String[]{
                TrunkRow.PROPERTY_FILE_ORIGIN,
                TrunkRow.PROPERTY_DATE,
                TrunkRow.PROPERTY_TIME,
                TrunkRow.PROPERTY_BALANCE_ID,
                TrunkRow.PROPERTY_SIGN,
                TrunkRow.PROPERTY_OPERATION_CODE,
                TrunkRow.PROPERTY_RIG_NUMBER,
                TrunkRow.PROPERTY_DIRECTION,
                TrunkRow.PROPERTY_WEIGHT
        };
    }

    @Override
    public TrunkRow newEmptyInstance() {
        return new TrunkRow();
    }
}

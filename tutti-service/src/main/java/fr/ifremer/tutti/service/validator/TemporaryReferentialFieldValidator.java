package fr.ifremer.tutti.service.validator;

/*
 * #%L
 * Tutti :: Service
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.opensymphony.xwork2.validator.ValidationException;
import fr.ifremer.tutti.persistence.entities.referential.Status;
import fr.ifremer.tutti.persistence.entities.referential.TuttiReferentialEntity;
import org.nuiton.validator.xwork2.field.NuitonFieldValidatorSupport;

import java.util.Collection;

/**
 * Created on 7/9/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.6
 */
public class TemporaryReferentialFieldValidator extends NuitonFieldValidatorSupport {

    @Override
    protected void validateWhenNotSkip(Object object) throws ValidationException {

        String fieldName = getFieldName();

        Object fieldValue = getFieldValue(fieldName, object);

        boolean valid = true;

        if (fieldValue != null) {

            if (fieldValue instanceof TuttiReferentialEntity) {

                valid = validateReference((TuttiReferentialEntity) fieldValue);

            } else if (fieldValue instanceof Collection) {

                Collection<TuttiReferentialEntity> value = (Collection<TuttiReferentialEntity>) fieldValue;

                for (TuttiReferentialEntity referentialEntity : value) {
                    valid = validateReference(referentialEntity);
                    if (!valid) {
                        break;
                    }
                }

            } else {

                throw new IllegalStateException(
                        "validator " + this +
                        " must be used on a referential or a collection of referential field, but was uses on " +
                        fieldValue);

            }

        }


        if (!valid) {

            addFieldError(fieldName, object);

        }

    }

    protected boolean validateReference(TuttiReferentialEntity value) {
        if (value == null) {
            // no value defined, can not validate it
            return true;
        }

        Status status = value.getStatus();

        return status.getIdAsInt() != 2;
    }

    @Override
    public String getValidatorType() {
        return "temporaryReferential";
    }

}

package fr.ifremer.tutti.service.genericformat;

/*
 * #%L
 * Tutti :: Service
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ImmutableSet;
import fr.ifremer.tutti.persistence.entities.data.Program;
import fr.ifremer.tutti.persistence.entities.data.SampleCategoryModel;
import fr.ifremer.tutti.persistence.entities.protocol.TuttiProtocol;
import fr.ifremer.tutti.persistence.entities.referential.Gear;
import fr.ifremer.tutti.persistence.entities.referential.Person;
import fr.ifremer.tutti.persistence.entities.referential.Species;
import fr.ifremer.tutti.persistence.entities.referential.Vessel;
import fr.ifremer.tutti.persistence.model.ProgramDataModel;

import java.io.File;
import java.io.Serializable;
import java.util.Collections;
import java.util.Date;
import java.util.Set;

/**
 * Created on 2/25/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.14
 */
public abstract class GenericFormatResultSupport implements Serializable {

    private static final long serialVersionUID = 1L;

    private final boolean valid;

    private GenericFormatImportRequest importRequest;

    private final Set<String> archiveLayoutErrors;

    private final TuttiProtocol protocol;

    private final GenericFormatCsvFileResult sampleCategoryFileResult;

    private final GenericFormatReferentialImportResult<Gear, Integer> referentialTemporaryGearFileResult;

    private final GenericFormatReferentialImportResult<Person, Integer> referentialTemporaryPersonFileResult;

    private final GenericFormatReferentialImportResult<Species, Integer> referentialTemporarySpeciesFileResult;

    private final GenericFormatReferentialImportResult<Vessel, String> referentialTemporaryVesselFileResult;

    private final GenericFormatFileResult protocolFileResult;

    private final GenericFormatCsvFileResult surveyFileResult;

    private final GenericFormatCsvFileResult gearCaracteristicFileResult;

    private final GenericFormatCsvFileResult operationFileResult;

    private final GenericFormatCsvFileResult parameterFileResult;

    private final GenericFormatCsvFileResult catchFileResult;

    private final GenericFormatCsvFileResult marineLitterFileResult;

    private final GenericFormatCsvFileResult individualObservationFileResult;

    private final GenericFormatCsvFileResult accidentalCatchFileResult;

    private final GenericFormatCsvFileResult attachmentsFileResult;

    private String protocolOriginalName;

    private ProgramDataModel dataModel;

    protected GenericFormatResultSupport(GenericFormatContextSupport importContext) {

        this.importRequest = importContext.getImportRequest();
        this.protocol = importContext.getImportedProtocol();
        this.sampleCategoryFileResult = importContext.getSampleCategoryFileResult();
        this.referentialTemporaryGearFileResult = importContext.getReferentialTemporaryGearFileResult();
        this.referentialTemporaryPersonFileResult = importContext.getReferentialTemporaryPersonFileResult();
        this.referentialTemporarySpeciesFileResult = importContext.getReferentialTemporarySpeciesFileResult();
        this.referentialTemporaryVesselFileResult = importContext.getReferentialTemporaryVesselFileResult();
        this.protocolFileResult = importContext.getProtocolFileResult();
        this.surveyFileResult = importContext.getSurveyFileResult();
        this.gearCaracteristicFileResult = importContext.getGearCaracteristicFileResult();
        this.operationFileResult = importContext.getOperationFileResult();
        this.parameterFileResult = importContext.getParameterFileResult();
        this.catchFileResult = importContext.getCatchFileResult();
        this.marineLitterFileResult = importContext.getMarineLitterFileResult();
        this.individualObservationFileResult = importContext.getIndividualObservationFileResult();
        this.accidentalCatchFileResult = importContext.getAccidentalCatchFileResult();
        this.attachmentsFileResult = importContext.getAttachmentFileResult();
        this.protocolOriginalName  = importContext.getProtocolOriginalName();
        Set<String> archiveLayoutErrors1;
        if (importContext.isArchiveLayoutValid()) {
            archiveLayoutErrors1 = Collections.emptySet();
        } else {
            archiveLayoutErrors1 = importContext.getArchiveLayoutErrors();
        }
        this.archiveLayoutErrors = ImmutableSet.copyOf(archiveLayoutErrors1);
        this.valid = computeValid();
    }

    private boolean computeValid() {

        return isArchiveLayoutValid()
                         && sampleCategoryFileResult.isValid()
                         && referentialTemporaryGearFileResult.isValid()
                         && referentialTemporaryPersonFileResult.isValid()
                         && referentialTemporarySpeciesFileResult.isValid()
                         && referentialTemporaryVesselFileResult.isValid()
                         && protocolFileResult.isValid()
                         && surveyFileResult.isValid()
                         && gearCaracteristicFileResult.isValid()
                         && operationFileResult.isValid()
                         && parameterFileResult.isValid()
                         && catchFileResult.isValid()
                         && marineLitterFileResult.isValid()
                         && accidentalCatchFileResult.isValid()
                         && individualObservationFileResult.isValid()
                         && attachmentsFileResult.isValid();

    }

    public boolean isArchiveLayoutValid() {
        return archiveLayoutErrors.isEmpty();
    }

    public Set<String> getArchiveLayoutErrors() {
        return archiveLayoutErrors;
    }

    public TuttiProtocol getProtocol() {
        return protocol;
    }

    public GenericFormatCsvFileResult getSampleCategoryFileResult() {
        return sampleCategoryFileResult;
    }

    public SampleCategoryModel getSampleCategoryModel() {
        return importRequest.getSampleCategoryModel();
    }

    public GenericFormatReferentialImportResult<Gear, Integer> getReferentialTemporaryGearFileResult() {
        return referentialTemporaryGearFileResult;
    }

    public GenericFormatReferentialImportResult<Person, Integer> getReferentialTemporaryPersonFileResult() {
        return referentialTemporaryPersonFileResult;
    }

    public GenericFormatReferentialImportResult<Species, Integer> getReferentialTemporarySpeciesFileResult() {
        return referentialTemporarySpeciesFileResult;
    }

    public GenericFormatReferentialImportResult<Vessel, String> getReferentialTemporaryVesselFileResult() {
        return referentialTemporaryVesselFileResult;
    }

    public GenericFormatFileResult getProtocolFileResult() {
        return protocolFileResult;
    }

    public GenericFormatCsvFileResult getSurveyFileResult() {
        return surveyFileResult;
    }

    public GenericFormatCsvFileResult getGearCaracteristicFileResult() {
        return gearCaracteristicFileResult;
    }

    public GenericFormatCsvFileResult getOperationFileResult() {
        return operationFileResult;
    }

    public GenericFormatCsvFileResult getParameterFileResult() {
        return parameterFileResult;
    }

    public GenericFormatCsvFileResult getCatchFileResult() {
        return catchFileResult;
    }

    public GenericFormatCsvFileResult getMarineLitterFileResult() {
        return marineLitterFileResult;
    }

    public GenericFormatCsvFileResult getIndividualObservationFileResult() {
        return individualObservationFileResult;
    }

    public GenericFormatCsvFileResult getAccidentalCatchFileResult() {
        return accidentalCatchFileResult;
    }

    public GenericFormatCsvFileResult getAttachmentsFileResult() {
        return attachmentsFileResult;
    }

    public File getReportFile() {
        return importRequest.getReportFile();
    }

    public GenericFormatImportConfiguration getImportConfiguration() {
        return importRequest.getImportConfiguration();
    }

    public Program getProgram() {
        return importRequest.getProgram();
    }

    public Date getStartingDate() {
        return importRequest.getStartingDate();
    }

    public boolean isCleanWeights() {
        return importRequest.isCleanWeights();
    }

    public boolean isCheckWeights() {
        return importRequest.isCheckWeights();
    }

    public GenericFormatArchive getArchive() {
        return importRequest.getArchive();
    }

    public boolean isValid() {
        return valid;
    }

    public String getProtocolOriginalName() {
        return protocolOriginalName;
    }

    public void setProtocolOriginalName(String protocolOriginalName) {
        this.protocolOriginalName = protocolOriginalName;
    }

    public ProgramDataModel getDataModel() {
        return dataModel;
    }

    public void setDataModel(ProgramDataModel dataModel) {
        this.dataModel = dataModel;
    }

}

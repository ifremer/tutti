package fr.ifremer.tutti.service.sampling;

/*
 * #%L
 * Tutti :: Service
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2016 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.entities.referential.CaracteristicQualitativeValue;
import fr.ifremer.tutti.persistence.entities.referential.Species;

import java.io.Serializable;

/**
 * @author Kevin Morin (Code Lutin)
 * @since 4.5
 */
public class CacheExtractedKey implements Serializable {

    public static final String PROPERTY_SPECIES = "species";
    public static final String PROPERTY_LENGTH_STEP = "lengthStep";
    public static final String PROPERTY_MATURITY = "maturity";
    public static final String PROPERTY_SEX = "sex";
    public static final String PROPERTY_SAMPLING_NB = "samplingNb";
    public static final String PROPERTY_MAX_BY_LENGTH_STEP = "maxByLengthStep";

    protected Species species;
    protected int lengthStep;
    protected Boolean maturity;
    protected CaracteristicQualitativeValue sex;
    protected int samplingNb;
    protected Integer maxByLengthStep;

    public int getLengthStep() {
        return lengthStep;
    }

    public void setLengthStep(int lengthStep) {
        this.lengthStep = lengthStep;
    }

    public Boolean getMaturity() {
        return maturity;
    }

    public void setMaturity(Boolean maturity) {
        this.maturity = maturity;
    }

    public Integer getMaxByLengthStep() {
        return maxByLengthStep;
    }

    public void setMaxByLengthStep(Integer maxByLengthStep) {
        this.maxByLengthStep = maxByLengthStep;
    }

    public int getSamplingNb() {
        return samplingNb;
    }

    public void setSamplingNb(int samplingNb) {
        this.samplingNb = samplingNb;
    }

    public CaracteristicQualitativeValue getSex() {
        return sex;
    }

    public void setSex(CaracteristicQualitativeValue sex) {
        this.sex = sex;
    }

    public Species getSpecies() {
        return species;
    }

    public void setSpecies(Species species) {
        this.species = species;
    }
}

package fr.ifremer.tutti.service.pupitri.csv;

/*
 * #%L
 * Tutti :: Service
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import fr.ifremer.tutti.persistence.entities.referential.Species;
import org.apache.commons.lang3.StringUtils;

import java.io.Serializable;

/**
 * @author Kevin Morin - kmorin@codelutin.com
 * @since 1.2
 */
public class SpeciesRow implements Serializable {

    private static final long serialVersionUID = 1L;

    public static final String PROPERTY_CODE_FIRST_PART = "codeFirstPart";

    public static final String PROPERTY_CODE_SECOND_PART = "codeSecondPart";

    public static final String PROPERTY_SCIENTIFIC_NAME = "scientificName";

    protected final String codeFirstPart;

    protected final String codeSecondPart;

    protected final String scientificName;

    public SpeciesRow(Species species) {

        String surveyCode = species.getSurveyCode();

        Preconditions.checkNotNull(surveyCode,
                                   "Unable to export a species with a null survey code : " + species);

        int signIndex = surveyCode.indexOf("-");

        if (signIndex == -1) {

            // surveyCode is XXXXYYY
            codeFirstPart = StringUtils.rightPad(StringUtils.substring(surveyCode, 0, 4), 4);
            codeSecondPart = StringUtils.rightPad(StringUtils.substring(surveyCode, 4, 7), 3);

        } else {

            // surveycode is XXXX-YYY
            codeFirstPart = StringUtils.rightPad(StringUtils.substring(surveyCode, 0, 4), 4);
            codeSecondPart = StringUtils.rightPad(StringUtils.substring(surveyCode, 5, 8), 3);

        }

        scientificName = species.getName();

    }

    public String getCodeFirstPart() {
        return codeFirstPart;
    }

    public String getCodeSecondPart() {
        return codeSecondPart;
    }

    public String getScientificName() {
        return scientificName;
    }

}

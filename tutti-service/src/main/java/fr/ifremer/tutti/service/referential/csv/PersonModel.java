package fr.ifremer.tutti.service.referential.csv;

/*
 * #%L
 * Tutti :: Service
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.entities.referential.Person;
import fr.ifremer.tutti.persistence.entities.referential.Persons;
import fr.ifremer.tutti.service.csv.AbstractTuttiImportExportModel;
import fr.ifremer.tutti.service.csv.TuttiCsvUtil;

import static org.nuiton.i18n.I18n.t;

/**
 * Model to import / export {@link Person} in csv format.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.0
 */
public class PersonModel extends AbstractTuttiImportExportModel<PersonRow> {

    public static PersonModel forExport(char separator) {

        PersonModel exportModel = new PersonModel(separator);
        exportModel.forExport();
        return exportModel;

    }

    public static PersonModel forImport(char separator) {

        PersonModel importModel = new PersonModel(separator);
        importModel.forImport();
        return importModel;

    }

    @Override
    public PersonRow newEmptyInstance() {
        return new PersonRow();
    }

    protected PersonModel(char separator) {
        super(separator);
    }

    protected void forImport() {

        newMandatoryColumn(PersonRow.PROPERTY_ID, new TemporaryReferentialEntityIdParser(
                t("tutti.service.referential.import.person.error.idNotNegative")){

            @Override
            protected boolean isTemporaryId(String parse) {
                int id = Integer.parseInt(parse);
                return Persons.isTemporaryId(id);
            }
        });
        newMandatoryColumn(PersonRow.PROPERTY_FIRST_NAME);
        newMandatoryColumn(PersonRow.PROPERTY_LAST_NAME);
        newMandatoryColumn(PersonRow.PROPERTY_TO_DELETE, TuttiCsvUtil.BOOLEAN);

    }

    protected void forExport() {

        newColumnForExport(PersonRow.PROPERTY_ID);
        newColumnForExport(PersonRow.PROPERTY_FIRST_NAME);
        newColumnForExport(PersonRow.PROPERTY_LAST_NAME);
        newColumnForExport(PersonRow.PROPERTY_TO_DELETE);

    }

}

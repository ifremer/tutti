package fr.ifremer.tutti.service.catches.multipost.csv;

/*
 * #%L
 * Tutti :: Service
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.entities.referential.Caracteristic;
import fr.ifremer.tutti.service.catches.multipost.MultiPostConstants;
import fr.ifremer.tutti.service.csv.AbstractTuttiImportExportModel;
import fr.ifremer.tutti.service.csv.CaracteristicValueParserFormatter;
import fr.ifremer.tutti.service.csv.TuttiCsvUtil;

/**
 * @author Kevin Morin - kmorin@codelutin.com
 * @since 2.2
 */
public class MarineLitterRowModel extends AbstractTuttiImportExportModel<MarineLitterRow> {

    private MarineLitterRowModel(Caracteristic categoryCaracteristic, Caracteristic sizeCategoryCaracteristic) {
        super(MultiPostConstants.CSV_SEPARATOR);

        newColumnForImportExport(MarineLitterRow.BATCH_ID);

        newColumnForExport(MarineLitterRow.CATEGORY, TuttiCsvUtil.CARACTERISTIC_VALUE_TECHNICAL_FORMATTER);
        newMandatoryColumn(MarineLitterRow.CATEGORY, CaracteristicValueParserFormatter.newParser(categoryCaracteristic));

        newColumnForExport(MarineLitterRow.SIZE_CATEGORY, TuttiCsvUtil.CARACTERISTIC_VALUE_TECHNICAL_FORMATTER);
        newMandatoryColumn(MarineLitterRow.SIZE_CATEGORY, CaracteristicValueParserFormatter.newParser(sizeCategoryCaracteristic));

        newColumnForImportExport(MarineLitterRow.NUMBER, TuttiCsvUtil.INTEGER);

        newColumnForImportExport(MarineLitterRow.WEIGHT, TuttiCsvUtil.WEIGHT_PARSER_FORMATTER);

        newColumnForImportExport(MarineLitterRow.COMMENT);
    }

    public static MarineLitterRowModel forExport() {
        return new MarineLitterRowModel(null, null);
    }

    public static MarineLitterRowModel forImport(Caracteristic categoryCaracteristic, Caracteristic sizeCategoryCaracteristic) {
        return new MarineLitterRowModel(categoryCaracteristic, sizeCategoryCaracteristic);
    }

    @Override
    public MarineLitterRow newEmptyInstance() {
        return new MarineLitterRow();
    }
}

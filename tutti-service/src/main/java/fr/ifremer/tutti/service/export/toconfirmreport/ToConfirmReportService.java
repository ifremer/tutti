package fr.ifremer.tutti.service.export.toconfirmreport;

/*
 * #%L
 * Tutti :: Service
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.ProgressionModel;
import fr.ifremer.tutti.persistence.entities.data.Cruise;
import fr.ifremer.tutti.persistence.entities.data.SpeciesBatch;
import fr.ifremer.tutti.persistence.entities.protocol.TuttiProtocol;
import fr.ifremer.tutti.persistence.entities.referential.Species;
import fr.ifremer.tutti.persistence.entities.referential.TaxonCache;
import fr.ifremer.tutti.persistence.entities.referential.TaxonCaches;
import fr.ifremer.tutti.service.AbstractTuttiService;
import fr.ifremer.tutti.service.DecoratorService;
import fr.ifremer.tutti.service.PdfGeneratorService;
import fr.ifremer.tutti.service.PersistenceService;
import fr.ifremer.tutti.service.TuttiServiceContext;
import fr.ifremer.tutti.service.catches.WeightComputingService;
import fr.ifremer.tutti.type.WeightUnit;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import static org.nuiton.i18n.I18n.t;

/**
 * @author Kevin Morin (Code Lutin)
 * @since 3.13
 */
public class ToConfirmReportService extends AbstractTuttiService {

    private static final Log log = LogFactory.getLog(ToConfirmReportService.class);

    protected PersistenceService persistenceService;

    protected DecoratorService decoratorService;

    protected PdfGeneratorService pdfGeneratorService;

    protected WeightComputingService weightComputingService;

    protected WeightUnit speciesWeightUnit;

    protected WeightUnit benthosWeightUnit;

    @Override
    public void setServiceContext(TuttiServiceContext context) {
        super.setServiceContext(context);
        persistenceService = getService(PersistenceService.class);
        decoratorService = getService(DecoratorService.class);
        pdfGeneratorService = getService(PdfGeneratorService.class);
        weightComputingService = getService(WeightComputingService.class);

        speciesWeightUnit = context.getConfig().getSpeciesWeightUnit();
        benthosWeightUnit = context.getConfig().getBenthosWeightUnit();
    }

    public int getNumberOfSteps(Integer cruiseId) {

        List<Integer> allFishingOperation = persistenceService.getAllFishingOperationIds(cruiseId);
        return allFishingOperation.size() + 3;

    }

    public void createToConfirmReport(File file, Integer cruiseId, ProgressionModel progressionModel) {

        progressionModel.increments(t("tutti.toconfirmReport.loading.cruise", cruiseId));

        Cruise cruise = persistenceService.getCruise(cruiseId);
        if (log.isDebugEnabled()) {
            log.debug("Cruise " + decoratorService.getDecorator(cruise).toString(cruise));
        }

        progressionModel.increments(t("tutti.toconfirmReport.loading.protocol"));

        TuttiProtocol protocol = context.getDataContext().getProtocol();

        TaxonCache speciesCache = TaxonCaches.createSpeciesCache(persistenceService, protocol);
        TaxonCache benthosCache = TaxonCaches.createBenthosCache(persistenceService, protocol);

        List<Integer> allFishingOperation = persistenceService.getAllFishingOperationIds(cruiseId);

        ToConfirmReportBean reportBean = new ToConfirmReportBean(cruise, speciesWeightUnit, benthosWeightUnit);

        int currentOperation = 1;
        int nbOperations = allFishingOperation.size();

        for (Integer operationId : allFishingOperation) {

            progressionModel.increments(t("tutti.toconfirmReport.loading.operation", operationId, currentOperation++, nbOperations));

            ToConfirmReportFishingOperationData fishingOperationData =
                    ToConfirmReportFishingOperationData.create(persistenceService, operationId);

            if (fishingOperationData != null) {

                ToConfirmReportFishingOperationBean operationBean = createOperationBean(fishingOperationData, speciesCache, benthosCache);
                reportBean.addOperation(operationBean);
            }

        }

        progressionModel.increments(t("tutti.toconfirmReport.generate.report"));

        generatePdf(file, reportBean);

    }

    protected ToConfirmReportFishingOperationBean createOperationBean(ToConfirmReportFishingOperationData fishingOperationData,
                                                                      TaxonCache speciesCache,
                                                                      TaxonCache benthosCache) {

        // Species

        List<ToConfirmReportBatchEntryBean> speciesCatchList;

        if (fishingOperationData.isWithSpeciesBatchToConfirm()) {

            List<SpeciesBatch> speciesBatchEntries = fishingOperationData.getSpeciesBatchToConfirm();
            speciesCache.loadInBatches(speciesBatchEntries);
            speciesCatchList = createBatchBeans(speciesWeightUnit, speciesBatchEntries);

        } else {

            speciesCatchList = null;
        }

        // Benthos

        List<ToConfirmReportBatchEntryBean> benthosCatchList;

        if (fishingOperationData.isWithBenthosBatchToConfirm()) {

            List<SpeciesBatch> benthosBatchEntries = fishingOperationData.getBenthosBatchToConfirm();
            benthosCache.loadInBatches(benthosBatchEntries);
            benthosCatchList = createBatchBeans(benthosWeightUnit, benthosBatchEntries);

        } else {

            benthosCatchList = null;
        }

        return new ToConfirmReportFishingOperationBean(fishingOperationData.getFishingOperation(),
                                                       speciesCatchList,
                                                       benthosCatchList);

    }

    protected List<ToConfirmReportBatchEntryBean> createBatchBeans(WeightUnit weightUnit, List<SpeciesBatch> batchEntries) {

        List<ToConfirmReportBatchEntryBean> catchList = new ArrayList<>();

        for (SpeciesBatch batch : batchEntries) {

            Species species = batch.getSpecies();

            Float sampleCategoryWeightValue = weightUnit.fromEntity(batch.getSampleCategoryWeight());
            String sampleCategoryWeight = weightUnit.renderWeight(sampleCategoryWeightValue);

            Float weightValue = weightUnit.fromEntity(batch.getWeight());
            String weight = weightUnit.renderWeight(weightValue);

            String comment = batch.getComment();

            String category = getBatchDecoratedSampleCategoryValue(batch);

            while (batch.getParentBatch() != null) {
                batch = batch.getParentBatch();
                category = getBatchDecoratedSampleCategoryValue(batch) + " / " + category;
            }

            ToConfirmReportBatchEntryBean reportEntry = new ToConfirmReportBatchEntryBean(species,
                                                                                          category,
                                                                                          sampleCategoryWeight,
                                                                                          weight,
                                                                                          comment);
            catchList.add(reportEntry);
        }

        return catchList;
    }

    protected String getBatchDecoratedSampleCategoryValue(SpeciesBatch batch) {
        Serializable sampleCategoryValue = batch.getSampleCategoryValue();
        return decoratorService.getDecorator(sampleCategoryValue).toString(sampleCategoryValue);
    }

    protected void generatePdf(File targetFile, ToConfirmReportBean reportBean) {

        Locale locale = context.getConfig().getI18nLocale();

        pdfGeneratorService.generatePdf(targetFile, locale, "toConfirmSpeciesReport.ftl", reportBean);

    }
}

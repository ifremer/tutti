package fr.ifremer.tutti.service.catches.multipost.csv;

/*
 * #%L
 * Tutti :: Service
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.entities.referential.Caracteristic;
import fr.ifremer.tutti.service.catches.multipost.MultiPostConstants;
import fr.ifremer.tutti.service.csv.AbstractTuttiImportExportModel;
import fr.ifremer.tutti.service.csv.TuttiCsvUtil;

import java.util.List;

/**
 * @author Kevin Morin - kmorin@codelutin.com
 * @since 2.3
 */
public class CaracteristicRowModel extends AbstractTuttiImportExportModel<CaracteristicRow> {

    private CaracteristicRowModel(List<Caracteristic> caracteristics) {
        super(MultiPostConstants.CSV_SEPARATOR);

        newColumnForImportExport(CaracteristicRow.BATCH_ID);

        newColumnForExport(CaracteristicRow.CARACTERISTIC, TuttiCsvUtil.CARACTERISTIC_TECHNICAL_FORMATTER);
        newForeignKeyColumn(CaracteristicRow.CARACTERISTIC, Caracteristic.class, caracteristics);

        newColumnForExport(CaracteristicRow.VALUE, TuttiCsvUtil.CARACTERISTIC_VALUE_TECHNICAL_FORMATTER);
        newMandatoryColumn(CaracteristicRow.VALUE, TuttiCsvUtil.STRING);

    }

    public static CaracteristicRowModel forExport() {
        return new CaracteristicRowModel(null);
    }

    public static CaracteristicRowModel forImport(List<Caracteristic> caracteristics) {
        return new CaracteristicRowModel(caracteristics);
    }

    @Override
    public CaracteristicRow newEmptyInstance() {
        return new CaracteristicRow();
    }
}

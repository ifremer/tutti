package fr.ifremer.tutti.service.genericformat.csv;

/*
 * #%L
 * Tutti :: Service
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.service.csv.AbstractTuttiImportExportModel;
import fr.ifremer.tutti.service.csv.TuttiCsvUtil;

/**
 * To export a species used in data to export.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.3
 */
public class SpeciesExportModel extends AbstractTuttiImportExportModel<SpeciesExportRow> {

    public static SpeciesExportModel forExport(char separator) {

        SpeciesExportModel exportModel = new SpeciesExportModel(separator);
        exportModel.forExport();
        return exportModel;

    }

    @Override
    public SpeciesExportRow newEmptyInstance() {

        return new SpeciesExportRow();

    }

    protected SpeciesExportModel(char separator) {
        super(separator);
    }

    protected void forExport() {

        newColumnForExport("Code_Taxon", SpeciesExportRow.SPECIES, TuttiCsvUtil.SPECIES_TECHNICAL_FORMATTER);
        newColumnForExport("Code_Rubin", SpeciesExportRow.SPECIES, TuttiCsvUtil.SPECIES_REF_TAX_CODE_FORMATTER);
        newColumnForExport("Nom_Scientifique", SpeciesExportRow.SPECIES, TuttiCsvUtil.SPECIES_FORMATTER);
        newColumnForExport("Code_Campagne", SpeciesExportRow.SPECIES, TuttiCsvUtil.SPECIES_SURVEY_CODE_FORMATTER);

    }

}

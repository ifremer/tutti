package fr.ifremer.tutti.service.psionimport;

/*
 * #%L
 * Tutti :: Service
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import fr.ifremer.tutti.persistence.entities.referential.Species;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.mutable.MutableInt;

import java.io.Serializable;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Created on 1/20/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.0.1
 */
public class PsionImportBatchModel {

    public static class SampleCategory {

        protected final Integer categoryId;

        protected final Serializable categoryValue;

        public SampleCategory(Integer categoryId, Serializable categoryValue) {
            this.categoryId = categoryId;
            this.categoryValue = categoryValue;
        }

        public Integer getCategoryId() {
            return categoryId;
        }

        public Serializable getCategoryValue() {
            return categoryValue;
        }
    }

    protected final Species species;

    protected final Integer lengthStepCaracteristicId;

    protected Float weight;

    protected Float sampleWeight;

    protected final List<SampleCategory> categories;

    protected final Map<Float, MutableInt> frequencies;

    protected String categoryCode;

    protected boolean applyBothWeightOnCategorizedBatch;

    public PsionImportBatchModel(Species species, Integer lengthStepCaracteristicId) {
        this.species = species;
        this.lengthStepCaracteristicId = lengthStepCaracteristicId;
        this.frequencies = Maps.newTreeMap();
        this.categories = Lists.newArrayList();
    }

    public void setCategoryCode(String categoryCode) {
        this.categoryCode = categoryCode;
    }

    public void setWeight(Float weight) {
        this.weight = weight;
    }

    public void setSampleWeight(Float sampleWeight) {
        this.sampleWeight = sampleWeight;
    }

    public void setCategory(Integer categoryId, Serializable categoryValue) {
        SampleCategory category = new SampleCategory(categoryId, categoryValue);
        categories.add(category);
    }

    public void addFrequency(Float size, int number) {
        MutableInt mutableFloat = frequencies.get(size);
        if (mutableFloat == null) {
            mutableFloat = new MutableInt(0);
            frequencies.put(size, mutableFloat);
        }
        mutableFloat.add(number);
    }

    public Species getSpecies() {
        return species;
    }

    public String getCategoryCode() {
        return categoryCode;
    }

    public Integer getLengthStepCaracteristicId() {
        return lengthStepCaracteristicId;
    }

    public Float getWeight() {
        return weight;
    }

    public Float getSampleWeight() {
        return sampleWeight;
    }

    public Iterator<SampleCategory> getCategoryIterator() {
        return categories.iterator();
    }

    public boolean withFrequencies() {
        return !frequencies.isEmpty();
    }

    public boolean withCategories() {
        return !categories.isEmpty();
    }

    public Map<Float, MutableInt> getFrequencies() {
        return frequencies;
    }

    public int getNbFrequencies() {
        return frequencies.size();
    }

    public void setApplyBothWeightOnCategorizedBatch(boolean applyBothWeightOnCategorizedBatch) {
        this.applyBothWeightOnCategorizedBatch = applyBothWeightOnCategorizedBatch;
    }

    public boolean isApplyBothWeightOnCategorizedBatch() {
        return applyBothWeightOnCategorizedBatch;
    }

    void merge(PsionImportBatchModel batchModel) {
        setWeight(getWeight() + batchModel.getWeight());
        setSampleWeight(getSampleWeight() + batchModel.getSampleWeight());

        for (Map.Entry<Float, MutableInt> entry : batchModel.getFrequencies().entrySet()) {
            Float stepClass = entry.getKey();
            int number = entry.getValue().intValue();
            addFrequency(stepClass, number);
        }
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("species", species.getSurveyCode())
                .append("categoryCode", categoryCode)
                .append("weight", weight)
                .append("sampleWeight", sampleWeight)
                .append("frequencies", frequencies.size())
                .toString();
    }
}

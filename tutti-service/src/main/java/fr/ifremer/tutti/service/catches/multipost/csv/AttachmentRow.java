package fr.ifremer.tutti.service.catches.multipost.csv;

/*
 * #%L
 * Tutti :: Service
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.io.File;
import java.io.Serializable;

/**
 * @author Kevin Morin - kmorin@codelutin.com
 * @since 2.2
 */
public class AttachmentRow implements Serializable {

    public static final String ATTACHMENT_NAME = "name";
    public static final String ATTACHMENT_BATCH_ID = "batchId";
    public static final String ATTACHMENT_COMMENT = "comment";
    public static final String ATTACHMENT_FILE = "file";
    private static final long serialVersionUID = 1L;
    protected String name;

    protected String batchId;

    protected String comment;

    protected File file;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBatchId() {
        return batchId;
    }

    public void setBatchId(String batchId) {
        this.batchId = batchId;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public File getFile() {
        return file;
    }

    public void setFile(File file) {
        this.file = file;
    }
}

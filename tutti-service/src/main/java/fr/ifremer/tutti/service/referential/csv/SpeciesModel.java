package fr.ifremer.tutti.service.referential.csv;

/*
 * #%L
 * Tutti :: Service
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.entities.referential.Species;
import fr.ifremer.tutti.persistence.entities.referential.Speciess;
import fr.ifremer.tutti.service.csv.AbstractTuttiImportExportModel;
import fr.ifremer.tutti.service.csv.TuttiCsvUtil;

import static org.nuiton.i18n.I18n.t;

/**
 * Model to import / export {@link Species} in csv format.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.0
 */
public class SpeciesModel extends AbstractTuttiImportExportModel<SpeciesRow> {

    public static SpeciesModel forExport(char separator, boolean addReferenceTaxonId) {

        SpeciesModel exportModel = new SpeciesModel(separator);
        exportModel.forExport(addReferenceTaxonId);
        return exportModel;

    }

    public static SpeciesModel forImport(char separator, boolean addReferenceTaxonId) {

        SpeciesModel importModel = new SpeciesModel(separator);
        importModel.forImport(addReferenceTaxonId);
        return importModel;

    }

    @Override
    public SpeciesRow newEmptyInstance() {
        return new SpeciesRow();
    }

    protected void forImport(boolean addReferenceTaxonId) {

        newMandatoryColumn(SpeciesRow.PROPERTY_ID, new TemporaryReferentialEntityIdParser(
                t("tutti.service.referential.import.species.error.idNotNegative")) {

            @Override
            protected boolean isTemporaryId(String parse) {
                int id = Integer.parseInt(parse);
                return Speciess.isTemporaryId(id);
            }
        });
        newMandatoryColumn(SpeciesRow.PROPERTY_NAME);
        newMandatoryColumn(SpeciesRow.PROPERTY_TO_DELETE, TuttiCsvUtil.BOOLEAN);

        if (addReferenceTaxonId) {
            newMandatoryColumn(SpeciesRow.PROPERTY_REFERENCE_TAXON_ID, TuttiCsvUtil.INTEGER);
        }

    }

    protected void forExport(boolean addReferenceTaxonId) {

        newColumnForExport(SpeciesRow.PROPERTY_ID);
        newColumnForExport(SpeciesRow.PROPERTY_NAME);
        newColumnForExport(SpeciesRow.PROPERTY_TO_DELETE);

        if (addReferenceTaxonId) {
            newColumnForExport(SpeciesRow.PROPERTY_REFERENCE_TAXON_ID, TuttiCsvUtil.INTEGER);
        }

    }

    protected SpeciesModel(char separator) {
        super(separator);
    }

}

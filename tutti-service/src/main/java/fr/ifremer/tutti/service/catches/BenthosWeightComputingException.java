package fr.ifremer.tutti.service.catches;

/*
 * #%L
 * Tutti :: Service
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2016 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.entities.data.SpeciesBatch;
import fr.ifremer.tutti.type.WeightUnit;

import static org.nuiton.i18n.I18n.t;

/**
 * Created on 13/04/16.
 *
 * @author Tony Chemit - chemit@codelutin.com
 */
public class BenthosWeightComputingException extends TuttiWeightComputingException {

    public static BenthosWeightComputingException forValidationMessage(WeightUnit weightUnit,
                                                                       String message,
                                                                       String species,
                                                                       String categoryLabel,
                                                                       String categoryValue,
                                                                       Float weight,
                                                                       Float sampleCategoryWeight,
                                                                       int thisIndex) {

        String errorMessage = t(message,
                                species,
                                categoryLabel,
                                categoryValue,
                                weightUnit.renderFromEntityWithShortLabel(weight),
                                weightUnit.renderFromEntityWithShortLabel(sampleCategoryWeight)
        );
        return new BenthosWeightComputingException(errorMessage, SpeciesBatch.PROPERTY_WEIGHT, thisIndex);

    }

    public static BenthosWeightComputingException forIncoherentTotalSorted(WeightUnit weightUnit,
                                                                           Float speciesTotalSortedWeight,
                                                                           Float speciesTotalComputedSortedWeight) {

        String errorMessage = t("tutti.service.operations.computeWeights.error.benthos.incoherentTotalSorted",
                                weightUnit.renderFromEntityWithShortLabel(speciesTotalSortedWeight),
                                weightUnit.renderFromEntityWithShortLabel(speciesTotalComputedSortedWeight));
        return new BenthosWeightComputingException(errorMessage);

    }

    public static BenthosWeightComputingException forIncoherentParentCategoryWeight(WeightUnit weightUnit,
                                                                                    String species,
                                                                                    String categoryLabel,
                                                                                    String categoryValue,
                                                                                    Float categoryWeight,
                                                                                    Float sum,
                                                                                    int thisIndex) {

        String errorMessage = t("tutti.service.operations.computeWeights.error.benthos.incoherentParentCategoryWeight",
                                species,
                                categoryLabel,
                                categoryValue,
                                weightUnit.renderFromEntityWithShortLabel(categoryWeight),
                                weightUnit.renderFromEntityWithShortLabel(sum));
        return new BenthosWeightComputingException(errorMessage, SpeciesBatch.PROPERTY_SAMPLE_CATEGORY_WEIGHT, thisIndex);

    }

    public static BenthosWeightComputingException forIncoherentRowWeightFrequency(WeightUnit weightUnit,
                                                                                  String species,
                                                                                  String categoryLabel,
                                                                                  String categoryValue,
                                                                                  Float frequencyWeight,
                                                                                  Float rowWeight,
                                                                                  int thisIndex) {

        String errorMessage = t("tutti.service.operations.computeWeights.error.benthos.incoherentRowWeightFrequency",
                                species,
                                categoryLabel,
                                categoryValue,
                                weightUnit.renderFromEntityWithShortLabel(frequencyWeight),
                                weightUnit.renderFromEntityWithShortLabel(rowWeight));
        return new BenthosWeightComputingException(errorMessage, SpeciesBatch.PROPERTY_WEIGHT, thisIndex);

    }

    public static BenthosWeightComputingException forIncoherentCategoryWeight(WeightUnit weightUnit,
                                                                              String species,
                                                                              String categoryLabel,
                                                                              String categoryValue,
                                                                              Float frequencyWeight,
                                                                              Float categoryWeight,
                                                                              int thisIndex) {

        String errorMessage = t("tutti.service.operations.computeWeights.error.benthos.incoherentCategoryWeight",
                                species,
                                categoryLabel,
                                categoryValue,
                                weightUnit.renderFromEntityWithShortLabel(frequencyWeight),
                                weightUnit.renderFromEntityWithShortLabel(categoryWeight));
        return new BenthosWeightComputingException(errorMessage, SpeciesBatch.PROPERTY_SAMPLE_CATEGORY_WEIGHT, thisIndex);

    }

    public static BenthosWeightComputingException forNoWeight(String species,
                                                              String categoryLabel,
                                                              String categoryValue,
                                                              int thisIndex) {

        String errorMessage = t("tutti.service.operations.computeWeights.error.benthos.noWeight",
                                species,
                                categoryLabel,
                                categoryValue);
        return new BenthosWeightComputingException(errorMessage, SpeciesBatch.PROPERTY_SAMPLE_CATEGORY_WEIGHT, thisIndex);

    }


    private BenthosWeightComputingException(String message) {
        super(message);
    }

    private BenthosWeightComputingException(String message, String property, int index) {
        super(message, property, index);
    }
}

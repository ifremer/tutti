package fr.ifremer.tutti.service;

/*
 * #%L
 * Tutti :: Service
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import fr.ifremer.tutti.TuttiConfiguration;
import fr.ifremer.tutti.persistence.entities.data.Cruise;
import fr.ifremer.tutti.persistence.entities.data.FishingOperation;
import fr.ifremer.tutti.persistence.entities.data.Program;
import fr.ifremer.tutti.persistence.entities.data.SampleCategoryModel;
import fr.ifremer.tutti.persistence.entities.data.SampleCategoryModelEntry;
import fr.ifremer.tutti.persistence.entities.protocol.SpeciesProtocol;
import fr.ifremer.tutti.persistence.entities.protocol.TuttiProtocol;
import fr.ifremer.tutti.persistence.entities.protocol.TuttiProtocols;
import fr.ifremer.tutti.persistence.entities.referential.Caracteristic;
import fr.ifremer.tutti.persistence.entities.referential.CaracteristicQualitativeValue;
import fr.ifremer.tutti.persistence.entities.referential.Gear;
import fr.ifremer.tutti.persistence.entities.referential.Person;
import fr.ifremer.tutti.persistence.entities.referential.Species;
import fr.ifremer.tutti.persistence.entities.referential.TaxonCache;
import fr.ifremer.tutti.persistence.entities.referential.TaxonCaches;
import fr.ifremer.tutti.persistence.entities.referential.Vessel;
import fr.ifremer.tutti.service.cruise.CruiseCache;
import fr.ifremer.tutti.service.cruise.CruiseCacheLoader;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jdesktop.beans.AbstractBean;

import java.io.Closeable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Data context of ui.
 *
 * All shared data must be there to avoid reloading some stuff.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.0.2
 */
public class TuttiDataContext extends AbstractBean implements Closeable {

    /** Logger. */
    private static final Log log = LogFactory.getLog(TuttiDataContext.class);

    public static final String PROPERTY_PROGRAM_ID = "programId";

    public static final String PROPERTY_CRUISE_ID = "cruiseId";

    public static final String PROPERTY_PROTOCOL_ID = "protocolId";

    public static final String PROPERTY_FISHING_OPERATION_ID = "fishingOperationId";

    public static final String PROPERTY_PROGRAM_FILLED = "programFilled";

    public static final String PROPERTY_CRUISE__FILLED = "cruiseFilled";

    public static final String PROPERTY_PROTOCOL_FILLED = "protocolFilled";

    public static final String PROPERTY_FISHING_OPERATION_FILLED = "fishingOperationFilled";

    /**
     * Id of last selected program (can be null if none ever selected).
     *
     * @since 0.1
     */
    protected String programId;

    /**
     * Id of last selected cruise (can be null if none ever selected).
     *
     * @since 0.1
     */
    protected Integer cruiseId;

    /**
     * Id of last selected protocol (can be null if none ever selected).
     *
     * @since 0.1
     */
    protected String protocolId;

    /**
     * Id of last selected fishing operation (can be null if none ever selected).
     *
     * @since 1.2
     */
    protected Integer fishingOperationId;

    /**
     * Model of sampling as defined in configuration.
     *
     * @since 2.4
     */
    protected SampleCategoryModel sampleCategoryModel;

    /**
     * Flag to reload sampleCategoryModel in lazy mode.
     *
     * @since 3.13
     */
    protected boolean dirtySampleCategoryModel;

    protected Program program;

    protected Cruise cruise;

    /**
     * Le cache des données liées à la campagne sélectionnée.
     *
     * @since 4.5
     */
    protected CruiseCache cruiseCache;

    protected FishingOperation fishingOperation;

    protected List<Caracteristic> caracteristics;

    protected List<Caracteristic> caracteristicsWithProtected;

    protected List<Caracteristic> lengthStepCaracteristics;

    protected List<Caracteristic> maturityCaracteristics;

    protected List<CaracteristicQualitativeValue> genderValues;

    protected List<CaracteristicQualitativeValue> deadOrAliveValues;

    protected List<CaracteristicQualitativeValue> cpsTypeValues;

    protected List<Person> persons;

    protected List<Species> species;

    protected List<Species> referentSpeciesWithSurveyCode;

    protected List<Species> referentBenthosWithSurveyCode;

    protected List<Species> referentSpecies;

    protected List<Vessel> fishingVessels;

    protected List<Vessel> scientificVessels;

    protected List<Gear> fishingGears;

    protected List<Gear> scientificGears;

    protected List<Caracteristic> defaultIndividualObservationCaracteristics;

    protected TuttiValidationDataContextSupport validationContext = new TuttiValidationDataContext(this);

    protected PersistenceService service;

    TuttiDataContext() {
        addPropertyChangeListener(PROPERTY_PROGRAM_ID, evt -> resetProgram());
        addPropertyChangeListener(PROPERTY_CRUISE_ID, evt -> resetCruise());
        addPropertyChangeListener(PROPERTY_PROTOCOL_ID, evt -> resetProtocol());
        addPropertyChangeListener(PROPERTY_FISHING_OPERATION_ID, evt -> resetFishingOperation());
    }

    public void setPersistenceService(PersistenceService service) {
        this.service = service;
    }

    public void open(TuttiConfiguration config) {

        PersistenceService persistenceService = this.service;

        // Check there is a persistence service injected
        Preconditions.checkNotNull(persistenceService, "Can't have a null persistence service.");

        // Close data context
        close();

        // Reset the persistence service
        this.service = persistenceService;

        // load protocol and propagate it to service
        if (isProtocolFilled()) {

            if (!persistenceService.isProtocolExist(getProtocolId())) {

                if (log.isErrorEnabled()) {
                    log.error("Could not find protocol with id: " + getProtocolId());
                }

                setProtocolId(null);

            } else {

                try {
                    getProtocol();
                } catch (Exception e) {

                    if (log.isErrorEnabled()) {
                        log.error("Could not use load protocol with id:" + getProtocolId(), e);
                    }
                    setProtocolId(null);

                }

            }

        }
        setSampleCategoryModel(config.getSampleCategoryModel());
    }

    @Override
    public void close() {
        clearContext();
        service = null;
    }

    public void clearContext() {
        resetProgram();
        dirtySampleCategoryModel = true;
        resetProtocol();
        resetVessels();
        resetGears();
        resetPersons();
        resetSpecies();
        resetValidationDataContext();
        resetCaracteristics();
    }

    public void checkDbContext() {

        String saneProtocolId = null;
        String saneProgramId = null;
        Integer saneCruiseId = null;

        String oldProtocolId = getProtocolId();
        String oldProgramId = getProgramId();
        Integer oldCruiseId = getCruiseId();

        if (isProtocolFilled()) {

            if (!service.isProtocolExist(oldProtocolId)) {

                // not found in this db

                if (log.isWarnEnabled()) {
                    log.warn("Remove invalid protocolId: " + oldProtocolId);
                }

            } else {
                if (log.isInfoEnabled()) {
                    log.info("protocolId valid: " + oldProtocolId);
                }

                // can keep this id
                saneProtocolId = oldProtocolId;
            }
        }

        if (isProgramFilled()) {


            Program program = null;

            try {
                program = service.getProgram(oldProgramId);
            } catch (Exception e) {
                // program does not exist
            }
            if (program == null) {

                // not found in this db

                if (log.isWarnEnabled()) {
                    log.warn("Remove invalid programId: " + oldProgramId);
                }

            } else {

                if (log.isInfoEnabled()) {
                    log.info("ProgramId valid: " + oldProgramId);
                }

                // can keep this id

                saneProgramId = oldProgramId;

                // test cruiseId
                if (isCruiseFilled()) {

                    Cruise cruise = null;


                    try {
                        cruise = service.getCruise(oldCruiseId);
                    } catch (Exception e) {
                        // cruise does not exist
                    }

                    if (cruise != null &&
                            !cruise.getProgram().getId().equals(oldProgramId)) {

                        // not matchin program, reset cruise id
                        cruise = null;
                    }

                    if (cruise == null) {

                        // not found in this db

                        if (log.isWarnEnabled()) {
                            log.warn("Remove invalid cruiseId: " + oldCruiseId);
                        }
                        setCruiseId(null);

                    } else {

                        if (log.isInfoEnabled()) {
                            log.info("CruiseId valid: " + oldCruiseId);
                        }

                        // can keep this id
                        saneCruiseId = oldCruiseId;
                    }
                }
            }
        }

        boolean oldProtocolFilled = isProtocolFilled();
        boolean oldProgramFilled = isProgramFilled();
        boolean oldCruiseFilled = isCruiseFilled();
        this.programId = saneProgramId;
        this.cruiseId = saneCruiseId;
        this.protocolId = saneProtocolId;

        firePropertyChange(PROPERTY_PROGRAM_FILLED, oldProgramFilled, isProgramFilled());
        firePropertyChange(PROPERTY_PROTOCOL_FILLED, oldProtocolFilled, isProtocolFilled());
        firePropertyChange(PROPERTY_CRUISE__FILLED, oldCruiseFilled, isCruiseFilled());
        firePropertyChange(PROPERTY_PROGRAM_ID, oldProgramId, saneProgramId);
        firePropertyChange(PROPERTY_PROTOCOL_ID, oldProtocolId, saneProtocolId);
        firePropertyChange(PROPERTY_CRUISE_ID, oldCruiseId, saneCruiseId);
    }

    public SampleCategoryModel getSampleCategoryModel() {
        if (dirtySampleCategoryModel) {

            try {
                if (log.isInfoEnabled()) {
                    log.info("Loading sampleCategoryModel: " + sampleCategoryModel);
                }
                sampleCategoryModel.load(service);
            } finally {

                dirtySampleCategoryModel = false;
            }

        }
        return sampleCategoryModel;
    }

    public void setSampleCategoryModel(SampleCategoryModel sampleCategoryModel) {
        Preconditions.checkNotNull(sampleCategoryModel, "Can't get a null sampleCategoryModel");
        this.sampleCategoryModel = sampleCategoryModel;
        this.dirtySampleCategoryModel = true;
    }

    public String getProgramId() {
        return programId;
    }

    public Integer getCruiseId() {
        return cruiseId;
    }

    public String getProtocolId() {
        return protocolId;
    }

    public Integer getFishingOperationId() {
        return fishingOperationId;
    }

    /**
     * @return {@code true} si une campache est charfée.
     */
    public boolean isCruiseFilled() {
        return isProgramFilled() && cruiseId != null;
    }

    /**
     * @return {@code true} si un protocol est chargé.
     */
    public boolean isProtocolFilled() {
        return StringUtils.isNotBlank(protocolId);
    }

    /**
     * @return {@code true} si une série de campagne est chargée.
     */
    public boolean isProgramFilled() {
        return StringUtils.isNotBlank(programId);
    }

    /**
     * @return {@code true} si une opérationde pêche est chargée.
     */
    public boolean isFishingOperationFilled() {
        return fishingOperationId != null;
    }

    /**
     * @return {@code true} si on peut utiliser le cache sur la campagne.
     */
    public boolean isCanUseCruiseCache() {
        return isCruiseFilled();
    }

    /**
     * @return {@code true} si le cache de campagne est chargé.
     */
    public boolean isCruiseCacheLoaded() {
        return cruiseCache != null;
    }

    /**
     * @return {@code true} si le cache de campagne est à jour.
     */
    public boolean isCruiseCacheUpToDate() {
        return isCruiseCacheLoaded() && cruiseCache.isCacheUpToDate(getCruiseId(), getProtocolId());
    }

    /**
     * @return {@code true} si on peut utiliser le cache de prélèvement des pièces calcifiées.
     */
    public boolean isCanUseCruiseSamplingCache() {
        return isCanUseCruiseCache() && isProtocolFilled() && getProtocol().isUseCalcifiedPieceSampling();
    }

    public void setProgramId(String programId) {
        boolean oldProgramFilled = isProgramFilled();
        boolean oldCruiseFilled = isCruiseFilled();

        this.programId = programId;

        // always propagate the change
        firePropertyChange(PROPERTY_PROGRAM_ID, -1, programId);
        firePropertyChange(PROPERTY_PROGRAM_FILLED, oldProgramFilled, isProgramFilled());
        firePropertyChange(PROPERTY_CRUISE__FILLED, oldCruiseFilled, isCruiseFilled());
    }

    public void setCruiseId(Integer cruiseId) {
        boolean oldValue = isCruiseFilled();

        this.cruiseId = cruiseId;

        // always propagate the change
        firePropertyChange(PROPERTY_CRUISE_ID, -1, cruiseId);
        firePropertyChange(PROPERTY_CRUISE__FILLED,
                           oldValue, isCruiseFilled());
    }

    public void setProtocolId(String protocolId) {
        boolean oldValue = isProtocolFilled();
        this.protocolId = protocolId;

        // always propagate the change
        firePropertyChange(PROPERTY_PROTOCOL_ID, -1, protocolId);
        firePropertyChange(PROPERTY_PROTOCOL_FILLED,
                           oldValue, isProtocolFilled());

        if (getService() != null) {
            // always reload protocol (as a side-effect push it to protocol service)
            getProtocol();
        }
    }

    public void setFishingOperationId(Integer fishingOperationId) {
        boolean oldValue = isFishingOperationFilled();
        this.fishingOperationId = fishingOperationId;

        // always propagate the change
        firePropertyChange(PROPERTY_FISHING_OPERATION_ID, -1, fishingOperationId);
        firePropertyChange(PROPERTY_FISHING_OPERATION_FILLED, oldValue, isFishingOperationFilled());
    }

    public Program getProgram() {
        checkOpened();
        if (program == null) {
            if (isProgramFilled()) {
                String id = getProgramId();
                if (log.isInfoEnabled()) {
                    log.info("Loading program: " + id);
                }
                program = service.getProgram(id);
            }
        }
        return program;
    }

    public Cruise getCruise() {
        checkOpened();
        if (cruise == null) {
            if (isCruiseFilled()) {
                Integer id = getCruiseId();
                if (log.isInfoEnabled()) {
                    log.info("Loading cruise: " + id);
                }
                cruise = service.getCruise(id);
            }
        }
        return cruise;
    }

    public Cruise reloadCruise() {
        checkOpened();
        Preconditions.checkState(isCruiseFilled());
        Integer id = getCruiseId();
        if (log.isInfoEnabled()) {
            log.info("Reloading cruise: " + id);
        }
        cruise = service.getCruise(id);
        return cruise;
    }

    public TuttiProtocol getProtocol() {
        checkOpened();
        if (isProtocolFilled()) {
            TuttiProtocol protocol = service.getProtocol();
            if (protocol == null) {
                String id = getProtocolId();
                if (log.isInfoEnabled()) {
                    log.info("Loading protocol: " + id + " for service: " + service);
                }
                protocol = service.getProtocol(id);
                service.setProtocol(protocol);
            }
        } else {
            TuttiProtocol protocol = service.getProtocol();
            if (protocol != null) {
                if (log.isInfoEnabled()) {
                    log.info("Remove protocol: " + protocol);
                }
                service.setProtocol(null);
            }
        }
        return service.getProtocol();
    }

    public Optional<CruiseCache> getOptionalCruiseCache() {
        return Optional.ofNullable(isCruiseCacheUpToDate() ? cruiseCache : null);
    }

    public void loadCruiseCache(CruiseCacheLoader cruiseCacheLoader) {

        checkOpened();

        if (!isCanUseCruiseCache()) {
            throw new IllegalStateException("Pas autorisé à charger le cache d'échantillons");
        }

        if (isCruiseCacheLoaded() && !isCruiseCacheUpToDate()) {
            closeCruiseCache();
        }

        if (log.isInfoEnabled()) {
            log.info("Loading cruise sampling cache: {cruiseId:" + getCruiseId() + ", protocolId: " + getProtocolId() + "}");
        }

        cruiseCache = cruiseCacheLoader.loadCruiseCache();

        if (log.isInfoEnabled()) {
            log.info("cruise sampling cache loaded: " + cruiseCache);
        }

    }

    public List<Integer> getCruiseFishingOperationIds() {
        checkOpened();
        if (!isCruiseFilled()) {
            throw new IllegalStateException("Aucune campagne chargée, impossible de récupérer les identifiants de traits");
        }
        return service.getAllFishingOperationIds(getCruiseId());
    }

    public FishingOperation getFishingOperation() {
        checkOpened();
        if (fishingOperation == null) {
            if (isFishingOperationFilled()) {
                Integer id = getFishingOperationId();
                if (log.isInfoEnabled()) {
                    log.info("Loading fishingOperation: " + id);
                }
                fishingOperation = service.getFishingOperation(id);
            }
        }
        return fishingOperation;
    }

    public void reloadFishingOperation() {
        checkOpened();
        Preconditions.checkState(isFishingOperationFilled());
        Integer id = getFishingOperationId();
        if (log.isInfoEnabled()) {
            log.info("Reloading fishingOperation: " + id);
        }
        resetFishingOperation();
        firePropertyChange(PROPERTY_FISHING_OPERATION_ID, -1, fishingOperationId);
    }

    public List<Caracteristic> getCaracteristics() {
        checkOpened();
        if (caracteristics == null) {
            if (log.isInfoEnabled()) {
                log.info("Loading allCaracteristic");
            }
            caracteristics = service.getAllCaracteristic();
        }
        return caracteristics;
    }

    public List<Caracteristic> getCaracteristicWithProtected() {
        checkOpened();
        if (caracteristicsWithProtected == null) {
            if (log.isInfoEnabled()) {
                log.info("Loading allCaracteristicWithProtected");
            }
            caracteristicsWithProtected = service.getAllCaracteristicWithProtected();
        }
        return caracteristicsWithProtected;
    }

    public List<Caracteristic> getLengthStepCaracteristics() {
        checkOpened();
        if (lengthStepCaracteristics == null) {

            if (log.isInfoEnabled()) {
                log.info("Loading lengthStepCaracteristics");
            }
            lengthStepCaracteristics = service.getLengthStepCaracteristics(getCaracteristics());
        }
        return lengthStepCaracteristics;
    }

    public List<Caracteristic> getMaturityCaracteristics() {
        checkOpened();
        if (maturityCaracteristics == null) {

            if (log.isInfoEnabled()) {
                log.info("Loading maturityCaracteristics");
            }
            maturityCaracteristics = service.getMaturityCaracteristics(getCaracteristics());
        }
        return maturityCaracteristics;
    }

    public SampleCategoryModelEntry getBestFirstSampleCategory(List<SampleCategoryModelEntry> categories,
                                                               SpeciesProtocol speciesProtocol) {

        SampleCategoryModelEntry selectedCategory = null;

        if (speciesProtocol != null) {

            if (!speciesProtocol.isMandatorySampleCategoryIdEmpty()) {

                // use the first category
                Integer categoryId = speciesProtocol.getMandatorySampleCategoryId().get(0);
                selectedCategory = getSampleCategoryModel().getCategoryById(categoryId);

                if (categories.contains(selectedCategory)) {

                    // ok can use this category
                    if (log.isInfoEnabled()) {
                        log.info("Use first category from protocol: " + categoryId + " :: " + selectedCategory);
                    }
                } else {

                    // can't use this category, not in universe
                    selectedCategory = null;
                }
            }
        }

        if (selectedCategory == null) {

            // by default use the first one
            if (!categories.isEmpty()) {
                selectedCategory = categories.get(0);

                if (log.isInfoEnabled()) {
                    log.info("Use default first category: " + selectedCategory);
                }
            }
        }
        return selectedCategory;
    }

    public List<CaracteristicQualitativeValue> getGenderValues() {
        checkOpened();
        if (genderValues == null) {
            if (log.isInfoEnabled()) {
                log.info("Loading genderValues");
            }
            genderValues = service.getSexCaracteristic().getQualitativeValue();
        }
        return genderValues;
    }

    public List<CaracteristicQualitativeValue> getDeadOrAliveValues() {
        checkOpened();
        if (deadOrAliveValues == null) {
            if (log.isInfoEnabled()) {
                log.info("Loading deadOrAliveValues");
            }
            deadOrAliveValues = service.getDeadOrAliveCaracteristic().getQualitativeValue();
        }
        return deadOrAliveValues;
    }

    public List<CaracteristicQualitativeValue> getCpsTypeValues() {
        checkOpened();
        if (cpsTypeValues == null) {
            if (log.isInfoEnabled()) {
                log.info("Loading cpsTypeValues");
            }
            cpsTypeValues = service.getCalcifiedStructureCaracteristic().getQualitativeValue();
        }
        return cpsTypeValues;
    }

//    public List<Species> getReferentSpeciesWithSurveyCode() {
//        return getReferentSpeciesWithSurveyCode(false);
//    }

    public List<Species> getReferentSpeciesWithSurveyCode(boolean restrictToProtocol) {
        checkOpened();
        if (referentSpeciesWithSurveyCode == null) {

            if (log.isInfoEnabled()) {
                log.info("Loading referentSpecies");
            }

            TaxonCache taxonCache = TaxonCaches.createSpeciesCacheWithoutVernacularCode(service, getProtocol());
            referentSpeciesWithSurveyCode = new ArrayList<>(getReferentSpecies());
            taxonCache.load(referentSpeciesWithSurveyCode);
        }

        List<Species> result;
        if (restrictToProtocol && isProtocolFilled()) {

            // On restreint uniquement aux espèces définies dans le protocole

            result = new ArrayList<>();
            Map<Integer, SpeciesProtocol> speciesProtocolMap = TuttiProtocols.toSpeciesProtocolMap(getProtocol());
            result.addAll(referentSpeciesWithSurveyCode
                                  .stream()
                                  .filter(aSpecies -> speciesProtocolMap.containsKey(aSpecies.getReferenceTaxonId()))
                                  .collect(Collectors.toList()));

        } else {

            // On utilise la liste complete des espèces référentes
            result = referentSpeciesWithSurveyCode;

        }
        return result;

    }

    public List<Species> getReferentBenthosWithSurveyCode(boolean restrictToProtocol) {
        checkOpened();
        if (referentBenthosWithSurveyCode == null) {

            if (log.isInfoEnabled()) {
                log.info("Loading referentBenthos");
            }
            TaxonCache taxonCache = TaxonCaches.createBenthosCacheWithoutVernacularCode(service, getProtocol());
            referentBenthosWithSurveyCode = new ArrayList<>(getReferentSpecies());
            taxonCache.load(referentBenthosWithSurveyCode);
        }

        List<Species> result;
        if (restrictToProtocol && isProtocolFilled()) {

            // On restreint uniquement aux espèces définies dans le protocole

            result = new ArrayList<>();
            Map<Integer, SpeciesProtocol> speciesProtocolMap = TuttiProtocols.toBenthosProtocolMap(getProtocol());
            result.addAll(referentBenthosWithSurveyCode
                                  .stream()
                                  .filter(aSpecies -> speciesProtocolMap.containsKey(aSpecies.getReferenceTaxonId()))
                                  .collect(Collectors.toList()));

        } else {

            // On utilise la liste complete des espèces référentes
            result = referentBenthosWithSurveyCode;

        }
        return result;
    }

    public List<Species> getSpecies() {
        if (species == null) {
            if (log.isInfoEnabled()) {
                log.info("Loading allSpecies");
            }
            species = service.getAllSpecies();
        }
        return species;
    }

    public List<Species> getReferentSpecies() {
        if (referentSpecies == null) {
            if (log.isInfoEnabled()) {
                log.info("Loading allReferentSpecies");
            }
            referentSpecies = service.getAllReferentSpecies();
        }
        return referentSpecies;
    }

    public List<Person> getPersons() {
        checkOpened();
        if (persons == null) {
            if (log.isInfoEnabled()) {
                log.info("Loading allPerson");
            }
            persons = service.getAllPerson();
        }
        return persons;
    }

    public List<Vessel> getFishingVessels() {
        checkOpened();
        if (fishingVessels == null) {
            if (log.isInfoEnabled()) {
                log.info("Loading allfishingVessel");
            }
            fishingVessels = service.getAllFishingVessel();
        }
        return fishingVessels;
    }

    public List<Vessel> getScientificVessels() {
        checkOpened();
        if (scientificVessels == null) {
            if (log.isInfoEnabled()) {
                log.info("Loading allScientificVessel");
            }
            scientificVessels = service.getAllScientificVessel();
        }
        return scientificVessels;
    }

    public List<Gear> getFishingGears() {
        checkOpened();
        if (fishingGears == null) {
            if (log.isInfoEnabled()) {
                log.info("Loading allFishingGear");
            }
            fishingGears = service.getAllFishingGear();
        }
        return fishingGears;
    }

    public List<Gear> getScientificGears() {
        checkOpened();
        if (scientificGears == null) {
            if (log.isInfoEnabled()) {
                log.info("Loading allScientificGear");
            }
            scientificGears = service.getAllScientificGear();
        }
        return scientificGears;
    }

    public List<Caracteristic> getDefaultIndividualObservationCaracteristics() {
        if (defaultIndividualObservationCaracteristics == null) {

            if (log.isInfoEnabled()) {
                log.info("Loading defaultIndividualObservationCaracteristics");
            }
            defaultIndividualObservationCaracteristics = service.getDefaultIndividualObservationCaracteristics();
        }
        return defaultIndividualObservationCaracteristics;
    }

    private void resetProgram() {
        program = null;
        resetCruise();
    }

    public void resetCruise() {
        cruise = null;
        resetFishingOperation();
        closeCruiseCache();
    }

    public void resetFishingOperation() {
        fishingOperation = null;
        validationContext.resetExistingFishingOperations();
    }

    public void resetProtocol() {
        if (service != null) {
            service.setProtocol(null);
        }
        lengthStepCaracteristics = null;
        maturityCaracteristics = null;
        defaultIndividualObservationCaracteristics = null;
        // survey code must be refresh
        resetSpecies();
        closeCruiseCache();
    }

    public void resetGears() {
        fishingGears = null;
        scientificGears = null;
    }

    public void resetVessels() {
        fishingVessels = null;
        scientificVessels = null;
    }

    public void resetPersons() {
        persons = null;
    }

    public void resetSpecies() {
        species = null;
        referentSpecies = null;
        referentSpeciesWithSurveyCode = null;
        referentBenthosWithSurveyCode = null;
    }

    public void resetCaracteristics() {
        caracteristics = null;
        caracteristicsWithProtected = null;
        genderValues = null;
        deadOrAliveValues = null;
    }

    public void resetValidationDataContext() {
        validationContext.reset();
    }

    public PersistenceService getService() {
        return service;
    }

    public TuttiValidationDataContextSupport getValidationContext() {
        return validationContext;
    }

    public void setValidationContext(TuttiValidationDataContextSupport validationContext) {
        Preconditions.checkNotNull(validationContext, "cant set a null validation context");
        this.validationContext = validationContext;
    }

    protected void checkOpened() {
        Preconditions.checkState(service != null, "No persistence service assigned!");
    }

    public void closeCruiseCache() {
        if (!isCruiseCacheLoaded()) {
            return;
        }
        if (log.isInfoEnabled()) {
            log.info("Close cruise cache: " + cruiseCache);
        }

        cruiseCache.close();
        cruiseCache = null;
    }
}

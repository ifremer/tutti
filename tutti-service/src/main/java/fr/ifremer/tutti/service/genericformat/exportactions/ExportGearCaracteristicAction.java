package fr.ifremer.tutti.service.genericformat.exportactions;

/*
 * #%L
 * Tutti :: Service
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Lists;
import fr.ifremer.tutti.persistence.entities.CaracteristicMap;
import fr.ifremer.tutti.persistence.entities.data.Cruise;
import fr.ifremer.tutti.persistence.entities.referential.Gear;
import fr.ifremer.tutti.persistence.entities.referential.Gears;
import fr.ifremer.tutti.service.PersistenceService;
import fr.ifremer.tutti.service.genericformat.GenericFormatExportContext;
import fr.ifremer.tutti.service.genericformat.csv.GearCaracteristicRow;
import fr.ifremer.tutti.service.genericformat.producer.CsvProducerForGearCaracteristics;
import org.nuiton.jaxx.application.ApplicationTechnicalException;

import java.util.List;

import static org.nuiton.i18n.I18n.t;

/**
 * Created on 3/28/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.14.3
 */
public class ExportGearCaracteristicAction extends ExportCruiseActionSupport {

    public ExportGearCaracteristicAction(PersistenceService persistenceService) {
        super(persistenceService);
    }

    @Override
    public void execute(GenericFormatExportContext exportContext, Cruise cruise) {

        exportContext.increments(t("tutti.service.genericFormat.exportCruise.exportGearCaracteristics", cruise.getName()));

        try {

            CsvProducerForGearCaracteristics producerForGearCaracteristics = exportContext.getProducerForGearCaracteristics();

            List<GearCaracteristicRow> rows = Lists.newArrayList();

            for (Gear gear : cruise.getGear()) {

                CaracteristicMap caracteristics = persistenceService.getGearCaracteristics(cruise.getIdAsInt(), gear.getIdAsInt(), gear.getRankOrder());
                Gear gearWithCaracteristics = Gears.newGear(gear);
                gearWithCaracteristics.setCaracteristics(caracteristics);

                List<GearCaracteristicRow> dataToExport = producerForGearCaracteristics.getDataToExport(cruise, gearWithCaracteristics);
                rows.addAll(dataToExport);

            }

            producerForGearCaracteristics.write(rows);

        } catch (Exception e) {
            throw new ApplicationTechnicalException(t("tutti.service.genericFormat.export.gearCaracteristics.error"), e);
        }

    }
    
}
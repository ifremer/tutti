package fr.ifremer.tutti.service.csv;

/*
 * #%L
 * Tutti :: Service
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Lists;
import fr.ifremer.tutti.persistence.entities.TuttiEntities;
import fr.ifremer.tutti.persistence.entities.TuttiEntity;
import fr.ifremer.tutti.persistence.entities.referential.Species;
import fr.ifremer.tutti.persistence.entities.referential.Speciess;
import org.nuiton.csv.ExportableColumn;
import org.nuiton.csv.ValueFormatter;
import org.nuiton.csv.ext.AbstractImportExportModel;

import java.util.List;
import java.util.Map;

/**
 * Created on 2/5/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.13
 */
public abstract class AbstractTuttiImportExportModel<M> extends AbstractImportExportModel<M> {

    public AbstractTuttiImportExportModel(char separator) {
        super(separator);
    }

    public <E extends TuttiEntity> void newForeignKeyColumn(String propertyName, Class<E> entityType, String foreignKeyName, Map<String, E> universe) {
        newMandatoryColumn(propertyName, propertyName, new ForeignKeyParserFormatter<>(entityType, foreignKeyName, universe));
    }

    public <E extends TuttiEntity> void newForeignKeyColumn(String headerName, String propertyName, Class<E> entityType, String foreignKeyName, Map<String, E> universe) {
        newMandatoryColumn(headerName, propertyName, new ForeignKeyParserFormatter<>(entityType, foreignKeyName, universe));
    }

    public void newSpeciesForeignKeyColumn(String propertyName, List<Species> species) {
        if (species == null) {
            species = Lists.newArrayList();
        }
        Map<String, Species> universe = Speciess.splitReferenceSpeciesByReferenceTaxonId(species);
        newMandatoryColumn(propertyName, propertyName, new ForeignKeyParserFormatter<>(Species.class, Species.PROPERTY_REFERENCE_TAXON_ID, universe));
    }

    public <E extends TuttiEntity> void newForeignKeyColumn(String propertyName, Class<E> entityType, List<E> list) {
        if (list == null) {
            list = Lists.newArrayList();
        }
        Map<String, E> universe = TuttiEntities.splitById(list);
        newMandatoryColumn(propertyName, propertyName, new ForeignKeyParserFormatter<>(entityType, TuttiEntity.PROPERTY_ID, universe));
    }

    public <T> ExportableColumn<M, T> newNullableColumnForExport(String headerName, String propertyName, ValueFormatter<T> valueFormatter) {
        return modelBuilder.newColumnForExport(headerName, new BeanNullableGetter<>(propertyName), valueFormatter);
    }

    public ExportableColumn<M, String> newNullableColumnForExport(String headerName, String propertyName) {
        return newNullableColumnForExport(headerName, propertyName, TuttiCsvUtil.STRING);
    }

    public <T> ExportableColumn<M, T> newIndexNullableColumnForExport(String headerName, String collectionName, int order, String propertyName, ValueFormatter<T> valueFormatter) {
        return modelBuilder.newColumnForExport(headerName, new BeanIndexNullableGetter<>(collectionName, order, propertyName), valueFormatter);
    }

    public ExportableColumn<M, String> newIndexNullableColumnForExport(String headerName, String collectionName, int order, String propertyName) {
        return newIndexNullableColumnForExport(headerName, collectionName, order, propertyName, TuttiCsvUtil.STRING);
    }

}

package fr.ifremer.tutti.service.genericformat.exportactions;

/*
 * #%L
 * Tutti :: Service
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.adagio.core.dao.referential.ObjectTypeCode;
import fr.ifremer.tutti.persistence.entities.TuttiEntities;
import fr.ifremer.tutti.persistence.entities.data.Attachment;
import fr.ifremer.tutti.persistence.entities.data.Cruise;
import fr.ifremer.tutti.persistence.entities.referential.TuttiLocation;
import fr.ifremer.tutti.service.PersistenceService;
import fr.ifremer.tutti.service.genericformat.GenericFormatExportContext;
import fr.ifremer.tutti.service.genericformat.csv.AttachmentRow;
import fr.ifremer.tutti.service.genericformat.csv.SurveyRow;
import fr.ifremer.tutti.service.genericformat.producer.CsvProducerForAttachment;
import fr.ifremer.tutti.service.genericformat.producer.CsvProducerForSurvey;
import org.nuiton.jaxx.application.ApplicationTechnicalException;

import java.util.ArrayList;
import java.util.List;

import static org.nuiton.i18n.I18n.t;

/**
 * Created on 3/28/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.14.3
 */
public class ExportSurveyAction extends ExportCruiseActionSupport {

    private final String countryId;

    public ExportSurveyAction(PersistenceService persistenceService, String countryId) {
        super(persistenceService);
        this.countryId = countryId;
    }

    @Override
    public void execute(GenericFormatExportContext exportContext, Cruise cruise) {

        exportContext.increments(t("tutti.service.genericFormat.exportCruise.exportSurvey", cruise.getName()));

        List<TuttiLocation> allCountry = persistenceService.getAllCountry();

        TuttiLocation country = TuttiEntities.splitById(allCountry).get(countryId);

        try {

            CsvProducerForSurvey producerForSurvey = exportContext.getProducerForSurvey();

            SurveyRow surveyRow = producerForSurvey.getDataToExport(cruise, country);
            producerForSurvey.write(surveyRow);

        } catch (Exception e) {
            throw new ApplicationTechnicalException(t("tutti.service.genericFormat.export.survey.error"), e);
        }

        if (exportContext.isExportAttachments()) {

            exportAttachments(exportContext, cruise);

        }

    }

    protected void exportAttachments(GenericFormatExportContext exportContext, Cruise cruise) {

        List<Attachment> attachments = persistenceService.getAllAttachments(ObjectTypeCode.SCIENTIFIC_CRUISE, cruise.getIdAsInt());

        CsvProducerForAttachment producerForAttachment = exportContext.getProducerForAttachment();
        List<AttachmentRow> attachmentRows = new ArrayList<>();
        producerForAttachment.addAttachments(attachments, attachmentRows);

        try {
            producerForAttachment.write(attachmentRows);
        } catch (Exception e) {
            throw new ApplicationTechnicalException(t("tutti.service.genericFormat.export.attachment.error"), e);
        }
    }

}
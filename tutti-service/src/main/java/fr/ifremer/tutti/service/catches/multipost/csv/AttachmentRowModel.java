package fr.ifremer.tutti.service.catches.multipost.csv;

/*
 * #%L
 * Tutti :: Service
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.service.catches.multipost.MultiPostConstants;
import fr.ifremer.tutti.service.csv.AbstractTuttiImportExportModel;
import org.nuiton.csv.ValueFormatter;

import java.io.File;

/**
 * @author Kevin Morin - kmorin@codelutin.com
 * @since 2.2
 */
public class AttachmentRowModel extends AbstractTuttiImportExportModel<AttachmentRow> {

    private AttachmentRowModel(final File directory) {
        super(MultiPostConstants.CSV_SEPARATOR);

        newColumnForImportExport(AttachmentRow.ATTACHMENT_BATCH_ID);

        newColumnForImportExport(AttachmentRow.ATTACHMENT_NAME);

        newColumnForImportExport(AttachmentRow.ATTACHMENT_COMMENT);

        newColumnForExport(AttachmentRow.ATTACHMENT_FILE,
                           new ValueFormatter<File>() {

                               @Override
                               public String format(File value) {
                                   return value != null ? value.getName() : null;
                               }
                           });
        newMandatoryColumn(AttachmentRow.ATTACHMENT_FILE,
                           value -> {
                               return new File(directory, value);
                           });

    }

    public static AttachmentRowModel forExport() {
        return new AttachmentRowModel(null);
    }

    public static AttachmentRowModel forImport(File directory) {
        return new AttachmentRowModel(directory);
    }

    @Override
    public AttachmentRow newEmptyInstance() {
        return new AttachmentRow();
    }
}

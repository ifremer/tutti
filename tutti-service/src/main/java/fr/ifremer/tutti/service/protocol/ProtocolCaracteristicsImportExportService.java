package fr.ifremer.tutti.service.protocol;

/*
 * #%L
 * Tutti :: Service
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2016 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import com.google.common.collect.Maps;
import com.google.common.io.Files;
import fr.ifremer.tutti.persistence.entities.protocol.CaracteristicMappingRow;
import fr.ifremer.tutti.persistence.entities.protocol.CaracteristicMappingRowBean;
import fr.ifremer.tutti.persistence.entities.protocol.CaracteristicType;
import fr.ifremer.tutti.persistence.entities.protocol.MaturityCaracteristic;
import fr.ifremer.tutti.persistence.entities.protocol.MaturityCaracteristics;
import fr.ifremer.tutti.persistence.entities.protocol.TuttiProtocol;
import fr.ifremer.tutti.persistence.entities.referential.Caracteristic;
import fr.ifremer.tutti.service.AbstractTuttiService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.csv.Export;
import org.nuiton.csv.Import;
import org.nuiton.csv.ImportRuntimeException;
import org.nuiton.csv.ext.CsvReaders;
import org.nuiton.jaxx.application.ApplicationTechnicalException;

import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.io.Reader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

import static org.nuiton.i18n.I18n.t;

/**
 * Created on 11/05/16.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 4.5.1
 */
public class ProtocolCaracteristicsImportExportService extends AbstractTuttiService {

    /** Logger. */
    private static final Log log = LogFactory.getLog(ProtocolCaracteristicsImportExportService.class);

    public void importProtocolCaracteristic(File file,
                                            TuttiProtocol protocol,
                                            Map<String, Caracteristic> caracteristicMap) throws IOException {
        if (log.isInfoEnabled()) {
            log.info("Will import protocol caracteristic from file: " + file);
        }

        List<String> lengthClassesPmfmIds = new ArrayList<>(protocol.getLengthClassesPmfmId());
        List<String> individualObservationPmfmId = new ArrayList<>(protocol.getIndividualObservationPmfmId());
        Map<String, MaturityCaracteristic> maturityCaracteristics = new LinkedHashMap<>();
        if (!protocol.isMaturityCaracteristicsEmpty()) {
            maturityCaracteristics.putAll(Maps.uniqueIndex(protocol.getMaturityCaracteristics(), MaturityCaracteristic::getId));
        }

        Map<String, CaracteristicMappingRow> rowsByCaracteristicId = new LinkedHashMap<>();
        if (!protocol.isCaracteristicMappingEmpty()) {
            rowsByCaracteristicId.putAll(Maps.uniqueIndex(protocol.getCaracteristicMapping(), CaracteristicMappingRow::getPmfmId));
        }

        try (Reader reader = Files.newReader(file, StandardCharsets.UTF_8)) {

            CaracteristicRowModel csvModel = CaracteristicRowModel.forImport(getCsvSeparator(), caracteristicMap);
            try (Import<CaracteristicRow> importer = Import.newImport(csvModel, reader)) {

                for (CaracteristicRow bean : importer) {

                    CaracteristicType caracteristicType = bean.getPmfmType();
                    Caracteristic caracteristic = bean.getPmfm();
                    String id = caracteristic.getId();
                    switch (caracteristicType) {

                        case INDIVIDUAL_OBSERVATION:
                            individualObservationPmfmId.add(id);
                            break;
                        case LENGTH_STEP:
                            lengthClassesPmfmIds.add(id);
                            break;
                        case MATURITY:
                            MaturityCaracteristic maturityCaracteristic = maturityCaracteristics.get(id);
                            if (maturityCaracteristic == null) {
                                maturityCaracteristic = MaturityCaracteristics.newMaturityCaracteristic();
                                maturityCaracteristic.setId(id);
                                maturityCaracteristic.setMatureStateIds(bean.getMatureStateIds());
                                maturityCaracteristics.put(id, maturityCaracteristic);
                            }
                            break;

                        case VESSEL_USE_FEATURE:
                        case GEAR_USE_FEATURE:
                            CaracteristicMappingRow row = rowsByCaracteristicId.get(id);
                            if (row == null) {
                                row = new CaracteristicMappingRowBean();
                                row.setPmfmId(id);
                                row.setTab(caracteristicType.name());
                                rowsByCaracteristicId.put(id, row);
                            }
                            break;

                    }

                }

            }

        } catch (ImportRuntimeException e) {
            throw e;
        } catch (Exception e) {
            throw new ImportRuntimeException("Could not import protocol [" + protocol.getName() + "] caracteristic from file " + file, e);
        }

        protocol.setLengthClassesPmfmId(lengthClassesPmfmIds);
        protocol.setIndividualObservationPmfmId(individualObservationPmfmId);
        protocol.setMaturityCaracteristics(new ArrayList<>(maturityCaracteristics.values()));
        protocol.setCaracteristicMapping(new ArrayList<>(rowsByCaracteristicId.values()));

    }

    public void exportAllCaracteristic(File file, Map<String, Caracteristic> caracteristicMap) {

        if (log.isInfoEnabled()) {
            log.info("Will export all caracteristics to file: " + file);
        }

        PmfmIdToCaracteristicRowFunction function = new PmfmIdToCaracteristicRowFunction(caracteristicMap);

        List<CaracteristicRow> rows = caracteristicMap.keySet().stream().map(function).collect(Collectors.toList());

        try (BufferedWriter writer = Files.newWriter(file, StandardCharsets.UTF_8)) {

            CaracteristicRowModel csvModel = CaracteristicRowModel.forExport(getCsvSeparator());
            Export export = Export.newExport(csvModel, rows);
            export.write(writer);

        } catch (Exception e) {
            throw new ImportRuntimeException(t("tutti.service.protocol.export.caracteristics.all.error", file), e);
        }

    }

    public void exportProtocolCaracteristic(File file,
                                            TuttiProtocol protocol,
                                            Map<String, Caracteristic> caracteristicMap) {

        if (log.isInfoEnabled()) {
            log.info("Will export all caracteristics to file: " + file);
        }

        List<CaracteristicRow> rows = new ArrayList<>();

        PmfmIdToCaracteristicRowFunction function = new PmfmIdToCaracteristicRowFunction(caracteristicMap);

        if (!protocol.isLengthClassesPmfmIdEmpty()) {
            for (String pmfmId : protocol.getLengthClassesPmfmId()) {

                CaracteristicRow row = getCaracteristicRow(CaracteristicType.LENGTH_STEP, pmfmId, function);
                rows.add(row);

            }
        }

        if (!protocol.isIndividualObservationPmfmIdEmpty()) {
            for (String pmfmId : protocol.getIndividualObservationPmfmId()) {

                CaracteristicRow row = getCaracteristicRow(CaracteristicType.INDIVIDUAL_OBSERVATION, pmfmId, function);
                rows.add(row);

            }

        }

        if (!protocol.isMaturityCaracteristicsEmpty()) {

            for (MaturityCaracteristic maturityCaracteristic : protocol.getMaturityCaracteristics()) {

                CaracteristicRow row = getCaracteristicRow(CaracteristicType.MATURITY, maturityCaracteristic.getId(), function);
                if (!maturityCaracteristic.isMatureStateIdsEmpty()) {
                    row.setMatureStateIds(new LinkedHashSet<>(maturityCaracteristic.getMatureStateIds()));
                }
                rows.add(row);

            }

        }

        if (!protocol.isCaracteristicMappingEmpty()) {
            for (CaracteristicMappingRow mappingRow : protocol.getCaracteristicMapping()) {

                CaracteristicRow row = getCaracteristicRow(CaracteristicType.valueOf(mappingRow.getTab()), mappingRow.getPmfmId(), function);
                rows.add(row);

            }
        }

        try (BufferedWriter writer = Files.newWriter(file, StandardCharsets.UTF_8)) {

            CaracteristicRowModel csvModel = CaracteristicRowModel.forExport(getCsvSeparator());
            Export export = Export.newExport(csvModel, rows);
            export.write(writer);

        } catch (Exception e) {
            throw new ApplicationTechnicalException(t("tutti.service.protocol.export.caracteristics.protocol.error", protocol.getName(), file), e);
        }
    }

    public List<String> loadProtocolCaracteristicsImportColumns(File columnsFile) throws IOException {

        String[] headers = CsvReaders.getHeader(columnsFile, ';');
        List<String> result = new ArrayList<>(headers.length);
        for (String header : headers) {
            if ((header.startsWith("\"") && header.endsWith("\"")) ||
                    (header.startsWith("'") && header.endsWith("'"))) {
                header = header.substring(1, header.length() - 1);
            }
            result.add(header);
        }
        Collections.sort(result);
        return result;

    }

    private CaracteristicRow getCaracteristicRow(CaracteristicType caracteristicType, String id, PmfmIdToCaracteristicRowFunction function) {
        CaracteristicRow row = function.apply(id);
        row.setPmfmType(caracteristicType);
        return row;
    }


    private static class PmfmIdToCaracteristicRowFunction implements Function<String, CaracteristicRow> {

        private final Map<String, Caracteristic> caracteristicMap;

        public PmfmIdToCaracteristicRowFunction(Map<String, Caracteristic> caracteristicMap) {
            this.caracteristicMap = caracteristicMap;
        }

        @Override
        public CaracteristicRow apply(String input) {
            Caracteristic caracteristic = caracteristicMap.get(input);
            Preconditions.checkNotNull(caracteristic, "Could not find a caracteristic with id: " + input);
            CaracteristicRow result = new CaracteristicRow();
            result.setPmfm(caracteristic);
            return result;
        }

    }

    private char getCsvSeparator() {
        return context.getConfig().getCsvSeparator();
    }

}

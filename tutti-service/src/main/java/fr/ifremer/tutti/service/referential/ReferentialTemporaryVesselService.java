package fr.ifremer.tutti.service.referential;

/*
 * #%L
 * Tutti :: Service
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Lists;
import fr.ifremer.tutti.persistence.entities.TuttiEntities;
import fr.ifremer.tutti.persistence.entities.referential.Vessel;
import fr.ifremer.tutti.persistence.entities.referential.Vessels;
import fr.ifremer.tutti.service.AbstractTuttiService;
import fr.ifremer.tutti.service.DecoratorService;
import fr.ifremer.tutti.service.PersistenceService;
import fr.ifremer.tutti.service.TuttiServiceContext;
import fr.ifremer.tutti.service.referential.consumer.CsvConsumerForTemporaryVessel;
import fr.ifremer.tutti.service.referential.csv.VesselRow;
import fr.ifremer.tutti.service.referential.producer.CsvProducerForTemporaryVessel;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.csv.ImportRow;
import org.nuiton.jaxx.application.ApplicationTechnicalException;

import java.io.File;
import java.io.IOException;
import java.util.List;

import static org.nuiton.i18n.I18n.t;

/**
 * Created on 11/16/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.10
 */
public class ReferentialTemporaryVesselService extends AbstractTuttiService {

    /** Logger. */
    private static final Log log =
            LogFactory.getLog(ReferentialTemporaryVesselService.class);

    protected PersistenceService persistenceService;

    protected DecoratorService decoratorService;

    @Override
    public void setServiceContext(TuttiServiceContext context) {
        super.setServiceContext(context);
        persistenceService = getService(PersistenceService.class);
        decoratorService = getService(DecoratorService.class);
    }

    public ReferentialImportRequest<Vessel, String> createReferentialImportRequest() {

        List<Vessel> existingVessels = Lists.newArrayList(persistenceService.getAllVessel());
        return new ReferentialImportRequest<>(existingVessels, TuttiEntities.<Vessel>newIdFunction(), Vessels.GET_INTERNAL_REGISTRATION_CODE);

    }

    public ReferentialImportResult<Vessel> importTemporaryVessel(File file) {

        if (log.isInfoEnabled()) {
            log.info("Will import vessels from file: " + file);
        }

        ReferentialImportRequest<Vessel, String> requestResult = createReferentialImportRequest();

        try (CsvConsumerForTemporaryVessel consumer = new CsvConsumerForTemporaryVessel(file.toPath(), getCsvSeparator(), true)) {

            for (ImportRow<VesselRow> bean : consumer) {

                consumer.checkRow(bean, persistenceService, decoratorService, requestResult);

            }

        } catch (IOException e) {
            throw new ApplicationTechnicalException(t("tutti.service.referential.import.vessels.error", file), e);
        }

        return executeImportRequest(requestResult);

    }

    public ReferentialImportResult<Vessel> executeImportRequest(ReferentialImportRequest<Vessel, String> requestResult) {

        ReferentialImportResult<Vessel> result = new ReferentialImportResult<>();

        if (requestResult.withEntitiesToDelete()) {

            List<String> idsToDelete = requestResult.getIdsToDelete();
            persistenceService.deleteTemporaryVessels(idsToDelete);
            result.setNbRefDeleted(idsToDelete.size());

        }

        if (requestResult.withEntitiesToAdd()) {

            List<Vessel> entitiesToAdd = requestResult.getEntitiesToAdd();
            List<Vessel> entitiesAdded = persistenceService.addTemporaryVessels(entitiesToAdd);
            result.addAllRefsAdded(entitiesAdded);
        }

        if (requestResult.withEntitiesToUpdate()) {

            List<Vessel> entitiesToUpdate = requestResult.getEntitiesToUpdate();
            List<Vessel> entitiesUpdated = persistenceService.updateTemporaryVessels(entitiesToUpdate);
            result.addAllRefsUpdated(entitiesUpdated);

        }

        if (requestResult.withEntitiesToLink()) {

            List<Vessel> entitiesToLink = requestResult.getEntitiesToLink();
            List<Vessel> entitiesLinked = persistenceService.linkTemporaryVessels(entitiesToLink);
            result.addAllRefsLinked(entitiesLinked);

        }

        return result;

    }

    public List<Vessel> getTemporaryVessels() {

        if (log.isInfoEnabled()) {
            log.info("Getting all vessels from database");
        }
        List<Vessel> targetList = Lists.newArrayList(persistenceService.getAllVessel());
        if (log.isInfoEnabled()) {
            log.info("Got " + targetList.size() + " vessels");
        }
        List<Vessel> toExport = persistenceService.retainTemporaryVesselList(targetList);

        if (log.isInfoEnabled()) {
            log.info("Got " + toExport.size() + " temporary vessels");
        }
        return toExport;

    }

    public void exportExistingTemporaryVessel(File file) throws IOException {

        List<Vessel> toExport = getTemporaryVessels();
        exportTemporaryVessel(file, toExport);

    }

    public void exportTemporaryVesselExample(File file) throws IOException {

        List<Vessel> toExport = Lists.newArrayList();

        {
            Vessel v = Vessels.newVessel();
            v.setRegistrationCode("RegCode1");
            v.setName("Temporary fishing vessel name 1");
            v.setInternationalRegistrationCode("International registration code F1");
            v.setScientificVessel(false);
            toExport.add(v);
        }
        {
            Vessel v = Vessels.newVessel();
            v.setRegistrationCode("RegCode2");
            v.setName("Temporary fishing vessel name 2");
            v.setInternationalRegistrationCode("International registration code F2");
            v.setScientificVessel(false);
            toExport.add(v);
        }
        {
            Vessel v = Vessels.newVessel();
            v.setRegistrationCode("RegCode3");
            v.setName("Temporary scientific vessel name 3");
            v.setInternationalRegistrationCode("International registration code S3");
            v.setScientificVessel(true);
            toExport.add(v);
        }
        {
            Vessel v = Vessels.newVessel();
            v.setRegistrationCode("RegCode4");
            v.setName("Temporary scientific vessel name 4");
            v.setInternationalRegistrationCode("International registration code S4");
            v.setScientificVessel(true);
            toExport.add(v);
        }
        exportTemporaryVessel(file, toExport);

    }

    public void exportTemporaryVessel(File file, List<Vessel> toExport) throws IOException {

        try (CsvProducerForTemporaryVessel producerForTemporaryVessel = new CsvProducerForTemporaryVessel(file.toPath(), getCsvSeparator())) {

            List<VesselRow> dataToExport = producerForTemporaryVessel.getDataToExport(toExport);
            producerForTemporaryVessel.write(dataToExport);

        } catch (Exception e) {
            throw new ApplicationTechnicalException(t("tutti.service.referential.export.vessel.error", file), e);
        }

    }

    protected char getCsvSeparator() {
        return ';';
    }
}

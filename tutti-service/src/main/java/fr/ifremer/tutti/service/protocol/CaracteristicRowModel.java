/*
 * #%L
 * Tutti :: Service
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.ifremer.tutti.service.protocol;


import fr.ifremer.tutti.persistence.entities.protocol.CaracteristicType;
import fr.ifremer.tutti.persistence.entities.referential.Caracteristic;
import fr.ifremer.tutti.service.csv.AbstractTuttiImportExportModel;
import fr.ifremer.tutti.service.csv.TuttiCsvUtil;

import java.util.Map;

/**
 * Model to import / export {@link CaracteristicRow}.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.0
 */
public class CaracteristicRowModel extends AbstractTuttiImportExportModel<CaracteristicRow> {

    public static CaracteristicRowModel forImport(char separator, Map<String, Caracteristic> caracteristicMap) {

        CaracteristicRowModel result = new CaracteristicRowModel(separator);

        result.newForeignKeyColumn(CaracteristicRow.PROPERTY_PMFM_ID,
                                   CaracteristicRow.PROPERTY_PMFM,
                                   Caracteristic.class,
                                   Caracteristic.PROPERTY_ID,
                                   caracteristicMap);

        result.newMandatoryColumn(CaracteristicRow.PROPERTY_PMFM_TYPE,
                                  CaracteristicRow.PROPERTY_PMFM_TYPE,
                                  TuttiCsvUtil.newEnumByNameParserFormatter(CaracteristicType.class, true));

        result.newMandatoryColumn(CaracteristicRow.PROPERTY_MATURE_STATE_IDS, TuttiCsvUtil.STRING_SET_PARSER_FORMATTER);

        result.newIgnoredColumn(CaracteristicRow.PROPERTY_PMFM_PARAMETER_NAME);
        result.newIgnoredColumn(CaracteristicRow.PROPERTY_PMFM_MATRIX_NAME);
        result.newIgnoredColumn(CaracteristicRow.PROPERTY_PMFM_FRACTION_NAME);
        result.newIgnoredColumn(CaracteristicRow.PROPERTY_PMFM_METHOD_NAME);

        return result;

    }

    public static CaracteristicRowModel forExport(char separator) {

        CaracteristicRowModel result = new CaracteristicRowModel(separator);

        result.newColumnForExport(CaracteristicRow.PROPERTY_PMFM_ID);
        result.newColumnForExport(CaracteristicRow.PROPERTY_PMFM_TYPE, TuttiCsvUtil.newEnumByNameParserFormatter(CaracteristicType.class, true));
        result.newColumnForExport(CaracteristicRow.PROPERTY_PMFM_PARAMETER_NAME);
        result.newColumnForExport(CaracteristicRow.PROPERTY_PMFM_MATRIX_NAME);
        result.newColumnForExport(CaracteristicRow.PROPERTY_PMFM_FRACTION_NAME);
        result.newColumnForExport(CaracteristicRow.PROPERTY_PMFM_METHOD_NAME);
        result.newColumnForExport(CaracteristicRow.PROPERTY_MATURE_STATE_IDS, TuttiCsvUtil.STRING_SET_PARSER_FORMATTER);
        return result;

    }

    @Override
    public CaracteristicRow newEmptyInstance() {
        return new CaracteristicRow();
    }

    private CaracteristicRowModel(char separator) {
        super(separator);
    }

}

package fr.ifremer.tutti.service.genericformat.consumer;

/*
 * #%L
 * Tutti :: Service
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.entities.CaracteristicMap;
import fr.ifremer.tutti.persistence.entities.data.IndividualObservationBatch;
import fr.ifremer.tutti.persistence.entities.data.IndividualObservationBatchs;
import fr.ifremer.tutti.persistence.entities.referential.Caracteristic;
import fr.ifremer.tutti.service.csv.CaracteristicValueParseException;
import fr.ifremer.tutti.service.csv.CsvComsumer;
import fr.ifremer.tutti.service.genericformat.GenericFormatContextSupport;
import fr.ifremer.tutti.service.genericformat.GenericFormatImportEntityParserFactory;
import fr.ifremer.tutti.service.genericformat.GenericFormatImportOperationContext;
import fr.ifremer.tutti.service.genericformat.csv.IndividualObservationModel;
import fr.ifremer.tutti.service.genericformat.csv.IndividualObservationRow;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.csv.ImportRow;

import java.io.Serializable;
import java.nio.file.Path;

/**
 * Created on 2/11/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.14
 */
public class CsvConsumerForIndividualObservation extends CsvComsumer<IndividualObservationRow, IndividualObservationModel> {

    /** Logger. */
    private static final Log log = LogFactory.getLog(CsvConsumerForIndividualObservation.class);

    public CsvConsumerForIndividualObservation(Path file, char separator, GenericFormatImportEntityParserFactory parserFactory, boolean reportError) {
        super(file, IndividualObservationModel.forImport(separator, parserFactory), reportError);
    }

    public GenericFormatImportOperationContext validateRow(ImportRow<IndividualObservationRow> row, GenericFormatContextSupport importContext) {

        GenericFormatImportOperationContext operationContext = importContext.getValidationHelper().getFishingOperationContext(this, row, importContext);

        if (operationContext != null) {

            IndividualObservationRow bean = row.getBean();

            Caracteristic caracteristic = bean.getCaracteristic();
            if (caracteristic == null) {
                addCheckError(row, new NullPointerException("Caracteristique non définie."));
            }

            if (bean.getCaracteristicValue() == null) {
                addCheckError(row, new NullPointerException("Valeur de caracteristique non définie."));
            }

            // parse caracteristic value
            String value = (String) bean.getCaracteristicValue();
            try {
                Serializable serializable = importContext.parseCaracteristicValue(caracteristic, value);
                bean.setCaracteristicValue(serializable);
            } catch (CaracteristicValueParseException e) {
                addCheckError(row, e);
            }

        }

        reportError(row);

        return operationContext;

    }

    public void prepareRowForPersist(GenericFormatImportOperationContext operationContext, ImportRow<IndividualObservationRow> row) {

        IndividualObservationRow bean = row.getBean();
        Integer individualObservationBatchId = bean.getId();
        IndividualObservationBatch batch = operationContext.getIndividualObservationBatchById(individualObservationBatchId);
        if (batch == null) {

            if (log.isInfoEnabled()) {
                log.info("Detects individualObservationBatch: " + individualObservationBatchId);
            }

            batch = IndividualObservationBatchs.newIndividualObservationBatch();
            batch.setFishingOperation(operationContext.getFishingOperation());
            batch.setCaracteristics(new CaracteristicMap());
            batch.setComment(bean.getComment());
            batch.setSpecies(bean.getSpecies());
            batch.setId(individualObservationBatchId);
            batch.setBatchId(bean.getBatchId());
            batch.setRankOrder(bean.getRankOrder());
            operationContext.addIndividualObservationBatch(individualObservationBatchId, batch);

        }

        Caracteristic caracteristic = bean.getCaracteristic();
        Serializable caracteristicValue = bean.getCaracteristicValue();
        batch.getCaracteristics().put(caracteristic, caracteristicValue);

    }

}
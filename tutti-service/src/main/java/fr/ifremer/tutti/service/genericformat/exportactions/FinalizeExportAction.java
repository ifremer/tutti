package fr.ifremer.tutti.service.genericformat.exportactions;

/*
 * #%L
 * Tutti :: Service
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.service.csv.CsvProducer;
import fr.ifremer.tutti.service.genericformat.GenericFormatExportContext;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.jaxx.application.ApplicationTechnicalException;

import java.util.Set;

import static org.nuiton.i18n.I18n.t;

/**
 * Created on 3/28/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.14.3
 */
public class FinalizeExportAction extends ExportTechnicalActionSupport {

    /** Logger. */
    private static final Log log = LogFactory.getLog(FinalizeExportAction.class);

    @Override
    public void execute(GenericFormatExportContext exportContext) {

        try {

            Set<CsvProducer<?, ?>> producers = exportContext.producers();
            for (CsvProducer<?, ?> producer : producers) {

                if (!producer.wasTouched()) {
                    try {
                        if (log.isInfoEnabled()) {
                            log.info("Write empty file for csv producer: " + producer);
                        }
                        producer.writeEmpty();
                    } catch (Exception e) {
                        throw new ApplicationTechnicalException(t("tutti.service.genericFormat.export.emptyFile.error"), e);
                    }

                }
            }

        } finally {

            exportContext.close();

        }

    }

}

package fr.ifremer.tutti.service.genericformat.producer;

/*
 * #%L
 * Tutti :: Service
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.entities.data.BatchContainer;
import fr.ifremer.tutti.persistence.entities.data.MarineLitterBatch;
import fr.ifremer.tutti.service.csv.CsvProducer;
import fr.ifremer.tutti.service.genericformat.GenericFormatExportOperationContext;
import fr.ifremer.tutti.service.genericformat.csv.MarineLitterModel;
import fr.ifremer.tutti.service.genericformat.csv.MarineLitterRow;
import org.apache.commons.collections4.CollectionUtils;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

/**
 * Created on 2/6/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.13
 */
public class CsvProducerForMarineLitter extends CsvProducer<MarineLitterRow, MarineLitterModel> {

    public CsvProducerForMarineLitter(Path file, MarineLitterModel model) {
        super(file, model);
    }


    public List<MarineLitterRow> getDataToExport(GenericFormatExportOperationContext operationExportContext) {

        List<MarineLitterRow> rows = new ArrayList<>();

        BatchContainer<MarineLitterBatch> rootMarineLitterBatch = operationExportContext.getRootMarineLitterBatch();

        List<MarineLitterBatch> children = rootMarineLitterBatch.getChildren();
        if (CollectionUtils.isNotEmpty(children)) {
            for (MarineLitterBatch child : children) {
                MarineLitterRow row = new MarineLitterRow();
                row.setCruise(operationExportContext.getCruise());
                row.setFishingOperation(operationExportContext.getOperation());
                row.setMarineLitterBatch(child);
                rows.add(row);
            }
        }

        return rows;

    }

}

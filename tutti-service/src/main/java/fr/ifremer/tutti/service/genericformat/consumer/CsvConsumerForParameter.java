package fr.ifremer.tutti.service.genericformat.consumer;

/*
 * #%L
 * Tutti :: Service
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.entities.referential.Caracteristic;
import fr.ifremer.tutti.service.csv.CaracteristicValueParseException;
import fr.ifremer.tutti.service.csv.CsvComsumer;
import fr.ifremer.tutti.service.genericformat.GenericFormatContextSupport;
import fr.ifremer.tutti.service.genericformat.GenericFormatImportEntityParserFactory;
import fr.ifremer.tutti.service.genericformat.GenericFormatImportOperationContext;
import fr.ifremer.tutti.service.genericformat.csv.ParameterModel;
import fr.ifremer.tutti.service.genericformat.csv.ParameterRow;
import org.nuiton.csv.ImportRow;

import java.io.Serializable;
import java.nio.file.Path;

/**
 * Created on 2/11/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.14
 */
public class CsvConsumerForParameter extends CsvComsumer<ParameterRow, ParameterModel> {

    public CsvConsumerForParameter(Path file, char separator, GenericFormatImportEntityParserFactory parserFactory, boolean reportError) {
        super(file, ParameterModel.forImport(separator, parserFactory), reportError);
    }

    public GenericFormatImportOperationContext validateRow(ImportRow<ParameterRow> row, GenericFormatContextSupport importContext) {

        GenericFormatImportOperationContext operationContext = importContext.getValidationHelper().getFishingOperationContext(this, row, importContext);

        if (operationContext != null) {

            ParameterRow bean = row.getBean();

            bean.setFishingOperation(operationContext.getFishingOperation());

            //TODO Use a validator ?
            if (bean.getParameterType() == null) {
                //TODO Should done by parser ?
            }

            //TODO Use a validator ?
            Caracteristic caracteristic = bean.getCaracteristic();
            if (caracteristic == null) {
                //TODO Should done by parser ?
            }

            //TODO Use a validator ?
            if (bean.getValue() == null) {

            }

            // parse caracteristic value
            String value = (String) bean.getValue();
            try {
                Serializable serializable = importContext.parseCaracteristicValue(caracteristic, value);
                bean.setValue(serializable);
            } catch (CaracteristicValueParseException e) {
                addCheckError(row, e);
            }

            //TODO other checks ?

        }

        reportError(row);

        return operationContext;

    }

    public void prepareRowForPersist(GenericFormatImportOperationContext operationContext, ImportRow<ParameterRow> row) {

        ParameterRow bean = row.getBean();

        ParameterRow.ParameterType parameterType = bean.getParameterType();

        switch (parameterType) {

            case GEAR:

                operationContext.addGearUseFeature(bean.getCaracteristic(), bean.getValue());
                break;

            case VESSEL:

                operationContext.addVesselUseFeature(bean.getCaracteristic(), bean.getValue());
                break;

        }

    }

}
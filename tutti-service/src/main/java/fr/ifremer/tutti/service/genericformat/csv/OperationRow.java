package fr.ifremer.tutti.service.genericformat.csv;

/*
 * #%L
 * Tutti :: Service
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.entities.data.CatchBatch;
import fr.ifremer.tutti.persistence.entities.data.CatchBatchs;
import fr.ifremer.tutti.persistence.entities.data.FishingOperation;
import fr.ifremer.tutti.persistence.entities.referential.Gear;
import fr.ifremer.tutti.persistence.entities.referential.Person;
import fr.ifremer.tutti.persistence.entities.referential.TuttiLocation;
import fr.ifremer.tutti.persistence.entities.referential.Vessel;
import fr.ifremer.tutti.util.DateTimes;
import fr.ifremer.tutti.util.Numbers;

import java.util.Date;
import java.util.List;

/**
 * A row in a operation export.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.3
 */
public class OperationRow extends RowWithOperationContextSupport {

    public static final String PROPERTY_GEAR_RANK_ORDER = "gearRankOrder";

    public static final String PROPERTY_DURATION = "duration";

    public static final String PROPERTY_CATCH_TOTAL_WEIGHT = "catchTotalWeight";

    public static final String PROPERTY_CATCH_TOTAL_WEIGHT_COMPUTED = "catchTotalWeightComputed";

    public static final String PROPERTY_CATCH_TOTAL_SORTED_TREMIS_WEIGHT = "catchTotalSortedTremisWeight";

    public static final String PROPERTY_CATCH_TOTAL_SORTED_TREMIS_WEIGHT_COMPUTED = "catchTotalSortedTremisWeightComputed";

    public static final String PROPERTY_CATCH_TOTAL_SORTED_CAROUSSEL_WEIGHT = "catchTotalSortedCarousselWeight";

    public static final String PROPERTY_CATCH_TOTAL_SORTED_CAROUSSEL_WEIGHT_COMPUTED = "catchTotalSortedCarousselWeightComputed";

    public static final String PROPERTY_CATCH_TOTAL_SORTED_WEIGHT = "catchTotalSortedWeight";

    public static final String PROPERTY_CATCH_TOTAL_SORTED_WEIGHT_COMPUTED = "catchTotalSortedWeightComputed";

    public static final String PROPERTY_CATCH_TOTAL_UNSORTED_WEIGHT = "catchTotalUnsortedWeight";

    public static final String PROPERTY_CATCH_TOTAL_UNSORTED_WEIGHT_COMPUTED = "catchTotalUnsortedWeightComputed";

    public static final String PROPERTY_CATCH_TOTAL_REJECTED_WEIGHT = "catchTotalRejectedWeight";

    public static final String PROPERTY_CATCH_TOTAL_REJECTED_WEIGHT_COMPUTED = "catchTotalRejectedWeightComputed";

    public static final String PROPERTY_SPECIES_TOTAL_WEIGHT = "speciesTotalWeight";

    public static final String PROPERTY_SPECIES_TOTAL_WEIGHT_COMPUTED = "speciesTotalWeightComputed";

    public static final String PROPERTY_SPECIES_TOTAL_SORTED_WEIGHT = "speciesTotalSortedWeight";

    public static final String PROPERTY_SPECIES_TOTAL_SORTED_WEIGHT_COMPUTED = "speciesTotalSortedWeightComputed";

    public static final String PROPERTY_SPECIES_TOTAL_UNSORTED_WEIGHT = "speciesTotalUnsortedWeight";

    public static final String PROPERTY_SPECIES_TOTAL_UNSORTED_WEIGHT_COMPUTED = "speciesTotalUnsortedWeightComputed";

    public static final String PROPERTY_SPECIES_TOTAL_SAMPLE_SORTED_WEIGHT = "speciesTotalSampleSortedWeight";

    public static final String PROPERTY_SPECIES_TOTAL_SAMPLE_SORTED_WEIGHT_COMPUTED = "speciesTotalSampleSortedWeightComputed";

    public static final String PROPERTY_SPECIES_TOTAL_INERT_WEIGHT = "speciesTotalInertWeight";

    public static final String PROPERTY_SPECIES_TOTAL_INERT_WEIGHT_COMPUTED = "speciesTotalInertWeightComputed";

    public static final String PROPERTY_SPECIES_TOTAL_LIVING_NOT_ITEMIZED_WEIGHT = "speciesTotalLivingNotItemizedWeight";

    public static final String PROPERTY_SPECIES_TOTAL_LIVING_NOT_ITEMIZED_WEIGHT_COMPUTED = "speciesTotalLivingNotItemizedWeightComputed";

    public static final String PROPERTY_BENTHOS_TOTAL_WEIGHT = "benthosTotalWeight";

    public static final String PROPERTY_BENTHOS_TOTAL_WEIGHT_COMPUTED = "benthosTotalWeightComputed";

    public static final String PROPERTY_BENTHOS_TOTAL_SORTED_WEIGHT = "benthosTotalSortedWeight";

    public static final String PROPERTY_BENTHOS_TOTAL_SORTED_WEIGHT_COMPUTED = "benthosTotalSortedWeightComputed";

    public static final String PROPERTY_BENTHOS_TOTAL_UNSORTED_WEIGHT = "benthosTotalUnsortedWeight";

    public static final String PROPERTY_BENTHOS_TOTAL_UNSORTED_WEIGHT_COMPUTED = "benthosTotalUnsortedWeightComputed";

    public static final String PROPERTY_BENTHOS_TOTAL_SAMPLE_SORTED_WEIGHT = "benthosTotalSampleSortedWeight";

    public static final String PROPERTY_BENTHOS_TOTAL_SAMPLE_SORTED_WEIGHT_COMPUTED = "benthosTotalSampleSortedWeightComputed";

    public static final String PROPERTY_BENTHOS_TOTAL_INERT_WEIGHT = "benthosTotalInertWeight";

    public static final String PROPERTY_BENTHOS_TOTAL_INERT_WEIGHT_COMPUTED = "benthosTotalInertWeightComputed";

    public static final String PROPERTY_BENTHOS_TOTAL_LIVING_NOT_ITEMIZED_WEIGHT = "benthosTotalLivingNotItemizedWeight";

    public static final String PROPERTY_BENTHOS_TOTAL_LIVING_NOT_ITEMIZED_WEIGHT_COMPUTED = "benthosTotalLivingNotItemizedWeightComputed";

    public static final String PROPERTY_MARINE_LITTER_TOTAL_WEIGHT = "marineLitterTotalWeight";

    public static final String PROPERTY_MARINE_LITTER_TOTAL_WEIGHT_COMPUTED = "marineLitterTotalWeightComputed";

    private static final long serialVersionUID = 1L;

    public static final String PROPERTY_CATCH_OBJECT_ID = "catchObjectId";

    public static final String PROPERTY_FISHING_OPERATION_OBJECT_ID = "fishingOperationObjectId";

    protected Short gearRankOrder;

    protected CatchBatch catchBatch;

    protected Boolean catchTotalWeightComputed;

    protected Boolean catchTotalRejectedWeightComputed;

    protected Boolean speciesTotalSortedWeightComputed;

    protected Boolean speciesTotalInertWeightComputed;

    protected Boolean speciesTotalLivingNotItemizedWeightComputed;

    protected Boolean benthosTotalSortedWeightComputed;

    protected Boolean benthosTotalInertWeightComputed;

    protected Boolean benthosTotalLivingNotItemizedWeightComputed;

    protected Boolean marineLitterTotalWeightComputed;

    private Boolean speciesTotalWeightComputed;

    private Boolean speciesTotalUnsortedWeightComputed;

    private Boolean speciesTotalSampleSortedWeightComputed;

    private Boolean benthosTotalWeightComputed;

    private Boolean benthosTotalUnsortedWeightComputed;

    private Boolean benthosTotalSampleSortedWeightComputed;

    protected Integer fishingOperationObjectId;

    protected Integer catchObjectId;

    public static OperationRow newEmptyInstance() {

        OperationRow row = new OperationRow();
        row.forImport();
        row.setCatchBatch(CatchBatchs.newCatchBatch());
        return row;

    }

    public void setCatchBatch(CatchBatch catchBatch) {

        CatchBatch batch = CatchBatchs.newCatchBatch();

        if (catchBatch != null) {

            batch.setCatchTotalWeight(catchBatch.getCatchTotalWeight());
            batch.setCatchTotalComputedWeight(catchBatch.getCatchTotalComputedWeight());
            batch.setCatchTotalSortedTremisWeight(catchBatch.getCatchTotalSortedTremisWeight());
            batch.setCatchTotalSortedCarousselWeight(catchBatch.getCatchTotalSortedCarousselWeight());
            batch.setCatchTotalSortedComputedWeight(catchBatch.getCatchTotalSortedComputedWeight());
            batch.setCatchTotalUnsortedComputedWeight(catchBatch.getCatchTotalUnsortedComputedWeight());
            batch.setCatchTotalRejectedWeight(catchBatch.getCatchTotalRejectedWeight());
            batch.setCatchTotalRejectedComputedWeight(catchBatch.getCatchTotalRejectedComputedWeight());

            batch.setSpeciesTotalComputedWeight(catchBatch.getSpeciesTotalComputedWeight());
            batch.setSpeciesTotalSortedWeight(catchBatch.getSpeciesTotalSortedWeight());
            batch.setSpeciesTotalSortedComputedWeight(catchBatch.getSpeciesTotalSortedComputedWeight());
            batch.setSpeciesTotalUnsortedComputedWeight(catchBatch.getSpeciesTotalUnsortedComputedWeight());
            batch.setSpeciesTotalSampleSortedComputedWeight(catchBatch.getSpeciesTotalSampleSortedComputedWeight());
            batch.setSpeciesTotalLivingNotItemizedWeight(catchBatch.getSpeciesTotalLivingNotItemizedWeight());
            batch.setSpeciesTotalLivingNotItemizedComputedWeight(catchBatch.getSpeciesTotalLivingNotItemizedComputedWeight());
            batch.setSpeciesTotalInertWeight(catchBatch.getSpeciesTotalInertWeight());
            batch.setSpeciesTotalInertComputedWeight(catchBatch.getSpeciesTotalInertComputedWeight());

            batch.setBenthosTotalComputedWeight(catchBatch.getBenthosTotalComputedWeight());
            batch.setBenthosTotalSortedWeight(catchBatch.getBenthosTotalSortedWeight());
            batch.setBenthosTotalSortedComputedWeight(catchBatch.getBenthosTotalSortedComputedWeight());
            batch.setBenthosTotalUnsortedComputedWeight(catchBatch.getBenthosTotalUnsortedComputedWeight());
            batch.setBenthosTotalInertWeight(catchBatch.getBenthosTotalInertWeight());
            batch.setBenthosTotalInertComputedWeight(catchBatch.getBenthosTotalInertComputedWeight());
            batch.setBenthosTotalSampleSortedComputedWeight(catchBatch.getBenthosTotalSampleSortedComputedWeight());
            batch.setBenthosTotalLivingNotItemizedWeight(catchBatch.getBenthosTotalLivingNotItemizedWeight());
            batch.setBenthosTotalLivingNotItemizedComputedWeight(catchBatch.getBenthosTotalLivingNotItemizedComputedWeight());

            batch.setMarineLitterTotalWeight(catchBatch.getMarineLitterTotalWeight());
            batch.setMarineLitterTotalComputedWeight(catchBatch.getMarineLitterTotalComputedWeight());

            setCatchObjectId(catchBatch.getIdAsInt());

        }

        this.catchBatch = batch;

    }

    public void computeCatchValues(boolean exportSpecies, boolean exportBenthos, boolean exportMarineLitter) {

        setCatchTotalWeightComputed(Numbers.getValueOrComputedValueComputed(catchBatch.getCatchTotalWeight(), catchBatch.getCatchTotalComputedWeight()));
        setCatchTotalRejectedWeightComputed(Numbers.getValueOrComputedValueComputed(catchBatch.getCatchTotalRejectedWeight(), catchBatch.getCatchTotalRejectedComputedWeight()));


        if (exportSpecies) {

            setSpeciesTotalSortedWeightComputed(Numbers.getValueOrComputedValueComputed(catchBatch.getSpeciesTotalSortedWeight(), catchBatch.getSpeciesTotalSortedComputedWeight()));
            setSpeciesTotalInertWeightComputed(Numbers.getValueOrComputedValueComputed(catchBatch.getSpeciesTotalInertWeight(), catchBatch.getSpeciesTotalInertComputedWeight()));
            setSpeciesTotalLivingNotItemizedWeightComputed(Numbers.getValueOrComputedValueComputed(catchBatch.getSpeciesTotalLivingNotItemizedWeight(), catchBatch.getSpeciesTotalLivingNotItemizedComputedWeight()));

            speciesTotalWeightComputed = true;
            speciesTotalUnsortedWeightComputed = true;
            speciesTotalSampleSortedWeightComputed = true;

        } else {

            // remove species values

            catchBatch.setSpeciesTotalComputedWeight(null);
            catchBatch.setSpeciesTotalSortedWeight(null);
            catchBatch.setSpeciesTotalSortedWeight(null);
            catchBatch.setSpeciesTotalUnsortedComputedWeight(null);
            catchBatch.setSpeciesTotalSampleSortedComputedWeight(null);
            catchBatch.setSpeciesTotalLivingNotItemizedWeight(null);
            catchBatch.setSpeciesTotalLivingNotItemizedComputedWeight(null);
            catchBatch.setSpeciesTotalInertWeight(null);
            catchBatch.setSpeciesTotalInertComputedWeight(null);

        }

        if (exportBenthos) {

            setBenthosTotalSortedWeightComputed(Numbers.getValueOrComputedValueComputed(catchBatch.getBenthosTotalSortedWeight(), catchBatch.getBenthosTotalSortedComputedWeight()));
            setBenthosTotalInertWeightComputed(Numbers.getValueOrComputedValueComputed(catchBatch.getBenthosTotalInertWeight(), catchBatch.getBenthosTotalInertComputedWeight()));
            setBenthosTotalLivingNotItemizedWeightComputed(Numbers.getValueOrComputedValueComputed(catchBatch.getBenthosTotalLivingNotItemizedWeight(), catchBatch.getBenthosTotalLivingNotItemizedComputedWeight()));

            benthosTotalWeightComputed = true;
            benthosTotalUnsortedWeightComputed = true;
            benthosTotalSampleSortedWeightComputed = true;

        } else {

            // remove benthos values

            catchBatch.setBenthosTotalComputedWeight(null);
            catchBatch.setBenthosTotalSortedWeight(null);
            catchBatch.setBenthosTotalSortedComputedWeight(null);
            catchBatch.setBenthosTotalUnsortedComputedWeight(null);
            catchBatch.setBenthosTotalInertWeight(null);
            catchBatch.setBenthosTotalInertComputedWeight(null);
            catchBatch.setBenthosTotalSampleSortedComputedWeight(null);
            catchBatch.setBenthosTotalLivingNotItemizedWeight(null);
            catchBatch.setBenthosTotalLivingNotItemizedComputedWeight(null);

        }

        if (exportMarineLitter) {

            setMarineLitterTotalWeightComputed(Numbers.getValueOrComputedValueComputed(catchBatch.getMarineLitterTotalWeight(), catchBatch.getMarineLitterTotalComputedWeight()));

        } else {

            // remove marine litter values
            catchBatch.setMarineLitterTotalWeight(null);
            catchBatch.setMarineLitterTotalComputedWeight(null);
        }

    }

    public CatchBatch getCatchBatch() {
        return catchBatch;
    }

    public void setFishingOperation(FishingOperation fishingOperation) {
        super.setFishingOperation(fishingOperation);
        if (getGear() != null) {
            setGearRankOrder(getGear().getRankOrder());
        }
        setFishingOperationObjectId(fishingOperation.getIdAsInt());
    }

    public void setCatchObjectId(Integer catchObjectId) {
        this.catchObjectId = catchObjectId;
    }

    public void setFishingOperationObjectId(Integer fishingOperationObjectId) {
        this.fishingOperationObjectId = fishingOperationObjectId;
    }

    public void setGear(Gear gear) {
        getFishingOperation().setGear(gear);
    }

    public void setGearRankOrder(Short gearRankOrder) {
        this.gearRankOrder = gearRankOrder;
    }

    public void setVessel(Vessel vessel) {
        getFishingOperation().setVessel(vessel);
    }

    public void setSubStrata(TuttiLocation subStrata) {
        getFishingOperation().setSubStrata(subStrata);
    }

    public void setStrata(TuttiLocation strata) {
        getFishingOperation().setStrata(strata);
    }

    public void setRecorderPerson(List<Person> recorderPerson) {
        getFishingOperation().setRecorderPerson(recorderPerson);
    }

    public void setLocation(TuttiLocation location) {
        getFishingOperation().setLocation(location);
    }

    public void setSecondaryVessel(List<Vessel> secondaryVessel) {
        getFishingOperation().setSecondaryVessel(secondaryVessel);
    }

    public void setComment(String comment) {
        getFishingOperation().setComment(comment);
    }

    public void setFishingOperationValid(Boolean fishingOperationValid) {
        getFishingOperation().setFishingOperationValid(fishingOperationValid);
    }

    public void setTrawlDistance(Integer trawlDistance) {
        getFishingOperation().setTrawlDistance(trawlDistance);
    }

    public void setFishingOperationRectiligne(boolean fishingOperationRectiligne) {
        getFishingOperation().setFishingOperationRectiligne(fishingOperationRectiligne);
    }

    public void setGearShootingEndDate(Date gearShootingEndDate) {
        getFishingOperation().setGearShootingEndDate(gearShootingEndDate);
    }

    public void setGearShootingEndLongitude(Float gearShootingEndLongitude) {
        getFishingOperation().setGearShootingEndLongitude(gearShootingEndLongitude);
    }

    public void setGearShootingEndLatitude(Float gearShootingEndLatitude) {
        getFishingOperation().setGearShootingEndLatitude(gearShootingEndLatitude);
    }

    public void setGearShootingStartDate(Date gearShootingStartDate) {
        getFishingOperation().setGearShootingStartDate(gearShootingStartDate);
    }

    public void setGearShootingStartLongitude(Float gearShootingStartLongitude) {
        getFishingOperation().setGearShootingStartLongitude(gearShootingStartLongitude);
    }

    public void setGearShootingStartLatitude(Float gearShootingStartLatitude) {
        getFishingOperation().setGearShootingStartLatitude(gearShootingStartLatitude);
    }

    public void setBenthosTotalLivingNotItemizedWeight(Float benthosTotalLivingNotItemizedWeight) {
        catchBatch.setBenthosTotalLivingNotItemizedWeight(benthosTotalLivingNotItemizedWeight);
    }

    public void setCatchTotalRejectedWeight(Float catchTotalRejectedWeight) {
        catchBatch.setCatchTotalRejectedWeight(catchTotalRejectedWeight);
    }

    public void setMarineLitterTotalWeight(Float marineLitterTotalWeight) {
        catchBatch.setMarineLitterTotalWeight(marineLitterTotalWeight);
    }

    public void setBenthosTotalInertWeight(Float benthosTotalInertWeight) {
        catchBatch.setBenthosTotalInertWeight(benthosTotalInertWeight);
    }

    public void setCatchTotalSortedTremisWeight(Float catchTotalSortedTremisWeight) {
        catchBatch.setCatchTotalSortedTremisWeight(catchTotalSortedTremisWeight);
    }

    public void setCatchTotalWeight(Float catchTotalWeight) {
        catchBatch.setCatchTotalWeight(catchTotalWeight);
    }

    public void setBenthosTotalSortedWeight(Float benthosTotalSortedWeight) {
        catchBatch.setBenthosTotalSortedWeight(benthosTotalSortedWeight);
    }

    public void setCatchTotalSortedCarousselWeight(Float catchTotalSortedCarousselWeight) {
        catchBatch.setCatchTotalSortedCarousselWeight(catchTotalSortedCarousselWeight);
    }

    public void setSpeciesTotalSortedWeight(Float speciesTotalSortedWeight) {
        catchBatch.setSpeciesTotalSortedWeight(speciesTotalSortedWeight);
    }

    public void setSpeciesTotalLivingNotItemizedWeight(Float speciesTotalLivingNotItemizedWeight) {
        catchBatch.setSpeciesTotalLivingNotItemizedWeight(speciesTotalLivingNotItemizedWeight);
    }

    public void setSpeciesTotalInertWeight(Float speciesTotalInertWeight) {
        catchBatch.setSpeciesTotalInertWeight(speciesTotalInertWeight);
    }

    public Short getGearRankOrder() {
        return gearRankOrder;
    }

    public Integer getFishingOperationObjectId() {
        return fishingOperationObjectId;
    }

    public Integer getCatchObjectId() {
        return catchObjectId;
    }

    public Gear getGear() {
        return getFishingOperation().getGear();
    }

    public Vessel getVessel() {
        return getFishingOperation().getVessel();
    }

    public Date getGearShootingStartDate() {
        return getFishingOperation().getGearShootingStartDate();
    }

    public Float getGearShootingStartLatitude() {
        return getFishingOperation().getGearShootingStartLatitude();
    }

    public Float getGearShootingStartLongitude() {
        return getFishingOperation().getGearShootingStartLongitude();
    }

    public Date getGearShootingEndDate() {
        return getFishingOperation().getGearShootingEndDate();
    }

    public Float getGearShootingEndLatitude() {
        return getFishingOperation().getGearShootingEndLatitude();
    }

    public Float getGearShootingEndLongitude() {
        return getFishingOperation().getGearShootingEndLongitude();
    }

    public TuttiLocation getStrata() {
        return getFishingOperation().getStrata();
    }

    public TuttiLocation getSubStrata() {
        return getFishingOperation().getSubStrata();
    }

    public TuttiLocation getLocation() {
        return getFishingOperation().getLocation();
    }

    public Boolean getFishingOperationValid() {
        return getFishingOperation().getFishingOperationValid();
    }

    public boolean isFishingOperationRectiligne() {
        return getFishingOperation().isFishingOperationRectiligne();
    }

    public Integer getTrawlDistance() {
        return getFishingOperation().getTrawlDistance();
    }

    public String getComment() {
        return getFishingOperation().getComment();
    }

    public List<Person> getRecorderPerson() {
        return getFishingOperation().getRecorderPerson();
    }

    public List<Vessel> getSecondaryVessel() {
        return getFishingOperation().getSecondaryVessel();
    }

    public String getDuration() {
        return DateTimes.getDuration(
                getGearShootingStartDate(),
                getGearShootingEndDate(),
                "mm");
    }

    public Float getCatchTotalWeight() {
        return Numbers.getValueOrComputedValue(
                catchBatch.getCatchTotalWeight(),
                catchBatch.getCatchTotalComputedWeight());
    }

    public Float getCatchTotalSortedTremisWeight() {
        return catchBatch.getCatchTotalSortedTremisWeight();
    }

    public Float getCatchTotalSortedCarousselWeight() {
        return catchBatch.getCatchTotalSortedCarousselWeight();
    }

    public Float getCatchTotalSortedWeight() {
        return catchBatch.getCatchTotalSortedComputedWeight();
    }

    public Float getCatchTotalUnsortedWeight() {
        return catchBatch.getCatchTotalUnsortedComputedWeight();
    }

    public Float getCatchTotalRejectedWeight() {
        return Numbers.getValueOrComputedValue(
                catchBatch.getCatchTotalRejectedWeight(),
                catchBatch.getCatchTotalRejectedComputedWeight());
    }

    public Float getSpeciesTotalWeight() {
        return catchBatch.getSpeciesTotalComputedWeight();
    }

    public Float getSpeciesTotalSortedWeight() {
        return Numbers.getValueOrComputedValue(
                catchBatch.getSpeciesTotalSortedWeight(),
                catchBatch.getSpeciesTotalSortedComputedWeight());
    }

    public Float getSpeciesTotalUnsortedWeight() {
        return catchBatch.getSpeciesTotalUnsortedComputedWeight();
    }

    public Float getSpeciesTotalSampleSortedWeight() {
        return catchBatch.getSpeciesTotalSampleSortedComputedWeight();
    }

    public Float getSpeciesTotalLivingNotItemizedWeight() {
        return Numbers.getValueOrComputedValue(
                catchBatch.getSpeciesTotalLivingNotItemizedWeight(),
                catchBatch.getSpeciesTotalLivingNotItemizedComputedWeight());
    }

    public Float getSpeciesTotalInertWeight() {
        return Numbers.getValueOrComputedValue(
                catchBatch.getSpeciesTotalInertWeight(),
                catchBatch.getSpeciesTotalInertComputedWeight());
    }

    public Float getBenthosTotalWeight() {
        return catchBatch.getBenthosTotalComputedWeight();
    }

    public Float getBenthosTotalSortedWeight() {
        return Numbers.getValueOrComputedValue(
                catchBatch.getBenthosTotalSortedWeight(),
                catchBatch.getBenthosTotalSortedComputedWeight());
    }

    public Float getBenthosTotalUnsortedWeight() {
        return catchBatch.getBenthosTotalUnsortedComputedWeight();
    }

    public Float getBenthosTotalInertWeight() {
        return Numbers.getValueOrComputedValue(
                catchBatch.getBenthosTotalInertWeight(),
                catchBatch.getBenthosTotalInertComputedWeight());
    }

    public Float getBenthosTotalSampleSortedWeight() {
        return catchBatch.getBenthosTotalSampleSortedComputedWeight();
    }

    public Float getBenthosTotalLivingNotItemizedWeight() {
        return Numbers.getValueOrComputedValue(
                catchBatch.getBenthosTotalLivingNotItemizedWeight(),
                catchBatch.getBenthosTotalLivingNotItemizedComputedWeight());
    }

    public Float getMarineLitterTotalWeight() {
        return Numbers.getValueOrComputedValue(
                catchBatch.getMarineLitterTotalWeight(),
                catchBatch.getMarineLitterTotalComputedWeight());
    }

    public Boolean getCatchTotalWeightComputed() {
        return catchTotalWeightComputed;
    }

    public Boolean getCatchTotalRejectedWeightComputed() {
        return catchTotalRejectedWeightComputed;
    }

    public Boolean getCatchTotalSortedTremisWeightComputed() {
        return false;
    }

    public Boolean getCatchTotalSortedCarousselWeightComputed() {
        return false;
    }

    public Boolean getCatchTotalSortedWeightComputed() {
        return true;
    }

    public Boolean getCatchTotalUnsortedWeightComputed() {
        return true;
    }

    public Boolean getSpeciesTotalWeightComputed() {
        return speciesTotalWeightComputed;
    }

    public Boolean getSpeciesTotalUnsortedWeightComputed() {
        return speciesTotalUnsortedWeightComputed;
    }

    public Boolean getSpeciesTotalSampleSortedWeightComputed() {
        return speciesTotalSampleSortedWeightComputed;
    }

    public Boolean getSpeciesTotalSortedWeightComputed() {
        return speciesTotalSortedWeightComputed;
    }

    public Boolean getSpeciesTotalInertWeightComputed() {
        return speciesTotalInertWeightComputed;
    }

    public Boolean getSpeciesTotalLivingNotItemizedWeightComputed() {
        return speciesTotalLivingNotItemizedWeightComputed;
    }

    public Boolean getBenthosTotalSortedWeightComputed() {
        return benthosTotalSortedWeightComputed;
    }

    public Boolean getBenthosTotalInertWeightComputed() {
        return benthosTotalInertWeightComputed;
    }

    public Boolean getBenthosTotalLivingNotItemizedWeightComputed() {
        return benthosTotalLivingNotItemizedWeightComputed;
    }

    public Boolean getBenthosTotalWeightComputed() {
        return benthosTotalWeightComputed;
    }

    public Boolean getBenthosTotalUnsortedWeightComputed() {
        return benthosTotalUnsortedWeightComputed;
    }

    public Boolean getBenthosTotalSampleSortedWeightComputed() {
        return benthosTotalSampleSortedWeightComputed;
    }

    public Boolean getMarineLitterTotalWeightComputed() {
        return marineLitterTotalWeightComputed;
    }

    public void setCatchTotalWeightComputed(Boolean catchTotalWeightComputed) {
        this.catchTotalWeightComputed = catchTotalWeightComputed;
    }

    public void setCatchTotalRejectedWeightComputed(Boolean catchTotalRejectedWeightComputed) {
        this.catchTotalRejectedWeightComputed = catchTotalRejectedWeightComputed;
    }

    public void setSpeciesTotalSortedWeightComputed(Boolean speciesTotalSortedWeightComputed) {
        this.speciesTotalSortedWeightComputed = speciesTotalSortedWeightComputed;
    }

    public void setSpeciesTotalInertWeightComputed(Boolean speciesTotalInertWeightComputed) {
        this.speciesTotalInertWeightComputed = speciesTotalInertWeightComputed;
    }

    public void setSpeciesTotalLivingNotItemizedWeightComputed(Boolean speciesTotalLivingNotItemizedWeightComputed) {
        this.speciesTotalLivingNotItemizedWeightComputed = speciesTotalLivingNotItemizedWeightComputed;
    }

    public void setBenthosTotalSortedWeightComputed(Boolean benthosTotalSortedWeightComputed) {
        this.benthosTotalSortedWeightComputed = benthosTotalSortedWeightComputed;
    }

    public void setBenthosTotalInertWeightComputed(Boolean benthosTotalInertWeightComputed) {
        this.benthosTotalInertWeightComputed = benthosTotalInertWeightComputed;
    }

    public void setBenthosTotalLivingNotItemizedWeightComputed(Boolean benthosTotalLivingNotItemizedWeightComputed) {
        this.benthosTotalLivingNotItemizedWeightComputed = benthosTotalLivingNotItemizedWeightComputed;
    }

    public void setMarineLitterTotalWeightComputed(Boolean marineLitterTotalWeightComputed) {
        this.marineLitterTotalWeightComputed = marineLitterTotalWeightComputed;
    }

}

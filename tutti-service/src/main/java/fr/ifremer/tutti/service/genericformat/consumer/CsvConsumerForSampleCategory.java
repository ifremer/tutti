package fr.ifremer.tutti.service.genericformat.consumer;

/*
 * #%L
 * Tutti :: Service
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.entities.referential.Caracteristic;
import fr.ifremer.tutti.service.csv.CsvComsumer;
import fr.ifremer.tutti.service.genericformat.GenericFormatImportEntityParserFactory;
import fr.ifremer.tutti.service.genericformat.csv.SampleCategoryModel;
import fr.ifremer.tutti.service.genericformat.csv.SampleCategoryRow;
import org.apache.commons.lang3.StringUtils;
import org.nuiton.csv.ImportRow;

import java.nio.file.Path;
import java.util.Set;

import static org.nuiton.i18n.I18n.t;

/**
 * Created on 2/11/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.14
 */
public class CsvConsumerForSampleCategory extends CsvComsumer<SampleCategoryRow, SampleCategoryModel> {

    public CsvConsumerForSampleCategory(Path file, char separator, GenericFormatImportEntityParserFactory parserFactory, boolean reportError) {
        super(file, SampleCategoryModel.forImport(separator, parserFactory), reportError);
    }

    public void validateRow(ImportRow<SampleCategoryRow> row, Set<String> existingCodes, Set<Integer> existingCaracteristicIds) {

        if (row.isValid()) {

            SampleCategoryRow bean = row.getBean();

            String code = bean.getCode();

            if (StringUtils.isBlank(code)) {

                addCheckError(row, new IllegalStateException(t("tutti.service.genericFormat.import.sampleCategoryModel.error.noCode")));

            } else if (!existingCodes.add(code)) {
                addCheckError(row, new IllegalStateException(t("tutti.service.genericFormat.import.sampleCategoryModel.error.alreadyUsedCode", code)));
            }

            Caracteristic caracteristic = bean.getCaracteristic();
            if (caracteristic == null) {

                addCheckError(row, new IllegalStateException(t("tutti.service.genericFormat.import.sampleCategoryModel.error.noCaracteristic")));

            } else if (!existingCaracteristicIds.add(caracteristic.getIdAsInt())) {

                addCheckError(row, new IllegalStateException(t("tutti.service.genericFormat.import.sampleCategoryModel.error.alreadyUsedCaracteristic", caracteristic.getIdAsInt())));

            }

        }

        reportError(row);

    }

}
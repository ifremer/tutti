package fr.ifremer.tutti.service.bigfin.signs;

/*
 * #%L
 * Tutti :: Service
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.entities.referential.Caracteristic;
import fr.ifremer.tutti.persistence.entities.referential.CaracteristicQualitativeValue;

import java.util.Map;

/**
 * Created on 2/3/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.13
 */
public interface Sign {

    public abstract Integer getCategory();

    public abstract Integer getQualitativeValueId();

    /**
     * @param parentSignChildrenNb the number of subbatches of the parent
     *                             (eg 2 for a size batch with NONE sex subbatches and MALE subbatches)
     * @return true to use this value in the import file to replace a skipped category
     */
    public abstract boolean isNullEquivalent(int parentSignChildrenNb);

    public String getSign();

    public void registerSign(Caracteristic caracteristic, Map<Sign, CaracteristicQualitativeValue> map);

}

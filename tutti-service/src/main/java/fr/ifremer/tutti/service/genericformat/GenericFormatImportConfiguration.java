package fr.ifremer.tutti.service.genericformat;

/*
 * #%L
 * Tutti :: Service
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.model.ProgramDataModel;

import java.io.File;
import java.io.Serializable;

/**
 * Created on 2/23/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.14
 */
public class GenericFormatImportConfiguration implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * File to import.
     */
    private File importFile;

    /**
     * Path to report to generation.
     */
    private File reportFile;

    /**
     * Should perform clean weights after import ?
     */
    private boolean cleanWeights;

    /**
     * Should perform check weights after import ?
     */
    private boolean checkWeights;

    /**
     * Override protocol if same name ?
     */
    private boolean overrideProtocol;

    /**
     * Data to export.
     */
    private ProgramDataModel dataToExport;

    /**
     * Should we import species batches ?
     */
    private boolean importSpecies;

    /**
     * Should we import benthos batches ?
     */
    private boolean importBenthos;

    /**
     * Should we import marine litter batches ?
     */
    private boolean importMarineLitter;

    /**
     * Should we import accidental catches ?
     */
    private boolean importAccidentalCatch;

    /**
     * Should we import individual observations ?
     */
    private boolean importIndividualObservation;

    /**
     * Should we import attachments ?
     */
    private boolean importAttachments;

    /**
     * Should we update existing cruises ?
     */
    private boolean updateCruises;

    /**
     * Should we update existing operations ?
     */
    private boolean updateOperations;

    /**
     * Maximum rows in errors by file before stopping validation.
     */
    private int maximumRowsInErrorPerFile;

    private boolean authorizeObsoleteReferentials;

    public ProgramDataModel getDataToExport() {
        return dataToExport;
    }

    public void setDataToExport(ProgramDataModel dataToExport) {
        this.dataToExport = dataToExport;
    }

    public File getImportFile() {
        return importFile;
    }

    public void setImportFile(File importFile) {
        this.importFile = importFile;
    }

    public File getReportFile() {
        return reportFile;
    }

    public void setReportFile(File reportFile) {
        this.reportFile = reportFile;
    }

    public boolean isCleanWeights() {
        return cleanWeights;
    }

    public void setCleanWeights(boolean cleanWeights) {
        this.cleanWeights = cleanWeights;
    }

    public boolean isCheckWeights() {
        return checkWeights;
    }

    public void setCheckWeights(boolean checkWeights) {
        this.checkWeights = checkWeights;
    }

    public void setOverrideProtocol(boolean overrideProtocol) {
        this.overrideProtocol = overrideProtocol;
    }

    public boolean isOverrideProtocol() {
        return overrideProtocol;
    }

    public boolean isUpdateCruises() {
        return updateCruises;
    }

    public void setUpdateCruises(boolean updateCruises) {
        this.updateCruises = updateCruises;
    }

    public boolean isUpdateOperations() {
        return updateOperations;
    }

    public void setUpdateOperations(boolean updateOperations) {
        this.updateOperations = updateOperations;
    }

    public boolean isImportSpecies() {
        return importSpecies;
    }

    public void setImportSpecies(boolean importSpecies) {
        this.importSpecies = importSpecies;
    }

    public boolean isImportBenthos() {
        return importBenthos;
    }

    public void setImportBenthos(boolean importBenthos) {
        this.importBenthos = importBenthos;
    }

    public boolean isImportMarineLitter() {
        return importMarineLitter;
    }

    public void setImportMarineLitter(boolean importMarineLitter) {
        this.importMarineLitter = importMarineLitter;
    }

    public boolean isImportAccidentalCatch() {
        return importAccidentalCatch;
    }

    public void setImportAccidentalCatch(boolean importAccidentalCatch) {
        this.importAccidentalCatch = importAccidentalCatch;
    }

    public boolean isImportIndividualObservation() {
        return importIndividualObservation;
    }

    public void setImportIndividualObservation(boolean importIndividualObservation) {
        this.importIndividualObservation = importIndividualObservation;
    }

    public boolean isImportAttachments() {
        return importAttachments;
    }

    public void setImportAttachments(boolean importAttachments) {
        this.importAttachments = importAttachments;
    }

    public int getMaximumRowsInErrorPerFile() {
        return maximumRowsInErrorPerFile;
    }

    public void setMaximumRowsInErrorPerFile(int maximumRowsInErrorPerFile) {
        this.maximumRowsInErrorPerFile = maximumRowsInErrorPerFile;
    }

    public boolean isAuthorizeObsoleteReferentials() {
        return authorizeObsoleteReferentials;
    }

    public void setAuthorizeObsoleteReferentials(boolean authorizeObsoleteReferentials) {
        this.authorizeObsoleteReferentials = authorizeObsoleteReferentials;
    }
}

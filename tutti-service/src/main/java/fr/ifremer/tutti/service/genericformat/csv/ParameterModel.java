package fr.ifremer.tutti.service.genericformat.csv;

/*
 * #%L
 * Tutti :: Service
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.entities.data.Cruise;
import fr.ifremer.tutti.persistence.entities.data.FishingOperation;
import fr.ifremer.tutti.service.csv.AbstractTuttiImportExportModel;
import fr.ifremer.tutti.service.csv.TuttiCsvUtil;
import fr.ifremer.tutti.service.genericformat.GenericFormatImportEntityParserFactory;

/**
 * Model of a parameter export.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.3
 */
public class ParameterModel extends AbstractTuttiImportExportModel<ParameterRow> {

    public static ParameterModel forExport(char separator) {

        ParameterModel exportModel = new ParameterModel(separator);
        exportModel.forExport();
        return exportModel;

    }

    public static ParameterModel forImport(char separator, GenericFormatImportEntityParserFactory parserFactory) {

        ParameterModel importModel = new ParameterModel(separator);
        importModel.forImport(parserFactory);
        return importModel;

    }

    @Override
    public ParameterRow newEmptyInstance() {

        return ParameterRow.newEmptyInstance();

    }

    protected ParameterModel(char separator) {
        super(separator);
    }

    protected void forExport() {

        newColumnForExport("Annee", Cruise.PROPERTY_BEGIN_DATE, TuttiCsvUtil.YEAR);
        newColumnForExport("Serie", Cruise.PROPERTY_PROGRAM, TuttiCsvUtil.PROGRAM_FORMATTER);
        newColumnForExport("Serie_Partielle", Cruise.PROPERTY_SURVEY_PART);

        newColumnForExport("Code_Station", FishingOperation.PROPERTY_STATION_NUMBER);
        newColumnForExport("Id_Operation", FishingOperation.PROPERTY_FISHING_OPERATION_NUMBER, TuttiCsvUtil.INTEGER);
        newColumnForExport("Poche", FishingOperation.PROPERTY_MULTIRIG_AGGREGATION);

        newColumnForExport("Code_PMFM", ParameterRow.PROPERTY_CARACTERISTIC, TuttiCsvUtil.CARACTERISTIC_TECHNICAL_FORMATTER);
        newColumnForExport("Libelle_PMFM", ParameterRow.PROPERTY_CARACTERISTIC, TuttiCsvUtil.CARACTERISTIC_FORMATTER);
        newColumnForExport("Valeur", ParameterRow.PROPERTY_VALUE, TuttiCsvUtil.CARACTERISTIC_VALUE_FORMATTER);

        newColumnForExport("Type", ParameterRow.PROPERTY_PARAMETER_TYPE, TuttiCsvUtil.newEnumByNameParserFormatter(ParameterRow.ParameterType.class));
        newColumnForExport("Serie_Id", Cruise.PROPERTY_PROGRAM, TuttiCsvUtil.PROGRAM_TECHNICAL_FORMATTER);
        newColumnForExport("Valeur_Id", ParameterRow.PROPERTY_VALUE, TuttiCsvUtil.CARACTERISTIC_VALUE_TECHNICAL_FORMATTER);

    }

    protected void forImport(GenericFormatImportEntityParserFactory parserFactory) {

        newMandatoryColumn("Annee", Cruise.PROPERTY_BEGIN_DATE, TuttiCsvUtil.YEAR);
        newIgnoredColumn("Serie");
        newMandatoryColumn("Serie_Partielle", Cruise.PROPERTY_SURVEY_PART);

        newMandatoryColumn("Code_Station", FishingOperation.PROPERTY_STATION_NUMBER);
        newMandatoryColumn("Id_Operation", FishingOperation.PROPERTY_FISHING_OPERATION_NUMBER, TuttiCsvUtil.INTEGER);
        newMandatoryColumn("Poche", FishingOperation.PROPERTY_MULTIRIG_AGGREGATION);

        newMandatoryColumn("Code_PMFM", ParameterRow.PROPERTY_CARACTERISTIC, parserFactory.getCaracteristicParser());
        newIgnoredColumn("Libelle_PMFM");
        newIgnoredColumn("Valeur");

        newMandatoryColumn("Type", ParameterRow.PROPERTY_PARAMETER_TYPE, TuttiCsvUtil.newEnumByNameParserFormatter(ParameterRow.ParameterType.class));
        newMandatoryColumn("Serie_Id", Cruise.PROPERTY_PROGRAM, parserFactory.getProgramParser());
        newMandatoryColumn("Valeur_Id", ParameterRow.PROPERTY_VALUE);

    }

}

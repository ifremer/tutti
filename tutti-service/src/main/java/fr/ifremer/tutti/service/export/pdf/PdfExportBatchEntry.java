package fr.ifremer.tutti.service.export.pdf;

/*
 * #%L
 * Tutti :: Service
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.service.export.ExportBatchEntry;

/**
 * To store a species or batch entry within his speices informations,
 * his sorted weight, total weight and percentage amoong the total catch
 * weight.
 *
 * @since 3.0-rc-1
 */
public class PdfExportBatchEntry extends ExportBatchEntry {

    protected final String code;

    protected final String scientificName;

    protected final String vernacularCode;

    protected float percentage;

    public PdfExportBatchEntry(String code,
                               String scientificName,
                               String vernacularCode,
                               float sortedWeight, float totalWeight,
                               float catchTotalWeight) {
        super(null);

        this.code = code;
        this.scientificName = scientificName;
        this.vernacularCode = vernacularCode;

        addSortedWeight(sortedWeight);
        addTotalWeight(totalWeight);
        this.percentage = 100 * totalWeight / catchTotalWeight;
    }

    public String getCode() {
        return code;
    }

    public String getScientificName() {
        return scientificName;
    }

    public String getVernacularCode() {
        return vernacularCode;
    }

    public float getPercentage() {
        return percentage;
    }
}

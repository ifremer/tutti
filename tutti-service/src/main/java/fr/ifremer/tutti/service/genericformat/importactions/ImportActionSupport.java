package fr.ifremer.tutti.service.genericformat.importactions;

/*
 * #%L
 * Tutti :: Service
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.service.csv.CsvComsumer;
import fr.ifremer.tutti.service.genericformat.GenericFormatContextSupport;
import fr.ifremer.tutti.service.genericformat.GenericFormatCsvFileResult;

/**
 * Created on 3/3/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.14
 */
public abstract class ImportActionSupport {

    protected final GenericFormatContextSupport importContext;

    protected abstract boolean canExecute();

    protected abstract void doExecute();

    protected ImportActionSupport(GenericFormatContextSupport importContext) {
        this.importContext = importContext;
    }

    public void execute() {

        boolean doExecute = canExecute();

        if (doExecute) {

            doExecute();

        } else {

            skipExecute();

        }
    }

    protected void skipExecute() {
        // by default do nothing
    }

    protected void flushConsumer(CsvComsumer consumer, GenericFormatCsvFileResult importFileResult) {

        int maximumRowsInErrorPerFile = importContext.getImportRequest().getMaximumRowsInErrorPerFile();

        boolean fullyLoaded = consumer.getNbRowsInErrors() <= maximumRowsInErrorPerFile;
        importFileResult.flushErrors(consumer);
        importFileResult.setFullyLoaded(fullyLoaded);

    }

}

package fr.ifremer.tutti.service.csv;

/*
 * #%L
 * Tutti :: Service
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Charsets;
import com.google.common.base.Preconditions;
import com.google.common.io.Files;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.jaxx.application.ApplicationTechnicalException;

import java.io.BufferedWriter;
import java.io.Closeable;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created on 2/6/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.13
 */
public abstract class CsvProducer<O, M extends AbstractTuttiImportExportModel<O>> implements Closeable {

    /** Logger. */
    private static final Log log = LogFactory.getLog(CsvProducer.class);

    private final BufferedWriter writer;

    private final TuttiRepeatableExport<O> export;

    private boolean touch;

    private final String filename;

    public CsvProducer(Path file, M model) {

        Preconditions.checkNotNull(file, "CsvProducer need a not null file!");
        Preconditions.checkNotNull(model, "CsvProducer need a not null model!");

        this.filename = file.toString();

        try {
            this.writer = Files.newWriter(file.toFile(), Charsets.UTF_8);
        } catch (FileNotFoundException e) {
            // should never happen
            throw new ApplicationTechnicalException("file not found " + file, e);
        }

        this.export = new TuttiRepeatableExport<>(model);

    }

    @Override
    public void close() throws IOException {
        writer.close();
    }

    public void write(O row) throws Exception {
        if (row != null) {
            List<O> rows = new ArrayList<>(1);
            rows.add(row);
            write(rows);
        }
    }

    public void write(List<O> rows) throws Exception {
        if (rows != null) {
            if (!touch) {

                if (log.isDebugEnabled()) {
                    log.debug("CsvProducer " + this + " touched.");
                }
                touch = true;

            }
            export.write(rows, writer);
        }
    }

    public void writeEmpty() throws Exception {
        write(Collections.<O>emptyList());
    }

    public boolean wasTouched() {
        return touch;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("filename", filename).toString();
    }
}

package fr.ifremer.tutti.service.genericformat.producer;

/*
 * #%L
 * Tutti :: Service
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.entities.CaracteristicMap;
import fr.ifremer.tutti.persistence.entities.data.Cruise;
import fr.ifremer.tutti.persistence.entities.data.FishingOperation;
import fr.ifremer.tutti.persistence.entities.referential.Caracteristic;
import fr.ifremer.tutti.service.csv.CsvProducer;
import fr.ifremer.tutti.service.genericformat.csv.ParameterModel;
import fr.ifremer.tutti.service.genericformat.csv.ParameterRow;

import java.io.Serializable;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created on 2/6/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.13
 */
public class CsvProducerForParameter extends CsvProducer<ParameterRow, ParameterModel> {

    public CsvProducerForParameter(Path file, ParameterModel model) {
        super(file, model);
    }

    public List<ParameterRow> getDataToExport(Cruise cruise, FishingOperation operation) {

        List<ParameterRow> rows = new ArrayList<>();

        CaracteristicMap gearUseFeatures = operation.getGearUseFeatures();
        if (gearUseFeatures != null) {
            addCaracteristicMap(rows, cruise, operation, ParameterRow.ParameterType.GEAR, gearUseFeatures);
        }

        CaracteristicMap vesselUseFeatures = operation.getVesselUseFeatures();
        if (vesselUseFeatures != null) {
            addCaracteristicMap(rows, cruise, operation, ParameterRow.ParameterType.VESSEL, vesselUseFeatures);
        }

        return rows;

    }

    protected void addCaracteristicMap(List<ParameterRow> rows,
                                       Cruise cruise,
                                       FishingOperation operation,
                                       ParameterRow.ParameterType parameterType,
                                       CaracteristicMap caracteristicMap) {

        for (Map.Entry<Caracteristic, Serializable> entry : caracteristicMap.entrySet()) {

            ParameterRow row = new ParameterRow();
            row.setParameterType(parameterType);
            row.setCruise(cruise);
            row.setFishingOperation(operation);
            row.setCaracteristic(entry.getKey());
            row.setValue(entry.getValue());
            rows.add(row);

        }

    }

}

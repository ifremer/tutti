package fr.ifremer.tutti.service.csv;

/*
 * #%L
 * Tutti :: Service
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.entities.TuttiEntities;
import fr.ifremer.tutti.persistence.entities.TuttiEntity;
import org.apache.commons.lang3.StringUtils;
import org.nuiton.csv.ValueParserFormatter;

import java.text.ParseException;
import java.util.List;
import java.util.Map;

/**
 * Created on 2/14/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.14
 */
public abstract class EntityParserFormatterSupport<E extends TuttiEntity> implements ValueParserFormatter<E> {

    protected final String formatNullValue;

    protected final boolean technical;

    protected final Class<E> entityType;

    protected Map<String, E> entitiesById;

    protected boolean authorizeObsoleteReferentials;

    protected EntityParserFormatterSupport(String formatNullValue, boolean technical, Class<E> entityType) {
        this.formatNullValue = formatNullValue;
        this.technical = technical;
        this.entityType = entityType;
    }

    protected abstract List<E> getEntitiesWithObsoletes();

    protected abstract List<E> getEntities();

    protected abstract String formatBusiness(E e);

    protected Map<String, E> getEntitiesById() {

        if (entitiesById == null) {

            List<E> entities = isAuthorizeObsoleteReferentials() ? getEntitiesWithObsoletes() : getEntities();
            entitiesById = TuttiEntities.splitById(entities);

        }
        return entitiesById;

    }

    @Override
    public E parse(String value) throws ParseException {

        E result = null;
        if (StringUtils.isNotBlank(value)) {

            result = parseNotBlankValue(value);

        }
        return result;

    }

    @Override
    public String format(E e) {

        String value;
        if (e == null) {

            if (technical) {
                value = "";
            } else {
                value = formatNullValue;
            }

        } else {

            if (technical) {
                value = formatTechnical(e);
            } else {
                value = formatBusiness(e);
            }

        }
        return value;

    }

    protected E parseNotBlankValue(String value) {

        E result = getEntitiesById().get(value);

        if (result == null) {

            throw new EntityNotFoundException(entityType, value);

        }

        return result;

    }

    protected String formatTechnical(E e) {
        String value;
        value = e.getId();
        return value;
    }

    public boolean isAuthorizeObsoleteReferentials() {
        return authorizeObsoleteReferentials;
    }

    public void setAuthorizeObsoleteReferentials(boolean authorizeObsoleteReferentials) {
        this.authorizeObsoleteReferentials = authorizeObsoleteReferentials;
    }

}
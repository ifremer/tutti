package fr.ifremer.tutti.service.pupitri;

/*
 * #%L
 * Tutti :: Service
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

/**
 * Created on 11/22/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.11
 */
public enum BoxType {

    SMALL("001"),
    BIG("002");

    private final String importValue;

    BoxType(String importValue) {
        this.importValue = importValue;
    }

    public static BoxType getBoxType(String importValue) {

        BoxType result = null;
        for (BoxType s : values()) {
            if (s.importValue.equals(importValue)) {
                result = s;
                break;
            }
        }
        return result;

    }

}

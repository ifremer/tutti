package fr.ifremer.tutti.service.catches.multipost;

/*
 * #%L
 * Tutti :: Service
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Function;
import com.google.common.base.Preconditions;
import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.ListMultimap;
import com.google.common.collect.Multimap;
import fr.ifremer.adagio.core.dao.referential.ObjectTypeCode;
import fr.ifremer.tutti.persistence.entities.CaracteristicMap;
import fr.ifremer.tutti.persistence.entities.TuttiEntity;
import fr.ifremer.tutti.persistence.entities.data.AccidentalBatch;
import fr.ifremer.tutti.persistence.entities.data.AccidentalBatchs;
import fr.ifremer.tutti.persistence.entities.data.Attachment;
import fr.ifremer.tutti.persistence.entities.data.Attachments;
import fr.ifremer.tutti.persistence.entities.data.BatchContainer;
import fr.ifremer.tutti.persistence.entities.data.CatchBatch;
import fr.ifremer.tutti.persistence.entities.data.CopyIndividualObservationMode;
import fr.ifremer.tutti.persistence.entities.data.FishingOperation;
import fr.ifremer.tutti.persistence.entities.data.IndividualObservationBatch;
import fr.ifremer.tutti.persistence.entities.data.IndividualObservationBatchs;
import fr.ifremer.tutti.persistence.entities.data.MarineLitterBatch;
import fr.ifremer.tutti.persistence.entities.data.MarineLitterBatchs;
import fr.ifremer.tutti.persistence.entities.data.SampleCategoryModel;
import fr.ifremer.tutti.persistence.entities.data.SampleCategoryModelEntry;
import fr.ifremer.tutti.persistence.entities.data.SampleEntity;
import fr.ifremer.tutti.persistence.entities.data.SpeciesBatch;
import fr.ifremer.tutti.persistence.entities.data.SpeciesBatchFrequency;
import fr.ifremer.tutti.persistence.entities.data.SpeciesBatchFrequencys;
import fr.ifremer.tutti.persistence.entities.data.SpeciesBatchs;
import fr.ifremer.tutti.persistence.entities.referential.Caracteristic;
import fr.ifremer.tutti.persistence.entities.referential.CaracteristicBean;
import fr.ifremer.tutti.persistence.entities.referential.CaracteristicQualitativeValue;
import fr.ifremer.tutti.persistence.entities.referential.CaracteristicType;
import fr.ifremer.tutti.persistence.entities.referential.Species;
import fr.ifremer.tutti.service.AbstractTuttiService;
import fr.ifremer.tutti.service.DecoratorService;
import fr.ifremer.tutti.service.PersistenceService;
import fr.ifremer.tutti.service.TuttiServiceContext;
import fr.ifremer.tutti.service.catches.multipost.csv.AccidentalCatchRow;
import fr.ifremer.tutti.service.catches.multipost.csv.AccidentalCatchRowModel;
import fr.ifremer.tutti.service.catches.multipost.csv.AttachmentRow;
import fr.ifremer.tutti.service.catches.multipost.csv.AttachmentRowModel;
import fr.ifremer.tutti.service.catches.multipost.csv.CaracteristicRow;
import fr.ifremer.tutti.service.catches.multipost.csv.CaracteristicRowModel;
import fr.ifremer.tutti.service.catches.multipost.csv.CatchBatchRow;
import fr.ifremer.tutti.service.catches.multipost.csv.CatchBatchRowModel;
import fr.ifremer.tutti.service.catches.multipost.csv.CatchFrequencyRow;
import fr.ifremer.tutti.service.catches.multipost.csv.CatchFrequencyRowModel;
import fr.ifremer.tutti.service.catches.multipost.csv.CatchRow;
import fr.ifremer.tutti.service.catches.multipost.csv.CatchRowModel;
import fr.ifremer.tutti.service.catches.multipost.csv.CatchWeightsRow;
import fr.ifremer.tutti.service.catches.multipost.csv.CatchWeightsRowModel;
import fr.ifremer.tutti.service.catches.multipost.csv.FishingOperationRowModel;
import fr.ifremer.tutti.service.catches.multipost.csv.IndividualObservationRow;
import fr.ifremer.tutti.service.catches.multipost.csv.IndividualObservationRowModel;
import fr.ifremer.tutti.service.catches.multipost.csv.MarineLitterRow;
import fr.ifremer.tutti.service.catches.multipost.csv.MarineLitterRowModel;
import fr.ifremer.tutti.service.catches.multipost.csv.MarineLitterWeightRow;
import fr.ifremer.tutti.service.catches.multipost.csv.MarineLitterWeightRowModel;
import fr.ifremer.tutti.service.csv.CaracteristicValueParserFormatter;
import fr.ifremer.tutti.type.WeightUnit;
import java.io.File;
import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.TreeMap;
import java.util.function.Supplier;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.nuiton.csv.Import;
import org.nuiton.jaxx.application.ApplicationBusinessException;
import org.nuiton.jaxx.application.ApplicationTechnicalException;

import static org.nuiton.i18n.I18n.t;

/**
 * Service to export batches from a satellite post or import batches
 * into a master post.
 *
 * @author Kevin Morin - kmorin@codelutin.com
 * @since 2.2
 */
public class MultiPostImportService extends AbstractTuttiService implements MultiPostConstants {

    protected PersistenceService persistenceService;

    protected DecoratorService decoratorService;

    protected SampleCategoryModel sampleCategoryModel;

    protected Map<String, CaracteristicQualitativeValue> sampleCategoryValueMap;

    @Override
    public void setServiceContext(TuttiServiceContext context) {
        super.setServiceContext(context);
        persistenceService = getService(PersistenceService.class);
        decoratorService = getService(DecoratorService.class);
        sampleCategoryModel = context.getSampleCategoryModel();
        sampleCategoryValueMap = sampleCategoryModel.toMap();
    }

    //------------------------------------------------------------------------//
    //-- Import Catch                                                       --//
    //------------------------------------------------------------------------//

    /**
     * Import catch batch from a satellite post
     *
     * @param file      the file to import the batches from
     * @param operation the operation in which to add the batches
     * @return the list of the catch weight which have not been imported, because there were concurrent batches
     */
    public Map<String, Object> importCatchBatch(File file, FishingOperation operation) {

        return persistenceService.invoke(() -> {

            try (MultiPostImportContext importContext = new MultiPostImportContext(context, file, operation)) {

                //check operation
                CatchBatchRowModel weightsModel = new CatchBatchRowModel();
                importContext.checkSameOperation(CATCH_BATCH_FILE, weightsModel);

                Map<String, Object> notImportedData = new HashMap<>();

                // Import weights
                CatchBatch catchBatch = importCatchWeights(importContext, weightsModel, notImportedData);

                // Import attachments
                importAttachments(importContext, catchBatch.getIdAsInt(), ObjectTypeCode.CATCH_BATCH);

                return notImportedData;

            }

        });

    }

    protected CatchBatch importCatchWeights(MultiPostImportContext importContext,
                                            CatchBatchRowModel weightsModel,
                                            Map<String, Object> notImportedData) throws IOException {

        CatchBatch catchBatch = null;
        try (Reader reader = importContext.newFileReader(CATCH_BATCH_FILE)) {

            try (Import<CatchBatchRow> importer = Import.newImport(weightsModel, reader)) {

                Iterator<CatchBatchRow> iterator = importer.iterator();
                if (iterator.hasNext()) {

                    CatchBatchRow row = iterator.next();

                    FishingOperation operation = importContext.getOperation();

                    catchBatch = persistenceService.getCatchBatchFromFishingOperation(operation.getIdAsInt());
                    catchBatch.setFishingOperation(operation);

                    if (catchBatch.getCatchTotalWeight() == null) {
                        catchBatch.setCatchTotalWeight(row.getCatchTotalWeight());

                    } else if (row.getCatchTotalWeight() != null) {
                        notImportedData.put(CatchBatch.PROPERTY_CATCH_TOTAL_WEIGHT, row.getCatchTotalWeight());
                    }

                    if (catchBatch.getCatchTotalRejectedWeight() == null) {
                        catchBatch.setCatchTotalRejectedWeight(row.getCatchTotalRejectedWeight());

                    } else if (row.getCatchTotalRejectedWeight() != null) {
                        notImportedData.put(CatchBatch.PROPERTY_CATCH_TOTAL_REJECTED_WEIGHT, row.getCatchTotalRejectedWeight());
                    }

                    if (catchBatch.getSpeciesTotalSortedWeight() == null) {
                        catchBatch.setSpeciesTotalSortedWeight(row.getSpeciesTotalSortedWeight());

                    } else if (row.getSpeciesTotalSortedWeight() != null) {
                        notImportedData.put(CatchBatch.PROPERTY_SPECIES_TOTAL_SORTED_WEIGHT, row.getSpeciesTotalSortedWeight());
                    }

                    if (catchBatch.getBenthosTotalSortedWeight() == null) {
                        catchBatch.setBenthosTotalSortedWeight(row.getBenthosTotalSortedWeight());

                    } else if (row.getBenthosTotalSortedWeight() != null) {
                        notImportedData.put(CatchBatch.PROPERTY_BENTHOS_TOTAL_SORTED_WEIGHT, row.getBenthosTotalSortedWeight());
                    }

                    if (catchBatch.getMarineLitterTotalWeight() == null) {
                        catchBatch.setMarineLitterTotalWeight(row.getMarineLitterTotalWeight());

                    } else if (row.getMarineLitterTotalWeight() != null) {
                        notImportedData.put(CatchBatch.PROPERTY_MARINE_LITTER_TOTAL_WEIGHT, row.getMarineLitterTotalWeight());
                    }

                    persistenceService.saveCatchBatch(catchBatch);

                }

            }

        }

        return catchBatch;

    }

    //------------------------------------------------------------------------//
    //-- Import Species                                                     --//
    //------------------------------------------------------------------------//

    /**
     * Import species batches from a satellite post
     *
     * @param file      the file to import the batches from
     * @param operation the operation in which to add the batches
     * @return the list of the species which have not been imported, because there were concurrent batches
     */
    public Map<String, Object> importSpecies(File file,
                                             FishingOperation operation,
                                             boolean importFrequencies,
                                             boolean importIndividualObservations) {

        return persistenceService.invoke(() -> {

            try (MultiPostImportContext importContext = new MultiPostImportContext(context, file, operation)) {

                //check operation
                CatchWeightsRowModel weightsModel = new CatchWeightsRowModel();
                importContext.checkSameOperation(WEIGHTS_FILE, weightsModel);

                Map<String, Object> notImportedData = new HashMap<>();

                Map<String, SpeciesBatch> notImportedBatches = new LinkedHashMap<>();

                BatchContainer<SpeciesBatch> speciesBatches = persistenceService.getRootSpeciesBatch(operation.getIdAsInt(), false);

                // Import batches
                // map containing the batches by their persistence id

                Map<String, SpeciesBatch> batches = importSpeciesBatches(importContext, speciesBatches, notImportedBatches);

                // Import frequencies

                if (importFrequencies) {

                    ListMultimap<String, SpeciesBatchFrequency> frequencyListMultimap = importFrequencies(importContext,
                                                                                                          batches,
                                                                                                          notImportedBatches,
                                                                                                          SpeciesBatchFrequencys::newSpeciesBatchFrequency);

                    for (String batchId : frequencyListMultimap.keySet()) {

                        List<SpeciesBatchFrequency> frequencies = frequencyListMultimap.get(batchId);
                        SpeciesBatch speciesBatch = batches.get(batchId);
                        frequencies.forEach(speciesBatchFrequency -> speciesBatchFrequency.setBatch(speciesBatch));
                        persistenceService.saveSpeciesBatchFrequency(speciesBatch.getIdAsInt(), frequencies);

                    }

                }

                // Import indivudal observations

                if (importIndividualObservations) {

                    importIndividualObservations(importContext, importFrequencies, batches, notImportedBatches, speciesFrequencyHelper);

                }

                // Import attachments

                importAttachments(importContext, batches, notImportedBatches, ObjectTypeCode.BATCH);

                // Import weights

                importSpeciesWeights(importContext, weightsModel, notImportedData);

                notImportedData.put(BATCHES_KEY, notImportedBatches.values());

                return notImportedData;

            }
        });

    }

    interface FrequencyHelper {

        SpeciesBatchFrequency newSpeciesBatchFrequency();

        List<SpeciesBatchFrequency> saveSpeciesBatchFrequency(Integer speciesBatchId, List<SpeciesBatchFrequency> frequencies);

    }

    private final FrequencyHelper speciesFrequencyHelper = new FrequencyHelper() {

        @Override
        public SpeciesBatchFrequency newSpeciesBatchFrequency() {
            return SpeciesBatchFrequencys.newSpeciesBatchFrequency();
        }

        @Override
        public List<SpeciesBatchFrequency> saveSpeciesBatchFrequency(Integer speciesBatchId, List<SpeciesBatchFrequency> frequencies) {
            return persistenceService.saveSpeciesBatchFrequency(speciesBatchId, frequencies);
        }
    };

    private final FrequencyHelper benthosFrequencyHelper = new FrequencyHelper() {

        @Override
        public SpeciesBatchFrequency newSpeciesBatchFrequency() {
            return SpeciesBatchFrequencys.newBenthosBatchFrequency();
        }
        @Override
        public List<SpeciesBatchFrequency> saveSpeciesBatchFrequency(Integer speciesBatchId, List<SpeciesBatchFrequency> frequencies) {
            return persistenceService.saveBenthosBatchFrequency(speciesBatchId, frequencies);
        }
    };

    protected Map<String, SpeciesBatch> importSpeciesBatches(MultiPostImportContext importContext,
                                                             BatchContainer<SpeciesBatch> speciesBatches,
                                                             Map<String, SpeciesBatch> notImportedBatches) throws IOException {

        Map<String, SpeciesBatch> batches = new HashMap<>();

        try (Reader reader = importContext.newFileReader(SPECIES_FILE)) {

            CatchRowModel csvModel = CatchRowModel.forImport(importContext.getSpeciesList());

            try (Import<CatchRow> importer = Import.newImport(csvModel, reader)) {

                for (CatchRow row : importer) {

                    // create batch
                    SpeciesBatch batch = SpeciesBatchs.newSpeciesBatch();
                    batch.setFishingOperation(importContext.getOperation());

                    Species species = row.getSpecies();
                    batch.setSpecies(row.getSpecies());
                    batch.setWeight(row.getWeight());
                    batch.setNumber(row.getNumber());
                    batch.setComment(row.getComment());
                    batch.setSpeciesToConfirm(row.isToConfirm());

                    Pair<Integer, Serializable> valueAndCategoryType = getValueAndCategoryType(row);
                    Integer categoryId = valueAndCategoryType.getKey();
                    Serializable value = valueAndCategoryType.getValue();

                    batch.setSampleCategoryId(categoryId);
                    batch.setSampleCategoryValue(value);
                    batch.setSampleCategoryWeight(row.getCategoryWeight());

                    // get parent batch
                    String parentId = row.getParentId();
                    Integer parentPersistedId = null;
                    SpeciesBatch parent = null;
                    if (StringUtils.isNotEmpty(parentId)) {
                        parent = batches.get(parentId);
                        parentPersistedId = parent.getIdAsInt();
                    }

                    SpeciesBatch existingBatch = null;
                    List<SpeciesBatch> batchesToBrowse;
                    if (parent == null) {
                        batchesToBrowse = speciesBatches.getChildren();
                    } else {
                        batchesToBrowse = parent.getChildBatchs();
                    }

                    // check if the parent has already a batch with the caracteristic value
                    // and if the children batch are categorized with the same caracterstic
                    if (batchesToBrowse != null) {
                        for (SpeciesBatch sb : batchesToBrowse) {
                            if (sb.getSpecies().equals(species)
                                    && (!sb.getSampleCategoryId().equals(batch.getSampleCategoryId())
                                    || sb.getSampleCategoryValue().equals(value))) {

                                existingBatch = sb;
                                batches.put(row.getId(), sb);
                                notImportedBatches.put(row.getId(), batch);
                                break;
                            }
                        }
                    }

                    if (existingBatch == null) {
                        batch = persistenceService.createSpeciesBatch(batch, parentPersistedId, true);
                        batches.put(row.getId(), batch);
                    }
                }
            }
        }
        return batches;

    }

    protected void importSpeciesWeights(MultiPostImportContext importContext,
                                        CatchWeightsRowModel weightsModel,
                                        Map<String, Object> notImportedData) throws IOException {

        try (Reader reader = importContext.newFileReader(WEIGHTS_FILE)) {

            try (Import<CatchWeightsRow> importer = Import.newImport(weightsModel, reader)) {
                Iterator<CatchWeightsRow> iterator = importer.iterator();
                if (iterator.hasNext()) {

                    FishingOperation operation = importContext.getOperation();

                    CatchWeightsRow row = iterator.next();
                    CatchBatch catchBatch = persistenceService.getCatchBatchFromFishingOperation(operation.getIdAsInt());
                    catchBatch.setFishingOperation(operation);

                    if (catchBatch.getSpeciesTotalSortedWeight() == null) {
                        catchBatch.setSpeciesTotalSortedWeight(row.getTotalSortedWeight());

                    } else if (row.getTotalSortedWeight() != null) {
                        notImportedData.put(CatchBatch.PROPERTY_SPECIES_TOTAL_SORTED_WEIGHT, row.getTotalSortedWeight());
                    }

                    if (catchBatch.getSpeciesTotalInertWeight() == null) {
                        catchBatch.setSpeciesTotalInertWeight(row.getInertWeight());

                    } else if (row.getInertWeight() != null) {
                        notImportedData.put(CatchBatch.PROPERTY_SPECIES_TOTAL_INERT_WEIGHT, row.getInertWeight());
                    }

                    if (catchBatch.getSpeciesTotalLivingNotItemizedWeight() == null) {
                        catchBatch.setSpeciesTotalLivingNotItemizedWeight(row.getLivingNotItemizedWeight());

                    } else if (row.getLivingNotItemizedWeight() != null) {
                        notImportedData.put(CatchBatch.PROPERTY_SPECIES_TOTAL_LIVING_NOT_ITEMIZED_WEIGHT,
                                            row.getLivingNotItemizedWeight());
                    }
                    persistenceService.saveCatchBatch(catchBatch);

                }

            }

        }

    }

    //------------------------------------------------------------------------//
    //-- Import Species Batches data                                        --//
    //------------------------------------------------------------------------//

    /**
     * Import species batches frequencies or individual observations from a satellite post.
     *
     * @param file      the file to import the batches from
     * @param operation the operation in which to add the batches
     * @return the list of the species which have not been imported, because there were concurrent batches
     */
    public MultiPostImportResult importSpeciesBatch(File file,
                                                    FishingOperation operation,
                                                    SpeciesBatch speciesBatch,
                                                    boolean importFrequencies,
                                                    boolean importIndividualObservations) {

        return importSpeciesOrBenthosBatch(file,
                                           operation,
                                           speciesBatch,
                                           importFrequencies,
                                           importIndividualObservations);

    }

    //------------------------------------------------------------------------//
    //-- Import Benthos                                                     --//
    //------------------------------------------------------------------------//

    /**
     * Import benthos batches from a satellite post
     *
     * @param file      the file to import the batches from
     * @param operation the operation in which to add the batches
     * @return the list of the benthos which have not been imported, because there were concurrent batches
     */
    public Map<String, Object> importBenthos(File file, FishingOperation operation, boolean importFrequencies, boolean importIndividualObservations) {

        return persistenceService.invoke(() -> {

            try (MultiPostImportContext importContext = new MultiPostImportContext(context, file, operation)) {

                //check operation
                CatchWeightsRowModel weightsModel = new CatchWeightsRowModel();
                importContext.checkSameOperation(WEIGHTS_FILE, weightsModel);

                Map<String, Object> notImportedData = new HashMap<>();
                Map<String, SpeciesBatch> notImportedBatches = new LinkedHashMap<>();

                BatchContainer<SpeciesBatch> benthosBatches = persistenceService.getRootBenthosBatch(operation.getIdAsInt(), false);

                // Import batches
                // map containing the batches by their persistence id
                Map<String, SpeciesBatch> batches = importBenthosBatches(importContext, benthosBatches, notImportedBatches);

                // Import frequencies

                if (importFrequencies) {

                    ListMultimap<String, SpeciesBatchFrequency> frequencyListMultimap = importFrequencies(importContext,
                                                                                                          batches,
                                                                                                          notImportedBatches,
                                                                                                          SpeciesBatchFrequencys::newBenthosBatchFrequency);

                    for (String batchId : frequencyListMultimap.keySet()) {

                        List<SpeciesBatchFrequency> frequencies = frequencyListMultimap.get(batchId);
                        SpeciesBatch speciesBatch = batches.get(batchId);
                        frequencies.forEach(speciesBatchFrequency -> speciesBatchFrequency.setBatch(speciesBatch));
                        persistenceService.saveBenthosBatchFrequency(speciesBatch.getIdAsInt(), frequencies);

                    }

                }

                // Import indivudal observations

                if (importIndividualObservations) {

                    importIndividualObservations(importContext, importFrequencies, batches, notImportedBatches, benthosFrequencyHelper);

                }

                // Import attachments

                importAttachments(importContext, batches, notImportedBatches, ObjectTypeCode.BATCH);

                // Import weights

                importBenthosWeights(importContext, weightsModel, notImportedData);

                notImportedData.put(BATCHES_KEY, notImportedBatches.values());
                return notImportedData;

            }

        });

    }

    protected Map<String, SpeciesBatch> importBenthosBatches(MultiPostImportContext importContext,
                                                             BatchContainer<SpeciesBatch> benthosBatches,
                                                             Map<String, SpeciesBatch> notImportedBatches) throws IOException {
        Map<String, SpeciesBatch> batches = new HashMap<>();

        try (Reader reader = importContext.newFileReader(BENTHOS_FILE)) {

            CatchRowModel csvModel = CatchRowModel.forImport(importContext.getSpeciesList());

            try (Import<CatchRow> importer = Import.newImport(csvModel, reader)) {

                FishingOperation operation = importContext.getOperation();
                for (CatchRow row : importer) {

                    // create batch
                    SpeciesBatch batch = SpeciesBatchs.newBenthosBatch();
                    batch.setFishingOperation(operation);

                    Species species = row.getSpecies();
                    batch.setSpecies(row.getSpecies());
                    batch.setWeight(row.getWeight());
                    batch.setNumber(row.getNumber());
                    batch.setComment(row.getComment());
                    batch.setSpeciesToConfirm(row.isToConfirm());

                    Pair<Integer, Serializable> valueAndCategoryType = getValueAndCategoryType(row);
                    Integer categoryId = valueAndCategoryType.getKey();
                    Serializable value = valueAndCategoryType.getValue();

                    batch.setSampleCategoryId(categoryId);
                    batch.setSampleCategoryValue(value);
                    batch.setSampleCategoryWeight(row.getCategoryWeight());

                    // get parent batch
                    String parentId = row.getParentId();
                    Integer parentPersistedId = null;
                    SpeciesBatch parent = null;
                    if (StringUtils.isNotEmpty(parentId)) {
                        parent = batches.get(parentId);
                        parentPersistedId = parent.getIdAsInt();
                    }

                    SpeciesBatch existingBatch = null;
                    List<SpeciesBatch> batchesToBrowse;
                    if (parent == null) {
                        batchesToBrowse = benthosBatches.getChildren();
                    } else {
                        batchesToBrowse = parent.getChildBatchs();
                    }

                    // check if the parent has already a batch with the caracteristic value
                    // and if the children batch are categorized with the same caracterstic
                    if (batchesToBrowse != null) {
                        for (SpeciesBatch bb : batchesToBrowse) {
                            if (bb.getSpecies().equals(species)
                                    && (!bb.getSampleCategoryId().equals(batch.getSampleCategoryId())
                                    || bb.getSampleCategoryValue().equals(value))) {

                                existingBatch = bb;
                                batches.put(row.getId(), bb);
                                notImportedBatches.put(row.getId(), batch);
                                break;
                            }
                        }
                    }

                    if (existingBatch == null) {
                        batch = persistenceService.createBenthosBatch(batch, parentPersistedId, true);
                        batches.put(row.getId(), batch);
                    }
                }

            }

        }
        return batches;

    }

    protected void importBenthosWeights(MultiPostImportContext importContext,
                                        CatchWeightsRowModel weightsModel,
                                        Map<String, Object> notImportedData) throws IOException {


        try (Reader reader = importContext.newFileReader(WEIGHTS_FILE)) {

            try (Import<CatchWeightsRow> importer = Import.newImport(weightsModel, reader)) {

                Iterator<CatchWeightsRow> iterator = importer.iterator();
                if (iterator.hasNext()) {

                    FishingOperation operation = importContext.getOperation();

                    CatchWeightsRow row = iterator.next();
                    CatchBatch catchBatch = persistenceService.getCatchBatchFromFishingOperation(operation.getIdAsInt());
                    catchBatch.setFishingOperation(operation);

                    if (catchBatch.getBenthosTotalSortedWeight() == null) {
                        catchBatch.setBenthosTotalSortedWeight(row.getTotalSortedWeight());

                    } else if (row.getTotalSortedWeight() != null) {
                        notImportedData.put(CatchBatch.PROPERTY_BENTHOS_TOTAL_SORTED_WEIGHT, row.getTotalSortedWeight());
                    }

                    if (catchBatch.getBenthosTotalInertWeight() == null) {
                        catchBatch.setBenthosTotalInertWeight(row.getInertWeight());

                    } else if (row.getInertWeight() != null) {
                        notImportedData.put(CatchBatch.PROPERTY_BENTHOS_TOTAL_INERT_WEIGHT, row.getInertWeight());
                    }

                    if (catchBatch.getBenthosTotalLivingNotItemizedWeight() == null) {
                        catchBatch.setBenthosTotalLivingNotItemizedWeight(row.getLivingNotItemizedWeight());

                    } else if (row.getLivingNotItemizedWeight() != null) {
                        notImportedData.put(CatchBatch.PROPERTY_BENTHOS_TOTAL_LIVING_NOT_ITEMIZED_WEIGHT,
                                            row.getLivingNotItemizedWeight());
                    }

                    persistenceService.saveCatchBatch(catchBatch);
                }

            }

        }

    }

    //------------------------------------------------------------------------//
    //-- Import Benthos Batches data                                        --//
    //------------------------------------------------------------------------//

    /**
     * Import benthos batches frequencies or individual observations from a satellite post.
     *
     * @param file      the file to import the batches from
     * @param operation the operation in which to add the batches
     * @return the list of the species which have not been imported, because there were concurrent batches
     */
    public MultiPostImportResult importBenthosBatch(File file,
                                                    FishingOperation operation,
                                                    SpeciesBatch speciesBatch,
                                                    boolean importFrequencies,
                                                    boolean importIndividualObservations) {

        return importSpeciesOrBenthosBatch(file,
                                           operation,
                                           speciesBatch,
                                           importFrequencies,
                                           importIndividualObservations);


    }


    //------------------------------------------------------------------------//
    //-- Import Marine litter                                               --//
    //------------------------------------------------------------------------//

    /**
     * Import marine litter batches from a satellite post.
     *
     * @param file      the file to import the batches from
     * @param operation the operation in which to add the batches
     * @return the list of the marine litter which have not been imported, because there were concurrent batches
     */
    public Map<String, Object> importMarineLitter(File file, FishingOperation operation) {

        return persistenceService.invoke(() -> {

            try (MultiPostImportContext importContext = new MultiPostImportContext(context, file, operation)) {

                //check operation
                MarineLitterWeightRowModel weightModel = new MarineLitterWeightRowModel();
                importContext.checkSameOperation(WEIGHTS_FILE, weightModel);

                Map<String, Object> notImportedData = new HashMap<>();
                Map<String, MarineLitterBatch> notImportedBatches = new LinkedHashMap<>();

                // Import batches
                // map containing the batches by their persistence id

                Map<String, MarineLitterBatch> batches = importMarineLitterBatches(importContext, notImportedBatches);

                // Import attachments

                importAttachments(importContext, batches, notImportedBatches, ObjectTypeCode.BATCH);

                // Import weights

                importMarineLitterCatchWeights(importContext, weightModel, notImportedData);

                notImportedData.put(BATCHES_KEY, notImportedBatches.values());
                return notImportedData;

            }

        });
    }

    protected Map<String, MarineLitterBatch> importMarineLitterBatches(MultiPostImportContext importContext,
                                                                       Map<String, MarineLitterBatch> notImportedBatches) throws IOException {

        Map<String, MarineLitterBatch> batches = new LinkedHashMap<>();

        FishingOperation operation = importContext.getOperation();

        BatchContainer<MarineLitterBatch> marineLitterBatches =
                persistenceService.getRootMarineLitterBatch(operation.getIdAsInt());

        Caracteristic categoryCaracteristic = persistenceService.getMarineLitterCategoryCaracteristic();
        Caracteristic sizeCategoryCaracteristic = persistenceService.getMarineLitterSizeCategoryCaracteristic();

        try (Reader reader = importContext.newFileReader(MARINE_LITTER_FILE)) {

            MarineLitterRowModel csvModel = MarineLitterRowModel.forImport(
                    categoryCaracteristic,
                    sizeCategoryCaracteristic);

            try (Import<MarineLitterRow> importer = Import.newImport(csvModel, reader)) {


                for (MarineLitterRow row : importer) {

                    // create batch
                    MarineLitterBatch batch = MarineLitterBatchs.newMarineLitterBatch();
                    batch.setFishingOperation(operation);

                    batch.setWeight(row.getWeight());
                    batch.setNumber(row.getNumber());
                    batch.setComment(row.getComment());
                    batch.setMarineLitterCategory(row.getCategory());
                    batch.setMarineLitterSizeCategory(row.getSizeCategory());

                    MarineLitterBatch existingBatch = null;
                    List<MarineLitterBatch> batchesToBrowse = marineLitterBatches.getChildren();

                    // check if the parent has already a batch with the caracteristic value
                    // and if the children batch are categorized with the same caracterstic
                    if (batchesToBrowse != null) {
                        for (MarineLitterBatch mlb : batchesToBrowse) {
                            if (mlb.getMarineLitterCategory().equals(batch.getMarineLitterCategory())
                                    && mlb.getMarineLitterSizeCategory().equals(batch.getMarineLitterSizeCategory())) {

                                existingBatch = mlb;
                                batches.put(row.getBatchId(), mlb);
                                notImportedBatches.put(row.getBatchId(), batch);
                                break;
                            }
                        }
                    }

                    if (existingBatch == null) {
                        batch = persistenceService.createMarineLitterBatch(batch);
                        batches.put(row.getBatchId(), batch);
                    }

                }

            }

        }
        return batches;

    }

    protected void importMarineLitterCatchWeights(MultiPostImportContext importContext,
                                                  MarineLitterWeightRowModel weightModel,
                                                  Map<String, Object> notImportedData) throws IOException {

        try (Reader reader = importContext.newFileReader(WEIGHTS_FILE)) {

            try (Import<MarineLitterWeightRow> weight = Import.newImport(weightModel, reader)) {

                Iterator<MarineLitterWeightRow> iterator = weight.iterator();
                if (iterator.hasNext()) {

                    FishingOperation operation = importContext.getOperation();

                    MarineLitterWeightRow row = iterator.next();
                    CatchBatch catchBatch = persistenceService.getCatchBatchFromFishingOperation(operation.getIdAsInt());
                    catchBatch.setFishingOperation(operation);

                    if (catchBatch.getMarineLitterTotalWeight() == null) {
                        catchBatch.setMarineLitterTotalWeight(row.getTotalWeight());

                    } else if (row.getTotalWeight() != null) {
                        notImportedData.put(CatchBatch.PROPERTY_MARINE_LITTER_TOTAL_WEIGHT, row.getTotalWeight());
                    }

                    persistenceService.saveCatchBatch(catchBatch);
                }

            }

        }

    }

    //------------------------------------------------------------------------//
    //-- Import Accidental catches                                          --//
    //------------------------------------------------------------------------//

    /**
     * Import accidental catches batches from a satellite post.
     *
     * @param file      the file to import the batches from
     * @param operation the operation in which to add the batches
     */
    public void importAccidentalCatches(File file, FishingOperation operation) {

        persistenceService.invoke(() -> {

            try (MultiPostImportContext importContext = new MultiPostImportContext(context, file, operation)) {

                //check operation
                FishingOperationRowModel operationModel = new FishingOperationRowModel();
                importContext.checkSameOperation(WEIGHTS_FILE, operationModel);

                // Import batches
                // map containing the batches by their persistence id
                Map<String, AccidentalBatch> accidentalBatches = importAccidentalCatchesBatches(importContext);

                // Import caracteristics

                importCaracteristics(importContext, accidentalBatches);

                // Persist batches

                persistenceService.createAccidentalBatches(accidentalBatches.values());

                // Import attachments

                importAttachments(importContext, accidentalBatches, ObjectTypeCode.SAMPLE);

                return null;
            }

        });

    }

    protected Map<String, AccidentalBatch> importAccidentalCatchesBatches(MultiPostImportContext importContext) throws IOException {

        Map<String, AccidentalBatch> batches = new LinkedHashMap<>();

        Caracteristic sexCaracteristic = persistenceService.getSexCaracteristic();
        Caracteristic deadOrAliveCaracteristic = persistenceService.getDeadOrAliveCaracteristic();

        try (Reader reader = importContext.newFileReader(ACCIDENTAL_CATCHES_FILE)) {

            AccidentalCatchRowModel csvModel = AccidentalCatchRowModel.forImport(
                    importContext.getSpeciesList(),
                    sexCaracteristic,
                    deadOrAliveCaracteristic,
                    importContext.getCaracteristicsWithProtected());

            try (Import<AccidentalCatchRow> importer = Import.newImport(csvModel, reader)) {

                FishingOperation operation = importContext.getOperation();

                for (AccidentalCatchRow row : importer) {

                    // create batch
                    AccidentalBatch batch = AccidentalBatchs.newAccidentalBatch();
                    batch.setFishingOperation(operation);

                    batch.setSpecies(row.getSpecies());
                    batch.setGender(row.getGender());
                    batch.setWeight(row.getWeight());
                    batch.setSize(row.getSize());
                    batch.setLengthStepCaracteristic(row.getLengthStepCaracteristic());
                    batch.setDeadOrAlive(row.getDeadOrAlive());
                    batch.setComment(row.getComment());
                    batch.setCaracteristics(new CaracteristicMap());

                    batches.put(row.getBatchId(), batch);
                }

            }

        }
        return batches;

    }

    //------------------------------------------------------------------------//
    //-- Internal methods                                                   --//
    //------------------------------------------------------------------------//

    protected Pair<Integer, Serializable> getValueAndCategoryType(CatchRow row) {

        Serializable value = row.getCategoryValue();
        Integer categoryId = row.getCategoryId();

        SampleCategoryModelEntry categoryModelEntry = sampleCategoryModel.getCategoryById(categoryId);

        Caracteristic caracteristic = categoryModelEntry.getCaracteristic();
        if (caracteristic.getCaracteristicType() == CaracteristicType.QUALITATIVE) {
            CaracteristicQualitativeValue caracteristicQualitativeValue = sampleCategoryValueMap.get(String.valueOf(value));
            Preconditions.checkNotNull(caracteristicQualitativeValue, "Can't find caracteristic qualitative value with id: " + value + " for caracteristic of id: " + categoryId);
            value = caracteristicQualitativeValue;
        }
        return Pair.of(categoryId, value);

    }

    protected void importAttachments(MultiPostImportContext importContext, Integer catchBatchId, ObjectTypeCode objectType) throws IOException {

        Function<AttachmentRow, Integer> getObjetcIdFunction = input -> catchBatchId;

        importAttachments(importContext, getObjetcIdFunction, objectType);

    }

    protected <R extends TuttiEntity> void importAttachments(MultiPostImportContext importContext,
                                                             Map<String, R> data,
                                                             Map<String, R> notImportedData,
                                                             ObjectTypeCode objectType) throws IOException {

        Function<AttachmentRow, Integer> getObjetcIdFunction = input -> {
            Integer result = null;
            if (notImportedData.get(input.getBatchId()) == null) {
                R batch = data.get(input.getBatchId());
                if (batch != null) {
                    result = batch.getIdAsInt();
                }
            }
            return result;
        };

        importAttachments(importContext, getObjetcIdFunction, objectType);

    }

    protected <R extends TuttiEntity> void importAttachments(MultiPostImportContext importContext, Map<String, R> data, ObjectTypeCode objectType) throws IOException {

        Function<AttachmentRow, Integer> getObjetcIdFunction = input -> {
            Integer result = null;
            R batch = data.get(input.getBatchId());
            if (batch != null) {
                result = batch.getIdAsInt();
            }
            return result;
        };

        importAttachments(importContext, getObjetcIdFunction, objectType);
    }

    protected void importAttachments(MultiPostImportContext importContext, Function<AttachmentRow, Integer> getObjetcIdFunction, ObjectTypeCode objectType) throws IOException {

        try (Reader reader = importContext.newFileReader(ATTACHMENTS_FILE)) {

            AttachmentRowModel attachmentModel =
                    AttachmentRowModel.forImport(importContext.newFile(ATTACHMENTS_DIRECTORY));
            try (Import<AttachmentRow> importer = Import.newImport(attachmentModel, reader)) {

                for (AttachmentRow row : importer) {
                    Integer objectId = getObjetcIdFunction.apply(row);
                    if (objectId != null) {
                        Attachment attachment = Attachments.newAttachment();
                        attachment.setName(row.getName());
                        attachment.setComment(row.getComment());
                        attachment.setObjectType(objectType);

                        attachment.setObjectId(objectId);

                        persistenceService.createAttachment(attachment, row.getFile());
                    }
                }

            }
        }

    }

    protected <R extends TuttiEntity> Multimap<Integer, File> getAttachmentFiles(MultiPostImportContext importContext, Map<String, R> data, ObjectTypeCode objectType) throws IOException {

        Function<AttachmentRow, Integer> getObjetcIdFunction = input -> {
            Integer result = null;
            R batch = data.get(input.getBatchId());
            if (batch != null) {
                result = batch.getIdAsInt();
            }
            return result;
        };

        return getAttachmentFiles(importContext, getObjetcIdFunction, objectType);
    }

    protected Multimap<Integer, File> getAttachmentFiles(MultiPostImportContext importContext, Function<AttachmentRow, Integer> getObjetcIdFunction, ObjectTypeCode objectType) throws IOException {
        Multimap<Integer, File> result = HashMultimap.create();
        try (Reader reader = importContext.newFileReader(ATTACHMENTS_FILE)) {

            AttachmentRowModel attachmentModel =
                    AttachmentRowModel.forImport(importContext.newFile(ATTACHMENTS_DIRECTORY));
            try (Import<AttachmentRow> importer = Import.newImport(attachmentModel, reader)) {

                for (AttachmentRow row : importer) {
                    Integer objectId = getObjetcIdFunction.apply(row);
                    if (objectId != null) {

                        result.put(objectId, row.getFile());
                    }
                }

            }
        }
        return result;
    }

    protected <B extends SampleEntity> void importCaracteristics(MultiPostImportContext importContext, Map<String, B> batches) throws IOException {

        try (Reader reader = importContext.newFileReader(CARACTERISTIC_FILE)) {

            CaracteristicRowModel caracteristicModel = CaracteristicRowModel.forImport(importContext.getCaracteristicsWithProtected());

            try (Import<CaracteristicRow> importer = Import.newImport(caracteristicModel, reader)) {

                for (CaracteristicRow caracteristicRow : importer) {
                    B batch = batches.get(caracteristicRow.getBatchId());
                    if (batch != null) {
                        Caracteristic caracteristic = caracteristicRow.getCaracteristic();
                        CaracteristicValueParserFormatter parser = CaracteristicValueParserFormatter.newParser(caracteristic);
                        Serializable value = caracteristicRow.getValue();
                        try {

                            Serializable caracteristicValue = parser.parse(String.valueOf(value));
                            batch.getCaracteristics().put(caracteristic, caracteristicValue);

                        } catch (ParseException e) {
                            throw new ApplicationTechnicalException("Could not parse caracteristic value: " + value, e);
                        }

                    }

                }

            }

        }

    }

    private ListMultimap<String, SpeciesBatchFrequency> importFrequencies(MultiPostImportContext importContext,
                                                                          Map<String, SpeciesBatch> batches,
                                                                          Map<String, SpeciesBatch> notImportedBatches,
                                                                          Supplier<SpeciesBatchFrequency> newFrequencySupplier) throws IOException {

        ListMultimap<String, SpeciesBatchFrequency> frequencyMap = ArrayListMultimap.create();

        try (Reader reader = importContext.newFileReader(FREQUENCIES_FILE)) {

            CatchFrequencyRowModel frequencyModel = CatchFrequencyRowModel.forImport(importContext.getSpeciesList(), importContext.getCaracteristics());

            try (Import<CatchFrequencyRow> importer = Import.newImport(frequencyModel, reader)) {

                for (CatchFrequencyRow frequencyRow : importer) {
                    String frequencyRowBatchId = frequencyRow.getBatchId();
                    if (notImportedBatches.containsKey(frequencyRowBatchId)) {

                        // not imported
                        continue;
                    }
                    SpeciesBatch batch = batches.get(frequencyRowBatchId);
                    if (batch == null) {

                        // not imported (FIXME Should be an error ?)
                        continue;
                    }

                    SpeciesBatchFrequency frequency = newFrequencySupplier.get();
                    frequency.setLengthStepCaracteristic(frequencyRow.getLengthStepCaracteristic());
                    frequency.setLengthStep(frequencyRow.getLengthStep());
                    frequency.setNumber(frequencyRow.getNumber());
                    frequency.setWeight(frequencyRow.getWeight());
//                    frequency.setBatch(batch);
                    frequencyMap.put(frequencyRowBatchId, frequency);

                }
            }
        }

        return frequencyMap;

    }

    private Map<String, IndividualObservationBatch> importIndividualObservationBatches(MultiPostImportContext importContext,
                                                                                       Map<String, SpeciesBatch> speciesOrBenthosBatches,
                                                                                       Map<String, SpeciesBatch> notImportedBatches) throws IOException {

        Map<String, IndividualObservationBatch> batches = new LinkedHashMap<>();

        try (Reader reader = importContext.newFileReader(INDIVIDUAL_OBSERVATION_FILE)) {

            IndividualObservationRowModel csvModel = IndividualObservationRowModel.forImport(
                    importContext.getSpeciesList(),
                    importContext.getCaracteristicsWithProtected());

            try (Import<IndividualObservationRow> importer = Import.newImport(csvModel, reader)) {

                FishingOperation operation = importContext.getOperation();

                for (IndividualObservationRow row : importer) {

                    String speciesBatchId = row.getSpeciesBatchId();

                    if (notImportedBatches.containsKey(speciesBatchId)) {
                        // do not import these batch
                        continue;
                    }
                    SpeciesBatch speciesOfBenthosBatch = speciesOrBenthosBatches.get(speciesBatchId);
                    if (speciesOfBenthosBatch == null) {
                        // not found (FIXME Should ne an error)
                        continue;
                    }

                    // create batch
                    IndividualObservationBatch batch = IndividualObservationBatchs.newIndividualObservationBatch();
                    batch.setFishingOperation(operation);

                    batch.setBatchId(speciesOfBenthosBatch.getIdAsInt());

                    batch.setSpecies(row.getSpecies());
                    batch.setWeight(row.getWeight());
                    batch.setSize(row.getSize());
                    batch.setLengthStepCaracteristic(row.getLengthStepCaracteristic());
                    batch.setComment(row.getComment());
                    batch.setCopyIndividualObservationMode(row.getCopyIndividualObservationMode());
                    if (StringUtils.isNotEmpty(row.getSamplingCode())) {
                        batch.setSamplingCode(row.getSamplingCode());
                    }
                    batch.setCaracteristics(new CaracteristicMap());

                    batches.put(row.getBatchId(), batch);
                }

            }

        }
        return batches;

    }

    protected List<SpeciesBatchFrequency> importFrequencies(MultiPostImportContext importContext, SpeciesBatch speciesBatch, Supplier<SpeciesBatchFrequency> newFrequencySupplier) throws IOException {

        List<SpeciesBatchFrequency> frequenciesToSave = new LinkedList<>();

        try (Reader reader = importContext.newFileReader(FREQUENCIES_FILE)) {

            CatchFrequencyRowModel frequencyModel = CatchFrequencyRowModel.forImport(importContext.getSpeciesList(), importContext.getCaracteristics());

            try (Import<CatchFrequencyRow> importer = Import.newImport(frequencyModel, reader)) {

                Integer requiredSpeciesId = speciesBatch.getSpecies().getIdAsInt();

                for (CatchFrequencyRow frequencyRow : importer) {

                    Integer speciesId = frequencyRow.getSpecies().getIdAsInt();

                    if (!requiredSpeciesId.equals(speciesId)) {

                        // Bad species, import fail
                        throw new ApplicationBusinessException(t("tutti.service.multipost.import.wrongSpecies.error"));

                    }

                    SpeciesBatchFrequency frequency = newFrequencySupplier.get();
                    frequency.setLengthStepCaracteristic(frequencyRow.getLengthStepCaracteristic());
                    frequency.setLengthStep(frequencyRow.getLengthStep());
                    frequency.setNumber(frequencyRow.getNumber());
                    frequency.setWeight(frequencyRow.getWeight());
//                    frequency.setBatch(speciesBatch);
                    frequenciesToSave.add(frequency);

                }
            }

        }

        return frequenciesToSave;

    }

    protected Map<String, IndividualObservationBatch> importIndividualObservationBatches(MultiPostImportContext importContext,
                                                                                         SpeciesBatch speciesOfBenthosBatch) throws IOException {

        Map<String, IndividualObservationBatch> batches = new LinkedHashMap<>();

        try (Reader reader = importContext.newFileReader(INDIVIDUAL_OBSERVATION_FILE)) {

            IndividualObservationRowModel csvModel = IndividualObservationRowModel.forImport(
                    importContext.getSpeciesList(),
                    importContext.getCaracteristicsWithProtected());

            try (Import<IndividualObservationRow> importer = Import.newImport(csvModel, reader)) {

                FishingOperation operation = importContext.getOperation();

                Integer requiredSpeciesId = speciesOfBenthosBatch.getSpecies().getIdAsInt();
                Integer speciesOrBenthosBatchId = speciesOfBenthosBatch.getIdAsInt();

                for (IndividualObservationRow row : importer) {

                    if (!requiredSpeciesId.equals(row.getSpecies().getIdAsInt())) {

                        // Bad species, import fail
                        throw new ApplicationBusinessException(t("tutti.service.multipost.import.wrongSpecies.error"));

                    }

                    // create batch
                    IndividualObservationBatch batch = IndividualObservationBatchs.newIndividualObservationBatch();
                    batch.setFishingOperation(operation);

                    batch.setBatchId(speciesOrBenthosBatchId);

                    batch.setSpecies(row.getSpecies());
                    batch.setWeight(row.getWeight());
                    batch.setSize(row.getSize());
                    batch.setLengthStepCaracteristic(row.getLengthStepCaracteristic());
                    batch.setComment(row.getComment());
                    batch.setCaracteristics(new CaracteristicMap());
                    if (StringUtils.isNotEmpty(row.getSamplingCode())) {
                        batch.setSamplingCode(row.getSamplingCode());
                    }
                    batch.setCopyIndividualObservationMode(row.getCopyIndividualObservationMode());

                    batches.put(row.getBatchId(), batch);
                }

            }

        }

        return batches;

    }

    private void importIndividualObservations(MultiPostImportContext importContext, boolean importFrequencies, Map<String, SpeciesBatch> batches, Map<String, SpeciesBatch> notImportedBatches, FrequencyHelper frequencyHelper) throws IOException {

        // Import batches
        // map containing the batches by their persistence id
        Map<String, IndividualObservationBatch> individualObservationsBatches = importIndividualObservationBatches(importContext, batches, notImportedBatches);

        // Import caracteristics

        importCaracteristics(importContext, individualObservationsBatches);

        // Persist batches

        persistenceService.createIndividualObservationBatches(context.getDataContext().getFishingOperation(), individualObservationsBatches.values());

        // Import attachments

        importAttachments(importContext, individualObservationsBatches, ObjectTypeCode.SAMPLE);

        if (!importFrequencies) {

            // Must generate frequencies if required
            batches.values().forEach(speciesBatch -> {

                Integer speciesBatchId = speciesBatch.getIdAsInt();

                List<IndividualObservationBatch> allIndividualObservationBatchsForBatch = persistenceService.getAllIndividualObservationBatchsForBatch(speciesBatchId);
                if (!allIndividualObservationBatchsForBatch.isEmpty()) {
                    IndividualObservationBatch firstIndividualObservationBatch = allIndividualObservationBatchsForBatch.get(0);
                    CopyIndividualObservationMode copyIndividualObservationMode = firstIndividualObservationBatch.getCopyIndividualObservationMode();
                    Caracteristic lengthStepCaracteristic = firstIndividualObservationBatch.getLengthStepCaracteristic();

                    boolean copySize = false;
                    final boolean copyWeight;

                    switch (copyIndividualObservationMode) {

                        case ALL:
                            copySize = true;
                            copyWeight = true;
                            break;

                        case SIZE:
                            copySize = true;
                            copyWeight = false;
                            break;

                        case NOTHING:// nothing we said!
                        default:
                            copyWeight = false;

                    }
                    if (copySize) {

                        float step = getStep(lengthStepCaracteristic);
                        Map<Float, SpeciesBatchFrequency> frequencies = new TreeMap<>();

                        allIndividualObservationBatchsForBatch
                                .stream()
                                .filter(individualObservationBatch -> individualObservationBatch.getSize() != null)
                                .forEach(individualObservationBatch -> {

                                    // compute the lengthstep according to the step of the lengthstep caracteristic
                                    Float lengthStep = getLengthStep(individualObservationBatch.getSize(), step);
                                    // get the existing frequency
                                    SpeciesBatchFrequency frequency = frequencies.get(lengthStep);

                                    if (frequency == null) {
                                        // or create a new one
                                        frequency = frequencyHelper.newSpeciesBatchFrequency();
                                        frequency.setLengthStep(lengthStep);
                                        frequency.setLengthStepCaracteristic(lengthStepCaracteristic);
                                        frequency.setNumber(0);
                                        frequency.setBatch(speciesBatch);

                                        // only set a weight if we copy the weights
                                        frequency.setWeight(copyWeight ? 0f : null);

                                        frequencies.put(lengthStep, frequency);
                                    }

                                    // increment the number of the frequency
                                    frequency.setNumber(frequency.getNumber() + 1);

                                    if (copyWeight) {
                                        // there should always be a weight if we must copy the weight
                                        Objects.requireNonNull(individualObservationBatch.getWeight(), t("tutti.service.multipost.import.noWeight.error"));
                                        frequency.setWeight(frequency.getWeight() + individualObservationBatch.getWeight());
                                    }

                                });

                        // round the final weights
                        if (copyWeight) {
                            frequencies.values().forEach(frequency -> frequency.setWeight(WeightUnit.KG.round(frequency.getWeight())));
                        }
                        frequencyHelper.saveSpeciesBatchFrequency(speciesBatchId, new ArrayList<>(frequencies.values()));
                    }

                }
            });
        }

    }

    private float getStep(Caracteristic caracteristic) {
        Float step = null;
        if (caracteristic != null) {
            step = caracteristic.getPrecision();
        }
        if (step == null) {
            step = CaracteristicBean.DEFAULT_PRECISION;
        }
        return step;
    }

    private float getLengthStep(float lengthStep, float step) {
        int intValue = (int) (lengthStep * 10);
        int intStep = (int) (step * 10);
        int correctIntStep = intValue - (intValue % intStep);
        return correctIntStep / 10f;
    }

    private MultiPostImportResult importSpeciesOrBenthosBatch(File file,
                                                              FishingOperation operation,
                                                              SpeciesBatch speciesBatch,
                                                              boolean importFrequencies,
                                                              boolean importIndividualObservations) {

        return persistenceService.invoke(() -> {


            try (MultiPostImportContext importContext = new MultiPostImportContext(context, file, operation)) {

                // Import frequencies

                List<SpeciesBatchFrequency> frequenciesToSave = new ArrayList<>();

                if (importFrequencies) {
                    frequenciesToSave.addAll(importFrequencies(importContext, speciesBatch, SpeciesBatchFrequencys::newSpeciesBatchFrequency));
                    frequenciesToSave.forEach(speciesBatchFrequency -> speciesBatchFrequency.setBatch(speciesBatch));
                }

                // Import indivudal observations

                List<IndividualObservationBatch> individualObservations = new ArrayList<>();

                Multimap<Integer, File> attachmentFiles = HashMultimap.create();

                if (importIndividualObservations) {

                    // Import batches
                    // map containing the batches by their persistence id
                    Map<String, IndividualObservationBatch> individualObservationsBatches = importIndividualObservationBatches(importContext, speciesBatch);

                    // Import caracteristics

                    importCaracteristics(importContext, individualObservationsBatches);

                    individualObservations.addAll(individualObservationsBatches.values());

                    // Import attachments

                    attachmentFiles.putAll(getAttachmentFiles(importContext, individualObservationsBatches, ObjectTypeCode.SAMPLE));

                }

                return new MultiPostImportResult(frequenciesToSave, individualObservations, attachmentFiles);

            }

        });

    }
}
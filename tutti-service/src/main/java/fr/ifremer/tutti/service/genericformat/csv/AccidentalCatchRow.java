package fr.ifremer.tutti.service.genericformat.csv;

/*
 * #%L
 * Tutti :: Service
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.entities.referential.Caracteristic;
import fr.ifremer.tutti.persistence.entities.referential.Species;

import java.io.Serializable;

/**
 * A row in a accidental catch export.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.2
 */
public class AccidentalCatchRow extends RowWithOperationContextSupport {

    private static final long serialVersionUID = 1L;

    public static final String PROPERTY_BATCH_ID = "batchId";

    public static final String PROPERTY_CARACTERISTIC = "caracteristic";

    public static final String PROPERTY_CARACTERISTIC_VALUE = "caracteristicValue";

    public static AccidentalCatchRow newEmptyInstance() {
        AccidentalCatchRow row = new AccidentalCatchRow();
        row.forImport();
        return row;
    }

    protected Integer batchId;

    protected Species species;

    protected String comment;

    protected Caracteristic caracteristic;

    protected Serializable caracteristicValue;

    public void setBatchId(Integer batchId) {
        this.batchId = batchId;
    }

    public void setCaracteristic(Caracteristic caracteristic) {
        this.caracteristic = caracteristic;
    }

    public void setCaracteristicValue(Serializable caracteristicValue) {
        this.caracteristicValue = caracteristicValue;
    }

    public void setSpecies(Species species) {
        this.species = species;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Integer getBatchId() {
        return batchId;
    }

    public Caracteristic getCaracteristic() {
        return caracteristic;
    }

    public Serializable getCaracteristicValue() {
        return caracteristicValue;
    }

    public String getComment() {
        return comment;
    }

    public Species getSpecies() {
        return species;
    }

}

package fr.ifremer.tutti.service.genericformat.exportactions;

/*
 * #%L
 * Tutti :: Service
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.adagio.core.dao.referential.ObjectTypeCode;
import fr.ifremer.tutti.persistence.entities.data.Attachment;
import fr.ifremer.tutti.persistence.entities.data.IndividualObservationBatch;
import fr.ifremer.tutti.service.PersistenceService;
import fr.ifremer.tutti.service.genericformat.GenericFormatExportContext;
import fr.ifremer.tutti.service.genericformat.GenericFormatExportOperationContext;
import fr.ifremer.tutti.service.genericformat.csv.AttachmentRow;
import fr.ifremer.tutti.service.genericformat.csv.IndividualObservationRow;
import fr.ifremer.tutti.service.genericformat.producer.CsvProducerForAttachment;

import java.util.ArrayList;
import java.util.List;

/**
 * Created on 3/28/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.14.3
 */
public class CreateIndividualObservationRowsAction extends ExportFishingOperationActionSupport {

    private final PersistenceService persistenceService;

    public CreateIndividualObservationRowsAction(PersistenceService persistenceService) {
        this.persistenceService = persistenceService;
    }

    @Override
    public boolean doExecute(GenericFormatExportContext exportContext, GenericFormatExportOperationContext operationContext) {
        return exportContext.isExportIndividualObservation() && operationContext.isWithCatchBatch();
    }

    @Override
    public void execute(GenericFormatExportContext exportContext, GenericFormatExportOperationContext operationContext) {

        List<IndividualObservationRow> individualObservationRows = exportContext.getProducerForIndividualObservation().getDataToExport(operationContext);
        operationContext.setIndividualObservationRows(individualObservationRows);
        exportContext.getProducerForSpecies().prepareIndividualRows(individualObservationRows);

        if (exportContext.isExportAttachments()) {

            List<AttachmentRow> attachmentRows = getAttachmentRows(exportContext, operationContext.getIndividualObservations());
            operationContext.addAttachmentRows(attachmentRows);

        }

    }

    public List<AttachmentRow> getAttachmentRows(GenericFormatExportContext exportContext, List<IndividualObservationBatch> individualObservationBatches) {

        CsvProducerForAttachment producerForAttachment = exportContext.getProducerForAttachment();

        List<AttachmentRow> attachmentRows = new ArrayList<>();

        for (IndividualObservationBatch individualObservationBatch : individualObservationBatches) {
            List<Attachment> attachments = persistenceService.getAllAttachments(ObjectTypeCode.SAMPLE, individualObservationBatch.getIdAsInt());
            producerForAttachment.addAttachments(attachments, attachmentRows);
        }

        return attachmentRows;

    }
}

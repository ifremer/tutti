package fr.ifremer.tutti.service.genericformat.producer;

/*
 * #%L
 * Tutti :: Service
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import fr.ifremer.tutti.persistence.entities.data.BatchContainer;
import fr.ifremer.tutti.persistence.entities.data.SpeciesBatch;
import fr.ifremer.tutti.persistence.entities.referential.Species;
import fr.ifremer.tutti.service.csv.CsvProducer;
import fr.ifremer.tutti.service.genericformat.csv.AccidentalCatchRow;
import fr.ifremer.tutti.service.genericformat.csv.IndividualObservationRow;
import fr.ifremer.tutti.service.genericformat.csv.SpeciesExportModel;
import fr.ifremer.tutti.service.genericformat.csv.SpeciesExportRow;

import java.nio.file.Path;
import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * Created on 2/6/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.13
 */
public class CsvProducerForSpecies extends CsvProducer<SpeciesExportRow, SpeciesExportModel> {

    final Map<String, Species> speciesByReferenceTaxonId;

    final Map<String, SpeciesExportRow> speciesToExport = Maps.newTreeMap();

    public CsvProducerForSpecies(Path file, SpeciesExportModel model, Map<String, Species> speciesByReferenceTaxonId) {
        super(file, model);
        this.speciesByReferenceTaxonId = speciesByReferenceTaxonId;
    }

    public void prepareSpeciesBatchRows(BatchContainer<SpeciesBatch> rootSpeciesBatch) {

        for (SpeciesBatch speciesBatch : rootSpeciesBatch.getChildren()) {
            addSpecies(speciesBatch.getSpecies());
        }

    }

    public void prepareBenthosBatchRows(BatchContainer<SpeciesBatch> rootBenthosBatch) {

        for (SpeciesBatch benthosBatch : rootBenthosBatch.getChildren()) {
            addSpecies(benthosBatch.getSpecies());
        }

    }

    public void prepareIndividualRows(List<IndividualObservationRow> rows) {

        for (IndividualObservationRow row : rows) {
            addSpecies(row.getSpecies());
        }

    }

    public void prepareAccidentalRows(List<AccidentalCatchRow> rows) {

        for (AccidentalCatchRow row : rows) {
            addSpecies(row.getSpecies());
        }

    }

    public List<SpeciesExportRow> getDataToExport() {

        List<SpeciesExportRow> result = Lists.newArrayList(speciesToExport.values());

        Collections.sort(result, (o1, o2) -> o1.getSpecies().getReferenceTaxonId().compareTo(o2.getSpecies().getReferenceTaxonId()));
        return result;

    }

    public void addSpecies(Species species) {

        String speciesId = String.valueOf(species.getReferenceTaxonId());

        if (!speciesToExport.containsKey(speciesId)) {

            // not treated species, add a new row
            Species fullSpecies = speciesByReferenceTaxonId.get(speciesId);
            SpeciesExportRow row = new SpeciesExportRow();
            row.setSpecies(fullSpecies);
            speciesToExport.put(speciesId, row);
            // add the survey code (see http://forge.codelutin.com/issues/4799)
            species.setSurveyCode(fullSpecies.getSurveyCode());
        }
    }

}

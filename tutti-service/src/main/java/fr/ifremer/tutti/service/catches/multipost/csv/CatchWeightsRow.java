package fr.ifremer.tutti.service.catches.multipost.csv;

/*
 * #%L
 * Tutti :: Service
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

/**
 * @author Kevin Morin - kmorin@codelutin.com
 * @since 2.3
 */
public class CatchWeightsRow extends AbstractFishingOperationRow {

    public static final String TOTAL_SORTED_WEIGHT = "totalSortedWeight";

    public static final String INERT_WEIGHT = "inertWeight";

    public static final String LIVING_NOT_ITEMIZED_WEIGHT = "livingNotItemizedWeight";

    protected Float totalSortedWeight;

    protected Float inertWeight;

    protected Float livingNotItemizedWeight;

    public Float getTotalSortedWeight() {
        return totalSortedWeight;
    }

    public void setTotalSortedWeight(Float totalSortedWeight) {
        this.totalSortedWeight = totalSortedWeight;
    }

    public Float getInertWeight() {
        return inertWeight;
    }

    public void setInertWeight(Float inertWeight) {
        this.inertWeight = inertWeight;
    }

    public Float getLivingNotItemizedWeight() {
        return livingNotItemizedWeight;
    }

    public void setLivingNotItemizedWeight(Float livingNotItemizedWeight) {
        this.livingNotItemizedWeight = livingNotItemizedWeight;
    }
}

package fr.ifremer.tutti.service.export;

/*
 * #%L
 * Tutti :: Service
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Predicate;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Multimap;
import fr.ifremer.tutti.persistence.entities.data.BatchContainer;
import fr.ifremer.tutti.persistence.entities.data.CatchBatch;
import fr.ifremer.tutti.persistence.entities.data.FishingOperation;
import fr.ifremer.tutti.persistence.entities.data.MarineLitterBatch;
import fr.ifremer.tutti.persistence.entities.data.SpeciesBatch;
import fr.ifremer.tutti.persistence.entities.data.SpeciesBatchFrequency;
import fr.ifremer.tutti.persistence.entities.referential.Species;
import fr.ifremer.tutti.service.PersistenceService;
import fr.ifremer.tutti.service.catches.WeightComputingService;
import fr.ifremer.tutti.util.Numbers;
import org.apache.commons.collections4.CollectionUtils;

import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * Contains the global context for a catch export.
 *
 * Created on 11/21/13.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.0-rc-1
 */
public class ExportCatchContext {

    final FishingOperation fishingOperation;

    final CatchBatch catchBatch;

    final BatchContainer<SpeciesBatch> rootSpeciesBatch;

    final BatchContainer<SpeciesBatch> rootBenthosBatch;

    final BatchContainer<MarineLitterBatch> marineLitterBatches;

    final Multimap<Species, SpeciesBatchFrequency> speciesFrequencies;

    final Multimap<Species, SpeciesBatchFrequency> benthosFrequencies;

    final Predicate<SpeciesBatch> vracPredicate;

    public static ExportCatchContext newExportContext(PersistenceService persistenceService,
                                                      WeightComputingService weightComputingService,
                                                      Integer fishingOperationId,
                                                      boolean loadFrequencies) {

        FishingOperation fishingOperation =
                persistenceService.getFishingOperation(fishingOperationId);

        CatchBatch catchBatch =
                persistenceService.getCatchBatchFromFishingOperation(fishingOperationId);

        BatchContainer<SpeciesBatch> rootSpeciesBatch =
                weightComputingService.getComputedSpeciesBatches(fishingOperationId);

        BatchContainer<SpeciesBatch> rootBenthosBatch =
                weightComputingService.getComputedBenthosBatches(fishingOperationId);

        BatchContainer<MarineLitterBatch> marineLitterBatches =
                weightComputingService.getComputedMarineLitterBatches(
                        fishingOperationId,
                        catchBatch.getMarineLitterTotalWeight());
        weightComputingService.computeCatchBatchWeights(catchBatch,
                                                        rootSpeciesBatch,
                                                        rootBenthosBatch,
                                                        marineLitterBatches);

        Multimap<Species, SpeciesBatchFrequency> speciesFrequencies;
        Multimap<Species, SpeciesBatchFrequency> benthosFrequencies;

        if (loadFrequencies) {

            speciesFrequencies = persistenceService.getAllSpeciesBatchFrequencyForBatch(rootSpeciesBatch);
            benthosFrequencies = persistenceService.getAllBenthosBatchFrequencyForBatch(rootBenthosBatch);
        } else {
            speciesFrequencies = null;
            benthosFrequencies = null;
        }

        Predicate<SpeciesBatch> vracPredicate = persistenceService.getVracBatchPredicate();

        return new ExportCatchContext(vracPredicate,
                                      fishingOperation,
                                      catchBatch,
                                      rootSpeciesBatch,
                                      speciesFrequencies,
                                      rootBenthosBatch,
                                      benthosFrequencies,
                                      marineLitterBatches);

    }

    private ExportCatchContext(Predicate<SpeciesBatch> vracPredicate,
                               FishingOperation fishingOperation,
                               CatchBatch catchBatch,
                               BatchContainer<SpeciesBatch> rootSpeciesBatch,
                               Multimap<Species, SpeciesBatchFrequency> speciesFrequencies,
                               BatchContainer<SpeciesBatch> rootBenthosBatch,
                               Multimap<Species, SpeciesBatchFrequency> benthosFrequencies,
                               BatchContainer<MarineLitterBatch> marineLitterBatches) {
        this.vracPredicate = vracPredicate;
        this.fishingOperation = fishingOperation;
        this.catchBatch = catchBatch;
        this.rootSpeciesBatch = rootSpeciesBatch;
        this.speciesFrequencies = speciesFrequencies;
        this.benthosFrequencies = benthosFrequencies;
        this.rootBenthosBatch = rootBenthosBatch;
        this.marineLitterBatches = marineLitterBatches;
    }

    public FishingOperation getFishingOperation() {
        return fishingOperation;
    }

    public float getCatchTotalWeight() {
        return Numbers.getValueOrComputedValue(
                catchBatch.getCatchTotalWeight(),
                catchBatch.getCatchTotalComputedWeight());
    }

    public float getCatchTotalSortedWeight() {
        return catchBatch.getSpeciesTotalSampleSortedComputedWeight() +
                catchBatch.getBenthosTotalSampleSortedComputedWeight() +
                catchBatch.getSpeciesTotalUnsortedComputedWeight() +
                catchBatch.getBenthosTotalUnsortedComputedWeight();
    }

    public float getSpeciesTotalSortedWeight() {
        return Numbers.getValueOrComputedValue(
                catchBatch.getSpeciesTotalSortedWeight(),
                catchBatch.getSpeciesTotalSortedComputedWeight());
    }

    public float getBenthosTotalSortedWeight() {
        return Numbers.getValueOrComputedValue(
                catchBatch.getBenthosTotalSortedWeight(),
                catchBatch.getBenthosTotalSortedComputedWeight());
    }

    public boolean withSpeciesBatches() {
        return rootSpeciesBatch != null && !rootSpeciesBatch.isEmptyChildren();
    }

    public boolean withBenthosBatches() {
        return rootBenthosBatch != null && !rootBenthosBatch.isEmptyChildren();
    }

    public boolean withSpeciesFrequencies() {
        return speciesFrequencies != null && !speciesFrequencies.isEmpty();
    }

    public boolean withBenthosFrequencies() {
        return benthosFrequencies != null && !benthosFrequencies.isEmpty();
    }

    public List<ExportBatchEntry> getSpeciesBatchEntry(boolean computeNumber) {
        List<ExportBatchEntry> catchList = Lists.newArrayList();

        if (withSpeciesBatches()) {

            boolean withFrequencies = withSpeciesFrequencies();

            Map<Species, ExportBatchEntry> catches = Maps.newLinkedHashMap();

            // ratio total species weight / total sorted sampled species weight
            float rate = getSpeciesElevationRate();

            List<SpeciesBatch> batches = rootSpeciesBatch.getChildren();
            for (SpeciesBatch batch : batches) {

                ExportBatchEntry aCatch = createExportBatchCatch(
                        batch,
                        catches,
                        rate,
                        computeNumber);

                if (withFrequencies) {
                    addFrequencies(aCatch, speciesFrequencies);
                }
            }
            catchList.addAll(catches.values());
        }
        return catchList;
    }

    public List<ExportBatchEntry> getBenthosBatchEntry(boolean computeNumber) {
        List<ExportBatchEntry> catchList = Lists.newArrayList();

        if (withBenthosBatches()) {

            boolean withFrequencies = withBenthosFrequencies();

            Map<Species, ExportBatchEntry> catches = Maps.newLinkedHashMap();

            // ratio total species weight / total sorted sampled species weight
            float rate = getBenthosElevationRate();

            List<SpeciesBatch> batches = rootBenthosBatch.getChildren();
            for (SpeciesBatch batch : batches) {

                ExportBatchEntry aCatch = createExportBatchCatch(
                        batch,
                        catches,
                        rate,
                        computeNumber);

                if (withFrequencies) {
                    addFrequencies(aCatch, benthosFrequencies);
                }
            }

            catchList.addAll(catches.values());
        }
        return catchList;
    }

    public ExportBatchEntry getInertAndLivingNotItemizedCatch() {

        ExportBatchEntry result = new ExportBatchEntry(null);

        float speciesRatio = getSpeciesElevationRate();
        float benthosRatio = getBenthosElevationRate();

        Float speciesInterWeight = Numbers.getValueOrComputedValue(
                catchBatch.getSpeciesTotalInertWeight(),
                catchBatch.getSpeciesTotalInertComputedWeight());

        if (speciesInterWeight != null) {
            result.addSortedWeight(speciesInterWeight);
            result.addTotalWeight(speciesInterWeight * speciesRatio);
        }

        Float speciesLivingNotItemizedWeigth = Numbers.getValueOrComputedValue(
                catchBatch.getSpeciesTotalLivingNotItemizedWeight(),
                catchBatch.getSpeciesTotalLivingNotItemizedComputedWeight());

        if (speciesLivingNotItemizedWeigth != null) {
            result.addSortedWeight(speciesLivingNotItemizedWeigth);
            result.addTotalWeight(speciesLivingNotItemizedWeigth * speciesRatio);
        }

        Float benthosInterWeight = Numbers.getValueOrComputedValue(
                catchBatch.getBenthosTotalInertWeight(),
                catchBatch.getBenthosTotalInertComputedWeight());

        if (benthosInterWeight != null) {
            result.addSortedWeight(benthosInterWeight);
            result.addTotalWeight(benthosInterWeight * benthosRatio);
        }

        Float benthosLivingNotItemizedWeight = Numbers.getValueOrComputedValue(
                catchBatch.getBenthosTotalLivingNotItemizedWeight(),
                catchBatch.getBenthosTotalLivingNotItemizedComputedWeight());

        if (benthosLivingNotItemizedWeight != null) {
            result.addSortedWeight(benthosLivingNotItemizedWeight);
            result.addTotalWeight(benthosLivingNotItemizedWeight * benthosRatio);
        }

        return result;
    }

    public boolean isVracBatch(SpeciesBatch batch) {
        return vracPredicate.apply(batch);
    }

    protected float getSpeciesElevationRate() {

        float globalRatio = catchBatch.getCatchTotalSortedComputedWeight() / catchBatch.getCatchTotalSortedSortedComputedWeight();

        float speciesTotalSortedWeight = getSpeciesTotalSortedWeight();

        // ratio total species weight / total sorted sampled species weight
        float result = globalRatio * speciesTotalSortedWeight;
        if (catchBatch.getSpeciesTotalSampleSortedComputedWeight() > 0) {
            result /= catchBatch.getSpeciesTotalSampleSortedComputedWeight();
        }
        return result;
    }

    protected float getBenthosElevationRate() {

        float globalRatio = catchBatch.getCatchTotalSortedComputedWeight() / catchBatch.getCatchTotalSortedSortedComputedWeight();

        float benthosTotalSortedWeight = getBenthosTotalSortedWeight();

        // ratio total benthos weight / total sorted sampled benthos weight
        float result = globalRatio * benthosTotalSortedWeight;
        if (catchBatch.getBenthosTotalSampleSortedComputedWeight() > 0) {
            result /= catchBatch.getBenthosTotalSampleSortedComputedWeight();
        }
        return result;
    }

    protected ExportBatchEntry createExportBatchCatch(SpeciesBatch batch,
                                                      Map<Species, ExportBatchEntry> catches,
                                                      float ratio,
                                                      boolean computeNumber) {

        Species species = batch.getSpecies();

        ExportBatchEntry ktch = catches.get(species);
        if (ktch == null) {
            ktch = new ExportBatchEntry(batch);
            catches.put(species, ktch);
        }

        float sortedWeight = Numbers.getValueOrComputedValue(
                batch.getSampleCategoryWeight(),
                batch.getSampleCategoryComputedWeight());

        ktch.addSortedWeight(sortedWeight);

        boolean isVracBatch = isVracBatch(batch);
        float speciesTotalWeight = sortedWeight;
        if (isVracBatch) {
            speciesTotalWeight *= ratio;
        }
        ktch.addTotalWeight(speciesTotalWeight);

        if (computeNumber) {
            float number = Math.round(computeNumber(batch, 1.0f));
            if (isVracBatch) {
                number *= ratio;
            }
            ktch.addNumber(Math.round(number));
        }
        return ktch;
    }

    protected float computeNumber(SpeciesBatch batch, float rf) {
        float result;

        float weight = Numbers.getValueOrComputedValue(
                batch.getSampleCategoryWeight(),
                batch.getSampleCategoryComputedWeight());

        if (batch.isChildBatchsEmpty()) {

            // on a leaf, get his weight

            Integer number = Numbers.getValueOrComputedValue(
                    batch.getNumber(),
                    batch.getComputedNumber());
            if (number == null) {

                // no count
                number = 0;
            }

            // get the sample weight
            Float subweight = Numbers.getValueOrComputedValue(
                    batch.getWeight(),
                    batch.getComputedWeight());

            if (subweight != null) {

                // with sub sample, update the raising factor
                rf *= weight / subweight;
            }

            result = number.floatValue() * rf;
        } else {

            // get total weight of all childs
            float totalWeight = 0.f;
            for (SpeciesBatch child : batch.getChildBatchs()) {
                totalWeight += Numbers.getValueOrComputedValue(
                        child.getSampleCategoryWeight(),
                        child.getSampleCategoryComputedWeight());
            }

            result = 0f;

            float rf2 = rf * weight / totalWeight;
            // sum result of each child
            for (SpeciesBatch child : batch.getChildBatchs()) {

                result += computeNumber(child, rf2);
            }

        }
        return result;
    }

    protected <F extends SpeciesBatchFrequency> void addFrequencies(ExportBatchEntry aCatch,
                                                                    Multimap<Species, F> frequencies) {
        Species species = aCatch.getBatch().getSpecies();
        Collection<F> batchFrequencies = frequencies.get(species);
        if (CollectionUtils.isNotEmpty(batchFrequencies)) {
            for (F batchFrequency : batchFrequencies) {
                Integer number = batchFrequency.getNumber();
                Float lengthStep = batchFrequency.getLengthStep();
                aCatch.addFrequency(lengthStep, number);
            }
        }
    }
}

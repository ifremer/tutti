package fr.ifremer.tutti.service;

/*
 * #%L
 * Tutti :: Service
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.io.Closeable;

/**
 * Contract for any tutti service.
 *
 * This contract just allow to inject the {@link TuttiServiceContext} in your service.
 *
 * A default support implement is given: {@link AbstractTuttiService}.
 *
 * To use a such service, you should not instanciate it by your self but prefer
 * use the servie factory method: {@link TuttiServiceContext#getService(Class)}.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @see AbstractTuttiService
 * @since 0.1
 */
public interface TuttiService extends Closeable {

    void setServiceContext(TuttiServiceContext context);
}

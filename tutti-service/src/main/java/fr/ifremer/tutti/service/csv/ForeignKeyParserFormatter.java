package fr.ifremer.tutti.service.csv;

/*
 * #%L
 * Tutti :: Service
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.entities.TuttiEntity;
import org.apache.commons.lang3.StringUtils;
import org.nuiton.csv.ImportRuntimeException;
import org.nuiton.csv.ValueParserFormatter;

import java.text.ParseException;
import java.util.Map;

import static org.nuiton.i18n.I18n.t;

/**
 * @param <E>
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.0
 */
public class ForeignKeyParserFormatter<E extends TuttiEntity> implements ValueParserFormatter<E> {

    protected final String propertyName;

    protected final Class<E> entityType;

    protected final Map<String, E> universe;

    public ForeignKeyParserFormatter(Class<E> entityType, String propertyName, Map<String, E> universe) {
        this.entityType = entityType;
        this.propertyName = propertyName;
        this.universe = universe;
    }

    @Override
    public E parse(String value) throws ParseException {
        E result = null;
        if (StringUtils.isNotBlank(value)) {

            // get entity from universe
            result = universe.get(value);

            if (result == null) {

                // can not find entity this is a big problem for us...
                throw new ImportRuntimeException(t("tutti.service.csv.parse.entityNotFound", entityType.getSimpleName(), propertyName, value));
            }
        }
        return result;
    }

    @Override
    public String format(E e) {
        String value = "";
        if (e != null) {
            value = e.getId();
        }
        return value;
    }
}

package fr.ifremer.tutti.service.genericformat;

/*
 * #%L
 * Tutti :: Service
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Function;
import com.google.common.base.Preconditions;
import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Multimap;
import com.google.common.collect.Ordering;
import fr.ifremer.adagio.core.dao.referential.ObjectTypeCode;
import fr.ifremer.tutti.persistence.ProgressionModel;
import fr.ifremer.tutti.persistence.entities.data.CatchBatch;
import fr.ifremer.tutti.persistence.entities.data.Cruise;
import fr.ifremer.tutti.persistence.entities.data.Cruises;
import fr.ifremer.tutti.persistence.entities.data.FishingOperation;
import fr.ifremer.tutti.persistence.entities.data.SampleCategoryModel;
import fr.ifremer.tutti.persistence.entities.protocol.TuttiProtocol;
import fr.ifremer.tutti.persistence.entities.referential.Caracteristic;
import fr.ifremer.tutti.persistence.entities.referential.Gear;
import fr.ifremer.tutti.persistence.entities.referential.Person;
import fr.ifremer.tutti.persistence.entities.referential.TuttiReferentialEntity;
import fr.ifremer.tutti.persistence.entities.referential.Vessel;
import fr.ifremer.tutti.persistence.model.CruiseDataModel;
import fr.ifremer.tutti.service.PersistenceService;
import fr.ifremer.tutti.service.ValidationService;
import fr.ifremer.tutti.service.csv.CaracteristicValueParseException;
import fr.ifremer.tutti.service.csv.CaracteristicValueParserFormatter;
import fr.ifremer.tutti.service.genericformat.consumer.CsvConsumerForAccidentalCatch;
import fr.ifremer.tutti.service.genericformat.consumer.CsvConsumerForAttachment;
import fr.ifremer.tutti.service.genericformat.consumer.CsvConsumerForCatch;
import fr.ifremer.tutti.service.genericformat.consumer.CsvConsumerForGearCaracteristic;
import fr.ifremer.tutti.service.genericformat.consumer.CsvConsumerForIndividualObservation;
import fr.ifremer.tutti.service.genericformat.consumer.CsvConsumerForMarineLitter;
import fr.ifremer.tutti.service.genericformat.consumer.CsvConsumerForOperation;
import fr.ifremer.tutti.service.genericformat.consumer.CsvConsumerForParameter;
import fr.ifremer.tutti.service.genericformat.consumer.CsvConsumerForSampleCategory;
import fr.ifremer.tutti.service.genericformat.consumer.CsvConsumerForSurvey;
import fr.ifremer.tutti.service.genericformat.csv.AttachmentRow;
import fr.ifremer.tutti.service.genericformat.csv.RowWithCruiseContextSupport;
import fr.ifremer.tutti.service.referential.consumer.CsvConsumerForTemporaryGear;
import fr.ifremer.tutti.service.referential.consumer.CsvConsumerForTemporaryPerson;
import fr.ifremer.tutti.service.referential.consumer.CsvConsumerForTemporarySpecies;
import fr.ifremer.tutti.service.referential.consumer.CsvConsumerForTemporaryVessel;
import org.apache.commons.collections4.CollectionUtils;
import org.nuiton.decorator.Decorator;

import java.io.Closeable;
import java.io.File;
import java.io.Serializable;
import java.nio.file.Path;
import java.text.ParseException;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

/**
 * Created on 2/5/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.13
 */
public abstract class GenericFormatContextSupport implements Closeable, Iterable<GenericFormatImportCruiseContext> {

    private final GenericFormatImportRequest importRequest;

    private final ProgressionModel progressionModel;

    private final Decorator<Cruise> cruiseDecorator;

    private final Decorator<FishingOperation> fishingOperationDecorator;

    private final GenericFormatImportEntityParserFactory importEntityParserFactory;

    private final GenericFormatImportValidationHelper validationHelper;

    private Set<String> archiveLayoutErrors;

    private TuttiProtocol importedProtocol;

    private SampleCategoryModel importedSampleCategoryModel;

    private final GenericFormatCsvFileResult sampleCategoryFileResult;

    private final GenericFormatReferentialImportResult<Gear, Integer> referentialTemporaryGearFileResult;

    private final GenericFormatReferentialImportResult<Person, Integer> referentialTemporaryPersonFileResult;

    private final GenericFormatReferentialSpeciesImportResult referentialTemporarySpeciesFileResult;

    private final GenericFormatReferentialImportResult<Vessel, String> referentialTemporaryVesselFileResult;

    private final GenericFormatFileResult protocolFileResult;

    private final GenericFormatCsvFileResult surveyFileResult;

    private final GenericFormatCsvFileResult accidentalCatchFileResult;

    private final GenericFormatCsvFileResult individualObservationFileResult;

    private final GenericFormatCsvFileResult marineLitterFileResult;

    private final GenericFormatCsvFileResult catchFileResult;

    private final GenericFormatCsvFileResult parameterFileResult;

    private final GenericFormatCsvFileResult operationFileResult;

    private final GenericFormatCsvFileResult gearCaracteristicFileResult;

    private final GenericFormatCsvFileResult attachmentFileResult;

    private final Map<String, GenericFormatImportCruiseContext> cruiseContexts;

    private final Set<String> skippedCruisesNaturalId;

    private String protocolOriginalName;

    private final Multimap<String, AttachmentRow> attachmentRows;

    protected abstract void onClose();

    public GenericFormatContextSupport(GenericFormatImportRequest importRequest,
                                       ProgressionModel progressionModel,
                                       PersistenceService persistenceService,
                                       Decorator<Cruise> cruiseDecorator,
                                       Decorator<FishingOperation> fishingOperationDecorator) {

        Preconditions.checkNotNull(importRequest);
        Preconditions.checkNotNull(progressionModel);
        Preconditions.checkNotNull(persistenceService);
        Preconditions.checkNotNull(cruiseDecorator);
        Preconditions.checkNotNull(fishingOperationDecorator);

        this.importRequest = importRequest;
        this.progressionModel = progressionModel;
        this.cruiseDecorator = cruiseDecorator;
        this.fishingOperationDecorator = fishingOperationDecorator;
        this.importEntityParserFactory = new GenericFormatImportEntityParserFactory(persistenceService, this);
        this.validationHelper = new GenericFormatImportValidationHelper(this, new ValidationService());
        this.cruiseContexts = new LinkedHashMap<>();
        this.skippedCruisesNaturalId = new LinkedHashSet<>();

        GenericFormatArchive archive = importRequest.getArchive();

        this.sampleCategoryFileResult = createFileResult(archive.getSampleCategoryModelPath(), true);

        this.referentialTemporaryGearFileResult = createReferentialFileResult(archive.getTemporaryReferentialGearsPath());
        this.referentialTemporaryPersonFileResult = createReferentialFileResult(archive.getTemporaryReferentialPersonsPath());
        this.referentialTemporaryVesselFileResult = createReferentialFileResult(archive.getTemporaryReferentialVesselsPath());
        File file = archive.getTemporaryReferentialSpeciesPath().toFile();
        this.referentialTemporarySpeciesFileResult = new GenericFormatReferentialSpeciesImportResult(file.getName(), file.exists());
        this.protocolFileResult = createFileResult(archive.getProtocolPath(), false);

        this.surveyFileResult = createFileResult(archive.getSurveyPath(), true);
        this.gearCaracteristicFileResult = createFileResult(archive.getGearCaracteristicsPath(), true);
        this.operationFileResult = createFileResult(archive.getOperationPath(), true);
        this.parameterFileResult = createFileResult(archive.getParameterPath(), true);
        this.catchFileResult = createFileResult(archive.getCatchPath(), true);
        this.marineLitterFileResult = createFileResult(archive.getMarineLitterPath(), true);
        this.individualObservationFileResult = createFileResult(archive.getIndividualObservationPath(), true);
        this.accidentalCatchFileResult = createFileResult(archive.getAccidentalCatchPath(), true);
        this.attachmentFileResult = createFileResult(archive.getAttachmentFilePath(), true);

        this.attachmentRows = ArrayListMultimap.create();
    }

    @Override
    public final void close() {

        try {

            onClose();

        } finally {

            validationHelper.close();

            for (GenericFormatImportCruiseContext cruiseContext : cruiseContexts.values()) {
                cruiseContext.close();
            }
            cruiseContexts.clear();

        }

    }

    @Override
    public Iterator<GenericFormatImportCruiseContext> iterator() {
        return ImmutableSet.copyOf(cruiseContexts.values()).iterator();
    }

    public boolean isArchiveLayoutValid() {
        return CollectionUtils.isEmpty(archiveLayoutErrors);
    }

    public void setArchiveLayoutErrors(Set<String> archiveLayoutErrors) {
        this.archiveLayoutErrors = archiveLayoutErrors;
    }

    public Set<String> getArchiveLayoutErrors() {
        return ImmutableSet.copyOf(archiveLayoutErrors);
    }

    public GenericFormatCsvFileResult getSampleCategoryFileResult() {
        return sampleCategoryFileResult;
    }

    public GenericFormatReferentialImportResult<Gear, Integer> getReferentialTemporaryGearFileResult() {
        return referentialTemporaryGearFileResult;
    }

    public GenericFormatReferentialImportResult<Person, Integer> getReferentialTemporaryPersonFileResult() {
        return referentialTemporaryPersonFileResult;
    }

    public GenericFormatReferentialSpeciesImportResult getReferentialTemporarySpeciesFileResult() {
        return referentialTemporarySpeciesFileResult;
    }

    public GenericFormatReferentialImportResult<Vessel, String> getReferentialTemporaryVesselFileResult() {
        return referentialTemporaryVesselFileResult;
    }

    public GenericFormatFileResult getProtocolFileResult() {
        return protocolFileResult;
    }

    public GenericFormatCsvFileResult getSurveyFileResult() {
        return surveyFileResult;
    }

    public GenericFormatCsvFileResult getGearCaracteristicFileResult() {
        return gearCaracteristicFileResult;
    }

    public GenericFormatCsvFileResult getOperationFileResult() {
        return operationFileResult;
    }

    public GenericFormatCsvFileResult getParameterFileResult() {
        return parameterFileResult;
    }

    public GenericFormatCsvFileResult getCatchFileResult() {
        return catchFileResult;
    }

    public GenericFormatCsvFileResult getMarineLitterFileResult() {
        return marineLitterFileResult;
    }

    public GenericFormatCsvFileResult getIndividualObservationFileResult() {
        return individualObservationFileResult;
    }

    public GenericFormatCsvFileResult getAccidentalCatchFileResult() {
        return accidentalCatchFileResult;
    }

    public GenericFormatCsvFileResult getAttachmentFileResult() {
        return attachmentFileResult;
    }

    public GenericFormatImportRequest getImportRequest() {
        return importRequest;
    }

    public TuttiProtocol getImportedProtocol() {
        return importedProtocol;
    }

    public void setImportedProtocol(TuttiProtocol importedProtocol) {
        this.importedProtocol = importedProtocol;
    }

    public SampleCategoryModel getImportedSampleCategoryModel() {
        return importedSampleCategoryModel;
    }

    public void setImportedSampleCategoryModel(SampleCategoryModel importedSampleCategoryModel) {
        this.importedSampleCategoryModel = importedSampleCategoryModel;
    }

    public boolean isCruiseAlreadyImported(Cruise cruise) {

        GenericFormatImportCruiseContext cruiseContext = getCruiseContext(cruise);
        return cruiseContext != null;

    }

    public void addImportedCruise(Cruise cruise, CruiseDataModel selectedCruise, CruiseDataModel existingCruiseData, Set<FishingOperation> existingFishingOperations) {

        String cruiseId = cruise.getId();

        GenericFormatImportCruiseContext cruiseContext = new GenericFormatImportCruiseContext(cruise, selectedCruise, existingCruiseData, existingFishingOperations, cruiseDecorator.toString(cruise));
        cruiseContexts.put(cruiseId, cruiseContext);

    }

    public void addSkippedCruise(Cruise cruise) {

        String naturalId = Cruises.getNaturalId(cruise);
        skippedCruisesNaturalId.add(naturalId);

    }

    public void addImportedFishingOperation(FishingOperation fishingOperation, CatchBatch catchBatch) {

        GenericFormatImportCruiseContext cruiseContext = getCruiseContext(fishingOperation.getCruise());
        String fishingOperationLabel = decorate(fishingOperation);
        cruiseContext.addFishingOperation(fishingOperation, catchBatch, fishingOperationLabel);

    }

    public boolean isCruiseSkipped(RowWithCruiseContextSupport row) {

        Cruise cruise = row.getCruise();
        String naturalId = Cruises.getNaturalId(cruise);
        return skippedCruisesNaturalId.contains(naturalId);

    }

    public GenericFormatImportCruiseContext getCruiseContext(RowWithCruiseContextSupport row) {

        Cruise cruise = row.getCruise();

        GenericFormatImportCruiseContext result = null;
        for (GenericFormatImportCruiseContext cruiseContext : cruiseContexts.values()) {

            boolean equals = Cruises.equals(cruise, cruiseContext.getCruise());
            if (equals) {
                result = cruiseContext;
                break;
            }

        }
        return result;

    }

    public GenericFormatImportCruiseContext getCruiseContext(Cruise cruise) {

        return cruiseContexts.get(cruise.getId());

    }

    public CsvConsumerForSampleCategory loadSampleCategories(boolean failFast) {
        sampleCategoryFileResult.setImported(true);
        surveyFileResult.setLinesCount(importRequest.getArchive().getSampleCategoryLineCount());
        return new CsvConsumerForSampleCategory(importRequest.getArchive().getSampleCategoryModelPath(), importRequest.getCsvSeparator(), importEntityParserFactory, failFast);
    }

    public CsvConsumerForSurvey loadSurveys(boolean failFast) {
        surveyFileResult.setImported(true);
        surveyFileResult.setLinesCount(importRequest.getArchive().getSurveyLineCount());
        return new CsvConsumerForSurvey(importRequest.getArchive().getSurveyPath(), importRequest.getCsvSeparator(), importEntityParserFactory, failFast);
    }

    public CsvConsumerForGearCaracteristic loadGearCaracteristics(boolean failFast) {
        gearCaracteristicFileResult.setImported(true);
        gearCaracteristicFileResult.setLinesCount(importRequest.getArchive().getGearCaracteristicsPathLineCount());
        return new CsvConsumerForGearCaracteristic(importRequest.getArchive().getGearCaracteristicsPath(), importRequest.getCsvSeparator(), importEntityParserFactory, failFast);
    }

    public CsvConsumerForOperation loadOperations(boolean failFast) {
        operationFileResult.setImported(true);
        operationFileResult.setLinesCount(importRequest.getArchive().getOperationPathLineCount());
        return new CsvConsumerForOperation(importRequest.getArchive().getOperationPath(), importRequest.getCsvSeparator(), importEntityParserFactory, failFast);
    }

    public CsvConsumerForParameter loadParameters(boolean failFast) {
        parameterFileResult.setImported(true);
        parameterFileResult.setLinesCount(importRequest.getArchive().getParameterPathLineCount());
        return new CsvConsumerForParameter(importRequest.getArchive().getParameterPath(), importRequest.getCsvSeparator(), importEntityParserFactory, failFast);
    }

    public CsvConsumerForCatch loadCatches(boolean failFast) {
        catchFileResult.setImported(true);
        catchFileResult.setLinesCount(importRequest.getArchive().getCatchPathLineCount());
        return new CsvConsumerForCatch(importRequest.getArchive().getCatchPath(), importRequest.getCsvSeparator(), getImportedSampleCategoryModel(), importEntityParserFactory, failFast);
    }

    public CsvConsumerForMarineLitter loadMarineLitters(boolean failFast) {
        marineLitterFileResult.setImported(true);
        marineLitterFileResult.setLinesCount(importRequest.getArchive().getMarineLitterPathLineCount());
        return new CsvConsumerForMarineLitter(importRequest.getArchive().getMarineLitterPath(), importRequest.getCsvSeparator(), importEntityParserFactory, failFast);
    }

    public CsvConsumerForIndividualObservation loadIndividualObservations(boolean failFast) {
        individualObservationFileResult.setImported(true);
        individualObservationFileResult.setLinesCount(importRequest.getArchive().getIndividualObservationPathLineCount());
        return new CsvConsumerForIndividualObservation(importRequest.getArchive().getIndividualObservationPath(), importRequest.getCsvSeparator(), importEntityParserFactory, failFast);
    }

    public CsvConsumerForAccidentalCatch loadAccidentalCatches(boolean failFast) {
        accidentalCatchFileResult.setImported(true);
        accidentalCatchFileResult.setLinesCount(importRequest.getArchive().getAccidentalCatchPathLineCount());
        return new CsvConsumerForAccidentalCatch(importRequest.getArchive().getAccidentalCatchPath(), importRequest.getCsvSeparator(), importEntityParserFactory, failFast);
    }

    public CsvConsumerForAttachment loadAttachments(boolean failFast) {
        attachmentFileResult.setImported(true);
        attachmentFileResult.setLinesCount(importRequest.getArchive().getAttachemntsPathLineCount());
        return new CsvConsumerForAttachment(importRequest.getArchive().getAttachmentFilePath(), importRequest.getCsvSeparator(), importEntityParserFactory, failFast);
    }

    public CsvConsumerForTemporaryGear loadTemporaryGears(boolean failFast) {
        referentialTemporaryGearFileResult.setImported(true);
        referentialTemporaryGearFileResult.setLinesCount(importRequest.getArchive().getTemporaryReferentialGearLineCount());
        return new CsvConsumerForTemporaryGear(importRequest.getArchive().getTemporaryReferentialGearsPath(), importRequest.getCsvSeparator(), failFast);
    }

    public CsvConsumerForTemporaryPerson loadTemporaryPersons(boolean failFast) {
        referentialTemporaryPersonFileResult.setImported(true);
        referentialTemporaryPersonFileResult.setLinesCount(importRequest.getArchive().getTemporaryReferentialPersonLineCount());
        return new CsvConsumerForTemporaryPerson(importRequest.getArchive().getTemporaryReferentialPersonsPath(), importRequest.getCsvSeparator(), failFast);
    }

    public CsvConsumerForTemporarySpecies loadTemporarySpecies(boolean failFast) {
        referentialTemporarySpeciesFileResult.setImported(true);
        referentialTemporarySpeciesFileResult.setLinesCount(importRequest.getArchive().getTemporaryReferentialSpeciesLineCount());
        return new CsvConsumerForTemporarySpecies(importRequest.getArchive().getTemporaryReferentialSpeciesPath(), importRequest.getCsvSeparator(), true, failFast);
    }

    public CsvConsumerForTemporaryVessel loadTemporaryVessels(boolean failFast) {
        referentialTemporaryVesselFileResult.setImported(true);
        referentialTemporaryVesselFileResult.setLinesCount(importRequest.getArchive().getTemporaryReferentialVesselLineCount());
        return new CsvConsumerForTemporaryVessel(importRequest.getArchive().getTemporaryReferentialVesselsPath(), importRequest.getCsvSeparator(), failFast);
    }

    public Serializable parseCaracteristicValue(Caracteristic caracteristic, String value) throws CaracteristicValueParseException {

        CaracteristicValueParserFormatter parser = CaracteristicValueParserFormatter.newParser(caracteristic);
        try {

            return parser.parse(value);

        } catch (ParseException e) {
            throw new CaracteristicValueParseException(caracteristic, value, e.getMessage());
        }

    }

    public void increments(String message) {
        progressionModel.increments(message);
    }

    public GenericFormatImportValidationHelper getValidationHelper() {
        return validationHelper;
    }

    public void doActionOnCruiseContexts(CruiseContextAction action) {

        for (GenericFormatImportCruiseContext cruiseContext : cruiseContexts.values()) {

            action.onCruise(cruiseContext, progressionModel);

        }
    }

    public Iterable<GenericFormatImportCruiseContext> orderedCruiseContexts() {

        return Ordering.from(Cruises.CRUISE_COMPARATOR).onResultOf(new Function<GenericFormatImportCruiseContext, Cruise>() {

            @Override
            public Cruise apply(GenericFormatImportCruiseContext input) {
                return input.getCruise();
            }
        }).sortedCopy(cruiseContexts.values());

    }

    public void doActionOnSortedCruiseContexts(CruiseContextAction action) {

        for (GenericFormatImportCruiseContext cruiseContext : orderedCruiseContexts()) {

            action.onCruise(cruiseContext, progressionModel);

        }
    }


    public boolean isTechnicalFilesValid() {
        return isArchiveLayoutValid()
               && sampleCategoryFileResult.isValid()
               && referentialTemporaryGearFileResult.isValid()
               && referentialTemporaryPersonFileResult.isValid()
               && referentialTemporarySpeciesFileResult.isValid()
               && referentialTemporaryVesselFileResult.isValid()
               && protocolFileResult.isValid();
    }

    public String getProtocolOriginalName() {
        return protocolOriginalName;
    }

    public void setProtocolOriginalName(String protocolOriginalName) {
        this.protocolOriginalName = protocolOriginalName;
    }

    public void setAttachmentRows(Multimap<String, AttachmentRow> attachmentRows) {
        this.attachmentRows.putAll(attachmentRows);
    }

    public Collection<AttachmentRow> popAttachmentRows(ObjectTypeCode objectTypeCode, Integer objectId) {
        return objectId == null ? null : attachmentRows.removeAll(objectTypeCode + "_" + objectId);
    }

    public interface CruiseContextAction {

        void onCruise(GenericFormatImportCruiseContext cruiseContext, ProgressionModel progressionModel);

    }

    protected GenericFormatCsvFileResult createFileResult(Path path, boolean mandatory) {

        File file = path.toFile();
        return new GenericFormatCsvFileResult(file.getName(), mandatory, file.exists());

    }

    protected <E extends TuttiReferentialEntity, K extends Comparable<K>> GenericFormatReferentialImportResult<E, K> createReferentialFileResult(Path path) {

        File file = path.toFile();
        return new GenericFormatReferentialImportResult<>(file.getName(), file.exists());

    }

    public String decorate(Cruise cruise) {
        return cruiseDecorator.toString(cruise);
    }

    public String decorate(FishingOperation fishingOperation) {
        return fishingOperationDecorator.toString(fishingOperation);
    }

}

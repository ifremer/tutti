package fr.ifremer.tutti.service.csv;

/*
 * #%L
 * Tutti :: Service
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.apache.commons.lang3.StringUtils;
import org.nuiton.csv.ValueParserFormatter;

import java.text.ParseException;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Created on 2/5/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 4.5.1
 */
public class StringSetParserFormatter implements ValueParserFormatter<Set<String>> {

    @Override
    public String format(Set<String> value) {
        return value.stream().collect(Collectors.joining("|"));
    }

    @Override
    public Set<String> parse(String value) throws ParseException {
        Set<String> result = new LinkedHashSet<>();
        if (StringUtils.isNotBlank(value)) {
            String[] split = value.split("\\s*\\|\\s*");
            Collections.addAll(result, split);
        }
        return result;
    }
}

package fr.ifremer.tutti.service.protocol;

/*
 * #%L
 * Tutti :: Service
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2016 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.entities.referential.Species;
import fr.ifremer.tutti.service.csv.AbstractTuttiImportExportModel;
import fr.ifremer.tutti.service.csv.TuttiCsvUtil;
import org.nuiton.csv.ValueFormatter;

import java.util.Map;

/**
 * @author Kevin Morin (Code Lutin)
 * @since 4.5
 */
public class CalcifiedPiecesSamplingRowModel extends AbstractTuttiImportExportModel<CalcifiedPiecesSamplingRow> {

    public CalcifiedPiecesSamplingRowModel(char separator) {
        super(separator);
    }

    public static CalcifiedPiecesSamplingRowModel forExport(char separator) {

        CalcifiedPiecesSamplingRowModel result = new CalcifiedPiecesSamplingRowModel(separator);

        result.newColumnForExport(CalcifiedPiecesSamplingRow.PROPERTY_REFTAX,
                                  CalcifiedPiecesSamplingRow.PROPERTY_SPECIES,
                                  new ValueFormatter<Species>() {

                                      @Override
                                      public String format(Species species) {
                                          return String.valueOf(species.getReferenceTaxonId());
                                      }
                                  });

        result.newColumnForExport(CalcifiedPiecesSamplingRow.PROPERTY_SURVEY_CODE,
                                  CalcifiedPiecesSamplingRow.PROPERTY_SPECIES,
                                  new ValueFormatter<Species>() {

                                      @Override
                                      public String format(Species species) {
                                          return String.valueOf(species.getSurveyCode());
                                      }
                                  });

        result.newColumnForExport(CalcifiedPiecesSamplingRow.PROPERTY_SCIENTIFIC_NAME,
                                  CalcifiedPiecesSamplingRow.PROPERTY_SPECIES,
                                  new ValueFormatter<Species>() {

                                      @Override
                                      public String format(Species species) {
                                          return String.valueOf(species.getName());
                                      }
                                  });

        result.newColumnForExport(CalcifiedPiecesSamplingRow.PROPERTY_MATURITY, TuttiCsvUtil.BOOLEAN);
        result.newColumnForExport(CalcifiedPiecesSamplingRow.PROPERTY_SEX, TuttiCsvUtil.PRIMITIVE_BOOLEAN);
        result.newColumnForExport(CalcifiedPiecesSamplingRow.PROPERTY_MIN_SIZE, TuttiCsvUtil.PRIMITIVE_INTEGER);
        result.newColumnForExport(CalcifiedPiecesSamplingRow.PROPERTY_MAX_SIZE, TuttiCsvUtil.INTEGER);
        result.newColumnForExport(CalcifiedPiecesSamplingRow.PROPERTY_MAX_BY_LENGHT_STEP, TuttiCsvUtil.INTEGER);
        result.newColumnForExport(CalcifiedPiecesSamplingRow.PROPERTY_SAMPLING_INTERVAL, TuttiCsvUtil.PRIMITIVE_INTEGER);
        result.newColumnForExport(CalcifiedPiecesSamplingRow.PROPERTY_OPERATION_LIMITATION, TuttiCsvUtil.INTEGER);
        result.newColumnForExport(CalcifiedPiecesSamplingRow.PROPERTY_ZONE_LIMITATION, TuttiCsvUtil.INTEGER);

        return result;
    }

    public static CalcifiedPiecesSamplingRowModel forImport(char separator,
                                                            Map<String, Species> allSpecies) {

        CalcifiedPiecesSamplingRowModel result = new CalcifiedPiecesSamplingRowModel(separator);

        result.newForeignKeyColumn(CalcifiedPiecesSamplingRow.PROPERTY_REFTAX,
                                   CalcifiedPiecesSamplingRow.PROPERTY_SPECIES,
                                   Species.class,
                                   Species.PROPERTY_REFERENCE_TAXON_ID,
                                   allSpecies);
        result.newMandatoryColumn(CalcifiedPiecesSamplingRow.PROPERTY_MATURITY, TuttiCsvUtil.BOOLEAN);
        result.newMandatoryColumn(CalcifiedPiecesSamplingRow.PROPERTY_SEX, TuttiCsvUtil.PRIMITIVE_BOOLEAN);
        result.newMandatoryColumn(CalcifiedPiecesSamplingRow.PROPERTY_MIN_SIZE, TuttiCsvUtil.PRIMITIVE_INTEGER);
        result.newMandatoryColumn(CalcifiedPiecesSamplingRow.PROPERTY_MAX_SIZE, TuttiCsvUtil.INTEGER);
        result.newMandatoryColumn(CalcifiedPiecesSamplingRow.PROPERTY_MAX_BY_LENGHT_STEP, TuttiCsvUtil.INTEGER);
        result.newMandatoryColumn(CalcifiedPiecesSamplingRow.PROPERTY_SAMPLING_INTERVAL, TuttiCsvUtil.PRIMITIVE_INTEGER);
        result.newMandatoryColumn(CalcifiedPiecesSamplingRow.PROPERTY_OPERATION_LIMITATION, TuttiCsvUtil.INTEGER);
        result.newMandatoryColumn(CalcifiedPiecesSamplingRow.PROPERTY_ZONE_LIMITATION, TuttiCsvUtil.INTEGER);

        result.newIgnoredColumn(CalcifiedPiecesSamplingRow.PROPERTY_SURVEY_CODE);
        result.newIgnoredColumn(CalcifiedPiecesSamplingRow.PROPERTY_SCIENTIFIC_NAME);

        return result;
    }

    @Override
    public CalcifiedPiecesSamplingRow newEmptyInstance() {
        return new CalcifiedPiecesSamplingRow();
    }
}

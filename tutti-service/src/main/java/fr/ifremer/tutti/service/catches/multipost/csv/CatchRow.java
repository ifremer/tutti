package fr.ifremer.tutti.service.catches.multipost.csv;

/*
 * #%L
 * Tutti :: Service
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.entities.referential.Species;

import java.io.Serializable;

/**
 * A row in a catch export.
 *
 * @author Kevin Morin - kmorin@codelutin.com
 * @since 2.2
 */
public class CatchRow implements Serializable {

    public static final String ID = "id";
    public static final String PARENT_ID = "parentId";
    public static final String SPECIES = "species";
    public static final String CATEGORY_ID = "categoryId";
    public static final String CATEGORY_VALUE = "categoryValue";
    public static final String CATEGORY_WEIGHT = "categoryWeight";
    public static final String WEIGHT = "weight";
    public static final String NUMBER = "number";
    public static final String COMMENT = "comment";
    public static final String TO_CONFIRM = "toConfirm";
    private static final long serialVersionUID = 1L;
    protected String id;

    protected String parentId;

    protected Species species;

    protected Integer categoryId;

    protected Serializable categoryValue;

    protected Float categoryWeight;

    protected Float weight;

    protected Integer number;

    protected String comment;

    protected boolean toConfirm;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public Species getSpecies() {
        return species;
    }

    public void setSpecies(Species species) {
        this.species = species;
    }

    public Integer getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }

    public Serializable getCategoryValue() {
        return categoryValue;
    }

    public void setCategoryValue(Serializable categoryValue) {
        this.categoryValue = categoryValue;
    }

    public Float getCategoryWeight() {
        return categoryWeight;
    }

    public void setCategoryWeight(Float categoryWeight) {
        this.categoryWeight = categoryWeight;
    }

    public Float getWeight() {
        return weight;
    }

    public void setWeight(Float weight) {
        this.weight = weight;
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public boolean isToConfirm() {
        return toConfirm;
    }

    public void setToConfirm(boolean toConfirm) {
        this.toConfirm = toConfirm;
    }
}

package fr.ifremer.tutti.service.genericformat;

/*
 * #%L
 * Tutti :: Service
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import fr.ifremer.tutti.persistence.ProgressionModel;
import fr.ifremer.tutti.persistence.entities.data.BatchContainer;
import fr.ifremer.tutti.persistence.entities.data.CatchBatch;
import fr.ifremer.tutti.persistence.entities.data.Cruise;
import fr.ifremer.tutti.persistence.entities.data.FishingOperation;
import fr.ifremer.tutti.persistence.entities.data.MarineLitterBatch;
import fr.ifremer.tutti.persistence.entities.data.SpeciesBatch;
import fr.ifremer.tutti.service.AbstractTuttiService;
import fr.ifremer.tutti.service.DecoratorService;
import fr.ifremer.tutti.service.PersistenceService;
import fr.ifremer.tutti.service.TuttiServiceContext;
import fr.ifremer.tutti.service.catches.WeightComputingService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.decorator.Decorator;
import org.nuiton.jaxx.application.ApplicationBusinessException;

import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

import static org.nuiton.i18n.I18n.t;

/**
 * Created on 3/28/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.14.3
 */
public class GenericFormatCheckDataService extends AbstractTuttiService {

    /** Logger. */
    private static final Log log = LogFactory.getLog(GenericFormatCheckDataService.class);

    protected PersistenceService persistenceService;

    protected WeightComputingService weightComputingService;

    protected Decorator<FishingOperation> fishingOperationDecorator;

    protected Decorator<Cruise> cruiseDecorator;

    @Override
    public void setServiceContext(TuttiServiceContext context) {

        super.setServiceContext(context);
        persistenceService = getService(PersistenceService.class);
        weightComputingService = getService(WeightComputingService.class);

        DecoratorService decoratorService = getService(DecoratorService.class);
        cruiseDecorator = decoratorService.getDecoratorByType(Cruise.class);
        fishingOperationDecorator = decoratorService.getDecoratorByType(FishingOperation.class);

    }

    public String getCruiseErrors(Cruise cruise, Set<FishingOperation> operations, ProgressionModel progressionModel) {

        Map<Integer, String> errors = getFishingOperationsErrors(cruise, operations, progressionModel);

        String result;

        if (!errors.isEmpty()) {

            StringBuilder errorMessageBuilder = new StringBuilder();

            for (String error : errors.values()) {
                errorMessageBuilder.append("<li>").append(error).append("</li>");
            }

            String cruiseStr = cruiseDecorator.toString(cruise);
            result = t("tutti.service.genericFormat.invalid.cruise", cruiseStr, errorMessageBuilder.toString());

        } else {

            result = null;

        }

        return result;

    }

    protected Map<Integer, String> getFishingOperationsErrors(Cruise cruise,
                                                              Set<FishingOperation> operations,
                                                              ProgressionModel progressionModel) {

        Preconditions.checkNotNull(cruise);
        Preconditions.checkNotNull(progressionModel);

        progressionModel.increments(t("tutti.service.genericFormat.checkCruise", cruise.getName()));

        Map<Integer, String> errors = new LinkedHashMap<>();

        for (FishingOperation fishingOperation : operations) {

            Integer fishingOperationId = fishingOperation.getIdAsInt();

            progressionModel.increments(t("tutti.service.genericFormat.checkCruiseFishingOperation", cruise.getName(), fishingOperationDecorator.toString(fishingOperation)));

            Set<String> errorsForOperation = checkFishingOperation(fishingOperationId);
            if (!errorsForOperation.isEmpty()) {

                String fishingOperationStr = fishingOperationDecorator.toString(fishingOperation);

                StringBuilder sb = new StringBuilder();
                for (String error : errorsForOperation) {
                    sb.append("<li>").append(error).append("</li>");
                }
                String errorMessage = t("tutti.service.genericFormat.invalid.fishingOperation", fishingOperationStr, sb.toString());
                errors.put(fishingOperationId, errorMessage);

            }

        }

        return errors;

    }

    public Set<String> checkFishingOperation(Integer fishingOperationId) {

        if (log.isDebugEnabled()) {
            log.debug("Will check operation: " + fishingOperationId);
        }

        Set<String> errors = new LinkedHashSet<>();

        boolean withCatchBatch = persistenceService.isFishingOperationWithCatchBatch(fishingOperationId);

        if (!withCatchBatch) {

            if (log.isWarnEnabled()) {
                log.warn("Skip fishing operation " + fishingOperationId + " since no catchBatch associated.");
            }

        } else {

            BatchContainer<SpeciesBatch> rootSpeciesBatch;
            try {
                rootSpeciesBatch = weightComputingService.getComputedSpeciesBatches(fishingOperationId);

            } catch (ApplicationBusinessException e) {
                errors.add(e.getMessage());
                rootSpeciesBatch = persistenceService.getRootSpeciesBatch(fishingOperationId, false);
            }

            BatchContainer<SpeciesBatch> rootBenthosBatch;
            try {
                rootBenthosBatch = weightComputingService.getComputedBenthosBatches(fishingOperationId);

            } catch (ApplicationBusinessException e) {
                errors.add(e.getMessage());
                rootBenthosBatch = persistenceService.getRootBenthosBatch(fishingOperationId, false);
            }

            CatchBatch catchBatch = persistenceService.getCatchBatchFromFishingOperation(fishingOperationId);

            BatchContainer<MarineLitterBatch> rootMarineLitterBatch;
            try {
                Float weight = catchBatch == null ? null : catchBatch.getMarineLitterTotalWeight();
                rootMarineLitterBatch = weightComputingService.getComputedMarineLitterBatches(fishingOperationId, weight);

            } catch (ApplicationBusinessException e) {
                errors.add(e.getMessage());
                rootMarineLitterBatch = persistenceService.getRootMarineLitterBatch(fishingOperationId);
            }

            try {
                if (catchBatch != null) {
                    weightComputingService.computeCatchBatchWeights(catchBatch,
                                                                    rootSpeciesBatch,
                                                                    rootBenthosBatch,
                                                                    rootMarineLitterBatch);
                }
            } catch (ApplicationBusinessException e) {
                errors.add(e.getMessage());
            }
        }

        return errors;

    }

}

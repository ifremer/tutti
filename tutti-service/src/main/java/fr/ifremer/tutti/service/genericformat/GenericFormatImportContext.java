package fr.ifremer.tutti.service.genericformat;

/*
 * #%L
 * Tutti :: Service
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.ProgressionModel;
import fr.ifremer.tutti.persistence.entities.data.Cruise;
import fr.ifremer.tutti.persistence.entities.data.FishingOperation;
import fr.ifremer.tutti.service.PersistenceService;
import fr.ifremer.tutti.service.genericformat.importactions.RestoreAfterImportAction;
import org.nuiton.decorator.Decorator;

/**
 * Created on 2/5/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.13
 */
public class GenericFormatImportContext extends GenericFormatContextSupport {

    private final RestoreAfterImportAction closeAction;

    public GenericFormatImportContext(GenericFormatImportRequest importRequest,
                                      ProgressionModel progressionModel,
                                      PersistenceService persistenceService,
                                      Decorator<Cruise> cruiseDecorator,
                                      Decorator<FishingOperation> fishingOperationDecorator) {

        super(importRequest, progressionModel, persistenceService, cruiseDecorator, fishingOperationDecorator);
        this.closeAction = new RestoreAfterImportAction(this, persistenceService);

    }

    @Override
    protected void onClose() {

        closeAction.execute();

    }


}

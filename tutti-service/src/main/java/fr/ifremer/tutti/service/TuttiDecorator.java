package fr.ifremer.tutti.service;

/*
 * #%L
 * Tutti :: Service
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.google.common.collect.Maps;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.decorator.DecoratorUtil;
import org.nuiton.decorator.JXPathDecorator;
import org.nuiton.decorator.MultiJXPathDecorator;
import org.nuiton.util.beans.BeanUtil;

import java.beans.PropertyDescriptor;
import java.io.Serializable;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutionException;

/**
 * TODO
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since TODO
 */
public class TuttiDecorator<O> extends MultiJXPathDecorator<O> implements Cloneable {

    private static final long serialVersionUID = 1L;

    /** Logger. */
    private static final Log log = LogFactory.getLog(TuttiDecorator.class);

    protected final Map<String, Method> tokenMethods;

    protected final LoadingCache<O, String> cache;

    protected boolean useCache;

    /**
     * To sort always on the selected context.
     *
     * @since 2.8
     */
    protected boolean sortOnlyOnSelectedContext;

    /**
     * List of tokens to sort using the {@link #sortOnlyOnSelectedContext} mode.
     *
     * If the mode is not set, then won't use this.
     *
     * @since 2.8
     */
    protected Set<String> sortOnlyOnSelectedContextTokens;

    public static <O> TuttiDecorator<O> newDecorator(Class<O> internalClass,
                                                     String expression,
                                                     String separator,
                                                     String separatorReplacement) {
        return new TuttiDecorator<>(internalClass, expression, separator, separatorReplacement);
    }

    protected TuttiDecorator(Class<O> internalClass,
                             String expression,
                             String separator,
                             String separatorReplacement) throws IllegalArgumentException, NullPointerException {
        super(internalClass,
              expression,
              separator,
              separatorReplacement,
              DecoratorUtil.<O>createMultiJXPathContextKeepingOrder(expression,
                                                                    separator,
                                                                    separatorReplacement));
        tokenMethods = Maps.newHashMap();
        Set<PropertyDescriptor> descriptors =
                BeanUtil.getDescriptors(type, BeanUtil.IS_READ_DESCRIPTOR);

        for (String token : getTokens()) {
            Method m = null;
            for (PropertyDescriptor propertyDescriptor : descriptors) {
                if (propertyDescriptor.getName().equals(token)) {
                    m = propertyDescriptor.getReadMethod();
                    break;
                }
            }
            if (m == null) {
                throw new IllegalArgumentException(
                        "could not find the property " + token + " in " + type);
            }
            tokenMethods.put(token, m);
        }

        int i = 0;
        for (Context<O> OContext : contexts) {
            OContext.setComparator(new TuttiDecoratorComparator<>(getProperty(i++)));
        }

        this.cache = CacheBuilder.newBuilder().build(new CacheLoader<O, String>() {
            @Override
            public String load(O key) throws Exception {
                String result;
                if (key == null) {
                    result = "";
                } else {
                    result = TuttiDecorator.this.toString(key);
                }
                return result;
            }
        });
    }

    public boolean isUseCache() {
        return useCache;
    }

    public void setUseCache(boolean useCache) {
        this.useCache = useCache;
        cache.invalidateAll();
    }

    public boolean isSortOnlyOnSelectedContext() {
        return sortOnlyOnSelectedContext;
    }

    public void setSortOnlyOnSelectedContext(boolean sortOnlyOnSelectedContext) {
        this.sortOnlyOnSelectedContext = sortOnlyOnSelectedContext;
    }

    public Set<String> getSortOnlyOnSelectedContextTokens() {
        return sortOnlyOnSelectedContextTokens;
    }

    public void setSortOnlyOnSelectedContextTokens(Set<String> sortOnlyOnSelectedContextTokens) {
        this.sortOnlyOnSelectedContextTokens = sortOnlyOnSelectedContextTokens;
    }

    public TuttiDecoratorComparator<O> getCurrentComparator() {
        return (TuttiDecoratorComparator<O>) context.getComparator(0);
    }

    @Override
    public String toString(Object bean) {
        O bean1 = (O) bean;
        String result = null;

        if (useCache) {

            // try first in cache
            try {
                result = cache.get(bean1);
            } catch (ExecutionException e) {
                if (log.isErrorEnabled()) {
                    log.error("Could not obtain from cache", e);
                }
            }
        }
        if (result == null) {

            if (bean != null) {
                Object[] args = new Object[nbToken];

                String[] tokens = getTokens();
                for (int i = 0; i < nbToken; i++) {
                    String token = tokens[i];
                    Object value = getValue(bean1, token);
                    if (value == null) {
                        value = onNullValue(bean1, token);
                    }
                    args[i] = value;
                }

                try {
                    result = String.format(getExpression(), args);
                } catch (Exception eee) {
                    if (log.isErrorEnabled()) {
                        log.error("Could not format " + getExpression() + "" +
                                  " with args : " + Arrays.toString(args), eee);
                    }
                    result = "";
                }

                if (useCache) {
                    cache.put(bean1, result);
                }
            }
        }

        return result;
    }

    protected Object getValue(O bean, String token) {
        Method method = tokenMethods.get(token);
        Preconditions.checkNotNull(method,
                                   "Could not find method for token " + token);
        Object result;
        try {
            result = method.invoke(bean);
        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Could not obtain token [" + token + "] value", e);
            }
            result = "";
        }
        return result;
    }

    protected Object onNullValue(O bean, String token) {
        return null;
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    public static class TuttiDecoratorComparator<O> extends JXPathComparator<O> implements Serializable, Cloneable {

        private static final long serialVersionUID = 1L;

        protected String expression;

        public TuttiDecoratorComparator(String expression) {
            super(expression);
            this.expression = expression;
        }

        @Override
        public void init(JXPathDecorator<O> decorator, List<O> datas) {
            clear();
            TuttiDecorator<O> tuttiDecorator = (TuttiDecorator<O>) decorator;
            String token = decorator.getTokens()[0];
            boolean sortOnlyOnSelectedContext =
                    tuttiDecorator.isSortOnlyOnSelectedContext() &&
                    tuttiDecorator.getSortOnlyOnSelectedContextTokens() != null &&
                    tuttiDecorator.getSortOnlyOnSelectedContextTokens().contains(token);

            for (O data : datas) {
                if (sortOnlyOnSelectedContext) {
                    Object tokenValue = tuttiDecorator.getValue(data, token);

                    valueCache.put(data, (Comparable) tokenValue);
                } else if (data instanceof Number) {
                    valueCache.put(data, (Comparable) data);
                } else {
                    Comparable key = tuttiDecorator.toString(data);
                    valueCache.put(data, key);
                }
            }
        }

        @Override
        public TuttiDecoratorComparator<O> clone() {
            return new TuttiDecoratorComparator<>(expression);
        }

        public String getExpression() {
            return expression;
        }
    }
}

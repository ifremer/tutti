package fr.ifremer.tutti.service.genericformat.consumer;

/*
 * #%L
 * Tutti :: Service
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.entities.referential.Caracteristic;
import fr.ifremer.tutti.persistence.entities.referential.Gear;
import fr.ifremer.tutti.service.csv.CaracteristicValueParseException;
import fr.ifremer.tutti.service.csv.CsvComsumer;
import fr.ifremer.tutti.service.genericformat.GenericFormatContextSupport;
import fr.ifremer.tutti.service.genericformat.GenericFormatImportCruiseContext;
import fr.ifremer.tutti.service.genericformat.GenericFormatImportEntityParserFactory;
import fr.ifremer.tutti.service.genericformat.GenericFormatImportValidationHelper;
import fr.ifremer.tutti.service.genericformat.csv.GearCaracteristicModel;
import fr.ifremer.tutti.service.genericformat.csv.GearCaracteristicRow;
import org.nuiton.csv.ImportRow;

import java.io.Serializable;
import java.nio.file.Path;

/**
 * Created on 2/11/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.14
 */
public class CsvConsumerForGearCaracteristic extends CsvComsumer<GearCaracteristicRow, GearCaracteristicModel> {

    public CsvConsumerForGearCaracteristic(Path file, char separator, GenericFormatImportEntityParserFactory parserFactory, boolean reportError) {
        super(file, GearCaracteristicModel.forImport(separator, parserFactory), reportError);
    }

    public GenericFormatImportCruiseContext validateRow(ImportRow<GearCaracteristicRow> row, GenericFormatContextSupport importContext) {


        GenericFormatImportValidationHelper validationHelper = importContext.getValidationHelper();

        GenericFormatImportCruiseContext cruiseContext = validationHelper.getCruise(this, row, importContext);

        if (cruiseContext != null) {

            GearCaracteristicRow bean = row.getBean();

            Gear gear = bean.getGear();
            short rankOrder = bean.getRankOrder();

            Gear cruiseGear = validationHelper.getGear(this, importContext, row, gear, rankOrder);
            if (cruiseGear != null) {
                bean.setGear(cruiseGear);
            }

            //TODO Use a validator ?
            Caracteristic caracteristic = bean.getCaracteristic();
            if (caracteristic == null) {
                //TODO Should done by parser ?
            }

            //TODO Use a validator ?
            if (bean.getValue() == null) {

            }

            // parse caracteristic value
            String value = (String) bean.getValue();
            try {
                Serializable serializable = importContext.parseCaracteristicValue(caracteristic, value);
                bean.setValue(serializable);
            } catch (CaracteristicValueParseException e) {
                addCheckError(row, e);
            }

        }

        reportError(row);

        return cruiseContext;

    }

    public void prepareRowForPersist(GenericFormatImportCruiseContext cruiseContext, ImportRow<GearCaracteristicRow> row) {

        GearCaracteristicRow bean = row.getBean();

        cruiseContext.addGearCaracteristic(bean.getGear(), bean.getCaracteristic(), bean.getValue());

    }

}
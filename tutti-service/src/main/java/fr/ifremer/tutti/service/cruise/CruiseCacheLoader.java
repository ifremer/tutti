package fr.ifremer.tutti.service.cruise;

/*
 * #%L
 * Tutti :: Service
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2016 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.ProgressionModel;
import fr.ifremer.tutti.persistence.entities.data.FishingOperation;
import fr.ifremer.tutti.persistence.entities.data.IndividualObservationBatch;
import fr.ifremer.tutti.persistence.entities.protocol.TuttiProtocol;
import fr.ifremer.tutti.persistence.entities.referential.Caracteristic;
import fr.ifremer.tutti.service.DecoratorService;
import fr.ifremer.tutti.service.PersistenceService;
import fr.ifremer.tutti.service.sampling.CruiseSamplingCache;
import fr.ifremer.tutti.service.sampling.SamplingCodeCache;
import java.util.Collection;
import java.util.List;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.decorator.Decorator;

import static org.nuiton.i18n.I18n.t;

/**
 * Pour charger le cache de données de campagnes.
 *
 * Created on 20/03/16.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 4.5
 */
public class CruiseCacheLoader {

    /** Logger. */
    private static final Log log = LogFactory.getLog(CruiseCacheLoader.class);

    private final PersistenceService persistenceService;
    private final DecoratorService decoratorService;
    private final ProgressionModel progressionModel;
    private final CruiseCache cruiseCache;

    public static CruiseCacheLoader newCacheLoader(PersistenceService persistenceService,
                                                   DecoratorService decoratorService,
                                                   ProgressionModel progressionModel,
                                                   TuttiProtocol protocol,
                                                   Integer cruiseId) {

        boolean loadSamplingCache = protocol != null && protocol.isUseCalcifiedPieceSampling();

        CruiseSamplingCache cruiseSamplingCache;

        if (loadSamplingCache) {

            Caracteristic sexCaracteristic = persistenceService.getSexCaracteristic();

            Collection<Caracteristic> maturityCaracteristics = persistenceService.getMaturityCaracteristics(persistenceService.getAllCaracteristic());
            cruiseSamplingCache = new CruiseSamplingCache(protocol, sexCaracteristic, maturityCaracteristics);

        } else {

            cruiseSamplingCache = null;

        }

        CruiseCache cruiseCache = new CruiseCache(cruiseId, protocol == null ? null : protocol.getId(), cruiseSamplingCache, new SamplingCodeCache());
        return new CruiseCacheLoader(persistenceService, decoratorService, progressionModel, cruiseCache);
    }

    public static CruiseCacheLoader newCacheLoader(PersistenceService persistenceService,
                                                   DecoratorService decoratorService,
                                                   ProgressionModel progressionModel,
                                                   CruiseCache cruiseCache) {
        return new CruiseCacheLoader(persistenceService, decoratorService, progressionModel, cruiseCache);
    }

    public CruiseCache loadCruiseCache() {

        if (log.isInfoEnabled()) {
            log.info("Loading cruise cache: " + cruiseCache);
        }

        // FIXME poussin 20160608 a reecrire pour de meilleur performance, il faut remonter
        // tous les fishingOperation en une fois et non pas faire N requete
        Decorator<FishingOperation> fishingOperationDecorator = decoratorService.getDecoratorByType(FishingOperation.class);

        persistenceService.getAllFishingOperationIds(cruiseCache.getCruiseId()).forEach(fishingOperationId -> {

            FishingOperation fishingOperation = persistenceService.getFishingOperation(fishingOperationId);

            progressionModel.increments(t("tutti.cruise.cacheLoader.loading.fishingOperation", fishingOperationDecorator.toString(fishingOperation)));

            loadCruiseCacheForFishingOperation(fishingOperation);

        });

        if (log.isInfoEnabled()) {
            log.info("Cruise cache loaded: " + cruiseCache);
        }

        return cruiseCache;
    }

    public CruiseCache loadCruiseCacheForFishingOperation(FishingOperation fishingOperation) {

        if (log.isInfoEnabled()) {
            log.info("Loading cruise cache for fishing operation: " + fishingOperation + " → " + cruiseCache);
        }

        List<IndividualObservationBatch> individualObservations =
                persistenceService.getAllIndividualObservationBatchsForFishingOperation(fishingOperation.getIdAsInt());

        cruiseCache.addFishingOperation(fishingOperation, individualObservations);

        if (log.isInfoEnabled()) {
            log.info("Cruise cache loaded for fishing operation: " + fishingOperation + " → " + cruiseCache);
        }

        return cruiseCache;

    }

    private CruiseCacheLoader(PersistenceService persistenceService,
                              DecoratorService decoratorService,
                              ProgressionModel progressionModel,
                              CruiseCache cruiseCache) {
        this.persistenceService = persistenceService;
        this.decoratorService = decoratorService;
        this.progressionModel = progressionModel;
        this.cruiseCache = cruiseCache;
    }

}

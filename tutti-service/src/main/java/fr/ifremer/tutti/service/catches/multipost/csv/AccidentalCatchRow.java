package fr.ifremer.tutti.service.catches.multipost.csv;

/*
 * #%L
 * Tutti :: Service
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.entities.referential.Caracteristic;
import fr.ifremer.tutti.persistence.entities.referential.CaracteristicQualitativeValue;
import fr.ifremer.tutti.persistence.entities.referential.Species;

import java.io.Serializable;

/**
 * @author Kevin Morin - kmorin@codelutin.com
 * @since 2.2
 */
public class AccidentalCatchRow implements Serializable {

    public static final String BATCH_ID = "batchId";
    public static final String SPECIES = "species";
    public static final String GENDER = "gender";
    public static final String WEIGHT = "weight";
    public static final String SIZE = "size";
    public static final String LENGTH_STEP_CARACTERISTIC = "lengthStepCaracteristic";
    public static final String DEAD_OR_ALIVE = "deadOrAlive";
    public static final String COMMENT = "comment";
    private static final long serialVersionUID = 1L;
    protected String batchId;

    protected Species species;

    protected CaracteristicQualitativeValue gender;

    protected Float weight;

    protected Float size;

    protected Caracteristic lengthStepCaracteristic;

    protected CaracteristicQualitativeValue deadOrAlive;

    protected String comment;

    public String getBatchId() {
        return batchId;
    }

    public void setBatchId(String batchId) {
        this.batchId = batchId;
    }

    public Species getSpecies() {
        return species;
    }

    public void setSpecies(Species species) {
        this.species = species;
    }

    public CaracteristicQualitativeValue getGender() {
        return gender;
    }

    public void setGender(CaracteristicQualitativeValue gender) {
        this.gender = gender;
    }

    public Float getWeight() {
        return weight;
    }

    public void setWeight(Float weight) {
        this.weight = weight;
    }

    public Float getSize() {
        return size;
    }

    public void setSize(Float size) {
        this.size = size;
    }

    public Caracteristic getLengthStepCaracteristic() {
        return lengthStepCaracteristic;
    }

    public void setLengthStepCaracteristic(Caracteristic lengthStepCaracteristic) {
        this.lengthStepCaracteristic = lengthStepCaracteristic;
    }

    public CaracteristicQualitativeValue getDeadOrAlive() {
        return deadOrAlive;
    }

    public void setDeadOrAlive(CaracteristicQualitativeValue deadOrAlive) {
        this.deadOrAlive = deadOrAlive;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
}

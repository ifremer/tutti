package fr.ifremer.tutti.service.genericformat;

/*
 * #%L
 * Tutti :: Service
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableSet;
import fr.ifremer.tutti.persistence.ProgressionModel;
import fr.ifremer.tutti.persistence.entities.data.Cruise;
import fr.ifremer.tutti.persistence.entities.data.FishingOperation;
import fr.ifremer.tutti.persistence.entities.data.Program;
import fr.ifremer.tutti.persistence.entities.protocol.TuttiProtocol;
import fr.ifremer.tutti.persistence.model.ProgramDataModel;
import fr.ifremer.tutti.persistence.service.util.SynchronizationStatusHelper;
import fr.ifremer.tutti.service.AbstractTuttiService;
import fr.ifremer.tutti.service.DecoratorService;
import fr.ifremer.tutti.service.PdfGeneratorService;
import fr.ifremer.tutti.service.PersistenceService;
import fr.ifremer.tutti.service.TuttiServiceContext;
import fr.ifremer.tutti.service.catches.WeightCleaningService;
import fr.ifremer.tutti.service.genericformat.importactions.DoCheckWeightAction;
import fr.ifremer.tutti.service.genericformat.importactions.DoCleanWeightAction;
import fr.ifremer.tutti.service.genericformat.importactions.ImportAccidentalCatchAction;
import fr.ifremer.tutti.service.genericformat.importactions.ImportActionSupport;
import fr.ifremer.tutti.service.genericformat.importactions.ImportCatchAction;
import fr.ifremer.tutti.service.genericformat.importactions.ImportGearCaracteristicAction;
import fr.ifremer.tutti.service.genericformat.importactions.ImportIndividualObservationAction;
import fr.ifremer.tutti.service.genericformat.importactions.ImportMarineLitterAction;
import fr.ifremer.tutti.service.genericformat.importactions.ImportOperationAction;
import fr.ifremer.tutti.service.genericformat.importactions.ImportParameterAction;
import fr.ifremer.tutti.service.genericformat.importactions.ImportProtocolAction;
import fr.ifremer.tutti.service.genericformat.importactions.ImportReferentialGearAction;
import fr.ifremer.tutti.service.genericformat.importactions.ImportReferentialPersonAction;
import fr.ifremer.tutti.service.genericformat.importactions.ImportReferentialSpeciesAction;
import fr.ifremer.tutti.service.genericformat.importactions.ImportReferentialVesselAction;
import fr.ifremer.tutti.service.genericformat.importactions.ImportSurveyAction;
import fr.ifremer.tutti.service.genericformat.importactions.ImpotSampleCategoryAction;
import fr.ifremer.tutti.service.genericformat.importactions.LoadAttachmentsAction;
import fr.ifremer.tutti.service.genericformat.importactions.ValidateAccidentalCatchAction;
import fr.ifremer.tutti.service.genericformat.importactions.ValidateCatchAction;
import fr.ifremer.tutti.service.genericformat.importactions.ValidateGearCaracteristicAction;
import fr.ifremer.tutti.service.genericformat.importactions.ValidateIndividualObservationAction;
import fr.ifremer.tutti.service.genericformat.importactions.ValidateMarineLitterAction;
import fr.ifremer.tutti.service.genericformat.importactions.ValidateOperationAction;
import fr.ifremer.tutti.service.genericformat.importactions.ValidateParameterAction;
import fr.ifremer.tutti.service.genericformat.importactions.ValidateSurveyAction;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.decorator.Decorator;

import java.io.File;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.stream.Collectors;

import static org.nuiton.i18n.I18n.t;

/**
 * Created on 2/11/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.14
 */
public class GenericFormatImportService extends AbstractTuttiService {

    private static final Log log = LogFactory.getLog(GenericFormatImportService.class);

    private PersistenceService persistenceService;

    private PdfGeneratorService pdfGeneratorService;

    private Decorator<FishingOperation> fishingOperationDecorator;

    private Decorator<Cruise> cruiseDecorator;

    @Override
    public void setServiceContext(TuttiServiceContext context) {

        super.setServiceContext(context);

        pdfGeneratorService = getService(PdfGeneratorService.class);
        persistenceService = getService(PersistenceService.class);

        DecoratorService decoratorService = getService(DecoratorService.class);
        cruiseDecorator = decoratorService.getDecoratorByType(Cruise.class);
        fishingOperationDecorator = decoratorService.getDecoratorByType(FishingOperation.class);

    }

    public int getValidateImportFileNbSteps(GenericFormatImportConfiguration importConfiguration) {

        Preconditions.checkNotNull(importConfiguration);

        GenericFormatArchive archive = createArchive(importConfiguration);

        return 6   // check sampleCategoryModel + import ( gear + person + species + vessel + protocol )
                     + archive.getSurveyLineCount() // load cruises
                     + archive.getGearCaracteristicsPathLineCount()// load gear caracteristics
                     + archive.getOperationPathLineCount() // load operations
                     + archive.getParameterPathLineCount()// load parameters
                     + archive.getCatchPathLineCount()// load catches
                     + archive.getMarineLitterPathLineCount()// load marine litters
                     + archive.getIndividualObservationPathLineCount()// load individualObservations
                     + archive.getAccidentalCatchPathLineCount();

    }

    public GenericFormatValidateFileResult validateImportFile(GenericFormatImportConfiguration importConfiguration, ProgressionModel progressionModel) throws GenericFormatArchiveInvalidLayoutException {

        Preconditions.checkNotNull(importConfiguration);

        Preconditions.checkNotNull(progressionModel);

        GenericFormatArchive archive = createArchive(importConfiguration);

        GenericFormatImportRequest importRequest = createImportRequest(importConfiguration, archive);

        GenericFormatValidateFileResult result = doValidate(importRequest, progressionModel);
        generateValidateReport(result, progressionModel);
        return result;

    }

    public int getImportProgramNbSteps(GenericFormatImportConfiguration importConfiguration) {

        //TODO Recompute exact nb steps used...
        Preconditions.checkNotNull(importConfiguration);

        GenericFormatArchive archive = createArchive(importConfiguration);

        int nbCruises = archive.getSurveyLineCount();
        if (log.isInfoEnabled()) {
            log.info("Count " + nbCruises + " cruises to import.");
        }
        int nbOperations = archive.getOperationPathLineCount();
        if (log.isInfoEnabled()) {
            log.info("Count " + nbOperations + " operations to import.");
        }

        boolean checkWeights = importConfiguration.isCheckWeights();
        boolean cleanWeights = importConfiguration.isCleanWeights();

        return 6                      // check sampleCategoryModel + import ( gear + person + species + vessel + protocol )
                     + 1 + nbCruises        // load cruises + nbCruise * persist cruise
                     + 1 + nbCruises        // load gear caracteristics + nbCruise * persist gear caracteristics
                     + 1 + nbOperations     // load operations + nbOperations * persist operation
                     + 1 + nbOperations     // load parameters + nbOperations * persist parameters
                     + 1 + nbOperations * 2 // load catches + nbOperations * persist (species batches + benthos batches)
                     + 1 + nbOperations     // load marine litters + nbOperations * persist marine litters
                     + 1 + nbOperations     // load individualObservations + nbOperations * persist individualObservations
                     + 1 + nbOperations     // load accidental catches + nbOperations * persist accidental catches
                     + (cleanWeights ? nbOperations : 0)         // nbOperations * cleanWeights
                     + (checkWeights ? nbOperations : 0)         // nbOperations * checkWeights
                     + 1;

    }

    public GenericFormatImportResult importProgram(GenericFormatImportConfiguration importConfiguration, ProgressionModel progressionModel) {

        Preconditions.checkNotNull(importConfiguration);

        Preconditions.checkNotNull(progressionModel);

        GenericFormatArchive archive = createArchive(importConfiguration);

        archive.validateArchiveLayout();

        GenericFormatImportRequest importRequest = createImportRequest(importConfiguration, archive);

        SynchronizationStatusHelper.doNotPropagateDirtyStatusToParents();

        try {

            GenericFormatImportResult result = doImport(importRequest, progressionModel);

            generateImportReport(result, progressionModel);

            return result;

        } finally {

            SynchronizationStatusHelper.propagateDirtyStatusToParents();

        }

    }

    protected void generateImportReport(GenericFormatImportResult result, ProgressionModel progressionModel) {

        File reportFile = result.getReportFile();

        progressionModel.increments(t("tutti.service.genericFormat.import.computeReport", reportFile));

        pdfGeneratorService.generatePdf(reportFile, context.getConfig().getI18nLocale(), "genericFormatImportReport.ftl", result);

    }

    protected void generateValidateReport(GenericFormatValidateFileResult result, ProgressionModel progressionModel) {

        File reportFile = result.getReportFile();

        progressionModel.increments(t("tutti.service.genericFormat.validate.computeReport"));

        pdfGeneratorService.generatePdf(reportFile, context.getConfig().getI18nLocale(), "genericFormatValidateReport.ftl", result);

    }

    protected void validateArchiveLayout(GenericFormatContextSupport importContext) {

        try {
            importContext.getImportRequest().getArchive().validateArchiveLayout();
        } catch (GenericFormatArchiveInvalidLayoutException e) {

            importContext.setArchiveLayoutErrors(e.getErrors());

        }

    }

    protected GenericFormatValidateFileResult doValidate(GenericFormatImportRequest request, ProgressionModel progressionModel) {

        try (GenericFormatValidateFileContext importContext = new GenericFormatValidateFileContext(request, progressionModel, persistenceService, cruiseDecorator, fishingOperationDecorator)) {

            validateArchiveLayout(importContext);

            if (importContext.isArchiveLayoutValid()) {

                GenericformatImportPersistenceHelper persitenceHelper = new GenericformatImportPersistenceHelper(context, importContext);

                Set<ImportActionSupport> actions = createValidateActions(importContext, persitenceHelper);

                actions.forEach(ImportActionSupport::execute);

            }

            return new GenericFormatValidateFileResult(importContext);

        }

    }

    protected GenericFormatImportResult doImport(GenericFormatImportRequest request, ProgressionModel progressionModel) {

        try (GenericFormatImportContext importContext = new GenericFormatImportContext(request, progressionModel, persistenceService, cruiseDecorator, fishingOperationDecorator)) {

            validateArchiveLayout(importContext);

            if (importContext.isArchiveLayoutValid()) {

                GenericformatImportPersistenceHelper persitenceHelper = new GenericformatImportPersistenceHelper(context, importContext);

                Set<ImportActionSupport> actions = createImportActions(importContext, persitenceHelper);

                actions.forEach(ImportActionSupport::execute);

            }

            return new GenericFormatImportResult(importContext);

        }

    }

    protected GenericFormatImportRequest createImportRequest(GenericFormatImportConfiguration importConfiguration, GenericFormatArchive archive) {

        ProgramDataModel dataToExport = importConfiguration.getDataToExport();
        Preconditions.checkNotNull(dataToExport);

        String programId = dataToExport.getId();
        Preconditions.checkNotNull(programId);

        Program program = persistenceService.getProgram(programId);
        Preconditions.checkNotNull(program);

        if (log.isDebugEnabled()) {
            log.debug("Will import into program: " + programId);
        }

        // Full load cruise
        Set<Cruise> cruises = persistenceService.getAllCruiseId(program.getId())
                                                .stream()
                                                .map(cruiseId -> persistenceService.getCruise(cruiseId))
                                                .collect(Collectors.toCollection(LinkedHashSet::new));

        TuttiProtocol protocol = persistenceService.getProtocol();

        ProgramDataModel dataModel = persistenceService.loadProgram(programId, true);
        return new GenericFormatImportRequest(importConfiguration,
                                              archive,
                                              ';',
                                              program,
                                              context.getSampleCategoryModel(),
                                              ImmutableSet.copyOf(cruises),
                                              dataModel,
                                              protocol);

    }

    protected GenericFormatArchive createArchive(GenericFormatImportConfiguration importConfiguration) {

        File importFile = importConfiguration.getImportFile();
        Preconditions.checkNotNull(importFile);
        Preconditions.checkState(importFile.exists());

        return GenericFormatArchive.forImport(importFile, context.getConfig().getTmpDirectory());

    }

    protected Set<ImportActionSupport> createValidateActions(GenericFormatValidateFileContext importContext, GenericformatImportPersistenceHelper persitenceHelper) {

        Set<ImportActionSupport> actions = new LinkedHashSet<>();
        addTechnicalActions(importContext, actions, persitenceHelper);
        actions.add(new LoadAttachmentsAction(importContext));
        actions.add(new ValidateSurveyAction(importContext, persitenceHelper));
        actions.add(new ValidateGearCaracteristicAction(importContext));
        actions.add(new ValidateOperationAction(importContext));
        actions.add(new ValidateParameterAction(importContext));
        actions.add(new ValidateCatchAction(importContext));
        actions.add(new ValidateMarineLitterAction(importContext));
        actions.add(new ValidateAccidentalCatchAction(importContext));
        actions.add(new ValidateIndividualObservationAction(importContext, persitenceHelper));
        return actions;

    }

    protected Set<ImportActionSupport> createImportActions(GenericFormatImportContext importContext, GenericformatImportPersistenceHelper persitenceHelper) {

        Set<ImportActionSupport> actions = new LinkedHashSet<>();
        addTechnicalActions(importContext, actions, persitenceHelper);
        actions.add(new LoadAttachmentsAction(importContext));
        actions.add(new ImportSurveyAction(importContext, persitenceHelper));
        actions.add(new ImportGearCaracteristicAction(importContext, persitenceHelper));
        actions.add(new ImportOperationAction(importContext, persitenceHelper));
        actions.add(new ImportParameterAction(importContext, persitenceHelper));
        actions.add(new ImportCatchAction(importContext, persitenceHelper));
        actions.add(new ImportMarineLitterAction(importContext, persitenceHelper));
        actions.add(new ImportAccidentalCatchAction(importContext, persitenceHelper));
        actions.add(new ImportIndividualObservationAction(importContext, persitenceHelper));
        actions.add(new DoCleanWeightAction(importContext, context.getService(WeightCleaningService.class)));
        actions.add(new DoCheckWeightAction(importContext, context.getService(GenericFormatCheckDataService.class)));
        return actions;

    }

    protected void addTechnicalActions(GenericFormatContextSupport importContext, Set<ImportActionSupport> actions, GenericformatImportPersistenceHelper persitenceHelper) {

        actions.add(new ImpotSampleCategoryAction(importContext, persitenceHelper));
        actions.add(new ImportReferentialGearAction(importContext, persitenceHelper));
        actions.add(new ImportReferentialPersonAction(importContext, persitenceHelper));
        actions.add(new ImportReferentialSpeciesAction(importContext, persitenceHelper));
        actions.add(new ImportReferentialVesselAction(importContext, persitenceHelper));
        actions.add(new ImportProtocolAction(importContext, persitenceHelper));

    }

}

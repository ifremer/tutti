package fr.ifremer.tutti.service.pupitri.report;

/*
 * #%L
 * Tutti :: Service
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Function;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Ordering;
import fr.ifremer.tutti.persistence.entities.data.FishingOperation;
import fr.ifremer.tutti.service.pupitri.PupitriImportResult;
import fr.ifremer.tutti.type.WeightUnit;
import fr.ifremer.tutti.util.Numbers;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Set;

/**
 * Modèle du rapport d'import Pupitri.
 *
 * Created on 11/22/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.11
 */
public class PupitriImportReportModel {

    private final FishingOperation operation;

    private final PupitriImportResult importResult;

    private final List<PupitriImportReportRow> rows;

    public PupitriImportReportModel(FishingOperation operation, PupitriImportResult importResult) {
        this.operation = operation;
        this.importResult = importResult;
        this.rows = new ArrayList<>();
    }

    public String getStationNumber() {
        return operation.getStationNumber();
    }

    public Integer getFishingOperationNumber() {
        return operation.getFishingOperationNumber();
    }

    public String getMultirigAggregation() {
        return operation.getMultirigAggregation();
    }

    public Date getGearShootingStartDate() {
        return operation.getGearShootingStartDate();
    }

    public Date getGearShootingEndDate() {
        return operation.getGearShootingEndDate();
    }

    public Float getTrunkSortedWeight() {
        Float sortedWeight = importResult.getSortedWeight();
        return sortedWeight == null ? null : WeightUnit.KG.round(sortedWeight);
    }

    public Float getTrunkRejectedWeight() {
        Float rejectedWeight = importResult.getRejectedWeight();
        return rejectedWeight == null ? null : WeightUnit.KG.round(rejectedWeight);
    }

    public Float getTrunkTotalWeight() {
        float trunkSortedWeight = Numbers.getValueOrComputedValue(importResult.getSortedWeight(), 0f);
        float trunkRejectedWeight = Numbers.getValueOrComputedValue(importResult.getRejectedWeight(), 0f);

        return WeightUnit.KG.round(trunkSortedWeight + trunkRejectedWeight);
    }

    public Float getCarrouselSortedWeight() {
        Float carrouselSortedWeight = importResult.getCarrouselSortedWeight();
        return carrouselSortedWeight == null ? null : WeightUnit.KG.round(carrouselSortedWeight);
    }

    public Float getCarrouselUnsortedWeight() {
        Float carrouselUnsortedWeight = importResult.getCarrouselUnsortedWeight();
        return carrouselUnsortedWeight == null ? null : WeightUnit.KG.round(carrouselUnsortedWeight);
    }

    public Float getCarrouselTotalWeight() {
        float trunkSortedWeight = Numbers.getValueOrComputedValue(importResult.getCarrouselSortedWeight(), 0f);
        float trunkRejectedWeight = Numbers.getValueOrComputedValue(importResult.getCarrouselUnsortedWeight(), 0f);

        return WeightUnit.KG.round(trunkSortedWeight + trunkRejectedWeight);
    }

    public Set<String> getNotImportedSpeciesIds() {
        return importResult.getNotImportedSpeciesIds();
    }

    public List<PupitriImportReportRow> getRows() {
        return rows;
    }

    public void addRow(PupitriImportReportRow aCatch) {
        rows.add(aCatch);
    }

    public void sortRows() {

        Ordering<PupitriImportReportRow> o1 = Ordering.natural().reverse().onResultOf(new Function<PupitriImportReportRow, Boolean>() {
            @Override
            public Boolean apply(PupitriImportReportRow input) {
                return input.isSorted();
            }
        });
        Ordering<PupitriImportReportRow> o2 = Ordering.natural().onResultOf(new Function<PupitriImportReportRow, String>() {
            @Override
            public String apply(PupitriImportReportRow input) {
                return input.getSpeciesCode();
            }
        });
        Ordering<PupitriImportReportRow> ordering = Ordering.compound(ImmutableSet.of(o1, o2));

        Collections.sort(rows, ordering);

    }
}

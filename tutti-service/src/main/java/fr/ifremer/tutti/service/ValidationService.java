package fr.ifremer.tutti.service;

/*
 * #%L
 * Tutti :: Service
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.entities.data.AccidentalBatch;
import fr.ifremer.tutti.persistence.entities.data.CatchBatch;
import fr.ifremer.tutti.persistence.entities.data.Cruise;
import fr.ifremer.tutti.persistence.entities.data.FishingOperation;
import fr.ifremer.tutti.persistence.entities.data.IndividualObservationBatch;
import fr.ifremer.tutti.persistence.entities.data.MarineLitterBatch;
import fr.ifremer.tutti.persistence.entities.data.Program;
import fr.ifremer.tutti.persistence.entities.data.SpeciesBatch;
import fr.ifremer.tutti.persistence.entities.protocol.TuttiProtocol;
import org.nuiton.validator.NuitonValidator;
import org.nuiton.validator.NuitonValidatorFactory;
import org.nuiton.validator.NuitonValidatorResult;

import java.io.IOException;

/**
 * To validate some incoming data using nuiton-validators.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.3
 */
public class ValidationService implements TuttiService {

    public static final String VALIDATION_CONTEXT_EDIT = "edit";

    public static final String VALIDATION_CONTEXT_VALIDATE = "validate";

    public NuitonValidatorResult validateValidateCruise(Cruise cruise) {
        NuitonValidator<Cruise> validator = NuitonValidatorFactory.newValidator(Cruise.class, VALIDATION_CONTEXT_VALIDATE);
        return validator.validate(cruise);
    }

    public NuitonValidatorResult validateEditCruise(Cruise cruise) {
        NuitonValidator<Cruise> validator = NuitonValidatorFactory.newValidator(Cruise.class, VALIDATION_CONTEXT_EDIT);
        return validator.validate(cruise);
    }

    public NuitonValidatorResult validateProgram(Program program) {
        NuitonValidator<Program> validator = NuitonValidatorFactory.newValidator(Program.class, VALIDATION_CONTEXT_EDIT);
        return validator.validate(program);
    }

    public NuitonValidatorResult validateProtocol(TuttiProtocol protocol) {
        NuitonValidator<TuttiProtocol> validator = NuitonValidatorFactory.newValidator(TuttiProtocol.class);
        return validator.validate(protocol);
    }

    public NuitonValidatorResult validateEditFishingOperation(FishingOperation fishingOperation) {
        NuitonValidator<FishingOperation> validator = NuitonValidatorFactory.newValidator(FishingOperation.class, VALIDATION_CONTEXT_EDIT);
        return validator.validate(fishingOperation);
    }

    public NuitonValidatorResult validateValidateFishingOperation(FishingOperation fishingOperation) {
        NuitonValidator<FishingOperation> validator = NuitonValidatorFactory.newValidator(FishingOperation.class, VALIDATION_CONTEXT_VALIDATE);
        return validator.validate(fishingOperation);
    }

    public NuitonValidatorResult validateCatchBatch(CatchBatch catchBatch, String context) {
        NuitonValidator<CatchBatch> validator = NuitonValidatorFactory.newValidator(CatchBatch.class, context);
        return validator.validate(catchBatch);
    }

    public NuitonValidatorResult validateValidateCatchBatch(CatchBatch catchBatch) {
        NuitonValidator<CatchBatch> validator = NuitonValidatorFactory.newValidator(CatchBatch.class, VALIDATION_CONTEXT_VALIDATE);
        return validator.validate(catchBatch);
    }

    public NuitonValidatorResult validateValidateSpeciesBatch(SpeciesBatch speciesBatch) {
        NuitonValidator<SpeciesBatch> validator = NuitonValidatorFactory.newValidator(SpeciesBatch.class, VALIDATION_CONTEXT_VALIDATE);
        return validator.validate(speciesBatch);
    }

    public NuitonValidatorResult validateEditSpeciesBatch(SpeciesBatch speciesBatch) {
        NuitonValidator<SpeciesBatch> validator = NuitonValidatorFactory.newValidator(SpeciesBatch.class, VALIDATION_CONTEXT_EDIT);
        return validator.validate(speciesBatch);
    }

    public NuitonValidatorResult validateValidateBenthosBatch(SpeciesBatch benthosBatch) {
        NuitonValidator<SpeciesBatch> validator = NuitonValidatorFactory.newValidator(SpeciesBatch.class, VALIDATION_CONTEXT_VALIDATE);
        return validator.validate(benthosBatch);
    }

    public NuitonValidatorResult validateEditBenthosBatch(SpeciesBatch benthosBatch) {
        NuitonValidator<SpeciesBatch> validator = NuitonValidatorFactory.newValidator(SpeciesBatch.class, VALIDATION_CONTEXT_EDIT);
        return validator.validate(benthosBatch);
    }

    public NuitonValidatorResult validateValidateMarineLitterBatch(MarineLitterBatch marineLitterBatch) {
        NuitonValidator<MarineLitterBatch> validator = NuitonValidatorFactory.newValidator(MarineLitterBatch.class, VALIDATION_CONTEXT_VALIDATE);
        return validator.validate(marineLitterBatch);
    }

    public NuitonValidatorResult validateEditMarineLitterBatch(MarineLitterBatch marineLitterBatch) {
        NuitonValidator<MarineLitterBatch> validator = NuitonValidatorFactory.newValidator(MarineLitterBatch.class, VALIDATION_CONTEXT_EDIT);
        return validator.validate(marineLitterBatch);
    }

    public NuitonValidatorResult validateValidateAccidentalBatch(AccidentalBatch accidentalBatch) {
        NuitonValidator<AccidentalBatch> validator = NuitonValidatorFactory.newValidator(AccidentalBatch.class, VALIDATION_CONTEXT_VALIDATE);
        return validator.validate(accidentalBatch);
    }

    public NuitonValidatorResult validateEditAccidentalBatch(AccidentalBatch accidentalBatch) {
        NuitonValidator<AccidentalBatch> validator = NuitonValidatorFactory.newValidator(AccidentalBatch.class, VALIDATION_CONTEXT_EDIT);
        return validator.validate(accidentalBatch);
    }

    public NuitonValidatorResult validateValidateIndividualObservationBatch(IndividualObservationBatch individualObservationBatch) {
        NuitonValidator<IndividualObservationBatch> validator = NuitonValidatorFactory.newValidator(IndividualObservationBatch.class, VALIDATION_CONTEXT_VALIDATE);
        return validator.validate(individualObservationBatch);
    }

    public NuitonValidatorResult validateEditIndividualObservationBatch(IndividualObservationBatch individualObservationBatch) {
        NuitonValidator<IndividualObservationBatch> validator = NuitonValidatorFactory.newValidator(IndividualObservationBatch.class, VALIDATION_CONTEXT_EDIT);
        return validator.validate(individualObservationBatch);
    }

    @Override
    public void setServiceContext(TuttiServiceContext context) {
    }

    @Override
    public void close() throws IOException {
    }
}

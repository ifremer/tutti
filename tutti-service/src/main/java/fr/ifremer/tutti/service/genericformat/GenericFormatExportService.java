package fr.ifremer.tutti.service.genericformat;

/*
 * #%L
 * Tutti :: Service
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import fr.ifremer.tutti.persistence.ProgressionModel;
import fr.ifremer.tutti.persistence.entities.data.Cruise;
import fr.ifremer.tutti.persistence.entities.data.FishingOperation;
import fr.ifremer.tutti.persistence.entities.protocol.TuttiProtocol;
import fr.ifremer.tutti.persistence.model.CruiseDataModel;
import fr.ifremer.tutti.persistence.model.ProgramDataModel;
import fr.ifremer.tutti.service.AbstractTuttiService;
import fr.ifremer.tutti.service.DecoratorService;
import fr.ifremer.tutti.service.PersistenceService;
import fr.ifremer.tutti.service.TuttiServiceContext;
import fr.ifremer.tutti.service.catches.WeightComputingService;
import fr.ifremer.tutti.service.genericformat.csv.SampleCategoryModel;
import fr.ifremer.tutti.service.genericformat.csv.SampleCategoryRow;
import fr.ifremer.tutti.service.genericformat.producer.CsvProducerForSampleCategory;
import org.nuiton.decorator.Decorator;
import org.nuiton.jaxx.application.ApplicationTechnicalException;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import static org.nuiton.i18n.I18n.t;

/**
 * To export data in the generic format.
 *
 * See http://forge.codelutin.com/issues/1875.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.3
 */
public class GenericFormatExportService extends AbstractTuttiService {

    private PersistenceService persistenceService;

    private WeightComputingService weightComputingService;

    private GenericFormatExportActionEngine actionEngine;

    private Decorator<FishingOperation> fishingOperationDecorator;

    @Override
    public void setServiceContext(TuttiServiceContext context) {

        super.setServiceContext(context);

        this.persistenceService = getService(PersistenceService.class);
        this.weightComputingService = getService(WeightComputingService.class);
        this.actionEngine = new GenericFormatExportActionEngine(context);

        DecoratorService decoratorService = getService(DecoratorService.class);
        this.fishingOperationDecorator = decoratorService.getDecoratorByType(FishingOperation.class);

    }

    public int getExportNbSteps(GenericFormatExportConfiguration configuration) {

        ProgramDataModel dataToExport = configuration.getDataToExport();

        int result = 8; // export species + sampleCategoryModel + protocol + temporary gear + temporary person + temporar species +  temporary vessel + zip
        for (CruiseDataModel cruise : dataToExport) {
            int nbFishingOperations = cruise.size();
            result += getCruiseNbStep(nbFishingOperations);
        }

        return result;

    }

    public GenericFormatExportResult export(GenericFormatExportConfiguration configuration, ProgressionModel progressionModel) {

        Preconditions.checkNotNull(configuration);
        Preconditions.checkNotNull(progressionModel);

        GenericFormatExportRequest exportRequest = createExportRequest(configuration);

        List<String> errors = new ArrayList<>();

        try (GenericFormatExportContext exportContext = createExportContext(exportRequest, progressionModel)) {

            actionEngine.executeLoadActions(exportContext);

            for (GenericFormatExportCruiseContext cruiseContext : exportContext) {

                Cruise cruise = cruiseContext.getCruise();

                Set<FishingOperation> operations = cruiseContext.getOperations();

                String checkErrors = cruiseContext.getCheckErrors();
                if (checkErrors != null) {
                    errors.add(checkErrors);
                }

                exportCruise(exportContext, cruise, operations);
            }

            actionEngine.executeTechnicalActions(exportContext);

            return new GenericFormatExportResult(exportRequest, errors);

        }

    }


    /**
     * Export the sample category model as a csv file used for generic format import-export.
     *
     * @param exportFile where to export sample category model
     * @since 3.14
     */
    public void exportSampleCategoryModel(File exportFile) throws IOException {

        ProgressionModel progressionModel = new ProgressionModel();
        progressionModel.setTotal(1000);

        GenericFormatArchive archive = GenericFormatArchive.forExport(null, context.getConfig().getTmpDirectory());

        try (CsvProducerForSampleCategory producerForSampleCategory = new CsvProducerForSampleCategory(archive.getSampleCategoryModelPath(), SampleCategoryModel.forExport(';'))) {

            List<SampleCategoryRow> dataToExport = producerForSampleCategory.getDataToExport(context.getSampleCategoryModel());

            try {
                producerForSampleCategory.write(dataToExport);
            } catch (Exception e) {
                throw new ApplicationTechnicalException("Could not export sample category model", e);
            }
        }

        try {
            Files.copy(archive.getSampleCategoryModelPath(), exportFile.toPath(), StandardCopyOption.REPLACE_EXISTING);
        } catch (IOException e) {
            throw new ApplicationTechnicalException("Could not copy csv file to export file", e);
        }

    }

    protected GenericFormatExportRequest createExportRequest(GenericFormatExportConfiguration configuration) {

        Path attachmentsSourcePath = context.getConfig().getDbAttachmentDirectory().toPath();

        TuttiProtocol tuttiProtocol = context.getDataContext().getProtocol();

        GenericFormatArchive archive = GenericFormatArchive.forExport(configuration.getExportFile(), context.getConfig().getTmpDirectory());

        return new GenericFormatExportRequest(configuration,
                                              archive,
                                              ';',
                                              context.getSampleCategoryModel(),
                                              tuttiProtocol,
                                              context.getConfig().getExportCountryId(),
                                              attachmentsSourcePath
        );

    }

    protected GenericFormatExportContext createExportContext(GenericFormatExportRequest exportRequest, ProgressionModel progressionModel) {

        return new GenericFormatExportContext(progressionModel,
                                              exportRequest,
                                              persistenceService,
                                              fishingOperationDecorator);

    }

    protected void exportCruise(GenericFormatExportContext exportContext, Cruise cruise, Set<FishingOperation> operations) {

        actionEngine.executeCruiseActions(exportContext, cruise);

        for (FishingOperation operation : operations) {

            GenericFormatExportOperationContext operationContext = exportContext.newOperationContext(persistenceService, weightComputingService, cruise, operation);
            exportContext.increments(t("tutti.service.genericFormat.exportCruise.exportOperation", cruise.getName(), operationContext.getOperationLabel()));

            actionEngine.executeOperationActions(exportContext, operationContext);

        }

    }

    protected int getCruiseNbStep(int nbFishingOperations) {

        return 1 + nbFishingOperations // check cruise + operations
                     + 1 // export cruise
                     + 1 // export gear caracteristics
                     + nbFishingOperations;

    }

}

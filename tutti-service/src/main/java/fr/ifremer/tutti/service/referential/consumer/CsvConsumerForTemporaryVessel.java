package fr.ifremer.tutti.service.referential.consumer;

/*
 * #%L
 * Tutti :: Service
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.entities.referential.Vessel;
import fr.ifremer.tutti.persistence.entities.referential.Vessels;
import fr.ifremer.tutti.service.DecoratorService;
import fr.ifremer.tutti.service.PersistenceService;
import fr.ifremer.tutti.service.csv.CsvComsumer;
import fr.ifremer.tutti.service.referential.ReferentialImportRequest;
import fr.ifremer.tutti.service.referential.csv.VesselModel;
import fr.ifremer.tutti.service.referential.csv.VesselRow;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.csv.ImportRow;
import org.nuiton.jaxx.application.ApplicationBusinessException;

import java.nio.file.Path;

import static org.nuiton.i18n.I18n.t;

/**
 * Created on 2/11/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.14
 */
public class CsvConsumerForTemporaryVessel extends CsvComsumer<VesselRow, VesselModel> {

    /** Logger. */
    private static final Log log = LogFactory.getLog(CsvConsumerForTemporaryVessel.class);

    public CsvConsumerForTemporaryVessel(Path file, char separator, boolean reportError) {
        super(file, VesselModel.forImport(separator), reportError);
    }

    public void checkRow(ImportRow<VesselRow> row,
                         PersistenceService persistenceService,
                         DecoratorService decoratorService,
                         ReferentialImportRequest<Vessel, String> requestResult) {

        if (row.isValid()) {

            VesselRow bean = row.getBean();

            String id = bean.getId();
            boolean delete = BooleanUtils.isTrue(bean.getToDelete());

            if (StringUtils.isBlank(id)) {

                // Ajout
                checkAdd(bean, requestResult);

            } else {

                // Mise à jour ou Suppression

                Vessel vessel = requestResult.getExistingEntityById(id);

                if (vessel == null) {

                    throw new ApplicationBusinessException(t("tutti.service.referential.import.vessel.error.notExistingId", id));
                }

                if (delete) {

                    // Suppression
                    checkDelete(bean, vessel, persistenceService, decoratorService, requestResult);

                } else {

                    // Mise à jour
                    checkUpdate(bean, vessel, requestResult);

                }
            }

        }

        reportError(row);

    }

    public void checkRowForGenericFormatImport(ImportRow<VesselRow> row, ReferentialImportRequest<Vessel, String> requestResult) {

        VesselRow bean = row.getBean();
        String internationalRegistrationCode = bean.getInternationalRegistrationCode();

        if (row.isValid()) {

            String id = bean.getId();

            if (id == null) {

                addCheckError(row, new ApplicationBusinessException(t("tutti.service.referential.import.vessel.error.noId")));

            } else if (!Vessels.isTemporaryId(id)) {

                addCheckError(row, new ApplicationBusinessException(t("tutti.service.referential.import.vessel.error.idNotTemporary", id)));

            } else if (requestResult.isIdAlreadyAdded(id)) {

                addCheckError(row, new ApplicationBusinessException(t("tutti.service.referential.import.vessel.error.id.alreaydAdded", id)));

            }

            if (StringUtils.isBlank(internationalRegistrationCode)) {

                addCheckError(row, new ApplicationBusinessException(t("tutti.service.referential.import.vessel.error.noRegistrationCode")));

            } else if (requestResult.isNaturalIdAlreadyAdded(internationalRegistrationCode)) {

                addCheckError(row, new ApplicationBusinessException(t("tutti.service.referential.import.vessel.error.name.alreaydAdded", internationalRegistrationCode)));

            }

        }

        reportError(row);

        if (row.isValid()) {

            Vessel entity = bean.toEntity();
            boolean toAdd = requestResult.addExistingNaturalId(internationalRegistrationCode);
            if (toAdd) {

                if (log.isInfoEnabled()) {
                    log.info("Will add vessel with internationalRegistrationCode: " + internationalRegistrationCode);
                }
                requestResult.addEntityToAdd(entity);

            } else {

                if (log.isInfoEnabled()) {
                    log.info("Will link vessel with internationalRegistrationCode: " + internationalRegistrationCode);
                }
                requestResult.addEntityToLink(entity);

            }

        }
    }

    protected void checkAdd(VesselRow bean, ReferentialImportRequest<Vessel, String> requestResult) {

        String internationalRegistrationCode = bean.getInternationalRegistrationCode();
        boolean delete = BooleanUtils.isTrue(bean.getToDelete());

        if (delete) {
            throw new ApplicationBusinessException(t("tutti.service.referential.import.vessel.error.cannotDeleteWithoutId"));
        }

        if (StringUtils.isBlank(internationalRegistrationCode)) {
            throw new ApplicationBusinessException(t("tutti.service.referential.import.vessel.error.noRegistrationCode"));
        }

        if (!requestResult.addExistingNaturalId(internationalRegistrationCode)) {
            throw new ApplicationBusinessException(t("tutti.service.referential.import.vessel.error.existingRegistrationCode", internationalRegistrationCode));
        }

        if (log.isInfoEnabled()) {
            log.info("Will add vessel with internationalRegistrationCode: " + internationalRegistrationCode);
        }

        requestResult.addEntityToAdd(bean.toEntity());


    }

    protected void checkDelete(VesselRow bean, Vessel vessel,
                               PersistenceService persistenceService,
                               DecoratorService decoratorService,
                               ReferentialImportRequest<Vessel, String> requestResult) {

        String id = StringUtils.trimToNull(bean.getId());
        String internationalRegistrationCode = bean.getInternationalRegistrationCode();

        if (persistenceService.isTemporaryVesselUsed(id)) {

            String vesselRef = id + " :" + decoratorService.getDecoratorByType(Vessel.class).toString(vessel);
            throw new ApplicationBusinessException(t("tutti.service.referential.import.error.vessel.used", vesselRef));
        }

        if (log.isInfoEnabled()) {
            log.info("Will delete vessel with internationalRegistrationCode: " + internationalRegistrationCode);
        }

        requestResult.addIdToDelete(id);
        requestResult.removeExistingNaturalId(internationalRegistrationCode);

    }

    protected void checkUpdate(VesselRow bean, Vessel vessel, ReferentialImportRequest<Vessel, String> requestResult) {

        String id = StringUtils.trimToNull(bean.getId());
        String internationalRegistrationCode = bean.getInternationalRegistrationCode();

        if (StringUtils.isBlank(internationalRegistrationCode)) {
            throw new ApplicationBusinessException(t("tutti.service.referential.import.vessel.error.noRegistrationCode", id));
        }

        if (!internationalRegistrationCode.equals(vessel.getInternationalRegistrationCode()) && !requestResult.addExistingNaturalId(internationalRegistrationCode)) {
            throw new ApplicationBusinessException(t("tutti.service.referential.import.vessel.error.existingRegistrationCode", internationalRegistrationCode));
        }

        if (log.isInfoEnabled()) {
            log.info("Will update vessel with internationalRegistrationCode: " + internationalRegistrationCode);
        }

        requestResult.addEntityToUpdate(bean.toEntity());

    }

}
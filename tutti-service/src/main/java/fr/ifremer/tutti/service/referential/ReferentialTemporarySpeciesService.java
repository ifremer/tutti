package fr.ifremer.tutti.service.referential;

/*
 * #%L
 * Tutti :: Service
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Lists;
import fr.ifremer.tutti.persistence.entities.TuttiEntities;
import fr.ifremer.tutti.persistence.entities.referential.Species;
import fr.ifremer.tutti.persistence.entities.referential.Speciess;
import fr.ifremer.tutti.service.AbstractTuttiService;
import fr.ifremer.tutti.service.DecoratorService;
import fr.ifremer.tutti.service.PersistenceService;
import fr.ifremer.tutti.service.TuttiServiceContext;
import fr.ifremer.tutti.service.referential.consumer.CsvConsumerForTemporarySpecies;
import fr.ifremer.tutti.service.referential.csv.SpeciesRow;
import fr.ifremer.tutti.service.referential.producer.CsvProducerForTemporarySpecies;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.csv.ImportRow;
import org.nuiton.jaxx.application.ApplicationTechnicalException;

import java.io.File;
import java.io.IOException;
import java.util.List;

import static org.nuiton.i18n.I18n.t;

/**
 * Created on 11/16/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.10
 */
public class ReferentialTemporarySpeciesService extends AbstractTuttiService {

    /** Logger. */
    private static final Log log =
            LogFactory.getLog(ReferentialTemporarySpeciesService.class);

    protected PersistenceService persistenceService;

    protected DecoratorService decoratorService;

    @Override
    public void setServiceContext(TuttiServiceContext context) {
        super.setServiceContext(context);
        persistenceService = getService(PersistenceService.class);
        decoratorService = getService(DecoratorService.class);
    }

    public ReferentialImportRequest<Species, Integer> createReferentialImportRequest() {

        List<Species> allSpecies = persistenceService.getAllSpecies();
        return new ReferentialImportRequest<>(allSpecies, TuttiEntities.<Species>newIdAstIntFunction(), Speciess.GET_NAME);

    }

    public ReferentialImportResult<Species> importTemporarySpecies(File file) {

        if (log.isInfoEnabled()) {
            log.info("Will import species from file: " + file);
        }

        ReferentialImportRequest<Species, Integer> requestResult = createReferentialImportRequest();

        try (CsvConsumerForTemporarySpecies consumer = new CsvConsumerForTemporarySpecies(file.toPath(), getCsvSeparator(), false, true)) {

            for (ImportRow<SpeciesRow> bean : consumer) {

                consumer.checkRow(bean, persistenceService, decoratorService, requestResult);

            }

        } catch (IOException e) {
            throw new ApplicationTechnicalException(t("tutti.service.referential.import.species.error", file), e);
        }

        return executeImportRequest(requestResult);

    }

    public ReferentialImportResult<Species> executeImportRequest(ReferentialImportRequest<Species, Integer> requestResult) {

        ReferentialImportResult<Species> result = new ReferentialImportResult<>();

        if (requestResult.withEntitiesToDelete()) {

            List<Integer> idsToDelete = requestResult.getIdsToDelete();
            persistenceService.deleteTemporarySpecies(idsToDelete);
            result.setNbRefDeleted(idsToDelete.size());

        }

        if (requestResult.withEntitiesToAdd()) {

            List<Species> entitiesToAdd = requestResult.getEntitiesToAdd();
            List<Species> entitiesAdded = persistenceService.addTemporarySpecies(entitiesToAdd);
            result.addAllRefsAdded(entitiesAdded);
        }

        if (requestResult.withEntitiesToUpdate()) {

            List<Species> entitiesToUpdate = requestResult.getEntitiesToUpdate();
            List<Species> entitiesUpdated = persistenceService.updateTemporarySpecies(entitiesToUpdate);
            result.addAllRefsUpdated(entitiesUpdated);

        }

        if (requestResult.withEntitiesToLink()) {

            List<Species> entitiesToLink = requestResult.getEntitiesToLink();
            List<Species> entitiesLinked = persistenceService.linkTemporarySpecies(entitiesToLink);
            result.addAllRefsLinked(entitiesLinked);

        }

        return result;

    }

    public List<Species> getTemporarySpeciess() {

        if (log.isInfoEnabled()) {
            log.info("Getting all species from database");
        }
        List<Species> targetList = Lists.newArrayList(persistenceService.getAllReferentSpecies());

        if (log.isInfoEnabled()) {
            log.info("Got " + targetList.size() + " species");
        }
        List<Species> toExport = persistenceService.retainTemporarySpeciesList(targetList);
        if (log.isInfoEnabled()) {
            log.info("Got " + toExport.size() + " temporary species");
        }
        return toExport;

    }

    public void exportExistingTemporarySpecies(File file) throws IOException {

        List<Species> toExport = getTemporarySpeciess();
        exportTemporarySpecies(file, toExport);

    }

    public void exportTemporarySpeciesExample(File file) throws IOException {

        List<Species> toExport = Lists.newArrayList();

        {
            Species s = Speciess.newSpecies();
            s.setName("Temporary Species name 1");
            toExport.add(s);
        }
        {
            Species s = Speciess.newSpecies();
            s.setName("Temporary Species name 2");
            toExport.add(s);
        }
        {
            Species s = Speciess.newSpecies();
            s.setName("Temporary Species name 3");
            toExport.add(s);
        }
        exportTemporarySpecies(file, toExport);

    }

    public void exportTemporarySpecies(File file, List<Species> toExport) throws IOException {

        try (CsvProducerForTemporarySpecies producerForTemporarySpecies = new CsvProducerForTemporarySpecies(file.toPath(), getCsvSeparator(), false)) {

            List<SpeciesRow> dataToExport = producerForTemporarySpecies.getDataToExport(toExport);
            producerForTemporarySpecies.write(dataToExport);

        } catch (Exception e) {
            throw new ApplicationTechnicalException(t("tutti.service.referential.export.species.error", file), e);
        }

    }

    protected char getCsvSeparator() {
        return ';';
    }
}

package fr.ifremer.tutti.service.genericformat.importactions;

/*
 * #%L
 * Tutti :: Service
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.service.genericformat.GenericFormatContextSupport;
import fr.ifremer.tutti.service.genericformat.GenericFormatCsvFileResult;
import fr.ifremer.tutti.service.genericformat.GenericFormatImportCruiseContext;
import fr.ifremer.tutti.service.genericformat.consumer.CsvConsumerForGearCaracteristic;
import fr.ifremer.tutti.service.genericformat.csv.GearCaracteristicRow;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.csv.ImportRow;
import org.nuiton.csv.ImportRuntimeException;
import org.nuiton.jaxx.application.ApplicationTechnicalException;

import java.io.IOException;

import static org.nuiton.i18n.I18n.t;

/**
 * Created on 3/3/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.14
 */
public class ValidateGearCaracteristicAction extends ImportActionSupport {

    /** Logger. */
    private static final Log log = LogFactory.getLog(ValidateGearCaracteristicAction.class);

    public ValidateGearCaracteristicAction(GenericFormatContextSupport importContext) {
        super(importContext);
    }

    @Override
    protected boolean canExecute() {
        return importContext.isTechnicalFilesValid() && importContext.getSurveyFileResult().isValid();
    }

    @Override
    protected void doExecute() {

        if (log.isInfoEnabled()) {
            log.info("Validate gearCaracteristics.csv file.");
        }

        int maximumRowsInErrorPerFile = importContext.getImportRequest().getMaximumRowsInErrorPerFile();

        GenericFormatCsvFileResult importFileResult = importContext.getGearCaracteristicFileResult();
        try (CsvConsumerForGearCaracteristic consumer = importContext.loadGearCaracteristics(false)) {
            for (ImportRow<GearCaracteristicRow> row : consumer) {

                importContext.increments(t("tutti.service.genericFormat.validate.gearCaracteristics", row.getLineNumber()));

                GenericFormatImportCruiseContext cruiseContext = consumer.validateRow(row, importContext);

                if (cruiseContext != null) {

                    consumer.prepareRowForPersist(cruiseContext, row);

                }

                if (consumer.getNbRowsInErrors() > maximumRowsInErrorPerFile) {
                    if (log.isWarnEnabled()) {
                        log.warn("Too much errors, stop validating this file.");
                    }
                    break;
                }

            }

            flushConsumer(consumer, importFileResult);

        } catch (IOException e) {
            throw new ApplicationTechnicalException("Could not close gearCaracteristic.csv file", e);
        } catch (ImportRuntimeException e) {

            importFileResult.addGlobalError(e.getMessage());

        }

    }

}

package fr.ifremer.tutti.service.catches.multipost;

/*
 * #%L
 * Tutti :: Service
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

/**
 * Created on 11/16/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.9
 */
public interface MultiPostConstants {

    String BATCHES_KEY = "batchesKey";

    String ATTACHMENTS_DIRECTORY = "attachments";

    String ATTACHMENTS_FILE = "attachments.csv";

    String CATCH_BATCH_FILE = "catchBatch.csv";

    String SPECIES_FILE = "species.csv";

    String BENTHOS_FILE = "benthos.csv";

    String MARINE_LITTER_FILE = "marineLitter.csv";

    String INDIVIDUAL_OBSERVATION_FILE = "individualObservation.csv";

    String ACCIDENTAL_CATCHES_FILE = "accidentalCatches.csv";

    String FREQUENCIES_FILE = "frequencies.csv";

    String CARACTERISTIC_FILE = "caracteristics.csv";

    String WEIGHTS_FILE = "weights.csv";

    char CSV_SEPARATOR = ';';
}

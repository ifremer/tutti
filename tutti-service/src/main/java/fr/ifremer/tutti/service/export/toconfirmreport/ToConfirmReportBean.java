package fr.ifremer.tutti.service.export.toconfirmreport;

/*
 * #%L
 * Tutti :: Service
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.entities.data.Cruise;
import fr.ifremer.tutti.type.WeightUnit;

import java.util.LinkedList;
import java.util.List;

/**
 * To store the cruise and fishing operation to report.
 *
 * Created on 2/9/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.13
 */
public class ToConfirmReportBean {

    private final Cruise cruise;

    private final WeightUnit speciesWeightUnit;

    private final WeightUnit benthosWeightUnit;

    private final List<ToConfirmReportFishingOperationBean> operations;

    public ToConfirmReportBean(Cruise cruise,
                               WeightUnit speciesWeightUnit,
                               WeightUnit benthosWeightUnit) {
        this.cruise = cruise;
        this.speciesWeightUnit = speciesWeightUnit;
        this.benthosWeightUnit = benthosWeightUnit;
        this.operations = new LinkedList<>();
    }

    public Cruise getCruise() {
        return cruise;
    }

    public WeightUnit getSpeciesWeightUnit() {
        return speciesWeightUnit;
    }

    public WeightUnit getBenthosWeightUnit() {
        return benthosWeightUnit;
    }

    public void addOperation(ToConfirmReportFishingOperationBean operation) {
        operations.add(operation);
    }

    public List<ToConfirmReportFishingOperationBean> getOperations() {
        return operations;
    }
}

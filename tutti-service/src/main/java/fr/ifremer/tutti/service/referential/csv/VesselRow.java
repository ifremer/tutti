package fr.ifremer.tutti.service.referential.csv;

/*
 * #%L
 * Tutti :: Service
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import fr.ifremer.tutti.persistence.entities.referential.Vessel;
import fr.ifremer.tutti.persistence.entities.referential.Vessels;

/**
 * @author Kevin Morin (Code Lutin)
 * @since 3.8
 */
public class VesselRow {

    public static final String PROPERTY_ID = "id";

    public static final String PROPERTY_NAME = "name";

    public static final String PROPERTY_SCIENTIFIC_VESSEL = "scientificVessel";

    public static final String PROPERTY_INTERNATIONAL_REGISTRATION_CODE = "internationalRegistrationCode";

    public static final String PROPERTY_TO_DELETE = "toDelete";

    protected String id;

    protected String name;

    protected boolean scientificVessel;

    protected String internationalRegistrationCode;

    protected Boolean toDelete;

    public VesselRow() {
        super();
    }

    public VesselRow(Vessel vessel) {
        super();
        Preconditions.checkNotNull(vessel);
        setId(vessel.getId());
//        setRegistrationCode(vessel.getRegistrationCode());
        setName(vessel.getName());
        setInternationalRegistrationCode(vessel.getInternationalRegistrationCode());
        setScientificVessel(vessel.isScientificVessel());
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isScientificVessel() {
        return scientificVessel;
    }

    public void setScientificVessel(boolean scientificVessel) {
        this.scientificVessel = scientificVessel;
    }

    public String getInternationalRegistrationCode() {
        return internationalRegistrationCode;
    }

    public void setInternationalRegistrationCode(String internationalRegistrationCode) {
        this.internationalRegistrationCode = internationalRegistrationCode;
    }

    public Boolean getToDelete() {
        return toDelete;
    }

    public void setToDelete(Boolean toDelete) {
        this.toDelete = toDelete;
    }

    public Vessel toEntity() {

        Vessel vessel = Vessels.newVessel();
        vessel.setId(getId());
        vessel.setName(getName());
        vessel.setScientificVessel(isScientificVessel());
        vessel.setInternationalRegistrationCode(getInternationalRegistrationCode());
        return vessel;

    }

    public Integer getIdAsInt() {
        Integer idAsInt = null;
        if (id != null) {
            idAsInt = Integer.valueOf(id);
        }
        return idAsInt;
    }
}

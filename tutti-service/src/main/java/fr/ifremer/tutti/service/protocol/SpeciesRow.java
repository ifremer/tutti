package fr.ifremer.tutti.service.protocol;

/*
 * #%L
 * Tutti :: Service
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.entities.protocol.Rtp;
import fr.ifremer.tutti.persistence.entities.protocol.SpeciesProtocol;
import fr.ifremer.tutti.persistence.entities.protocol.SpeciesProtocols;
import fr.ifremer.tutti.persistence.entities.referential.Caracteristic;
import fr.ifremer.tutti.persistence.entities.referential.Species;
import org.apache.commons.lang3.StringUtils;

import java.io.Serializable;
import java.util.List;

/**
 * Row of a {@link SpeciesProtocol} to import or export.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.0
 */
public class SpeciesRow implements Serializable {

    private static final long serialVersionUID = 1L;

    public static final String PROPERTY_SPECIES = "species";

    public static final String PROPERTY_SPECIES_REFERENCE_TAXON_ID = "speciesReferenceTaxonId";

    public static final String PROPERTY_SPECIES_REF_TAX_CODE = "speciesRefTaxCode";

    public static final String PROPERTY_SPECIES_NAME = "speciesName";

    public static final String PROPERTY_SPECIES_SURVEY_CODE = "speciesSurveyCode";

    public static final String PROPERTY_LENGTH_STEP_PMFM = "lengthStepPmfm";

    public static final String PROPERTY_LENGTH_STEP_PMFM_PARAMETER_NAME = "lengthStepPmfmParameterName";

    public static final String PROPERTY_LENGTH_STEP_PMFM_MATRIX_NAME = "lengthStepPmfmMatrixName";

    public static final String PROPERTY_LENGTH_STEP_PMFM_FRACTION_NAME = "lengthStepPmfmFractionName";

    public static final String PROPERTY_LENGTH_STEP_PMFM_METHOD_NAME = "lengthStepPmfmMethodName";

    public static final String PROPERTY_LENGTH_STEP_PMFM_ID = "lengthStepPmfmId";

    public static final String PROPERTY_WEIGHT_ENABLED = "weightEnabled";

    public static final String PROPERTY_COUNT_IF_NO_FREQUENCY_ENABLED = "countIfNoFrequencyEnabled";

    public static final String PROPERTY_CALCIFY_SAMPLE_ENABLED = "calcifySampleEnabled";

    public static final String PROPERTY_MANDATORY_SAMPLE_CATEGORY_ID = "mandatorySampleCategoryId";

    public static final String PROPERTY_RTP_MALE_A = "rtpMaleA";

    public static final String PROPERTY_RTP_MALE_B = "rtpMaleB";

    public static final String PROPERTY_RTP_FEMALE_A = "rtpFemaleA";

    public static final String PROPERTY_RTP_FEMALE_B = "rtpFemaleB";

    public static final String PROPERTY_RTP_UNDEFINED_A = "rtpUndefinedA";

    public static final String PROPERTY_RTP_UNDEFINED_B = "rtpUndefinedB";

    protected final SpeciesProtocol delegate;

    protected Species species;

    protected Caracteristic lengthStepPmfm;

    public SpeciesRow() {
        delegate = SpeciesProtocols.newSpeciesProtocol();
        delegate.setMadeFromAReferentTaxon(true);
    }

    public Species getSpecies() {
        return species;
    }

    public Integer getSpeciesReferenceTaxonId() {
        return species == null ? null : delegate.getSpeciesReferenceTaxonId();
    }

    public String getSpeciesName() {
        return species == null ? null : species.getName();
    }

    public void setSpeciesName(String name) {
        if (!StringUtils.isEmpty(name)) {
            boolean madeFromAReferentTaxon = isMadeFromAReferentTaxon();
            setMadeFromAReferentTaxon(madeFromAReferentTaxon &&
                    species != null && species.getName().equals(name));
        }
    }

    public String getSpeciesRefTaxCode() {
        return species.getRefTaxCode();
    }

    public void setSpeciesRefTaxCode(String refTaxCode) {
        if (!StringUtils.isEmpty(refTaxCode)) {
            boolean madeFromAReferentTaxon = isMadeFromAReferentTaxon();
            setMadeFromAReferentTaxon(madeFromAReferentTaxon &&
                    species != null && species.getRefTaxCode().equals(refTaxCode));
        }
    }

    public void setSpecies(Species species) {
        this.species = species;
        delegate.setSpeciesReferenceTaxonId(species == null ? null : species.getReferenceTaxonId());
    }

    public String getSpeciesSurveyCode() {
        return delegate.getSpeciesSurveyCode();
    }

    public void setSpeciesSurveyCode(String speciesSuurveyCode) {
        delegate.setSpeciesSurveyCode(speciesSuurveyCode);
    }

    public Caracteristic getLengthStepPmfm() {
        return lengthStepPmfm;
    }

    public void setLengthStepPmfm(Caracteristic lengthStepPmfm) {
        this.lengthStepPmfm = lengthStepPmfm;
        delegate.setLengthStepPmfmId(lengthStepPmfm == null ? null : lengthStepPmfm.getId());
    }

    public String getLengthStepPmfmId() {
        return delegate.getLengthStepPmfmId();
    }

    public String getLengthStepPmfmParameterName() {
        return lengthStepPmfm == null ? null : lengthStepPmfm.getParameterName();
    }

    public String getLengthStepPmfmMethodName() {
        return lengthStepPmfm == null ? null : lengthStepPmfm.getMethodName();
    }

    public String getLengthStepPmfmMatrixName() {
        return lengthStepPmfm == null ? null : lengthStepPmfm.getMatrixName();
    }

    public String getLengthStepPmfmFractionName() {
        return lengthStepPmfm == null ? null : lengthStepPmfm.getFractionName();
    }

    public boolean isWeightEnabled() {
        return delegate.isWeightEnabled();
    }

    public void setWeightEnabled(boolean weightEnabled) {
        delegate.setWeightEnabled(weightEnabled);
    }

    public boolean isMadeFromAReferentTaxon() {
        return delegate.isMadeFromAReferentTaxon();
    }

    public void setMadeFromAReferentTaxon(boolean referent) {
        delegate.setMadeFromAReferentTaxon(referent);
    }

    public void setId(String id) {
        delegate.setId(id);
    }

    public String getId() {
        return delegate.getId();
    }

    public void setMandatorySampleCategoryId(List<Integer> mandatorySampleCategoryId) {
        delegate.setMandatorySampleCategoryId(mandatorySampleCategoryId);
    }

    public List<Integer> getMandatorySampleCategoryId() {
        return delegate.getMandatorySampleCategoryId();
    }

    public boolean withRtpMale() {
        return delegate.withRtpMale();
    }

    public boolean withRtpFemale() {
        return delegate.withRtpFemale();
    }

    public boolean withRtpUndefined() {
        return delegate.withRtpUndefined();
    }

    public Rtp getRtpMale() {
        return delegate.getRtpMale();
    }

    public Rtp getRtpFemale() {
        return delegate.getRtpFemale();
    }

    public Rtp getRtpUndefined() {
        return delegate.getRtpUndefined();
    }

    public void setRtpMale(Rtp rtpMale) {
        delegate.setRtpMale(rtpMale);
    }

    public void setRtpFemale(Rtp rtpFemale) {
        delegate.setRtpFemale(rtpFemale);
    }

    public void setRtpUndefined(Rtp rtpUndefined) {
        delegate.setRtpUndefined(rtpUndefined);
    }

    public Double getRtpMaleA() {
        return withRtpMale() ? getRtpMale().getA() : null;
    }

    public Float getRtpMaleB() {
        return withRtpMale() ? getRtpMale().getB() : null;
    }

    public Double getRtpFemaleA() {
        return withRtpFemale() ? getRtpFemale().getA() : null;
    }

    public Float getRtpFemaleB() {
        return withRtpFemale() ? getRtpFemale().getB() : null;
    }

    public Double getRtpUndefinedA() {
        return withRtpUndefined() ? getRtpUndefined().getA() : null;
    }

    public Float getRtpUndefinedB() {
        return withRtpUndefined() ? getRtpUndefined().getB() : null;
    }

    public void setRtpMaleA(Double a) {
        getRtpMale().setA(a);
    }

    public void setRtpMaleB(Float b) {
        getRtpMale().setB(b);
    }

    public void setRtpFemaleA(Double a) {
        getRtpFemale().setA(a);
    }

    public void setRtpFemaleB(Float b) {
        getRtpFemale().setB(b);
    }

    public void setRtpUndefinedA(Double a) {
        getRtpUndefined().setA(a);
    }

    public void setRtpUndefinedB(Float b) {
        getRtpUndefined().setB(b);
    }

}

package fr.ifremer.tutti.service.genericformat.exportactions;

/*
 * #%L
 * Tutti :: Service
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.adagio.core.dao.referential.ObjectTypeCode;
import fr.ifremer.tutti.persistence.entities.data.Attachment;
import fr.ifremer.tutti.persistence.entities.data.BatchContainer;
import fr.ifremer.tutti.persistence.entities.data.CatchBatch;
import fr.ifremer.tutti.persistence.entities.data.SpeciesBatch;
import fr.ifremer.tutti.service.PersistenceService;
import fr.ifremer.tutti.service.genericformat.GenericFormatExportContext;
import fr.ifremer.tutti.service.genericformat.GenericFormatExportOperationContext;
import fr.ifremer.tutti.service.genericformat.csv.AttachmentRow;
import fr.ifremer.tutti.service.genericformat.csv.CatchRow;
import fr.ifremer.tutti.service.genericformat.producer.CsvProducerForAttachment;
import fr.ifremer.tutti.service.genericformat.producer.CsvProducerForCatch;
import fr.ifremer.tutti.service.genericformat.producer.CsvProducerForSpecies;
import fr.ifremer.tutti.util.Numbers;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * Created on 3/28/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.14.3
 */
public class CreateCatchBatchRowsAction extends ExportFishingOperationActionSupport {

    /** Logger. */
    private static final Log log = LogFactory.getLog(CreateCatchBatchRowsAction.class);

    private final PersistenceService persistenceService;

    public CreateCatchBatchRowsAction(PersistenceService persistenceService) {
        this.persistenceService = persistenceService;
    }

    @Override
    public boolean doExecute(GenericFormatExportContext exportContext, GenericFormatExportOperationContext operationContext) {
        return (exportContext.isExportSpecies() || exportContext.isExportBenthos()) && operationContext.isWithCatchBatch();
    }

    @Override
    public void execute(GenericFormatExportContext exportContext, GenericFormatExportOperationContext operationContext) {

        boolean exportSpecies = exportContext.isExportSpecies();
        boolean exportBenthos = exportContext.isExportBenthos();
        boolean exportAll = exportSpecies && exportBenthos;

        if (exportSpecies) {
            prepareSpeciesBatches(exportContext, operationContext);
        }

        if (exportBenthos) {
            prepareBenthosBatches(exportContext, operationContext);
        }

        CatchBatch catchBatch = operationContext.getCatchBatch();

        Float catchRaisingfactor = computeCatchRaisingfactor(catchBatch);

        Float speciesCatchRaisingFactor = null;
        if (exportSpecies) {
            speciesCatchRaisingFactor = computeSpeciesRaisingFactor(catchBatch, catchRaisingfactor);
        }

        Float benthosCatchRaisingFactor = null;
        if (exportBenthos) {
            benthosCatchRaisingFactor = computeBenthosRaisingFactor(catchBatch, catchRaisingfactor);
        }

        CsvProducerForCatch producerForCatch = exportContext.getProducerForCatch();

        if (exportAll) {

            if (log.isInfoEnabled()) {
                log.info(String.format("Operation %s [catchRF: %s / speciesRF: %s / benthosRF: %s ]", operationContext.getOperation().getId(), catchRaisingfactor, speciesCatchRaisingFactor, benthosCatchRaisingFactor));
            }

            List<CatchRow> catchRows = producerForCatch.getDataToExport(operationContext, speciesCatchRaisingFactor, benthosCatchRaisingFactor);
            operationContext.setCatchRows(catchRows);

        } else if (exportSpecies) {

            if (log.isInfoEnabled()) {
                log.info(String.format("Operation %s [catchRF: %s / speciesRF: %s ]", operationContext.getOperation().getId(), catchRaisingfactor, speciesCatchRaisingFactor));
            }

            List<CatchRow> catchRows = producerForCatch.getSpeciesOnlyDataToExport(operationContext, speciesCatchRaisingFactor);
            operationContext.setCatchRows(catchRows);

        } else if (exportBenthos) {

            if (log.isInfoEnabled()) {
                log.info(String.format("Operation %s [catchRF: %s / benthosRF: %s ]", operationContext.getOperation().getId(), catchRaisingfactor, benthosCatchRaisingFactor));
            }

            List<CatchRow> catchRows = producerForCatch.getBenthosOnlyDataToExport(operationContext, benthosCatchRaisingFactor);
            operationContext.setCatchRows(catchRows);

        }

    }

    private void prepareSpeciesBatches(GenericFormatExportContext exportContext, GenericFormatExportOperationContext operationContext) {

        BatchContainer<SpeciesBatch> rootSpeciesBatch = operationContext.getRootSpeciesBatch();

        // Warning! This will also add the survey code to batch species
        CsvProducerForSpecies producerForSpecies = exportContext.getProducerForSpecies();
        producerForSpecies.prepareSpeciesBatchRows(rootSpeciesBatch);

        if (exportContext.isExportAttachments()) {

            CsvProducerForAttachment producerForAttachment = exportContext.getProducerForAttachment();

            List<AttachmentRow> attachmentRows = new ArrayList<>();

            for (SpeciesBatch speciesBatch : rootSpeciesBatch.getChildren()) {
                List<Attachment> attachments = persistenceService.getAllAttachments(ObjectTypeCode.BATCH, speciesBatch.getIdAsInt());
                producerForAttachment.addAttachments(attachments, attachmentRows);
            }

            operationContext.addAttachmentRows(attachmentRows);

        }

    }

    private void prepareBenthosBatches(GenericFormatExportContext exportContext, GenericFormatExportOperationContext operationContext) {

        BatchContainer<SpeciesBatch> rootSpeciesBatch = operationContext.getRootBenthosBatch();

        // Warning! This will also add the survey code to batch species
        CsvProducerForSpecies producerForSpecies = exportContext.getProducerForSpecies();
        producerForSpecies.prepareBenthosBatchRows(rootSpeciesBatch);

        CsvProducerForAttachment producerForAttachment = exportContext.getProducerForAttachment();

        if (exportContext.isExportAttachments()) {

            List<AttachmentRow> attachmentRows = new ArrayList<>();

            for (SpeciesBatch benthosBatch : rootSpeciesBatch.getChildren()) {
                List<Attachment> attachments = persistenceService.getAllAttachments(ObjectTypeCode.BATCH, benthosBatch.getIdAsInt());
                producerForAttachment.addAttachments(attachments, attachmentRows);
            }

            operationContext.addAttachmentRows(attachmentRows);

        }


    }

    private Float computeSpeciesRaisingFactor(CatchBatch catchBatch, Float catchRaisingFactor) {

        Float totalSortedSpeciesWeight = Numbers.getValueOrComputedValue(
                catchBatch.getSpeciesTotalSortedWeight(),
                catchBatch.getSpeciesTotalSortedComputedWeight());

        Float totalSampleSortedSpeciesWeight = catchBatch.getSpeciesTotalSampleSortedComputedWeight();

        Float speciesCatchRaisingFactor;
        if (totalSampleSortedSpeciesWeight == null || totalSortedSpeciesWeight == null) {
            speciesCatchRaisingFactor = 1f;
        } else {
            if (totalSampleSortedSpeciesWeight == 0) {
                speciesCatchRaisingFactor = 0f;
            } else {
                speciesCatchRaisingFactor = (totalSortedSpeciesWeight / totalSampleSortedSpeciesWeight) * catchRaisingFactor;
            }
        }
        return speciesCatchRaisingFactor;

    }

    private Float computeBenthosRaisingFactor(CatchBatch catchBatch, Float catchRaisingFactor) {

        Float totalSampleSortedBenthosWeight = catchBatch.getBenthosTotalSampleSortedComputedWeight();

        Float totalSortedBenthosWeight = Numbers.getValueOrComputedValue(
                catchBatch.getBenthosTotalSortedWeight(),
                catchBatch.getBenthosTotalSortedComputedWeight());

        Float benthosCatchRaisingFactor;

        if (totalSampleSortedBenthosWeight == null || totalSortedBenthosWeight == null) {
            benthosCatchRaisingFactor = 1f;
        } else {
            if (totalSampleSortedBenthosWeight == 0) {
                benthosCatchRaisingFactor = 0f;
            } else {
                benthosCatchRaisingFactor = (totalSortedBenthosWeight / totalSampleSortedBenthosWeight) * catchRaisingFactor;
            }
        }

        return benthosCatchRaisingFactor;

    }

    private Float computeCatchRaisingfactor(CatchBatch catchBatch) {

        Float totalSortedWeight = catchBatch.getCatchTotalSortedComputedWeight();
        Float totalSortedSortedWeight = catchBatch.getCatchTotalSortedSortedComputedWeight();

        // tchemit 2015-04-28 see http://forge.codelutin.com/issues/7021

        return totalSortedWeight == null || totalSortedSortedWeight == null ? 1 : totalSortedWeight / totalSortedSortedWeight;

    }

}

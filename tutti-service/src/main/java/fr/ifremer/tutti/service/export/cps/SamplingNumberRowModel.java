package fr.ifremer.tutti.service.export.cps;

/*
 * #%L
 * Tutti :: Service
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2016 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.entities.referential.CaracteristicQualitativeValue;
import fr.ifremer.tutti.persistence.entities.referential.Species;
import fr.ifremer.tutti.service.csv.AbstractTuttiImportExportModel;
import fr.ifremer.tutti.service.csv.TuttiCsvUtil;
import fr.ifremer.tutti.service.sampling.CacheExtractedKey;
import org.nuiton.csv.ValueFormatter;
import org.nuiton.decorator.Decorator;

import static org.nuiton.i18n.I18n.t;

/**
 * @author Kevin Morin (Code Lutin)
 * @since 4.5
 */
public class SamplingNumberRowModel extends AbstractTuttiImportExportModel<CacheExtractedKey> {

    public SamplingNumberRowModel(char separator, Decorator<Species> speciesCodeDecorator) {
        super(separator);

        newColumnForExport(t("tutti.service.calcifiedPiecesSamplingReport.header.surveyCode"), CacheExtractedKey.PROPERTY_SPECIES, new ValueFormatter<Species>() {

            @Override
            public String format(Species species) {
                return speciesCodeDecorator.toString(species);
            }
        });
        newColumnForExport(t("tutti.service.calcifiedPiecesSamplingReport.header.geniusName"), CacheExtractedKey.PROPERTY_SPECIES, new ValueFormatter<Species>() {

            @Override
            public String format(Species species) {
                return species.getName();
            }
        });
        newColumnForExport(t("tutti.service.calcifiedPiecesSamplingReport.header.lengthStep"), CacheExtractedKey.PROPERTY_LENGTH_STEP, TuttiCsvUtil.PRIMITIVE_INTEGER);
        newColumnForExport(t("tutti.service.calcifiedPiecesSamplingReport.header.maturity"), CacheExtractedKey.PROPERTY_MATURITY, new ValueFormatter<Boolean>() {

            @Override
            public String format(Boolean maturity) {
                if (maturity == null) {
                    return "";
                }
                return maturity ? t("tutti.maturity.mature") : t("tutti.maturity.immature");
            }
        });
        newColumnForExport(t("tutti.service.calcifiedPiecesSamplingReport.header.gender"), CacheExtractedKey.PROPERTY_SEX, new ValueFormatter<CaracteristicQualitativeValue>() {
            @Override
            public String format(CaracteristicQualitativeValue value) {
                return value == null ? "" : t(value.getDescription());
            }
        });
        newColumnForExport(t("tutti.service.calcifiedPiecesSamplingReport.header.samplingNb"), CacheExtractedKey.PROPERTY_SAMPLING_NB, TuttiCsvUtil.PRIMITIVE_INTEGER);
        newColumnForExport(t("tutti.service.calcifiedPiecesSamplingReport.header.maxByLengthStep"), CacheExtractedKey.PROPERTY_MAX_BY_LENGTH_STEP, TuttiCsvUtil.INTEGER);
    }

    @Override
    public CacheExtractedKey newEmptyInstance() {
        return new CacheExtractedKey();
    }
}

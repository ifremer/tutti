package fr.ifremer.tutti.service.genericformat;

/*
 * #%L
 * Tutti :: Service
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

/**
 * Created on 3/2/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.14
 */
public class GenericFormatFileResult implements Serializable {

    private static final long serialVersionUID = 1L;

    private final String filename;

    private final boolean mandatory;

    private final boolean found;

    private final Map<Long, Set<String>> errors;

    private boolean imported;

    private boolean skipped;

    public GenericFormatFileResult(String filename, boolean mandatory, boolean found) {
        this.filename = filename;
        this.mandatory = mandatory;
        this.found = found;
        this.errors = new LinkedHashMap<>();
    }

    public String getFilename() {
        return filename;
    }

    public Set<Map.Entry<Long, Set<String>>> getErrorsEntries() {
        return errors.entrySet();
    }

    public boolean isValid() {
        boolean result;

        if (!found) {

            // valid only if not mandatory
            result = !mandatory;

        } else {

            // must be imported and with no errors please
            result = isSkipped() || (isImported() && errors.isEmpty());

        }

        return result;
    }

    public void addGlobalError(String error) {

        Set<String> globalErrors = errors.get(0l);
        if (globalErrors==null) {
            globalErrors = new LinkedHashSet<>();
            errors.put(0l, globalErrors);
        }
        globalErrors.add(error);
    }

    public boolean isImported() {
        return imported;
    }

    public void setImported(boolean imported) {
        this.imported = imported;
    }

    public boolean isSkipped() {
        return skipped;
    }

    public void setSkipped(boolean skipped) {
        this.skipped = skipped;
    }

    public boolean isFound() {
        return found;
    }

    public boolean isMandatory() {
        return mandatory;
    }

    protected Map<Long, Set<String>> getErrors() {
        return errors;
    }
}

package fr.ifremer.tutti.service.report;

/*
 * #%L
 * Tutti :: Service
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import fr.ifremer.tutti.TuttiConfiguration;
import fr.ifremer.tutti.persistence.ProgressionModel;
import fr.ifremer.tutti.persistence.entities.data.FishingOperation;
import fr.ifremer.tutti.persistence.model.ProgramDataModel;
import fr.ifremer.tutti.service.AbstractTuttiService;
import fr.ifremer.tutti.service.PersistenceService;
import fr.ifremer.tutti.service.genericformat.GenericFormatExportConfiguration;
import fr.ifremer.tutti.service.genericformat.GenericFormatExportResult;
import fr.ifremer.tutti.service.genericformat.GenericFormatExportService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.jaxx.application.ApplicationTechnicalException;

import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.nuiton.i18n.I18n.t;

/**
 * Created on 3/11/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.13.2
 */
public class ReportGenerationService extends AbstractTuttiService {

    private static final Log log = LogFactory.getLog(ReportGenerationService.class);

    private static final FileFilter REPORT_FILTER = pathname -> pathname.isFile() && pathname.getName().endsWith(".rptdesign");

    public List<File> getAvailableReports() {
        List<File> result = Lists.newArrayList();
        File reportDirectory = context.getConfig().getReportDirectory();
        File[] files = reportDirectory.listFiles(REPORT_FILTER);
        if (files != null) {
            result.addAll(Arrays.asList(files));
        }
        return result;
    }

    public ReportGenerationResult generateReport(ReportGenerationRequest request, ProgressionModel progressionModel) {

        Preconditions.checkNotNull(request);
        Preconditions.checkNotNull(request.getReport());
        Preconditions.checkState(request.getReport().exists());
        Preconditions.checkNotNull(request.getProgramId());
        Preconditions.checkNotNull(request.getCruiseId());
        Preconditions.checkNotNull(request.getFishingOperationId());

        // load fishing operation
        progressionModel.increments(t("tutti.report.step.load.fishingOperation"));
        FishingOperation operation = getService(PersistenceService.class).
                getFishingOperation(request.getFishingOperationId());

        // export fishing operation
        progressionModel.increments(t("tutti.report.step.export.fishingOperation"));
        GenericFormatExportService service = getService(GenericFormatExportService.class);
        GenericFormatExportConfiguration exportConfiguration = createExportConfigurationForFishingOperation(request.getProgramId(), request.getCruiseId(), request.getFishingOperationId(), null);

        GenericFormatExportResult exportResult = service.export(exportConfiguration, progressionModel);

        File exportDirectory = exportResult.getArchive().getWorkingDirectoryPath();
        
        File outputFile = newOutputFile();

        ReportGenerationContext reportContext = new ReportGenerationContext(request,
                                                                            operation,
                                                                            exportDirectory,
                                                                            outputFile);

        progressionModel.increments(t("tutti.report.step.generateReport", request.getReport().getName()));

        return generateReport(reportContext);

    }

    protected GenericFormatExportConfiguration createExportConfigurationForFishingOperation(String programId, Integer cruiseId, Integer fishingOperationId, File exportFile) {

        Preconditions.checkNotNull(programId);
        Preconditions.checkNotNull(cruiseId);
        Preconditions.checkNotNull(fishingOperationId);

        PersistenceService persistenceService = getService(PersistenceService.class);

        ProgramDataModel dataToExport = persistenceService.loadCruise(programId, cruiseId, fishingOperationId);

        GenericFormatExportConfiguration configuration = new GenericFormatExportConfiguration();
        configuration.setExportFile(exportFile);
        configuration.setExportAttachments(true);
        configuration.setExportSpecies(true);
        configuration.setExportBenthos(true);
        configuration.setExportMarineLitter(true);
        configuration.setExportAccidentalCatch(true);
        configuration.setExportIndividualObservation(true);
        configuration.setDataToExport(dataToExport);
        return configuration;

    }


    protected File newOutputFile() {
        return context.getConfig().newTempFile("tutti-report", ".pdf");
    }

    protected ReportGenerationResult generateReport(ReportGenerationContext reportContext) {

        Preconditions.checkNotNull(reportContext.getFishingOperation());
        Preconditions.checkNotNull(reportContext.getExportDirectory());
        Preconditions.checkState(reportContext.getExportDirectory().exists());

        if (log.isInfoEnabled()) {
            log.info("Will generate report using report " + reportContext.getReport().getName());
        }

        List<String> arguments = prepareCall(reportContext);

        ProcessBuilder pb = new ProcessBuilder(arguments);

        pb.inheritIO();

        if (log.isInfoEnabled()) {
            log.info("Starts java command: " + arguments);
        }

        try {

            int exitCode = pb.start().waitFor();

            if (log.isDebugEnabled()) {
                log.debug("Exit Code: " + exitCode);
            }

            if (exitCode != 0) {
                throw new ApplicationTechnicalException("Report execution failed, see the report log directory " + context.getConfig().getReportLogDirectory());
            }

        } catch (InterruptedException | IOException e) {
            throw new ApplicationTechnicalException("Could not generate report", e);
        }

        if (log.isInfoEnabled()) {
            log.info("Report generated at: " + reportContext.getOutputFile());
        }

        return reportContext.toResult();

    }

    protected List<String> prepareCall(ReportGenerationContext reportContext) {

        TuttiConfiguration config = context.getConfig();

        File reportJarPath = config.getReportJarPath();

        Preconditions.checkState(reportJarPath != null, "No reportJarPath configured!");

        List<String> command = new ArrayList<>();

        command.add(config.getJavaCommandPath());

        // jvm options
        command.add("-XX:MaxPermSize=128M");
        command.add("-XX:PermSize=64M");
        command.add("-XX:MaxNewSize=32M");
        command.add("-XX:NewSize=32M");
        command.add("-Xms128m");
        command.add("-Xmx512m");

        //command.add("-agentlib:jdwp=transport=dt_socket,server=y,suspend=y,address=8000");

        command.add("-jar");

        command.add(reportJarPath.getAbsolutePath());

        // arguments

        command.add(config.getReportHomeDirectory().getAbsolutePath());
        command.add(config.getReportLogDirectory().getAbsolutePath());
        command.add(reportContext.getReport().getAbsolutePath());
        command.add(reportContext.getOutputFile().getAbsolutePath());
        command.add(reportContext.getExportDirectory().getAbsolutePath());
        command.add(reportContext.getFishingOperation().getStationNumber());
        command.add(reportContext.getFishingOperation().getFishingOperationNumber().toString());

        return command;

    }

    public int getNbSteps(ReportGenerationRequest request) {

        // get data / export / generate
        int result = 4;

        GenericFormatExportService service = getService(GenericFormatExportService.class);
        GenericFormatExportConfiguration exportConfiguration = createExportConfigurationForFishingOperation(request.getProgramId(), request.getCruiseId(), request.getFishingOperationId(), null);
        result += service.getExportNbSteps(exportConfiguration);
        return result;

    }

}

package fr.ifremer.tutti.service.pupitri;

/*
 * #%L
 * Tutti :: Service
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.ListMultimap;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import fr.ifremer.adagio.core.dao.referential.ObjectTypeCode;
import fr.ifremer.adagio.core.dao.referential.pmfm.PmfmId;
import fr.ifremer.adagio.core.dao.referential.pmfm.QualitativeValueId;
import fr.ifremer.tutti.persistence.entities.data.Attachment;
import fr.ifremer.tutti.persistence.entities.data.Attachments;
import fr.ifremer.tutti.persistence.entities.data.BatchContainer;
import fr.ifremer.tutti.persistence.entities.data.CatchBatch;
import fr.ifremer.tutti.persistence.entities.data.FishingOperation;
import fr.ifremer.tutti.persistence.entities.data.SpeciesBatch;
import fr.ifremer.tutti.persistence.entities.data.SpeciesBatchs;
import fr.ifremer.tutti.persistence.entities.protocol.SpeciesProtocol;
import fr.ifremer.tutti.persistence.entities.protocol.SpeciesProtocols;
import fr.ifremer.tutti.persistence.entities.protocol.TuttiProtocol;
import fr.ifremer.tutti.persistence.entities.referential.Caracteristic;
import fr.ifremer.tutti.persistence.entities.referential.CaracteristicQualitativeValue;
import fr.ifremer.tutti.persistence.entities.referential.CaracteristicQualitativeValues;
import fr.ifremer.tutti.persistence.entities.referential.Species;
import fr.ifremer.tutti.persistence.entities.referential.Speciess;
import fr.ifremer.tutti.persistence.entities.referential.TaxonCache;
import fr.ifremer.tutti.persistence.entities.referential.TaxonCaches;
import fr.ifremer.tutti.service.AbstractTuttiService;
import fr.ifremer.tutti.service.DecoratorService;
import fr.ifremer.tutti.service.PdfGeneratorService;
import fr.ifremer.tutti.service.PersistenceService;
import fr.ifremer.tutti.service.TuttiDataContext;
import fr.ifremer.tutti.service.TuttiServiceContext;
import fr.ifremer.tutti.service.csv.ImportModelWithHeader;
import fr.ifremer.tutti.service.pupitri.csv.CarrouselRow;
import fr.ifremer.tutti.service.pupitri.csv.CarrouselRowModel;
import fr.ifremer.tutti.service.pupitri.csv.TrunkRow;
import fr.ifremer.tutti.service.pupitri.csv.TrunkRowModel;
import fr.ifremer.tutti.service.pupitri.report.PupitriImportReportModel;
import fr.ifremer.tutti.service.pupitri.report.PupitriImportReportRow;
import fr.ifremer.tutti.type.WeightUnit;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.csv.Import;
import org.nuiton.decorator.Decorator;
import org.nuiton.jaxx.application.ApplicationIOUtil;
import org.nuiton.jaxx.application.ApplicationTechnicalException;

import java.io.File;
import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import static org.nuiton.i18n.I18n.n;
import static org.nuiton.i18n.I18n.t;

/**
 * @author Kevin Morin - kmorin@codelutin.com
 * @since 1.2
 */
public class PupitriImportService extends AbstractTuttiService {

    private static final Log log =
            LogFactory.getLog(PupitriImportService.class);

    protected DecoratorService decoratorService;

    protected PersistenceService persistenceService;

    protected PdfGeneratorService pdfGeneratorService;

    protected TuttiDataContext dataContext;

    protected CaracteristicQualitativeValue sortedCaracteristic;

    protected CaracteristicQualitativeValue unsortedCaracteristic;

    protected Map<Signs, CaracteristicQualitativeValue> signsToCaracteristicValue;

    @Override
    public void setServiceContext(TuttiServiceContext context) {
        super.setServiceContext(context);
        persistenceService = getService(PersistenceService.class);
        decoratorService = getService(DecoratorService.class);
        pdfGeneratorService = getService(PdfGeneratorService.class);
        dataContext = context.getDataContext();

        signsToCaracteristicValue = Maps.newEnumMap(Signs.class);

        { // sorted/unsorted caracteristic
            Caracteristic caracteristic =
                    persistenceService.getSortedUnsortedCaracteristic();

            sortedCaracteristic = CaracteristicQualitativeValues.getQualitativeValue(caracteristic, QualitativeValueId.SORTED_VRAC.getValue());
            unsortedCaracteristic = CaracteristicQualitativeValues.getQualitativeValue(caracteristic, QualitativeValueId.SORTED_HORS_VRAC.getValue());
        }

        { // sex category
            Caracteristic caracteristic = persistenceService.getSexCaracteristic();

            Signs.DEFAULT.registerSign(caracteristic, signsToCaracteristicValue);
            Signs.FEMALE.registerSign(caracteristic, signsToCaracteristicValue);
            Signs.MALE.registerSign(caracteristic, signsToCaracteristicValue);
        }
        { // size category
            Caracteristic caracteristic = persistenceService.getSizeCategoryCaracteristic();

            Signs.SMALL.registerSign(caracteristic, signsToCaracteristicValue);
            Signs.MEDIUM.registerSign(caracteristic, signsToCaracteristicValue);
            Signs.BIG.registerSign(caracteristic, signsToCaracteristicValue);
        }
    }

    /**
     * Used merely in tests.
     * @param trunkFile     incoming trunk file
     * @param carrouselFile incoming carroussle file
     * @param operation     target operation
     * @param catchBatch    target catch batch
     * @return the number of rows of the .car file which have not been imported
     */
    public PupitriImportResult importPupitri(File trunkFile,
                                             File carrouselFile,
                                             FishingOperation operation,
                                             CatchBatch catchBatch) {

        return importPupitri(trunkFile, carrouselFile, operation, catchBatch, false);
    }

    /**
     * Used merely in tests.
     * @param trunkFile     incoming trunk file
     * @param carrouselFile incoming carroussle file
     * @param operation     target operation
     * @param catchBatch    target catch batch
     * @param importMissingCategoryBatches import or not empty batches for missing sex or size batches
     * @return the number of rows of the .car file which have not been imported
     */
    public PupitriImportResult importPupitri(File trunkFile,
                                             File carrouselFile,
                                             FishingOperation operation,
                                             CatchBatch catchBatch,
                                             boolean importMissingCategoryBatches) {

        PupitriImportResult result = readImportPupitri(trunkFile, carrouselFile, operation, importMissingCategoryBatches);
        return saveImportPupitri(trunkFile, carrouselFile, operation, catchBatch, importMissingCategoryBatches, result);
    }

    /**
     * @param trunkFile     incoming trunk file
     * @param carrouselFile incoming carroussle file
     * @param operation     target operation
     * @param importMissingCategoryBatches import or not empty batches for missing sex or size batches
     * @return the number of rows of the .car file which have not been imported
     */
    public PupitriImportResult readImportPupitri(File trunkFile,
                                                 File carrouselFile,
                                                 FishingOperation operation,
                                                 boolean importMissingCategoryBatches) {

        PupitriImportResult result = new PupitriImportResult();

        importPupitriTrunk(result, trunkFile, operation);
        importPupitriCarrousel(result, carrouselFile, operation, importMissingCategoryBatches);

        return result;
    }

    /**
     * @param trunkFile     incoming trunk file
     * @param carrouselFile incoming carroussle file
     * @param operation     target operation
     * @param importMissingCategoryBatches import or not empty batches for missing sex or size batches
     * @return the number of rows of the .car file which have not been imported
     */
    public PupitriImportResult saveImportPupitri(File trunkFile,
                                                 File carrouselFile,
                                                 FishingOperation operation,
                                                 CatchBatch catchBatch,
                                                 boolean importMissingCategoryBatches,
                                                 PupitriImportResult result) {

        if (result.isFishingOperationFound()) {

            // generation du rapport
            File reportFile = generatePupitriReport(operation, result);

            // gestion du melange
            Decorator<Species> decorator = getService(DecoratorService.class).getDecoratorByType(Species.class);
            result.prepareMelag(decorator);

            try {

                // persistence des lots
                savePupitriImportResult(result, operation, catchBatch, importMissingCategoryBatches);

                // ajout des pièces-jointes
                addFileAsAttachment(trunkFile, catchBatch);
                addFileAsAttachment(carrouselFile, catchBatch);

                DateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd__HH_mm");
                String reportAttachmentFilename = "rapport-pupitri-" + simpleDateFormat.format(context.currentDate()) + ".pdf";
                if (log.isInfoEnabled()) {
                    log.info("Save report with attachment filename: " + reportAttachmentFilename);
                }
                String reportId = addFileAsAttachment(reportFile, reportAttachmentFilename, catchBatch);
                result.setReportAttachmentId(reportId);
                result.setReportAttachmentFilename(reportAttachmentFilename);

            } finally {
                FileUtils.deleteQuietly(reportFile);
            }

        }

        return result;
    }

    //FIXME Check how to deal with Melag meta species and species involed in melag
    protected File generatePupitriReport(FishingOperation operation, PupitriImportResult result) {

        TaxonCache taxonCache = TaxonCaches.createSpeciesCache(persistenceService, context.getDataContext().getProtocol());

        PupitriImportReportModel reportModel = new PupitriImportReportModel(operation, result);

        for (PupitriSpeciesContext aCatch : result.getCatches()) {

            addCatchEntry(taxonCache, aCatch, reportModel);

        }

        reportModel.sortRows();

        File reportFile = context.getConfig().newTempFile("puputri-report", ".pdf");
        Locale locale = context.getConfig().getI18nLocale();

        pdfGeneratorService.generatePdf(reportFile, locale, "pupitriReport.ftl", reportModel);

        return reportFile;

    }

    protected void addCatchEntry(TaxonCache taxonCache, PupitriSpeciesContext aCatch, PupitriImportReportModel reportModel) {

        Species species = aCatch.getSpecies();
        taxonCache.load(species);

        String code = Speciess.getSurveyCodeOrRefTaxCode(species);

        String name = species.getName();

        String vernacularCode = species.getVernacularCode();

        for (Signs signs : aCatch.getSigns()) {

            PupitriSignContext signContext = aCatch.getSignContext(signs);

            if (!WeightUnit.KG.isNullOrZero(signContext.getWeight())) {

                PupitriImportReportRow reportCatch = PupitriImportReportRow.newRow(code,
                                                                                   name,
                                                                                   vernacularCode,
                                                                                   aCatch.isSorted(),
                                                                                   signContext);
                reportModel.addRow(reportCatch);
            }
        }
    }

    protected String addFileAsAttachment(File f, CatchBatch catchBatch) {
        return addFileAsAttachment(f, f.getName(), catchBatch);
    }

    protected String addFileAsAttachment(File f, String attachmentName, CatchBatch catchBatch) {
        Attachment attachment = Attachments.newAttachment();
        attachment.setObjectType(ObjectTypeCode.CATCH_BATCH);
        attachment.setObjectId(Integer.valueOf(catchBatch.getId()));
        attachment.setName(attachmentName);
        String date = DateFormat.getDateTimeInstance().format(context.currentDate());
        String comment = t("tutti.service.pupitri.import.attachment.comment", date);
        attachment.setComment(comment);
        Attachment savedAttachment = persistenceService.createAttachment(attachment, f);
        return savedAttachment.getId();
    }

    protected void importPupitriTrunk(PupitriImportResult result, File file, FishingOperation operation) {

        if (log.isInfoEnabled()) {
            log.info("Will import pupitri operation [" + operation.toString() + "] trunk from file: " + file);
        }

        // prepare import

        float sortedWeight = 0f;
        float rejectedWeight = 0f;

        TrunkRowModel csvModel = new TrunkRowModel(',');
        File fileWithHeaders = createFileWithHeaders(csvModel, file);

        try (Reader reader = ApplicationIOUtil.newReader(
                fileWithHeaders,
                n("tutti.service.pupitri.import.trunk.error"))) {
            try (Import<TrunkRow> importer = Import.newImport(csvModel, reader)) {

                for (TrunkRow bean : importer) {
                    if (bean.acceptOperation(operation)) {
                        switch (bean.getDirection()) {
                            case VAT:
                                sortedWeight += bean.getWeight();
                                break;

                            case VNT:
                                rejectedWeight += bean.getWeight();
                        }
                        result.incrementNbTrunkImported();
                    } else {
                        result.incrementNbTrunkNotImported();
                    }
                }
            }

        } catch (Exception e) {
            throw new ApplicationTechnicalException(
                    t("tutti.service.pupitri.import.trunk.error", operation.toString(), file), e);

        } finally {
            FileUtils.deleteQuietly(fileWithHeaders);
        }

        result.setSortedWeight(WeightUnit.KG.round(sortedWeight));

        if (rejectedWeight > 0f) {

            // On set rejectweight when not zero, See http://forge.codelutin.com/issues/5676
            result.setRejectedWeight(WeightUnit.KG.round(rejectedWeight));
        }

    }

    protected void importPupitriCarrousel(PupitriImportResult result,
                                          File carrouselFile,
                                          FishingOperation operation,
                                          boolean importMissingCategoryBatches) {

        if (log.isInfoEnabled()) {
            log.info("Will import pupitri operation [" + operation.toString() +
                     "] carrousel from file: " + carrouselFile);
        }

        // process import file
        CarrouselImportRequestResult carrouselImportRequestResult = processCarrouselImportFile(carrouselFile,
                                                                                               operation,
                                                                                               importMissingCategoryBatches);

        // save it to global result
        result.flushCarrouselResult(carrouselImportRequestResult);

    }

    protected CarrouselImportRequestResult processCarrouselImportFile(File carrouselFile,
                                                                      FishingOperation operation,
                                                                      boolean importMissingCategoryBatches) {

        // get the map of species by survey code or reftax code
        ListMultimap<String, Species> speciesBySurveyCode = ArrayListMultimap.create();
        speciesBySurveyCode.putAll(Speciess.splitBySurveyCode(dataContext.getReferentSpeciesWithSurveyCode(false)));
        speciesBySurveyCode.putAll(Speciess.splitByRefTaxCode(dataContext.getReferentSpecies()));

        CarrouselImportRequestResult result = new CarrouselImportRequestResult(speciesBySurveyCode);

        CarrouselRowModel carrouselCsvModel = new CarrouselRowModel(',');
        File fileWithHeaders = createFileWithHeaders(carrouselCsvModel, carrouselFile);

        try (Reader reader = ApplicationIOUtil.newReader(
                fileWithHeaders,
                n("tutti.service.pupitri.import.carrousel.error"))) {

            try (Import<CarrouselRow> importer = Import.newImport(carrouselCsvModel, reader)) {

                for (CarrouselRow bean : importer) {

                    importCarrouselRow(result, operation, bean, importMissingCategoryBatches);

                }
            }

        } catch (Exception e) {
            DecoratorService service = getService(DecoratorService.class);
            throw new ApplicationTechnicalException(
                    t("tutti.service.pupitri.import.carrousel.error", carrouselFile, service.getDecorator(operation).toString(operation)), e);

        } finally {

            FileUtils.deleteQuietly(fileWithHeaders);
        }

        return result;

    }

    protected void importCarrouselRow(CarrouselImportRequestResult result,
                                      FishingOperation operation,
                                      CarrouselRow bean,
                                      boolean importMissingCategoryBatches) {

        if (!bean.acceptOperation(operation)) {

            // ce lot n'est pas sur la bonne operation
            return;

        }

        // le lot est sur la bonne operation
        result.incrementNbCarrousselImported();

        // poids du lot
        Float beanWeight = bean.getWeight();
        if (beanWeight < 0f) {

            //FIXME Savoir pourquoi cela peut arriver ?
            beanWeight = 0f;

        }

        boolean sorted = bean.isSorted();
        if (sorted) {

            // ajout au total des poids trie du carrousel
            // meme si ensuite l'espèce peut-être rejetée
            result.addCarrouselSortedWeight(beanWeight);

        } else {

            // ajout au total des poids non trié du carrousel
            // meme si ensuite l'espèce peut-être rejetée
            result.addCarrouselUnsortedWeight(beanWeight);

        }

        String speciesId = bean.getSpeciesId();
        List<Species> speciesList = result.getSpecies(speciesId);
        if (CollectionUtils.isEmpty(speciesList)) {

            // l'espece n'est pas reconnu
            // pas de traitement sur ce lot
            result.addNotImportedSpeciesId(speciesId);

        } else {

            // creation d'un nouveau lot (par rapport au tuple (espece, trie))
            // ou bien recuperation d'un lot deja existant
            PupitriSpeciesContext pupitriSpeciesContext = result.getOrCreateCatch(speciesList,
                                                                                  importMissingCategoryBatches,
                                                                                  sorted);

            // ajout des données (type de box, poids) au signe donne dans le lot
            pupitriSpeciesContext.addToSignContext(bean.getSign(), bean.getBoxType(), beanWeight);

        }

    }

    protected void savePupitriImportResult(PupitriImportResult result,
                                           FishingOperation operation,
                                           CatchBatch catchBatch,
                                           boolean importMissingCategoryBatches) {

        catchBatch.setCatchTotalSortedTremisWeight(result.getSortedWeight());
        catchBatch.setCatchTotalRejectedWeight(result.getRejectedWeight());

        catchBatch.setCatchTotalSortedCarousselWeight(result.getCarrouselSortedWeight());

        // delete all species batches

        BatchContainer<SpeciesBatch> rootSpeciesBatch = persistenceService.getRootSpeciesBatch(operation.getIdAsInt(), false);
        for (SpeciesBatch batch : rootSpeciesBatch.getChildren()) {
            persistenceService.deleteSpeciesBatch(batch.getIdAsInt());
        }

        // insert all imported species batches

        TuttiProtocol protocol = dataContext.getProtocol();

        // especes à sexer
        Collection<String> surveyCodesToSex =
                getSurveyCodeWhoseCategoryIsMandatory(protocol,
                                                      persistenceService.getSexCaracteristic());

        // especes à trier par taille
        Collection<String> surveyCodesToSize =
                getSurveyCodeWhoseCategoryIsMandatory(protocol,
                                                      persistenceService.getSizeCategoryCaracteristic());

        // especes à sexer et trier par taille
        Collection<String> surveyCodesToSexAndSize = CollectionUtils.intersection(surveyCodesToSex, surveyCodesToSize);

        // on ne veut que les espèces qu'il faut soit sexer soit trier par taille
        surveyCodesToSex.removeAll(surveyCodesToSexAndSize);
        surveyCodesToSize.removeAll(surveyCodesToSexAndSize);

        String melagComment = result.getMelagComment();

        Decorator<Species> speciesDecorator = decoratorService.getDecoratorByType(Species.class);
        for (PupitriSpeciesContext pupitriSpeciesContext : result.getCatches()) {

            Species species = pupitriSpeciesContext.getSpecies();
            CaracteristicQualitativeValue cqv = pupitriSpeciesContext.isSorted() ?
                                                sortedCaracteristic : unsortedCaracteristic;

            boolean splitSpecies = pupitriSpeciesContext.isSplitSpecies();

            String speciesStr = speciesDecorator.toString(species);

            if (splitSpecies) {

                // create a top batch + a son for each sign
                if (log.isDebugEnabled()) {
                    log.debug("Create a categorized batches for species " + speciesStr);
                }

                SpeciesBatch batch = createSpeciesBatch(operation,
                                                        species,
                                                        null,
                                                        PmfmId.SORTED_UNSORTED.getValue(),
                                                        cqv);

                batch = persistenceService.createSpeciesBatch(batch, null, true);

                Integer parentBatchId = batch.getIdAsInt();

                for (Signs signs : pupitriSpeciesContext.getSigns()) {

                    boolean addMelagComment = pupitriSpeciesContext.isAddMelagComment(signs);
                    float catchWeight = pupitriSpeciesContext.getWeight(signs);

                    Integer categoryId = signs.getCategory();
                    CaracteristicQualitativeValue splitCqv = signsToCaracteristicValue.get(signs);
                    SpeciesBatch childBatch = createSpeciesBatch(
                            operation,
                            species,
                            WeightUnit.KG.isNullOrZero(catchWeight) ? null : catchWeight,
                            categoryId,
                            splitCqv);

                    if (addMelagComment) {
                        childBatch.setComment(melagComment);
                        if (log.isInfoEnabled()) {
                            log.info("Add melag comment for sign " + signs + " on " + speciesStr);
                        }
                    }

                    persistenceService.createSpeciesBatch(childBatch, parentBatchId, true);
                }

            } else {

                // create a unique batch with sum all weights as simple weight
                float totalWeight = pupitriSpeciesContext.getTotalWeight();
                if (log.isInfoEnabled()) {
                    log.info("Create a unique batch for species " + speciesStr + " with total weight: " + totalWeight);
                }

                SpeciesBatch batch = createSpeciesBatch(operation,
                                                        species,
                                                        totalWeight,
                                                        PmfmId.SORTED_UNSORTED.getValue(),
                                                        cqv);

                boolean addMelagComment = pupitriSpeciesContext.isAddMelagComment(Signs.DEFAULT);
                if (addMelagComment) {
                    if (log.isInfoEnabled()) {
                        log.info("Add melag comment for simple batch " + speciesStr);
                    }
                    batch.setComment(melagComment);
                }

                batch = persistenceService.createSpeciesBatch(batch, null, true);

                if (importMissingCategoryBatches) {

                    Integer parentBatchId = batch.getIdAsInt();
                    String speciesId = species.getSurveyCode();

                    List<Signs> signs2add = null;

                    if (surveyCodesToSex.contains(speciesId)) {

                        signs2add = Lists.newArrayList(Signs.MALE, Signs.FEMALE, Signs.DEFAULT);

                    } else if (surveyCodesToSize.contains(speciesId)) {

                        signs2add = Lists.newArrayList(Signs.SMALL, Signs.BIG);
                    }

                    if (signs2add != null) {
                        for (Signs signs : signs2add) {

                            Integer categoryId = signs.getCategory();
                            CaracteristicQualitativeValue splitCqv = signsToCaracteristicValue.get(signs);
                            SpeciesBatch childBatch = createSpeciesBatch(
                                    operation,
                                    species,
                                    null,
                                    categoryId,
                                    splitCqv);

                            persistenceService.createSpeciesBatch(childBatch, parentBatchId, true);
                        }
                    }
                }
            }
        }

        persistenceService.saveCatchBatch(catchBatch);
    }

    protected SpeciesBatch createSpeciesBatch(FishingOperation operation,
                                              Species species,
                                              Float catchWeight,
                                              Integer categoryId,
                                              Serializable cqv) {
        SpeciesBatch batch = SpeciesBatchs.newSpeciesBatch();
        batch.setFishingOperation(operation);
        batch.setSampleCategoryId(categoryId);
        batch.setSampleCategoryValue(cqv);
        batch.setSpecies(species);
        batch.setSampleCategoryWeight(
                catchWeight == null ? null : WeightUnit.KG.round(catchWeight));
        return batch;
    }

    protected File createFileWithHeaders(ImportModelWithHeader<?> model,
                                         File file) {
        File fileWithHeaders = new File(FileUtils.getTempDirectory(), file.getName());
        String headers = StringUtils.join(model.getHeader(), model.getSeparator());

        try {
            FileUtils.writeLines(fileWithHeaders, Collections.singletonList(headers));
            FileUtils.writeLines(fileWithHeaders, FileUtils.readLines(file), true);
        } catch (IOException e) {
            throw new ApplicationTechnicalException("Could not create file with header " + file, e);
        }

        return fileWithHeaders;
    }

    private Collection<String> getSurveyCodeWhoseCategoryIsMandatory(TuttiProtocol protocol, Caracteristic caracteristic) {

        Objects.requireNonNull(caracteristic);

        Collection<String> surveyCodes;

        if (protocol == null) {

            surveyCodes = new HashSet<>();

        } else {

            Predicate<SpeciesProtocol> predicate = SpeciesProtocols.speciesProtocolWhoseCategoryIsMandatoryPredicate(caracteristic);
            Collection<SpeciesProtocol> speciesProtocols = protocol.getSpecies().stream().filter(predicate).collect(Collectors.toSet());

            surveyCodes = speciesProtocols.stream().map(SpeciesProtocol::getSpeciesSurveyCode).collect(Collectors.toSet());

        }

        return surveyCodes;
    }
}

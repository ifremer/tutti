package fr.ifremer.tutti.service.genericformat;

/*
 * #%L
 * Tutti :: Service
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.entities.data.SampleCategoryModel;
import fr.ifremer.tutti.persistence.entities.protocol.TuttiProtocol;

import java.io.Serializable;
import java.nio.file.Path;

/**
 * Created on 3/28/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.14.3
 */
public class GenericFormatExportRequest implements Serializable {

    private static final long serialVersionUID = 1L;

    private final GenericFormatExportConfiguration exportConfiguration;

    private final GenericFormatArchive archive;

    private final char csvSeparator;

    private final SampleCategoryModel sampleCategoryModel;

    private final TuttiProtocol protocol;

    private final String countryId;

    private final Path attachmentsSourcePath;

    public GenericFormatExportRequest(GenericFormatExportConfiguration exportConfiguration,
                                      GenericFormatArchive archive,
                                      char csvSeparator,
                                      SampleCategoryModel sampleCategoryModel,
                                      TuttiProtocol protocol,
                                      String countryId,
                                      Path attachmentsSourcePath) {
        this.exportConfiguration = exportConfiguration;
        this.archive = archive;
        this.csvSeparator = csvSeparator;
        this.sampleCategoryModel = sampleCategoryModel;
        this.protocol = protocol;
        this.countryId = countryId;
        this.attachmentsSourcePath = attachmentsSourcePath;
    }

    public GenericFormatExportConfiguration getExportConfiguration() {
        return exportConfiguration;
    }

    public GenericFormatArchive getArchive() {
        return archive;
    }

    public char getCsvSeparator() {
        return csvSeparator;
    }

    public SampleCategoryModel getSampleCategoryModel() {
        return sampleCategoryModel;
    }

    public TuttiProtocol getProtocol() {
        return protocol;
    }

    public String getCountryId() {
        return countryId;
    }

    public Path getAttachmentsSourcePath() {
        return attachmentsSourcePath;
    }

}

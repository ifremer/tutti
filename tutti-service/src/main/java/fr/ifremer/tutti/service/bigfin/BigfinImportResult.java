package fr.ifremer.tutti.service.bigfin;

/*
 * #%L
 * Tutti :: Service
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Created on 1/20/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.0.1
 */
public class BigfinImportResult {

    protected final File importFile;

    protected final List<String> errors = new ArrayList<>();

    protected final List<String> warnings = new ArrayList<>();

    protected int nbFrequenciesImported;

    protected int nbFrequenciesDeleted;

    public BigfinImportResult(File importFile) {
        this.importFile = importFile;
    }

    public File getImportFile() {
        return importFile;
    }

    public int getNbFrequenciesImported() {
        return nbFrequenciesImported;
    }

    public int getNbFrequenciesDeleted() {
        return nbFrequenciesDeleted;
    }

    public List<String> getErrors() {
        return errors;
    }

    public List<String> getWarnings() {
        return warnings;
    }

    public void incrementNbFrequenciesImported(int nb) {
        this.nbFrequenciesImported += nb;
    }

    public void incrementNbFrequenciesDeleted(int nb) {
        this.nbFrequenciesDeleted += nb;
    }

    public void addError(String error) {
        errors.add(error);
    }

    public void addWarning(String warning) {
        warnings.add(warning);
    }

    public boolean isDone() {
        return errors.isEmpty();
    }
}

package fr.ifremer.tutti.service.referential.csv;

/*
 * #%L
 * Tutti :: Service
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import fr.ifremer.tutti.persistence.entities.referential.Gear;
import fr.ifremer.tutti.persistence.entities.referential.Gears;

/**
 * @author Kevin Morin (Code Lutin)
 * @since 3.8
 */
public class GearRow {

    public static final String PROPERTY_ID = "id";

    public static final String PROPERTY_NAME = "name";

    public static final String PROPERTY_TO_DELETE = "toDelete";

    public static final String PROPERTY_LABEL = "label";

    public static final String PROPERTY_SCIENTIFIC_GEAR = "scientificGear";

    protected String id;

    protected String name;

    protected String label;

    protected boolean scientificGear;

    protected Boolean toDelete;

    public GearRow() {
        super();
    }

    public GearRow(Gear gear) {
        super();
        Preconditions.checkNotNull(gear);
        setId(gear.getId());
        setName(gear.getName());
        setLabel(gear.getLabel());
        setScientificGear(gear.isScientificGear());
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public boolean isScientificGear() {
        return scientificGear;
    }

    public void setScientificGear(boolean scientificGear) {
        this.scientificGear = scientificGear;
    }

    public Boolean getToDelete() {
        return toDelete;
    }

    public void setToDelete(Boolean toDelete) {
        this.toDelete = toDelete;
    }

    public Gear toEntity() {

        Gear gear = Gears.newGear();
        gear.setId(getId());
        gear.setName(getName());
        gear.setLabel(getLabel());
        gear.setScientificGear(isScientificGear());
        return gear;

    }

    public Integer getIdAsInt() {
        Integer idAsInt = null;
        if (id != null) {
            idAsInt = Integer.valueOf(id);
        }
        return idAsInt;
    }
}

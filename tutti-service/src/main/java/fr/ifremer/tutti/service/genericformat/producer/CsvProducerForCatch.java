package fr.ifremer.tutti.service.genericformat.producer;

/*
 * #%L
 * Tutti :: Service
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.entities.data.BatchContainer;
import fr.ifremer.tutti.persistence.entities.data.SampleCategoryModel;
import fr.ifremer.tutti.persistence.entities.data.SampleCategoryModelEntry;
import fr.ifremer.tutti.persistence.entities.data.SpeciesBatch;
import fr.ifremer.tutti.persistence.entities.data.SpeciesBatchFrequency;
import fr.ifremer.tutti.persistence.entities.referential.Caracteristic;
import fr.ifremer.tutti.service.csv.CsvProducer;
import fr.ifremer.tutti.service.genericformat.GenericFormatExportOperationContext;
import fr.ifremer.tutti.service.genericformat.csv.CatchModel;
import fr.ifremer.tutti.service.genericformat.csv.CatchRow;
import fr.ifremer.tutti.service.genericformat.csv.ExportSampleCategory;
import fr.ifremer.tutti.type.WeightUnit;
import fr.ifremer.tutti.util.Numbers;
import fr.ifremer.tutti.util.Weights;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.Serializable;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

/**
 * Created on 2/6/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.13
 */
public class CsvProducerForCatch extends CsvProducer<CatchRow, CatchModel> {

    /** Logger. */
    private static final Log log = LogFactory.getLog(CsvProducerForCatch.class);

    public CsvProducerForCatch(Path file, CatchModel model) {
        super(file, model);
    }

    public List<CatchRow> getDataToExport(GenericFormatExportOperationContext operationExportContext,
                                          Float speciesCatchRaisingFactor,
                                          Float benthosCatchRaisingFactor) {

        List<CatchRow> rows = new ArrayList<>();

        BatchContainer<SpeciesBatch> rootSpeciesBatch = operationExportContext.getRootSpeciesBatch();

        BatchContainer<SpeciesBatch> rootBenthosBatch = operationExportContext.getRootBenthosBatch();

        Caracteristic weightMeasuredCaracteristic = operationExportContext.getWeightMeasuredCaracteristic();
        String batchWeightUnit = weightMeasuredCaracteristic.getUnit();

        addSpeciesBatches(operationExportContext, rootSpeciesBatch, speciesCatchRaisingFactor, batchWeightUnit, rows);
        addBenthosBatches(operationExportContext, rootBenthosBatch, benthosCatchRaisingFactor, batchWeightUnit, rows);

        // compute final raising factor
        // see http://forge.codelutin.com/issues/4135
        // see http://forge.codelutin.com/issues/5110
        for (CatchRow row : rows) {

            Float raisingFactor = row.isBenthos() ? benthosCatchRaisingFactor : speciesCatchRaisingFactor;

            float finalRaisingFactor = computeFinalRaisingFactor(row, raisingFactor);
            row.setFinalRaisingFactor(finalRaisingFactor);

        }

        return rows;

    }

    public List<CatchRow> getBenthosOnlyDataToExport(GenericFormatExportOperationContext operationExportContext,
                                                     Float benthosCatchRaisingFactor) {

        List<CatchRow> rows = new ArrayList<>();

        BatchContainer<SpeciesBatch> rootBenthosBatch = operationExportContext.getRootBenthosBatch();

        Caracteristic weightMeasuredCaracteristic = operationExportContext.getWeightMeasuredCaracteristic();
        String batchWeightUnit = weightMeasuredCaracteristic.getUnit();

        addBenthosBatches(operationExportContext, rootBenthosBatch, benthosCatchRaisingFactor, batchWeightUnit, rows);

        for (CatchRow row : rows) {

            float finalRaisingFactor = computeFinalRaisingFactor(row, benthosCatchRaisingFactor);
            row.setFinalRaisingFactor(finalRaisingFactor);

        }

        return rows;

    }

    public List<CatchRow> getSpeciesOnlyDataToExport(GenericFormatExportOperationContext operationExportContext,
                                                     Float speciesCatchRaisingFactor) {

        List<CatchRow> rows = new ArrayList<>();

        BatchContainer<SpeciesBatch> rootSpeciesBatch = operationExportContext.getRootSpeciesBatch();

        Caracteristic weightMeasuredCaracteristic = operationExportContext.getWeightMeasuredCaracteristic();
        String batchWeightUnit = weightMeasuredCaracteristic.getUnit();

        addSpeciesBatches(operationExportContext, rootSpeciesBatch, speciesCatchRaisingFactor, batchWeightUnit, rows);

        for (CatchRow row : rows) {

            float finalRaisingFactor = computeFinalRaisingFactor(row, speciesCatchRaisingFactor);
            row.setFinalRaisingFactor(finalRaisingFactor);

        }

        return rows;

    }

    protected void addBenthosBatches(GenericFormatExportOperationContext operationExportContext,
                                     BatchContainer<SpeciesBatch> rootBenthosBatch,
                                     Float benthosCatchRaisingFactor,
                                     String batchWeightUnit,
                                     List<CatchRow> rows) {

        for (SpeciesBatch benthosBatch : rootBenthosBatch.getChildren()) {

            boolean vracBatch = operationExportContext.isVracBatch(benthosBatch);

            CatchRow row = new CatchRow();
            row.setVrac(vracBatch);
            row.setBenthos(true);
            row.setCruise(operationExportContext.getCruise());
            row.setFishingOperation(operationExportContext.getOperation());
            row.setSpecies(benthosBatch.getSpecies());
            row.setSpeciesToConfirm(benthosBatch.isSpeciesToConfirm());
            row.setBatchWeightUnit(batchWeightUnit);

            float raisingFactor = benthosCatchRaisingFactor;

            if (!vracBatch) {

                // for HV, always use a 1.0 raising factor
                raisingFactor = 1.f;
            }

            Float batchWeight = Numbers.getValueOrComputedValue(
                    benthosBatch.getSampleCategoryWeight(),
                    benthosBatch.getSampleCategoryComputedWeight());

            float totalBatchWeight = batchWeight == null ? 0 : raisingFactor * batchWeight;

            addBenthosBatch(operationExportContext,
                            row,
                            rows,
                            benthosBatch,
                            totalBatchWeight);

        }

    }

    protected void addSpeciesBatches(GenericFormatExportOperationContext operationExportContext,
                                     BatchContainer<SpeciesBatch> rootSpeciesBatch,
                                     Float speciesCatchRaisingFactor,
                                     String batchWeightUnit,
                                     List<CatchRow> rows) {

        for (SpeciesBatch speciesBatch : rootSpeciesBatch.getChildren()) {

            boolean vracBatch = operationExportContext.isVracBatch(speciesBatch);

            CatchRow row = new CatchRow();
            row.setVrac(vracBatch);
            row.setBenthos(false);
            row.setCruise(operationExportContext.getCruise());
            row.setFishingOperation(operationExportContext.getOperation());
            row.setSpecies(speciesBatch.getSpecies());
            row.setSpeciesToConfirm(speciesBatch.isSpeciesToConfirm());
            row.setBatchWeightUnit(batchWeightUnit);

            float raisingFactor = speciesCatchRaisingFactor;

            if (!vracBatch) {

                // for HV, always use a 1.0 raising factor
                raisingFactor = 1.f;
            }

            Float batchWeight = Numbers.getValueOrComputedValue(
                    speciesBatch.getSampleCategoryWeight(),
                    speciesBatch.getSampleCategoryComputedWeight());

            float totalBatchWeight = batchWeight == null ? 0 : raisingFactor * batchWeight;

            addSpeciesBatch(operationExportContext,
                            row,
                            rows,
                            speciesBatch,
                            totalBatchWeight);

        }
    }

    protected void addSpeciesBatch(GenericFormatExportOperationContext operationExportContext,
                                   CatchRow currentRow,
                                   List<CatchRow> rows,
                                   SpeciesBatch speciesBatch,
                                   float totalBatchWeight) {

        Integer number = Numbers.getValueOrComputedValue(speciesBatch.getNumber(),
                                                         speciesBatch.getComputedNumber());
        Boolean numberComputed = Numbers.getValueOrComputedValueComputed(speciesBatch.getNumber(),
                                                                         speciesBatch.getComputedNumber());

        Integer rankOrder = speciesBatch.getRankOrder();

        addBatchSampleCategory(operationExportContext.getSampleCategoryModel(),
                               currentRow,
                               speciesBatch.getIdAsInt(),
                               speciesBatch.getSampleCategoryId(),
                               speciesBatch.getSampleCategoryValue(),
                               speciesBatch.getSampleCategoryWeight(),
                               speciesBatch.getSampleCategoryComputedWeight(),
                               speciesBatch.getWeight(),
                               speciesBatch.getComment(),
                               number,
                               numberComputed,
                               rankOrder);

        if (speciesBatch.isChildBatchsEmpty()) {

            // on a leaf, get frequencies

            List<SpeciesBatchFrequency> speciesBatchFrequency =
                    operationExportContext.getAllSpeciesBatchFrequency(speciesBatch.getIdAsInt());

            if (CollectionUtils.isEmpty(speciesBatchFrequency)) {

                // no frequency

                CatchRow row = currentRow.copy();

                // Get reference weight

                Float referenceWeight = Numbers.getValueOrComputedValue(
                        speciesBatch.getWeight(),
                        speciesBatch.getComputedWeight());

                if (referenceWeight == null) {

                    referenceWeight = Numbers.getValueOrComputedValue(
                            speciesBatch.getSampleCategoryWeight(),
                            speciesBatch.getSampleCategoryComputedWeight());
                }
                setRaisingFactor(row, totalBatchWeight, referenceWeight);
                rows.add(row);
            } else {

                // there is some frequencies

                boolean withNoWeightOnFrequencies =
                        speciesBatchFrequency.get(0).getWeight() == null;

                Float referenceWeight = null;

                if (withNoWeightOnFrequencies) {

                    // no weight on frequencies use upper reference weight

                    referenceWeight = Numbers.getValueOrComputedValue(
                            speciesBatch.getWeight(),
                            speciesBatch.getComputedWeight());

                    if (referenceWeight == null) {
                        referenceWeight = Numbers.getValueOrComputedValue(
                                speciesBatch.getSampleCategoryWeight(),
                                speciesBatch.getSampleCategoryComputedWeight());
                    }
                }

                float frequencyWeights = 0f;
                Set<CatchRow> frequencyRows = new LinkedHashSet<>();
                for (SpeciesBatchFrequency batchFrequency : speciesBatchFrequency) {
                    CatchRow row = currentRow.copy();
                    row.setFrequency(batchFrequency);
                    frequencyRows.add(row);
                    rows.add(row);

                    if (!withNoWeightOnFrequencies) {

                        // use concrete frequency weight
                        referenceWeight = batchFrequency.getWeight();
                        frequencyWeights += referenceWeight;
                    }

                    setRaisingFactor(row, totalBatchWeight, referenceWeight);
                }

                if (!withNoWeightOnFrequencies) {
                    float computedFrequencyTotalWeight = frequencyWeights;
                    frequencyRows.forEach(catchRow -> {
                        catchRow.getLastSampleCategoryFilled().setSampleComputedWeight(computedFrequencyTotalWeight);
                        catchRow.setComputedFrequencyTotalWeight(computedFrequencyTotalWeight);
                    });
                }
            }

        } else {

            float sampleComputedWeight = 0f;

            for (SpeciesBatch childBatch : speciesBatch.getChildBatchs()) {
                // always use a copy, otherwise sample categories can be shared by brothers
                CatchRow childRow = currentRow.copy();
                addSpeciesBatch(operationExportContext,
                                childRow,
                                rows,
                                childBatch,
                                totalBatchWeight);

                ExportSampleCategory exportSampleCategory = childRow.getLastSampleCategoryFilled();
                if (exportSampleCategory != null) {
                    Float categoryWeight = Numbers.getValueOrComputedValue(
                            exportSampleCategory.getCategoryWeight(),
                            exportSampleCategory.getComputedWeight());

                    if (categoryWeight == null) {

                        if (log.isWarnEnabled()) {
                            log.warn(String.format("Campagne %s - Trait %s : Il existe une catégorie de poids non renseignée (lot espèce %s)", operationExportContext.getCruise().getName(), operationExportContext.getOperationLabel(), childBatch.getId()));
                        }
                        categoryWeight = 0f;

                    }

                    sampleComputedWeight += categoryWeight;
                }
            }
            currentRow.getLastSampleCategoryFilled().setSampleComputedWeight(sampleComputedWeight);
        }
    }

    protected void addBenthosBatch(GenericFormatExportOperationContext operationExportContext,
                                   CatchRow currentRow,
                                   List<CatchRow> rows,
                                   SpeciesBatch benthosBatch,
                                   float totalBatchWeight) {

        Integer number = Numbers.getValueOrComputedValue(benthosBatch.getNumber(),
                                                         benthosBatch.getComputedNumber());
        Boolean numberComputed = Numbers.getValueOrComputedValueComputed(benthosBatch.getNumber(),
                                                                         benthosBatch.getComputedNumber());

        Integer rankOrder = benthosBatch.getRankOrder();

        addBatchSampleCategory(operationExportContext.getSampleCategoryModel(),
                               currentRow,
                               benthosBatch.getIdAsInt(),
                               benthosBatch.getSampleCategoryId(),
                               benthosBatch.getSampleCategoryValue(),
                               benthosBatch.getSampleCategoryWeight(),
                               benthosBatch.getSampleCategoryComputedWeight(),
                               benthosBatch.getWeight(),
                               benthosBatch.getComment(),
                               number,
                               numberComputed,
                               rankOrder);

        if (benthosBatch.isChildBatchsEmpty()) {

            // on a leaf, get frequencies

            List<SpeciesBatchFrequency> benthosBatchFrequency =
                    operationExportContext.getAllBenthosBatchFrequency(benthosBatch.getIdAsInt());

            if (CollectionUtils.isEmpty(benthosBatchFrequency)) {

                // no frequency

                CatchRow row = currentRow.copy();

                // Get reference weight

                Float referenceWeight = Numbers.getValueOrComputedValue(
                        benthosBatch.getWeight(),
                        benthosBatch.getComputedWeight());

                if (referenceWeight == null) {
                    referenceWeight = Numbers.getValueOrComputedValue(
                            benthosBatch.getSampleCategoryWeight(),
                            benthosBatch.getSampleCategoryComputedWeight());
                }

                setRaisingFactor(row, totalBatchWeight, referenceWeight);
                rows.add(row);
            } else {

                // there is some frequencies

                Float referenceWeight = null;

                boolean withNoWeightOnFrequencies =
                        benthosBatchFrequency.get(0).getWeight() == null;

                if (withNoWeightOnFrequencies) {

                    // no weight on frequencies use upper reference weight

                    referenceWeight = Numbers.getValueOrComputedValue(
                            benthosBatch.getWeight(),
                            benthosBatch.getComputedWeight());

                    if (referenceWeight == null) {
                        referenceWeight = Numbers.getValueOrComputedValue(
                                benthosBatch.getSampleCategoryWeight(),
                                benthosBatch.getSampleCategoryComputedWeight());
                    }
                }

                float frequencyWeights = 0f;
                Set<CatchRow> frequencyRows = new LinkedHashSet<>();
                for (SpeciesBatchFrequency batchFrequency : benthosBatchFrequency) {
                    CatchRow row = currentRow.copy();
                    row.setFrequency(batchFrequency);
                    frequencyRows.add(row);
                    rows.add(row);

                    if (!withNoWeightOnFrequencies) {

                        // use concrete frequency weight
                        referenceWeight = batchFrequency.getWeight();
                        frequencyWeights += referenceWeight;
                    }

                    setRaisingFactor(row, totalBatchWeight, referenceWeight);
                }

                if (!withNoWeightOnFrequencies) {
                    float computedFrequencyTotalWeight = frequencyWeights;
                    frequencyRows.forEach(catchRow -> {
                        catchRow.getLastSampleCategoryFilled().setSampleComputedWeight(computedFrequencyTotalWeight);
                        catchRow.setComputedFrequencyTotalWeight(computedFrequencyTotalWeight);
                    });
                }

            }

        } else {

            float sampleComputedWeight = 0f;

            for (SpeciesBatch childBatch : benthosBatch.getChildBatchs()) {
                // always use a copy, otherwise sample categories can be shared by brothers
                CatchRow childRow = currentRow.copy();
                addBenthosBatch(operationExportContext,
                                childRow,
                                rows,
                                childBatch,
                                totalBatchWeight);

                ExportSampleCategory exportSampleCategory = childRow.getLastSampleCategoryFilled();
                Float categoryWeight = Numbers.getValueOrComputedValue(
                        exportSampleCategory.getCategoryWeight(),
                        exportSampleCategory.getComputedWeight());

                if (categoryWeight == null) {

                    if (log.isWarnEnabled()) {
                        log.warn(String.format("Campagne %s - Trait %s : Il existe une catégorie de poids non renseignée (lot benthos %s)", operationExportContext.getCruise().getName(), operationExportContext.getOperationLabel(), childBatch.getId()));
                    }
                    categoryWeight = 0f;

                }

                sampleComputedWeight += categoryWeight;
            }
            currentRow.getLastSampleCategoryFilled().setSampleComputedWeight(sampleComputedWeight);
        }
    }

    protected float computeFinalRaisingFactor(CatchRow row, Float raisingFactor) {

        float finalRaisingFactor;

        if (row.isVrac()) {

            finalRaisingFactor = raisingFactor;

        } else {

            finalRaisingFactor = 1f;

        }

        for (ExportSampleCategory exportSampleCategory : row.getSampleCategory()) {

            if (exportSampleCategory != null) {
                Float totalWeight = exportSampleCategory.getCategoryWeight();
                Float sampleWeight = Numbers.getValueOrComputedValue(
                        exportSampleCategory.getSampleWeight(),
                        exportSampleCategory.getSampleComputedWeight());

                if (totalWeight != null && sampleWeight != null && sampleWeight != 0f) {


                    float currentRate = totalWeight / sampleWeight;
                    if (WeightUnit.KG.isNotEquals(1.0f, currentRate)) {

                        // the only case which can change the final rate
                        finalRaisingFactor *= currentRate;
                    }
                }
            }
        }

        if (row.isFrequencyWithWeight()) {

            Float frequencyWeight = row.getFrequencyWeight();
            Float computedFrequencyTotalWeight = row.getComputedFrequencyTotalWeight();
            float currentRate = computedFrequencyTotalWeight / frequencyWeight;
            if (WeightUnit.KG.isNotEquals(1.0f, currentRate)) {

                // the only case which can change the final rate
                finalRaisingFactor *= currentRate;
            }

        }
        return finalRaisingFactor;

    }

    protected void setRaisingFactor(CatchRow row, float totalBatchWeight, Float referenceWeight) {
        row.setReferenceWeight(referenceWeight);
        row.setRaisingFactor(referenceWeight == null ? 1.0f : (totalBatchWeight / referenceWeight));
    }

    protected void addBatchSampleCategory(SampleCategoryModel sampleCategoryModel,
                                          CatchRow currentRow,
                                          Integer batchId,
                                          Integer sampleCategoryId,
                                          Serializable sampleCategoryValue,
                                          Float sampleCategoryWeight,
                                          Float sampleCategoryComputedWeight,
                                          Float batchWeight,
                                          String comment,
                                          Integer batchNumber,
                                          Boolean batchNumberComputed,
                                          Integer batchRankOrder) {
        currentRow.setBatchNumber(batchNumber);
        currentRow.setBatchNumberComputed(batchNumberComputed);

        ExportSampleCategory<Serializable> sampleCategory = new ExportSampleCategory<>();
        sampleCategory.setBatchId(batchId);
        SampleCategoryModelEntry categoryById = sampleCategoryModel.getCategoryById(sampleCategoryId);
        sampleCategory.setCategoryDef(categoryById);
        sampleCategory.setCategoryValue(sampleCategoryValue);
        sampleCategory.setCategoryWeight(sampleCategoryWeight);
        sampleCategory.setSampleWeight(batchWeight);
        sampleCategory.setComment(comment);

        sampleCategory.setComputedWeight(sampleCategoryComputedWeight);
        sampleCategory.setWeightOrVolType(CatchModel.WEIGHT_OR_VOL_TYPE);
        sampleCategory.setRankOrder(batchRankOrder);

        currentRow.addSampleCategory(sampleCategory);
    }


}

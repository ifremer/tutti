package fr.ifremer.tutti.service.referential;

/*
 * #%L
 * Tutti :: Service
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Function;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import fr.ifremer.tutti.persistence.entities.referential.TuttiReferentialEntity;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Created on 11/16/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.10
 */
public class ReferentialImportRequest<E extends TuttiReferentialEntity, K> {

    private final List<E> toAdd = Lists.newArrayList();

    private final List<E> toUpdate = Lists.newArrayList();

    private final List<E> toLink = Lists.newArrayList();

    private final List<K> toDelete = Lists.newArrayList();

    private final Map<K, E> existingEntitiesById;

    private final Set<String> existingNaturalIds;

    private final Set<K> newIds;

    private final Set<String> newNaturalIds;

    private final Function<E, K> entityToIdFunction;

    public ReferentialImportRequest(List<E> existingEntities, Function<E, K> entityToIdFunction, Function<E, String> naturalIdFunction) {

        this.entityToIdFunction = entityToIdFunction;
        this.existingEntitiesById = Maps.uniqueIndex(existingEntities, entityToIdFunction);
        this.existingNaturalIds = Sets.newHashSet(existingEntities.stream().map(naturalIdFunction::apply).collect(Collectors.toList()));
        this.newIds = new HashSet<>();
        this.newNaturalIds = new HashSet<>();
    }

    public void addEntityToAdd(E entityToAdd) {
        toAdd.add(entityToAdd);
        K id = entityToIdFunction.apply(entityToAdd);
        newIds.add(id);
    }

    public void addEntityToUpdate(E entityToUpdate) {
        toUpdate.add(entityToUpdate);
    }

    public void addEntityToLink(E entityToLink) {
        toLink.add(entityToLink);
    }

    public void addIdToDelete(K entityToDelete) {
        toDelete.add(entityToDelete);
    }

    public boolean withEntitiesToAdd() {
        return !toAdd.isEmpty();
    }

    public boolean withEntitiesToUpdate() {
        return !toUpdate.isEmpty();
    }

    public boolean withEntitiesToLink() {
        return !toLink.isEmpty();
    }

    public boolean withEntitiesToDelete() {
        return !toDelete.isEmpty();
    }

    public List<K> getIdsToDelete() {
        return Lists.newArrayList(toDelete);
    }

    public List<E> getEntitiesToAdd() {
        return Lists.newArrayList(toAdd);
    }

    public List<E> getEntitiesToUpdate() {
        return Lists.newArrayList(toUpdate);
    }

    public List<E> getEntitiesToLink() {
        return Lists.newArrayList(toLink);
    }

    public E getExistingEntityById(K id) {
        return existingEntitiesById.get(id);
    }

    public boolean addExistingNaturalId(String naturalId) {
        boolean added = existingNaturalIds.add(naturalId);
        if (added) {
            newNaturalIds.add(naturalId);
        }
        return added;
    }

    public boolean isNaturalIdAlreadyAdded(String naturalId) {
        return newNaturalIds.contains(naturalId);
    }

    public boolean isIdAlreadyAdded(K id) {
        return newIds.contains(id);
    }

    public void removeExistingNaturalId(String naturalId) {
        existingNaturalIds.remove(naturalId);
    }

    public Function<E, K> getEntityToIdFunction() {
        return entityToIdFunction;
    }

}

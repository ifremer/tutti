package fr.ifremer.tutti.service.csv;

/*
 * #%L
 * Tutti :: Service
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.entities.TuttiEntity;
import org.apache.commons.lang3.StringUtils;
import org.nuiton.csv.ImportRuntimeException;
import org.nuiton.csv.ValueParserFormatter;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static org.nuiton.i18n.I18n.t;

/**
 * @param <E>
 * @author Kevin Morin - kmorin@codelutin.com
 * @since 3.10
 */
public class ForeignKeyListParserFormatter<E extends TuttiEntity> implements ValueParserFormatter<List<E>> {

    public static final String LIST_SEPARATOR = ",";

    protected final String propertyName;

    protected final Class<E> entityType;

    protected final Map<String, E> universe;

    public ForeignKeyListParserFormatter(Class<E> entityType, String propertyName, Map<String, E> universe) {
        this.entityType = entityType;
        this.propertyName = propertyName;
        this.universe = universe;
    }

    @Override
    public List<E> parse(String value) throws ParseException {
        List<E> result = new ArrayList<>();
        if (StringUtils.isNotBlank(value)) {

            String[] ids = value.split(LIST_SEPARATOR);
            for (String id : ids) {
                // get entity from universe
                E entity = universe.get(id);

                if (entity == null) {

                    // can not find entity this is a big problem for us...
                    throw new ImportRuntimeException(t("tutti.service.csv.parse.entityNotFound", entityType.getSimpleName(), propertyName, id));
                }

                result.add(entity);
            }
        }
        return result;
    }

    @Override
    public String format(List<E> e) {
        List<String> ids = new ArrayList<>();
        for (E entity : e) {
            if (entity != null) {
                ids.add(entity.getId());
            }
        }
        return StringUtils.join(ids, LIST_SEPARATOR);
    }
}

package fr.ifremer.tutti.service.catches.multipost.csv;

/*
 * #%L
 * Tutti :: Service
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

/**
 * Created on 11/15/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.9
 */
public class CatchBatchRow extends AbstractFishingOperationRow {

    public static final String CATCH_TOTAL_WEIGHT = "catchTotalWeight";

    public static final String CATCH_TOTAL_REJECTED_WEIGHT = "catchTotalRejectedWeight";

    public static final String BENTHOS_TOTAL_SORTED_WEIGHT = "benthosTotalSortedWeight";

    public static final String SPECIES_TOTAL_SORTED_WEIGHT = "speciesTotalSortedWeight";

    public static final String MARINE_LITTER_TOTAL_WEIGHT = "marineLitterTotalWeight";

    protected Float benthosTotalSortedWeight;

    protected Float speciesTotalSortedWeight;

    protected Float catchTotalWeight;

    protected Float catchTotalRejectedWeight;

    protected Float marineLitterTotalWeight;

    public Float getBenthosTotalSortedWeight() {
        return benthosTotalSortedWeight;
    }

    public void setBenthosTotalSortedWeight(Float benthosTotalSortedWeight) {
        this.benthosTotalSortedWeight = benthosTotalSortedWeight;
    }

    public Float getSpeciesTotalSortedWeight() {
        return speciesTotalSortedWeight;
    }

    public void setSpeciesTotalSortedWeight(Float speciesTotalSortedWeight) {
        this.speciesTotalSortedWeight = speciesTotalSortedWeight;
    }

    public Float getCatchTotalWeight() {
        return catchTotalWeight;
    }

    public void setCatchTotalWeight(Float catchTotalWeight) {
        this.catchTotalWeight = catchTotalWeight;
    }

    public Float getCatchTotalRejectedWeight() {
        return catchTotalRejectedWeight;
    }

    public void setCatchTotalRejectedWeight(Float catchTotalRejectedWeight) {
        this.catchTotalRejectedWeight = catchTotalRejectedWeight;
    }

    public Float getMarineLitterTotalWeight() {
        return marineLitterTotalWeight;
    }

    public void setMarineLitterTotalWeight(Float marineLitterTotalWeight) {
        this.marineLitterTotalWeight = marineLitterTotalWeight;
    }
}

package fr.ifremer.tutti.service.pupitri.report;

/*
 * #%L
 * Tutti :: Service
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.service.pupitri.PupitriSignContext;

/**
 * Created on 11/22/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.11
 */
public class PupitriImportReportRow {

    private boolean sorted;

    private String speciesCode;

    private String speciesName;

    private String speciesVernucalCode;

    private String sign;

    private float sortedWeight;

    private int nbBox;

    private int nbSmallBox;

    private int nbBigBox;

    public static PupitriImportReportRow newRow(String code,
                                                String name,
                                                String vernacularCode,
                                                boolean sorted,
                                                PupitriSignContext signContext) {
        PupitriImportReportRow result = new PupitriImportReportRow();
        result.speciesCode = code;
        result.speciesName = name;
        result.speciesVernucalCode = vernacularCode;
        result.sorted = sorted;
        result.sign = signContext.getSigns().getSign();
        result.sortedWeight = signContext.getWeight();
        result.nbBox = signContext.getNbBox();
        result.nbSmallBox = signContext.getNbSmallBox();
        result.nbBigBox = signContext.getNbBigBox();
        return result;
    }

    public String getSpeciesCode() {
        return speciesCode;
    }

    public String getSpeciesName() {
        return speciesName;
    }

    public String getSpeciesVernucalCode() {
        return speciesVernucalCode;
    }

    public boolean isSorted() {
        return sorted;
    }

    public float getSortedWeight() {
        return sortedWeight;
    }

    public String getSign() {
        return sign;
    }

    public int getNbBox() {
        return nbBox;
    }

    public int getNbSmallBox() {
        return nbSmallBox;
    }

    public int getNbBigBox() {
        return nbBigBox;
    }
}

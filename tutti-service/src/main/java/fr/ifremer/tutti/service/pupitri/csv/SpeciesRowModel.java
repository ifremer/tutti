package fr.ifremer.tutti.service.pupitri.csv;

/*
 * #%L
 * Tutti :: Service
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.nuiton.csv.ext.AbstractExportModel;

/**
 * @author Kevin Morin - kmorin@codelutin.com
 * @since 1.2
 */
public class SpeciesRowModel extends AbstractExportModel<SpeciesRow> {

    public SpeciesRowModel(char separator) {
        super(separator);

        newColumnForExport(SpeciesRow.PROPERTY_CODE_FIRST_PART);
        newColumnForExport(SpeciesRow.PROPERTY_CODE_SECOND_PART);
        newColumnForExport(SpeciesRow.PROPERTY_SCIENTIFIC_NAME);
    }


}

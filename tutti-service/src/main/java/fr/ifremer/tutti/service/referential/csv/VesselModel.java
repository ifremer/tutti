package fr.ifremer.tutti.service.referential.csv;

/*
 * #%L
 * Tutti :: Service
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.adagio.core.dao.technical.hibernate.TemporaryDataHelper;
import fr.ifremer.tutti.persistence.entities.referential.Vessel;
import fr.ifremer.tutti.persistence.entities.referential.Vessels;
import fr.ifremer.tutti.service.csv.AbstractTuttiImportExportModel;
import fr.ifremer.tutti.service.csv.TuttiCsvUtil;
import org.nuiton.csv.Common;

import static org.nuiton.i18n.I18n.t;

/**
 * Model to import / export {@link Vessel} in csv format.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.0
 */
public class VesselModel extends AbstractTuttiImportExportModel<VesselRow> {

    public static VesselModel forExport(char separator) {

        VesselModel exportModel = new VesselModel(separator);
        exportModel.forExport();
        return exportModel;

    }

    public static VesselModel forImport(char separator) {

        VesselModel importModel = new VesselModel(separator);
        importModel.forImport();
        return importModel;

    }

    @Override
    public VesselRow newEmptyInstance() {
        return new VesselRow();
    }

    protected VesselModel(char separator) {
        super(separator);
    }

    protected void forImport() {

        newMandatoryColumn(GearRow.PROPERTY_ID, new TemporaryReferentialEntityIdParser(
                t("tutti.service.referential.import.vessel.codePrefixMissing.error", TemporaryDataHelper.TEMPORARY_NAME_PREFIX)){

            @Override
            protected boolean isTemporaryId(String parse) {
                return Vessels.isTemporaryId(parse);
            }
        });
        newMandatoryColumn(VesselRow.PROPERTY_NAME);
        newMandatoryColumn(VesselRow.PROPERTY_INTERNATIONAL_REGISTRATION_CODE);
        newMandatoryColumn(VesselRow.PROPERTY_SCIENTIFIC_VESSEL, Common.PRIMITIVE_BOOLEAN);
        newMandatoryColumn(VesselRow.PROPERTY_TO_DELETE, TuttiCsvUtil.BOOLEAN);

    }

    protected void forExport() {

        newColumnForExport(VesselRow.PROPERTY_ID);
        newColumnForExport(VesselRow.PROPERTY_NAME);
        newColumnForExport(VesselRow.PROPERTY_INTERNATIONAL_REGISTRATION_CODE);
        newColumnForExport(VesselRow.PROPERTY_SCIENTIFIC_VESSEL, Common.PRIMITIVE_BOOLEAN);
        newColumnForExport(VesselRow.PROPERTY_TO_DELETE);

    }

}

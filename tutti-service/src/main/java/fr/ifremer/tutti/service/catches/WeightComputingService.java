package fr.ifremer.tutti.service.catches;

/*
 * #%L
 * Tutti :: Service
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.entities.data.BatchContainer;
import fr.ifremer.tutti.persistence.entities.data.CatchBatch;
import fr.ifremer.tutti.persistence.entities.data.MarineLitterBatch;
import fr.ifremer.tutti.persistence.entities.data.SampleCategoryModelEntry;
import fr.ifremer.tutti.persistence.entities.data.SpeciesBatch;
import fr.ifremer.tutti.persistence.entities.data.SpeciesBatchFrequency;
import fr.ifremer.tutti.persistence.entities.referential.Species;
import fr.ifremer.tutti.service.AbstractTuttiService;
import fr.ifremer.tutti.service.DecoratorService;
import fr.ifremer.tutti.service.PersistenceService;
import fr.ifremer.tutti.service.TuttiServiceContext;
import fr.ifremer.tutti.service.ValidationService;
import fr.ifremer.tutti.type.WeightUnit;
import fr.ifremer.tutti.util.Numbers;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.nuiton.decorator.Decorator;
import org.nuiton.validator.NuitonValidatorResult;

import java.io.Serializable;
import java.util.List;

/**
 * @author Kevin Morin - kmorin@codelutin.com
 * @since 1.3
 */
public class WeightComputingService extends AbstractTuttiService {

    protected PersistenceService persistenceService;

    protected ValidationService validationService;

    protected DecoratorService decoratorService;

    private Decorator<Species> speciesDecorator;

    private int currentSpeciesRowIndex;
    private int currentBenthosRowIndex;

    @Override
    public void setServiceContext(TuttiServiceContext context) {
        super.setServiceContext(context);
        persistenceService = getService(PersistenceService.class);
        validationService = getService(ValidationService.class);
        decoratorService = getService(DecoratorService.class);
    }

    /**
     * Compute the weights of the catch batch (not the ones of the species, benthos nor marine litter batches)
     *
     * @param catchBatch            the catch batch with the weights to compute
     * @param rootSpeciesBatch      the species batches with already computed weights
     * @param rootBenthosBatch      the benthos batches with already computed weights
     * @param rootMarineLitterBatch the marine litter batches with already computed weights
     */
    public void computeCatchBatchWeights(CatchBatch catchBatch,
                                         BatchContainer<SpeciesBatch> rootSpeciesBatch,
                                         BatchContainer<SpeciesBatch> rootBenthosBatch,
                                         BatchContainer<MarineLitterBatch> rootMarineLitterBatch) {

        // Species
        Float speciesTotalComputedSortedWeight = 0f;
        Float speciesTotalComputedUnsortedWeight = 0f;

        if (rootSpeciesBatch != null) {
            Pair<Float, Float> pair = computeTotalComputedSortedAndUnsortedWeights(rootSpeciesBatch);
            speciesTotalComputedSortedWeight = pair.getLeft();
            speciesTotalComputedUnsortedWeight = pair.getRight();
        }

        Float speciesInertWeight = catchBatch.getSpeciesTotalInertWeight();
        if (speciesInertWeight != null) {
            speciesTotalComputedSortedWeight += speciesInertWeight;
        } else {
            catchBatch.setSpeciesTotalInertComputedWeight(0f);
        }

        Float speciesLivingNotItemizedWeight = catchBatch.getSpeciesTotalLivingNotItemizedWeight();
        if (speciesLivingNotItemizedWeight != null) {
            speciesTotalComputedSortedWeight += speciesLivingNotItemizedWeight;
        } else {
            catchBatch.setSpeciesTotalLivingNotItemizedComputedWeight(0f);
        }

        catchBatch.setSpeciesTotalSampleSortedComputedWeight(WeightUnit.KG.round(speciesTotalComputedSortedWeight));

        Float speciesTotalSortedWeight = catchBatch.getSpeciesTotalSortedWeight();
        if (speciesTotalSortedWeight == null) {
            speciesTotalSortedWeight = speciesTotalComputedSortedWeight;
            catchBatch.setSpeciesTotalSortedComputedWeight(WeightUnit.KG.round(speciesTotalSortedWeight));

        } else if (WeightUnit.KG.isSmallerThan(speciesTotalSortedWeight, speciesTotalComputedSortedWeight)) {

            throw SpeciesWeightComputingException.forIncoherentTotalSorted(getSpeciesWeightUnit(),
                                                                           speciesTotalSortedWeight,
                                                                           speciesTotalComputedSortedWeight);

        }
        catchBatch.setSpeciesTotalUnsortedComputedWeight(WeightUnit.KG.round(speciesTotalComputedUnsortedWeight));

        if (speciesTotalSortedWeight == null) {
            speciesTotalSortedWeight = catchBatch.getSpeciesTotalSortedComputedWeight();
        }

        // Benthos
        Float benthosTotalComputedSortedWeight = 0f;
        Float benthosTotalComputedUnsortedWeight = 0f;

        if (rootBenthosBatch != null) {

            Pair<Float, Float> pair = computeTotalComputedSortedAndUnsortedWeights(rootBenthosBatch);
            benthosTotalComputedSortedWeight = pair.getLeft();
            benthosTotalComputedUnsortedWeight = pair.getRight();

        }

        Float benthosInertWeight = catchBatch.getBenthosTotalInertWeight();
        if (benthosInertWeight != null) {
            benthosTotalComputedSortedWeight += benthosInertWeight;
        } else {
            catchBatch.setBenthosTotalInertComputedWeight(0f);
        }

        Float benthosLivingNotItemizedWeight = catchBatch.getBenthosTotalLivingNotItemizedWeight();
        if (benthosLivingNotItemizedWeight != null) {
            benthosTotalComputedSortedWeight += benthosLivingNotItemizedWeight;
        } else {
            catchBatch.setBenthosTotalLivingNotItemizedComputedWeight(0f);
        }

        catchBatch.setBenthosTotalSampleSortedComputedWeight(WeightUnit.KG.round(benthosTotalComputedSortedWeight));

        Float benthosTotalSortedWeight = catchBatch.getBenthosTotalSortedWeight();
        if (benthosTotalSortedWeight == null) {
            benthosTotalSortedWeight = benthosTotalComputedSortedWeight;
            catchBatch.setBenthosTotalSortedComputedWeight(WeightUnit.KG.round(benthosTotalSortedWeight));

        } else if (WeightUnit.KG.isSmallerThan(benthosTotalSortedWeight, benthosTotalComputedSortedWeight)) {

            throw BenthosWeightComputingException.forIncoherentTotalSorted(
                    getBenthosWeightUnit(),
                    benthosTotalSortedWeight,
                    benthosTotalComputedSortedWeight);

        }
        catchBatch.setBenthosTotalUnsortedComputedWeight(WeightUnit.KG.round(benthosTotalComputedUnsortedWeight));

        if (benthosTotalSortedWeight == null) {
            benthosTotalSortedWeight = catchBatch.getBenthosTotalSortedComputedWeight();
        }

        // Marine litter
        Float marineLitterTotalComputedWeight = 0f;

        if (rootMarineLitterBatch != null) {

            for (MarineLitterBatch row : rootMarineLitterBatch.getChildren()) {
                Float rowWeight = row.getWeight();
                if (rowWeight == null) {
                    marineLitterTotalComputedWeight = null;
                    break;
                }
                marineLitterTotalComputedWeight += rowWeight;
            }

        }
        if (marineLitterTotalComputedWeight != null) {

            marineLitterTotalComputedWeight = WeightUnit.KG.round(marineLitterTotalComputedWeight);
            catchBatch.setMarineLitterTotalComputedWeight(marineLitterTotalComputedWeight);

        }

        Float marineLitterTotalWeight = catchBatch.getMarineLitterTotalWeight();
        if (marineLitterTotalWeight != null
                && marineLitterTotalComputedWeight != null
                && WeightUnit.KG.isSmallerThan(marineLitterTotalWeight, marineLitterTotalComputedWeight)) {

            throw MarineLitterWeightComputingException.forIncoherentTotal(getMarineLitterWeightUnit(),
                                                                          marineLitterTotalWeight,
                                                                          marineLitterTotalComputedWeight);

        }
        // nothing to do with the marine litter weight, it is an isolated weight

        // Catch
        Float totalUnsortedWeight = catchBatch.getSpeciesTotalUnsortedComputedWeight() + catchBatch.getBenthosTotalUnsortedComputedWeight();

        Float totalSortedSortedWeight = speciesTotalSortedWeight + benthosTotalSortedWeight;

        catchBatch.setCatchTotalSortedSortedComputedWeight(WeightUnit.KG.round(totalSortedSortedWeight));
        catchBatch.setCatchTotalUnsortedComputedWeight(WeightUnit.KG.round(totalUnsortedWeight));

        Float totalWeight = catchBatch.getCatchTotalWeight();
        Float rejectedWeight = catchBatch.getCatchTotalRejectedWeight();

        if (rejectedWeight == null && totalWeight != null) {

            Float computedTotalWeight = WeightUnit.KG.round(totalUnsortedWeight + totalSortedSortedWeight);

            if (WeightUnit.KG.isNotEquals(totalWeight, computedTotalWeight)) {

                throw CatchWeightComputingException.forIncoherentTotal(getCatchWeightUnit(),
                                                                       totalWeight,
                                                                       computedTotalWeight);

            }

            //FIXME voir avec Vincent si on calcule le poids rejeté
            catchBatch.setCatchTotalRejectedComputedWeight(WeightUnit.KG.round(totalWeight - totalUnsortedWeight - totalSortedSortedWeight));

        } else if (totalWeight == null) {
            if (rejectedWeight == null) {
                rejectedWeight = 0f;
                catchBatch.setCatchTotalRejectedComputedWeight(0f);
            }
            catchBatch.setCatchTotalComputedWeight(WeightUnit.KG.round(totalUnsortedWeight + totalSortedSortedWeight + rejectedWeight));

        } else {

            Float computedTotalWeight = WeightUnit.KG.round(totalUnsortedWeight + totalSortedSortedWeight + rejectedWeight);
            if (WeightUnit.KG.isNotEquals(totalWeight, computedTotalWeight)) {

                throw CatchWeightComputingException.forIncoherentTotalWithRejected(getCatchWeightUnit(),
                                                                                   totalWeight,
                                                                                   computedTotalWeight);
            }

        }

        Float totalSortedWeight = totalSortedSortedWeight;
        if (rejectedWeight != null) {
            totalSortedWeight += rejectedWeight;
        }
        catchBatch.setCatchTotalSortedComputedWeight(WeightUnit.KG.round(totalSortedWeight));

        Float catchRate;

        if (WeightUnit.KG.isZero(totalSortedSortedWeight)) {
            catchRate = 0f;
        } else {
            catchRate = totalSortedWeight / totalSortedSortedWeight;
        }

        Float speciesTotalWeight = speciesTotalSortedWeight * catchRate + speciesTotalComputedUnsortedWeight;
        catchBatch.setSpeciesTotalComputedWeight(WeightUnit.KG.round(speciesTotalWeight));

        Float benthosTotalWeight = benthosTotalSortedWeight * catchRate + benthosTotalComputedUnsortedWeight;
        catchBatch.setBenthosTotalComputedWeight(WeightUnit.KG.round(benthosTotalWeight));
    }

    public BatchContainer<SpeciesBatch> getComputedSpeciesBatches(Integer operationId) {

        BatchContainer<SpeciesBatch> rootSpeciesBatch = null;

        if (persistenceService.isFishingOperationWithCatchBatch(operationId)) {
            rootSpeciesBatch = persistenceService.getRootSpeciesBatch(operationId, false);

            currentSpeciesRowIndex = 0;
            if (rootSpeciesBatch != null) {
                rootSpeciesBatch.getChildren().forEach(this::computeSpeciesBatch);
            }
        }

        return rootSpeciesBatch;

    }

    public Float computeSpeciesBatch(SpeciesBatch batch) {

        Float result = null;
        int thisIndex = currentSpeciesRowIndex++;
        Float categoryWeight = batch.getSampleCategoryWeight();
        Float rowWeight = batch.getWeight();

        NuitonValidatorResult validation = validationService.validateEditSpeciesBatch(batch);
        if (!validation.isValid()) {

            List<String> messages = validation.getErrorMessages(SpeciesBatch.PROPERTY_WEIGHT);
            throw SpeciesWeightComputingException.forValidationMessage(
                    getSpeciesWeightUnit(),
                    messages.get(0),
                    decorateSpecies(batch.getSpecies()),
                    getCategoryLabel(batch.getSampleCategoryId()),
                    decorateCategoryValue(batch.getSampleCategoryValue()),
                    batch.getWeight(),
                    batch.getSampleCategoryWeight(),
                    thisIndex);
        }

        List<SpeciesBatch> children = batch.getChildBatchs();
        // if the row is not a leaf
        if (batch.sizeChildBatchs() > 0) {
            Float sum = 0f;
            // make the sum of the children weights
            for (SpeciesBatch child : children) {
                Float weight = computeSpeciesBatch(child);
                if (weight == null) {
                    sum = null;
                    break;
                }
                sum += weight;
            }
            sum = WeightUnit.KG.round(sum);

            if (sum != null) {
                if (categoryWeight == null) {
                    batch.setSampleCategoryComputedWeight(sum);

                } else if (WeightUnit.KG.isSmallerThan(categoryWeight, sum)) {

                    throw SpeciesWeightComputingException.forIncoherentParentCategoryWeight(
                            getSpeciesWeightUnit(),
                            decorateSpecies(batch.getSpecies()),
                            getCategoryLabel(batch.getSampleCategoryId()),
                            decorateCategoryValue(batch.getSampleCategoryValue()),
                            categoryWeight,
                            sum,
                            thisIndex);

                } else {
                    sum = categoryWeight;
                }
                result = sum;
            }

        } else {// the row is a leaf

            batch.setComputedWeight(null);

            List<SpeciesBatchFrequency> frequencies = persistenceService.getAllSpeciesBatchFrequency(batch.getIdAsInt());

            if (CollectionUtils.isNotEmpty(frequencies)) {
                // if there are frequencies, then compute their weight
                Float frequencyWeight = sumFrequenciesWeight(frequencies);

                if (categoryWeight == null && rowWeight != null) {
//                    throw new TuttiBusinessException(t("tutti.service.operations.computeWeights.error.incoherentRowWeightCategory"));

                } else if (rowWeight != null && frequencyWeight != null && WeightUnit.KG.isNotEquals(rowWeight, frequencyWeight)) {

                    throw SpeciesWeightComputingException.forIncoherentRowWeightFrequency(getSpeciesWeightUnit(),
                                                                                          decorateSpecies(batch.getSpecies()),
                                                                                          getCategoryLabel(batch.getSampleCategoryId()),
                                                                                          decorateCategoryValue(batch.getSampleCategoryValue()),
                                                                                          frequencyWeight,
                                                                                          rowWeight,
                                                                                          thisIndex);

                } else if (categoryWeight == null && frequencyWeight != null) {
                    // if the category weight is null and the frequencies have a weight,
                    // then this weight is the result
                    batch.setSampleCategoryComputedWeight(frequencyWeight);
                    result = frequencyWeight;

                } else if (frequencyWeight != null && WeightUnit.KG.isNotEquals(frequencyWeight, categoryWeight)) {

                    // if the weight of the frequencies is different from the category
                    // weight, then set the weight of the sample
                    if (WeightUnit.KG.isGreaterThan(frequencyWeight, categoryWeight)) {

                        throw SpeciesWeightComputingException.forIncoherentCategoryWeight(getSpeciesWeightUnit(),
                                                                                          decorateSpecies(batch.getSpecies()),
                                                                                          getCategoryLabel(batch.getSampleCategoryId()),
                                                                                          decorateCategoryValue(batch.getSampleCategoryValue()),
                                                                                          frequencyWeight,
                                                                                          categoryWeight,
                                                                                          thisIndex);

                    } else if (rowWeight == null) {
                        batch.setComputedWeight(frequencyWeight);

                    }
                    result = categoryWeight;

                } else {
                    result = categoryWeight;
                }

                // compute number from frequencies
                Integer frequencyNumber = sumFrequenciesNumber(frequencies);
                batch.setComputedNumber(frequencyNumber);

            } else {
                result = categoryWeight;
            }
        }
        if (WeightUnit.KG.isNullOrZero(result)) {

            throw SpeciesWeightComputingException.forNoWeight(decorateSpecies(batch.getSpecies()),
                                                              getCategoryLabel(batch.getSampleCategoryId()),
                                                              decorateCategoryValue(batch.getSampleCategoryValue()),
                                                              thisIndex);
        }

        return WeightUnit.KG.round(result);
    }

    public BatchContainer<SpeciesBatch> getComputedBenthosBatches(Integer operationId) {

        BatchContainer<SpeciesBatch> rootBenthosBatch = null;

        if (persistenceService.isFishingOperationWithCatchBatch(operationId)) {

            rootBenthosBatch = persistenceService.getRootBenthosBatch(operationId, false);

            currentBenthosRowIndex = 0;
            if (rootBenthosBatch != null) {
                rootBenthosBatch.getChildren().forEach(this::computeBenthosBatch);
            }
        }

        return rootBenthosBatch;

    }

    public Float computeBenthosBatch(SpeciesBatch batch) {

        Float result = null;
        int thisIndex = currentBenthosRowIndex++;
        Float categoryWeight = batch.getSampleCategoryWeight();
        Float rowWeight = batch.getWeight();

        NuitonValidatorResult validation = validationService.validateEditBenthosBatch(batch);
        if (!validation.isValid()) {
            List<String> messages = validation.getErrorMessages(SpeciesBatch.PROPERTY_WEIGHT);

            throw BenthosWeightComputingException.forValidationMessage(getBenthosWeightUnit(),
                                                                       messages.get(0),
                                                                       decorateSpecies(batch.getSpecies()),
                                                                       getCategoryLabel(batch.getSampleCategoryId()),
                                                                       decorateCategoryValue(batch.getSampleCategoryValue()),
                                                                       batch.getWeight(),
                                                                       batch.getSampleCategoryWeight(),
                                                                       thisIndex);
        }

        List<SpeciesBatch> children = batch.getChildBatchs();
        // if the row is not a leaf
        if (!batch.isChildBatchsEmpty()) {
            Float sum = 0f;
            // make the sum of the children weights
            for (SpeciesBatch child : children) {
                Float weight = computeBenthosBatch(child);
                if (weight == null) {
                    sum = null;
                    break;
                }
                sum += weight;
            }
            sum = WeightUnit.KG.round(sum);

            if (sum != null) {
                if (categoryWeight == null) {
                    batch.setSampleCategoryComputedWeight(sum);

                } else if (WeightUnit.KG.isSmallerThan(categoryWeight, sum)) {

                    throw BenthosWeightComputingException.forIncoherentParentCategoryWeight(getBenthosWeightUnit(),
                                                                                            decorateSpecies(batch.getSpecies()),
                                                                                            getCategoryLabel(batch.getSampleCategoryId()),
                                                                                            decorateCategoryValue(batch.getSampleCategoryValue()),
                                                                                            categoryWeight,
                                                                                            sum,
                                                                                            thisIndex);

                } else {
                    sum = categoryWeight;
                }
                result = sum;

            }

        } else {// the row is a leaf

            batch.setComputedWeight(null);

            List<SpeciesBatchFrequency> frequencies = persistenceService.getAllBenthosBatchFrequency(batch.getIdAsInt());

            if (CollectionUtils.isNotEmpty(frequencies)) {
                // if there are frequencies, then compute their weight
                Float frequencyWeight = sumFrequenciesWeight(frequencies);

                if (categoryWeight == null && rowWeight != null) {
//                    throw new TuttiBusinessException(t("tutti.service.operations.computeWeights.error.incoherentRowWeightCategory"));

                } else if (rowWeight != null && frequencyWeight != null && WeightUnit.KG.isNotEquals(rowWeight, frequencyWeight)) {

                    throw BenthosWeightComputingException.forIncoherentRowWeightFrequency(getBenthosWeightUnit(),
                                                                                          decorateSpecies(batch.getSpecies()),
                                                                                          getCategoryLabel(batch.getSampleCategoryId()),
                                                                                          decorateCategoryValue(batch.getSampleCategoryValue()),
                                                                                          rowWeight,
                                                                                          categoryWeight,
                                                                                          thisIndex);

                } else if (categoryWeight == null && frequencyWeight != null) {
                    // if the category weight is null and the frequencies have a weight,
                    // then this weight is the result
                    batch.setSampleCategoryComputedWeight(frequencyWeight);
                    result = frequencyWeight;

                } else if (frequencyWeight != null && WeightUnit.KG.isNotEquals(frequencyWeight, categoryWeight)) {

                    // if the weight of the frequencies is different from the category
                    // weight, then set the weight of the sample
                    if (WeightUnit.KG.isGreaterThan(frequencyWeight, categoryWeight)) {

                        throw BenthosWeightComputingException.forIncoherentCategoryWeight(getBenthosWeightUnit(),
                                                                                          decorateSpecies(batch.getSpecies()),
                                                                                          getCategoryLabel(batch.getSampleCategoryId()),
                                                                                          decorateCategoryValue(batch.getSampleCategoryValue()),
                                                                                          frequencyWeight,
                                                                                          categoryWeight,
                                                                                          thisIndex);

                    } else if (rowWeight == null) {
                        batch.setComputedWeight(frequencyWeight);

                    }
                    result = categoryWeight;

                } else {
                    result = categoryWeight;
                }

                // compute number from frequencies
                Integer frequencyNumber = sumFrequenciesNumber(frequencies);
                batch.setComputedNumber(frequencyNumber);
            } else {
                result = categoryWeight;
            }
        }
        if (WeightUnit.KG.isNullOrZero(result)) {

            throw BenthosWeightComputingException.forNoWeight(decorateSpecies(batch.getSpecies()),
                                                              getCategoryLabel(batch.getSampleCategoryId()),
                                                              decorateCategoryValue(batch.getSampleCategoryValue()),
                                                              thisIndex);
        }

        return WeightUnit.KG.round(result);

    }

    public BatchContainer<MarineLitterBatch> getComputedMarineLitterBatches(Integer fishingOperationId, Float marineLitterWeight) {

        BatchContainer<MarineLitterBatch> rootMarineLitterBatch = persistenceService.getRootMarineLitterBatch(fishingOperationId);

        if (rootMarineLitterBatch != null) {
            boolean checkWeight = marineLitterWeight == null;

            List<MarineLitterBatch> children = rootMarineLitterBatch.getChildren();
            for (int i = 0; i < children.size(); i++) {
                MarineLitterBatch batch = children.get(i);

                if (checkWeight && WeightUnit.KG.isNullOrZero(batch.getWeight())) {
                    throw MarineLitterWeightComputingException.forNoWeight(i);
                }
            }
        }

        return rootMarineLitterBatch;

    }

    private Pair<Float, Float> computeTotalComputedSortedAndUnsortedWeights(BatchContainer<SpeciesBatch> rootSpeciesBatch) {

        Float speciesTotalComputedSortedWeight = 0f;
        Float speciesTotalComputedUnsortedWeight = 0f;

        for (SpeciesBatch speciesBatch : rootSpeciesBatch.getChildren()) {
            Float weight = Numbers.getValueOrComputedValue(speciesBatch.getSampleCategoryWeight(),
                                                           speciesBatch.getSampleCategoryComputedWeight());
            if (weight == null) {
                break;
            }

            if (persistenceService.isVracBatch(speciesBatch)) {
                speciesTotalComputedSortedWeight += weight;
            } else {
                speciesTotalComputedUnsortedWeight += weight;
            }

        }

        return Pair.of(speciesTotalComputedSortedWeight, speciesTotalComputedUnsortedWeight);

    }

    private String getCategoryLabel(Integer sampleCategoryId) {
        SampleCategoryModelEntry category = context.getSampleCategoryModel().getCategoryById(sampleCategoryId);
        return category.getLabel();
    }

    private Integer sumFrequenciesNumber(List<SpeciesBatchFrequency> frequencies) {
        Integer frequencyNumber = 0;
        for (SpeciesBatchFrequency frequency : frequencies) {
            Integer c = frequency.getNumber();
            frequencyNumber += c;
        }
        return frequencyNumber;
    }

    private Float sumFrequenciesWeight(List<SpeciesBatchFrequency> frequencies) {
        Float frequencyWeight = 0f;
        for (SpeciesBatchFrequency frequency : frequencies) {
            Float w = frequency.getWeight();
            if (w == null) {

                // can't sum when a null value appears
                frequencyWeight = null;
                break;

            } else if (frequencyWeight != null) {

                // still can sum weights
                frequencyWeight += w;
            }
        }
        frequencyWeight = WeightUnit.KG.round(frequencyWeight);
        return frequencyWeight;
    }

    private String decorateSpecies(Species species) {
        if (speciesDecorator == null) {
            speciesDecorator = decoratorService.getDecoratorByType(Species.class);
        }
        return speciesDecorator.toString(species);
    }

    private String decorateCategoryValue(Serializable sampleCategoryValue) {
        return decoratorService.getDecorator(sampleCategoryValue).toString(sampleCategoryValue);
    }

    private WeightUnit getSpeciesWeightUnit() {
        return context.getConfig().getSpeciesWeightUnit();
    }

    private WeightUnit getBenthosWeightUnit() {
        return context.getConfig().getBenthosWeightUnit();
    }

    private WeightUnit getMarineLitterWeightUnit() {
        return context.getConfig().getMarineLitterWeightUnit();
    }

    private WeightUnit getCatchWeightUnit() {
        return WeightUnit.KG;
    }

}

package fr.ifremer.tutti.service.csv;

/*
 * #%L
 * Tutti :: Service
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.entities.referential.TuttiLocation;
import fr.ifremer.tutti.persistence.entities.referential.TuttiLocations;
import fr.ifremer.tutti.service.PersistenceService;

import java.util.List;

/**
 * Created on 2/14/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.14
 */
public class FishingOperationStrataParserFormatter extends EntityParserFormatterSupport<TuttiLocation> {

    public static FishingOperationStrataParserFormatter newFormatter() {
        return new FishingOperationStrataParserFormatter(false, null, null);
    }

    public static FishingOperationStrataParserFormatter newTechnicalFormatter() {
        return new FishingOperationStrataParserFormatter(true, null, null);
    }

    public static FishingOperationStrataParserFormatter newParser(PersistenceService persistenceService, String zoneId) {
        return new FishingOperationStrataParserFormatter(true, persistenceService, zoneId);
    }

    private final String zoneId;

    private final PersistenceService persistenceService;

    protected FishingOperationStrataParserFormatter(boolean technical, PersistenceService persistenceService, String zoneId) {
        super("NA", technical, TuttiLocation.class);
        this.zoneId = zoneId;
        this.persistenceService = persistenceService;
    }

    @Override
    protected List<TuttiLocation> getEntities() {
        return persistenceService.getAllFishingOperationStrata(zoneId);
    }

    @Override
    protected List<TuttiLocation> getEntitiesWithObsoletes() {
        return persistenceService.getAllFishingOperationStrataWithObsoletes(zoneId);
    }

    @Override
    protected String formatBusiness(TuttiLocation value) {
        return TuttiLocations.GET_NAME.apply(value);
    }

}

package fr.ifremer.tutti.service.catches.multipost.csv;

/*
 * #%L
 * Tutti :: Service
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.entities.referential.Caracteristic;
import fr.ifremer.tutti.persistence.entities.referential.Species;
import fr.ifremer.tutti.service.catches.multipost.MultiPostConstants;
import fr.ifremer.tutti.service.csv.AbstractTuttiImportExportModel;
import fr.ifremer.tutti.service.csv.CaracteristicValueParserFormatter;
import fr.ifremer.tutti.service.csv.TuttiCsvUtil;

import java.util.List;

/**
 * Model of a individual observation export.
 *
 * @author Kevin Morin - kmorin@codelutin.com
 * @since 2.2
 */
public class AccidentalCatchRowModel extends AbstractTuttiImportExportModel<AccidentalCatchRow> {

    private AccidentalCatchRowModel(List<Species> species,
                                    Caracteristic sexCaracteristic,
                                    Caracteristic dedOrAliveCaracteristic,
                                    List<Caracteristic> lengthStepCaracteristics) {
        super(MultiPostConstants.CSV_SEPARATOR);

        newColumnForImportExport(AccidentalCatchRow.BATCH_ID);

        newColumnForExport(AccidentalCatchRow.SPECIES, TuttiCsvUtil.SPECIES_TECHNICAL_FORMATTER);
        newSpeciesForeignKeyColumn(AccidentalCatchRow.SPECIES, species);

        newColumnForExport(AccidentalCatchRow.GENDER, TuttiCsvUtil.CARACTERISTIC_VALUE_TECHNICAL_FORMATTER);
        newMandatoryColumn(AccidentalCatchRow.GENDER, CaracteristicValueParserFormatter.newParser(sexCaracteristic));

        newColumnForImportExport(AccidentalCatchRow.WEIGHT, TuttiCsvUtil.WEIGHT_PARSER_FORMATTER);

        newColumnForImportExport(AccidentalCatchRow.SIZE, TuttiCsvUtil.FLOAT);

        newColumnForExport(AccidentalCatchRow.LENGTH_STEP_CARACTERISTIC, TuttiCsvUtil.CARACTERISTIC_TECHNICAL_FORMATTER);
        newForeignKeyColumn(AccidentalCatchRow.LENGTH_STEP_CARACTERISTIC, Caracteristic.class, lengthStepCaracteristics);

        newColumnForExport(AccidentalCatchRow.DEAD_OR_ALIVE, TuttiCsvUtil.CARACTERISTIC_VALUE_TECHNICAL_FORMATTER);
        newMandatoryColumn(AccidentalCatchRow.DEAD_OR_ALIVE, CaracteristicValueParserFormatter.newParser(dedOrAliveCaracteristic));

        newColumnForImportExport(AccidentalCatchRow.COMMENT);

    }

    public static AccidentalCatchRowModel forExport() {
        return new AccidentalCatchRowModel( null, null, null, null);
    }

    public static AccidentalCatchRowModel forImport(List<Species> species, Caracteristic sexCaracteristic,
                                                    Caracteristic dedOrAliveCaracteristic,
                                                    List<Caracteristic> lengthStepCaracteristics) {
        return new AccidentalCatchRowModel(species, sexCaracteristic, dedOrAliveCaracteristic, lengthStepCaracteristics);
    }

    @Override
    public AccidentalCatchRow newEmptyInstance() {
        return new AccidentalCatchRow();
    }
}

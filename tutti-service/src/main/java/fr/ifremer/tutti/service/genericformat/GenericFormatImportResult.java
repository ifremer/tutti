package fr.ifremer.tutti.service.genericformat;

/*
 * #%L
 * Tutti :: Service
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.entities.data.Cruise;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created on 2/11/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.14
 */
public class GenericFormatImportResult extends GenericFormatResultSupport {

    private static final long serialVersionUID = 1L;

    private final Map<String, GenericFormatImportCruiseResult> cruiseResults;

    private int nbCruisesCreated;

    private int nbCruisesUpdated;

    private int nbOperationsCreated;

    private int nbOperationsUpdated;

    private boolean withInvalidWeights;

    public GenericFormatImportResult(GenericFormatImportContext importContext) {
        super(importContext);

        this.cruiseResults = new LinkedHashMap<>();

        this.withInvalidWeights = false;

        for (GenericFormatImportCruiseContext cruiseContext : importContext.orderedCruiseContexts()) {

            Cruise cruise = cruiseContext.getCruise();
            GenericFormatImportCruiseResult cruiseResult = new GenericFormatImportCruiseResult(cruiseContext);
            cruiseResults.put(cruise.getId(), cruiseResult);

            if (cruiseResult.isOverride()) {
                nbCruisesUpdated++;
            } else {
                nbCruisesCreated++;
            }
            nbOperationsCreated += cruiseResult.getNbOperationsCreated();
            nbOperationsUpdated += cruiseResult.getNbOperationsUpdated();

            if (cruiseResult.isWithInvalidWeights()) {
                withInvalidWeights = true;
            }

        }

    }

    public Collection<GenericFormatImportCruiseResult> getCruiseResults() {
        return cruiseResults.values();
    }

    public int getNbCruisesCreated() {
        return nbCruisesCreated;
    }

    public int getNbCruisesUpdated() {
        return nbCruisesUpdated;
    }

    public int getNbOperationsCreated() {
        return nbOperationsCreated;
    }

    public int getNbOperationsUpdated() {
        return nbOperationsUpdated;
    }

    public boolean isWithInvalidWeights() {
        return withInvalidWeights;
    }

}

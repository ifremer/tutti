package fr.ifremer.tutti.service.genericformat.importactions;

/*
 * #%L
 * Tutti :: Service
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import fr.ifremer.adagio.core.dao.referential.ObjectTypeCode;
import fr.ifremer.tutti.persistence.entities.TuttiEntities;
import fr.ifremer.tutti.persistence.entities.data.Cruise;
import fr.ifremer.tutti.persistence.entities.data.FishingOperation;
import fr.ifremer.tutti.persistence.model.CruiseDataModel;
import fr.ifremer.tutti.service.genericformat.GenericFormatCsvFileResult;
import fr.ifremer.tutti.service.genericformat.GenericFormatImportContext;
import fr.ifremer.tutti.service.genericformat.GenericformatImportPersistenceHelper;
import fr.ifremer.tutti.service.genericformat.consumer.CsvConsumerForSurvey;
import fr.ifremer.tutti.service.genericformat.csv.AttachmentRow;
import fr.ifremer.tutti.service.genericformat.csv.SurveyRow;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.csv.ImportRow;
import org.nuiton.csv.ImportRuntimeException;
import org.nuiton.jaxx.application.ApplicationTechnicalException;

import java.io.IOException;
import java.util.Collection;
import java.util.Set;

import static org.nuiton.i18n.I18n.t;

/**
 * Created on 3/3/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.14
 */
public class ImportSurveyAction extends ImportActionSupport {

    /** Logger. */
    private static final Log log = LogFactory.getLog(ImportSurveyAction.class);

    private final GenericformatImportPersistenceHelper persistenceHelper;

    public ImportSurveyAction(GenericFormatImportContext importContext, GenericformatImportPersistenceHelper persistenceHelper) {
        super(importContext);
        this.persistenceHelper = persistenceHelper;
    }

    @Override
    protected boolean canExecute() {
        return importContext.isTechnicalFilesValid();
    }

    @Override
    protected void skipExecute() {
        importContext.increments(t("tutti.service.genericFormat.skip.import.cruises"));
    }

    @Override
    protected void doExecute() {

        if (log.isInfoEnabled()) {
            log.info("Import survey.csv file.");
        }
        importContext.increments(t("tutti.service.genericFormat.import.cruises"));

        GenericFormatCsvFileResult importFileResult = importContext.getSurveyFileResult();
        try (CsvConsumerForSurvey consumer = importContext.loadSurveys(true)) {
            for (ImportRow<SurveyRow> row : consumer) {

                consumer.validateRow(row, importContext);

                consumer.prepareRowForPersist(row);

                SurveyRow bean = row.getBean();
                Cruise cruise = bean.getCruise();

                Collection<AttachmentRow> attachmentRows = importContext.popAttachmentRows(ObjectTypeCode.SCIENTIFIC_CRUISE, bean.getObjectId());

                CruiseDataModel selectionCruiseData = importContext.getImportRequest().getSelectedCruise(cruise);

                if (selectionCruiseData == null) {

                    // skip this cruise (not selected)
                    skipCruise(cruise);

                } else {

                    // process this cruise
                    processCruise(cruise, attachmentRows, selectionCruiseData);

                }

            }
        } catch (IOException e) {
            throw new ApplicationTechnicalException("Could not close survey.csv file", e);
        } catch (ImportRuntimeException e) {

            importFileResult.addGlobalError(e.getMessage());

        }

    }

    protected void skipCruise(Cruise cruise) {

        String cruiseStr = importContext.decorate(cruise);
        importContext.increments(t("tutti.service.genericFormat.persist.skipNotSelected.cruise", cruiseStr));
        importContext.addSkippedCruise(cruise);

    }

    protected void processCruise(Cruise cruise, Collection<AttachmentRow> attachmentRows, CruiseDataModel selectionCruiseData) {

        String cruiseStr = importContext.decorate(cruise);

        CruiseDataModel existingCruiseData = importContext.getImportRequest().getExistingCruiseData(cruise);

        Set<FishingOperation> existingFishingOperations;

        Cruise savedCruise;

        if (existingCruiseData == null) {

            // New cruise
            importContext.increments(t("tutti.service.genericFormat.persist.create.cruise", cruiseStr));
            savedCruise = addCruise(cruise, attachmentRows);

            existingFishingOperations = null;

        } else {

            // Existing cruise

            if (importContext.getImportRequest().isUpdateCruises()) {

                // Update cruise
                importContext.increments(t("tutti.service.genericFormat.persist.update.cruise", cruiseStr));
                savedCruise = updateCruise(cruise, attachmentRows);

            } else {

                // skip update cruise
                importContext.increments(t("tutti.service.genericFormat.persist.skip.cruise", cruiseStr));
                savedCruise = cruise;

            }

            existingFishingOperations = persistenceHelper.getFishingOperations(existingCruiseData.getIdAsInt());

        }

        importContext.addImportedCruise(savedCruise, selectionCruiseData, existingCruiseData, existingFishingOperations);

    }

    public Cruise addCruise(Cruise cruise, Collection<AttachmentRow> attachmentRows) {

        String cruiseStr = importContext.decorate(cruise);

        boolean createCruise = TuttiEntities.isNew(cruise);
        Preconditions.checkState(createCruise, "In addCruise method, can't update existing cruise: " + cruiseStr);

        if (log.isInfoEnabled()) {
            log.info("Create cruise: " + cruiseStr);
        }


        Cruise savedCruise = persistenceHelper.createCruise(cruise);

        boolean importAttachments = importContext.getImportRequest().isImportAttachments();

        if (importAttachments) {

            if (CollectionUtils.isNotEmpty(attachmentRows)) {
                persistenceHelper. persistAttachments(savedCruise.getIdAsInt(), attachmentRows);
            }

        }

        return savedCruise;

    }

    public Cruise updateCruise(Cruise cruise, Collection<AttachmentRow> attachmentRows) {

        String cruiseStr = importContext.decorate(cruise);

        boolean createCruise = TuttiEntities.isNew(cruise);
        Preconditions.checkState(!createCruise, "In updateCruise method, can't create new cruise: " + cruiseStr);
        Preconditions.checkState(importContext.getImportRequest().isUpdateCruises(), "In updateCruise method, must be allowed to update cruise: " + cruiseStr);

        if (log.isInfoEnabled()) {
            log.info("Update existing cruise: " + cruiseStr);
        }

        Cruise savedCruise = persistenceHelper.saveCruise(cruise);

        boolean importAttachments = importContext.getImportRequest().isImportAttachments();

        if (importAttachments) {

            // delete previous attachments
            persistenceHelper.deleteAllAttachments(ObjectTypeCode.SCIENTIFIC_CRUISE, cruise.getIdAsInt());

            persistenceHelper.persistAttachments(savedCruise.getIdAsInt(), attachmentRows);

        }

        return savedCruise;

    }

}

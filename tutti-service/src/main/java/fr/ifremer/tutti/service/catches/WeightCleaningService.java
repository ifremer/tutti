package fr.ifremer.tutti.service.catches;

/*
 * #%L
 * Tutti :: Service
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import com.google.common.collect.LinkedHashMultimap;
import com.google.common.collect.Multimap;
import fr.ifremer.tutti.persistence.entities.data.BatchContainer;
import fr.ifremer.tutti.persistence.entities.data.FishingOperation;
import fr.ifremer.tutti.persistence.entities.data.SampleCategoryModel;
import fr.ifremer.tutti.persistence.entities.data.SampleCategoryModelEntry;
import fr.ifremer.tutti.persistence.entities.data.SpeciesBatch;
import fr.ifremer.tutti.persistence.entities.data.SpeciesBatchFrequency;
import fr.ifremer.tutti.persistence.entities.referential.Species;
import fr.ifremer.tutti.service.AbstractTuttiService;
import fr.ifremer.tutti.service.DecoratorService;
import fr.ifremer.tutti.service.PersistenceService;
import fr.ifremer.tutti.service.TuttiServiceContext;
import fr.ifremer.tutti.type.WeightUnit;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static fr.ifremer.tutti.service.PersistenceService.FrequencyFunction;
import static org.nuiton.i18n.I18n.t;

/**
 * A service to check and clean species and benthos  batches  redundant weights.
 *
 * Created on 9/27/13.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.6
 */
public class WeightCleaningService extends AbstractTuttiService {

    private static final Log log = LogFactory.getLog(WeightCleaningService.class);

    protected PersistenceService persistenceService;

    protected DecoratorService decoratorService;

    protected SampleCategoryModel sampleCategoryModel;

    protected FrequencyFunction speciesFrequencyFunction;

    protected FrequencyFunction benthosFrequencyFunction;

    protected WeightUnit benthosWeightUnit;

    protected WeightUnit speciesWeightUnit;

    private String speciesBatchPrefix;

    private String benthosBatchPrefix;

    @Override
    public void setServiceContext(TuttiServiceContext context) {
        super.setServiceContext(context);
        persistenceService = getService(PersistenceService.class);
        decoratorService = getService(DecoratorService.class);
        sampleCategoryModel = context.getSampleCategoryModel();
        speciesFrequencyFunction = persistenceService.newSpeciesFrequenciesFunction();
        benthosFrequencyFunction = persistenceService.newBenthosFrequenciesFunction();
        benthosWeightUnit = context.getConfig().getBenthosWeightUnit();
        speciesWeightUnit = context.getConfig().getSpeciesWeightUnit();
        speciesBatchPrefix = t("tutti.service.operations.cleanWeights.species.batch");
        benthosBatchPrefix = t("tutti.service.operations.cleanWeights.benthos.batch");
    }

//    /**
//     * Check if there is some redundant weights that could be cleaned.
//     *
//     * If no warning found, then return is a empty map, otherwise all errors
//     * for any bad fishing operation.
//     *
//     * Result keys are fishing operation id, values are all batches in error for
//     * the fishing operation.
//     *
//     * @param cruiseId id of the cruise to check.
//     * @return map of warnings, or empty map if no error found.
//     */
//    public Map<Integer, String> checkCruise(Integer cruiseId) {
//
//        if (log.isDebugEnabled()) {
//            log.debug("Will check cruise: " + cruiseId);
//        }
//
//        Map<Integer, String> result = new LinkedHashMap<>();
//
//        List<FishingOperation> allFishingOperation =
//                persistenceService.getAllFishingOperation(cruiseId);
//
//        for (FishingOperation fishingOperation : allFishingOperation) {
//
//            Integer fishingOperationId = fishingOperation.getIdAsInt();
//
//            boolean withCatchBatch =
//                    persistenceService.isFishingOperationWithCatchBatch(
//                            fishingOperationId);
//
//            if (!withCatchBatch) {
//                if (log.isWarnEnabled()) {
//                    log.warn("Skip fishing operation " + fishingOperation + " since no catchBatch associated.");
//                }
//                continue;
//            }
//
//            Multimap<String, String> errors = checkFishingOperation(fishingOperationId);
//
//            if (!errors.isEmpty()) {
//                result.put(fishingOperationId, errorsToString(errors));
//            }
//        }
//        return result;
//    }

    public String errorsToString(Multimap<String, String> errors) {
        StringBuilder errorsStr = new StringBuilder("<ul style=\"font-size:11\">");
        for (String error : errors.keySet()) {

            errorsStr.append("<li>");
            Collection<String> strings = errors.get(error);
            errorsStr.append(error);
            if (strings.size() == 1) {
                errorsStr.append(" ").append(strings.iterator().next());
            } else {
                errorsStr.append("<ul>");
                for (String s : strings) {
                    errorsStr.append("<li>");
                    errorsStr.append(s);
                    errorsStr.append("</li>");
                }
                errorsStr.append("</ul>");
            }
            errorsStr.append("</li>");
        }
        errorsStr.append("</ul>");

        return errorsStr.toString();
    }

//    /**
//     * Clean all redundant weights for the given cruise.
//     *
//     * @param cruiseId id of the cruise to clean.
//     */
//    public void cleanCruise(Integer cruiseId) {
//
//        if (log.isDebugEnabled()) {
//            log.debug("Will clean cruise: " + cruiseId);
//        }
//
//        List<Integer> allFishingOperation = persistenceService.getAllFishingOperationIds(cruiseId);
//
//        allFishingOperation.forEach(this::cleanFishingOperation);
//    }

    /**
     * Check for some redundant weights in species or benthos batches of
     * the given operation.
     *
     * @param fishingOperationId id of the fishing operation to check
     * @return the list of all species / benthos which contains a redundant weight
     */
    public Multimap<String, String> checkFishingOperation(Integer fishingOperationId) {

        if (log.isDebugEnabled()) {
            log.debug("Will check fishingOperation: " + fishingOperationId);
        }
        Multimap<String, String> result = LinkedHashMultimap.create();

        boolean withCatchBatch = persistenceService.isFishingOperationWithCatchBatch(fishingOperationId);

        if (withCatchBatch) {

            // check species batches

            BatchContainer<SpeciesBatch> speciesBatch = persistenceService.getRootSpeciesBatch(fishingOperationId, true);

            for (SpeciesBatch batch : speciesBatch.getChildren()) {

                checkBatch(speciesBatchPrefix,
                           speciesWeightUnit,
                           batch,
                           result,
                           speciesFrequencyFunction);
            }

            // check benthos batches

            BatchContainer<SpeciesBatch> benthosBatch = persistenceService.getRootBenthosBatch(fishingOperationId, true);

            for (SpeciesBatch batch : benthosBatch.getChildren()) {

                checkBatch(benthosBatchPrefix,
                           benthosWeightUnit,
                           batch,
                           result,
                           benthosFrequencyFunction);
            }

        } else {

            // no catch, no check

            if (log.isWarnEnabled()) {
                log.warn("Skip fishing operation " + fishingOperationId + " since no catchBatch associated.");
            }
        }

        return result;
    }

    /**
     * Clean all the redundant weights in species or benthos batches of
     * the given operation.
     *
     * @param fishingOperationId id of the fishing operation to check
     * @return {@code true} if some weights were deleted, {@code false} otherwise
     */
    public boolean cleanFishingOperation(Integer fishingOperationId) {

        if (log.isDebugEnabled()) {
            log.debug("Will clean fishingOperation: " + fishingOperationId);
        }

        boolean withCatchBatch = persistenceService.isFishingOperationWithCatchBatch(fishingOperationId);

        boolean result = false;

        if (withCatchBatch) {

            // clean species batches

            Set<SpeciesBatch> speciesToSave = new HashSet<>();
            BatchContainer<SpeciesBatch> speciesBatch = persistenceService.getRootSpeciesBatch(fishingOperationId, true);

            for (SpeciesBatch batch : speciesBatch.getChildren()) {

                cleanBatch(speciesBatchPrefix,
                           speciesWeightUnit,
                           batch,
                           speciesToSave,
                           speciesFrequencyFunction);
            }

            // clean benthos batches

            Set<SpeciesBatch> benthosToSave = new HashSet<>();
            BatchContainer<SpeciesBatch> benthosBatch = persistenceService.getRootBenthosBatch(fishingOperationId, true);

            for (SpeciesBatch batch : benthosBatch.getChildren()) {

                cleanBatch(benthosBatchPrefix,
                           benthosWeightUnit,
                           batch,
                           benthosToSave,
                           benthosFrequencyFunction);
            }

            if (!(speciesToSave.isEmpty() && benthosToSave.isEmpty())) {

                // save modified batches

                result = true;

                FishingOperation fishingOperation = persistenceService.getFishingOperation(fishingOperationId);

                for (SpeciesBatch toSave : speciesToSave) {
                    toSave.setFishingOperation(fishingOperation);
                    persistenceService.saveSpeciesBatch(toSave);
                }

                for (SpeciesBatch toSave : benthosToSave) {
                    toSave.setFishingOperation(fishingOperation);
                    persistenceService.saveBenthosBatch(toSave);
                }
            }

        } else {

            // no catch, no clean

            if (log.isWarnEnabled()) {
                log.warn("Skip fishing operation " + fishingOperationId + " since no catchBatch associated.");
            }
        }

        return result;

    }

    protected void checkBatch(String batchPrefix, WeightUnit weightUnit, SpeciesBatch batch, Multimap<String, String> result, FrequencyFunction frequencyFunction) {

        if (!batch.isChildBatchsEmpty()) {

            // check childs
            for (SpeciesBatch childBatch : batch.getChildBatchs()) {

                // check batch sample category weight (before childs)
                String message = checkSampleCategoryWeightRedundant(weightUnit, batch, childBatch);
                addMessageIfNotExist(message, batchPrefix, weightUnit, childBatch, result);

                // check childs
                checkBatch(batchPrefix, weightUnit, childBatch, result, frequencyFunction);
            }
        } else {

            { // check batch weight
                String message = checkWeightRedundant(weightUnit, batch);
                addMessageIfNotExist(message, batchPrefix, weightUnit, batch, result);
            }

            { // check batch frequencies weight
                String message = checkFrequencyWeightRedundant(weightUnit, batch, frequencyFunction);
                addMessageIfNotExist(message, batchPrefix, weightUnit, batch, result);
            }
        }
    }

    protected void cleanBatch(String batchPrefix,
                              WeightUnit weightUnit,
                              SpeciesBatch batch,
                              Set<SpeciesBatch> result,
                              FrequencyFunction frequencyFunction) {

        if (!batch.isChildBatchsEmpty()) {

            // check childs
            for (SpeciesBatch childBatch : batch.getChildBatchs()) {

                // check batch sample category weight
                String message = checkSampleCategoryWeightRedundant(weightUnit, batch, childBatch);

                // clean childs before
                cleanBatch(batchPrefix, weightUnit, childBatch, result, frequencyFunction);

                if (message != null) {

                    // always clean batch after cleaning childs
                    buildLabelAndLogMessage(message, batchPrefix, weightUnit, childBatch);
                    batch.setSampleCategoryWeight(null);
                    result.add(batch);
                }
            }
        } else {

            boolean removeWeight = false;

            { // check batch frequencies weight
                String message = checkFrequencyWeightRedundant(weightUnit, batch, frequencyFunction);

                if (message != null) {
                    buildLabelAndLogMessage(message, batchPrefix, weightUnit, batch);
                    removeWeight = true;

                }
            }

            { // check batch weight
                String message = checkWeightRedundant(weightUnit, batch);

                if (message != null) {
                    buildLabelAndLogMessage(message, batchPrefix, weightUnit, batch);
                    removeWeight = true;
                }
            }

            if (removeWeight) {

                // must clean the batch weight
                batch.setWeight(null);
                result.add(batch);
            }
        }

    }

    protected String checkSampleCategoryWeightRedundant(WeightUnit weightUnit, SpeciesBatch parentBatch, SpeciesBatch batch) {

        Preconditions.checkNotNull(parentBatch);

        String result = null;

        // check the sample weight is different than his parent one
        Float sampleCategoryWeight = batch.getSampleCategoryWeight();
        Float parentSampleCategoryWeight = parentBatch.getSampleCategoryWeight();

        if (sampleCategoryWeight != null && parentSampleCategoryWeight != null && WeightUnit.KG.isEquals(parentSampleCategoryWeight, sampleCategoryWeight)) {

            result = t("tutti.service.operations.cleanWeights.error.redundant.sampleCategoryWeight",
                       weightUnit.fromEntity(sampleCategoryWeight) + weightUnit.getShortLabel(),
                       getCategoryLabel(batch),
                       getCategoryLabel(parentBatch)
            );

        }
        return result;
    }


    protected String checkFrequencyWeightRedundant(WeightUnit weightUnit,
                                                   SpeciesBatch batch,
                                                   FrequencyFunction frequencyFunction) {
        Preconditions.checkState(batch.isChildBatchsEmpty());

        String result = null;

        // on a leaf, check if weight is not = to sum of frequencies weight
        Float weight = batch.getWeight();
        if (weight != null) {

            // check if there is some frequencies
            List<SpeciesBatchFrequency> frequencies = frequencyFunction.apply(batch);
            Float frequenciesWeigth = persistenceService.countFrequenciesWeight(frequencies, true);

            if (frequenciesWeigth != null && WeightUnit.KG.isEquals(weight, frequenciesWeigth)) {
                result = t("tutti.service.operations.cleanWeights.error.redundant.frequencyWeight",
                           weightUnit.fromEntity(weight) + weightUnit.getShortLabel(),
                           getCategoryLabel(batch));
            }
        }

        return result;
    }

    protected String checkWeightRedundant(WeightUnit weightUnit, SpeciesBatch batch) {

        Preconditions.checkState(batch.isChildBatchsEmpty());

        String result = null;

        // on a leaf, check if weight is not = to the finest category weight
        Float weight = batch.getWeight();
        Float sampleCategoryWeight = batch.getSampleCategoryWeight();
        if (weight != null && sampleCategoryWeight != null && WeightUnit.KG.isEquals(weight, sampleCategoryWeight)) {

            result = t("tutti.service.operations.cleanWeights.error.redundant.weight",
                       weightUnit.fromEntity(weight) + weightUnit.getShortLabel(),
                       getCategoryLabel(batch));
        }

        return result;
    }

    protected String getBatchLabel(String type, WeightUnit weightUnit, SpeciesBatch batch) {
        StringBuilder sb = new StringBuilder("[ " + type);

        String species = decoratorService.getDecoratorByType(Species.class).toString(batch.getSpecies());
        sb.append(" ").append(species);

        List<String> categories = new ArrayList<>();
        SpeciesBatch b = batch;
        while (b.getParentBatch() != null) {
            String categoryValue = decoratorService.getDecorator(b.getSampleCategoryValue())
                                                   .toString(b.getSampleCategoryValue());
            categories.add(0, categoryValue + " / " + weightUnit.fromEntity(b.getSampleCategoryWeight()));
            b = b.getParentBatch();
        }

        for (String category : categories) {
            sb.append(" - ").append(category);
        }
        sb.append(" ]");
        return sb.toString();
    }

    protected String getCategoryLabel(SpeciesBatch batch) {
        SampleCategoryModelEntry category = sampleCategoryModel.getCategoryById(batch.getSampleCategoryId());
        return category.getLabel();
    }


    protected String buildLabelAndLogMessage(String message, String batchPrefix, WeightUnit weightUnit, SpeciesBatch b) {
        String label = getBatchLabel(batchPrefix, weightUnit, b);
        if (log.isInfoEnabled()) {
            log.info(label + " " + message);
        }
        return label;
    }

    protected void addMessageIfNotExist(String message, String batchPrefix, WeightUnit weightUnit, SpeciesBatch b, Multimap<String, String> result) {

        if (message != null) {

            String label = buildLabelAndLogMessage(message, batchPrefix, weightUnit, b);
            if (!result.containsEntry(label, message)) {
                result.put(label, message);
            }
        }

    }

}

package fr.ifremer.tutti.service.export.pdf;

/*
 * #%L
 * Tutti :: Service
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import fr.ifremer.tutti.persistence.entities.data.FishingOperation;
import fr.ifremer.tutti.persistence.entities.data.SpeciesBatch;
import fr.ifremer.tutti.persistence.entities.referential.Species;
import fr.ifremer.tutti.persistence.entities.referential.Speciess;
import fr.ifremer.tutti.persistence.entities.referential.TaxonCache;
import fr.ifremer.tutti.persistence.entities.referential.TaxonCaches;
import fr.ifremer.tutti.service.AbstractTuttiService;
import fr.ifremer.tutti.service.PdfGeneratorService;
import fr.ifremer.tutti.service.PersistenceService;
import fr.ifremer.tutti.service.TuttiServiceContext;
import fr.ifremer.tutti.service.catches.WeightComputingService;
import fr.ifremer.tutti.service.export.ExportBatchEntry;
import fr.ifremer.tutti.service.export.ExportCatchContext;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.File;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import static org.nuiton.i18n.I18n.t;

/**
 * @author Kevin Morin - kmorin@codelutin.com
 * @since 2.0
 */
public class CatchesPdfExportService extends AbstractTuttiService {

    /** Logger. */
    private static final Log log = LogFactory.getLog(CatchesPdfExportService.class);

    protected PersistenceService persistenceService;

    protected WeightComputingService weightComputingService;

    protected PdfGeneratorService pdfGeneratorService;

    public CatchesPdfExportService() {
        super();

    }

    @Override
    public void setServiceContext(TuttiServiceContext context) {
        super.setServiceContext(context);
        persistenceService = getService(PersistenceService.class);
        weightComputingService = getService(WeightComputingService.class);
        pdfGeneratorService = getService(PdfGeneratorService.class);
    }

    /**
     * Generate the PDF report for the fiven cruise.
     *
     * @param targetFile pdf file to generate
     * @param cruiseId   cruise id
     * @param locale     generated pdf locale
     */
    public void generateCruisePDFFile(File targetFile,
                                      Integer cruiseId,
                                      Locale locale) {


        List<Integer> allFishingOperation =
                persistenceService.getAllFishingOperationIds(cruiseId);

        List<Map<String, Object>> operations = Lists.newArrayList();
        for (Integer operationId : allFishingOperation) {

            prepareOperation(operationId, operations);

        }

        generatePdf(targetFile, locale, operations);
    }

    /**
     * Generate the PDF report for the given fishing operation.
     *
     * @param targetFile         pdf file to generate
     * @param fishingOperationId id of the fishing operation to export
     * @param locale             generated pdf locale
     * @since 2.7
     */
    public void generateFishingOperationPDFFile(File targetFile,
                                                Integer fishingOperationId,
                                                Locale locale) {

        List<Map<String, Object>> operations = Lists.newArrayList();

        prepareOperation(fishingOperationId, operations);

        generatePdf(targetFile, locale, operations);
    }

    protected void prepareOperation(Integer fishingOperationId, List<Map<String, Object>> operations) {

        // get operation and catch data
        boolean withCatchBatch =
                persistenceService.isFishingOperationWithCatchBatch(
                        fishingOperationId);

        if (!withCatchBatch) {
            if (log.isWarnEnabled()) {
                log.warn("Skip fishing operation " + fishingOperationId +
                                 " since no catchBatch associated.");
            }
            return;
        }

        ExportCatchContext exportContext = ExportCatchContext.newExportContext(
                persistenceService,
                weightComputingService,
                fishingOperationId,
                false);

        // create operation data model
        Map<String, Object> op = createOperation(exportContext.getFishingOperation());

        float totalWeight = exportContext.getCatchTotalWeight();

        op.put("totalWeight", totalWeight);
        op.put("totalSortedWeight", exportContext.getCatchTotalSortedWeight());

        List<PdfExportBatchEntry> catchList = Lists.newArrayList();

        // Species
        if (exportContext.withSpeciesBatches()) {

            prepareOperationSpecies(exportContext, totalWeight, catchList);

        }

        // Benthos
        if (exportContext.withBenthosBatches()) {

            prepareOperationBenthos(exportContext, totalWeight, catchList);

        }

        // Inert and living not itemized
        ExportBatchEntry inertLivingNotItemizedCatch = exportContext.getInertAndLivingNotItemizedCatch();

        if (inertLivingNotItemizedCatch.getSortedWeight() > 0f) {

            PdfExportBatchEntry pdfExportBatchEntry =
                    new PdfExportBatchEntry(
                            t("tutti.service.operations.exportCatchesReport.specialRows.inertAndLivinngNotItemized.code"),
                            "",
                            t("tutti.service.operations.exportCatchesReport.specialRows.inertAndLivinngNotItemized.name"),
                            inertLivingNotItemizedCatch.getSortedWeight(),
                            inertLivingNotItemizedCatch.getTotalWeight(),
                            totalWeight);
            catchList.add(pdfExportBatchEntry);
        }

        if (log.isDebugEnabled()) {

            // recompute total for check
            float computedTotalWeight = 0f;
            float computedPercentage = 0f;
            for (PdfExportBatchEntry entry : catchList) {
                computedTotalWeight += entry.getTotalWeight();
                computedPercentage += entry.getPercentage();
            }
            log.debug("TotalWeight: " + totalWeight);
            log.debug("ComputedTotalWeight: " + computedTotalWeight);
            log.debug("ComputedPercentage: " + computedPercentage);
        }
        op.put("catches", catchList);
        operations.add(op);
    }

    protected void prepareOperationSpecies(ExportCatchContext exportContext, float totalWeight, List<PdfExportBatchEntry> catchList) {

        List<ExportBatchEntry> speciesBatchEntries = exportContext.getSpeciesBatchEntry(false);

        TaxonCache taxonCache = TaxonCaches.createSpeciesCache(persistenceService, context.getDataContext().getProtocol());

        for (ExportBatchEntry entry : speciesBatchEntries) {

            SpeciesBatch batch = entry.getBatch();
            Species species = batch.getSpecies();

            taxonCache.load(species);

            String code = Speciess.getSurveyCodeOrRefTaxCode(species);

            PdfExportBatchEntry pdfEntry = new PdfExportBatchEntry(code,
                                                                   species.getName(),
                                                                   species.getVernacularCode(),
                                                                   entry.getSortedWeight(),
                                                                   entry.getTotalWeight(),
                                                                   totalWeight);
            catchList.add(pdfEntry);
        }
    }

    protected void prepareOperationBenthos(ExportCatchContext exportContext, float totalWeight, List<PdfExportBatchEntry> catchList) {

        List<ExportBatchEntry> benthosBatchEntries = exportContext.getBenthosBatchEntry(false);

        float sortedWeight = 0f;
        float benthosTotalWeight = 0f;

        for (ExportBatchEntry entry : benthosBatchEntries) {
            sortedWeight += entry.getSortedWeight();
            benthosTotalWeight += entry.getTotalWeight();
        }

        PdfExportBatchEntry pdfEntry = new PdfExportBatchEntry(
                t("tutti.service.operations.exportCatchesReport.specialRows.benthos.code"),
                "",
                t("tutti.service.operations.exportCatchesReport.specialRows.benthos.name"),
                sortedWeight,
                benthosTotalWeight,
                totalWeight);
        catchList.add(pdfEntry);

    }

    protected void generatePdf(File targetFile, Locale locale, List<Map<String, Object>> operations) {

        Map<String, Object> data = Maps.newHashMap();
        data.put("operations", operations);

        pdfGeneratorService.generatePdf(targetFile, locale, "catchesReport.ftl", data);

    }

    protected Map<String, Object> createOperation(FishingOperation fishingOperation) {
        Map<String, Object> op = Maps.newHashMap();
        op.put("number", fishingOperation.getFishingOperationNumber());
        op.put("station", fishingOperation.getStationNumber());
        op.put("rigNumber", fishingOperation.getMultirigAggregation());
        op.put("startDate", fishingOperation.getGearShootingStartDate());
        op.put("endDate", fishingOperation.getGearShootingEndDate());

        return op;
    }

}

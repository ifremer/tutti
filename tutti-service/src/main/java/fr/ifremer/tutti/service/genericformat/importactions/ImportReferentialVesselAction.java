package fr.ifremer.tutti.service.genericformat.importactions;

/*
 * #%L
 * Tutti :: Service
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.entities.referential.Vessel;
import fr.ifremer.tutti.service.genericformat.GenericFormatContextSupport;
import fr.ifremer.tutti.service.genericformat.GenericFormatReferentialImportResult;
import fr.ifremer.tutti.service.genericformat.GenericformatImportPersistenceHelper;
import fr.ifremer.tutti.service.referential.ReferentialImportRequest;
import fr.ifremer.tutti.service.referential.ReferentialImportResult;
import fr.ifremer.tutti.service.referential.consumer.CsvConsumerForTemporaryVessel;
import fr.ifremer.tutti.service.referential.csv.VesselRow;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.csv.ImportRow;
import org.nuiton.csv.ImportRuntimeException;
import org.nuiton.jaxx.application.ApplicationTechnicalException;

import java.io.IOException;

import static org.nuiton.i18n.I18n.t;

/**
 * Created on 3/3/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.14
 */
public class ImportReferentialVesselAction extends ImportActionSupport {

    /** Logger. */
    private static final Log log = LogFactory.getLog(ImportReferentialVesselAction.class);

    private final GenericformatImportPersistenceHelper persistenceHelper;

    public ImportReferentialVesselAction(GenericFormatContextSupport importContext, GenericformatImportPersistenceHelper persistenceHelper) {
        super(importContext);
        this.persistenceHelper = persistenceHelper;
    }

    @Override
    protected boolean canExecute() {
        return importContext.getReferentialTemporaryVesselFileResult().isFound();
    }

    @Override
    protected void skipExecute() {

        if (log.isInfoEnabled()) {
            log.info("Skip import temporary vessels (no file found).");
        }
        importContext.increments(t("tutti.service.genericFormat.skip.import.temporaryVessels"));

    }

    @Override
    protected void doExecute() {

        importContext.increments(t("tutti.service.genericFormat.import.temporaryVessels"));

        ReferentialImportRequest<Vessel, String> referentialImportRequest = persistenceHelper.createVesselsImportRequest();

        GenericFormatReferentialImportResult<Vessel, String> importFileResult = importContext.getReferentialTemporaryVesselFileResult();
        try (CsvConsumerForTemporaryVessel consumer = importContext.loadTemporaryVessels(false)) {
            for (ImportRow<VesselRow> row : consumer) {
                consumer.checkRowForGenericFormatImport(row, referentialImportRequest);
            }

            importFileResult.flushErrors(consumer);

        } catch (IOException e) {
            throw new ApplicationTechnicalException("Could not close vessel.csv file", e);
        } catch (ImportRuntimeException e) {

            importFileResult.addGlobalError(e.getMessage());

        }

        if (importFileResult.isValid()) {

            ReferentialImportResult<Vessel> referentialImportResult = persistenceHelper.importVessels(referentialImportRequest);
            importFileResult.flushResult(referentialImportRequest, referentialImportResult);

            if (log.isInfoEnabled()) {
                log.info("Temporary vessels import result: " + importFileResult.getReport());
            }

        } else {

            if (log.isWarnEnabled()) {
                log.warn("Do not import temporary vessels (the incoming file is not valid)");
            }

        }

    }
}

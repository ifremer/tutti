package fr.ifremer.tutti.service.referential.csv;

/*
 * #%L
 * Tutti :: Service
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import fr.ifremer.tutti.persistence.entities.referential.Person;
import fr.ifremer.tutti.persistence.entities.referential.Persons;

/**
 * @author Kevin Morin (Code Lutin)
 * @since 3.8
 */
public class PersonRow {

    public static final String PROPERTY_ID = "id";

    public static final String PROPERTY_FIRST_NAME = "firstName";

    public static final String PROPERTY_LAST_NAME = "lastName";

    public static final String PROPERTY_TO_DELETE = "toDelete";

    protected String id;

    protected String firstName;

    protected String lastName;

    protected Boolean toDelete;

    public PersonRow() {
        super();
    }

    public PersonRow(Person person) {
        super();
        Preconditions.checkNotNull(person);
        setId(person.getId());
        setFirstName(person.getFirstName());
        setLastName(person.getLastName());
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFullName() {

        Person person = this.toEntity();
        return Persons.GET_FULL_NAME.apply(person);

    }

    public Boolean getToDelete() {
        return toDelete;
    }

    public void setToDelete(Boolean toDelete) {
        this.toDelete = toDelete;
    }

    public Person toEntity() {

        Person person = Persons.newPerson();
        person.setId(getId());
        person.setFirstName(getFirstName());
        person.setLastName(getLastName());
        return person;

    }

    public Integer getIdAsInt() {
        Integer idAsInt = null;
        if (id != null) {
            idAsInt = Integer.valueOf(id);
        }
        return idAsInt;
    }
}

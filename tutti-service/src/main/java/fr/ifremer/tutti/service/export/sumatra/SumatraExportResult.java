package fr.ifremer.tutti.service.export.sumatra;

/*
 * #%L
 * Tutti :: Service
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.apache.commons.collections4.CollectionUtils;

import java.util.Set;

/**
 * Created on 5/20/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.4.4
 */
public class SumatraExportResult {

    /**
     * Name of species that are not well filled in protocol.
     */
    protected Set<String> badSpecies;

    /**
     * Name of benthos that are not well filled in protocol.
     */
    protected Set<String> badBenthos;

    public Set<String> getBadSpecies() {
        return badSpecies;
    }

    public Set<String> getBadBenthos() {
        return badBenthos;
    }

    public void setBadSpecies(Set<String> badSpecies) {
        this.badSpecies = badSpecies;
    }

    public void setBadBenthos(Set<String> badBenthos) {
        this.badBenthos = badBenthos;
    }

    public boolean withBadSpecies() {
        return CollectionUtils.isNotEmpty(badSpecies);
    }

    public boolean withBadBenthos() {
        return CollectionUtils.isNotEmpty(badBenthos);
    }
}

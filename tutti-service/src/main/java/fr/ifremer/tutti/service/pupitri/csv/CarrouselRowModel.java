package fr.ifremer.tutti.service.pupitri.csv;

/*
 * #%L
 * Tutti :: Service
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.service.csv.TuttiCsvUtil;
import fr.ifremer.tutti.service.csv.ImportModelWithHeader;
import fr.ifremer.tutti.service.pupitri.BoxType;
import fr.ifremer.tutti.service.pupitri.Directions;
import fr.ifremer.tutti.service.pupitri.Signs;
import org.nuiton.csv.Common;
import org.nuiton.csv.ValueParser;

import java.text.ParseException;

/**
 * @author Kevin Morin - kmorin@codelutin.com
 * @since 1.2
 */
public class CarrouselRowModel extends ImportModelWithHeader<CarrouselRow> {

    public CarrouselRowModel(char separator) {
        super(separator);

        newMandatoryColumn(CarrouselRow.PROPERTY_OPERATION_CODE);
        newMandatoryColumn(CarrouselRow.PROPERTY_DATE, new Common.DateValue("dd/MM/yy"));
        newMandatoryColumn(CarrouselRow.PROPERTY_SPECIES_ID);
        newMandatoryColumn(CarrouselRow.PROPERTY_SIGN,
                           new ValueParser<Signs>() {
                               @Override
                               public Signs parse(String value) throws ParseException {
                                   Signs result = Signs.getSign(value.toUpperCase());
                                   if (result == null) {
                                       throw new ParseException("Could not parse Sign value: " + value, 0);
                                   }
                                   return result;
                               }
                           }
        );
        newMandatoryColumn(CarrouselRow.PROPERTY_BOX_TYPE,
                           new ValueParser<BoxType>() {
                               @Override
                               public BoxType parse(String value) throws ParseException {
                                   BoxType result = BoxType.getBoxType(value.toUpperCase());
                                   if (result == null) {
                                       throw new ParseException("Could not parse BoxType value: " + value, 0);
                                   }
                                   return result;
                               }
                           });
        newMandatoryColumn(CarrouselRow.PROPERTY_DIRECTION, TuttiCsvUtil.newEnumByNameParserFormatter(Directions.class));
        newMandatoryColumn(CarrouselRow.PROPERTY_WEIGHT, TuttiCsvUtil.WEIGHT_PARSER_FORMATTER);

        newIgnoredColumn(CarrouselRow.PROPERTY_FILE_ORIGIN);
        newIgnoredColumn(CarrouselRow.PROPERTY_RIG_NUMBER);
        newIgnoredColumn(CarrouselRow.PROPERTY_TIME);
        newIgnoredColumn(CarrouselRow.PROPERTY_BALANCE_ID);
        newIgnoredColumn(CarrouselRow.PROPERTY_TO_CONFIRM);
    }

    @Override
    public String[] getHeader() {
        return new String[]{
                CarrouselRow.PROPERTY_FILE_ORIGIN,
                CarrouselRow.PROPERTY_DATE,
                CarrouselRow.PROPERTY_TIME,
                CarrouselRow.PROPERTY_BALANCE_ID,
                CarrouselRow.PROPERTY_TO_CONFIRM,
                CarrouselRow.PROPERTY_OPERATION_CODE,
                CarrouselRow.PROPERTY_RIG_NUMBER,
                CarrouselRow.PROPERTY_BOX_TYPE,
                CarrouselRow.PROPERTY_SPECIES_ID,
                CarrouselRow.PROPERTY_SIGN,
                CarrouselRow.PROPERTY_DIRECTION,
                CarrouselRow.PROPERTY_WEIGHT
        };
    }

    @Override
    public CarrouselRow newEmptyInstance() {
        return new CarrouselRow();
    }

}

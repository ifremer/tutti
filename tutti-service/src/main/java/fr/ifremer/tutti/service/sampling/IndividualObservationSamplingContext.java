package fr.ifremer.tutti.service.sampling;

/*
 * #%L
 * Tutti :: Service
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2016 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.entities.protocol.CalcifiedPiecesSamplingDefinition;
import fr.ifremer.tutti.persistence.entities.protocol.Zone;
import fr.ifremer.tutti.persistence.entities.referential.CaracteristicQualitativeValue;
import fr.ifremer.tutti.persistence.entities.referential.Species;

import java.util.Objects;

/**
 * Définit le context d'une observation individuelles ou d'un prélèvement, i.e toutes les caractéristiques qui
 * permettent de retrouver cette observation individuelle au niveau de l'algorithme de prélèvement.
 *
 * L'utilisation de cet objet sous-entend que l'observation individuelle liée rentre bien dans l'algorithme.
 *
 * Created on 18/04/16.
 *
 * @author Tony Chemit - chemit@codelutin.com
 */
public class IndividualObservationSamplingContext {

    /**
     * L'espèce de l'observation individuelle.
     */
    private final Species species;

    /**
     * La classe de taille de l'observation individuelle.
     */
    private final Integer lengthStep;

    /**
     * Le statut de maturité.
     */
    private final Boolean maturity;

    /**
     * Le sexe observé.
     */
    private final CaracteristicQualitativeValue gender;

    /**
     * La définition de l'algorithme de prélèvement de picèes calcifiées utilisée pour cette observation.
     */
    private final CalcifiedPiecesSamplingDefinition calcifiedPiecesSamplingDefinition;

    /**
     * La zone où est effectuée l'observation individuelle.
     */
    private final Zone zone;

    /**
     * La clef pour accéder au cache des compteurs (de niveau Campagne).
     */
    private final String cruiseSamplingKey;

    /**
     * La clef pour accéder au cache des compteurs (de niveau Zone).
     */
    private final String zoneSamplingKey;

    /**
     * La clef pour accéder au cache des compteurs (de niveau Opération de pêche).
     */
    private final String fishingOperationSamplingKey;

    public IndividualObservationSamplingContext(Species species,
                                                Integer lengthStep,
                                                Boolean maturity,
                                                CaracteristicQualitativeValue gender,
                                                CalcifiedPiecesSamplingDefinition calcifiedPiecesSamplingDefinition,
                                                Zone zone,
                                                String cruiseSamplingKey,
                                                String zoneSamplingKey,
                                                String fishingOperationSamplingKey) {
        Objects.requireNonNull(species);
        Objects.requireNonNull(calcifiedPiecesSamplingDefinition);
        Objects.requireNonNull(zone);
        Objects.requireNonNull(cruiseSamplingKey);
        Objects.requireNonNull(zoneSamplingKey);
        Objects.requireNonNull(fishingOperationSamplingKey);
        this.species = species;
        this.lengthStep = lengthStep;
        this.maturity = maturity;
        this.gender = gender;
        this.calcifiedPiecesSamplingDefinition = calcifiedPiecesSamplingDefinition;
        this.zone = zone;
        this.cruiseSamplingKey = cruiseSamplingKey;
        this.zoneSamplingKey = zoneSamplingKey;
        this.fishingOperationSamplingKey = fishingOperationSamplingKey;
    }

    public Species getSpecies() {
        return species;
    }

    public Integer getLengthStep() {
        return lengthStep;
    }

    public Boolean getMaturity() {
        return maturity;
    }

    public CaracteristicQualitativeValue getGender() {
        return gender;
    }

    public CalcifiedPiecesSamplingDefinition getCalcifiedPiecesSamplingDefinition() {
        return calcifiedPiecesSamplingDefinition;
    }

    public Zone getZone() {
        return zone;
    }

    public int getTotalSamplingRequiredInCruise() {
        return calcifiedPiecesSamplingDefinition.getMaxByLenghtStep();
    }

    public int getTotalSamplingRequiredInFishingOperation() {
        return calcifiedPiecesSamplingDefinition.getOperationLimitation();
    }

    public int getTotalSamplingRequiredInZone() {
        return calcifiedPiecesSamplingDefinition.getZoneLimitation();
    }

    public String getCruiseSamplingKey() {
        return cruiseSamplingKey;
    }

    public String getZoneSamplingKey() {
        return zoneSamplingKey;
    }

    public String getFishingOperationSamplingKey() {
        return fishingOperationSamplingKey;
    }

    public boolean withGender() {
        return gender!=null;
    }

    public boolean withMaturity() {
        return maturity!=null;
    }
}

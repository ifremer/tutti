package fr.ifremer.tutti.service.genericformat.consumer;

/*
 * #%L
 * Tutti :: Service
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.entities.data.Cruise;
import fr.ifremer.tutti.persistence.entities.referential.Gear;

import static org.nuiton.i18n.I18n.t;

/**
 * Created on 2/18/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since XXX
 */
public class GearNotFoundInCruiseException extends Exception {

    private static final long serialVersionUID = 1L;

    private final Cruise cruise;

    private final Gear gear;

    private final short rankOrder;

    public GearNotFoundInCruiseException(Cruise cruise, Gear gear, short rankOrder) {
        this.cruise = cruise;
        this.gear = gear;
        this.rankOrder = rankOrder;
    }

    public Cruise getCruise() {
        return cruise;
    }

    public Gear getGear() {
        return gear;
    }

    public short getRankOrder() {
        return rankOrder;
    }

    @Override
    public String getMessage() {
        return t("tutti.service.genericFormat.import.error.gearNotFoundInCruise", gear.getName(), rankOrder, cruise.getName());
    }

}

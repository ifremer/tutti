package fr.ifremer.tutti.service.genericformat;

/*
 * #%L
 * Tutti :: Service
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import com.google.common.collect.Sets;
import fr.ifremer.tutti.persistence.ProgressionModel;
import fr.ifremer.tutti.persistence.entities.data.Cruise;
import fr.ifremer.tutti.persistence.entities.data.FishingOperation;
import fr.ifremer.tutti.persistence.entities.data.SampleCategoryModel;
import fr.ifremer.tutti.persistence.entities.protocol.TuttiProtocol;
import fr.ifremer.tutti.persistence.entities.referential.Caracteristic;
import fr.ifremer.tutti.persistence.entities.referential.Species;
import fr.ifremer.tutti.persistence.entities.referential.Speciess;
import fr.ifremer.tutti.persistence.model.ProgramDataModel;
import fr.ifremer.tutti.service.PersistenceService;
import fr.ifremer.tutti.service.catches.WeightComputingService;
import fr.ifremer.tutti.service.csv.CsvProducer;
import fr.ifremer.tutti.service.genericformat.csv.AccidentalCatchModel;
import fr.ifremer.tutti.service.genericformat.csv.AttachmentModel;
import fr.ifremer.tutti.service.genericformat.csv.CatchModel;
import fr.ifremer.tutti.service.genericformat.csv.GearCaracteristicModel;
import fr.ifremer.tutti.service.genericformat.csv.IndividualObservationModel;
import fr.ifremer.tutti.service.genericformat.csv.MarineLitterModel;
import fr.ifremer.tutti.service.genericformat.csv.OperationModel;
import fr.ifremer.tutti.service.genericformat.csv.ParameterModel;
import fr.ifremer.tutti.service.genericformat.csv.SpeciesExportModel;
import fr.ifremer.tutti.service.genericformat.csv.SurveyModel;
import fr.ifremer.tutti.service.genericformat.producer.CsvProducerForAccidentalCatch;
import fr.ifremer.tutti.service.genericformat.producer.CsvProducerForAttachment;
import fr.ifremer.tutti.service.genericformat.producer.CsvProducerForCatch;
import fr.ifremer.tutti.service.genericformat.producer.CsvProducerForGearCaracteristics;
import fr.ifremer.tutti.service.genericformat.producer.CsvProducerForIndividualObservation;
import fr.ifremer.tutti.service.genericformat.producer.CsvProducerForMarineLitter;
import fr.ifremer.tutti.service.genericformat.producer.CsvProducerForOperation;
import fr.ifremer.tutti.service.genericformat.producer.CsvProducerForParameter;
import fr.ifremer.tutti.service.genericformat.producer.CsvProducerForSampleCategory;
import fr.ifremer.tutti.service.genericformat.producer.CsvProducerForSpecies;
import fr.ifremer.tutti.service.genericformat.producer.CsvProducerForSurvey;
import fr.ifremer.tutti.service.referential.producer.CsvProducerForTemporaryGear;
import fr.ifremer.tutti.service.referential.producer.CsvProducerForTemporaryPerson;
import fr.ifremer.tutti.service.referential.producer.CsvProducerForTemporarySpecies;
import fr.ifremer.tutti.service.referential.producer.CsvProducerForTemporaryVessel;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.decorator.Decorator;

import java.io.Closeable;
import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created on 2/5/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.13
 */
public class GenericFormatExportContext implements Closeable, Iterable<GenericFormatExportCruiseContext> {

    /** Logger. */
    private static final Log log = LogFactory.getLog(GenericFormatExportContext.class);

    private final GenericFormatExportRequest exportRequest;

    private final Decorator<FishingOperation> fishingOperationDecorator;

    private final Caracteristic deadOrAliveCaracteristic;

    private final Caracteristic genderCaracteristic;

    private final Caracteristic copyIndividualObservationModeCaracteristic;

    private final Caracteristic sampleCodeCaracteristic ;

    private final Caracteristic weightMeasuredCaracteristic;

    private final Caracteristic pmfmIdCaracteristic;

    private final CsvProducerForSurvey producerForSurvey;

    private final CsvProducerForGearCaracteristics producerForGearCaracteristics;

    private final CsvProducerForOperation producerForOperation;

    private final CsvProducerForParameter producerForParameter;

    private final CsvProducerForIndividualObservation producerForIndividualObservation;

    private final CsvProducerForAccidentalCatch producerForAccidentalCatch;

    private final CsvProducerForCatch producerForCatch;

    private final CsvProducerForSpecies producerForSpecies;

    private final CsvProducerForMarineLitter producerForMarineLitter;

    private final CsvProducerForTemporaryGear producerForTemporaryGear;

    private final CsvProducerForTemporaryPerson producerForTemporaryPerson;

    private final CsvProducerForTemporarySpecies producerForTemporarySpecies;

    private final CsvProducerForTemporaryVessel producerForTemporaryVessel;

    private final CsvProducerForSampleCategory producerForSampleCategory;

    private final CsvProducerForAttachment producerForAttachment;

    private final ProgressionModel progressionModel;

    private final Set<GenericFormatExportCruiseContext> cruiseContexts;

    GenericFormatExportContext(ProgressionModel progressionModel,
                               GenericFormatExportRequest exportRequest,
                               PersistenceService persistenceService,
                               Decorator<FishingOperation> fishingOperationDecorator) {

        Preconditions.checkNotNull(progressionModel);
        this.progressionModel = progressionModel;

        Preconditions.checkNotNull(exportRequest);
        this.exportRequest = exportRequest;

        Preconditions.checkNotNull(fishingOperationDecorator);
        this.fishingOperationDecorator = fishingOperationDecorator;

        Preconditions.checkNotNull(persistenceService);

        this.weightMeasuredCaracteristic = persistenceService.getWeightMeasuredCaracteristic();
        this.pmfmIdCaracteristic = persistenceService.getPmfmIdCaracteristic();
        this.deadOrAliveCaracteristic = persistenceService.getDeadOrAliveCaracteristic();
        this.genderCaracteristic = persistenceService.getSexCaracteristic();
        this.copyIndividualObservationModeCaracteristic = persistenceService.getCopyIndividualObservationModeCaracteristic();
        this.sampleCodeCaracteristic = persistenceService.getSampleCodeCaracteristic();
        this.cruiseContexts = new LinkedHashSet<>();

        List<Species> allReferentSpecies = persistenceService.getAllReferentSpecies();
        Map<String, Species> speciesByReferenceTaxonId = Speciess.splitReferenceSpeciesByReferenceTaxonId(allReferentSpecies);

        GenericFormatArchive archive = exportRequest.getArchive();

        char csvSeparator = exportRequest.getCsvSeparator();

        SampleCategoryModel sampleCategoryModel = exportRequest.getSampleCategoryModel();

        Path attachmentsSourcePath = exportRequest.getAttachmentsSourcePath();

        this.producerForSurvey = new CsvProducerForSurvey(archive.getSurveyPath(), SurveyModel.forExport(csvSeparator));
        this.producerForGearCaracteristics = new CsvProducerForGearCaracteristics(archive.getGearCaracteristicsPath(), GearCaracteristicModel.forExport(csvSeparator));
        this.producerForOperation = new CsvProducerForOperation(archive.getOperationPath(), OperationModel.forExport(csvSeparator));
        this.producerForIndividualObservation = new CsvProducerForIndividualObservation(archive.getIndividualObservationPath(), IndividualObservationModel.forExport(csvSeparator));
        this.producerForSpecies = new CsvProducerForSpecies(archive.getSpeciesPath(), SpeciesExportModel.forExport(csvSeparator), speciesByReferenceTaxonId);
        this.producerForCatch = new CsvProducerForCatch(archive.getCatchPath(), CatchModel.forExport(csvSeparator, sampleCategoryModel));
        this.producerForAccidentalCatch = new CsvProducerForAccidentalCatch(archive.getAccidentalCatchPath(), AccidentalCatchModel.forExport(csvSeparator));
        this.producerForParameter = new CsvProducerForParameter(archive.getParameterPath(), ParameterModel.forExport(csvSeparator));
        this.producerForMarineLitter = new CsvProducerForMarineLitter(archive.getMarineLitterPath(), MarineLitterModel.forExport(csvSeparator));
        this.producerForSampleCategory = new CsvProducerForSampleCategory(archive.getSampleCategoryModelPath(), fr.ifremer.tutti.service.genericformat.csv.SampleCategoryModel.forExport(csvSeparator));
        this.producerForAttachment = new CsvProducerForAttachment(archive.getAttachmentFilePath(), AttachmentModel.forExport(csvSeparator), attachmentsSourcePath, archive.getAttachmentDataPath());

        this.producerForTemporaryGear = new CsvProducerForTemporaryGear(archive.getTemporaryReferentialGearsPath(), csvSeparator);
        this.producerForTemporaryPerson = new CsvProducerForTemporaryPerson(archive.getTemporaryReferentialPersonsPath(), csvSeparator);
        this.producerForTemporarySpecies = new CsvProducerForTemporarySpecies(archive.getTemporaryReferentialSpeciesPath(), csvSeparator, true);
        this.producerForTemporaryVessel = new CsvProducerForTemporaryVessel(archive.getTemporaryReferentialVesselsPath(), csvSeparator);

    }

    public TuttiProtocol getProtocol() {
        return exportRequest.getProtocol();
    }

    public File getExportFile() {
        return getExportConfiguration().getExportFile();
    }

    public ProgramDataModel getDataToExport() {
        return getExportConfiguration().getDataToExport();
    }

    public boolean isExportAttachments() {
        return getExportConfiguration().isExportAttachments();
    }

    public boolean isExportSpecies() {
        return getExportConfiguration().isExportSpecies();
    }

    public boolean isExportBenthos() {
        return getExportConfiguration().isExportBenthos();
    }

    public boolean isExportMarineLitter() {
        return getExportConfiguration().isExportMarineLitter();
    }

    public boolean isExportAccidentalCatch() {
        return getExportConfiguration().isExportAccidentalCatch();
    }

    public boolean isExportIndividualObservation() {
        return getExportConfiguration().isExportIndividualObservation();
    }

    public GenericFormatArchive getArchive() {
        return exportRequest.getArchive();
    }

    public SampleCategoryModel getSampleCategoryModel() {
        return exportRequest.getSampleCategoryModel();
    }

    public File getProtocolFile() {
        return getArchive().getProtocolPath().toFile();
    }

    public ProgressionModel getProgressionModel() {
        return progressionModel;
    }

    public CsvProducerForSurvey getProducerForSurvey() {
        return producerForSurvey;
    }

    public CsvProducerForGearCaracteristics getProducerForGearCaracteristics() {
        return producerForGearCaracteristics;
    }

    public CsvProducerForOperation getProducerForOperation() {
        return producerForOperation;
    }

    public CsvProducerForParameter getProducerForParameter() {
        return producerForParameter;
    }

    public CsvProducerForIndividualObservation getProducerForIndividualObservation() {
        return producerForIndividualObservation;
    }

    public CsvProducerForAccidentalCatch getProducerForAccidentalCatch() {
        return producerForAccidentalCatch;
    }

    public CsvProducerForCatch getProducerForCatch() {
        return producerForCatch;
    }

    public CsvProducerForSpecies getProducerForSpecies() {
        return producerForSpecies;
    }

    public CsvProducerForMarineLitter getProducerForMarineLitter() {
        return producerForMarineLitter;
    }

    public CsvProducerForTemporaryGear getProducerForTemporaryGear() {
        return producerForTemporaryGear;
    }

    public CsvProducerForTemporaryPerson getProducerForTemporaryPerson() {
        return producerForTemporaryPerson;
    }

    public CsvProducerForTemporarySpecies getProducerForTemporarySpecies() {
        return producerForTemporarySpecies;
    }

    public CsvProducerForTemporaryVessel getProducerForTemporaryVessel() {
        return producerForTemporaryVessel;
    }

    public CsvProducerForSampleCategory getProducerForSampleCategory() {
        return producerForSampleCategory;
    }

    public CsvProducerForAttachment getProducerForAttachment() {
        return producerForAttachment;
    }

    public void increments(String message) {
        progressionModel.increments(message);
    }

    public void addCruiseContext(Cruise cruise, Set<FishingOperation> operations) {

        GenericFormatExportCruiseContext cruiseContext = new GenericFormatExportCruiseContext(cruise, operations);
        cruiseContexts.add(cruiseContext);

    }

    public GenericFormatExportOperationContext newOperationContext(PersistenceService persistenceService,
                                                                   WeightComputingService weightComputingService,
                                                                   Cruise cruise, FishingOperation operation) {

        String operationLabel = fishingOperationDecorator.toString(operation);
        return new GenericFormatExportOperationContext(cruise,
                                                       operation,
                                                       operationLabel,
                                                       persistenceService,
                                                       weightComputingService,
                                                       getSampleCategoryModel(),
                                                       weightMeasuredCaracteristic,
                                                       pmfmIdCaracteristic,
                                                       deadOrAliveCaracteristic,
                                                       genderCaracteristic,
                                                       copyIndividualObservationModeCaracteristic,
                                                       sampleCodeCaracteristic);

    }

    @Override
    public void close() {

        for (CsvProducer csvProducer : producers()) {

            try {
                csvProducer.close();
            } catch (IOException e) {
                if (log.isErrorEnabled()) {
                    log.error("Could not close producer: " + csvProducer.getClass().getSimpleName(), e);
                }
            }

        }

    }

    @Override
    public Iterator<GenericFormatExportCruiseContext> iterator() {
        return cruiseContexts.iterator();
    }

    public Set<CsvProducer<?, ?>> producers() {
        return Sets.newHashSet(
                producerForSurvey,
                producerForSpecies,
                producerForGearCaracteristics,
                producerForOperation,
                producerForParameter,
                producerForAccidentalCatch,
                producerForIndividualObservation,
                producerForCatch,
                producerForMarineLitter,
                producerForTemporaryGear,
                producerForTemporaryPerson,
                producerForTemporarySpecies,
                producerForTemporaryVessel,
                producerForSampleCategory,
                producerForAttachment);
    }

    protected GenericFormatExportConfiguration getExportConfiguration() {
        return exportRequest.getExportConfiguration();
    }

}

package fr.ifremer.tutti.service.referential;

/*
 * #%L
 * Tutti :: Service
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ImmutableSet;
import fr.ifremer.adagio.core.service.technical.synchro.ReferentialSynchroContext;
import fr.ifremer.adagio.core.service.technical.synchro.ReferentialSynchroResult;
import fr.ifremer.adagio.core.service.technical.synchro.ReferentialSynchroService;
import fr.ifremer.tutti.persistence.service.TuttiPersistenceServiceLocator;
import fr.ifremer.tutti.service.AbstractTuttiService;
import fr.ifremer.tutti.service.TuttiServiceContext;

import java.io.File;
import java.util.Set;

/**
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.0
 */
public class TuttiReferentialSynchronizeService extends AbstractTuttiService {

    protected ReferentialSynchroService synchroService;

    protected static Set<String> TABLE_NAMES = ImmutableSet.<String>builder().add(
            "STATUS",
            "QUALITY_FLAG",

            // PMFM
            "UNIT",
            "AGGREGATION_LEVEL",
            "PARAMETER_GROUP",
            "QUALITATIVE_VALUE",
            "PARAMETER",
            "MATRIX",
            "FRACTION",
            "FRACTION2MATRIX",
            "METHOD",
            "PMFM",
            "PMFM2QUALITATIVE_VALUE",

            // GEAR
            "GEAR_CLASSIFICATION",
            "GEAR_CLASSIFICATION_ASSOCIATIO",
            "GEAR",
            "GEAR_ASSOCIATION",

            // LOCATION
            "LOCATION_CLASSIFICATION",
            "LOCATION_LEVEL",
            "LOCATION_ASSOCIATION",
            "LOCATION",
            "LOCATION_HIERARCHY",
            "LOCATION_HIERARCHY_EXCEPTION",

            // TAXON
            "TAXONOMIC_LEVEL",
            "REFERENCE_TAXON",
            "TAXON_NAME",
            "TAXON_INFORMATION",
            "TAXON_INFORMATION_HISTORY",
            "VIRTUAL_COMPONENT",
            "TAXON_NAME_HISTORY",
            "REFERENCE_DOCUMENT",
            "AUTHOR",
            "CITATION",

            // TAXON GROUP
            "TAXON_GROUP_TYPE",
            "TAXON_GROUP",
            "TAXON_GROUP_HISTORICAL_RECORD",
            "TAXON_GROUP_INFORMATION",

            // TRANSCRIBING
            "TRANSCRIBING_SIDE",
            "TRANSCRIBING_SYSTEM",
            "TRANSCRIBING_ITEM_TYPE",
            "TRANSCRIBING_ITEM",

            // CONVERSION
            "ROUND_WEIGHT_CONVERSION",
            "WEIGHT_LENGTH_CONVERSION",
            "UNIT_CONVERSION",

            // VESSEL
            "VESSEL_TYPE",
            "VESSEL_REGISTRATION_PERIOD",
            "VESSEL_FEATURES",
            "VESSEL",

            // PERSON
            "USER_PROFIL",
            "DEPARTMENT",
            "PERSON",
            "PERSON2USER_PROFIL",

            // VESSEL_PERSON_ROLE
            "VESSEL_PERSON_ROLE",
            "VESSEL_PERSON",

            // ORDER
            "ORDER_ITEM",

            // PROGRAM (since 3.3.5)
            "PROGRAM",
            "PROGRAM2LOCATION",
            "PROGRAM2LOCATION_CLASSIF",

            // STRATEGY (since 3.5)
            "ACQUISITION_LEVEL",
            "PROGRAM_PRIVILEGE",
            "PROGRAM2DEPARTMENT",
            "PROGRAM2PERSON_EXCEPTION",
            "PROGRAM2PERSON",
            "STRATEGY",
            "STRATEGY2MANAGER_PERSON",
            "PMFM_APPLIED_STRATEGY",
            "APPLIED_STRATEGY",
            "PMFM_STRATEGY",
            "REFERENCE_TAXON_STRATEGY",
            "STRATEGY2GEAR",
            // FIXME-tchemit-2014-04-19 This table fails to update
            //"APPLIED_PERIOD",

            // OTHER
            "PRECISION_TYPE",
            "NUMERICAL_PRECISION",
            "PHOTO_TYPE",
            "OBJECT_TYPE",
            "ORDER_TYPE",
            "ANALYSIS_INSTRUMENT"
    ).build();

    @Override
    public void setServiceContext(TuttiServiceContext context) {
        super.setServiceContext(context);
        synchroService = TuttiPersistenceServiceLocator.getReferentialSynchroService();
    }

    public ReferentialSynchroContext createSynchroContext(File dbDirectory) {
        return ReferentialSynchroContext.newContext(
                TABLE_NAMES,
                dbDirectory,
                new ReferentialSynchroResult()
        );
    }

    public void prepare(ReferentialSynchroContext synchroContext) {
        synchroService.prepare(synchroContext);
    }

    public void synchronize(ReferentialSynchroContext synchroContext) {
        synchroService.synchronize(synchroContext);
    }


}

package fr.ifremer.tutti.service.catches;

/*
 * #%L
 * Tutti :: Service
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.nuiton.jaxx.application.ApplicationBusinessException;

import java.util.Optional;

/**
 * @author Kevin Morin - kmorin@codelutin.com
 * @since 2.0
 */
public class TuttiWeightComputingException extends ApplicationBusinessException {

    private static final long serialVersionUID = 1L;

    protected String property;

    protected Integer index;

    protected TuttiWeightComputingException(String message) {
        this(message, null, null);
    }

    protected TuttiWeightComputingException(String message, String property, Integer index) {
        super(message);
        this.property = property;
        this.index = index;
    }

    public Optional<String> getProperty() {
        return Optional.ofNullable(property);
    }

    public Optional<Integer> getIndex() {
        return Optional.ofNullable(index);
    }
}

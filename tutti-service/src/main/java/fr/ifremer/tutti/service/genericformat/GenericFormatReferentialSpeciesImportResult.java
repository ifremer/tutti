package fr.ifremer.tutti.service.genericformat;

/*
 * #%L
 * Tutti :: Service
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ImmutableMap;
import fr.ifremer.tutti.persistence.entities.referential.Species;

import java.util.Map;
import java.util.TreeMap;

/**
 * Created on 4/17/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 4.0-RC3
 */
public class GenericFormatReferentialSpeciesImportResult extends GenericFormatReferentialImportResult<Species, Integer> {

    private static final long serialVersionUID = 1L;

    private ImmutableMap<Integer, Integer> referenceTaxonIdTranslationMap;

    public GenericFormatReferentialSpeciesImportResult(String filename, boolean found) {
        super(filename, found);
        this.referenceTaxonIdTranslationMap = ImmutableMap.of();
    }

    public Map<Integer, Integer> getReferenceTaxonIdTranslationMap() {
        return referenceTaxonIdTranslationMap;
    }

    public void flushObsoleteReferenceTaxonIds(Map<Integer, Integer> referenceTaxonIdById) {

        referenceTaxonIdTranslationMap = ImmutableMap.copyOf(referenceTaxonIdById);

    }

    public void flushReferenceTaxonIds(Map<Integer, Integer> referenceTaxonIdById) {

        Map<Integer, Integer> referenceTaxonIdMap = new TreeMap<>();

        for (Map.Entry<Integer, Integer> entry : referenceTaxonIdById.entrySet()) {
            Integer oldId = entry.getKey();
            String originalId = String.valueOf(oldId);
            Species species = entitiesAdded.get(originalId);
            if (species == null) {
                species = entitiesLinked.get(originalId);
            }

            // il s'agit d'un taxon importé, on effectue la correspondance
            Integer oldReferenceTaxonId = entry.getValue();
            Integer newReferenceTaxonId = species.getReferenceTaxonId();

            referenceTaxonIdMap.put(oldReferenceTaxonId, newReferenceTaxonId);

        }

        referenceTaxonIdTranslationMap = ImmutableMap
                .<Integer, Integer>builder()
                .putAll(referenceTaxonIdTranslationMap)
                .putAll(referenceTaxonIdMap)
                .build();

    }

}

package fr.ifremer.tutti.service.genericformat.csv;

/*
 * #%L
 * Tutti :: Service
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.entities.referential.Caracteristic;
import fr.ifremer.tutti.persistence.entities.referential.Gear;

import java.io.Serializable;

/**
 * A row in a gear caracteristics export.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.3.2
 */
public class GearCaracteristicRow extends RowWithCruiseContextSupport {

    private static final long serialVersionUID = 1L;

    public static final String PROPERTY_GEAR = "gear";

    public static final String PROPERTY_CARACTERISTIC = "caracteristic";

    public static final String PROPERTY_VALUE = "value";

    public static final String PROPERTY_RANK_ORDER = Gear.PROPERTY_RANK_ORDER;

    public static GearCaracteristicRow newEmptyInstance() {
        GearCaracteristicRow row = new GearCaracteristicRow();
        row.forImport();
        return row;
    }

    protected Gear gear;

    protected Caracteristic caracteristic;

    protected short rankOrder;

    protected Serializable value;

    public void setGear(Gear gear) {
        this.gear = gear;
    }

    public void setCaracteristic(Caracteristic caracteristic) {
        this.caracteristic = caracteristic;
    }

    public void setValue(Serializable value) {
        this.value = value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Gear getGear() {
        return gear;
    }

    public Caracteristic getCaracteristic() {
        return caracteristic;
    }

    public Serializable getValue() {
        return value;
    }

    public short getRankOrder() {
        return rankOrder;
    }

    public void setRankOrder(short rankOrder) {
        this.rankOrder = rankOrder;
    }

}

package fr.ifremer.tutti.service.csv;

/*
 * #%L
 * Tutti :: Service
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import fr.ifremer.tutti.persistence.entities.data.Program;
import fr.ifremer.tutti.persistence.entities.referential.Caracteristic;
import fr.ifremer.tutti.persistence.entities.referential.Gear;
import fr.ifremer.tutti.persistence.entities.referential.Person;
import fr.ifremer.tutti.persistence.entities.referential.Species;
import fr.ifremer.tutti.persistence.entities.referential.TuttiLocation;
import fr.ifremer.tutti.persistence.entities.referential.Vessel;
import fr.ifremer.tutti.type.WeightUnit;
import org.nuiton.csv.Common;
import org.nuiton.csv.ValueFormatter;
import org.nuiton.csv.ValueParserFormatter;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

import static org.nuiton.i18n.I18n.t;

/**
 * Helper around csv import / export in Tutti.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.0
 */
public class TuttiCsvUtil extends Common {

    /**
     * To parse / format possible nullable weight.
     *
     * @since 3.3.2
     */
    public static final ValueParserFormatter<Float> WEIGHT_NULL_TO_9 =
            new FloatParserFormatter(-9f, true) {
                @Override
                public String format(Float value) {
                    if (value == null) {
                        // if value is null then use the default value
                        value = defaultValue;
                    }
                    return WeightUnit.KG.renderWeight(value);
                }

                @Override
                protected Float parseNoneEmptyValue(String value) {
                    Float aFloat = super.parseNoneEmptyValue(value);
                    if (defaultValue.equals(aFloat)) {
                        // if having -9, then value is null
                        aFloat = null;
                    }
                    return aFloat;
                }
            };

    public static final ValueParserFormatter<Integer> INTEGER_NULL_TO_9 =
            new IntegerParserFormatter(-9, true) {
                @Override
                public String format(Integer value) {
                    if (value == null) {
                        // if value is null then use the default value
                        value = defaultValue;
                    }
                    return super.format(value);
                }

                @Override
                protected Integer parseNoneEmptyValue(String value) {
                    Integer aFloat = super.parseNoneEmptyValue(value);
                    if (defaultValue.equals(aFloat)) {
                        // if having -9, then value is null
                        aFloat = null;
                    }
                    return aFloat;
                }
            };

//    public static final ValueParserFormatter<String> COMMENT_PARSER_FORMATTER = new ValueParserFormatter<String>() {
//
//        @Override
//        public String parse(String value) throws ParseException {
//            return value == null ? "" : value.replaceAll("@@", "\n");
//        }
//
//        @Override
//        public String format(String value) {
//            return value == null ? "" : value.replaceAll("\n", "@@");
//        }
//    };

//    public static final ValueFormatter<Species> SPECIES_NAME_FORMATTER = new ValueFormatter<Species>() {
//        @Override
//        public String format(Species s) {
//            Preconditions.checkNotNull(s, t("tutti.service.error.species.null"));
//            return s.getName();
//        }
//    };

    public static final ValueFormatter<Species> SPECIES_SURVEY_CODE_FORMATTER = s -> {
        Preconditions.checkNotNull(s, t("tutti.service.error.species.null"));
        String surveyCode = s.getSurveyCode();
        return surveyCode == null ? "" : surveyCode;
    };

    public static final ValueFormatter<Species> SPECIES_REF_TAX_CODE_FORMATTER = s -> {
        Preconditions.checkNotNull(s, t("tutti.service.error.species.null"));
        String surveyCode = s.getRefTaxCode();
        return surveyCode == null ? "" : surveyCode;
    };

    public static ValueParserFormatter<Float> WEIGHT_PARSER_FORMATTER = new FloatParserFormatter(null, true) {

        @Override
        public String format(Float value) {
            if (value != null) {
                value = WeightUnit.KG.round(value);
            }
            return super.format(value);
        }

        @Override
        protected Float parseNoneEmptyValue(String value) {
            Float aFloat = super.parseNoneEmptyValue(value);
            return WeightUnit.KG.round(aFloat);
        }
    };

    public static final CommentParserFormatter COMMENT_PARSER_FORMATTER = new CommentParserFormatter();

//    public static final ValueParserFormatter<List<String>> COMMENT_LIST_PARSER_FORMATTER = new CommentListParserFormatter(COMMENT_PARSER_FORMATTER);

    public static final ValueFormatter<Caracteristic> CARACTERISTIC_FORMATTER = CaracteristicParserFormatter.newFormatter();

    public static final ValueFormatter<Caracteristic> CARACTERISTIC_TECHNICAL_FORMATTER = CaracteristicParserFormatter.newTechnicalFormatter();

    public static final ValueFormatter<Serializable> CARACTERISTIC_VALUE_FORMATTER = CaracteristicValueParserFormatter.newFormatter();

    public static ValueFormatter<Serializable> CARACTERISTIC_VALUE_TECHNICAL_FORMATTER = CaracteristicValueParserFormatter.newTechnicalFormatter();

    public static final ValueFormatter<Program> PROGRAM_FORMATTER = ProgramParserFormatter.newFormatter();

    public static final ValueFormatter<Program> PROGRAM_TECHNICAL_FORMATTER = ProgramParserFormatter.newTechnicalFormatter();

    public static final ValueFormatter<Gear> GEAR_FORMATTER = GearParserFormatter.newFormatter();

    public static final ValueFormatter<Gear> GEAR_TECHNICAL_FORMATTER = GearParserFormatter.newTechnicalFormatter();

//    public static final ValueFormatter<List<Gear>> GEAR_LIST_FORMATTER = GearListParserFormatter.newFormatter(GearParserFormatter.newFormatter());

    public static final ValueFormatter<List<Gear>> GEAR_LIST_TECHNICAL_FORMATTER = GearListParserFormatter.newFormatter(GearParserFormatter.newTechnicalFormatter());

    public static final ValueFormatter<TuttiLocation> COUNTRY_FORMATTER = new CountryFormatter();

    public static final ValueFormatter<TuttiLocation> PROGRAM_ZONE_FORMATTER = new ProgramZoneFormatter();

    public static final ValueFormatter<TuttiLocation> HARBOUR_FORMATTER = HarbourParserFormatter.newFormatter();

    public static final ValueFormatter<TuttiLocation> HARBOUR_TECHNICAL_FORMATTER = HarbourParserFormatter.newTechnicalFormatter();

    public static final ValueFormatter<TuttiLocation> FISHING_OPERATION_STRATA_FORMATTER = FishingOperationStrataParserFormatter.newFormatter();

    public static final ValueFormatter<TuttiLocation> FISHING_OPERATION_STRATA_TECHNICAL_FORMATTER = FishingOperationStrataParserFormatter.newTechnicalFormatter();

    public static final ValueFormatter<TuttiLocation> FISHING_OPERATION_SUB_STRATA_FORMATTER = FishingOperationSubStrataParserFormatter.newFormatter();

    public static final ValueFormatter<TuttiLocation> FISHING_OPERATION_SUB_STRATA_TECHNICAL_FORMATTER = FishingOperationSubStrataParserFormatter.newTechnicalFormatter();

    public static final ValueFormatter<TuttiLocation> FISHING_OPERATION_LOCATION_FORMATTER = FishingOperationLocationParserFormatter.newFormatter();

    public static final ValueFormatter<TuttiLocation> FISHING_OPERATION_LOCATION_TECHNICAL_FORMATTER = FishingOperationLocationParserFormatter.newTechnicalFormatter();

    public static final ValueFormatter<Vessel> VESSEL_FORMATTER = VesselParserFormatter.newFormatter();

    public static final ValueFormatter<Vessel> VESSEL_TECHNICAL_FORMATTER = VesselParserFormatter.newTechnicalFormatter();

    public static final ValueFormatter<Species> SPECIES_FORMATTER = SpeciesParserFormatter.newFormatter();

    public static final ValueFormatter<Species> SPECIES_TECHNICAL_FORMATTER = SpeciesParserFormatter.newTechnicalFormatter();

    public static final ValueFormatter<List<Person>> PERSON_LIST_FORMATTER = PersonListParserFormatter.newFormatter(PersonParserFormatter.newFormatter());

    public static final ValueFormatter<List<Person>> PERSON_LIST_TECHNICAL_FORMATTER = PersonListParserFormatter.newFormatter(PersonParserFormatter.newTechnicalFormatter());

    public static final ValueFormatter<List<Vessel>> VESSEL_LIST_FORMATTER = VesselListParserFormatter.newFormatter(VesselParserFormatter.newFormatter());

    public static final ValueFormatter<List<Vessel>> VESSEL_LIST_TECHNICAL_FORMATTER = VesselListParserFormatter.newFormatter(VesselParserFormatter.newTechnicalFormatter());

    public static final ValueParserFormatter<List<Integer>> INTEGER_LIST_PARSER_FORMATTER = new IntegerListParserFormatter();

    public static final ValueParserFormatter<Set<String>> STRING_SET_PARSER_FORMATTER = new StringSetParserFormatter();

    public static <E extends Enum<E>> ValueParserFormatter<E> newEnumByNameParserFormatter(Class<E> enumType, boolean mandatory) {
        return new fr.ifremer.tutti.service.csv.EnumByNameParserFormatter<>(enumType, mandatory);
    }


    protected TuttiCsvUtil() {
        // no instance
    }

}

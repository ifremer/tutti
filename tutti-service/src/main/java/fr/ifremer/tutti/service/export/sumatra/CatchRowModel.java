package fr.ifremer.tutti.service.export.sumatra;

/*
 * #%L
 * Tutti :: Service
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.entities.data.FishingOperation;
import fr.ifremer.tutti.service.csv.TuttiCsvUtil;
import fr.ifremer.tutti.service.csv.AbstractTuttiImportExportModel;
import fr.ifremer.tutti.service.export.ExportBatchEntry;

import static org.nuiton.i18n.I18n.t;

/**
 * @author Kevin Morin - kmorin@codelutin.com
 * @since 2.0
 */
public class CatchRowModel extends AbstractTuttiImportExportModel<CatchRow> {

    public CatchRowModel(char separator) {
        super(separator);

        newColumnForExport(t("tutti.service.exportSumatra.header.year"), CatchRow.PROPERTY_GEAR_SHOOTING_START_DATE, TuttiCsvUtil.YEAR);
        newColumnForExport(t("tutti.service.exportSumatra.header.station"), CatchRow.PROPERTY_STATION_NUMBER);
        newColumnForExport(t("tutti.service.exportSumatra.header.multirigAggregation"), CatchRow.PROPERTY_MULTIRIG_AGGREGATION);
        newColumnForExport(t("tutti.service.exportSumatra.header.genuisSpecies"), CatchRow.PROPERTY_SPECIES, TuttiCsvUtil.SPECIES_FORMATTER);
        newColumnForExport(t("tutti.service.exportSumatra.header.surveySpecies"), CatchRow.PROPERTY_SPECIES, TuttiCsvUtil.SPECIES_SURVEY_CODE_FORMATTER);
        newColumnForExport(t("tutti.service.exportSumatra.header.sign"), CatchRow.PROPERTY_SIGN);
        newColumnForExport(t("tutti.service.exportSumatra.header.sortedWeight"), CatchRow.PROPERTY_SORTED_WEIGHT, TuttiCsvUtil.PRIMITIVE_FLOAT);
        newColumnForExport(t("tutti.service.exportSumatra.header.totalWeight"), CatchRow.PROPERTY_TOTAL_WEIGHT, TuttiCsvUtil.PRIMITIVE_FLOAT);
        newColumnForExport(t("tutti.service.exportSumatra.header.averageWeight"), CatchRow.PROPERTY_AVERAGE_WEIGHT, TuttiCsvUtil.FLOAT);
        newColumnForExport(t("tutti.service.exportSumatra.header.averageSize"), CatchRow.PROPERTY_AVERAGE_SIZE, TuttiCsvUtil.FLOAT);
        newColumnForExport(t("tutti.service.exportSumatra.header.number"), CatchRow.PROPERTY_NUMBER, TuttiCsvUtil.PRIMITIVE_INTEGER);
        newColumnForExport(t("tutti.service.exportSumatra.header.moule"), CatchRow.PROPERTY_MOULE, TuttiCsvUtil.FLOAT);
        newColumnForExport(t("tutti.service.exportSumatra.header.startLatitude"), CatchRow.PROPERTY_GEAR_SHOOTING_START_LATITUDE, TuttiCsvUtil.FLOAT);
        newColumnForExport(t("tutti.service.exportSumatra.header.startLongitude"), CatchRow.PROPERTY_GEAR_SHOOTING_START_LONGITUDE, TuttiCsvUtil.FLOAT);
        newColumnForExport(t("tutti.service.exportSumatra.header.endLatitude"), CatchRow.PROPERTY_GEAR_SHOOTING_END_LATITUDE, TuttiCsvUtil.FLOAT);
        newColumnForExport(t("tutti.service.exportSumatra.header.endLongitude"), CatchRow.PROPERTY_GEAR_SHOOTING_END_LONGITUDE, TuttiCsvUtil.FLOAT);
        newColumnForExport(t("tutti.service.exportSumatra.header.startDate"), CatchRow.PROPERTY_GEAR_SHOOTING_START_DATE, TuttiCsvUtil.DAY_TIME_SECOND);
        newColumnForExport(t("tutti.service.exportSumatra.header.endDate"), CatchRow.PROPERTY_GEAR_SHOOTING_END_DATE, TuttiCsvUtil.DAY_TIME_SECOND);
    }

    @Override
    public CatchRow newEmptyInstance() {
        return new CatchRow();
    }

    public CatchRow newRow(FishingOperation operation, ExportBatchEntry entry) {
        CatchRow row = newEmptyInstance();
        row.setFishingOperation(operation);
        row.setSpecies(entry.getBatch().getSpecies());
        row.setSortedWeight(entry.getSortedWeight());
        float totalWeight = entry.getTotalWeight();
        int number = entry.getTotalNumber();

        row.setNumber(number);
        row.setTotalWeight(totalWeight);

        // average size
        row.setAverageSize(entry.getAverageFrequency());

        // avarage weight
        row.setAverageWeight(number == 0 ? null : totalWeight / (float) number);
        // moule
        row.setMoule(totalWeight == 0 ? null : (float) number / totalWeight);
        return row;
    }

}

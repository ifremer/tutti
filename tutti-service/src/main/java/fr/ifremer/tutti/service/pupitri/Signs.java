package fr.ifremer.tutti.service.pupitri;

/*
 * #%L
 * Tutti :: Service
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.adagio.core.dao.referential.pmfm.PmfmId;
import fr.ifremer.adagio.core.dao.referential.pmfm.QualitativeValueId;
import fr.ifremer.tutti.persistence.entities.referential.Caracteristic;
import fr.ifremer.tutti.persistence.entities.referential.CaracteristicQualitativeValue;
import fr.ifremer.tutti.persistence.entities.referential.CaracteristicQualitativeValues;

import java.util.Map;

/**
 * Created on 5/14/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.4.2
 */
public enum Signs {

    MALE("1", PmfmId.SEX, QualitativeValueId.SEX_MALE),
    FEMALE("2", PmfmId.SEX, QualitativeValueId.SEX_FEMALE),
    SMALL("P", PmfmId.SIZE_CATEGORY, QualitativeValueId.SIZE_SMALL),
    MEDIUM("M", PmfmId.SIZE_CATEGORY, QualitativeValueId.SIZE_MEDIUM),
    BIG("G", PmfmId.SIZE_CATEGORY, QualitativeValueId.SIZE_BIG),
    DEFAULT("0", PmfmId.SEX, QualitativeValueId.SEX_UNDEFINED),
    UNSORTED("H", null, null),
    MELAG("T", null, null);

    private final String sign;
    private final Integer category;
    private final Integer qualitativeValue;

    Signs(String sign, PmfmId pmfmId, QualitativeValueId qualitativeValueId) {
        this.sign = sign;
        this.category = pmfmId == null ? null : pmfmId.getValue();
        this.qualitativeValue = qualitativeValueId == null ? null : qualitativeValueId.getValue();
    }

    public String getSign() {
        return sign;
    }

    public static Signs getSign(String sign) {
        Signs result = null;
        for (Signs s : values()) {
            if (s.sign.equals(sign)) {
                result = s;
                break;
            }
        }
        return result;
    }

    public Integer getCategory() {
        return category;
    }


    public Integer getQualitativeValue() {
        return qualitativeValue;
    }

    public void registerSign(Caracteristic caracteristic, Map<Signs, CaracteristicQualitativeValue> map) {
        CaracteristicQualitativeValue result = CaracteristicQualitativeValues.getQualitativeValue(caracteristic, qualitativeValue);
        map.put(this, result);
    }
}

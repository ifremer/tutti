package fr.ifremer.tutti.service.genericformat.csv;

/*
 * #%L
 * Tutti :: Service
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.entities.data.SampleCategoryModelEntry;
import fr.ifremer.tutti.persistence.entities.referential.Caracteristic;

import java.io.Serializable;

/**
 * Created on 2/8/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.13
 */
public class SampleCategoryRow implements Serializable {

    private static final long serialVersionUID = 1L;

    public static final String PROPERTY_CODE = "code";

    public static final String PROPERTY_ORDER = "order";

    public static final String PROPERTY_CARACTERISTIC = "caracteristic";

    private SampleCategoryModelEntry sampleCategoryModelEntry;

    public SampleCategoryModelEntry getSampleCategoryModelEntry() {
        return sampleCategoryModelEntry;
    }

    public void setSampleCategoryModelEntry(SampleCategoryModelEntry sampleCategoryModelEntry) {
        this.sampleCategoryModelEntry = sampleCategoryModelEntry;
    }

    public String getCode() {
        return sampleCategoryModelEntry.getCode();
    }

    public Caracteristic getCaracteristic() {
        return sampleCategoryModelEntry.getCaracteristic();
    }

    public int getOrder() {
        return sampleCategoryModelEntry.getOrder();
    }

    public void setOrder(int order) {
        sampleCategoryModelEntry.setOrder(order);
    }

    public void setCaracteristic(Caracteristic caracteristic) {
        sampleCategoryModelEntry.setCaracteristic(caracteristic);
        if (caracteristic != null) {
            sampleCategoryModelEntry.setCategoryId(caracteristic.getIdAsInt());
        }
    }

    public void setCode(String code) {
        sampleCategoryModelEntry.setCode(code);
    }
}

package fr.ifremer.tutti.service.catches.multipost;

/*
 * #%L
 * Tutti :: Service
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2016 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.io.Files;
import fr.ifremer.adagio.core.dao.referential.ObjectTypeCode;
import fr.ifremer.tutti.persistence.entities.CaracteristicMap;
import fr.ifremer.tutti.persistence.entities.data.AccidentalBatch;
import fr.ifremer.tutti.persistence.entities.data.Attachment;
import fr.ifremer.tutti.persistence.entities.data.IndividualObservationBatch;
import fr.ifremer.tutti.persistence.entities.data.MarineLitterBatch;
import fr.ifremer.tutti.persistence.entities.data.SpeciesBatch;
import fr.ifremer.tutti.persistence.entities.data.SpeciesBatchFrequency;
import fr.ifremer.tutti.persistence.entities.referential.Caracteristic;
import fr.ifremer.tutti.service.PersistenceService;
import fr.ifremer.tutti.service.catches.multipost.csv.AccidentalCatchRow;
import fr.ifremer.tutti.service.catches.multipost.csv.AccidentalCatchRowModel;
import fr.ifremer.tutti.service.catches.multipost.csv.AttachmentRow;
import fr.ifremer.tutti.service.catches.multipost.csv.AttachmentRowModel;
import fr.ifremer.tutti.service.catches.multipost.csv.CaracteristicRow;
import fr.ifremer.tutti.service.catches.multipost.csv.CaracteristicRowModel;
import fr.ifremer.tutti.service.catches.multipost.csv.CatchFrequencyRow;
import fr.ifremer.tutti.service.catches.multipost.csv.CatchFrequencyRowModel;
import fr.ifremer.tutti.service.catches.multipost.csv.CatchRow;
import fr.ifremer.tutti.service.catches.multipost.csv.CatchRowModel;
import fr.ifremer.tutti.service.catches.multipost.csv.IndividualObservationRow;
import fr.ifremer.tutti.service.catches.multipost.csv.IndividualObservationRowModel;
import fr.ifremer.tutti.service.catches.multipost.csv.MarineLitterRow;
import fr.ifremer.tutti.service.catches.multipost.csv.MarineLitterRowModel;
import org.nuiton.csv.Export;
import org.nuiton.csv.ExportModel;
import org.nuiton.jaxx.application.ApplicationIOUtil;
import org.nuiton.jaxx.application.ApplicationTechnicalException;

import java.io.Closeable;
import java.io.File;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import static org.nuiton.i18n.I18n.n;
import static org.nuiton.i18n.I18n.t;

/**
 * Created on 09/03/16.
 *
 * @author Tony Chemit - chemit@codelutin.com
 */
public class MultiPostExportContext implements MultiPostConstants, Closeable {

    private final File targetFile;
    private final File temporaryDirectory;

    private final PersistenceService persistenceService;

    private final List<AttachmentRow> attachmentRows = new ArrayList<>();

    /**
     * Dictionnaire de translation des ids de lots → id d'export
     */
    private final Map<Integer, String> speciesOrBenthosIdTranslations = new TreeMap<>();
    private final List<CatchRow> speciesOrBenthosRows = new ArrayList<>();
    private final List<CatchFrequencyRow> frequencyRows = new ArrayList<>();

    private final List<AccidentalCatchRow> accidentialCatchRows = new ArrayList<>();
    private final List<MarineLitterRow> marineLitterRows = new ArrayList<>();
    private final List<IndividualObservationRow> individualObservationRows = new ArrayList<>();
    private final List<CaracteristicRow> caracteristicRows = new ArrayList<>();

    private final List<File> file2zip = new ArrayList<>();

    public MultiPostExportContext(File targetFile, PersistenceService persistenceService) {
        this.targetFile = targetFile;
        this.temporaryDirectory = Files.createTempDir();
        this.persistenceService = persistenceService;
    }

    public void addAttachments(String batchId, int objectId, ObjectTypeCode objectType) {
        List<Attachment> attachments =
                persistenceService.getAllAttachments(objectType, objectId);
        for (Attachment attachment : attachments) {
            AttachmentRow attachmentRow = new AttachmentRow();
            attachmentRow.setBatchId(batchId);
            attachmentRow.setName(attachment.getName());
            attachmentRow.setComment(attachment.getComment());
            attachmentRow.setFile(persistenceService.getAttachmentFile(attachment.getId()));
            attachmentRows.add(attachmentRow);
        }
    }

    public void storeSpeciesOrBenthosBatches(String fileName, boolean exportFrequencies, boolean exportIndividualObservations) {

        export(fileName,
               CatchRowModel.forExport(),
               speciesOrBenthosRows,
               n("tutti.service.multipost.export.batches.error"));

        if (exportFrequencies) {
            storeFrequencies();
        }

        if (exportIndividualObservations) {
            storeIndividualObservations();
        }

    }

    public void storeFrequencies() {

        export(FREQUENCIES_FILE,
               CatchFrequencyRowModel.forExport(),
               frequencyRows,
               n("tutti.service.multipost.export.frequencies.error"));

    }

    public void storeIndividualObservations() {

        export(INDIVIDUAL_OBSERVATION_FILE,
               IndividualObservationRowModel.forExport(),
               individualObservationRows,
               n("tutti.service.multipost.export.batches.error"));

        export(CARACTERISTIC_FILE,
               CaracteristicRowModel.forExport(),
               caracteristicRows,
               n("tutti.service.multipost.export.batches.error"));

    }


    public void storeAttachments() {

        AttachmentRowModel csvAttachmentModel = AttachmentRowModel.forExport();

        File attachmentDirectory = new File(temporaryDirectory, ATTACHMENTS_DIRECTORY);
        ApplicationIOUtil.forceMkdir(attachmentDirectory, t("tutti.service.multipost.attachment.mkdir.error", attachmentDirectory));
        addFile(attachmentDirectory);
        for (AttachmentRow attachmentRow : attachmentRows) {
            File attachmentFile = attachmentRow.getFile();
            File destFile = new File(attachmentDirectory, attachmentFile.getName());
            ApplicationIOUtil.copyFile(attachmentFile,
                                       destFile,
                                       t("tutti.service.multipost.attachment.copy.error", attachmentFile));
            addFile(destFile);
        }

        export(ATTACHMENTS_FILE,
               csvAttachmentModel,
               attachmentRows,
               n("tutti.service.multipost.export.attachments.error"));

    }


    @Override
    public void close() {

        storeAttachments();

        try {
            ApplicationIOUtil.zip(temporaryDirectory,
                                  targetFile,
                                  file2zip,
                                  n("tutti.service.multipost.export.error"));
        } finally {
            ApplicationIOUtil.deleteDirectory(temporaryDirectory, t("tutti.service.multipost.export.deleteTempDirectory.error", targetFile));
        }
    }

    public void addSpeciesOrBenthosBatch(String batchId, String parentId, SpeciesBatch batch) {

        CatchRow row = new CatchRow();
        row.setId(batchId);
        row.setParentId(parentId);
        row.setSpecies(batch.getSpecies());

        row.setCategoryId(batch.getSampleCategoryId());
        row.setCategoryValue(batch.getSampleCategoryValue());
        row.setCategoryWeight(batch.getSampleCategoryWeight());
        row.setWeight(batch.getWeight());
        row.setNumber(batch.getNumber());
        row.setComment(batch.getComment());
        row.setToConfirm(batch.isSpeciesToConfirm());
        speciesOrBenthosRows.add(row);
        speciesOrBenthosIdTranslations.put(batch.getIdAsInt(), batchId);

        addAttachments(batchId, batch.getIdAsInt(), ObjectTypeCode.BATCH);

    }

    public void addFrequencies(String rowId, List<SpeciesBatchFrequency> frequencies) {

        for (SpeciesBatchFrequency frequency : frequencies) {
            CatchFrequencyRow frequencyRow = new CatchFrequencyRow();
            frequencyRow.setBatchId(rowId);
            frequencyRow.setLengthStepCaracteristic(frequency.getLengthStepCaracteristic());
            frequencyRow.setLengthStep(frequency.getLengthStep());
            frequencyRow.setNumber(frequency.getNumber());
            frequencyRow.setWeight(frequency.getWeight());
            frequencyRow.setSpecies(frequency.getBatch().getSpecies());
            frequencyRows.add(frequencyRow);
        }

    }

    public void addIndividualObservations(String batchId, IndividualObservationBatch batch) {

        IndividualObservationRow row = new IndividualObservationRow();
        row.setBatchId(batchId);

        // on conserve l'id d'export du lot parent de l'observation
        String speciesOrBenthosBatchId = speciesOrBenthosIdTranslations.get(batch.getBatchId());
        row.setSpeciesBatchId(speciesOrBenthosBatchId);

        row.setSpecies(batch.getSpecies());
        row.setWeight(batch.getWeight());
        row.setSize(batch.getSize());
        row.setLengthStepCaracteristic(batch.getLengthStepCaracteristic());
        row.setSamplingCode(batch.getSamplingCode());
        row.setComment(batch.getComment());
        row.setCopyIndividualObservationMode(batch.getCopyIndividualObservationMode());

        individualObservationRows.add(row);

        addCaracteristics(batchId, batch.getCaracteristics());

        addAttachments(batchId, batch.getIdAsInt(), ObjectTypeCode.SAMPLE);

    }

    public void addMarineLitterBatch(String batchId, MarineLitterBatch batch) {

        MarineLitterRow row = new MarineLitterRow();
        row.setBatchId(batchId);

        row.setCategory(batch.getMarineLitterCategory());
        row.setSizeCategory(batch.getMarineLitterSizeCategory());
        row.setNumber(batch.getNumber());
        row.setWeight(batch.getWeight());
        row.setComment(batch.getComment());

        marineLitterRows.add(row);

        addAttachments(batchId, batch.getIdAsInt(), ObjectTypeCode.BATCH);

    }

    public void addAccidentalCatch(String batchId, AccidentalBatch batch) {

        AccidentalCatchRow row = new AccidentalCatchRow();

        row.setBatchId(batchId);

        row.setSpecies(batch.getSpecies());
        row.setGender(batch.getGender());
        row.setWeight(batch.getWeight());
        row.setSize(batch.getSize());
        row.setLengthStepCaracteristic(batch.getLengthStepCaracteristic());
        row.setDeadOrAlive(batch.getDeadOrAlive());
        row.setComment(batch.getComment());

        accidentialCatchRows.add(row);

        addCaracteristics(batchId, batch.getCaracteristics());

        addAttachments(batchId, batch.getIdAsInt(), ObjectTypeCode.SAMPLE);

    }

    public <R> void export(String fileName,
                           ExportModel<R> exportModel,
                           List<R> rows,
                           String errorMessage) {

        File file = new File(temporaryDirectory, fileName);

        addFile(file);

        try (Writer writer = ApplicationIOUtil.newWriter(file, n("tutti.service.multipost.export.file.writer.error"))) {

            Export export = Export.newExport(exportModel, rows);
            export.write(writer);

        } catch (Exception e) {
            throw new ApplicationTechnicalException(t(errorMessage, file), e);
        }

    }

    public void storeAccidentalCatches() {

        export(ACCIDENTAL_CATCHES_FILE,
               AccidentalCatchRowModel.forExport(),
               accidentialCatchRows,
               n("tutti.service.multipost.export.batches.error"));

        // export caracteristics

        export(CARACTERISTIC_FILE,
               CaracteristicRowModel.forExport(),
               caracteristicRows,
               n("tutti.service.multipost.export.batches.error"));

    }

    public void storeMarineLitterBatches() {

        export(MARINE_LITTER_FILE,
               MarineLitterRowModel.forExport(),
               marineLitterRows,
               n("tutti.service.multipost.export.batches.error"));

    }

    protected void addFile(File file) {
        file2zip.add(file);
    }

    protected void addCaracteristics(String batchId, CaracteristicMap caracteristicMap) {

        for (Caracteristic caracteristic : caracteristicMap.keySet()) {
            CaracteristicRow caracteristicRow = new CaracteristicRow();
            caracteristicRow.setBatchId(batchId);
            caracteristicRow.setCaracteristic(caracteristic);
            caracteristicRow.setValue(caracteristicMap.get(caracteristic));
            caracteristicRows.add(caracteristicRow);
        }

    }
}

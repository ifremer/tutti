package fr.ifremer.tutti.service.catches.multipost.csv;

/*
 * #%L
 * Tutti :: Service
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.entities.referential.Species;
import fr.ifremer.tutti.service.catches.multipost.MultiPostConstants;
import fr.ifremer.tutti.service.csv.AbstractTuttiImportExportModel;
import fr.ifremer.tutti.service.csv.TuttiCsvUtil;

import java.util.List;

/**
 * Model of a catch export.
 *
 * @author Kevin Morin - kmorin@codelutin.com
 * @since 2.2
 */
public class CatchRowModel extends AbstractTuttiImportExportModel<CatchRow> {

    private CatchRowModel(List<Species> species) {
        super(MultiPostConstants.CSV_SEPARATOR);

        newColumnForImportExport(CatchRow.ID);

        newColumnForImportExport(CatchRow.PARENT_ID);

        newColumnForExport(CatchRow.SPECIES, TuttiCsvUtil.SPECIES_TECHNICAL_FORMATTER);
        newSpeciesForeignKeyColumn(CatchRow.SPECIES, species);

        newColumnForImportExport(CatchRow.CATEGORY_ID, TuttiCsvUtil.INTEGER);

        newColumnForExport(CatchRow.CATEGORY_VALUE, TuttiCsvUtil.CARACTERISTIC_VALUE_TECHNICAL_FORMATTER);
        newMandatoryColumn(CatchRow.CATEGORY_VALUE, TuttiCsvUtil.STRING);

        newColumnForImportExport(CatchRow.CATEGORY_WEIGHT, TuttiCsvUtil.WEIGHT_PARSER_FORMATTER);

        newColumnForImportExport(CatchRow.WEIGHT, TuttiCsvUtil.WEIGHT_PARSER_FORMATTER);

        newColumnForImportExport(CatchRow.NUMBER, TuttiCsvUtil.INTEGER);

        newColumnForImportExport(CatchRow.COMMENT);

        newColumnForImportExport(CatchRow.TO_CONFIRM, TuttiCsvUtil.PRIMITIVE_BOOLEAN);

    }

    public static CatchRowModel forExport() {
        return new CatchRowModel(null);
    }

    public static CatchRowModel forImport(List<Species> species) {
        return new CatchRowModel(species);
    }

    @Override
    public CatchRow newEmptyInstance() {
        return new CatchRow();
    }
}

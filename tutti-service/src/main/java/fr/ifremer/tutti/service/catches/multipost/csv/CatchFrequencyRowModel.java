package fr.ifremer.tutti.service.catches.multipost.csv;

/*
 * #%L
 * Tutti :: Service
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.entities.referential.Caracteristic;
import fr.ifremer.tutti.persistence.entities.referential.Species;
import fr.ifremer.tutti.service.catches.multipost.MultiPostConstants;
import fr.ifremer.tutti.service.csv.AbstractTuttiImportExportModel;
import fr.ifremer.tutti.service.csv.TuttiCsvUtil;

import java.util.List;

/**
 * Model of a catch export.
 *
 * @author Kevin Morin - kmorin@codelutin.com
 * @since 2.2
 */
public class CatchFrequencyRowModel extends AbstractTuttiImportExportModel<CatchFrequencyRow> {

    private CatchFrequencyRowModel(List<Species> species, List<Caracteristic> caracteristics) {

        super(MultiPostConstants.CSV_SEPARATOR);

        newColumnForImportExport(CatchFrequencyRow.BATCH_ID);

        newColumnForExport(CatchFrequencyRow.FREQUENCY_LENGTH_STEP_CARACTERISTIC, TuttiCsvUtil.CARACTERISTIC_TECHNICAL_FORMATTER);
        newForeignKeyColumn(CatchFrequencyRow.FREQUENCY_LENGTH_STEP_CARACTERISTIC, Caracteristic.class, caracteristics);

        newColumnForImportExport(CatchFrequencyRow.FREQUENCY_LENGTH_STEP, TuttiCsvUtil.FLOAT);

        newColumnForImportExport(CatchFrequencyRow.FREQUENCY_NUMBER, TuttiCsvUtil.INTEGER);

        newColumnForImportExport(CatchFrequencyRow.FREQUENCY_WEIGHT, TuttiCsvUtil.WEIGHT_PARSER_FORMATTER);

        newColumnForExport(CatchFrequencyRow.SPECIES, TuttiCsvUtil.SPECIES_TECHNICAL_FORMATTER);
        newSpeciesForeignKeyColumn(CatchFrequencyRow.SPECIES, species);

    }

    public static CatchFrequencyRowModel forExport() {
        return new CatchFrequencyRowModel(null, null);
    }

    public static CatchFrequencyRowModel forImport(List<Species> species, List<Caracteristic> caracteristics) {
        return new CatchFrequencyRowModel(species, caracteristics);
    }

    @Override
    public CatchFrequencyRow newEmptyInstance() {
        return new CatchFrequencyRow();
    }
}

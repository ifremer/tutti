package fr.ifremer.tutti.service.genericformat.importactions;

/*
 * #%L
 * Tutti :: Service
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.entities.data.Cruise;
import fr.ifremer.tutti.persistence.entities.referential.Gear;
import fr.ifremer.tutti.service.genericformat.GenericFormatCsvFileResult;
import fr.ifremer.tutti.service.genericformat.GenericFormatImportContext;
import fr.ifremer.tutti.service.genericformat.GenericFormatImportCruiseContext;
import fr.ifremer.tutti.service.genericformat.GenericformatImportPersistenceHelper;
import fr.ifremer.tutti.service.genericformat.consumer.CsvConsumerForGearCaracteristic;
import fr.ifremer.tutti.service.genericformat.csv.GearCaracteristicRow;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.csv.ImportRow;
import org.nuiton.csv.ImportRuntimeException;
import org.nuiton.jaxx.application.ApplicationTechnicalException;

import java.io.IOException;
import java.util.Set;

import static org.nuiton.i18n.I18n.t;

/**
 * Created on 3/3/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.14
 */
public class ImportGearCaracteristicAction extends ImportActionSupport {

    /** Logger. */
    private static final Log log = LogFactory.getLog(ImportGearCaracteristicAction.class);

    private final GenericformatImportPersistenceHelper persistenceHelper;

    public ImportGearCaracteristicAction(GenericFormatImportContext importContext, GenericformatImportPersistenceHelper persistenceHelper) {
        super(importContext);
        this.persistenceHelper = persistenceHelper;
    }

    @Override
    protected boolean canExecute() {
        return importContext.isTechnicalFilesValid() && importContext.getSurveyFileResult().isValid();
    }

    @Override
    protected void skipExecute() {
        importContext.increments(t("tutti.service.genericFormat.skip.import.gearCaracteristics"));
    }

    @Override
    protected void doExecute() {

        if (log.isInfoEnabled()) {
            log.info("Import gearCaracteristics.csv file.");
        }
        importContext.increments(t("tutti.service.genericFormat.import.gearCaracteristics"));
        GenericFormatCsvFileResult importFileResult = importContext.getGearCaracteristicFileResult();
        try (CsvConsumerForGearCaracteristic consumer = importContext.loadGearCaracteristics(true)) {
            for (ImportRow<GearCaracteristicRow> row : consumer) {

                GenericFormatImportCruiseContext cruiseContext = consumer.validateRow(row, importContext);

                if (cruiseContext != null) {

                    consumer.prepareRowForPersist(cruiseContext, row);

                }

            }
        } catch (IOException e) {
            throw new ApplicationTechnicalException("Could not close gearCaracteristic.csv file", e);
        } catch (ImportRuntimeException e) {

            importFileResult.addGlobalError(e.getMessage());

        }

        persistGearCaracteristics();

    }

    public void persistGearCaracteristics() {

        importContext.doActionOnCruiseContexts((cruiseContext, progressionModel) -> {

            importContext.increments(t("tutti.service.genericFormat.persist.gearCaracteristics", cruiseContext.getCruiseLabel()));

            if (cruiseContext.withGearCaracteristics()) {

                Set<Gear> gears = cruiseContext.getGearsWithcaracteristics();

                Cruise cruise = cruiseContext.getCruise();

                for (Gear gear : gears) {

                    if (log.isInfoEnabled()) {
                        log.info("Persist " + gear.getCaracteristics().size() + " gear caracteristics for gear: " + gear.getName() + " for cruise: " + cruiseContext.getCruiseLabel());
                    }
                    persistenceHelper.saveGearCaracteristics(gear, cruise);

                }

            }

        });

    }

}

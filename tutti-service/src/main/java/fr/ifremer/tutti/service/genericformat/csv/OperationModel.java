package fr.ifremer.tutti.service.genericformat.csv;

/*
 * #%L
 * Tutti :: Service
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.entities.data.Cruise;
import fr.ifremer.tutti.persistence.entities.data.FishingOperation;
import fr.ifremer.tutti.service.csv.AbstractTuttiImportExportModel;
import fr.ifremer.tutti.service.csv.TuttiCsvUtil;
import fr.ifremer.tutti.service.genericformat.GenericFormatImportEntityParserFactory;

/**
 * Model of a operation export.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.3
 */
public class OperationModel extends AbstractTuttiImportExportModel<OperationRow> {

    public static OperationModel forExport(char separator) {

        OperationModel exportModel = new OperationModel(separator);
        exportModel.forExport();
        return exportModel;

    }

    public static OperationModel forImport(char separator, GenericFormatImportEntityParserFactory parserFactory) {

        OperationModel importModel = new OperationModel(separator);
        importModel.forImport(parserFactory);
        return importModel;

    }

    @Override
    public OperationRow newEmptyInstance() {
        return OperationRow.newEmptyInstance();
    }

    protected OperationModel(char separator) {
        super(separator);

    }

    protected void forExport() {

        newColumnForExport("Annee", Cruise.PROPERTY_BEGIN_DATE, TuttiCsvUtil.YEAR);
        newColumnForExport("Serie", Cruise.PROPERTY_PROGRAM, TuttiCsvUtil.PROGRAM_FORMATTER);
        newColumnForExport("Serie_Partielle", Cruise.PROPERTY_SURVEY_PART);

        newColumnForExport("Code_Station", FishingOperation.PROPERTY_STATION_NUMBER);
        newColumnForExport("Id_Operation", FishingOperation.PROPERTY_FISHING_OPERATION_NUMBER, TuttiCsvUtil.INTEGER);
        newColumnForExport("Poche", FishingOperation.PROPERTY_MULTIRIG_AGGREGATION);
        newNullableColumnForExport("Engin", FishingOperation.PROPERTY_GEAR, TuttiCsvUtil.GEAR_FORMATTER);
        newNullableColumnForExport("Rang_Engin", OperationRow.PROPERTY_GEAR_RANK_ORDER, TuttiCsvUtil.SHORT);

        newColumnForExport("Navire", FishingOperation.PROPERTY_VESSEL, TuttiCsvUtil.VESSEL_FORMATTER);
        newColumnForExport("DateDeb", FishingOperation.PROPERTY_GEAR_SHOOTING_START_DATE, TuttiCsvUtil.DAY_TIME_SECOND);
        newColumnForExport("LatDeb", FishingOperation.PROPERTY_GEAR_SHOOTING_START_LATITUDE, TuttiCsvUtil.FLOAT);
        newColumnForExport("LongDeb", FishingOperation.PROPERTY_GEAR_SHOOTING_START_LONGITUDE, TuttiCsvUtil.FLOAT);
        newColumnForExport("DateFin", FishingOperation.PROPERTY_GEAR_SHOOTING_END_DATE, TuttiCsvUtil.DAY_TIME_SECOND);
        newColumnForExport("LatFin", FishingOperation.PROPERTY_GEAR_SHOOTING_END_LATITUDE, TuttiCsvUtil.FLOAT);
        newColumnForExport("LongFin", FishingOperation.PROPERTY_GEAR_SHOOTING_END_LONGITUDE, TuttiCsvUtil.FLOAT);
        newColumnForExport("Duree", OperationRow.PROPERTY_DURATION);
        newColumnForExport("Strate", FishingOperation.PROPERTY_STRATA, TuttiCsvUtil.FISHING_OPERATION_STRATA_FORMATTER);
        newColumnForExport("Sous_Strate", FishingOperation.PROPERTY_SUB_STRATA, TuttiCsvUtil.FISHING_OPERATION_SUB_STRATA_FORMATTER);
        newColumnForExport("Localite", FishingOperation.PROPERTY_LOCATION, TuttiCsvUtil.FISHING_OPERATION_LOCATION_FORMATTER);

        newColumnForExport("Validite_OP", FishingOperation.PROPERTY_FISHING_OPERATION_VALID, TuttiCsvUtil.BOOLEAN);
        newColumnForExport("Rectiligne", FishingOperation.PROPERTY_FISHING_OPERATION_RECTILIGNE, TuttiCsvUtil.BOOLEAN);
        newColumnForExport("Distance", FishingOperation.PROPERTY_TRAWL_DISTANCE, TuttiCsvUtil.INTEGER_NULL_TO_9);
        newColumnForExport("Saisisseur", FishingOperation.PROPERTY_RECORDER_PERSON, TuttiCsvUtil.PERSON_LIST_FORMATTER);
        newColumnForExport("Navire_Associe", FishingOperation.PROPERTY_SECONDARY_VESSEL, TuttiCsvUtil.VESSEL_LIST_FORMATTER);
        newColumnForExport("Commentaire", FishingOperation.PROPERTY_COMMENT, TuttiCsvUtil.COMMENT_PARSER_FORMATTER);

        newNullableColumnForExport("Poids_Total", OperationRow.PROPERTY_CATCH_TOTAL_WEIGHT, TuttiCsvUtil.WEIGHT_NULL_TO_9);
        newNullableColumnForExport("Poids_Total_Calcule", OperationRow.PROPERTY_CATCH_TOTAL_WEIGHT_COMPUTED, TuttiCsvUtil.BOOLEAN);

        newNullableColumnForExport("Poids_Total_Vrac", OperationRow.PROPERTY_CATCH_TOTAL_SORTED_WEIGHT, TuttiCsvUtil.WEIGHT_NULL_TO_9);
        newNullableColumnForExport("Poids_Total_Vrac_Calcule", OperationRow.PROPERTY_CATCH_TOTAL_SORTED_WEIGHT_COMPUTED, TuttiCsvUtil.BOOLEAN);

        newNullableColumnForExport("Poids_Total_HorsVrac", OperationRow.PROPERTY_CATCH_TOTAL_UNSORTED_WEIGHT, TuttiCsvUtil.WEIGHT_NULL_TO_9);
        newNullableColumnForExport("Poids_Total_HorsVrac_Calcule", OperationRow.PROPERTY_CATCH_TOTAL_UNSORTED_WEIGHT_COMPUTED, TuttiCsvUtil.BOOLEAN);

        newNullableColumnForExport("Poids_Total_Non_Trie", OperationRow.PROPERTY_CATCH_TOTAL_REJECTED_WEIGHT, TuttiCsvUtil.WEIGHT_NULL_TO_9);
        newNullableColumnForExport("Poids_Total_Non_Trie_Calcule", OperationRow.PROPERTY_CATCH_TOTAL_REJECTED_WEIGHT_COMPUTED, TuttiCsvUtil.BOOLEAN);

        newNullableColumnForExport("Poids_Total_Tremis", OperationRow.PROPERTY_CATCH_TOTAL_SORTED_TREMIS_WEIGHT, TuttiCsvUtil.WEIGHT_NULL_TO_9);
        newNullableColumnForExport("Poids_Total_Tremis_Calcule", OperationRow.PROPERTY_CATCH_TOTAL_SORTED_TREMIS_WEIGHT_COMPUTED, TuttiCsvUtil.BOOLEAN);

        newNullableColumnForExport("Poids_Total_Carroussel", OperationRow.PROPERTY_CATCH_TOTAL_SORTED_CAROUSSEL_WEIGHT, TuttiCsvUtil.WEIGHT_NULL_TO_9);
        newNullableColumnForExport("Poids_Total_Carroussel_Calcule", OperationRow.PROPERTY_CATCH_TOTAL_SORTED_CAROUSSEL_WEIGHT_COMPUTED, TuttiCsvUtil.BOOLEAN);

        newNullableColumnForExport("Poids_Total_Espece", OperationRow.PROPERTY_SPECIES_TOTAL_WEIGHT, TuttiCsvUtil.WEIGHT_NULL_TO_9);
        newNullableColumnForExport("Poids_Total_Espece_Calcule", OperationRow.PROPERTY_SPECIES_TOTAL_WEIGHT_COMPUTED, TuttiCsvUtil.BOOLEAN);

        newNullableColumnForExport("Poids_Total_Espece_Vrac", OperationRow.PROPERTY_SPECIES_TOTAL_SORTED_WEIGHT, TuttiCsvUtil.WEIGHT_NULL_TO_9);
        newNullableColumnForExport("Poids_Total_Espece_Vrac_Calcule", OperationRow.PROPERTY_SPECIES_TOTAL_SORTED_WEIGHT_COMPUTED, TuttiCsvUtil.BOOLEAN);

        newNullableColumnForExport("Poids_Total_Espece_Vrac_Trie", OperationRow.PROPERTY_SPECIES_TOTAL_SAMPLE_SORTED_WEIGHT, TuttiCsvUtil.WEIGHT_NULL_TO_9);
        newNullableColumnForExport("Poids_Total_Espece_Vrac_Trie_Calcule", OperationRow.PROPERTY_SPECIES_TOTAL_SAMPLE_SORTED_WEIGHT_COMPUTED, TuttiCsvUtil.BOOLEAN);

        newNullableColumnForExport("Poids_Total_Espece_HorsVrac", OperationRow.PROPERTY_SPECIES_TOTAL_UNSORTED_WEIGHT, TuttiCsvUtil.WEIGHT_NULL_TO_9);
        newNullableColumnForExport("Poids_Total_Espece_HorsVrac_Calcule", OperationRow.PROPERTY_SPECIES_TOTAL_UNSORTED_WEIGHT_COMPUTED, TuttiCsvUtil.BOOLEAN);

        newNullableColumnForExport("Poids_Total_Espece_Inerte_Trie", OperationRow.PROPERTY_SPECIES_TOTAL_INERT_WEIGHT, TuttiCsvUtil.WEIGHT_NULL_TO_9);
        newNullableColumnForExport("Poids_Total_Espece_Inerte_Trie_Calcule", OperationRow.PROPERTY_SPECIES_TOTAL_INERT_WEIGHT_COMPUTED, TuttiCsvUtil.BOOLEAN);

        newNullableColumnForExport("Poids_Total_Espece_Vivant_non_detaille_trie", OperationRow.PROPERTY_SPECIES_TOTAL_LIVING_NOT_ITEMIZED_WEIGHT, TuttiCsvUtil.WEIGHT_NULL_TO_9);
        newNullableColumnForExport("Poids_Total_Espece_Vivant_non_detaille_trie_Calcule", OperationRow.PROPERTY_SPECIES_TOTAL_LIVING_NOT_ITEMIZED_WEIGHT_COMPUTED, TuttiCsvUtil.BOOLEAN);

        newNullableColumnForExport("Poids_Total_Benthos", OperationRow.PROPERTY_BENTHOS_TOTAL_WEIGHT, TuttiCsvUtil.WEIGHT_NULL_TO_9);
        newNullableColumnForExport("Poids_Total_Benthos_Calcule", OperationRow.PROPERTY_BENTHOS_TOTAL_WEIGHT_COMPUTED, TuttiCsvUtil.BOOLEAN);

        newNullableColumnForExport("Poids_Total_Benthos_Vrac", OperationRow.PROPERTY_BENTHOS_TOTAL_SORTED_WEIGHT, TuttiCsvUtil.WEIGHT_NULL_TO_9);
        newNullableColumnForExport("Poids_Total_Benthos_Vrac_Calcule", OperationRow.PROPERTY_BENTHOS_TOTAL_SORTED_WEIGHT_COMPUTED, TuttiCsvUtil.BOOLEAN);

        newNullableColumnForExport("Poids_Total_Benthos_Vrac_Trie", OperationRow.PROPERTY_BENTHOS_TOTAL_SAMPLE_SORTED_WEIGHT, TuttiCsvUtil.WEIGHT_NULL_TO_9);
        newNullableColumnForExport("Poids_Total_Benthos_Vrac_Trie_Calcule", OperationRow.PROPERTY_BENTHOS_TOTAL_SAMPLE_SORTED_WEIGHT_COMPUTED, TuttiCsvUtil.BOOLEAN);

        newNullableColumnForExport("Poids_Total_Benthos_HorsVrac", OperationRow.PROPERTY_BENTHOS_TOTAL_UNSORTED_WEIGHT, TuttiCsvUtil.WEIGHT_NULL_TO_9);
        newNullableColumnForExport("Poids_Total_Benthos_HorsVrac_Calcule", OperationRow.PROPERTY_BENTHOS_TOTAL_UNSORTED_WEIGHT_COMPUTED, TuttiCsvUtil.BOOLEAN);

        newNullableColumnForExport("Poids_Total_Benthos_Inerte_Trie", OperationRow.PROPERTY_BENTHOS_TOTAL_INERT_WEIGHT, TuttiCsvUtil.WEIGHT_NULL_TO_9);
        newNullableColumnForExport("Poids_Total_Benthos_Inerte_Trie_Calcule", OperationRow.PROPERTY_BENTHOS_TOTAL_INERT_WEIGHT_COMPUTED, TuttiCsvUtil.BOOLEAN);

        newNullableColumnForExport("Poids_Total_Benthos_Vivant_non_detaille_trie", OperationRow.PROPERTY_BENTHOS_TOTAL_LIVING_NOT_ITEMIZED_WEIGHT, TuttiCsvUtil.WEIGHT_NULL_TO_9);
        newNullableColumnForExport("Poids_Total_Benthos_Vivant_non_detaille_trie_Calcule", OperationRow.PROPERTY_BENTHOS_TOTAL_LIVING_NOT_ITEMIZED_WEIGHT_COMPUTED, TuttiCsvUtil.BOOLEAN);

        newNullableColumnForExport("Poids_Total_Macro_Dechet", OperationRow.PROPERTY_MARINE_LITTER_TOTAL_WEIGHT, TuttiCsvUtil.WEIGHT_NULL_TO_9);
        newNullableColumnForExport("Poids_Total_Macro_Dechet_Calcule", OperationRow.PROPERTY_MARINE_LITTER_TOTAL_WEIGHT_COMPUTED, TuttiCsvUtil.BOOLEAN);

        newColumnForExport("Serie_Id", Cruise.PROPERTY_PROGRAM, TuttiCsvUtil.PROGRAM_TECHNICAL_FORMATTER);
        newColumnForExport("Engin_Id", FishingOperation.PROPERTY_GEAR, TuttiCsvUtil.GEAR_TECHNICAL_FORMATTER);
        newColumnForExport("Fishing_Operation_Id", OperationRow.PROPERTY_FISHING_OPERATION_OBJECT_ID, TuttiCsvUtil.INTEGER);
        newColumnForExport("Catch_Lot_Id", OperationRow.PROPERTY_CATCH_OBJECT_ID, TuttiCsvUtil.INTEGER);
        newColumnForExport("Navire_Id", FishingOperation.PROPERTY_VESSEL, TuttiCsvUtil.VESSEL_TECHNICAL_FORMATTER);
        newColumnForExport("Strate_Id", FishingOperation.PROPERTY_STRATA, TuttiCsvUtil.FISHING_OPERATION_STRATA_TECHNICAL_FORMATTER);
        newColumnForExport("Sous_Strate_Id", FishingOperation.PROPERTY_SUB_STRATA, TuttiCsvUtil.FISHING_OPERATION_SUB_STRATA_TECHNICAL_FORMATTER);
        newColumnForExport("Localite_Id", FishingOperation.PROPERTY_LOCATION, TuttiCsvUtil.FISHING_OPERATION_LOCATION_TECHNICAL_FORMATTER);
        newColumnForExport("Saisisseur_Id", FishingOperation.PROPERTY_RECORDER_PERSON, TuttiCsvUtil.PERSON_LIST_TECHNICAL_FORMATTER);
        newColumnForExport("Navire_Associe_Id", FishingOperation.PROPERTY_SECONDARY_VESSEL, TuttiCsvUtil.VESSEL_LIST_TECHNICAL_FORMATTER);

    }

    protected void forImport(GenericFormatImportEntityParserFactory parserFactory) {

        newMandatoryColumn("Annee", Cruise.PROPERTY_BEGIN_DATE, TuttiCsvUtil.YEAR);
        newIgnoredColumn("Serie");
        newMandatoryColumn("Serie_Partielle", Cruise.PROPERTY_SURVEY_PART);

        newMandatoryColumn("Code_Station", FishingOperation.PROPERTY_STATION_NUMBER);
        newMandatoryColumn("Id_Operation", FishingOperation.PROPERTY_FISHING_OPERATION_NUMBER, TuttiCsvUtil.INTEGER);
        newMandatoryColumn("Poche", FishingOperation.PROPERTY_MULTIRIG_AGGREGATION);
        newIgnoredColumn("Engin");
        newMandatoryColumn("Rang_Engin", OperationRow.PROPERTY_GEAR_RANK_ORDER, TuttiCsvUtil.SHORT);

        newIgnoredColumn("Navire");
        newMandatoryColumn("DateDeb", FishingOperation.PROPERTY_GEAR_SHOOTING_START_DATE, TuttiCsvUtil.DAY_TIME_SECOND);
        newMandatoryColumn("LatDeb", FishingOperation.PROPERTY_GEAR_SHOOTING_START_LATITUDE, TuttiCsvUtil.FLOAT);
        newMandatoryColumn("LongDeb", FishingOperation.PROPERTY_GEAR_SHOOTING_START_LONGITUDE, TuttiCsvUtil.FLOAT);
        newMandatoryColumn("DateFin", FishingOperation.PROPERTY_GEAR_SHOOTING_END_DATE, TuttiCsvUtil.DAY_TIME_SECOND);
        newMandatoryColumn("LatFin", FishingOperation.PROPERTY_GEAR_SHOOTING_END_LATITUDE, TuttiCsvUtil.FLOAT);
        newMandatoryColumn("LongFin", FishingOperation.PROPERTY_GEAR_SHOOTING_END_LONGITUDE, TuttiCsvUtil.FLOAT);
        newIgnoredColumn("Duree");
        newIgnoredColumn("Strate");
        newIgnoredColumn("Sous_Strate");
        newIgnoredColumn("Localite");
        newMandatoryColumn("Validite_OP", FishingOperation.PROPERTY_FISHING_OPERATION_VALID, TuttiCsvUtil.BOOLEAN);
        newMandatoryColumn("Rectiligne", FishingOperation.PROPERTY_FISHING_OPERATION_RECTILIGNE, TuttiCsvUtil.BOOLEAN);
        newMandatoryColumn("Distance", FishingOperation.PROPERTY_TRAWL_DISTANCE, TuttiCsvUtil.INTEGER_NULL_TO_9);
        newIgnoredColumn("Saisisseur");
        newIgnoredColumn("Navire_Associe");
        newMandatoryColumn("Commentaire", FishingOperation.PROPERTY_COMMENT, TuttiCsvUtil.COMMENT_PARSER_FORMATTER);

        newMandatoryColumn("Poids_Total", OperationRow.PROPERTY_CATCH_TOTAL_WEIGHT, TuttiCsvUtil.WEIGHT_NULL_TO_9);
        newMandatoryColumn("Poids_Total_Calcule", OperationRow.PROPERTY_CATCH_TOTAL_WEIGHT_COMPUTED, TuttiCsvUtil.BOOLEAN);

        newIgnoredColumn("Poids_Total_Vrac");
        newIgnoredColumn("Poids_Total_Vrac_Calcule");

        newIgnoredColumn("Poids_Total_HorsVrac");
        newIgnoredColumn("Poids_Total_HorsVrac_Calcule");

        newMandatoryColumn("Poids_Total_Non_Trie", OperationRow.PROPERTY_CATCH_TOTAL_REJECTED_WEIGHT, TuttiCsvUtil.WEIGHT_NULL_TO_9);
        newMandatoryColumn("Poids_Total_Non_Trie_Calcule", OperationRow.PROPERTY_CATCH_TOTAL_REJECTED_WEIGHT_COMPUTED, TuttiCsvUtil.BOOLEAN);

        newMandatoryColumn("Poids_Total_Tremis", OperationRow.PROPERTY_CATCH_TOTAL_SORTED_TREMIS_WEIGHT, TuttiCsvUtil.WEIGHT_NULL_TO_9);
        newIgnoredColumn("Poids_Total_Tremis_Calcule");

        newMandatoryColumn("Poids_Total_Carroussel", OperationRow.PROPERTY_CATCH_TOTAL_SORTED_CAROUSSEL_WEIGHT, TuttiCsvUtil.WEIGHT_NULL_TO_9);
        newIgnoredColumn("Poids_Total_Carroussel_Calcule");

        newIgnoredColumn("Poids_Total_Espece");
        newIgnoredColumn("Poids_Total_Espece_Calcule");

        newMandatoryColumn("Poids_Total_Espece_Vrac", OperationRow.PROPERTY_SPECIES_TOTAL_SORTED_WEIGHT, TuttiCsvUtil.WEIGHT_NULL_TO_9);
        newMandatoryColumn("Poids_Total_Espece_Vrac_Calcule", OperationRow.PROPERTY_SPECIES_TOTAL_SORTED_WEIGHT_COMPUTED, TuttiCsvUtil.BOOLEAN);

        newIgnoredColumn("Poids_Total_Espece_Vrac_Trie");
        newIgnoredColumn("Poids_Total_Espece_Vrac_Trie_Calcule");

        newIgnoredColumn("Poids_Total_Espece_HorsVrac");
        newIgnoredColumn("Poids_Total_Espece_HorsVrac_Calcule");

        newMandatoryColumn("Poids_Total_Espece_Inerte_Trie", OperationRow.PROPERTY_SPECIES_TOTAL_INERT_WEIGHT, TuttiCsvUtil.WEIGHT_NULL_TO_9);
        newMandatoryColumn("Poids_Total_Espece_Inerte_Trie_Calcule", OperationRow.PROPERTY_SPECIES_TOTAL_INERT_WEIGHT_COMPUTED, TuttiCsvUtil.BOOLEAN);

        newMandatoryColumn("Poids_Total_Espece_Vivant_non_detaille_trie", OperationRow.PROPERTY_SPECIES_TOTAL_LIVING_NOT_ITEMIZED_WEIGHT, TuttiCsvUtil.WEIGHT_NULL_TO_9);
        newMandatoryColumn("Poids_Total_Espece_Vivant_non_detaille_trie_Calcule", OperationRow.PROPERTY_SPECIES_TOTAL_LIVING_NOT_ITEMIZED_WEIGHT_COMPUTED, TuttiCsvUtil.BOOLEAN);

        newIgnoredColumn("Poids_Total_Benthos");
        newIgnoredColumn("Poids_Total_Benthos_Calcule");

        newMandatoryColumn("Poids_Total_Benthos_Vrac", OperationRow.PROPERTY_BENTHOS_TOTAL_SORTED_WEIGHT, TuttiCsvUtil.WEIGHT_NULL_TO_9);
        newMandatoryColumn("Poids_Total_Benthos_Vrac_Calcule", OperationRow.PROPERTY_BENTHOS_TOTAL_SORTED_WEIGHT_COMPUTED, TuttiCsvUtil.BOOLEAN);

        newIgnoredColumn("Poids_Total_Benthos_Vrac_Trie");
        newIgnoredColumn("Poids_Total_Benthos_Vrac_Trie_Calcule");

        newIgnoredColumn("Poids_Total_Benthos_HorsVrac");
        newIgnoredColumn("Poids_Total_Benthos_HorsVrac_Calcule");

        newMandatoryColumn("Poids_Total_Benthos_Inerte_Trie", OperationRow.PROPERTY_BENTHOS_TOTAL_INERT_WEIGHT, TuttiCsvUtil.WEIGHT_NULL_TO_9);
        newMandatoryColumn("Poids_Total_Benthos_Inerte_Trie_Calcule", OperationRow.PROPERTY_BENTHOS_TOTAL_INERT_WEIGHT_COMPUTED, TuttiCsvUtil.BOOLEAN);

        newMandatoryColumn("Poids_Total_Benthos_Vivant_non_detaille_trie", OperationRow.PROPERTY_BENTHOS_TOTAL_LIVING_NOT_ITEMIZED_WEIGHT, TuttiCsvUtil.WEIGHT_NULL_TO_9);
        newMandatoryColumn("Poids_Total_Benthos_Vivant_non_detaille_trie_Calcule", OperationRow.PROPERTY_BENTHOS_TOTAL_LIVING_NOT_ITEMIZED_WEIGHT_COMPUTED, TuttiCsvUtil.BOOLEAN);

        newMandatoryColumn("Poids_Total_Macro_Dechet", OperationRow.PROPERTY_MARINE_LITTER_TOTAL_WEIGHT, TuttiCsvUtil.WEIGHT_NULL_TO_9);
        newMandatoryColumn("Poids_Total_Macro_Dechet_Calcule", OperationRow.PROPERTY_MARINE_LITTER_TOTAL_WEIGHT_COMPUTED, TuttiCsvUtil.BOOLEAN);

        newMandatoryColumn("Serie_Id", Cruise.PROPERTY_PROGRAM, parserFactory.getProgramParser());
        newMandatoryColumn("Engin_Id", FishingOperation.PROPERTY_GEAR, parserFactory.getGearParser());
        newMandatoryColumn("Fishing_Operation_Id", OperationRow.PROPERTY_FISHING_OPERATION_OBJECT_ID, TuttiCsvUtil.INTEGER);
        newMandatoryColumn("Catch_Lot_Id", OperationRow.PROPERTY_CATCH_OBJECT_ID, TuttiCsvUtil.INTEGER);
        newMandatoryColumn("Navire_Id", FishingOperation.PROPERTY_VESSEL, parserFactory.getVesselParser());
        newMandatoryColumn("Strate_Id", FishingOperation.PROPERTY_STRATA, parserFactory.getFishingOperationStrataParser());
        newMandatoryColumn("Sous_Strate_Id", FishingOperation.PROPERTY_SUB_STRATA, parserFactory.getFishingOperationSubStrataParser());
        newMandatoryColumn("Localite_Id", FishingOperation.PROPERTY_LOCATION, parserFactory.getFishingOperationLocationParser());
        newMandatoryColumn("Saisisseur_Id", FishingOperation.PROPERTY_RECORDER_PERSON, parserFactory.getPersonListParser());
        newMandatoryColumn("Navire_Associe_Id", FishingOperation.PROPERTY_SECONDARY_VESSEL, parserFactory.getVesselListParser());

    }

}

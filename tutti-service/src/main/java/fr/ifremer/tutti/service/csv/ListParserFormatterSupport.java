package fr.ifremer.tutti.service.csv;

/*
 * #%L
 * Tutti :: Service
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Joiner;
import org.apache.commons.lang3.StringUtils;
import org.nuiton.csv.ValueParserFormatter;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created on 2/14/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.14
 */
public abstract class ListParserFormatterSupport<E> implements ValueParserFormatter<List<E>> {

    private final ValueParserFormatter<E> delegateParserFormatter;

    protected ListParserFormatterSupport(ValueParserFormatter<E> delegateParserFormatter) {
        this.delegateParserFormatter = delegateParserFormatter;
    }

    @Override
    public List<E> parse(String value) throws ParseException {

        List<E> list = new ArrayList<>();
        if (StringUtils.isNoneBlank(value)) {

            String[] ids = value.split("\\|");
            for (String id : ids) {

                E entity = delegateParserFormatter.parse(id.trim());
                list.add(entity);

            }

        }
        return list;

    }

    @Override
    public String format(List<E> entities) {

        List<String> ids = new ArrayList<>();
        for (E entity : entities) {
            String id = delegateParserFormatter.format(entity);
            ids.add(id);
        }
        return Joiner.on('|').join(ids);

    }
}
package fr.ifremer.tutti.service.genericformat.importactions;

/*
 * #%L
 * Tutti :: Service
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import fr.ifremer.adagio.core.dao.referential.ObjectTypeCode;
import fr.ifremer.tutti.persistence.entities.TuttiEntities;
import fr.ifremer.tutti.persistence.entities.data.CatchBatch;
import fr.ifremer.tutti.persistence.entities.data.FishingOperation;
import fr.ifremer.tutti.persistence.model.OperationDataModel;
import fr.ifremer.tutti.service.genericformat.GenericFormatImportContext;
import fr.ifremer.tutti.service.genericformat.GenericFormatImportCruiseContext;
import fr.ifremer.tutti.service.genericformat.GenericformatImportPersistenceHelper;
import fr.ifremer.tutti.service.genericformat.consumer.CsvConsumerForOperation;
import fr.ifremer.tutti.service.genericformat.csv.AttachmentRow;
import fr.ifremer.tutti.service.genericformat.csv.OperationRow;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.csv.ImportRow;
import org.nuiton.csv.ImportRuntimeException;
import org.nuiton.jaxx.application.ApplicationTechnicalException;

import java.io.IOException;
import java.util.Collection;

import static org.nuiton.i18n.I18n.t;

/**
 * Created on 3/3/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.14
 */
public class ImportOperationAction extends ImportActionSupport {

    /** Logger. */
    private static final Log log = LogFactory.getLog(ImportOperationAction.class);

    private final GenericformatImportPersistenceHelper persistenceHelper;

    public ImportOperationAction(GenericFormatImportContext importContext, GenericformatImportPersistenceHelper persistenceHelper) {
        super(importContext);
        this.persistenceHelper = persistenceHelper;
    }

    @Override
    protected boolean canExecute() {
        return importContext.isTechnicalFilesValid() && importContext.getSurveyFileResult().isValid();
    }

    @Override
    protected void skipExecute() {
        importContext.increments(t("tutti.service.genericFormat.skip.import.operations"));
    }

    @Override
    protected void doExecute() {

        if (log.isInfoEnabled()) {
            log.info("Import operation.csv file.");
        }
        importContext.increments(t("tutti.service.genericFormat.import.operations"));

        boolean importSpecies = importContext.getImportRequest().isImportSpecies();
        boolean importBenthos = importContext.getImportRequest().isImportBenthos();
        boolean importMarineLitter = importContext.getImportRequest().isImportMarineLitter();

        try (CsvConsumerForOperation consumer = importContext.loadOperations(true)) {
            for (ImportRow<OperationRow> row : consumer) {

                GenericFormatImportCruiseContext cruiseContext = consumer.validateRow(row, importContext);

                if (cruiseContext != null) {

                    OperationRow bean = row.getBean();
                    FishingOperation fishingOperation = bean.getFishingOperation();
                    CatchBatch catchBatch = bean.getCatchBatch();

                    Collection<AttachmentRow> operationAttachmentRows = importContext.popAttachmentRows(ObjectTypeCode.OPERATION, bean.getFishingOperationObjectId());
                    Collection<AttachmentRow> catchAttachmentRows = importContext.popAttachmentRows(ObjectTypeCode.CATCH_BATCH, bean.getCatchObjectId());

                    OperationDataModel selectionFishingOperation = cruiseContext.getSelectedFishingOperation(fishingOperation);

                    if (selectionFishingOperation == null) {

                        // skip this operation (not selected)
                        skipOperation(cruiseContext, fishingOperation);

                    } else {

                        // process this operation

                        consumer.prepareRowForPersist(row, importSpecies, importBenthos, importMarineLitter);

                        processOperation(cruiseContext, fishingOperation, catchBatch, operationAttachmentRows, catchAttachmentRows);

                    }

                }

            }
        } catch (IOException e) {
            throw new ApplicationTechnicalException("Could not close operation.csv file", e);
        } catch (ImportRuntimeException e) {

            importContext.getOperationFileResult().addGlobalError(e.getMessage());

        }

    }

    private void skipOperation(GenericFormatImportCruiseContext cruiseContext, FishingOperation fishingOperation) {

        String cruiseStr = cruiseContext.getCruiseLabel();
        String operationStr = importContext.decorate(fishingOperation);
        importContext.increments(t("tutti.service.genericFormat.persist.skipNotSelected.operation", cruiseStr, operationStr));
        cruiseContext.addSkippedFishingOperation(fishingOperation);

    }

    private void processOperation(GenericFormatImportCruiseContext cruiseContext, FishingOperation fishingOperation, CatchBatch catchBatch, Collection<AttachmentRow> operationAttachmentRows, Collection<AttachmentRow> catchAttachmentRows) {

        String cruiseStr = cruiseContext.getCruiseLabel();
        String operationStr = importContext.decorate(fishingOperation);
        OperationDataModel existingOperationData = cruiseContext.getExistingFishingOperationData(fishingOperation);

        Pair<FishingOperation, CatchBatch> savedFishingOperation;

        if (existingOperationData == null) {

            // New operation
            importContext.increments(t("tutti.service.genericFormat.persist.create.operation", cruiseStr, operationStr));
            savedFishingOperation = addFishingOperation(fishingOperation, catchBatch, operationAttachmentRows, catchAttachmentRows);

        } else {

            // Existing operation
            fishingOperation.setId(existingOperationData.getId());
            if (importContext.getImportRequest().isUpdateOperations()) {

                // Update operation
                importContext.increments(t("tutti.service.genericFormat.persist.update.operation", cruiseStr, operationStr));
                savedFishingOperation = updateFishingOperation(fishingOperation, catchBatch, operationAttachmentRows, catchAttachmentRows);

            } else {

                // Skip update operation
                importContext.increments(t("tutti.service.genericFormat.persist.skip.operation", cruiseStr, operationStr));
                savedFishingOperation = Pair.of(fishingOperation, catchBatch);

            }
        }

        importContext.addImportedFishingOperation(savedFishingOperation.getLeft(), savedFishingOperation.getRight());

    }

    private Pair<FishingOperation, CatchBatch> addFishingOperation(FishingOperation fishingOperation, CatchBatch catchBatch, Collection<AttachmentRow> operationAttachmentRows, Collection<AttachmentRow> catchAttachmentRows) {

        String cruiseStr = importContext.decorate(fishingOperation);

        boolean createCruise = TuttiEntities.isNew(fishingOperation);
        Preconditions.checkState(createCruise, "In addFishingOperation method, can't update existing operation: " + cruiseStr);

        if (log.isInfoEnabled()) {
            log.info("Create operation: " + cruiseStr);
        }

        FishingOperation createdFishingOperation = persistenceHelper.createFishingOperation(fishingOperation);

        catchBatch.setFishingOperation(createdFishingOperation);
        CatchBatch createdCatchBatch = persistenceHelper.createCatchBatch(catchBatch);

        boolean importAttachments = importContext.getImportRequest().isImportAttachments();

        if (importAttachments) {

            persistenceHelper.persistAttachments(createdFishingOperation.getIdAsInt(), operationAttachmentRows);
            persistenceHelper.persistAttachments(createdCatchBatch.getIdAsInt(), catchAttachmentRows);

        }

        return Pair.of(createdFishingOperation, createdCatchBatch);

    }

    private Pair<FishingOperation, CatchBatch> updateFishingOperation(FishingOperation fishingOperation, CatchBatch catchBatch, Collection<AttachmentRow> operationAttachmentRows, Collection<AttachmentRow> catchAttachmentRows) {

        String operationStr = importContext.decorate(fishingOperation);

        boolean createFishingOperation = TuttiEntities.isNew(fishingOperation);
        Preconditions.checkState(!createFishingOperation, "In updateFishingOperation method, can't create new operation: " + operationStr);
        Preconditions.checkState(importContext.getImportRequest().isUpdateOperations(), "In updateFishingOperation method, must be allowed to update operation: " + operationStr);

        if (log.isInfoEnabled()) {
            log.info("Persist fishing Operation: " + operationStr);
        }

        FishingOperation updatedFishingOperation = persistenceHelper.saveFishingOperation(fishingOperation);
        catchBatch.setFishingOperation(updatedFishingOperation);

        CatchBatch updatedCatchBatch;

        if (persistenceHelper.isWithCatchBatch(updatedFishingOperation.getIdAsInt())) {

            CatchBatch existingCatchBatch = persistenceHelper.getExistingCatchBatch(updatedFishingOperation.getIdAsInt());
            catchBatch.setId(existingCatchBatch.getId());

            if (!importContext.getImportRequest().isImportSpecies()) {

                // use back existing species weights
                catchBatch.setSpeciesTotalSortedWeight(existingCatchBatch.getSpeciesTotalSortedWeight());
                catchBatch.setSpeciesTotalInertWeight(existingCatchBatch.getSpeciesTotalInertWeight());
                catchBatch.setSpeciesTotalLivingNotItemizedWeight(existingCatchBatch.getSpeciesTotalLivingNotItemizedWeight());

            }

            if (!importContext.getImportRequest().isImportBenthos()) {

                // use back existing benthos weights
                catchBatch.setBenthosTotalSortedWeight(existingCatchBatch.getBenthosTotalSortedWeight());
                catchBatch.setBenthosTotalInertWeight(existingCatchBatch.getBenthosTotalInertWeight());
                catchBatch.setBenthosTotalLivingNotItemizedWeight(existingCatchBatch.getBenthosTotalLivingNotItemizedWeight());

            }

            if (!importContext.getImportRequest().isImportMarineLitter()) {

                // use back existing marine litters weights
                catchBatch.setMarineLitterTotalWeight(existingCatchBatch.getMarineLitterTotalWeight());

            }

            if (log.isInfoEnabled()) {
                log.info("Update catch batch (" + operationStr + "): " + existingCatchBatch.getId());
            }
            updatedCatchBatch = persistenceHelper.saveCatchBatch(catchBatch);

        } else {

            if (log.isInfoEnabled()) {
                log.info("Create new catchBatch (" + operationStr + ")");
            }
            updatedCatchBatch = persistenceHelper.createCatchBatch(catchBatch);

        }

        boolean importAttachments = importContext.getImportRequest().isImportAttachments();

        if (importAttachments) {

            // delete previous attachments
            persistenceHelper.deleteAllAttachments(ObjectTypeCode.OPERATION, fishingOperation.getIdAsInt());
            persistenceHelper.deleteAllAttachments(ObjectTypeCode.CATCH_BATCH, catchBatch.getIdAsInt());

            persistenceHelper.persistAttachments(updatedFishingOperation.getIdAsInt(), operationAttachmentRows);
            persistenceHelper.persistAttachments(updatedCatchBatch.getIdAsInt(), catchAttachmentRows);

        }

        return Pair.of(updatedFishingOperation, updatedCatchBatch);

    }


//    private Pair<FishingOperation, CatchBatch> loadFishingOperation(FishingOperation fishingOperation) {
//
//        String operationStr = importContext.decorate(fishingOperation);
//
//        boolean createFishingOperation = TuttiEntities.isNew(fishingOperation);
//        Preconditions.checkState(!createFishingOperation, "In loadFishingOperation method, can't create new operation: " + operationStr);
//        Preconditions.checkState(!importContext.getImportRequest().isUpdateOperations(), "In loadFishingOperation method, must not be allowed to update operation: " + operationStr);
//
//        if (log.isInfoEnabled()) {
//            log.info("Loading fishing Operation: " + operationStr);
//        }
//
//        FishingOperation loadedFishingOperation = persistenceHelper.saveFishingOperation(fishingOperation);
//
//        CatchBatch loadedCatchBatch = persistenceHelper.getExistingCatchBatch(loadedFishingOperation.getIdAsInt());
//
//        if (loadedCatchBatch == null) {
//
//            loadedCatchBatch = CatchBatchs.newCatchBatch();
//
//        }
//
//        return Pair.of(loadedFishingOperation, loadedCatchBatch);
//
//    }

}

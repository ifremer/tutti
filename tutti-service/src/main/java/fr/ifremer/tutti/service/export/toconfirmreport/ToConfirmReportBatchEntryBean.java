package fr.ifremer.tutti.service.export.toconfirmreport;

/*
 * #%L
 * Tutti :: Service
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.entities.referential.Species;
import fr.ifremer.tutti.persistence.entities.referential.Speciess;

/**
 * To store a species or batch entry within his speices informations,
 * his sorted weight, total weight and percentage amoong the total catch
 * weight.
 *
 * @author Kevin Morin (Code Lutin)
 * @since 3.13
 */
public class ToConfirmReportBatchEntryBean {

    private final Species species;

    protected final String category;

    protected final String categoryWeight;

    protected final String weight;

    protected final String comment;

    public ToConfirmReportBatchEntryBean(Species species,
                                         String category,
                                         String categoryWeight,
                                         String weight,
                                         String comment) {
        this.species = species;
        this.category = category;
        this.categoryWeight = categoryWeight;
        this.weight = weight;
        this.comment = comment;
    }

    public String getCode() {
        return Speciess.getSurveyCodeOrRefTaxCode(species);
    }

    public String getScientificName() {
        return species.getName();
    }

    public String getVernacularCode() {
        return species.getVernacularCode();
    }

    public String getCategory() {
        return category;
    }

    public String getCategoryWeight() {
        return categoryWeight;
    }

    public String getWeight() {
        return weight;
    }

    public String getComment() {
        return comment;
    }

}

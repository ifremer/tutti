package fr.ifremer.tutti.service.genericformat;

/*
 * #%L
 * Tutti :: Service
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Function;
import com.google.common.base.Joiner;
import fr.ifremer.tutti.persistence.entities.referential.TuttiReferentialEntity;
import fr.ifremer.tutti.service.referential.ReferentialImportRequest;
import fr.ifremer.tutti.service.referential.ReferentialImportResult;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

/**
 * Created on 2/15/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.14
 */
public class GenericFormatReferentialImportResult<E extends TuttiReferentialEntity, K extends Comparable<K>> extends GenericFormatCsvFileResult {

    private static final long serialVersionUID = 1L;

    protected Map<String, E> entitiesAdded;

    protected Map<String, E> entitiesLinked;

    private Map<String, String> idTranslationMap;

    public GenericFormatReferentialImportResult(String filename, boolean found) {
        super(filename, false, found);
        this.entitiesAdded = Collections.emptyMap();
        this.entitiesLinked = Collections.emptyMap();
        this.idTranslationMap = Collections.emptyMap();
    }

    public void flushResult(ReferentialImportRequest<E, K> importRequest, ReferentialImportResult<E> importResult) {

        Map<String, String> translationMap = new TreeMap<>();

        Function<E, K> entityToIdFunction = importRequest.getEntityToIdFunction();

        int nbRefAdded = importResult.getNbRefAdded();
        List<E> toAdds = importRequest.getEntitiesToAdd();
        List<E> addeds = importResult.getRefAdded();
        this.entitiesAdded = merge(entityToIdFunction, nbRefAdded, toAdds, addeds, translationMap);

        int nbRefLinked = importResult.getNbRefLinked();
        List<E> toLinks = importRequest.getEntitiesToLink();
        List<E> linkeds = importResult.getRefLinked();
        this.entitiesLinked = merge(entityToIdFunction, nbRefLinked, toLinks, linkeds, translationMap);

        this.idTranslationMap = Collections.unmodifiableMap(translationMap);

    }

    public Set<Map.Entry<String, E>> getEntitiesAddedEntries() {
        return entitiesAdded.entrySet();
    }

    public Collection<E> getEntitiesAdded() {
        return entitiesAdded.values();
    }

    public Set<Map.Entry<String, E>> getEntitiesLinkedEntries() {
        return entitiesLinked.entrySet();
    }

    public Map<String, String> getIdTranslationMap() {
        return idTranslationMap;
    }

    protected Map<String, E> merge(Function<E, K> entityToIdFunction,
                                   int nbEntities,
                                   List<E> request,
                                   List<E> result,
                                   Map<String, String> translationMap) {

        Map<String, E> mergeMap = new TreeMap<>();

        for (int index = 0; index < nbEntities; index++) {

            E toAdd = request.get(index);
            K toAddId = entityToIdFunction.apply(toAdd);
            String originalId = String.valueOf(toAddId);

            E added = result.get(index);
            K addedId = entityToIdFunction.apply(added);
            String targetId = String.valueOf(addedId);

            mergeMap.put(originalId, added);
            translationMap.put(originalId, targetId);

        }

        return Collections.unmodifiableMap(mergeMap);

    }

    public String getReport() {

        StringBuilder builder = new StringBuilder();

        Set<Map.Entry<Long, Set<String>>> errors = getErrorsEntries();

        if (!errors.isEmpty()) {

            builder.append(String.format("%s lines in error:", errors.size()));
            for (Map.Entry<Long, Set<String>> entry : errors) {
                Long lineNumber = entry.getKey();
                Set<String> error = entry.getValue();
                builder.append(String.format("\nline %s : \n%s", lineNumber, Joiner.on("\n\t").join(error)));
            }

        }

        List<String> added = getMapReport(entitiesAdded);
        if (!added.isEmpty()) {
            builder.append(String.format("\n%s entities added: \n%s", added.size(), Joiner.on("\n").join(added)));
        }

        List<String> linked = getMapReport(entitiesLinked);
        if (!linked.isEmpty()) {
            builder.append(String.format("\n%s entities linked: \n%s", linked.size(), Joiner.on("\n").join(linked)));
        }
        return builder.toString();

    }

    protected List<String> getMapReport(Map<String, E> map) {

        List<String> list = new ArrayList<>();
        for (Map.Entry<String, String> entry : idTranslationMap.entrySet()) {
            String originalId = entry.getKey();
            String targetId = entry.getValue();
            if (map.containsKey(originalId)) {
                list.add(String.format("original id: %s -> persist id: %s", originalId, targetId));
            }

        }
        return list;

    }
}

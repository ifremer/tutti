package fr.ifremer.tutti.service.genericformat.csv;

/*
 * #%L
 * Tutti :: Service
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.entities.data.Cruise;
import fr.ifremer.tutti.service.csv.AbstractTuttiImportExportModel;
import fr.ifremer.tutti.service.csv.TuttiCsvUtil;
import fr.ifremer.tutti.service.genericformat.GenericFormatImportEntityParserFactory;

/**
 * To export gear caracteristics.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.3.2
 */
public class GearCaracteristicModel extends AbstractTuttiImportExportModel<GearCaracteristicRow> {

    public static GearCaracteristicModel forExport(char separator) {

        GearCaracteristicModel exportModel = new GearCaracteristicModel(separator);
        exportModel.forExport();
        return exportModel;

    }

    public static GearCaracteristicModel forImport(char separator, GenericFormatImportEntityParserFactory parserFactory) {

        GearCaracteristicModel importModel = new GearCaracteristicModel(separator);
        importModel.forImport(parserFactory);
        return importModel;

    }

    @Override
    public GearCaracteristicRow newEmptyInstance() {

        return GearCaracteristicRow.newEmptyInstance();

    }

    protected GearCaracteristicModel(char separator) {
        super(separator);
    }

    protected void forExport() {

        newColumnForExport("Annee", Cruise.PROPERTY_BEGIN_DATE, TuttiCsvUtil.YEAR);
        newColumnForExport("Serie", Cruise.PROPERTY_PROGRAM, TuttiCsvUtil.PROGRAM_FORMATTER);
        newColumnForExport("Serie_Partielle", Cruise.PROPERTY_SURVEY_PART);

        newColumnForExport("Engin", GearCaracteristicRow.PROPERTY_GEAR, TuttiCsvUtil.GEAR_FORMATTER);
        newColumnForExport("Rang_Engin", GearCaracteristicRow.PROPERTY_RANK_ORDER, TuttiCsvUtil.SHORT);

        newColumnForExport("Code_PMFM", GearCaracteristicRow.PROPERTY_CARACTERISTIC, TuttiCsvUtil.CARACTERISTIC_TECHNICAL_FORMATTER);
        newColumnForExport("Libelle_PMFM", GearCaracteristicRow.PROPERTY_CARACTERISTIC, TuttiCsvUtil.CARACTERISTIC_FORMATTER);
        newColumnForExport("Valeur", GearCaracteristicRow.PROPERTY_VALUE, TuttiCsvUtil.CARACTERISTIC_VALUE_FORMATTER);

        newColumnForExport("Serie_Id", Cruise.PROPERTY_PROGRAM, TuttiCsvUtil.PROGRAM_TECHNICAL_FORMATTER);
        newColumnForExport("Engin_Id", GearCaracteristicRow.PROPERTY_GEAR, TuttiCsvUtil.GEAR_TECHNICAL_FORMATTER);
        newColumnForExport("Valeur_Id", GearCaracteristicRow.PROPERTY_VALUE, TuttiCsvUtil.CARACTERISTIC_VALUE_TECHNICAL_FORMATTER);

    }

    protected void forImport(GenericFormatImportEntityParserFactory parserFactory) {

        newMandatoryColumn("Annee", Cruise.PROPERTY_BEGIN_DATE, TuttiCsvUtil.YEAR);
        newIgnoredColumn("Serie");
        newMandatoryColumn("Serie_Partielle", Cruise.PROPERTY_SURVEY_PART);

        newIgnoredColumn("Engin");
        newMandatoryColumn("Rang_Engin", GearCaracteristicRow.PROPERTY_RANK_ORDER, TuttiCsvUtil.SHORT);

        newMandatoryColumn("Code_PMFM", GearCaracteristicRow.PROPERTY_CARACTERISTIC, parserFactory.getCaracteristicParser());
        newIgnoredColumn("Libelle_PMFM");
        newIgnoredColumn("Valeur");

        newMandatoryColumn("Serie_Id", Cruise.PROPERTY_PROGRAM, parserFactory.getProgramParser());
        newMandatoryColumn("Engin_Id", GearCaracteristicRow.PROPERTY_GEAR, parserFactory.getGearParser());
        newMandatoryColumn("Valeur_Id", GearCaracteristicRow.PROPERTY_VALUE);

    }

}

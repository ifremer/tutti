package fr.ifremer.tutti.service.genericformat.importactions;

/*
 * #%L
 * Tutti :: Service
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.entities.CaracteristicMap;
import fr.ifremer.tutti.persistence.entities.data.FishingOperation;
import fr.ifremer.tutti.service.genericformat.GenericFormatCsvFileResult;
import fr.ifremer.tutti.service.genericformat.GenericFormatImportContext;
import fr.ifremer.tutti.service.genericformat.GenericFormatImportOperationContext;
import fr.ifremer.tutti.service.genericformat.GenericformatImportPersistenceHelper;
import fr.ifremer.tutti.service.genericformat.consumer.CsvConsumerForParameter;
import fr.ifremer.tutti.service.genericformat.csv.ParameterRow;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.csv.ImportRow;
import org.nuiton.csv.ImportRuntimeException;
import org.nuiton.jaxx.application.ApplicationTechnicalException;

import java.io.IOException;

import static org.nuiton.i18n.I18n.t;

/**
 * Created on 3/3/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.14
 */
public class ImportParameterAction extends ImportActionSupport {

    /** Logger. */
    private static final Log log = LogFactory.getLog(ImportParameterAction.class);

    private final GenericformatImportPersistenceHelper persistenceHelper;

    public ImportParameterAction(GenericFormatImportContext importContext, GenericformatImportPersistenceHelper persistenceHelper) {
        super(importContext);
        this.persistenceHelper = persistenceHelper;
    }

    @Override
    protected boolean canExecute() {
        return importContext.isTechnicalFilesValid()
               && importContext.getSurveyFileResult().isValid()
               && importContext.getOperationFileResult().isValid();
    }

    @Override
    protected void skipExecute() {
        importContext.increments(t("tutti.service.genericFormat.skip.import.parameters"));
    }

    @Override
    protected void doExecute() {

        if (log.isInfoEnabled()) {
            log.info("Import parameter.csv file.");
        }
        importContext.increments(t("tutti.service.genericFormat.import.parameters"));
        GenericFormatCsvFileResult importFileResult = importContext.getParameterFileResult();
        try (CsvConsumerForParameter consumer = importContext.loadParameters(true)) {
            for (ImportRow<ParameterRow> row : consumer) {

                GenericFormatImportOperationContext operationContext = consumer.validateRow(row, importContext);
                if (operationContext != null) {
                    consumer.prepareRowForPersist(operationContext, row);
                }

            }
        } catch (IOException e) {
            throw new ApplicationTechnicalException("Could not close parameter.csv file", e);
        } catch (ImportRuntimeException e) {

            importFileResult.addGlobalError(e.getMessage());

        }

        persistFishingOperationParameters();

    }

    public void persistFishingOperationParameters() {

        importContext.doActionOnCruiseContexts((cruiseContext, progressionModel) -> {

            boolean updateOperations = importContext.getImportRequest().isUpdateOperations();

            for (GenericFormatImportOperationContext fishingOperationContext : cruiseContext) {

                String cruiseStr = cruiseContext.getCruiseLabel();
                String operationStr = fishingOperationContext.getFishingOperationLabel();

                importContext.increments(t("tutti.service.genericFormat.persist.operation.parameters", cruiseStr, operationStr));

                if (updateOperations) {

                    boolean persist = false;

                    FishingOperation fishingOperation = fishingOperationContext.getFishingOperation();

                    if (fishingOperationContext.withGearFeatures()) {

                        CaracteristicMap gearUseFeatures = fishingOperationContext.getGearUseFeatures();
                        fishingOperation.setGearUseFeatures(gearUseFeatures);
                        if (log.isInfoEnabled()) {
                            log.info("Persist " + gearUseFeatures.size() + " gear use features of " + operationStr + " for cruise: " + cruiseStr);
                        }

                        persist = true;

                    }

                    if (fishingOperationContext.withVesselFeatures()) {

                        CaracteristicMap vesselUseFeatures = fishingOperationContext.getVesselUseFeatures();
                        fishingOperation.setVesselUseFeatures(vesselUseFeatures);
                        if (log.isInfoEnabled()) {
                            log.info("Persist " + vesselUseFeatures.size() + " vessel use features of " + operationStr + " for cruise: " + cruiseStr);
                        }

                        persist = true;

                    }

                    if (persist) {

                        persistenceHelper.saveFishingOperation(fishingOperation);

                    }

                }

            }
        });

    }

}

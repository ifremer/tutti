package fr.ifremer.tutti.service.genericformat.importactions;

/*
 * #%L
 * Tutti :: Service
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ImmutableList;
import fr.ifremer.tutti.persistence.entities.CaracteristicMap;
import fr.ifremer.tutti.persistence.entities.data.CopyIndividualObservationMode;
import fr.ifremer.tutti.persistence.entities.data.IndividualObservationBatch;
import fr.ifremer.tutti.persistence.entities.referential.Caracteristic;
import fr.ifremer.tutti.persistence.entities.referential.CaracteristicQualitativeValue;
import fr.ifremer.tutti.service.genericformat.GenericFormatContextSupport;
import fr.ifremer.tutti.service.genericformat.GenericFormatCsvFileResult;
import fr.ifremer.tutti.service.genericformat.GenericFormatImportOperationContext;
import fr.ifremer.tutti.service.genericformat.GenericformatImportPersistenceHelper;
import fr.ifremer.tutti.service.genericformat.consumer.CsvConsumerForIndividualObservation;
import fr.ifremer.tutti.service.genericformat.csv.IndividualObservationRow;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.csv.ImportRow;
import org.nuiton.csv.ImportRuntimeException;
import org.nuiton.jaxx.application.ApplicationTechnicalException;

import java.io.IOException;
import java.util.Objects;

import static org.nuiton.i18n.I18n.t;

/**
 * Created on 3/3/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.14
 */
public class ValidateIndividualObservationAction extends ImportActionSupport {

    /** Logger. */
    private static final Log log = LogFactory.getLog(ValidateIndividualObservationAction.class);

    private final GenericformatImportPersistenceHelper persitenceHelper;

    public ValidateIndividualObservationAction(GenericFormatContextSupport importContext,
                                               GenericformatImportPersistenceHelper persitenceHelper) {
        super(importContext);
        this.persitenceHelper = persitenceHelper;
    }

    @Override
    protected boolean canExecute() {
        return importContext.isTechnicalFilesValid() && importContext.getOperationFileResult().isValid();
    }

    @Override
    protected void doExecute() {

        if (log.isInfoEnabled()) {
            log.info("Validate individualObservation.csv file.");
        }

        int maximumRowsInErrorPerFile = importContext.getImportRequest().getMaximumRowsInErrorPerFile();

        GenericFormatCsvFileResult importFileResult = importContext.getIndividualObservationFileResult();
        try (CsvConsumerForIndividualObservation consumer = importContext.loadIndividualObservations(false)) {

            for (ImportRow<IndividualObservationRow> row : consumer) {

                importContext.increments(t("tutti.service.genericFormat.validate.individualObservations", row.getLineNumber()));

                GenericFormatImportOperationContext operationContext = consumer.validateRow(row, importContext);

                if (operationContext != null) {
                    consumer.prepareRowForPersist(operationContext, row);
                }

                if (consumer.getNbRowsInErrors() > maximumRowsInErrorPerFile) {
                    if (log.isWarnEnabled()) {
                        log.warn("Too much errors, stop validating this file.");
                    }
                    break;
                }

            }

            flushConsumer(consumer, importFileResult);

        } catch (IOException e) {
            throw new ApplicationTechnicalException("Could not close individualObservation.csv file", e);
        } catch (ImportRuntimeException e) {

            importFileResult.addGlobalError(e.getMessage());

        }

        importContext.doActionOnCruiseContexts((cruiseContext, progressionModel) -> {

            for (GenericFormatImportOperationContext fishingOperationContext : cruiseContext) {

                String cruiseStr = cruiseContext.getCruiseLabel();
                String operationStr = fishingOperationContext.getFishingOperationLabel();

                if (fishingOperationContext.withIndividualObservationBatches()) {

                    ImmutableList<IndividualObservationBatch> individualObservationBatches = fishingOperationContext.getIndividualObservationBatches();

                    if (log.isInfoEnabled()) {
                        log.info("Check " + individualObservationBatches.size() + " individual observation(s) of " + operationStr + " for cruise: " + cruiseStr);
                    }
                    prepareIndividualObservationsForPersist(persitenceHelper, individualObservationBatches);

                }

            }

        });

    }

    public static void prepareIndividualObservationsForPersist(GenericformatImportPersistenceHelper persistenceHelper,
                                                               ImmutableList<IndividualObservationBatch> individualObservations) {

        for (IndividualObservationBatch individualObservation : individualObservations) {

            CaracteristicMap caracteristics = individualObservation.getCaracteristics();

            CaracteristicQualitativeValue copyModeCaracteristic = caracteristics.removeQualitativeValue(persistenceHelper.getCopyIndividualObservationModeCaracteristic());
            Objects.requireNonNull(copyModeCaracteristic, "No copy mode found for individual observation: " + individualObservation.getId());
            CopyIndividualObservationMode copyMode = CopyIndividualObservationMode.valueOf(copyModeCaracteristic.getIdAsInt());
            individualObservation.setCopyIndividualObservationMode(copyMode);

            String lengthClassId = caracteristics.removeStringValue(persistenceHelper.getPmfmIdCaracteristic());
            Objects.requireNonNull(lengthClassId, "No lengthClassId found for individual observation: " + individualObservation.getId());

            Caracteristic lengthStepCaracteristic = persistenceHelper.getCaracteristic(Integer.valueOf(lengthClassId));
            Objects.requireNonNull(lengthStepCaracteristic, "lengthStepCaracteristic " + lengthClassId + " does not exist.");
            individualObservation.setLengthStepCaracteristic(lengthStepCaracteristic);

            Float length = caracteristics.removeFloatValue(lengthStepCaracteristic);
            individualObservation.setSize(length);

            Float weigth = caracteristics.removeFloatValue(persistenceHelper.getWeightMeasuredCaracteristic());
            individualObservation.setWeight(weigth);

            String samplingCode = caracteristics.removeStringValue(persistenceHelper.getSampleCodeCaracteristic());
            individualObservation.setSamplingCode(samplingCode);

        }

    }

}

<#--
 #%L
 Tutti :: Service
 %%
 Copyright (C) 2012 - 2014 Ifremer
 %%
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as
 published by the Free Software Foundation, either version 3 of the 
 License, or (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public 
 License along with this program.  If not, see
 <http://www.gnu.org/licenses/gpl-3.0.html>.
 #L%
-->
<html>
  <head>
    <style type="text/css">

      <#assign blueColor="#000080">
      <#assign lightGrayColor="#f2f2f2">

      @page { size: A4 landscape;}

      h1, h4 {
        color: ${blueColor};
        font-weight: bold;
        font-style: italic;
      }

      h2 {
        margin-bottom: 50pt;
      }

      table {
        margin-bottom: 25pt;
        border-collapse: collapse;
      }

      th {
        color: ${blueColor};
        font-weight: bold;
        background: ${lightGrayColor};
      }

      td, th {
        padding: 1pt 5pt;
        border-left: 1px solid #ccc;
        border-right: 1px solid #ccc;
      }

      tbody tr:nth-child(even) {
        background: ${lightGrayColor};
      }
      tbody tr:nth-child(odd) {
        background: #fff;
      }

      td.number {
        text-align: right;
      }

      .species {
        width: 10%;
      }
      .scientificName {
        width: 18%;
      }
      .vernacular {
        width: 17%;
      }
      .category {
        width: 15%;
      }
      .categoryWeight {
        width: 10%;
      }
      .weight {
        width: 10%;
      }
      .comment {
        width: 20%;
      }

      .operationInfo {
        font-weight: bold;
      }

      .label {
        color: ${blueColor};
      }

      .value {
        margin-right: 50pt;
      }

    </style>
  </head>
  <body>

    <h1>Rapport des espèces à confirmer</h1>
    <h2>${cruise.name}</h2>

    <#assign orderedOperations = operations?sort_by("gearShootingStartDate")?reverse>
    <#list orderedOperations as operation>

      <p class='operationInfo'>
        <span class="label">Station :</span> <span class="value">${operation.stationNumber} - ${operation.fishingOperationNumber}</span>
        <span class="label">Poche :</span> <span class="value">${operation.multirigAggregation}</span>
        <span class="label">
          du ${operation.gearShootingStartDate?date?string.full} ${operation.gearShootingStartDate?time?string.short}
          <#if operation.gearShootingEndDate??>au ${operation.gearShootingEndDate?date?string.full} ${operation.gearShootingEndDate?time?string.short}</#if>
        </span>
      </p>

      <#if operation.speciesCatches??>
        <h3>Espèces</h3>
        <table>
          <thead>
            <tr>
              <th class="species">Espèce</th>
              <th class="scientificName">Nom scientifique</th>
              <th class="vernacular">Nom commun</th>
              <th class="category">Catégorie</th>
              <th class="categoryWeight">Poids (${speciesWeightUnit.shortLabel})</th>
              <th class="weight">Poids sous échantilloné (${speciesWeightUnit.shortLabel})</th>
              <th class="comment">Commentaire</th>
            </tr>
          </thead>
          <tbody>
            <#list operation.speciesCatches as catch>
              <tr>
                <td class="species"><#if catch.code??>${catch.code}</#if></td>
                <td class="scientificName"><em>${catch.scientificName}</em></td>
                <td class="vernacular"><#if catch.vernacularCode??>${catch.vernacularCode}</#if></td>
                <td class="category">${catch.category}</td>
                <td class="number categoryWeight">${catch.categoryWeight}</td>
                <td class="number weight">${catch.weight}</td>
                <td class="comment"><#if catch.comment??>${catch.comment?html}</#if></td>
              </tr>
            </#list>
          </tbody>
        </table>
      </#if>

      <#if operation.benthosCatches??>
        <h3>Benthos</h3>
        <table>
          <thead>
            <tr>
              <th class="species">Espèce</th>
              <th class="scientificName">Nom scientifique</th>
              <th class="vernacular">Nom commun</th>
              <th class="category">Catégorie</th>
              <th class="categoryWeight">Poids (${benthosWeightUnit.shortLabel})</th>
              <th class="weight">Poids sous échantilloné (${benthosWeightUnit.shortLabel})</th>
              <th class="comment">Commentaire</th>
            </tr>
          </thead>
          <tbody>
            <#list operation.benthosCatches as catch>
            <tr>
              <td class="species"><#if catch.code??>${catch.code}</#if></td>
              <td class="scientificName"><em>${catch.scientificName}</em></td>
              <td class="vernacular"><#if catch.vernacularCode??>${catch.vernacularCode}</#if></td>
              <td class="category">${catch.category}</td>
              <td class="number categoryWeight">${catch.categoryWeight}</td>
              <td class="number weight">${catch.weight}</td>
              <td class="comment"><#if catch.comment??>${catch.comment?html}</#if></td>
            </tr>
            </#list>
          </tbody>
        </table>
      </#if>

      <#if operation != orderedOperations?last>
        <h2 style="page-break-after:always"/>
      </#if>

    </#list>

  </body>
</html>

<#--
 #%L
 Tutti :: Service
 %%
 Copyright (C) 2012 - 2014 Ifremer
 %%
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public
 License along with this program.  If not, see
 <http://www.gnu.org/licenses/gpl-3.0.html>.
 #L%
-->

<#macro renderErrors errorsEntries>
<table>
    <tr>
        <th>Ligne</th>
        <th>Erreur</th>
    </tr>
  <#list errorsEntries as entry>
      <tr>
          <td>${entry.key}</td>
          <td>
              <ul>
                <#list entry.value as error>
                    <li class="error">
                    ${error}
                    </li>
                </#list>
              </ul>
          </td>
      </tr>
  </#list>
</table>
</#macro>

<#macro renderSimpleImportFile fileResult>
  <#assign errorsEntries = fileResult.errorsEntries>
<ul>
    <li>Nom du fichier : <span class="bold">${fileResult.filename}</span></li>
  <#if !fileResult.imported>
      <li class="error">Fichier non importé</li>
  </ul>
    <#return>
  </#if>
  <#if !fileResult.valid>
  <li class="error">Fichier non valide</li>
  </#if>
  <#if errorsEntries?size != 0>
  <li class="error">Nombre de lignes en erreurs : ${errorsEntries?size}</li>
  </#if>
</ul>
  <#if errorsEntries?size == 0>
    <#return>
  </#if>
  <@renderErrors errorsEntries=errorsEntries/>
</#macro>

<#macro renderImportFile fileResult showNbLines=true>
  <#assign errorsEntries = fileResult.errorsEntries>
<ul>
    <li>Nom du fichier : <span class="bold">${fileResult.filename}</span></li>
  <#if !fileResult.imported>
      <li class="error">Fichier non importé</li>
  </ul>
    <#return>
  </#if>
<#if showNbLines>
<li>Nombre de lignes à importer : ${fileResult.linesCount}</li>
</#if>
  <#if !fileResult.valid>
  <li class="error">Fichier non valide</li>
  </#if>
  <#if errorsEntries?size != 0>
  <li class="error">Nombre de lignes en erreurs : ${errorsEntries?size}</li>
    <#if !fileResult.fullyLoaded>
    <li class="error">Trop le lignes en erreurs, tout le fichier n'a pas été validé.</li>
    </#if>
  </#if>
</ul>
  <#if errorsEntries?size == 0>
    <#return>
  </#if>
  <@renderErrors errorsEntries=errorsEntries/>
</#macro>

<#macro renderReferentialImportFile fileResult>
  <#assign errorsEntries = fileResult.errorsEntries>
<ul>
    <li>Nom du fichier : <span class="bold">${fileResult.filename}</span></li>
  <#if !fileResult.imported>
      <li>Fichier non importé</li>
  </ul>
    <#return>
  </#if>
<li>Nombre de lignes à importer : ${fileResult.linesCount}</li>
  <#if fileResult.valid>
    <#if fileResult.entitiesAddedEntries?size != 0 >
    <li>Nombre de référentiels ajoutés : ${fileResult.entitiesAddedEntries?size}</li>
    </#if>
    <#if fileResult.entitiesLinkedEntries?size != 0 >
    <li>Nombre de référentiels associés : ${fileResult.entitiesLinkedEntries?size}</li>
    </#if>
  <#else>
  <li class="error">Fichier non valide</li>
  </#if>
  <#if errorsEntries?size != 0>
  <li class="error">Nombre de lignes en erreurs : ${errorsEntries?size}</li>
  </#if>
</ul>
  <#if errorsEntries?size == 0>
    <#return>
  </#if>
  <@renderErrors errorsEntries=errorsEntries/>
</#macro>

<#macro renderReferentialGearList title entries>
  <#if entries?size == 0>
    <#return>
  </#if>
<h4>${title}</h4>
<table>
    <tr>
        <th>Identifiant d'import</th>
        <th>Nom</th>
        <th>Libellé</th>
        <th>Engin scientifique</th>
        <th>Identifiant en base</th>
    </tr>
  <#list entries as entry>
      <tr>
          <td class="number">${entry.key}</td>
          <td>${entry.value.name}</td>
          <td>${entry.value.label}</td>
          <td>${entry.value.scientificGear?string('Oui', 'Non')}</td>
          <td class="number bold">${entry.value.id}</td>
      </tr>
  </#list>
</table>
</#macro>

<#macro renderReferentialPersonList title entries>

  <#if entries?size == 0>
    <#return>
  </#if>
<h4>${title}</h4>
<table>
    <tr>
        <th>Identifiant d'import</th>
        <th>Prénom</th>
        <th>Nom</th>
        <th>Identifiant en base</th>
    </tr>
  <#list entries as entry>
      <tr>
          <td class="number">${entry.key}</td>
          <td>${entry.value.firstName}</td>
          <td>${entry.value.lastName}</td>
          <td class="number bold">${entry.value.id}</td>
      </tr>
  </#list>
</table>
</#macro>

<#macro renderReferentialSpeciesList title entries>
  <#if entries?size == 0>
    <#return>
  </#if>
<h4>${title}</h4>
<table>
    <tr>
        <th>Identifiant d'import</th>
        <th>Nom</th>
        <th>Identifiant en base</th>
    </tr>
  <#list entries as entry>
      <tr>
          <td class="number">${entry.key}</td>
          <td>${entry.value.name}</td>
          <td class="number bold">${entry.value.id}</td>
      </tr>
  </#list>
</table>
</#macro>

<#macro renderReferentialVesselList title entries>
  <#if entries?size == 0>
    <#return>
  </#if>
<h4>${title}</h4>
<table>
    <tr>
        <th>Identifiant d'import</th>
        <th>Nom</th>
        <th>Immatriculation internationale</th>
        <th>Navire scientifique</th>
        <th>Identifiant en base</th>
    </tr>
  <#list entries as entry>
      <tr>
          <td class="number">${entry.key}</td>
          <td>${entry.value.name}</td>
          <td>${entry.value.internationalRegistrationCode}</td>
          <td>${entry.value.scientificVessel?string('Oui', 'Non')}</td>
          <td class="number bold">${entry.value.id}</td>
      </tr>
  </#list>
</table>
</#macro>

<#macro renderImportFileResult fileResult anchorName>
<tr>
    <td>${fileResult.filename}</td>
    <td>${fileResult.mandatory?string('Oui', 'Non')}</td>
  <#if fileResult.found>
      <td class="valid"></td>
    <#if fileResult.imported>
        <td class="valid"></td>
      <#if fileResult.valid>
          <td class="valid"></td>
      <#else>
          <td class="notValid"></td>
      </#if>
    <#else>
        <td class="notValid"></td>
        <td class="unkwon"></td>
    </#if>
  <#else>
      <td class="notValid"></td>
      <td class="unkwon"></td>
      <td class="unkwon"></td>
  </#if>
    <td><a href="#${anchorName}">Détails</a></td>
</tr>
</#macro>

<html>
<head>
    <style type="text/css">

      <#assign redColor="#FF0002">
      <#assign blueColor="#000080">
      <#assign lightGrayColor="#f2f2f2">

      @page {
          size: A4 landscape;
      }

      .valid {
          width: 100px;
          background: url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABGdBTUEAAK/INwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAGrSURBVDjLvZPZLkNhFIV75zjvYm7VGFNCqoZUJ+roKUUpjRuqp61Wq0NKDMelGGqOxBSUIBKXWtWGZxAvobr8lWjChRgSF//dv9be+9trCwAI/vIE/26gXmviW5bqnb8yUK028qZjPfoPWEj4Ku5HBspgAz941IXZeze8N1bottSo8BTZviVWrEh546EO03EXpuJOdG63otJbjBKHkEp/Ml6yNYYzpuezWL4s5VMtT8acCMQcb5XL3eJE8VgBlR7BeMGW9Z4yT9y1CeyucuhdTGDxfftaBO7G4L+zg91UocxVmCiy51NpiP3n2treUPujL8xhOjYOzZYsQWANyRYlU4Y9Br6oHd5bDh0bCpSOixJiWx71YY09J5pM/WEbzFcDmHvwwBu2wnikg+lEj4mwBe5bC5h1OUqcwpdC60dxegRmR06TyjCF9G9z+qM2uCJmuMJmaNZaUrCSIi6X+jJIBBYtW5Cge7cd7sgoHDfDaAvKQGAlRZYc6ltJlMxX03UzlaRlBdQrzSCwksLRbOpHUSb7pcsnxCCwngvM2Rm/ugUCi84fycr4l2t8Bb6iqTxSCgNIAAAAAElFTkSuQmCC') no-repeat center center;
      }

      .notValid {
          width: 100px;
          background: url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABGdBTUEAAK/INwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAHdSURBVDjLpZNraxpBFIb3a0ggISmmNISWXmOboKihxpgUNGWNSpvaS6RpKL3Ry//Mh1wgf6PElaCyzq67O09nVjdVlJbSDy8Lw77PmfecMwZg/I/GDw3DCo8HCkZl/RlgGA0e3Yfv7+DbAfLrW+SXOvLTG+SHV/gPbuMZRnsyIDL/OASziMxkkKkUQTJJsLaGn8/iHz6nd+8mQv87Ahg2H9Th/BxZqxEkEgSrq/iVCvLsDK9awtvfxb2zjD2ARID+lVVlbabTgWYTv1rFL5fBUtHbbeTJCb3EQ3ovCnRC6xAgzJtOE+ztheYIEkqbFaS3vY2zuIj77AmtYYDusPy8/zuvunJkDKXM7tYWTiyGWFjAqeQnAD6+7ueNx/FLpRGAru7mcoj5ebqzszil7DggeF/DX1nBN82rzPqrzbRayIsLhJqMPT2N83Sdy2GApwFqRN7jFPL0tF+10cDd3MTZ2AjNUkGCoyO6y9cRxfQowFUbpufr1ct4ZoHg+Dg067zduTmEbq4yi/UkYidDe+kaTcP4ObJIajksPd/eyx3c+N2rvPbMDPbUFPZSLKzcGjKPrbJaDsu+dQO3msfZzeGY2TCvKGYQhdSYeeJjUt21dIcjXQ7U7Kv599f4j/oF55W4g/2e3b8AAAAASUVORK5CYII=') no-repeat center center;
      }

      .unkwon {
          width: 100px;
          background: url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABGdBTUEAAK/INwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAKkSURBVDjLpZPdT5JhGMb9W+BPaK3matVqndXWOOigA6fmJ9DUcrUMlrN0mNMsKTUznQpq6pyKAm8CIogmypcg8GIiX8rHRHjhVbPt6o01nMvZWge/k3vP9duuZ/edAyDnf/hjoCMP2Vr3gUDj3CdV6zT1xZ6iFDaKnLEkBFOmPfaZArWT5sw60iFP+BAbOzTcQSqDZzsNRyCNkcVoaGghzDlVQKylOHJrMrUZ2Yf52y6kc36IxpyoH1lHF7EBgyMKV4jCJ5U/1UVscU4IZOYEa3I1HtwI01hwxlDLhDoJD/wxGr5YGmOLAdRIrVCuhmD3JdA6SQabx12srGB0KSpc86ew4olDOGjH4x4z0gdHDD9+c4TaQQtq+k2Yt0egXYugTmoVZgV9cyHSxXTtJjZR3WNCVfcK/NE0ppYDUNu2QTMCtS0IbrsOrVMOWL27eNJtJLOCDoWXdgeTEEosqPxoBK/TwDzWY9rowy51gJ1dGr2zLpS2aVH5QQ+Hbw88sZ7OClrGXbQrkMTTAQu4HXqUv9eh7J0OSfo7tiIU+GItilpUuM/AF2tg98eR36Q+FryQ2kjbVhximQu8dgPKxPMoeTuH4tfqDIWvCBQ2KlDQKEe9dBlGTwR36+THFZg+QoUxAL0jgsoOQzYYS+wjskcjTzSToVAkA7Hqg4Spc6tm4vgT+eIFVvmb+eCSMwLlih/cNg0KmpRoGzdl+BXOb5jAsMYNjSWAm9VjwesPR1knFilPNMu510CkdPZtqK1BvJQsoaRZjqLGaTzv1UNp9EJl9uNqxefU5QdDnFNX+Y5Qxrn9bDLUR6zjqzsMizeWYdG5gy6ZDbk8aehiuYRz5jHdeDTKvlY1IrhSMUxe4g9SuVwpdaFsgDxf2i84V9zH/us1/is/AdevBaK9Tb3EAAAAAElFTkSuQmCC') no-repeat center center;
      }

      .bold {
          font-weight: bold;
      }

      .error {
          color: ${redColor};
          font-weight: bold;
      }

      .nextPage {
          page-break-after: always;
      }

      h1 {
          background: url("data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAKEAAAAeCAYAAABEzX4WAAAABmJLR0QA/wD/AP+gvaeTAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAB3RJTUUH3gsYESwznZhaYQAAC8VJREFUeNrtnHmQHPV1xz+vu+fanZm9b2l1YFlCSGBkkI3DYQpwqrI7rFwuK44ph8QmcQyuUCQYl3EO4xg7wQaDBSZCMahQIiiBjXZWoiiIywIZZbkMiAAxhyWxOliJPbTH3N0vf/x6tKPdjQMY5BGaV9PVU7/+Xb2/7+/73vd1zwrHmW3ZsoXu7m6SyeRFIhIHxgBUNSsiu4GDIpLv7u52i20eeeQRLrroonc95ubNm1m1ahUVe39MjkMQ1qtqN3CBqp4sIiuAw0BaVV8BBkTkGeBRVd0nIqOJRKKQTCarLMuyuru7JwCSyWRMRCwfxA1ACnBUVUUEVS3Ytp3v6uoqVGBSAeFR1tfXFxCRJap6sapeKiINgAPE/SopVU0BeeBxEfmlqg4AnSJSr6qjIpJV1VNEJK2qh0TkD4DfABlVbQEUmBCRMf/6IHAA2JVIJPaUzIVEIlFB0YkEwuKib9u2jfHx8QWqeqGIXAKsBCI+eErvyVXVgs94I6oaEpEaVcVnuzEgLnKkyV7gLWApECyph4gcAPYALwKbEonEw6XhQcVOLHd8ZNEffPDBQKFQWAN82QdSEYS+Vz369lTV3PQUsIplxcrjwK+Bxaoam16v2J+qHhSRjYlE4iqA3t5eenp6Kmh6l2YdbxPu7u6mr6/P0Jzr1gIjPjg8VS2ibgYAi+ArlouITi9T1aiqrgCipW1KzsU2zcAVyWTyVqACwBMJhL29vQAkEgn6+vrCwCdF5AxVzQFWEVjvwgtIEbm+654VxKVtVDUgIpf29fV9oQKjEwSEmzZtoqenh3Xr1tHb21sPnAJ8XVWXiUj2WIcXPuCjwJeSyWTH9I1SsQ8gCFevXg1AQ0NDWEROVtUunwFTQOj3EU/78eR5IvIPfX19pwEEg8EKqj6owqS3txcRqQLqgC/4qlj949RZlPE7tRntp4mXWWPMknpvqupfXXzxxRUqfIfmlPsEt27dSldXV3HDdIrIUqBRVXeJyGmYRPP/CaoSIJXK5emAK62bAQZV9WcickBVR0RkFJMQL/jtwpjEdrWIxP38oy0iOyuQ+oAy4bXXqzOnRZ22hm1n2nbqEqCgKikR72xgkYhGQcJFPKmKiqioiuu76yrAVqwxQQOgERFVEClmc1TtnSD/XXDDP05nm159eMcpQ3evEbcCkWMR14zx7XKdXBEmk+kWMtnGurHJBcuGRk6dcL1gRESXT6TbBjOZpqingQWqokW9IOKharkibs6205FwcGTAkkJNU/2zFApVAweHP9qUyrQ0+xkXIqGh/Yvmb3yrPv7Sjlhs7348HMOMZvyKva+eeIfoGFrOAEQh71YxOrYE1wvj2BNMptsZOrwM1CKbqyWVadF0tklULRw7jaeORkJvSSR0EM8L6Emd90td/BVULQLOOE+8cB3Do8vVw5JoZL92tj8mHzppg8GkB7gl41fs/V7jdWUbExpHCZ4XRMTFw8LzLDzLwbYz1MdfZHR8CblCDNcLSCQ8SDrTDKKEAqMyv30L1VX7qa99TQLVIwZceRgeWUoq3U7BC0lV+CBLFvy7PP/8Czz6Cyi4sGghnLUSIpEKSI5RLJhzypYBAVUL1wsxmWkl6IwTrtrD6PgiXDeMbecIBg4TcFJ4XgBQolX7aG3sp67mZWqjrxCKHGbPLvjNbtPv2WfB6PgiLCtPa2M/J3X285O7nuaWH0/y5qAZ86yVcN/d0FEF5ekjKur4GDGgoCpYloeXD+AWIoSCoygWtp0mbBWYTLeSzrQQcA4TrRrAsVO0NvRTE3sdy8pRfHiy6QG4/gdQKMDrz0MoNMqH522kuXEn42OTrP1JhjcHIRSCyy+DFR+B6uoKAI8VFWr5pmhMWiVfqEZEqY4cIO9GGJ+cZ9jPniSbqyNbiNLR/BjRyD5i0V0E7ckZen9iAg4fNt89Dzpatxu2DXgMvgaptLl2/jlw083gToBlVfBxLF1y2caEquC6EVzPIRwcxlObgDOG64ZIZxqxrDwnL1hPtGofASc15cr/vySUeKDgZmEyZdoA1NYAWXBdsG1TlsmCWwDbgVAQ0hnwXMOUxZAhn4dczgBcLAgGIRg4OrTwPEinzVxCwam+1QPHgXB4qqxQMFMOBs0xXSCp+mPmTXvLNn06zvRQxmywYl8AmQwEAob1y4RrQMrwsZ0BhQVYOPYkwcAY6Wwjrhsm4Ewg4uJ6YdqatlNX8yoBJ3UESG9XzQ4Nw599BS7ohqEhU7Z5K9TNgRtuAYJAGL56NcxfBl+8HJ56BlaeB/WdU/0cPATf+h60L4Z4B7R9CP72Wth/APCBjAWvvmbqnHwG3LkBvn8LNM43bc7vgqefNccF3RBvh9q58NfXwPiEAbbq1GbZtRuuuBrq55r2Jy2Hf/mhX9f26wlkc7BwOaw4B+65H269A2LtcO11lN2qlyUTep6ZlmXnEDxyEiNfiBIMjFFdtZ+6+K+xrALqGeC901SKZUFDHTQ1+uxWgEgY2tsgHp3aoakUvDUEj/fDSy/Di/8DNTWmj1QKrvoGbNwEy5bC8qXwxNNw61rYvQc23gmxmOmrOgpjY+b455vMfNtaYPcbps0fX2rU+PAINDaYMdfeCa3N8K3vgBiiZ3gEPvfn8NSv4MwVsHABPPSf8Hf/BG/shbW3GaJHIRwxm+TwGKy7C/7rSX/By3DFyzT6URBF1SKbr8Vx0sSjuwgFhwkHR7CswuyudhazrSPEf8Ticbj2avjR96HG/1HAOZ+AhzfD51djHs4xFRu+MQCuB/96M6y/3ZRt224AuHgR3PEj2Lge7l1vrv38Udj6kM+oQKBk4YNBuPF66L0XTj/VZ7c90DkXeu+B+zdMuewdT4A77t9nEG5bawD4sTPgvg1w791wg/+o4b4H4Ml+IFC8cXPKZg3QP9MD99wFn/+snwstKyZUvlt2O0NcFCFfiIp6YgcCGRVxXUHxP8jbwaCN9+LLnAecc9RN29DcAQuGpuK/eAzaPwxMmFirtO9YFK66Ar74JX8BC7DhXnPtwvPhrLPN4p95Lnz8TOh/Cp57AT6XLw0xjPV0wapPG1D1dMGzO6EqApeshpV+P8uWwjPPwUTKsF9Ts/FZt9xuNsaF58O8pWZr/eUV8OUrDTP/YjusPNfkQ0u33WnL4YbvQOdCM3d1yyQRf0QdK/9YdgnMonhwgzj2JKiF61kEnMzs4mM2CwMhCj9L8u3pIDQ+3wiGo2NRn4SnDdDcBKctM200b673P2WuPfk0XH2NESwAgwfN+c1BmJzw0z0l1tTos1QeGvxXL6qrob4OcM0foNEv91wjVLBgcDcMjZhN89jj8PWrjEAJBAwwsznYMzC7b5vfCZ2LgHT5PQkSwJFayvgnjYfekxDzd3YXjq8oSxZweNScn3nOsF5pvGlZBjz5/NvZcTMpfQZILCOmwKj3Hf1TmwA1Lt7zjEqf1SHYhknL7lGk79LK/lWucrWamBEaf/on8DdfNeA4gnqFulqIVr93i1VX68eUAfjKZeYopmCK61lfi3ln6Lc8CChHq4DwXdrHV8LAAyb/t2QxBOI+51r+kZkSOL+zudA2DxrqDSOmM7B4hd+/loyZKqN47/hXx+Vvl11qzlsegutvgJd+BXtfhye3wx1rYOdzJbnC94gNv3al+Zp8EG69EV5/Cfa8Cr/8Odz4PTg4aHKFx5udCEwovz0nOVPBThcrns4sP/cTcM2VJrn93Rvhrv8wqZjJlAHM7TfDqafPEpHqzO/qvY1xc3D5X5gUzU974Zq/h5vWGNabmDTnP/oUNLfNHFO1rFfG/sCDcG4Ho47DUCpFDhgA3OIr1SIEO+fSFIti19aQAg6p7x0EvNo4jXM6iHa0khNhCMgVxXk4DNd9E124AOuHt9G6dx9NniLNTYz84QXs/+jp5HHx34jEbmlmbiiIFw4ziv9PnEJBYu2t1Le2UHBsho1+RevraZnTTqSliSzCIcBVRWJR+Lc16EeW46xbT9uBQRptG6+jjUOrujkwrxMtGZPOOXQC1NYefW9lZEGB1/4X0fnqfLJ+VzcAAAAASUVORK5CYII=") top left no-repeat;
          width: 100%;
          text-align: center;
      }

      h1, h4 {
          color: ${blueColor};
          font-weight: bold;
          font-style: italic;
      }

      h2 {
          margin-bottom: 20pt;
      }

      table {
          margin-bottom: 25pt;
          border-collapse: collapse;
      }

      th {
          color: ${blueColor};
          font-weight: bold;
          background: ${lightGrayColor};
      }

      td, th {
          /*padding: 1pt 5pt;*/
          border-left: 1px solid #ccc;
          border-right: 1px solid #ccc;
      }

      tbody tr:nth-child(even) {
          background: ${lightGrayColor};
      }

      tbody tr:nth-child(odd) {
          background: #fff;
      }

      .operationPadding {
          padding-left: 25pt;
      }

      .small {
          font-size: 11px;
      }

    </style>
</head>
<body>

<h1>Rapport de validation avant import générique</h1>

<h2>Informations générales</h2>

<ul>
    <li>Fichier : ${importConfiguration.importFile.name}</li>
    <li>Date - heure : ${startingDate?date?string.full} ${startingDate?time?string.short}</li>
    <li>Série de campagne: ${program.name}</li>
    <li>Nombre de campagnes détectées: ${nbCruises}</li>
    <li>Nombre de traits détectés: ${nbOperations}</li>
</ul>

<h3>Fichiers détectés <a name="summary_files"></a></h3>
<table align="center">
    <thead>
    <tr>
        <th>Fichier</th>
        <th>Obligatoire</th>
        <th>Présent</th>
        <th>Importé</th>
        <th>Valide</th>
        <th>Détail</th>
    </tr>
    </thead>
    <tbody>
    <@renderImportFileResult fileResult=sampleCategoryFileResult anchorName="sampleCategoryFileResult"/>
    <@renderImportFileResult fileResult=referentialTemporaryGearFileResult anchorName="referentialTemporaryGearFileResult"/>
    <@renderImportFileResult fileResult=referentialTemporaryPersonFileResult anchorName="referentialTemporaryPersonFileResult"/>
    <@renderImportFileResult fileResult=referentialTemporarySpeciesFileResult anchorName="referentialTemporarySpeciesFileResult"/>
    <@renderImportFileResult fileResult=referentialTemporaryVesselFileResult anchorName="referentialTemporaryVesselFileResult"/>
    <@renderImportFileResult fileResult=protocolFileResult anchorName="protocolFileResult"/>
    <@renderImportFileResult fileResult=surveyFileResult anchorName="surveyFileResult"/>
    <@renderImportFileResult fileResult=gearCaracteristicFileResult anchorName="gearCaracteristicFileResult"/>
    <@renderImportFileResult fileResult=operationFileResult anchorName="operationFileResult"/>
    <@renderImportFileResult fileResult=parameterFileResult anchorName="parameterFileResult"/>
    <@renderImportFileResult fileResult=catchFileResult anchorName="catchFileResult"/>
    <@renderImportFileResult fileResult=marineLitterFileResult anchorName="marineLitterFileResult"/>
    <@renderImportFileResult fileResult=accidentalCatchFileResult anchorName="accidentalCatchFileResult"/>
    <@renderImportFileResult fileResult=individualObservationFileResult anchorName="individualObservationFileResult"/>
    <@renderImportFileResult fileResult=attachmentsFileResult anchorName="attachmentsFileResult"/>
    </tbody>
</table>

<h2 class="nextPage"></h2>
<h2>Résumé des données détéctées <a name="summary_data_imported"></a> </h2>

<table>
    <thead>
    <tr>
        <th class="small">Campagne - Trait</th>
    </tr>
    </thead>
    <tbody>
    <#list dataModel.iterator() as cruise>
    <tr>
        <td class="small">${cruise.label} (${cruise.size()} traits)</td>
    </tr>
      <#list cruise.iterator() as operation>
      <tr>
          <td class="small operationPadding">Trait ${operation.label}</td>
      </tr>
      </#list>
    </#list>
    </tbody>
</table>

<h2 class="nextPage"></h2>
<h2>Modèle de catégorisation <a name="sampleCategoryFileResult"></a></h2>
<@renderImportFile fileResult=sampleCategoryFileResult showNbLines=false/>

<p>Nombre de catégories : ${sampleCategoryModel.nbSampling}</p>
<ul>
<#list sampleCategoryModel.category as category>
  <li>Catégorie ${category.code} (identifiant ${category.categoryId})</li>
</#list>
</ul>

    <p><a href="#summary_files">Remonter au résumé des fichiers détectés</a></p>
<h2 class="nextPage"></h2>

<h2>Référentiel temporaire Engin <a name="referentialTemporaryGearFileResult"></a></h2>
<#assign fileResult = referentialTemporaryGearFileResult>
<@renderReferentialImportFile fileResult=fileResult/>
<#if fileResult.imported && fileResult.valid>
  <@renderReferentialGearList title="Engins ajoutés" entries=fileResult.entitiesAddedEntries/>
  <@renderReferentialGearList title="Engins associés" entries=fileResult.entitiesLinkedEntries/>
</#if>

<p><a href="#summary_files">Remonter au résumé des fichiers détectés</a></p>
<h2 class="nextPage" style="width: 100%"></h2>

<h2>Référentiel temporaire Personne <a name="referentialTemporaryPersonFileResult"></a></h2>
<#assign fileResult = referentialTemporaryPersonFileResult>
<@renderReferentialImportFile fileResult=fileResult/>
<#if fileResult.imported && fileResult.valid>
  <@renderReferentialPersonList title="Personnes ajoutées" entries=fileResult.entitiesAddedEntries/>
  <@renderReferentialPersonList title="Personnes associées" entries=fileResult.entitiesLinkedEntries/>
</#if>

<p><a href="#summary_files">Remonter au résumé des fichiers détectés</a></p>
<h2 class="nextPage"></h2>

<h2>Référentiel temporaire Taxon <a name="referentialTemporarySpeciesFileResult"></a></h2>
<#assign fileResult = referentialTemporarySpeciesFileResult>
<@renderReferentialImportFile fileResult=fileResult/>
<#if fileResult.imported && fileResult.valid>
  <@renderReferentialSpeciesList title="Taxons ajoutés" entries=fileResult.entitiesAddedEntries/>
  <@renderReferentialSpeciesList title="Taxons associés" entries=fileResult.entitiesLinkedEntries/>
</#if>

<p><a href="#summary_files">Remonter au résumé des fichiers détectés</a></p>
<h2 class="nextPage"></h2>

<h2>Référentiel temporaire Navire <a name="referentialTemporaryVesselFileResult"></a></h2>
<#assign fileResult = referentialTemporaryVesselFileResult>
<@renderReferentialImportFile fileResult=fileResult/>
<#if fileResult.imported && fileResult.valid>
  <@renderReferentialVesselList title="Navires ajoutés" entries=fileResult.entitiesAddedEntries/>
  <@renderReferentialVesselList title="Navires associés" entries=fileResult.entitiesLinkedEntries/>
</#if>

<p><a href="#summary_files">Remonter au résumé des fichiers détectés</a></p>
<h2 class="nextPage"></h2>

<h2>Protocole <a name="protocolFileResult"></a></h2>
<#assign fileResult = protocolFileResult>
<@renderSimpleImportFile fileResult=fileResult/>

<#if fileResult.imported && fileResult.valid>
<p>Un protocol nommé ${protocol.name} a été importé et sélectionné pour l'import.</p>
</#if>

    <p><a href="#summary_files">Remonter au résumé des fichiers détectés</a></p>
<h2 class="nextPage"></h2>

<h2>Fichier des campagnes <a name="surveyFileResult"></a></h2>
<#assign fileResult = surveyFileResult>
<@renderImportFile fileResult=fileResult/>
<#if fileResult.valid>
<p>Des campagnes ont été validées.</p>
</#if>
    <p><a href="#summary_files">Remonter au résumé des fichiers détectés</a></p>
<h2 class="nextPage"></h2>

<h2>Fichier des mises en oeuvre d'engin <a name="gearCaracteristicFileResult"></a></h2>
<#assign fileResult = gearCaracteristicFileResult>
<@renderImportFile fileResult=fileResult/>
<#if fileResult.valid>
<p>Des mises en oeuvre d'engin ont été validées.</p>
</#if>
    <p><a href="#summary_files">Remonter au résumé des fichiers détectés</a></p>
<h2 class="nextPage"></h2>

<h2>Fichier des traits <a name="operationFileResult"></a></h2>
<#assign fileResult = operationFileResult>
<@renderImportFile fileResult=fileResult/>
<#if fileResult.valid>
<p>Des traits ont été validés.</p>
</#if>
    <p><a href="#summary_files">Remonter au résumé des fichiers détectés</a></p>
<h2 class="nextPage"></h2>

<h2>Fichier des caractéristiques de trait <a name="parameterFileResult"></a></h2>
<#assign fileResult = parameterFileResult>
<@renderImportFile fileResult=fileResult/>
<#if fileResult.valid>
<p>Des caractéristiques de trait ont été validées.</p>
</#if>
    <p><a href="#summary_files">Remonter au résumé des fichiers détectés</a></p>
<h2 class="nextPage"></h2>

<h2>Fichier des lots capture <a name="catchFileResult"></a></h2>
<#assign fileResult = catchFileResult>
<@renderImportFile fileResult=fileResult/>
<#if fileResult.valid>
<p>Des lots capture ont été validés.</p>
</#if>
    <p><a href="#summary_files">Remonter au résumé des fichiers détectés</a></p>
<h2 class="nextPage"></h2>

<h2>Fichier des macro déchets <a name="marineLitterFileResult"></a></h2>
<#assign fileResult = marineLitterFileResult>
<@renderImportFile fileResult=fileResult/>
<#if fileResult.valid>
<p>Des macro déchets ont été validés.</p>
</#if>
    <p><a href="#summary_files">Remonter au résumé des fichiers détectés</a></p>
<h2 class="nextPage"></h2>

<h2>Fichier des captures accidentelles <a name="accidentalCatchFileResult"></a></h2>
<#assign fileResult = accidentalCatchFileResult>
<@renderImportFile fileResult=fileResult/>
<#if fileResult.valid>
<p>Des captures accidentelles ont été validées.</p>
</#if>
    <p><a href="#summary_files">Remonter au résumé des fichiers détectés</a></p>
<h2 class="nextPage"></h2>

<h2>Fichier des observations individuelles <a name="individualObservationFileResult"></a></h2>
<#assign fileResult = individualObservationFileResult>
<@renderImportFile fileResult=fileResult/>
<#if fileResult.valid>
<p>Des observations individuelles ont été validées.</p>
</#if>
    <p><a href="#summary_files">Remonter au résumé des fichiers détectés</a></p>
<h2 class="nextPage"></h2>
<h2>Fichier des pièces jointes<a name="attachmentsFileResult"></a></h2>
<#assign fileResult = attachmentsFileResult>
<@renderImportFile fileResult=fileResult/>
<#if fileResult.valid>
<p>Des pièces-jointes ont été validées.</p>
</#if>
    <p><a href="#summary_files">Remonter au résumé des fichiers détectés</a></p>
</body>
</html>

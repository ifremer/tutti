<#--
 #%L
 Tutti :: Service
 %%
 Copyright (C) 2012 - 2014 Ifremer
 %%
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public
 License along with this program.  If not, see
 <http://www.gnu.org/licenses/gpl-3.0.html>.
 #L%
-->
<html>
<head>
    <style type="text/css">

      @page { size: A4 landscape;}

      <#assign blueColor="#000080">
      <#assign lightGrayColor="#ddd">

      body {
          font-size: 12pt;
      }

      h1 {
          background: url("data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAKEAAAAeCAYAAABEzX4WAAAABmJLR0QA/wD/AP+gvaeTAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAB3RJTUUH3gsYESwznZhaYQAAC8VJREFUeNrtnHmQHPV1xz+vu+fanZm9b2l1YFlCSGBkkI3DYQpwqrI7rFwuK44ph8QmcQyuUCQYl3EO4xg7wQaDBSZCMahQIiiBjXZWoiiIywIZZbkMiAAxhyWxOliJPbTH3N0vf/x6tKPdjQMY5BGaV9PVU7/+Xb2/7+/73vd1zwrHmW3ZsoXu7m6SyeRFIhIHxgBUNSsiu4GDIpLv7u52i20eeeQRLrroonc95ubNm1m1ahUVe39MjkMQ1qtqN3CBqp4sIiuAw0BaVV8BBkTkGeBRVd0nIqOJRKKQTCarLMuyuru7JwCSyWRMRCwfxA1ACnBUVUUEVS3Ytp3v6uoqVGBSAeFR1tfXFxCRJap6sapeKiINgAPE/SopVU0BeeBxEfmlqg4AnSJSr6qjIpJV1VNEJK2qh0TkD4DfABlVbQEUmBCRMf/6IHAA2JVIJPaUzIVEIlFB0YkEwuKib9u2jfHx8QWqeqGIXAKsBCI+eErvyVXVgs94I6oaEpEaVcVnuzEgLnKkyV7gLWApECyph4gcAPYALwKbEonEw6XhQcVOLHd8ZNEffPDBQKFQWAN82QdSEYS+Vz369lTV3PQUsIplxcrjwK+Bxaoam16v2J+qHhSRjYlE4iqA3t5eenp6Kmh6l2YdbxPu7u6mr6/P0Jzr1gIjPjg8VS2ibgYAi+ArlouITi9T1aiqrgCipW1KzsU2zcAVyWTyVqACwBMJhL29vQAkEgn6+vrCwCdF5AxVzQFWEVjvwgtIEbm+654VxKVtVDUgIpf29fV9oQKjEwSEmzZtoqenh3Xr1tHb21sPnAJ8XVWXiUj2WIcXPuCjwJeSyWTH9I1SsQ8gCFevXg1AQ0NDWEROVtUunwFTQOj3EU/78eR5IvIPfX19pwEEg8EKqj6owqS3txcRqQLqgC/4qlj949RZlPE7tRntp4mXWWPMknpvqupfXXzxxRUqfIfmlPsEt27dSldXV3HDdIrIUqBRVXeJyGmYRPP/CaoSIJXK5emAK62bAQZV9WcickBVR0RkFJMQL/jtwpjEdrWIxP38oy0iOyuQ+oAy4bXXqzOnRZ22hm1n2nbqEqCgKikR72xgkYhGQcJFPKmKiqioiuu76yrAVqwxQQOgERFVEClmc1TtnSD/XXDDP05nm159eMcpQ3evEbcCkWMR14zx7XKdXBEmk+kWMtnGurHJBcuGRk6dcL1gRESXT6TbBjOZpqingQWqokW9IOKharkibs6205FwcGTAkkJNU/2zFApVAweHP9qUyrQ0+xkXIqGh/Yvmb3yrPv7Sjlhs7348HMOMZvyKva+eeIfoGFrOAEQh71YxOrYE1wvj2BNMptsZOrwM1CKbqyWVadF0tklULRw7jaeORkJvSSR0EM8L6Emd90td/BVULQLOOE+8cB3Do8vVw5JoZL92tj8mHzppg8GkB7gl41fs/V7jdWUbExpHCZ4XRMTFw8LzLDzLwbYz1MdfZHR8CblCDNcLSCQ8SDrTDKKEAqMyv30L1VX7qa99TQLVIwZceRgeWUoq3U7BC0lV+CBLFvy7PP/8Czz6Cyi4sGghnLUSIpEKSI5RLJhzypYBAVUL1wsxmWkl6IwTrtrD6PgiXDeMbecIBg4TcFJ4XgBQolX7aG3sp67mZWqjrxCKHGbPLvjNbtPv2WfB6PgiLCtPa2M/J3X285O7nuaWH0/y5qAZ86yVcN/d0FEF5ekjKur4GDGgoCpYloeXD+AWIoSCoygWtp0mbBWYTLeSzrQQcA4TrRrAsVO0NvRTE3sdy8pRfHiy6QG4/gdQKMDrz0MoNMqH522kuXEn42OTrP1JhjcHIRSCyy+DFR+B6uoKAI8VFWr5pmhMWiVfqEZEqY4cIO9GGJ+cZ9jPniSbqyNbiNLR/BjRyD5i0V0E7ckZen9iAg4fNt89Dzpatxu2DXgMvgaptLl2/jlw083gToBlVfBxLF1y2caEquC6EVzPIRwcxlObgDOG64ZIZxqxrDwnL1hPtGofASc15cr/vySUeKDgZmEyZdoA1NYAWXBdsG1TlsmCWwDbgVAQ0hnwXMOUxZAhn4dczgBcLAgGIRg4OrTwPEinzVxCwam+1QPHgXB4qqxQMFMOBs0xXSCp+mPmTXvLNn06zvRQxmywYl8AmQwEAob1y4RrQMrwsZ0BhQVYOPYkwcAY6Wwjrhsm4Ewg4uJ6YdqatlNX8yoBJ3UESG9XzQ4Nw599BS7ohqEhU7Z5K9TNgRtuAYJAGL56NcxfBl+8HJ56BlaeB/WdU/0cPATf+h60L4Z4B7R9CP72Wth/APCBjAWvvmbqnHwG3LkBvn8LNM43bc7vgqefNccF3RBvh9q58NfXwPiEAbbq1GbZtRuuuBrq55r2Jy2Hf/mhX9f26wlkc7BwOaw4B+65H269A2LtcO11lN2qlyUTep6ZlmXnEDxyEiNfiBIMjFFdtZ+6+K+xrALqGeC901SKZUFDHTQ1+uxWgEgY2tsgHp3aoakUvDUEj/fDSy/Di/8DNTWmj1QKrvoGbNwEy5bC8qXwxNNw61rYvQc23gmxmOmrOgpjY+b455vMfNtaYPcbps0fX2rU+PAINDaYMdfeCa3N8K3vgBiiZ3gEPvfn8NSv4MwVsHABPPSf8Hf/BG/shbW3GaJHIRwxm+TwGKy7C/7rSX/By3DFyzT6URBF1SKbr8Vx0sSjuwgFhwkHR7CswuyudhazrSPEf8Ticbj2avjR96HG/1HAOZ+AhzfD51djHs4xFRu+MQCuB/96M6y/3ZRt224AuHgR3PEj2Lge7l1vrv38Udj6kM+oQKBk4YNBuPF66L0XTj/VZ7c90DkXeu+B+zdMuewdT4A77t9nEG5bawD4sTPgvg1w791wg/+o4b4H4Ml+IFC8cXPKZg3QP9MD99wFn/+snwstKyZUvlt2O0NcFCFfiIp6YgcCGRVxXUHxP8jbwaCN9+LLnAecc9RN29DcAQuGpuK/eAzaPwxMmFirtO9YFK66Ar74JX8BC7DhXnPtwvPhrLPN4p95Lnz8TOh/Cp57AT6XLw0xjPV0wapPG1D1dMGzO6EqApeshpV+P8uWwjPPwUTKsF9Ts/FZt9xuNsaF58O8pWZr/eUV8OUrDTP/YjusPNfkQ0u33WnL4YbvQOdCM3d1yyQRf0QdK/9YdgnMonhwgzj2JKiF61kEnMzs4mM2CwMhCj9L8u3pIDQ+3wiGo2NRn4SnDdDcBKctM200b673P2WuPfk0XH2NESwAgwfN+c1BmJzw0z0l1tTos1QeGvxXL6qrob4OcM0foNEv91wjVLBgcDcMjZhN89jj8PWrjEAJBAwwsznYMzC7b5vfCZ2LgHT5PQkSwJFayvgnjYfekxDzd3YXjq8oSxZweNScn3nOsF5pvGlZBjz5/NvZcTMpfQZILCOmwKj3Hf1TmwA1Lt7zjEqf1SHYhknL7lGk79LK/lWucrWamBEaf/on8DdfNeA4gnqFulqIVr93i1VX68eUAfjKZeYopmCK61lfi3ln6Lc8CChHq4DwXdrHV8LAAyb/t2QxBOI+51r+kZkSOL+zudA2DxrqDSOmM7B4hd+/loyZKqN47/hXx+Vvl11qzlsegutvgJd+BXtfhye3wx1rYOdzJbnC94gNv3al+Zp8EG69EV5/Cfa8Cr/8Odz4PTg4aHKFx5udCEwovz0nOVPBThcrns4sP/cTcM2VJrn93Rvhrv8wqZjJlAHM7TfDqafPEpHqzO/qvY1xc3D5X5gUzU974Zq/h5vWGNabmDTnP/oUNLfNHFO1rFfG/sCDcG4Ho47DUCpFDhgA3OIr1SIEO+fSFIti19aQAg6p7x0EvNo4jXM6iHa0khNhCMgVxXk4DNd9E124AOuHt9G6dx9NniLNTYz84QXs/+jp5HHx34jEbmlmbiiIFw4ziv9PnEJBYu2t1Le2UHBsho1+RevraZnTTqSliSzCIcBVRWJR+Lc16EeW46xbT9uBQRptG6+jjUOrujkwrxMtGZPOOXQC1NYefW9lZEGB1/4X0fnqfLJ+VzcAAAAASUVORK5CYII=") top left no-repeat;
          width: 100%;
          text-align: center;
      }

      h1, h4 {
          color: ${blueColor};
          font-weight: bold;
          font-style: italic;
      }

      th {
          color: ${blueColor};
          font-weight: bold;
      }

      td, th {
          padding: 2pt;
          vertical-align: top;
          height: 16pt;
      }

      td + td, th + th {
          padding-right: 10pt;
      }

      td.number {
          text-align: right;
      }

      th.text {
          text-align: center;
      }

      .alternateRows > tr:nth-child(odd) {
          background-color: ${lightGrayColor};
      }

      .operationInfo {
          font-weight: bold;
      }

      .label {
          color: ${blueColor};
      }

      .value {
          margin-right: 50pt;
      }

      .center {
          margin: auto;
      }

      .bordered {
          border: 1pt solid black;
          border-collapse: collapse;
      }

      .bordered tr :nth-child(3) {
          border-right: 1pt solid black;
      }

      .top-bottom-border {
          border-top: 1pt solid black;
          border-bottom: 1pt solid black;
      }

    </style>
</head>
<body>

<h1>
  Rapport détaillé du tri
</h1>

<hr/>

<p class='operationInfo'>
    <span class="label">Station :</span> <span class="value">${stationNumber} - ${fishingOperationNumber}</span>
        <span class="label">
          du ${gearShootingStartDate?date?string.short} ${gearShootingStartDate?time?string.short}
        <#if gearShootingEndDate??>
            au ${gearShootingEndDate?date?string.short} ${gearShootingEndDate?time?string.short}</#if>
        </span>
</p>

<hr/>

<table class="center bordered">
    <thead>
      <tr>
          <th colspan="3" class="text bordered">Balance Trémie</th>
          <th colspan="3" class="text bordered">Balance Carrousel</th>
      </tr>
      <tr>
          <th class="text">Trié</th>
          <th class="text">Non trié</th>
          <th class="text">Total</th>
          <th class="text">Vrac</th>
          <th class="text">Hors vrac</th>
          <th class="text">Total</th>
      </tr>
    </thead>
    <tbody>
        <tr>
            <td class="number"><#if trunkSortedWeight??>${trunkSortedWeight?string("0.00")}</#if></td>
            <td class="number"><#if trunkRejectedWeight??>${trunkRejectedWeight?string("")}</#if></td>
            <td class="number"><#if trunkTotalWeight??>${trunkTotalWeight?string("0.00")}</#if></td>
            <td class="number"><#if carrouselSortedWeight??>${carrouselSortedWeight?string("0.00")}</#if></td>
            <td class="number"><#if carrouselUnsortedWeight??>${carrouselUnsortedWeight?string("")}</#if></td>
            <td class="number"><#if carrouselTotalWeight??>${carrouselTotalWeight?string("0.00")}</#if></td>
        </tr>
    </tbody>
</table>

<p>
    NOTE: Les poids affichés sont en kg.
</p>

<table>
  <thead>
    <tr class="top-bottom-border">
        <th></th>
        <th>Espèce</th> <!-- trie d'abord tout le vrac sur le code espèce puis tout le HV trié sur le code espèce-->
        <th>V / HV</th>
        <th>Nom scientifique</th>
        <th>Nom commun</th>
        <th>Poids trié</th>
        <th>Signe</th>
        <th>Nb de caisses</th>
        <th>Petite</th>
        <th>Grande</th>
    </tr>
  </thead>

  <tbody class="alternateRows">
<#list rows as row>
    <tr>
        <td><input type="checkbox"/></td>
        <td>${row.speciesCode}</td>
        <td><#if row.sorted>V<#else>HV</#if></td>
        <td><em>${row.speciesName}</em></td>
        <td><#if row.speciesVernucalCode??>${row.speciesVernucalCode}</#if></td>
        <td class="number">${row.sortedWeight}</td>
        <td class="number">${row.sign}</td>
        <td class="number">${row.nbBox}</td>
        <td class="number">${row.nbSmallBox}</td>
        <td class="number">${row.nbBigBox}</td>
    </tr>
</#list>
    <tr>
        <td><input type="checkbox"/></td>
        <td>  </td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td><input type="checkbox"/></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td><input type="checkbox"/></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
  </tbody>
</table>

<h4>Espèces non importées</h4>
<ul>
<#list notImportedSpeciesIds as speciesId>
  <li>${speciesId}</li>
</#list>
</ul>

</body>
</html>

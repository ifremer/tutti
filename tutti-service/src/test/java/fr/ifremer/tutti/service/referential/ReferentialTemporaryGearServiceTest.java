package fr.ifremer.tutti.service.referential;

/*
 * #%L
 * Tutti :: Service
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Charsets;
import com.google.common.io.Files;
import fr.ifremer.tutti.persistence.entities.referential.Gear;
import fr.ifremer.tutti.service.ServiceDbResource;
import fr.ifremer.tutti.service.TuttiServiceContext;
import org.junit.Assert;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Test;
import org.nuiton.jaxx.application.ApplicationBusinessException;

import java.io.File;
import java.util.List;

public class ReferentialTemporaryGearServiceTest {

    @ClassRule
    public static final ServiceDbResource dbResource = ServiceDbResource.writeDb("dbCGFS");

    protected File dataDirectory;

    protected ReferentialTemporaryGearService service;

    private static final String GEAR_FILE_CONTENT =
            "id;name;label;scientificGear;toDelete\n" +
            ";Gear fishing name 1;Gear fishing label 1;N;\n" +
            ";Gear fishing name 2;Gear fishing label 2;N;\n" +
            ";Gear scientific name 3;Gear scientific label 3;Y;\n" +
            ";Gear scientific name 4;Gear scientific label 4;Y;";

    private static final String GEAR_DUPLICATE_NAME_FILE_CONTENT =
            "id;name;label;scientificGear;toDelete\n" +
            ";Temporary Gear name 1;;;\n" +
            ";Temporary Gear name 1;;;";

    private static final String GEAR_BLANK_NAME_FILE_CONTENT =
            "id;name;label;scientificGear;toDelete\n" +
            "; ;;;";

    private static final String GEAR_UPDATE_FILE_CONTENT =
            "id;name;label;scientificGear;toDelete\n" +
            "-1;Temporary Gear name 11;;N;N\n" +
            "-2;Temporary Gear name 2;;;Y";

    private static final String GEAR_DELETE_USED_FILE_CONTENT =
            "id;name;label;scientificGear;toDelete\n" +
            "-1;Temporary Gear name 11;;;Y";


    @Before
    public void setUp() throws Exception {

        dataDirectory = dbResource.getConfig().getDataDirectory();

        TuttiServiceContext serviceContext = dbResource.getServiceContext();

        serviceContext.getConfig().setCsvSeparator(';');

        service = serviceContext.getService(ReferentialTemporaryGearService.class);

    }

    @Test
    public void importTemporaryGear() throws Exception {
        File file = new File(dataDirectory, "importGear.csv");

        Files.createParentDirs(file);

        Files.write(GEAR_FILE_CONTENT, file, Charsets.UTF_8);

        ReferentialImportResult<Gear> result = service.importTemporaryGear(file);
        List<Gear> addedGears = result.getRefAdded();

        Assert.assertNotNull(result);
        Assert.assertEquals(4, addedGears.size());
        for (int i = 1; i <= 2; i++) {
            Gear actual = addedGears.get(i - 1);
            Assert.assertNotNull(actual);
            Assert.assertTrue(actual.getId().startsWith("-"));
            Assert.assertEquals("Gear fishing name " + i, actual.getName());
            Assert.assertEquals("Gear fishing label " + i, actual.getLabel());
            Assert.assertFalse(actual.isScientificGear());
        }
        for (int i = 3; i <= 4; i++) {
            Gear actual = addedGears.get(i - 1);
            Assert.assertNotNull(actual);
            Assert.assertTrue(actual.getId().startsWith("-"));
            Assert.assertEquals("Gear scientific name " + i, actual.getName());
            Assert.assertEquals("Gear scientific label " + i, actual.getLabel());
            Assert.assertTrue(actual.isScientificGear());
        }

        // TODO test to replace a used gear
    }

    @Test
    public void importNotExistingIdTemporaryGear() throws Exception {

        File file = new File(dataDirectory, "importGear.csv");

        Files.createParentDirs(file);

        // try to import not existing id
        Files.write(GEAR_UPDATE_FILE_CONTENT, file, Charsets.UTF_8);
        try {
            service.importTemporaryGear(file);
            Assert.fail();
        } catch (ApplicationBusinessException e) {
            Assert.assertTrue(true);
        }
    }

    @Test
    public void importBlankNameTemporaryGear() throws Exception {

        File file = new File(dataDirectory, "importGear.csv");

        Files.createParentDirs(file);
        // try to import blank name
        Files.write(GEAR_BLANK_NAME_FILE_CONTENT, file, Charsets.UTF_8);
        try {
            service.importTemporaryGear(file);
            Assert.fail();
        } catch (ApplicationBusinessException e) {
            Assert.assertTrue(true);
        }
    }

    @Test
    public void importDuplicateTemporaryGear() throws Exception {

        File file = new File(dataDirectory, "importGear.csv");

        Files.createParentDirs(file);

        // try to import duplicate names
        Files.write(GEAR_DUPLICATE_NAME_FILE_CONTENT, file, Charsets.UTF_8);
        try {
            service.importTemporaryGear(file);
            Assert.fail();
        } catch (ApplicationBusinessException e) {
            Assert.assertTrue(true);
        }
    }

}
package fr.ifremer.tutti.service.export.sumatra;

/*
 * #%L
 * Tutti :: Service
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.ProgressionModel;
import fr.ifremer.tutti.persistence.entities.protocol.TuttiProtocol;
import fr.ifremer.tutti.service.PersistenceService;
import fr.ifremer.tutti.service.ServiceDbResource;
import fr.ifremer.tutti.service.TuttiServiceContext;
import fr.ifremer.tutti.service.catches.TuttiWeightComputingException;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Test;

import java.io.File;

/**
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.4
 */
public class CatchesSumatraExportServiceTest {

    @ClassRule
    public static final ServiceDbResource dbResource =
            ServiceDbResource.readDb("dbExport");

    public static final String PROGRAM_ID = "CAM-TEST_ELEVATION";

    public static final Integer CRUISE_BAD_ID = 100003;

    public static final Integer CRUISE_ID = 100004;

    public static final Integer OPERATION_1_ID = 100116;

    public static final Integer OPERATION_BAD_1_ID = 100112;

    public static final Integer OPERATION_BAD_2_ID = 100113;

    public static final Integer OPERATION_BAD_3_ID = 100115;

    public static final String EXPORT_CONTENT =
            "annee;station;poche;especescientifique;espececampagne;signe;tri;total;poidsmoy;longueurmoy;nbindividus;moule;latitudedebut;longitudedebut;latitudefin;longitudefin;datedebutstation;datefinstation\n" +
            "2013;A;1;Chama magna;CHAMMAG;;100.0;100.0;2.5;;40;0.4;;;;;27/09/2013 00:00:00;27/09/2013 00:00:00\n" +
            "2013;A;1;Echinogammarus;ECHIGAM;;100.0;100.0;0.23980816;;417;4.17;;;;;27/09/2013 00:00:00;27/09/2013 00:00:00\n" +
            "2013;A;1;Brissopsis atlantica;BRISATLAN;;100.0;100.0;1.0638298;10.571428;94;0.94;;;;;27/09/2013 00:00:00;27/09/2013 00:00:00";

    protected CatchesSumatraExportService service;

    protected ServiceDbResource.DataContext dataContext;

    protected File dataDirectory;

    @Before
    public void setUp() throws Exception {

        dataDirectory = dbResource.getConfig().getDataDirectory();

        TuttiServiceContext serviceContext = dbResource.getServiceContext();

        dbResource.openDataContext();

        File protocolFile = new File("src/test/resources/tuttiProtocol.tuttiProtocol");

        TuttiProtocol protocol = dbResource.loadProtocol(protocolFile);

        serviceContext.getService(PersistenceService.class).setProtocol(protocol);

        service = serviceContext.getService(CatchesSumatraExportService.class);
    }

    @Test(expected = TuttiWeightComputingException.class)
    public void testExportCruiseForSumatraBadCruise() throws Exception {

        dataContext = dbResource.loadContext(PROGRAM_ID, CRUISE_BAD_ID, 3,
                                             OPERATION_BAD_2_ID,
                                             OPERATION_BAD_1_ID,
                                             OPERATION_BAD_3_ID);

        File exportFile = new File(dataDirectory, "exportSumatra.csv");

        ProgressionModel progressionModel = new ProgressionModel();
        progressionModel.setTotal(3);
        service.exportCruiseForSumatra(exportFile, CRUISE_BAD_ID, progressionModel);
    }

    @Test
    public void testExportCruiseForSumatra() throws Exception {

        dataContext = dbResource.loadContext(PROGRAM_ID, CRUISE_ID, 1, OPERATION_1_ID);

        File exportFile = new File(dataDirectory, "exportSumatra.csv");

        ProgressionModel progressionModel = new ProgressionModel();
        progressionModel.setTotal(3);
        service.exportCruiseForSumatra(exportFile, CRUISE_ID, progressionModel);

        ServiceDbResource.assertFileContent("Sumatra export file:\n",
                                            exportFile,
                                            EXPORT_CONTENT);
    }
}

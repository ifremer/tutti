package fr.ifremer.tutti.service.catches;

/*
 * #%L
 * Tutti :: Service
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.entities.data.BatchContainer;
import fr.ifremer.tutti.persistence.entities.data.CatchBatch;
import fr.ifremer.tutti.persistence.entities.data.MarineLitterBatch;
import fr.ifremer.tutti.persistence.entities.data.SpeciesBatch;
import fr.ifremer.tutti.service.PersistenceService;
import fr.ifremer.tutti.service.ServiceDbResource;
import fr.ifremer.tutti.service.TuttiServiceContext;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Assert;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Test;
import org.nuiton.jaxx.application.ApplicationBusinessException;

import java.util.List;

/**
 * @author Kevin Morin - kmorin@codelutin.com
 * @since 2.3
 */
public class WeightComputingServiceTest {

    /** Logger. */
    private static final Log log =
            LogFactory.getLog(WeightComputingServiceTest.class);

    @ClassRule
    public static final ServiceDbResource dbResource =
            ServiceDbResource.readDb("dbCGFS");

    public static final String PROGRAM_ID = "CAM-CGFS";

    public static final Integer CRUISE_ID = 100002;

    /* the one with the batches to compute */
    public static final Integer OPERATION_1_ID = 100108;

    /* the one with the total species weight less than the sum of the batches */
    public static final Integer OPERATION_2_ID = 100109;

    /* the one with the total benthos weight less than the sum of the batches */
    public static final Integer OPERATION_3_ID = 100110;

    /* the one with the total marine litter weight less than the sum of the batches */
    public static final Integer OPERATION_4_ID = 100111;

    /* the one with the total catch weight different from the sum of the sorted and unsorted total weight */
    public static final Integer OPERATION_5_ID = 100112;

    /* the one with the total catch weight different from the rejected weight */
    public static final Integer OPERATION_6_ID = 100113;

    /* the one working */
    public static final Integer OPERATION_7_ID = 100114;

    protected WeightComputingService weightComputingService;

    protected PersistenceService persistenceService;

    @Before
    public void setUp() throws Exception {
        TuttiServiceContext serviceContext = dbResource.getServiceContext();

        persistenceService = serviceContext.getService(PersistenceService.class);

        serviceContext.getDataContext().setPersistenceService(persistenceService);
        serviceContext.getDataContext().open(serviceContext.getConfig());
        weightComputingService = serviceContext.getService(WeightComputingService.class);
    }

    @Test
    public void computeCatchWeights() {
        CatchBatch catchBatch = persistenceService.getCatchBatchFromFishingOperation(OPERATION_2_ID);
        try {
            BatchContainer<SpeciesBatch> batches = persistenceService.getRootSpeciesBatch(OPERATION_2_ID, false);
            weightComputingService.computeCatchBatchWeights(catchBatch, batches, null, null);
            Assert.fail();

        } catch (ApplicationBusinessException e) {
            //ok, it is supposed to throw an exception
            if (log.isInfoEnabled()) {
                log.info("expected error on operation #2 : " + e.getMessage());
            }
        }

        catchBatch = persistenceService.getCatchBatchFromFishingOperation(OPERATION_3_ID);
        try {
            BatchContainer<SpeciesBatch> batches = persistenceService.getRootBenthosBatch(OPERATION_3_ID, false);
            weightComputingService.computeCatchBatchWeights(catchBatch, null, batches, null);
            Assert.fail();

        } catch (ApplicationBusinessException e) {
            //ok, it is supposed to throw an exception
            if (log.isInfoEnabled()) {
                log.info("expected error on operation #3 : " + e.getMessage());
            }
        }

        catchBatch = persistenceService.getCatchBatchFromFishingOperation(OPERATION_4_ID);
        try {
            BatchContainer<MarineLitterBatch> batches = persistenceService.getRootMarineLitterBatch(OPERATION_4_ID);
            weightComputingService.computeCatchBatchWeights(catchBatch, null, null, batches);
            Assert.fail();

        } catch (ApplicationBusinessException e) {
            //ok, it is supposed to throw an exception
            if (log.isInfoEnabled()) {
                log.info("expected error on operation #4 : " + e.getMessage());
            }
        }

        catchBatch = persistenceService.getCatchBatchFromFishingOperation(OPERATION_5_ID);
        try {
            BatchContainer<SpeciesBatch> speciesBatches = persistenceService.getRootSpeciesBatch(OPERATION_5_ID, false);
            BatchContainer<SpeciesBatch> benthosBatches = persistenceService.getRootBenthosBatch(OPERATION_5_ID, false);
            weightComputingService.computeCatchBatchWeights(catchBatch, speciesBatches, benthosBatches, null);
            Assert.fail();

        } catch (ApplicationBusinessException e) {
            //ok, it is supposed to throw an exception
            if (log.isInfoEnabled()) {
                log.info("expected error on operation #5 : " + e.getMessage());
            }
        }

        catchBatch = persistenceService.getCatchBatchFromFishingOperation(OPERATION_6_ID);
        try {
            weightComputingService.computeCatchBatchWeights(catchBatch, null, null, null);
            Assert.fail();

        } catch (ApplicationBusinessException e) {
            //ok, it is supposed to throw an exception
            if (log.isInfoEnabled()) {
                log.info("expected error on operation #6 : " + e.getMessage());
            }
        }

        catchBatch = persistenceService.getCatchBatchFromFishingOperation(OPERATION_7_ID);
        try {
            BatchContainer<SpeciesBatch> speciesBatches = persistenceService.getRootSpeciesBatch(OPERATION_7_ID, false);
            BatchContainer<SpeciesBatch> benthosBatches = persistenceService.getRootBenthosBatch(OPERATION_7_ID, false);
            weightComputingService.computeCatchBatchWeights(catchBatch, speciesBatches, benthosBatches, null);
            if (log.isInfoEnabled()) {
                log.info("Weight computing worked on operation #7");
            }

        } catch (ApplicationBusinessException e) {
            Assert.fail();
        }
    }

    @Test
    public void computeSpeciesBatch() {
        BatchContainer<SpeciesBatch> speciesBatchContainer = persistenceService.getRootSpeciesBatch(OPERATION_1_ID, false);
        List<SpeciesBatch> speciesBatches = speciesBatchContainer.getChildren();
        for (int i = 0; i < speciesBatches.size() - 1; i++) {
            try {
                SpeciesBatch batch = speciesBatches.get(i);
                weightComputingService.computeSpeciesBatch(batch);
                //expected error on species batch #0 : Espèces - Le lot de AAPT - Aaptos/V/HV/Vrac n'a pas de poids
                //expected error on species batch #1 : Espèces - Le lot de AAPTAAP - Aaptos aaptos/V/HV/Vrac n'a pas de poids
                //expected error on species batch #2 : Espèces - Le poids de sous-échantillon du lot ABAL - Abalistes/V/HV - Vrac (42.0kg) est supérieur au poids du lot (28.0kg)
                //expected error on species batch #3 : Espèces - Le poids du lot de ABALSTE - Abalistes stellatus/V/HV - Vrac (28.0kg) est inférieur à la somme des poids de ses sous-catégories (30.0kg)
                //expected error on species batch #4 : Espèces - Le poids total des mensurations du lot de ABIE - Abietinaria/V/HV - Vrac (0.9kg) est différent du poids du sous-échantillon (28.0kg)
                //expected error on species batch #5 : Espèces - Le poids total des mensurations du lot de ABIEABI - Abietinaria abietina/V/HV - Vrac (101.6kg) est supérieur au poids de la catégorie (42.0kg)
                Assert.fail();
            } catch (ApplicationBusinessException e) {
                //ok, it is supposed to throw an exception
                if (log.isInfoEnabled()) {
                    log.info("expected error on species batch #" + i + " : " + e.getMessage());
                }
            }
        }
        SpeciesBatch speciesBatch = speciesBatches.get(speciesBatches.size() - 1);
        try {
            weightComputingService.computeSpeciesBatch(speciesBatch);
            if (log.isInfoEnabled()) {
                log.info("last species batch weight computing worked");
            }

        } catch (ApplicationBusinessException e) {
            Assert.fail();
        }

        BatchContainer<SpeciesBatch> benthosBatchContainer = persistenceService.getRootBenthosBatch(OPERATION_1_ID, false);
        List<SpeciesBatch> benthosBatches = benthosBatchContainer.getChildren();
        for (int i = 0; i < benthosBatches.size() - 1; i++) {
            try {
                SpeciesBatch batch = benthosBatches.get(i);
                weightComputingService.computeBenthosBatch(batch);
                //expected error on species batch #0 : Benthos - Le lot de ABLEHIA - Ablennes hians/V/HV - Vrac n'a pas de poids
                //expected error on species batch #1 : Benthos - Le lot de ABLU - Abludomelita/V/HV - Vrac n'a pas de poids
                //expected error on species batch #2 : Benthos - Le poids de sous-échantillon du lot ABLUOBT - Abludomelita obtusata/V/HV - Vrac (42.0kg) est supérieur au poids du lot (28.0kg)
                //expected error on species batch #3 : Benthos - Le poids du lot de ABRA - Abra/V/HV - Vrac (28.0kg) est inférieur à la somme des poids de ses sous-catégories (30.0kg)
                //expected error on species batch #4 : Benthos - Le poids total des mensurations du lot de ABRAALB - Abra alba/V/HV - Vrac (28.0kg) est différent du poids du sous-échantillon (42.0kg)
                //expected error on species batch #5 : Benthos - Le poids total des mensurations du lot de ABRALON - Abra longicallus/V/HV - Vrac (150.6kg) est supérieur au poids de la catégorie (42.0kg)
                Assert.fail();
            } catch (ApplicationBusinessException e) {
                //ok, it is supposed to throw an exception
                if (log.isInfoEnabled()) {
                    log.info("expected error on species batch #" + i + " : " + e.getMessage());
                }
            }
        }
        SpeciesBatch benthosBatch = benthosBatches.get(benthosBatches.size() - 1);
        try {
            weightComputingService.computeBenthosBatch(benthosBatch);
            if (log.isInfoEnabled()) {
                log.info("last benthos batch weight computing worked");
            }

        } catch (ApplicationBusinessException e) {
            Assert.fail();
        }
    }

}

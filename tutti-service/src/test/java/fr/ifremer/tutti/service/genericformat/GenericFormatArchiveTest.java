package fr.ifremer.tutti.service.genericformat;

/*
 * #%L
 * Tutti :: Service
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.junit.Assert;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;

/**
 * Created on 4/1/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.14.3
 */
public class GenericFormatArchiveTest {

    @Test
    public void testCountLines() throws IOException {

        assertCountLines("src/test/resources/genericFormat/test_countLines/zeroLines.txt", 0);
        assertCountLines("src/test/resources/genericFormat/test_countLines/oneEmptyLine.txt", 0);
        assertCountLines("src/test/resources/genericFormat/test_countLines/oneLine.txt", 1);
        assertCountLines("src/test/resources/genericFormat/test_countLines/twoLines_secondEmpty.txt", 1);
        assertCountLines("src/test/resources/genericFormat/test_countLines/twoLines.txt", 2);

    }

    protected void assertCountLines(String filepath, int expected) throws IOException {

        File file = Paths.get(new File("").getAbsolutePath(), filepath).toFile();
        Assert.assertTrue("File " + file + " does not exists", file.exists());
        int actual = GenericFormatArchive.countLines(file.toPath());
        Assert.assertEquals("Sould have " + expected + " lines in file " + file, expected, actual);

    }

}

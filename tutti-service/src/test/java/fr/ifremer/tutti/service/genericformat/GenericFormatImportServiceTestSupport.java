package fr.ifremer.tutti.service.genericformat;

/*
 * #%L
 * Tutti :: Service
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.test.TuttiTestSupport;
import fr.ifremer.tutti.service.PersistenceService;
import fr.ifremer.tutti.service.ServiceDbResource;
import fr.ifremer.tutti.service.TuttiServiceContext;
import org.apache.commons.io.FileUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Before;
import org.nuiton.util.FileUtil;

import java.io.File;
import java.io.IOException;

/**
 * Created on 2/23/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.14
 */
public abstract class GenericFormatImportServiceTestSupport extends TuttiTestSupport {

    /** Logger. */
    private static final Log log = LogFactory.getLog(GenericFormatImportServiceTestSupport.class);

    public static final String PROGRAM_ID = "CAM-EVHOE";

    protected GenericFormatImportService service;

    protected PersistenceService persistenceService;

    protected abstract ServiceDbResource getServiceDbResource();

    @Before
    public void setUp() throws Exception {

        ServiceDbResource serviceDbResource = getServiceDbResource();

        TuttiServiceContext serviceContext = serviceDbResource.getServiceContext();

        persistenceService = serviceContext.getService(PersistenceService.class);

        serviceDbResource.setCountryInConfig("12");
        serviceDbResource.openDataContext();

        service = serviceContext.getService(GenericFormatImportService.class);

    }

    protected File createArchive(String archiveFileName, String... directoryNames) throws IOException {

        File tmpDirectory = getServiceDbResource().getConfig().getTmpDirectory();

        File explodedDirectory = new File(tmpDirectory, archiveFileName);
        if (log.isInfoEnabled()) {
            log.info("Exploded directory of archive: " + explodedDirectory);
        }

        for (String directoryName : directoryNames) {

            File directory = FileUtil.getFileFromPaths(new File("src"), "test", "resources", "genericFormat", directoryName);

            if (directory.exists()) {

                if (log.isInfoEnabled()) {
                    log.info("Copy into archive files from " + directory);
                }
                FileUtils.copyDirectory(directory, explodedDirectory);

            }

        }

        File dataDirectory = getServiceDbResource().getConfig().getDataDirectory();
        File archiveFile = new File(dataDirectory, archiveFileName);
        if (log.isInfoEnabled()) {
            log.info("Build import archive: " + archiveFile);
        }

        GenericFormatArchive archiveToExport = GenericFormatArchive.forExportFromWorkingDirectory(archiveFile, explodedDirectory);

        archiveToExport.createZip(null);

        return archiveFile;

    }

}

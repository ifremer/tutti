package fr.ifremer.tutti.service.genericformat;

/*
 * #%L
 * Tutti :: Service
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.ProgressionModel;
import fr.ifremer.tutti.persistence.model.CruiseDataModel;
import fr.ifremer.tutti.persistence.model.OperationDataModel;
import fr.ifremer.tutti.persistence.model.ProgramDataModel;
import fr.ifremer.tutti.service.ServiceDbResource;
import fr.ifremer.tutti.service.referential.ReferentialTemporaryGearService;
import fr.ifremer.tutti.service.referential.ReferentialTemporaryPersonService;
import fr.ifremer.tutti.service.referential.ReferentialTemporarySpeciesService;
import fr.ifremer.tutti.service.referential.ReferentialTemporaryVesselService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.util.LinkedHashSet;
import java.util.Set;

/**
 * Created on 2/15/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.14
 */
public class GenericFormatImportServiceTest extends GenericFormatImportServiceTestSupport {

    /** Logger. */
    private static final Log log = LogFactory.getLog(GenericFormatImportServiceTest.class);

    @Rule
    public final ServiceDbResource dbResource = ServiceDbResource.writeDb("dbEmpty");

    @Override
    protected ServiceDbResource getServiceDbResource() {
        return dbResource;
    }

    @Test
    public void testImport() throws IOException {

        dbResource.setDestroyResources(false);

        StringBuilder builder = new StringBuilder();

        doImport(builder, "testImport", PROGRAM_ID, "referentials", "sampleCategory", "protocol", "default");

        if (log.isInfoEnabled()) {
            log.info("Report files:" + builder.toString());
        }

    }

    @Test
    public void testImportOnlyReferentials() throws IOException {

        dbResource.setDestroyResources(true);

        ReferentialTemporaryGearService temporaryGearService = dbResource.getServiceContext().getService(ReferentialTemporaryGearService.class);
        ReferentialTemporaryPersonService temporaryPersonService = dbResource.getServiceContext().getService(ReferentialTemporaryPersonService.class);
        ReferentialTemporarySpeciesService temporarySpeciesService = dbResource.getServiceContext().getService(ReferentialTemporarySpeciesService.class);
        ReferentialTemporaryVesselService temporaryVesselService = dbResource.getServiceContext().getService(ReferentialTemporaryVesselService.class);

        Assert.assertEquals(0, temporaryGearService.getTemporaryGears().size());
        Assert.assertEquals(0, temporaryPersonService.getTemporaryPersons().size());
        Assert.assertEquals(0, temporarySpeciesService.getTemporarySpeciess().size());
        Assert.assertEquals(0, temporaryVesselService.getTemporaryVessels().size());

        StringBuilder builder = new StringBuilder();

        doImport(builder, "testImportOnlyReferentials", PROGRAM_ID, "referentials", "sampleCategory", "empty");

        Assert.assertEquals(4, temporaryGearService.getTemporaryGears().size());
        Assert.assertEquals(3, temporaryPersonService.getTemporaryPersons().size());
        Assert.assertEquals(8, temporarySpeciesService.getTemporarySpeciess().size());
        Assert.assertEquals(4, temporaryVesselService.getTemporaryVessels().size());

        doImport(builder, "testImportOnlyReferentialsAgain", PROGRAM_ID, "referentials", "sampleCategory", "empty");

        Assert.assertEquals(4, temporaryGearService.getTemporaryGears().size());
        Assert.assertEquals(3, temporaryPersonService.getTemporaryPersons().size());
        Assert.assertEquals(8, temporarySpeciesService.getTemporarySpeciess().size());
        Assert.assertEquals(4, temporaryVesselService.getTemporaryVessels().size());

        if (log.isInfoEnabled()) {
            log.info("Report files:" + builder.toString());
        }

    }

    protected GenericFormatImportResult doImport(StringBuilder builder, String archivName, String... directoryies) throws IOException {

        File archiveFile = createArchive(archivName + ".zip", directoryies);

        GenericFormatImportConfiguration importConfiguration = new GenericFormatImportConfiguration();
        importConfiguration.setImportFile(archiveFile);
        importConfiguration.setImportSpecies(true);
        importConfiguration.setImportBenthos(true);
        importConfiguration.setImportMarineLitter(true);
        importConfiguration.setImportAccidentalCatch(true);
        importConfiguration.setImportIndividualObservation(true);
        importConfiguration.setImportAttachments(true);
        importConfiguration.setUpdateCruises(true);
        importConfiguration.setUpdateOperations(true);
        importConfiguration.setAuthorizeObsoleteReferentials(true);

        Set<CruiseDataModel> cruises = new LinkedHashSet<>();
        {

            Set<OperationDataModel> operations = new LinkedHashSet<>();
            operations.add(new OperationDataModel("2014--1--CAM-EVHOE--S0820--1--1", "S0820 - 1 - 19/10/2014"));
            operations.add(new OperationDataModel("2014--1--CAM-EVHOE--S0981--156--1", "S0820 - 156 - 30/11/2014"));
            cruises.add(new CruiseDataModel("2014--1--CAM-EVHOE", "Campagne EVOHE 2014", operations));

        }
        ProgramDataModel dataModel = new ProgramDataModel("CAM-EVHOE", "Campagne EVOHE", cruises);
        importConfiguration.setDataToExport(dataModel);

        File reportFile = getServiceDbResource().getConfig().newTempFile(archivName, ".pdf");
        importConfiguration.setReportFile(reportFile);


        ProgressionModel progressionModel = new ProgressionModel();
        progressionModel.setTotal(10000);
        GenericFormatImportResult result = service.importProgram(importConfiguration, progressionModel);

        builder.append("\n").append(importConfiguration.getImportFile().getName()).append(" :\n evince ").append(importConfiguration.getReportFile());

        return result;

    }
}

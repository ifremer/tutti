package fr.ifremer.tutti.service;

/*
 * #%L
 * Tutti :: Service
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Lists;
import fr.ifremer.tutti.LabelAware;
import fr.ifremer.tutti.TuttiConfiguration;
import fr.ifremer.tutti.persistence.entities.data.Attachment;
import fr.ifremer.tutti.persistence.entities.data.Attachments;
import fr.ifremer.tutti.persistence.entities.data.Cruise;
import fr.ifremer.tutti.persistence.entities.data.Cruises;
import fr.ifremer.tutti.persistence.entities.data.FishingOperation;
import fr.ifremer.tutti.persistence.entities.data.FishingOperations;
import fr.ifremer.tutti.persistence.entities.data.Program;
import fr.ifremer.tutti.persistence.entities.data.Programs;
import fr.ifremer.tutti.persistence.entities.protocol.SpeciesProtocol;
import fr.ifremer.tutti.persistence.entities.protocol.SpeciesProtocols;
import fr.ifremer.tutti.persistence.entities.protocol.TuttiProtocol;
import fr.ifremer.tutti.persistence.entities.protocol.TuttiProtocols;
import fr.ifremer.tutti.persistence.entities.referential.Caracteristic;
import fr.ifremer.tutti.persistence.entities.referential.CaracteristicQualitativeValue;
import fr.ifremer.tutti.persistence.entities.referential.CaracteristicQualitativeValues;
import fr.ifremer.tutti.persistence.entities.referential.Caracteristics;
import fr.ifremer.tutti.persistence.entities.referential.Gear;
import fr.ifremer.tutti.persistence.entities.referential.Gears;
import fr.ifremer.tutti.persistence.entities.referential.Person;
import fr.ifremer.tutti.persistence.entities.referential.Persons;
import fr.ifremer.tutti.persistence.entities.referential.Species;
import fr.ifremer.tutti.persistence.entities.referential.Speciess;
import fr.ifremer.tutti.persistence.entities.referential.TuttiLocation;
import fr.ifremer.tutti.persistence.entities.referential.TuttiLocations;
import fr.ifremer.tutti.persistence.entities.referential.Vessel;
import fr.ifremer.tutti.persistence.entities.referential.Vessels;
import org.apache.commons.lang3.time.DateUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.nuiton.decorator.Decorator;
import org.nuiton.decorator.DecoratorUtil;
import org.nuiton.decorator.JXPathDecorator;

import java.text.ParseException;
import java.util.List;

import static org.nuiton.i18n.I18n.t;

/**
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.3
 */
public class DecoratorServiceTest extends AbstractServiceTest {

    private DecoratorService service;

    //    @Override
//    protected TuttiServiceContext createServiceContext(RessourceClassLoader loader, TuttiConfiguration config) {
//        return new TuttiServiceContext(loader, config);
//    }
    @Override
    protected TuttiServiceContext createServiceContext(TuttiConfiguration config) {
        return new TuttiServiceContext(config);
    }

    @Before
    public void setUp() throws Exception {

        super.setUp();

        service = serviceContext.getService(DecoratorService.class);

    }

    @Test
    public void getDecoratorByType() {

        assertDecoratorByTypeFound(TuttiLocation.class);
        assertDecoratorByTypeFound(Cruise.class);
        assertDecoratorByTypeFound(TuttiProtocol.class);
        assertDecoratorByTypeFound(FishingOperation.class);
        assertDecoratorByTypeFound(Gear.class);
        assertDecoratorByTypeFound(Person.class);
        assertDecoratorByTypeFound(Caracteristic.class);
        assertDecoratorByTypeFound(DecoratorService.CARACTERISTIC_PARAMETER_ONLY, Caracteristic.class);
        assertDecoratorByTypeFound(DecoratorService.CARACTERISTIC_PARAMETER_ONLY_WITH_UNIT, Caracteristic.class);
        assertDecoratorByTypeFound(DecoratorService.CARACTERISTIC_WITH_UNIT, Caracteristic.class);
        assertDecoratorByTypeFound(SpeciesProtocol.class);
        assertDecoratorByTypeFound(CaracteristicQualitativeValue.class);
        assertDecoratorByTypeFound(Attachment.class);
        assertDecoratorByTypeFound(LabelAware.class);
        assertDecoratorByTypeFound(Vessel.class);
        assertDecoratorByTypeFound(Program.class);
        assertDecoratorByTypeFound(Float.class);
        assertDecoratorByTypeFound(DecoratorService.SPACE_EVERY_3_DIGIT, String.class);
        assertDecoratorByTypeFound(Species.class);
        assertDecoratorByTypeFound(DecoratorService.FROM_PROTOCOL, Species.class);

        assertDecoratorByTypeFound(TuttiLocations.typeOfTuttiLocation());
        assertDecoratorByTypeFound(Cruises.typeOfCruise());
        assertDecoratorByTypeFound(TuttiProtocols.typeOfTuttiProtocol());
        assertDecoratorByTypeFound(FishingOperations.typeOfFishingOperation());
        assertDecoratorByTypeFound(Gears.typeOfGear());
        assertDecoratorByTypeFound(Persons.typeOfPerson());
        assertDecoratorByTypeFound(Caracteristics.typeOfCaracteristic());
        assertDecoratorByTypeFound(DecoratorService.CARACTERISTIC_PARAMETER_ONLY, Caracteristics.typeOfCaracteristic());
        assertDecoratorByTypeFound(DecoratorService.CARACTERISTIC_PARAMETER_ONLY_WITH_UNIT, Caracteristics.typeOfCaracteristic());
        assertDecoratorByTypeFound(DecoratorService.CARACTERISTIC_WITH_UNIT, Caracteristics.typeOfCaracteristic());
        assertDecoratorByTypeFound(CaracteristicQualitativeValues.typeOfCaracteristicQualitativeValue());
        assertDecoratorByTypeFound(SpeciesProtocols.typeOfSpeciesProtocol());
        assertDecoratorByTypeFound(Attachments.typeOfAttachment());
        assertDecoratorByTypeFound(Vessels.typeOfVessel());
        assertDecoratorByTypeFound(Programs.typeOfProgram());
        assertDecoratorByTypeFound(Speciess.typeOfSpecies());
        assertDecoratorByTypeFound(DecoratorService.FROM_PROTOCOL, Speciess.typeOfSpecies());
    }

    @Test
    public void decorateProgram() {

        Decorator<Program> d = assertDecoratorByTypeFound(Program.class);
        Program p = Programs.newProgram();
        p.setName("pName");
        String s;
        s = d.toString(p);
        Assert.assertEquals("pName - " + t("tutti.propety.no.zone"), s);

        TuttiLocation z = TuttiLocations.newTuttiLocation();
        z.setLabel("pLabel");
        p.setZone(z);
        s = d.toString(p);
        Assert.assertEquals("pName - pLabel", s);

    }

    // see http://forge.codelutin.com/issues/3462
    @Test
    public void decorateFishingOperation() throws ParseException {

        JXPathDecorator<FishingOperation> d = (JXPathDecorator<FishingOperation>) assertDecoratorByTypeFound(FishingOperation.class);

        List<FishingOperation> data = Lists.newArrayList();

        FishingOperation p0 = FishingOperations.newFishingOperation();
        p0.setStationNumber("A");
        p0.setFishingOperationNumber(1);
        p0.setMultirigAggregation("1");
        p0.setGearShootingStartDate(DateUtils.parseDate("08/10/2013", "dd/MM/yyyy"));
        data.add(p0);

        FishingOperation p1 = FishingOperations.newFishingOperation();
        p1.setStationNumber("A");
        p1.setFishingOperationNumber(10);
        p1.setMultirigAggregation("1");
        p1.setGearShootingStartDate(DateUtils.parseDate("08/10/2013", "dd/MM/yyyy"));
        data.add(p1);

        FishingOperation p2 = FishingOperations.newFishingOperation();
        p2.setStationNumber("A");
        p2.setFishingOperationNumber(2);
        p2.setMultirigAggregation("1");
        p2.setGearShootingStartDate(DateUtils.parseDate("08/10/2013", "dd/MM/yyyy"));
        data.add(p2);

        DecoratorUtil.sort(d, data, 0);
        // A1 A10 A2
        Assert.assertEquals(Lists.newArrayList(p0, p1, p2), data);


        DecoratorUtil.sort(d, data, 1);
        // A1 A2 A10
        Assert.assertEquals(Lists.newArrayList(p0, p2, p1), data);
    }

    @Test
    public void decorateGear() {
        Decorator<Gear> d = assertDecoratorByTypeFound(DecoratorService.GEAR_WITH_RANK_ORDER, Gear.class);

        List<Gear> gears = Lists.newArrayList();
        Gear g0 = Gears.newGear();
        g0.setRankOrder((short) 0);
        g0.setName("A");
        g0.setLabel("Gear A");
        Assert.assertEquals("0 - Gear A - A", d.toString(g0));
        gears.add(g0);

        Gear g1 = Gears.newGear();
        g1.setRankOrder((short) 1);
        g1.setName("B");
        g1.setLabel("Gear B");
        Assert.assertEquals("1 - Gear B - B", d.toString(g1));
        gears.add(g1);

        Gear g11 = Gears.newGear();
        g11.setRankOrder((short) 11);
        g11.setName("B");
        g11.setLabel("Gear B");
        Assert.assertEquals("11 - Gear B - B", d.toString(g11));
        gears.add(g11);

        Gear g2 = Gears.newGear();
        g2.setRankOrder((short) 2);
        g2.setName("C");
        g2.setLabel("Gear C");
        Assert.assertEquals("2 - Gear C - C", d.toString(g2));
        gears.add(g2);

        DecoratorUtil.sort((JXPathDecorator<Gear>) d, gears, 0);
        Assert.assertEquals(gears, Lists.newArrayList(g0, g1, g2, g11));

        DecoratorUtil.sort((JXPathDecorator<Gear>) d, gears, 0, true);
        Assert.assertEquals(gears, Lists.newArrayList(g11, g2, g1, g0));


    }

    @Test
    public void testDecorateId() {
        String id = "123456";
        Decorator<String> idDecorator = assertDecoratorByTypeFound(DecoratorService.SPACE_EVERY_3_DIGIT, String.class);
        String decoratedId = idDecorator.toString(id);
        Assert.assertEquals("123 456", decoratedId);
    }

    protected <O> Decorator<O> assertDecoratorByTypeFound(Class<O> type) {
        return assertDecoratorByTypeFound(null, type);
    }

    protected <O> Decorator<O> assertDecoratorByTypeFound(String context, Class<O> type) {
        Decorator<O> decorator = service.getDecoratorByType(type, context);
        Assert.assertNotNull(context, decorator);
        return decorator;
    }

}

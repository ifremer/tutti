package fr.ifremer.tutti.service.pupitri;

/*
 * #%L
 * Tutti :: Service
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.entities.data.BatchContainer;
import fr.ifremer.tutti.persistence.entities.data.CatchBatch;
import fr.ifremer.tutti.persistence.entities.data.FishingOperation;
import fr.ifremer.tutti.persistence.entities.data.SpeciesBatch;
import fr.ifremer.tutti.persistence.entities.referential.Species;
import org.junit.Assert;
import org.junit.Test;
import org.nuiton.decorator.Decorator;

import java.io.File;
import java.io.IOException;
import java.util.Set;

/**
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.4
 */
public class PupitriImportServiceTest extends PupitryImportServiceTestSupport {

    @Test
    public void importPupitri() throws IOException {

        File trunk = dbResource.copyClassPathResource("pupitri/pupitri.tnk", "pupitri.tnk");
        File carroussel = dbResource.copyClassPathResource("pupitri/pupitri.car", "pupitri.car");

        {
            FishingOperation operation = dataContext.operations.get(1);
            CatchBatch catchBatch = persistenceService.getCatchBatchFromFishingOperation(operation.getIdAsInt());
            catchBatch.setFishingOperation(operation);

            BatchContainer<SpeciesBatch> rootSpeciesBatch = persistenceService.getRootSpeciesBatch(operation.getIdAsInt(), false);
            Assert.assertEquals(0, rootSpeciesBatch.sizeChildren());
            PupitriImportResult importResult = service.importPupitri(trunk, carroussel, operation, catchBatch);

            Assert.assertFalse(importResult.isFishingOperationFound());
            Assert.assertEquals(0, importResult.getNbCarrousselImported());
            Assert.assertEquals(0, importResult.getNbTrunkImported());
        }
        {
            FishingOperation operation = dataContext.operations.get(0);
            CatchBatch catchBatch = persistenceService.getCatchBatchFromFishingOperation(operation.getIdAsInt());
            catchBatch.setFishingOperation(operation);

            BatchContainer<SpeciesBatch> rootSpeciesBatch = persistenceService.getRootSpeciesBatch(operation.getIdAsInt(), false);
            Assert.assertEquals(0, rootSpeciesBatch.sizeChildren());
            
            PupitriImportResult importResult = service.importPupitri(trunk, carroussel, operation, catchBatch);
            Assert.assertTrue(importResult.isFishingOperationFound());
            Assert.assertEquals(6, importResult.getNbCarrousselNotImported());
            Set<String> notImportedSpeciesIds = importResult.getNotImportedSpeciesIds();
            Assert.assertNotNull(notImportedSpeciesIds);
            Assert.assertTrue(notImportedSpeciesIds.contains("SCYLCAN"));
            Assert.assertTrue(notImportedSpeciesIds.contains("BENTHOS"));
            Assert.assertTrue(notImportedSpeciesIds.contains("MERLMNG"));
            Assert.assertTrue(notImportedSpeciesIds.contains("DECHETS"));
            Assert.assertTrue(notImportedSpeciesIds.contains("ECHIVIP"));
            Assert.assertTrue(notImportedSpeciesIds.contains("PALASER"));

            BatchContainer<SpeciesBatch> rootSpeciesBatchAfter = persistenceService.getRootSpeciesBatch(operation.getIdAsInt(), false);
            Assert.assertEquals(18, rootSpeciesBatchAfter.sizeChildren());

            Decorator<Species> speciesDecorator = decoratorService.getDecoratorByType(Species.class);

            for (SpeciesBatch speciesBatch : rootSpeciesBatchAfter.getChildren()) {

                int nbChildren;
                // species 17392 has two childs (MALE - FEMALE)
                if (17392 == speciesBatch.getSpecies().getIdAsInt()) {
                    nbChildren = 2;
                } else {
                    nbChildren = 0;
                }
                Assert.assertEquals("SpeciesBatch with species " + speciesDecorator.toString(speciesBatch.getSpecies()) + " should have with " + nbChildren + " but had " + speciesBatch.sizeChildBatchs(), nbChildren, speciesBatch.sizeChildBatchs());
            }
        }

    }
}

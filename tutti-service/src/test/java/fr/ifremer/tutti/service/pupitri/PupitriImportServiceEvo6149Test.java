package fr.ifremer.tutti.service.pupitri;

/*
 * #%L
 * Tutti :: Service
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Predicate;
import com.google.common.collect.Sets;
import fr.ifremer.adagio.core.dao.referential.pmfm.PmfmId;
import fr.ifremer.adagio.core.dao.referential.pmfm.QualitativeValueId;
import fr.ifremer.tutti.persistence.entities.data.BatchContainer;
import fr.ifremer.tutti.persistence.entities.data.CatchBatch;
import fr.ifremer.tutti.persistence.entities.data.FishingOperation;
import fr.ifremer.tutti.persistence.entities.data.SpeciesBatch;
import fr.ifremer.tutti.persistence.entities.data.SpeciesBatchs;
import fr.ifremer.tutti.persistence.entities.referential.Species;
import org.apache.commons.io.FileUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Assert;
import org.junit.Test;
import org.nuiton.decorator.Decorator;

import java.io.File;
import java.io.IOException;
import java.util.Set;

/**
 * Created on 11/21/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.10
 */
public class PupitriImportServiceEvo6149Test extends PupitryImportServiceTestSupport {

    /** Logger. */
    private static final Log log = LogFactory.getLog(PupitriImportServiceEvo6149Test.class);

    @Test
    public void importPupitri() throws IOException {

        File trunk = dbResource.copyClassPathResource("pupitri/evo-6149.tnk", "pupitri.tnk");
        File carroussel = dbResource.copyClassPathResource("pupitri/evo-6149.car", "pupitri.car");
        dbResource.loadInternalProtocolFile("pupitri/", "evo-6149");

        FishingOperation operation = dataContext.operations.get(1);
        CatchBatch catchBatch = persistenceService.getCatchBatchFromFishingOperation(operation.getIdAsInt());
        catchBatch.setFishingOperation(operation);

        BatchContainer<SpeciesBatch> rootSpeciesBatch = persistenceService.getRootSpeciesBatch(operation.getIdAsInt(), false);
        Assert.assertEquals(0, rootSpeciesBatch.sizeChildren());

        PupitriImportResult pupitriImportResult = service.importPupitri(trunk, carroussel, operation, catchBatch);
        Assert.assertTrue(pupitriImportResult.isFishingOperationFound());
        int nbNotAdded = pupitriImportResult.getNbCarrousselNotImported();
        Assert.assertEquals(1, nbNotAdded);
        Set<String> notImportedSpeciesIds = pupitriImportResult.getNotImportedSpeciesIds();
        Assert.assertNotNull(notImportedSpeciesIds);
        Assert.assertTrue(notImportedSpeciesIds.contains("TRAC-BIZ"));

        // copy result file into temp directory to check it after the test ends
        String reportAttachmentId = pupitriImportResult.getReportAttachmentId();
        File attachmentFile = persistenceService.getAttachmentFile(reportAttachmentId);
        File destFile = new File(FileUtils.getTempDirectory(), pupitriImportResult.getReportAttachmentFilename());
        FileUtils.copyFile(attachmentFile, destFile);
        log.info("Generated report copied to " + destFile.getPath());

        BatchContainer<SpeciesBatch> rootSpeciesBatchAfter = persistenceService.getRootSpeciesBatch(operation.getIdAsInt(), false);
        Assert.assertEquals(3, rootSpeciesBatchAfter.sizeChildren());

        /*
        Melange total 100Kg / sorted 50Kg -> ratio = x2
        MERL-MCC un lot G (100Kg)
        MERL-MCC un lot P (25x2 -> 50Kg)
        TRAC-TRU un lot VRAC (25x2 -> 50Kg)
        ENGR-ENC un lot HV 75Kg
         */
        Set<Integer> expectedSpecies = Sets.newHashSet(

                16994, // MERL-MCC
                17116, // TRAC-TRU
                16816  // ENGR-ENC
        );


        Predicate<SpeciesBatch> batchVracPredicate = SpeciesBatchs.newSpeciesAbleBatchCategoryPredicate(
                PmfmId.SORTED_UNSORTED.getValue(),
                QualitativeValueId.SORTED_VRAC.getValue());

        Predicate<SpeciesBatch> batchHorsVracPredicate = SpeciesBatchs.newSpeciesAbleBatchCategoryPredicate(
                PmfmId.SORTED_UNSORTED.getValue(),
                QualitativeValueId.SORTED_HORS_VRAC.getValue());


        Predicate<SpeciesBatch> bigVracPredicate = SpeciesBatchs.newSpeciesAbleBatchCategoryPredicate(
                PmfmId.SIZE_CATEGORY.getValue(),
                QualitativeValueId.SIZE_BIG.getValue());

        Predicate<SpeciesBatch> smallVracPredicate = SpeciesBatchs.newSpeciesAbleBatchCategoryPredicate(
                PmfmId.SIZE_CATEGORY.getValue(),
                QualitativeValueId.SIZE_SMALL.getValue());

        Decorator<Species> speciesDecorator = decoratorService.getDecoratorByType(Species.class);

        for (SpeciesBatch speciesBatch : rootSpeciesBatchAfter.getChildren()) {

            Species species = speciesBatch.getSpecies();
            Integer speciesId = species.getIdAsInt();

            Assert.assertTrue("l'espece " + speciesId + " ne doit pas etre importe", expectedSpecies.contains(speciesId));

            if (log.isInfoEnabled()) {
                log.info("Species : " + speciesId + " : " + speciesDecorator.toString(species));
            }

            if (16816 == speciesId) {

                // un lot HV 75Kg
                Assert.assertTrue(batchHorsVracPredicate.apply(speciesBatch));
                Assert.assertTrue(speciesBatch.isChildBatchsEmpty());
                Assert.assertEquals(75, speciesBatch.getSampleCategoryWeight(), 0.001);
                continue;

            }
            if (16994 == speciesId) {

                // un lot VRAC (sans poids) avec deux fils
                // un G (100Kg)
                // un P (50Kg)

                Assert.assertTrue(batchVracPredicate.apply(speciesBatch));
                Assert.assertFalse(speciesBatch.isChildBatchsEmpty());
                Assert.assertEquals(2, speciesBatch.sizeChildBatchs());

                {
                    SpeciesBatch child = speciesBatch.getChildBatchs(0);

                    if (bigVracPredicate.apply(child)) {

                        Assert.assertEquals(100, child.getSampleCategoryWeight(), 0.001);

                    } else if (smallVracPredicate.apply(child)) {

                        Assert.assertEquals(50, child.getSampleCategoryWeight(), 0.001);
                    } else {
                        Assert.fail("le lot " + child + " doit etre une categorisation Taille Gros ou petit");
                    }
                }
                {
                    SpeciesBatch child = speciesBatch.getChildBatchs(1);
                    if (bigVracPredicate.apply(child)) {

                        Assert.assertEquals(100, child.getSampleCategoryWeight(), 0.001);

                    } else if (smallVracPredicate.apply(child)) {

                        Assert.assertEquals(50, child.getSampleCategoryWeight(), 0.001);
                    } else {
                        Assert.fail("le lot " + child + " doit etre une categorisation Taille Gros ou petit");
                    }
                }

                continue;
            }

            if (17116 == speciesId) {

                // un unique lot vrac de 50Kg
                Assert.assertTrue(batchVracPredicate.apply(speciesBatch));
                Assert.assertTrue(speciesBatch.isChildBatchsEmpty());
                Assert.assertEquals(50, speciesBatch.getSampleCategoryWeight(), 0.001);
            }

        }

    }
}
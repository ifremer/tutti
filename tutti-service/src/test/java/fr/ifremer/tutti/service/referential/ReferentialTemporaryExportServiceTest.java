package fr.ifremer.tutti.service.referential;

/*
 * #%L
 * Tutti :: Service
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Charsets;
import com.google.common.io.Files;
import fr.ifremer.tutti.service.ServiceDbResource;
import fr.ifremer.tutti.service.TuttiServiceContext;
import org.junit.Assert;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Ignore;
import org.junit.Test;

import java.io.File;

/**
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.0
 */
public class ReferentialTemporaryExportServiceTest {

    @ClassRule
    public static final ServiceDbResource dbResource =
            ServiceDbResource.readDb("dbCGFS");

    protected File dataDirectory;

    private static final String SPECIES_FILE_CONTENT =
            "id;name;toDelete\n" +
            ";Temporary Species name 1;\n" +
            ";Temporary Species name 2;\n" +
            ";Temporary Species name 3;";

    private static final String GEAR_FILE_CONTENT =
            "id;name;label;scientificGear;toDelete\n" +
            ";Gear fishing name 1;Gear fishing label 1;N;\n" +
            ";Gear fishing name 2;Gear fishing label 2;N;\n" +
            ";Gear scientific name 3;Gear scientific label 3;Y;\n" +
            ";Gear scientific name 4;Gear scientific label 4;Y;";

    private static final String PERSON_FILE_CONTENT =
            "id;firstName;lastName;toDelete\n" +
            ";First name 1;Last name 1;\n" +
            ";First name 2;Last name 2;\n" +
            ";First name 3;Last name 3;";

    private static final String VESSEL_FILE_CONTENT =
            "id;name;internationalRegistrationCode;scientificVessel;toDelete\n" +
            ";Temporary fishing vessel name 1;International registration code F1;N;\n" +
            ";Temporary fishing vessel name 2;International registration code F2;N;\n" +
            ";Temporary scientific vessel name 3;International registration code S3;Y;\n" +
            ";Temporary scientific vessel name 4;International registration code S4;Y;";

    @Before
    public void setUp() throws Exception {

        dataDirectory = dbResource.getConfig().getDataDirectory();

        TuttiServiceContext serviceContext = dbResource.getServiceContext();

        serviceContext.getConfig().setCsvSeparator(';');
    }


    //FIXME tchemit-2015-02-11 there is no temporary gears in the db
    @Ignore
    @Test
    public void exportTemporaryGearExample() throws Exception {

        File file = new File(dataDirectory, "exportGear.csv");

        Assert.assertFalse(file.exists());

        TuttiServiceContext serviceContext = dbResource.getServiceContext();
        ReferentialTemporaryGearService service = serviceContext.getService(ReferentialTemporaryGearService.class);

        service.exportTemporaryGearExample(file);

        Assert.assertTrue(file.exists());

        String exportFileToString = Files.toString(file, Charsets.UTF_8).trim();
        Assert.assertEquals(GEAR_FILE_CONTENT, exportFileToString);
    }

    @Test
    public void exportTemporaryPersonExample() throws Exception {

        File file = new File(dataDirectory, "exportPerson.csv");

        Assert.assertFalse(file.exists());

        TuttiServiceContext serviceContext = dbResource.getServiceContext();
        ReferentialTemporaryPersonService service = serviceContext.getService(ReferentialTemporaryPersonService.class);

        service.exportTemporaryPersonExample(file);

        Assert.assertTrue(file.exists());

        String exportFileToString = Files.toString(file, Charsets.UTF_8).trim();
        Assert.assertEquals(PERSON_FILE_CONTENT, exportFileToString);
    }

    @Test
    public void exportTemporarySpeciesExample() throws Exception {

        File file = new File(dataDirectory, "exportSpecies.csv");

        Assert.assertFalse(file.exists());

        TuttiServiceContext serviceContext = dbResource.getServiceContext();
        ReferentialTemporarySpeciesService service = serviceContext.getService(ReferentialTemporarySpeciesService.class);

        service.exportTemporarySpeciesExample(file);

        Assert.assertTrue(file.exists());

        String exportFileToString = Files.toString(file, Charsets.UTF_8).trim();
        Assert.assertEquals(SPECIES_FILE_CONTENT, exportFileToString);
    }

    @Test
    public void exportTemporaryVesselExample() throws Exception {
        File file = new File(dataDirectory, "exportVessel.csv");

        Assert.assertFalse(file.exists());

        TuttiServiceContext serviceContext = dbResource.getServiceContext();
        ReferentialTemporaryVesselService service = serviceContext.getService(ReferentialTemporaryVesselService.class);

        service.exportTemporaryVesselExample(file);

        Assert.assertTrue(file.exists());

        String exportFileToString = Files.toString(file, Charsets.UTF_8).trim();
        Assert.assertEquals(VESSEL_FILE_CONTENT, exportFileToString);
    }

}

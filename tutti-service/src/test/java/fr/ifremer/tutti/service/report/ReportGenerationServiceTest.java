package fr.ifremer.tutti.service.report;

/*
 * #%L
 * Tutti :: Service
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.TuttiConfiguration;
import fr.ifremer.tutti.persistence.ProgressionModel;
import fr.ifremer.tutti.persistence.entities.data.FishingOperation;
import fr.ifremer.tutti.persistence.entities.data.FishingOperations;
import fr.ifremer.tutti.service.ServiceDbResource;
import fr.ifremer.tutti.service.TuttiServiceContext;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Assert;
import org.junit.Assume;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Test;
import org.nuiton.jaxx.application.ApplicationIOUtil;
import org.nuiton.util.FileUtil;

import java.io.File;
import java.util.List;

/**
 * Created on 3/11/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.13.2
 */
public class ReportGenerationServiceTest {

    @ClassRule
    public static final ServiceDbResource dbResource = ServiceDbResource.noDb();

    /** Logger. */
    private static final Log log = LogFactory.getLog(ReportGenerationServiceTest.class);

    public static final String PROGRAM_ID = "CAM-MEDITS";

    public static final Integer CRUISE_ID = 100001;

    public static final Integer OPERATION_1_ID = 100106;

    protected ReportGenerationService service;

    @Before
    public void setUp() throws Exception {

        TuttiServiceContext serviceContext = dbResource.getServiceContext();

        service = serviceContext.getService(ReportGenerationService.class);

        File reportJarPath = FileUtil.getFileFromPaths(new File("target"), "tutti-report-generator", "tutti-report-generator.jar");
        Assume.assumeTrue("Could not find " + reportJarPath, reportJarPath.exists());
        TuttiConfiguration config = dbResource.getConfig();

        config.setReportJarPath(reportJarPath);

        config.getReportLogDirectory().mkdirs();

    }

    @Test
    public void testGenerateReportFromExport() throws Exception {

        // copy report resources from class-path
        File sourceDirectory = FileUtil.getFileFromPaths(new File("src"), "test", "resources", "report", "2013.12.05");
        File targetDirectory = dbResource.getConfig().getReportHomeDirectory();
        if (log.isInfoEnabled()) {
            log.info("Prepare report birt home at " + targetDirectory);
        }
        ApplicationIOUtil.copyDirectory(sourceDirectory, targetDirectory, "Could not copy report directory");

        // copy export resources from class-path
        File exportSourceDirectory = FileUtil.getFileFromPaths(new File("src"), "test", "resources", "report", "exportCruise");
        File exportTargetDirectory = dbResource.getConfig().newTempFile("exportCruise");
        if (log.isInfoEnabled()) {
            log.info("Prepare export at " + exportTargetDirectory);
        }
        ApplicationIOUtil.copyDirectory(exportSourceDirectory, exportTargetDirectory, "Could not copy report directory");

        // get reports
        List<File> availableReports = service.getAvailableReports();
        Assert.assertNotNull(availableReports);
        Assert.assertFalse(availableReports.isEmpty());
        Assert.assertEquals(1, availableReports.size());

        // get unique report file
        File reportFileName = availableReports.get(0);

        ReportGenerationRequest request = new ReportGenerationRequest();
        request.setReport(reportFileName);
        request.setCruiseId(CRUISE_ID);
        request.setFishingOperationId(OPERATION_1_ID);

        ProgressionModel progressionModel = new ProgressionModel();
        progressionModel.setTotal(3);

        FishingOperation fishingOperation = FishingOperations.newFishingOperation();
        fishingOperation.setId(OPERATION_1_ID);
        fishingOperation.setFishingOperationNumber(53);
        fishingOperation.setStationNumber("G101");

        File outputFile = service.newOutputFile();

        // report NOT must exist
        Assert.assertFalse(outputFile.exists());

        ReportGenerationContext reportContext = new ReportGenerationContext(request,
                                                                            fishingOperation,
                                                                            exportTargetDirectory,
                                                                            outputFile);

        // generate report
        ReportGenerationResult result = service.generateReport(reportContext);

        // report must exist
        Assert.assertTrue(result.getOutputFile().exists());
    }

}

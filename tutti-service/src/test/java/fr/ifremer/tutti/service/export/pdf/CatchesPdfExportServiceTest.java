package fr.ifremer.tutti.service.export.pdf;

/*
 * #%L
 * Tutti :: Service
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.service.ServiceDbResource;
import fr.ifremer.tutti.service.TuttiServiceContext;
import org.junit.Assert;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Test;

import java.io.File;
import java.util.Locale;

/**
 * Created on 11/19/13.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.0-rc-1
 */
public class CatchesPdfExportServiceTest {

    @ClassRule
    public static final ServiceDbResource dbResource =
            ServiceDbResource.readDb("dbMedits");

    public static final String PROGRAM_ID = "CAM-MEDITS";

    public static final Integer CRUISE_ID = 100001;

    public static final Integer OPERATION_1_ID = 100106;

    protected CatchesPdfExportService exportService;

    protected File dataDirectory;

    @Before
    public void setUp() throws Exception {

        dataDirectory = dbResource.getConfig().getDataDirectory();

        TuttiServiceContext serviceContext = dbResource.getServiceContext();

        dbResource.openDataContext();

        exportService = serviceContext.getService(CatchesPdfExportService.class);
    }

    @Test
    public void generateFishingOperationPDFFile() throws Exception {

        File exportFile = new File(dataDirectory, "exportOperation.pdf");

        Assert.assertFalse(exportFile.exists());

        exportService.generateFishingOperationPDFFile(exportFile,
                                                      OPERATION_1_ID,
                                                      Locale.FRENCH);

        Assert.assertTrue(exportFile.exists());
    }
}

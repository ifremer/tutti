package fr.ifremer.tutti.service.catches.multipost;

/*
 * #%L
 * Tutti :: Service
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.io.Files;
import fr.ifremer.tutti.service.ServiceDbResource;
import fr.ifremer.tutti.service.TuttiServiceContext;
import org.apache.commons.lang3.tuple.Pair;
import org.junit.Assert;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Test;

import java.io.File;

/**
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.4
 * //FIXME Add more data in db
 */
public class MultiPostExportServiceTest {

    @ClassRule
    public static final ServiceDbResource dbResource =
            ServiceDbResource.readDb("dbCGFS");

    public static final String PROGRAM_ID = "CAM-CGFS";

    public static final Integer CRUISE_ID = 100001;

    public static final Integer OPERATION_1_ID = 100106;

    public static final Integer OPERATION_2_ID = 100107;

    public static final String SPECIES_CONTENT = "id;parentId;species;categoryId;categoryValue;categoryWeight;weight;number;comment;toConfirm\n" +
                                                 "CatchRow_1;;11242;1428;311;100.0;;;;N\n" +
                                                 "CatchRow_2;CatchRow_1;11242;198;305;80.0;;;;N\n" +
                                                 "CatchRow_3;CatchRow_2;11242;196;300;30.0;;;;N\n" +
                                                 "CatchRow_4;CatchRow_3;11242;174;272;10.0;5.0;;;N\n" +
                                                 "CatchRow_5;CatchRow_3;11242;174;274;10.0;;;;N\n" +
                                                 "CatchRow_6;CatchRow_2;11242;196;301;50.0;30.0;;;N\n" +
                                                 "CatchRow_7;CatchRow_1;11242;198;306;20.0;;;;N\n" +
                                                 "CatchRow_8;;11242;1428;310;20.0;;2;;N";

    public static final String SPECIES_FREQUENCIES_CONTENT = "batchId;lengthStepCaracteristic;lengthStep;number;weight\n" +
                                                             "CatchRow_4;307;10.0;5;\n" +
                                                             "CatchRow_4;307;10.5;2;\n" +
                                                             "CatchRow_4;307;11.0;1;\n" +
                                                             "CatchRow_5;307;11.0;5;\n" +
                                                             "CatchRow_6;1425;10.0;5;\n" +
                                                             "CatchRow_6;1425;11.0;6;\n" +
                                                             "CatchRow_6;1425;12.0;7;";

    public static final String SPECIES_WEIGHTS_CONTENT = "stationNumber;operationNumber;multirigAggregation;date;totalSortedWeight;inertWeight;livingNotItemizedWeight\n" +
                                                         "A;1;1;01/05/2013 00:00;;;";

    public static final String SPECIES_ATTACHMENT_CONTENT = "batchId;name;comment;file";


    public static final String BENTHOS_CONTENT = "id;parentId;species;categoryId;categoryValue;categoryWeight;weight;number;comment;toConfirm";

    public static final String BENTHOS_FREQUENCIES_CONTENT = "batchId;lengthStepCaracteristic;lengthStep;number;weight";

    public static final String BENTHOS_WEIGHTS_CONTENT = "stationNumber;operationNumber;multirigAggregation;date;totalSortedWeight;inertWeight;livingNotItemizedWeight\n" +
                                                         "A;1;1;01/05/2013 00:00;;;";

    public static final String BENTHOS_ATTACHMENT_CONTENT = "batchId;name;comment;file";

    public static final String MARINE_LITTER_CONTENT = "batchId;category;sizeCategory;number;weight;comment\n" +
                                                       "MarineLitterRow_1;2119;2120;2;5.0;S1\n" +
                                                       "MarineLitterRow_2;2126;2121;3;1.0;S2";

    public static final String MARINE_LITTER_WEIGHTS_CONTENT = "stationNumber;operationNumber;multirigAggregation;date;totalWeight\n" +
                                                               "A;1;1;01/05/2013 00:00;";

    public static final String ACCIDENTAL_CATCH_CONTENT = "batchId;species;gender;weight;size;lengthStepCaracteristic;deadOrAlive;comment\n" +
                                                          "AccidentalCatchRow_1;3835;301;10.0;4.0;1425;1769;";

    public static final String ACCIDENTAL_CATCH_WEIGHTS_CONTENT = "stationNumber;operationNumber;multirigAggregation;date\n" +
                                                                  "A;1;1;01/05/2013 00:00";

    public static final String ACCIDENTAL_CATCH_CARACTERISTIC_CONTENT = "batchId;caracteristic;value";

    public static final String ACCIDENTAL_CATCH_ATTACHMENT_CONTENT = "batchId;name;comment;file";

    public static final String INDIVIDUAL_OBSERVATION_CONTENT = "batchId;species;weight;size;lengthStepCaracteristic;comment\n" +
                                                                "IndividualObservationRow_1;11242;0.1;10.0;307;P1";

    public static final String INDIVIDUAL_OBSERVATION_CARACTERISTIC_CONTENT = "batchId;caracteristic;value\n" +
                                                                              "IndividualObservationRow_1;1436;10\n" +
                                                                              "IndividualObservationRow_1;1435;A20\n" +
                                                                              "IndividualObservationRow_1;101;10.0\n" +
                                                                              "IndividualObservationRow_1;46;168\n" +
                                                                              "IndividualObservationRow_1;1388;5.0";

    public static final String INDIVIDUAL_OBSERVATION_WEIGHTS_CONTENT = "stationNumber;operationNumber;multirigAggregation;date\n" +
                                                                        "A;1;1;01/05/2013 00:00";

    public static final String MARINE_LITTER_ATTACHMENT_CONTENT = "batchId;name;comment;file";

    public static final String INDIVIDUAL_OBSERVATION_ATTACHMENT_CONTENT = "batchId;name;comment;file";

    protected MultiPostExportService service;

    protected ServiceDbResource.DataContext dataContext;

    protected File dataDirectory;

    @Before
    public void setUp() throws Exception {

        dataDirectory = dbResource.getConfig().getDataDirectory();

        TuttiServiceContext serviceContext = dbResource.getServiceContext();

        dbResource.openDataContext();

        service = serviceContext.getService(MultiPostExportService.class);

        dataContext = dbResource.loadContext(PROGRAM_ID,
                                             CRUISE_ID,
                                             2,
                                             OPERATION_1_ID,
                                             OPERATION_2_ID);
    }

    @Test
    public void testExportSpecies() throws Exception {

        File exportFile = new File(dataDirectory,
                                   "exportSpecies.zip");

        Files.createParentDirs(exportFile);

        Assert.assertFalse(exportFile.exists());

        service.exportSpecies(exportFile, dataContext.operations.get(0), true, false);
        Assert.assertTrue(exportFile.exists());

        dbResource.assertZipContent(
                "Species export",
                exportFile,
                Pair.of(MultiPostExportService.FREQUENCIES_FILE, SPECIES_FREQUENCIES_CONTENT),
                Pair.of(MultiPostExportService.SPECIES_FILE, SPECIES_CONTENT),
                Pair.of(MultiPostExportService.WEIGHTS_FILE, SPECIES_WEIGHTS_CONTENT),
                Pair.of(MultiPostExportService.ATTACHMENTS_FILE, SPECIES_ATTACHMENT_CONTENT)
        );

    }

    @Test
    public void testExportBenthos() throws Exception {
        File exportFile = new File(dataDirectory,
                                   "exportBenthos.zip");

        Files.createParentDirs(exportFile);

        Assert.assertFalse(exportFile.exists());

        service.exportBenthos(exportFile, dataContext.operations.get(0), true, true);
        Assert.assertTrue(exportFile.exists());

        dbResource.assertZipContent(
                "Benthos export",
                exportFile,
                Pair.of(MultiPostExportService.FREQUENCIES_FILE, BENTHOS_FREQUENCIES_CONTENT),
                Pair.of(MultiPostExportService.BENTHOS_FILE, BENTHOS_CONTENT),
                Pair.of(MultiPostExportService.WEIGHTS_FILE, BENTHOS_WEIGHTS_CONTENT),
                Pair.of(MultiPostExportService.ATTACHMENTS_FILE, BENTHOS_ATTACHMENT_CONTENT)

        );
    }

    @Test
    public void testExportMarineLitter() throws Exception {
        File exportFile = new File(dataDirectory,
                                   "exportMarineLitter.zip");

        Files.createParentDirs(exportFile);

        Assert.assertFalse(exportFile.exists());

        service.exportMarineLitter(exportFile, dataContext.operations.get(0));
        Assert.assertTrue(exportFile.exists());

        dbResource.assertZipContent(
                "MarineLitter export",
                exportFile,
                Pair.of(MultiPostExportService.MARINE_LITTER_FILE, MARINE_LITTER_CONTENT),
                Pair.of(MultiPostExportService.WEIGHTS_FILE, MARINE_LITTER_WEIGHTS_CONTENT),
                Pair.of(MultiPostExportService.ATTACHMENTS_FILE, MARINE_LITTER_ATTACHMENT_CONTENT)
        );
    }

//    @Test
//    public void testExportIndividualObservation() throws Exception {
//        File exportFile = new File(dataDirectory,
//                                   "exportIndividualObservation.zip");
//
//        Files.createParentDirs(exportFile);
//
//        Assert.assertFalse(exportFile.exists());
//
//        service.exportIndividualObservation(exportFile, dataContext.operations.get(0));
//        Assert.assertTrue(exportFile.exists());
//
//        dbResource.assertZipContent(
//                "IndividualObservation export",
//                exportFile,
//                Pair.of(MultiPostExportService.INDIVIDUAL_OBSERVATION_FILE, INDIVIDUAL_OBSERVATION_CONTENT),
//                Pair.of(MultiPostExportService.CARACTERISTIC_FILE, INDIVIDUAL_OBSERVATION_CARACTERISTIC_CONTENT),
//                Pair.of(MultiPostExportService.WEIGHTS_FILE, INDIVIDUAL_OBSERVATION_WEIGHTS_CONTENT),
//                Pair.of(MultiPostExportService.ATTACHMENTS_FILE, INDIVIDUAL_OBSERVATION_ATTACHMENT_CONTENT)
//        );
//    }

    @Test
    public void testExportAccidentalCatch() throws Exception {
        File exportFile = new File(dataDirectory,
                                   "exportAccidentalCatch.zip");

        Files.createParentDirs(exportFile);

        Assert.assertFalse(exportFile.exists());

        service.exportAccidentalCatch(exportFile, dataContext.operations.get(0));
        Assert.assertTrue(exportFile.exists());

        dbResource.assertZipContent(
                "AccidentalCatch export",
                exportFile,
                Pair.of(MultiPostExportService.ACCIDENTAL_CATCHES_FILE, ACCIDENTAL_CATCH_CONTENT),
                Pair.of(MultiPostExportService.WEIGHTS_FILE, ACCIDENTAL_CATCH_WEIGHTS_CONTENT),
                Pair.of(MultiPostExportService.CARACTERISTIC_FILE, ACCIDENTAL_CATCH_CARACTERISTIC_CONTENT),
                Pair.of(MultiPostExportService.ATTACHMENTS_FILE, ACCIDENTAL_CATCH_ATTACHMENT_CONTENT)
        );
    }
}

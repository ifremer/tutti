package fr.ifremer.tutti.service;

/*
 * #%L
 * Tutti :: Service
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.TuttiConfiguration;
import fr.ifremer.tutti.TuttiConfigurationOption;
import org.apache.commons.io.FileUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.rules.TestName;
import org.nuiton.config.ApplicationConfig;
import org.nuiton.util.FileUtil;
import org.nuiton.converter.ConverterUtil;

import java.io.File;

/**
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.2
 */
public abstract class AbstractServiceTest {

    public static final long TIMESTAMP = System.nanoTime();

    @Rule
    public final TestName name = new TestName();

    protected File datadirectory;

    protected TuttiServiceContext serviceContext;

//    protected abstract TuttiServiceContext createServiceContext(RessourceClassLoader loader,
//                                                                TuttiConfiguration config);

    protected abstract TuttiServiceContext createServiceContext(TuttiConfiguration config);

    @Before
    public void setUp() throws Exception {

        datadirectory = FileUtil.getTestSpecificDirectory(getClass(),
                                                          name.getMethodName(),
                                                          null,
                                                          TIMESTAMP);

        FileUtils.forceMkdir(datadirectory);

        ApplicationConfig applicationConfig =
                new ApplicationConfig("tutti-test.properties");
        applicationConfig.loadDefaultOptions(
                TuttiConfigurationOption.values());
        applicationConfig.setDefaultOption(TuttiConfigurationOption.BASEDIR.getKey(),
                                           datadirectory.getAbsolutePath());
        applicationConfig.parse();

//        RessourceClassLoader loader = new RessourceClassLoader(getClass().getClassLoader());

        TuttiConfiguration config = new TuttiConfiguration(applicationConfig);

//        serviceContext = createServiceContext(loader, config);
        serviceContext = createServiceContext(config);

        ConverterUtil.deregister();
        ConverterUtil.initConverters();
    }

    @After
    public void tearDown() throws Exception {

        serviceContext.close();
    }
}

package fr.ifremer.tutti.service.pupitri;

/*
 * #%L
 * Tutti :: Service
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.service.DecoratorService;
import fr.ifremer.tutti.service.PersistenceService;
import fr.ifremer.tutti.service.ServiceDbResource;
import fr.ifremer.tutti.service.TuttiServiceContext;
import org.junit.Before;
import org.junit.ClassRule;

/**
 * Created on 11/21/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.10
 */
public abstract class PupitryImportServiceTestSupport {

    public static final String PROGRAM_ID = "CAM-CGFS";

    public static final Integer CRUISE_ID = 100000;

    public static final Integer OPERATION_1_ID = 100000;

    public static final Integer OPERATION_2_ID = 100001;

    @ClassRule
    public static final ServiceDbResource dbResource = ServiceDbResource.writeDb("dbPupitri");

    protected PupitriImportService service;

    protected PersistenceService persistenceService;

    protected DecoratorService decoratorService;

    protected ServiceDbResource.DataContext dataContext;

    @Before
    public void setUp() throws Exception {

        TuttiServiceContext serviceContext = dbResource.getServiceContext();

        persistenceService = serviceContext.getService(PersistenceService.class);

        decoratorService = serviceContext.getService(DecoratorService.class);

        dbResource.openDataContext();

        service = serviceContext.getService(PupitriImportService.class);

        dataContext = dbResource.loadContext(PupitryImportServiceTestSupport.PROGRAM_ID, PupitryImportServiceTestSupport.CRUISE_ID, 2, PupitryImportServiceTestSupport.OPERATION_2_ID, PupitryImportServiceTestSupport.OPERATION_1_ID);
    }

}

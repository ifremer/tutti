package fr.ifremer.tutti.service.catches.multipost;

/*
 * #%L
 * Tutti :: Service
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.entities.data.CatchBatch;
import fr.ifremer.tutti.persistence.entities.data.CatchBatchs;
import fr.ifremer.tutti.persistence.entities.data.FishingOperation;
import fr.ifremer.tutti.persistence.entities.data.FishingOperations;
import fr.ifremer.tutti.service.PersistenceService;
import fr.ifremer.tutti.service.ServiceDbResource;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Ignore;
import org.junit.Test;
import org.nuiton.jaxx.application.ApplicationIOUtil;
import org.nuiton.util.DateUtil;
import org.nuiton.util.FileUtil;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import static org.nuiton.i18n.I18n.n;

/**
 * Created on 9/29/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.8
 */
// This was more an IT than a UT
@Ignore
public class MultiPostImportServiceTest {

    /** Logger. */
    private static final Log log = LogFactory.getLog(MultiPostImportServiceTest.class);

    @ClassRule
    public static final ServiceDbResource dbResource = ServiceDbResource.writeDb("dbPerformance");

    protected File dataDirectory;

    protected ServiceDbResource.DataContext dataContext;

    protected File speciesFile;

    protected File benthosFile;

    protected File marineLitterFile;

    @Before
    public void setUp() throws Exception {

        dataDirectory = dbResource.getConfig().getDataDirectory();

        dbResource.openDataContext();

        dataContext = dbResource.loadContext("CAM-MEDITS", 100000, 0);

        speciesFile = FileUtil.getFileFromPaths(new File("src"), "test", "data", dbResource.getDbName(), "species");
        benthosFile = FileUtil.getFileFromPaths(new File("src"), "test", "data", dbResource.getDbName(), "benthos");
        marineLitterFile = FileUtil.getFileFromPaths(new File("src"), "test", "data", dbResource.getDbName(), "marineLitter");

    }

    DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

    @Test
    public void create100FishingOperations() throws IOException {

        MultiPostImportService multiPostImportService = dbResource.getServiceContext().getService(MultiPostImportService.class);
        PersistenceService persistenceService = dbResource.getServiceContext().getService(PersistenceService.class);

        for (int operationNumber = 0; operationNumber < 100; operationNumber++) {

            // create a fishing operation

            FishingOperation fishingOperation = FishingOperations.newFishingOperation();

            fishingOperation.setCruise(dataContext.cruise);

            String stationNumber = "MEDITS";
            Date date = DateUtil.createDate(1 + operationNumber, 10, 2014);

            if (log.isInfoEnabled()) {
                log.info("Create fishingOperation " + operationNumber);
            }

            fishingOperation.setStationNumber(stationNumber);
            fishingOperation.setFishingOperationNumber(operationNumber);
            fishingOperation.setMultirigAggregation("1");
            fishingOperation.setGearShootingStartDate(date);
            fishingOperation.setGearShootingEndDate(date);

            fishingOperation = persistenceService.createFishingOperation(fishingOperation);

            CatchBatch catchBatch = CatchBatchs.newCatchBatch();
            catchBatch.setFishingOperation(fishingOperation);
            persistenceService.createCatchBatch(catchBatch);

            // create import files

            File directory = dbResource.getServiceContext().getConfig().newTempFile("dbPerformance" + operationNumber);

            String operationDate = df.format(date);

            String operationNumberAsString = String.valueOf(operationNumber);

            {
                if (log.isInfoEnabled()) {
                    log.info("Create species multiImport " + operationNumber);
                }

                File speciesImportFile = copyImport(speciesFile, directory, "species", stationNumber, operationNumberAsString, operationDate);

                if (log.isInfoEnabled()) {
                    log.info("Import multiImport " + speciesImportFile);
                }

                multiPostImportService.importSpecies(speciesImportFile, fishingOperation, true, false);

            }

            {
                if (log.isInfoEnabled()) {
                    log.info("Create benthos multiImport " + operationNumber);
                }
                File benthosImportFile = copyImport(benthosFile, directory, "benthos", stationNumber, operationNumberAsString, operationDate);

                if (log.isInfoEnabled()) {
                    log.info("Import multiImport " + benthosImportFile);
                }

                multiPostImportService.importBenthos(benthosImportFile, fishingOperation, true, false);

            }

            {
                if (log.isInfoEnabled()) {
                    log.info("Create marineLitter multiImport " + operationNumber);
                }
                File marineLitterImportFile = copyImport(marineLitterFile, directory, "marineLitter", stationNumber, operationNumberAsString, operationDate);

                if (log.isInfoEnabled()) {
                    log.info("Import multiImport " + marineLitterImportFile);
                }

                multiPostImportService.importMarineLitter(marineLitterImportFile, fishingOperation);

            }

            if (log.isInfoEnabled()) {
                log.info("Import multiImport " + operationNumber);
            }

        }

    }

    protected File copyImport(File incomingDirectory,
                              File directory,
                              String type,
                              String stationNumber,
                              String operationNumber,
                              String operationDate) throws IOException {

        FileUtils.forceMkdir(directory);
        FileUtils.copyDirectoryToDirectory(incomingDirectory, directory);

        File targetDirectory = new File(directory, type);

        File weightsFile = new File(targetDirectory, "weights.csv");
        String weights = FileUtils.readFileToString(weightsFile)
                .replace("$STATIONNUMBER$", stationNumber)
                .replace("$OPERATIONNUMBER$", operationNumber)
                .replace("$DATE$", operationDate);

        FileUtils.write(weightsFile, weights);

        File file = new File(directory, type + ".tutti" + StringUtils.capitalize(type));

        List<File> files = new ArrayList<>();
        files.addAll(Arrays.asList(targetDirectory.listFiles()));

        ApplicationIOUtil.zip(targetDirectory,
                              file,
                              files,
                              n("tutti.service.multipost.export.error"));

        return file;

    }

}

package fr.ifremer.tutti.service.genericformat;

/*
 * #%L
 * Tutti :: Service
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.io.Files;
import fr.ifremer.tutti.persistence.ProgressionModel;
import fr.ifremer.tutti.persistence.entities.data.SampleCategoryModel;
import fr.ifremer.tutti.persistence.entities.data.SampleCategoryModelEntry;
import fr.ifremer.tutti.persistence.model.ProgramDataModel;
import fr.ifremer.tutti.service.PersistenceService;
import fr.ifremer.tutti.service.ServiceDbResource;
import fr.ifremer.tutti.service.TuttiServiceContext;
import org.junit.Assert;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Test;

import java.io.File;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

/**
 * Created on 9/22/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 4.5
 */
public class GenericFormatExportServiceAno7992Test {

    @ClassRule
    public static final ServiceDbResource dbResource = ServiceDbResource.readDb("dbAno7992");

    public static final String OPERATION_FILE_CONTENT
            = "Annee;Serie;Serie_Partielle;Code_Station;Id_Operation;Poche;Engin;Rang_Engin;Navire;DateDeb;LatDeb;LongDeb;DateFin;LatFin;LongFin;Duree;Strate;Sous_Strate;Localite;Validite_OP;Rectiligne;Distance;Saisisseur;Navire_Associe;Commentaire;Poids_Total;Poids_Total_Calcule;Poids_Total_Vrac;Poids_Total_Vrac_Calcule;Poids_Total_HorsVrac;Poids_Total_HorsVrac_Calcule;Poids_Total_Non_Trie;Poids_Total_Non_Trie_Calcule;Poids_Total_Tremis;Poids_Total_Tremis_Calcule;Poids_Total_Carroussel;Poids_Total_Carroussel_Calcule;Poids_Total_Espece;Poids_Total_Espece_Calcule;Poids_Total_Espece_Vrac;Poids_Total_Espece_Vrac_Calcule;Poids_Total_Espece_Vrac_Trie;Poids_Total_Espece_Vrac_Trie_Calcule;Poids_Total_Espece_HorsVrac;Poids_Total_Espece_HorsVrac_Calcule;Poids_Total_Espece_Inerte_Trie;Poids_Total_Espece_Inerte_Trie_Calcule;Poids_Total_Espece_Vivant_non_detaille_trie;Poids_Total_Espece_Vivant_non_detaille_trie_Calcule;Poids_Total_Benthos;Poids_Total_Benthos_Calcule;Poids_Total_Benthos_Vrac;Poids_Total_Benthos_Vrac_Calcule;Poids_Total_Benthos_Vrac_Trie;Poids_Total_Benthos_Vrac_Trie_Calcule;Poids_Total_Benthos_HorsVrac;Poids_Total_Benthos_HorsVrac_Calcule;Poids_Total_Benthos_Inerte_Trie;Poids_Total_Benthos_Inerte_Trie_Calcule;Poids_Total_Benthos_Vivant_non_detaille_trie;Poids_Total_Benthos_Vivant_non_detaille_trie_Calcule;Poids_Total_Macro_Dechet;Poids_Total_Macro_Dechet_Calcule;Serie_Id;Engin_Id;Fishing_Operation_Id;Catch_Lot_Id;Navire_Id;Strate_Id;Sous_Strate_Id;Localite_Id;Saisisseur_Id;Navire_Associe_Id\n" +
            "2015;Campagne EVHOE;;A;1;1;GOV 11.90/16.45;1;590117;03/11/2015 00:00:00;;;03/11/2015 00:00:00;;;00;NA;NA;NA;Y;N;-9;;;;6.4;Y;6.4;Y;0.0;Y;0.0;Y;-9.0;N;-9.0;N;6.4;Y;6.4;N;3.2;Y;0.0;Y;0.0;Y;0.0;Y;-9.0;?;-9.0;?;-9.0;?;-9.0;?;-9.0;?;-9.0;?;-9.0;?;CAM-EVHOE;386;100000;100000;590117;;;;;\n" +
            "2015;Campagne EVHOE;;A;2;1;GOV 11.90/16.45;1;590117;23/02/2016 00:00:00;;;23/02/2016 00:00:00;;;00;NA;NA;NA;Y;N;-9;;;;200.0;Y;200.0;Y;0.0;Y;0.0;Y;-9.0;N;-9.0;N;200.0;Y;200.0;N;100.0;Y;0.0;Y;0.0;Y;0.0;Y;-9.0;?;-9.0;?;-9.0;?;-9.0;?;-9.0;?;-9.0;?;-9.0;?;CAM-EVHOE;386;100001;100016;590117;;;;;\n" +
            "2015;Campagne EVHOE;;A;3;1;GOV 11.90/16.45;1;590117;24/02/2016 00:00:00;;;24/02/2016 00:00:00;;;00;NA;NA;NA;Y;N;-9;;;;200.0;Y;200.0;Y;0.0;Y;0.0;Y;-9.0;N;-9.0;N;200.0;Y;200.0;N;100.0;Y;0.0;Y;0.0;Y;0.0;Y;-9.0;?;-9.0;?;-9.0;?;-9.0;?;-9.0;?;-9.0;?;-9.0;?;CAM-EVHOE;386;100002;100029;590117;;;;;\n";

    public static final String CATCH_FILE_CONTENT
            = "Annee;Serie;Serie_Partielle;Code_Station;Id_Operation;Poche;Code_Taxon;Code_Espece_Campagne;Nom_Scientifique;Benthos;Lot_A_Confirmer;V_HV;Num_Ordre_V_HV_H2;Tot_V_HV;Ech_V_HV;Type_Volume_Poids_V_HV;Unite_Volume_Poids_V_HV;Commentaire_V_HV;Class_Tri;Num_Ordre_Class_Tri_H2;Tot_Class_Tri;Ech_Class_Tri;Type_Volume_Poids_Class_Tri;Unite_Volume_Poids_Class_Tri;Commentaire_Class_Tri;Sexe;Num_Ordre_Sexe_H2;Tot_Sexe;Ech_Sexe;Type_Volume_Poids_Sexe;Unite_Volume_Poids_Sexe;Commentaire_Sexe;Etat;Num_Ordre_Etat_H2;Tot_Etat;Ech_Etat;Type_Volume_Poids_Etat;Unite_Volume_Poids_Etat;Commentaire_Etat;Age;Num_Ordre_Age_H2;Tot_Age;Ech_Age;Type_Volume_Poids_Age;Unite_Volume_Poids_Age;Commentaire_Age;etat_sante;Num_Ordre_etat_sante_H2;Tot_etat_sante;Ech_etat_sante;Type_Volume_Poids_etat_sante;Unite_Volume_Poids_etat_sante;Commentaire_etat_sante;age_coquille;Num_Ordre_age_coquille_H2;Tot_age_coquille;Ech_age_coquille;Type_Volume_Poids_age_coquille;Unite_Volume_Poids_age_coquille;Commentaire_age_coquille;age2;Num_Ordre_age2_H2;Tot_age2;Ech_age2;Type_Volume_Poids_age2;Unite_Volume_Poids_age2;Commentaire_age2;Code_Longueur;Libelle_Longueur;Taille;NumOrdre_Taille_H2;Poids_Classe_Taille;Unite_Taille;Precision_Mesure;Nbr;Nbr_Calcule;Poids_Reference;Coef_Elev_Espece_Capture;Coef_Final_Elevation;Serie_Id;V_HV_Id;V_HV_Lot_Id;Class_Tri_Id;Class_Tri_Lot_Id;Sexe_Id;Sexe_Lot_Id;Etat_Id;Etat_Lot_Id;Age_Id;Age_Lot_Id;etat_sante_Id;etat_sante_Lot_Id;age_coquille_Id;age_coquille_Lot_Id;age2_Id;age2_Lot_Id\n" +
            "2015;Campagne EVHOE;;A;1;1;949;NEPH-NOR;Nephrops norvegicus;N;N;Vrac;1;3.2;;Poids;kg;;NA;;;;;kg;;Male;1;2.54;;Poids;kg;;NA;;;;;kg;;NA;;;;;kg;;NA;;;;;kg;;NA;;;;;kg;;NA;;;;;kg;;;;;;;;;;?;2.54;2.519685;2.3188405;CAM-EVHOE;311;100004;NA;;300;100005;NA;;NA;;NA;;NA;;NA;\n" +
            "2015;Campagne EVHOE;;A;1;1;949;NEPH-NOR;Nephrops norvegicus;N;N;Vrac;1;3.2;;Poids;kg;;NA;;;;;kg;;Femelle;2;0.22;;Poids;kg;;NA;;;;;kg;;NA;;;;;kg;;NA;;;;;kg;;NA;;;;;kg;;NA;;;;;kg;;;;;;;;;;?;0.22;29.09091;2.3188405;CAM-EVHOE;311;100004;NA;;301;100006;NA;;NA;;NA;;NA;;NA;\n" +
            "2015;Campagne EVHOE;;A;2;1;1662;TRAC-TRA;Trachurus trachurus;N;N;Vrac;1;100.0;;Poids;kg;;G - Gros;1;30.0;;Poids;kg;;Male;1;10.0;;Poids;kg;;NA;;;;;kg;;NA;;;;;kg;;NA;;;;;kg;;NA;;;;;kg;;NA;;;;;kg;;41;Poids déterminé par calcul - produit/lot - totale - Inconnue;10.0;1;2.0;kg;;1;N;2.0;100.0;40.0;CAM-EVHOE;311;100020;305;100021;300;100023;NA;;NA;;NA;;NA;;NA;\n" +
            "2015;Campagne EVHOE;;A;2;1;1662;TRAC-TRA;Trachurus trachurus;N;N;Vrac;1;100.0;;Poids;kg;;G - Gros;1;30.0;;Poids;kg;;Male;1;10.0;;Poids;kg;;NA;;;;;kg;;NA;;;;;kg;;NA;;;;;kg;;NA;;;;;kg;;NA;;;;;kg;;41;Poids déterminé par calcul - produit/lot - totale - Inconnue;11.0;2;3.0;kg;;1;N;3.0;66.666664;26.666666;CAM-EVHOE;311;100020;305;100021;300;100023;NA;;NA;;NA;;NA;;NA;\n" +
            "2015;Campagne EVHOE;;A;2;1;1662;TRAC-TRA;Trachurus trachurus;N;N;Vrac;1;100.0;;Poids;kg;;G - Gros;1;30.0;;Poids;kg;;Femelle;2;5.0;;Poids;kg;;NA;;;;;kg;;NA;;;;;kg;;NA;;;;;kg;;NA;;;;;kg;;NA;;;;;kg;;;;;;;;;;?;5.0;40.0;8.0;CAM-EVHOE;311;100020;305;100021;301;100024;NA;;NA;;NA;;NA;;NA;\n" +
            "2015;Campagne EVHOE;;A;2;1;1662;TRAC-TRA;Trachurus trachurus;N;N;Vrac;1;100.0;;Poids;kg;;P - Petit;2;20.0;10.0;Poids;kg;;NA;;;;;kg;;NA;;;;;kg;;NA;;;;;kg;;NA;;;;;kg;;NA;;;;;kg;;NA;;;;;kg;;;;;;;;;;?;10.0;20.0;8.0;CAM-EVHOE;311;100020;307;100022;NA;;NA;;NA;;NA;;NA;;NA;\n" +
            "2015;Campagne EVHOE;;A;3;1;949;NEPH-NOR;Nephrops norvegicus;N;N;Vrac;1;100.0;;Poids;kg;;G - Gros;1;20.0;;Poids;kg;;Male;1;5.0;;Poids;kg;;NA;;;;;kg;;NA;;;;;kg;;NA;;;;;kg;;NA;;;;;kg;;NA;;;;;kg;;;;;;;;;;?;5.0;40.0;4.0;CAM-EVHOE;311;100033;305;100034;300;100036;NA;;NA;;NA;;NA;;NA;\n" +
            "2015;Campagne EVHOE;;A;3;1;949;NEPH-NOR;Nephrops norvegicus;N;N;Vrac;1;100.0;;Poids;kg;;G - Gros;1;20.0;;Poids;kg;;Femelle;2;15.0;;Poids;kg;;Floches;1;5.0;;Poids;kg;;NA;;;;;kg;;DEAD - Mort;1;1.0;0.5;Poids;kg;;NA;;;;;kg;;NA;;;;;kg;;;;;;;;;;?;0.5;400.0;32.0;CAM-EVHOE;311;100033;305;100034;301;100037;2496;100038;NA;;2420;100042;NA;;NA;\n" +
            "2015;Campagne EVHOE;;A;3;1;949;NEPH-NOR;Nephrops norvegicus;N;N;Vrac;1;100.0;;Poids;kg;;G - Gros;1;20.0;;Poids;kg;;Femelle;2;15.0;;Poids;kg;;Floches;1;5.0;;Poids;kg;;NA;;;;;kg;;ALIVE - Vivant;2;1.5;0.75;Poids;kg;;NA;;;;;kg;;NA;;;;;kg;;;;;;;;;;?;0.75;266.66666;32.0;CAM-EVHOE;311;100033;305;100034;301;100037;2496;100038;NA;;2421;100043;NA;;NA;\n" +
            "2015;Campagne EVHOE;;A;3;1;949;NEPH-NOR;Nephrops norvegicus;N;N;Vrac;1;100.0;;Poids;kg;;G - Gros;1;20.0;;Poids;kg;;Femelle;2;15.0;;Poids;kg;;Cassees;2;2.5;;Poids;kg;;NA;;;;;kg;;NA;;;;;kg;;NA;;;;;kg;;NA;;;;;kg;;;;;;;;;;?;2.5;80.0;8.0;CAM-EVHOE;311;100033;305;100034;301;100037;2497;100039;NA;;NA;;NA;;NA;\n" +
            "2015;Campagne EVHOE;;A;3;1;949;NEPH-NOR;Nephrops norvegicus;N;N;Vrac;1;100.0;;Poids;kg;;P - Petit;2;30.0;;Poids;kg;;NA;;;;;kg;;NA;;;;;kg;;NA;;;;;kg;;NA;;;;;kg;;NA;;;;;kg;;NA;;;;;kg;;;;;;;;;;?;30.0;6.6666665;4.0;CAM-EVHOE;311;100033;307;100035;NA;;NA;;NA;;NA;;NA;;NA;\n";

    protected GenericFormatExportService service;

    protected PersistenceService persistenceService;

    protected ServiceDbResource.DataContext dataContext;

    protected ProgressionModel progressionModel;

    protected File dataDirectory;

    public static final String PROGRAM_ID = "CAM-EVHOE";

    public static final Integer CRUISE_ID = 100000;

    public static final Integer OPERATION_1_ID = 100000;
    public static final Integer OPERATION_2_ID = 100001;
    public static final Integer OPERATION_3_ID = 100002;

    @Before
    public void setUp() throws Exception {

        dataDirectory = dbResource.getConfig().getDataDirectory();

        TuttiServiceContext serviceContext = dbResource.getServiceContext();

        persistenceService = serviceContext.getService(PersistenceService.class);

        dbResource.loadInternalProtocolFile("genericFormat", "ano7992");

        dbResource.setCountryInConfig("12");
        dbResource.openDataContext();

        service = serviceContext.getService(GenericFormatExportService.class);

        dataContext = dbResource.loadContext(PROGRAM_ID, CRUISE_ID, 3, OPERATION_1_ID,OPERATION_2_ID, OPERATION_3_ID);

        progressionModel = new ProgressionModel();

    }

    @Test
    public void exportCruise() throws Exception {

        File exportFile = new File(dataDirectory, "exportCruise.zip");

        Files.createParentDirs(exportFile);

        Assert.assertFalse(exportFile.exists());

        String programId = dataContext.program.getId();
        Integer cruiseId = dataContext.cruise.getIdAsInt();

        ProgramDataModel dataToExport = persistenceService.loadCruises(programId, true, cruiseId);

        List<SampleCategoryModelEntry> categoryList = new ArrayList<>();

        {
            SampleCategoryModelEntry sampleCategoryModelEntry = new SampleCategoryModelEntry();
            sampleCategoryModelEntry.setCode("V_HV");
            sampleCategoryModelEntry.setOrder(0);
            sampleCategoryModelEntry.setCategoryId(1428);
            categoryList.add(sampleCategoryModelEntry);
        }
        {
            SampleCategoryModelEntry sampleCategoryModelEntry = new SampleCategoryModelEntry();
            sampleCategoryModelEntry.setCode("Class_Tri");
            sampleCategoryModelEntry.setOrder(1);
            sampleCategoryModelEntry.setCategoryId(198);
            categoryList.add(sampleCategoryModelEntry);
        }
        {
            SampleCategoryModelEntry sampleCategoryModelEntry = new SampleCategoryModelEntry();
            sampleCategoryModelEntry.setCode("Sexe");
            sampleCategoryModelEntry.setOrder(2);
            sampleCategoryModelEntry.setCategoryId(196);
            categoryList.add(sampleCategoryModelEntry);
        }
        {
            SampleCategoryModelEntry sampleCategoryModelEntry = new SampleCategoryModelEntry();
            sampleCategoryModelEntry.setCode("Etat");
            sampleCategoryModelEntry.setOrder(3);
            sampleCategoryModelEntry.setCategoryId(1682);
            categoryList.add(sampleCategoryModelEntry);
        }
        {
            SampleCategoryModelEntry sampleCategoryModelEntry = new SampleCategoryModelEntry();
            sampleCategoryModelEntry.setCode("Age");
            sampleCategoryModelEntry.setOrder(4);
            sampleCategoryModelEntry.setCategoryId(1702);
            categoryList.add(sampleCategoryModelEntry);
        }
        {
            SampleCategoryModelEntry sampleCategoryModelEntry = new SampleCategoryModelEntry();
            sampleCategoryModelEntry.setCode("etat_sante");
            sampleCategoryModelEntry.setOrder(5);
            sampleCategoryModelEntry.setCategoryId(1478);
            categoryList.add(sampleCategoryModelEntry);
        }{
            SampleCategoryModelEntry sampleCategoryModelEntry = new SampleCategoryModelEntry();
            sampleCategoryModelEntry.setCode("age_coquille");
            sampleCategoryModelEntry.setOrder(6);
            sampleCategoryModelEntry.setCategoryId(1418);
            categoryList.add(sampleCategoryModelEntry);
        }{
            SampleCategoryModelEntry sampleCategoryModelEntry = new SampleCategoryModelEntry();
            sampleCategoryModelEntry.setCode("age2");
            sampleCategoryModelEntry.setOrder(7);
            sampleCategoryModelEntry.setCategoryId(101);
            categoryList.add(sampleCategoryModelEntry);
        }

        categoryList.forEach(sampleCategoryModelEntry -> sampleCategoryModelEntry.load(persistenceService));

        persistenceService.setSampleCategoryModel(new SampleCategoryModel(categoryList));

        GenericFormatExportConfiguration exportConfiguration = new GenericFormatExportConfiguration();
        exportConfiguration.setExportFile(exportFile);
        exportConfiguration.setExportAttachments(false);
        exportConfiguration.setExportSpecies(true);
        exportConfiguration.setExportBenthos(false);
        exportConfiguration.setExportMarineLitter(false);
        exportConfiguration.setExportAccidentalCatch(false);
        exportConfiguration.setExportIndividualObservation(false);
        exportConfiguration.setDataToExport(dataToExport);

        int nbSteps = service.getExportNbSteps(exportConfiguration);
        progressionModel.setTotal(nbSteps);

        GenericFormatExportResult exportResult = service.export(exportConfiguration, progressionModel);
        Path catchPath = exportResult.getArchive().getCatchPath();
        String catchFileContent = new String(java.nio.file.Files.readAllBytes(catchPath));
        System.out.println("Catch file:\n" + catchFileContent);
        Assert.assertEquals(CATCH_FILE_CONTENT, catchFileContent);

        Path operationPath = exportResult.getArchive().getOperationPath();
        String operationFileContent = new String(java.nio.file.Files.readAllBytes(operationPath));
        System.out.println("Operation file:\n" + operationFileContent);
        Assert.assertEquals(OPERATION_FILE_CONTENT, operationFileContent);

    }

}

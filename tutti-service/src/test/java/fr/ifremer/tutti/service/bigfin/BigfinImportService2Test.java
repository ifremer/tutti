package fr.ifremer.tutti.service.bigfin;

/*
 * #%L
 * Tutti :: Service
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Charsets;
import com.google.common.base.Predicate;
import com.google.common.collect.Lists;
import com.google.common.io.Files;
import fr.ifremer.adagio.core.dao.referential.pmfm.PmfmId;
import fr.ifremer.adagio.core.dao.referential.pmfm.QualitativeValueId;
import fr.ifremer.tutti.persistence.entities.TuttiEntities;
import fr.ifremer.tutti.persistence.entities.data.BatchContainer;
import fr.ifremer.tutti.persistence.entities.data.CatchBatch;
import fr.ifremer.tutti.persistence.entities.data.CatchBatchs;
import fr.ifremer.tutti.persistence.entities.data.Cruise;
import fr.ifremer.tutti.persistence.entities.data.Cruises;
import fr.ifremer.tutti.persistence.entities.data.FishingOperation;
import fr.ifremer.tutti.persistence.entities.data.FishingOperations;
import fr.ifremer.tutti.persistence.entities.data.SpeciesBatch;
import fr.ifremer.tutti.persistence.entities.data.SpeciesBatchFrequency;
import fr.ifremer.tutti.persistence.entities.data.SpeciesBatchs;
import fr.ifremer.tutti.persistence.entities.referential.Caracteristic;
import fr.ifremer.tutti.persistence.entities.referential.CaracteristicQualitativeValue;
import fr.ifremer.tutti.persistence.entities.referential.CaracteristicQualitativeValues;
import fr.ifremer.tutti.persistence.entities.referential.Species;
import fr.ifremer.tutti.persistence.entities.referential.TuttiLocation;
import fr.ifremer.tutti.service.PersistenceService;
import fr.ifremer.tutti.service.ServiceDbResource;
import fr.ifremer.tutti.service.TuttiServiceContext;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.nuiton.util.DateUtil;

import java.io.File;
import java.io.IOException;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created on 3/9/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.14
 */
public class BigfinImportService2Test {

    /** Logger. */
    private static final Log log = LogFactory.getLog(BigfinImportService2Test.class);

    @Rule
    public final ServiceDbResource dbResource = ServiceDbResource.writeDb("dbEmpty");

    protected BigfinImportService service;

    protected PersistenceService persistenceService;

    protected ServiceDbResource.DataContext dataContext;

    protected File dataDirectory;

    protected Predicate<SpeciesBatch> vracPredicate;

    public static final String PROGRAM_ID = "CAM-CGFS";

    public static final Integer CRUISE_ID = 0;

    public static final Integer OPERATION_1_ID = 1;

    @Before
    public void setUp() throws Exception {

        dataDirectory = dbResource.getConfig().getDataDirectory();

        TuttiServiceContext serviceContext = dbResource.getServiceContext();

        dbResource.openDataContext();

        persistenceService = serviceContext.getService(PersistenceService.class);
        service = serviceContext.getService(BigfinImportService.class);

        dataContext = dbResource.loadContext(PROGRAM_ID);

        vracPredicate = SpeciesBatchs.newSpeciesAbleBatchCategoryPredicate(PmfmId.SORTED_UNSORTED.getValue(), QualitativeValueId.VRAC.getValue());
    }

    @Test
    public void testASpeciesWithNoLengthPmfmInProtocol() throws IOException {

        // Load protocol

        dbResource.loadInternalProtocolFile("bigfin/", "protocol");

        // Create cruise

        Cruise cruise = Cruises.newCruise();
        cruise.setName("MyCruise");
        cruise.setMultirigNumber(1);
        cruise.setBeginDate(DateUtil.createDate(1, 5, 2014));
        cruise.setEndDate(DateUtil.createDate(21, 5, 2014));
        TuttiLocation harbour = persistenceService.getAllHarbour().get(0);
        cruise.setDepartureLocation(harbour);
        cruise.setReturnLocation(harbour);
        cruise.setProgram(dataContext.program);
        cruise.setVessel(persistenceService.getAllScientificVessel().get(0));
        cruise.setHeadOfMission(Lists.newArrayList(persistenceService.getAllPerson().get(0)));

        cruise = persistenceService.createCruise(cruise);

        // Create empty fishing operation
        FishingOperation fishingOperation = FishingOperations.newFishingOperation();
        fishingOperation.setCruise(cruise);
        fishingOperation.setFishingOperationNumber(1);
        fishingOperation.setStationNumber("S0419");
        fishingOperation.setGearShootingStartDate(DateUtil.createDate(14, 5, 2014));
        fishingOperation.setGearShootingEndDate(DateUtil.createDate(14, 5, 2014));
        fishingOperation = persistenceService.createFishingOperation(fishingOperation);

        CatchBatch catchBatch = CatchBatchs.newCatchBatch();
        catchBatch.setFishingOperation(fishingOperation);

        persistenceService.createCatchBatch(catchBatch);

        {
            String fileContent = "Study name,ID,Date,LOC,STA,COMMENT,GPS X,GPS Y,SP CODE,SPEC,LENGTH (mm),WEIGHT (g),SEX,SIZE,MT,MS\n" +
                                 "Test,1,05/14/14 09:49:32, ,S0419, ,47.24713560992085,-1.5474773011082363, ,URANSCA,210, ,None,0,None,";

            File importFile = dbResource.getConfig().newTempFile("bigFin1");
            Files.write(fileContent, importFile, Charsets.UTF_8);

            BigfinImportResult importResult = service.importFile(importFile, fishingOperation, catchBatch);

            int nbFrequenciesAdded = importResult.getNbFrequenciesImported();
            List<String> errors = importResult.getErrors();
            List<String> warnings = importResult.getWarnings();

            if (log.isInfoEnabled()) {
                log.info("Frequencies Imported: " + nbFrequenciesAdded);
                log.info("Errors: " + errors.size());
                log.info("Warnings: " + warnings.size());
            }

            int nbNewFrequencies = 0;
            Assert.assertEquals(nbNewFrequencies, nbFrequenciesAdded);
            Assert.assertEquals(1, errors.size());
            Assert.assertEquals(0, warnings.size());

        }

        // Create a species batch for the species
        SpeciesBatch speciesBatch = SpeciesBatchs.newSpeciesBatch();
        speciesBatch.setFishingOperation(fishingOperation);
        Species species = persistenceService.getSpeciesByReferenceTaxonId(1764);
        speciesBatch.setSpecies(species);
        speciesBatch.setSampleCategoryId(PmfmId.SORTED_UNSORTED.getValue());
        Caracteristic sortedUnsortedCaracteristic = persistenceService.getSortedUnsortedCaracteristic();
        CaracteristicQualitativeValue qualitativeValue = CaracteristicQualitativeValues.getQualitativeValue(sortedUnsortedCaracteristic, QualitativeValueId.SORTED_VRAC.getValue());
        speciesBatch.setSampleCategoryValue(qualitativeValue);

        speciesBatch = persistenceService.createSpeciesBatch(speciesBatch, null, true);
        {

            String fileContent = "Study name,ID,Date,LOC,STA,COMMENT,GPS X,GPS Y,SP CODE,SPEC,LENGTH (mm),WEIGHT (g),SEX,SIZE,MT,MS\n" +
                                 "Test,1,05/14/14 09:49:32, ,S0419, ,47.24713560992085,-1.5474773011082363, ," + speciesBatch.getId() + ",210, ,None,0,None,";

            File importFile = dbResource.getConfig().newTempFile("bigFin1");
            Files.write(fileContent, importFile, Charsets.UTF_8);

            BigfinImportResult importResult = service.importFile(importFile, fishingOperation, catchBatch);

            int nbFrequenciesAdded = importResult.getNbFrequenciesImported();
            List<String> errors = importResult.getErrors();
            List<String> warnings = importResult.getWarnings();

            if (log.isInfoEnabled()) {
                log.info("Frequencies Imported: " + nbFrequenciesAdded);
                log.info("Errors: " + errors.size());
                log.info("Warnings: " + warnings.size());
            }

            int nbNewFrequencies = 0;
            Assert.assertEquals(nbNewFrequencies, nbFrequenciesAdded);
            Assert.assertEquals(1, errors.size());
            Assert.assertEquals(0, warnings.size());
        }

    }

    @Test
    public void testSpeciesWithNoneAndUnknownedSex() throws IOException {

        // Load protocol

        dbResource.loadInternalProtocolFile("bigfin/", "importbigfin2");

        Species tracTruSpecies = persistenceService.getSpeciesByReferenceTaxonId(1662);
        Assert.assertNotNull(tracTruSpecies);
        Species tracDraSpecies = persistenceService.getSpeciesByReferenceTaxonId(1759);
        Assert.assertNotNull(tracDraSpecies);
        Species rajaClaSpecies = persistenceService.getSpeciesByReferenceTaxonId(1302);
        Assert.assertNotNull(rajaClaSpecies);
        Species trigLyrSpecies = persistenceService.getSpeciesByReferenceTaxonId(1910);
        Assert.assertNotNull(trigLyrSpecies);
        Species ACANPALSpecies = persistenceService.getSpeciesByReferenceTaxonId(1732);
        Assert.assertNotNull(ACANPALSpecies);

        Caracteristic sizeCaracteristic = persistenceService.getSizeCategoryCaracteristic();
        Assert.assertNotNull(sizeCaracteristic);
        Map<String, CaracteristicQualitativeValue> sizeQualitativeValues = TuttiEntities.splitById(sizeCaracteristic.getQualitativeValue());

        Caracteristic sexCaracteristic = persistenceService.getSexCaracteristic();
        Assert.assertNotNull(sexCaracteristic);
        Map<String, CaracteristicQualitativeValue> sexQualitativeValues = TuttiEntities.splitById(sexCaracteristic.getQualitativeValue());

        // Create cruise

        Cruise cruise = Cruises.newCruise();
        cruise.setName("MyCruise2");
        cruise.setMultirigNumber(1);
        cruise.setBeginDate(DateUtil.createDate(1, 9, 2014));
        cruise.setEndDate(DateUtil.createDate(30, 9, 2014));
        TuttiLocation harbour = persistenceService.getAllHarbour().get(0);
        cruise.setDepartureLocation(harbour);
        cruise.setReturnLocation(harbour);
        cruise.setProgram(dataContext.program);
        cruise.setVessel(persistenceService.getAllScientificVessel().get(0));
        cruise.setHeadOfMission(Lists.newArrayList(persistenceService.getAllPerson().get(0)));

        cruise = persistenceService.createCruise(cruise);

        // Create empty fishing operation
        FishingOperation fishingOperation = FishingOperations.newFishingOperation();
        fishingOperation.setCruise(cruise);
        fishingOperation.setFishingOperationNumber(1);
        fishingOperation.setStationNumber("A");
        fishingOperation.setGearShootingStartDate(DateUtil.createDate(14, 9, 2015));
        fishingOperation.setGearShootingEndDate(DateUtil.createDate(14, 9, 2015));
        fishingOperation = persistenceService.createFishingOperation(fishingOperation);

        CatchBatch catchBatch = CatchBatchs.newCatchBatch();
        catchBatch.setFishingOperation(fishingOperation);

        persistenceService.createCatchBatch(catchBatch);

        {
            String fileContent = "Study name,ID,Date,LOC,STA,COMMENT,GPS X,GPS Y,SP CODE,SPEC,LENGTH (mm),WEIGHT (g),SEX,SIZE,MT,MS\n" +
                                 "Test, 1,09/14/15 09:49:32,,A,,47.24713560992085,-1.5474773011082363,,TRAC-TRU,200,    ,None,   2,None,\n" +
                                 "Test, 2,09/14/15 09:49:32,,A,,47.24713560992085,-1.5474773011082363,,TRAC-TRU,201,    ,None,   1,None,\n" +
                                 "Test, 3,09/14/15 09:49:32,,A,,47.24713560992085,-1.5474773011082363,,TRAC-TRU,202,    ,None,   1,None,\n" +
                                 "Test, 4,09/14/15 09:49:32,,A,,47.24713560992085,-1.5474773011082363,,TRAC-TRU,203,    ,None,   1,None,\n" +
                                 "Test, 5,09/14/15 09:49:32,,A,,47.24713560992085,-1.5474773011082363,,tracdra, 204,    ,None,   0,None,\n" +
                                 "Test, 6,09/14/15 09:49:32,,A,,47.24713560992085,-1.5474773011082363,,tracdra, 205,    ,Male,   0,None,\n" +
                                 "Test, 7,09/14/15 09:49:32,,A,,47.24713560992085,-1.5474773011082363,,tracdra, 206,    ,Female, 0,None,\n" +
                                 "Test, 8,09/14/15 09:49:32,,A,,47.24713560992085,-1.5474773011082363,,tracdra, 207,    ,Unknown,0,None,\n" +
                                 "Test,14,09/14/15 09:49:32,,A,,47.24713560992085,-1.5474773011082363,,rajacla, 213,    ,Female, 0,None,\n" +
                                 "Test,15,09/14/15 09:49:32,,A,,47.24713560992085,-1.5474773011082363,,rajacla, 214,    ,Female, 0,None,\n" +
                                 "Test,16,09/14/15 09:49:32,,A,,47.24713560992085,-1.5474773011082363,,rajacla, 215,    ,Male,   0,None,\n" +
                                 "Test,17,09/14/15 09:49:32,,A,,47.24713560992085,-1.5474773011082363,,rajacla, 216,    ,Female, 0,None,\n" +
                                 "Test,18,09/14/15 09:49:32,,A,,47.24713560992085,-1.5474773011082363,,rajacla, 217,    ,Female, 0,None,\n" +
                                 "Test,19,09/14/15 09:49:32,,A,,47.24713560992085,-1.5474773011082363,,triglyr, 500,1000,None,   0,None,\n" +
                                 "Test,20,09/14/15 09:49:32,,A,,47.24713560992085,-1.5474773011082363,,triglyr, 510,    ,None,   0,None,\n" +
                                 "Test,21,09/14/15 09:49:32,,A,,47.24713560992085,-1.5474773011082363,,triglyr, 520,    ,None,   0,None,\n" +
                                 "Test,22,09/14/15 09:49:32,,A,,47.24713560992085,-1.5474773011082363,,triglyr, 530,    ,None,   0,None,\n" +
                                 "Test,23,09/14/15 09:49:32,,A,,47.24713560992085,-1.5474773011082363,,triglyr, 500,    ,None,   0,None,\n" +
                                 "Test,24,09/14/15 09:49:32,,A,,47.24713560992085,-1.5474773011082363,,ACAN-PAL,100,    ,None,   0,None,\n" +
                                 "Test,25,09/14/15 09:49:32,,A,,47.24713560992085,-1.5474773011082363,,ACAN-PAL,110,1000,None,   0,None,\n" +
                                 "Test,26,09/14/15 09:49:32,,A,,47.24713560992085,-1.5474773011082363,,ACAN-PAL,120,    ,None,   0,None,\n" +
                                 "Test,27,09/14/15 09:49:32,,A,,47.24713560992085,-1.5474773011082363,,ACAN-PAL,130,2000,None,   0,None,";

            File importFile = dbResource.getConfig().newTempFile("bigFin1");
            Files.write(fileContent, importFile, Charsets.UTF_8);

            BigfinImportResult importResult = service.importFile(importFile, fishingOperation, catchBatch);

            int nbFrequenciesAdded = importResult.getNbFrequenciesImported();
            List<String> errors = importResult.getErrors();
            List<String> warnings = importResult.getWarnings();

            if (log.isInfoEnabled()) {
                log.info("Frequencies Imported: " + nbFrequenciesAdded);
                log.info("Errors: " + errors.size());
                log.info("Warnings: " + warnings.size());
            }

            int nbNewFrequencies = 22;
            Assert.assertEquals(nbNewFrequencies, nbFrequenciesAdded);
            Assert.assertEquals(0, errors.size());
            Assert.assertEquals(0, warnings.size());

            BatchContainer<SpeciesBatch> rootSpeciesBatch = persistenceService.getRootSpeciesBatch(fishingOperation.getIdAsInt(), false);
            Assert.assertNotNull(rootSpeciesBatch);
            Assert.assertEquals(5, rootSpeciesBatch.sizeChildren());

            Set<Species> speciesSet = new LinkedHashSet<>();
            SpeciesBatchs.grabSpeciesChildBatchs(rootSpeciesBatch.getChildren(), speciesSet);
            Assert.assertEquals(5, speciesSet.size());
            Assert.assertTrue(speciesSet.contains(rajaClaSpecies));
            Assert.assertTrue(speciesSet.contains(tracDraSpecies));
            Assert.assertTrue(speciesSet.contains(tracTruSpecies));
            Assert.assertTrue(speciesSet.contains(trigLyrSpecies));
            Assert.assertTrue(speciesSet.contains(ACANPALSpecies));

            for (SpeciesBatch speciesBatch : rootSpeciesBatch.getChildren()) {

                Set<Integer> sampleCategories = new LinkedHashSet<>();
                SpeciesBatchs.grabSampleCategorieValuesChildBatchs(speciesBatch, sampleCategories);
                List<SpeciesBatchFrequency> allSpeciesBatchFrequency = persistenceService.getAllSpeciesBatchFrequency(speciesBatch.getIdAsInt());

                if (rajaClaSpecies.equals(speciesBatch.getSpecies())) {

                    // 2 catégories Male, Femelle + Vrac
                    Assert.assertEquals(2, speciesBatch.getChildBatchs().size());
                    Assert.assertEquals(3, sampleCategories.size());
                    Assert.assertTrue(sampleCategories.contains(311)); // Vrac
                    Assert.assertTrue(sampleCategories.contains(300)); // M
                    Assert.assertTrue(sampleCategories.contains(301)); // F
                    Assert.assertEquals(0, allSpeciesBatchFrequency.size());
                    Assert.assertNull(speciesBatch.getSampleCategoryWeight());
                } else if (tracTruSpecies.equals(speciesBatch.getSpecies())) {

                    // 2 categories Gros et Petit
                    Assert.assertEquals(2, speciesBatch.getChildBatchs().size());
                    Assert.assertEquals(3, sampleCategories.size());
                    Assert.assertTrue(sampleCategories.contains(305)); // G
                    Assert.assertTrue(sampleCategories.contains(307)); // P
                    Assert.assertEquals(0, allSpeciesBatchFrequency.size());
                    Assert.assertNull(speciesBatch.getSampleCategoryWeight());
                } else if (tracDraSpecies.equals(speciesBatch.getSpecies())) {

                    // 4 catégories Non sexe, Male, Femelle et Sexe inconnu + Vrac
                    Assert.assertEquals(4, speciesBatch.getChildBatchs().size());
                    Assert.assertEquals(5, sampleCategories.size());
                    Assert.assertTrue(sampleCategories.contains(311)); // Vrac
                    Assert.assertTrue(sampleCategories.contains(299)); // U
                    Assert.assertTrue(sampleCategories.contains(300)); // M
                    Assert.assertTrue(sampleCategories.contains(301)); // F
                    Assert.assertTrue(sampleCategories.contains(302)); // N
                    Assert.assertEquals(0, allSpeciesBatchFrequency.size());
                    Assert.assertNull(speciesBatch.getSampleCategoryWeight());
                } else if (trigLyrSpecies.equals(speciesBatch.getSpecies())) {
                    // Un seul poids renseigne -> on le met comme poids de sous echantillon
                    // Pas de catégorie
                    Assert.assertEquals(0, speciesBatch.getChildBatchs().size());
                    Assert.assertEquals(1, sampleCategories.size());
                    Assert.assertTrue(sampleCategories.contains(311)); // Vrac
                    Assert.assertEquals(4, allSpeciesBatchFrequency.size());
                    Assert.assertEquals(1f, speciesBatch.getSampleCategoryWeight(), 0.0f);
                } else if (ACANPALSpecies.equals(speciesBatch.getSpecies())) {
                    // Plusieurs poids renseignes mais pas tous, on ne met aucun poids
                    // Pas de catégorie
                    Assert.assertEquals(0, speciesBatch.getChildBatchs().size());
                    Assert.assertEquals(1, sampleCategories.size());
                    Assert.assertTrue(sampleCategories.contains(311)); // Vrac
                    Assert.assertEquals(4, allSpeciesBatchFrequency.size());
                    Assert.assertNull(speciesBatch.getSampleCategoryWeight());
                    for (SpeciesBatchFrequency speciesBatchFrequency : allSpeciesBatchFrequency) {
                        Assert.assertNull(speciesBatchFrequency.getWeight());
                    }
                }
            }

        }

    }
}

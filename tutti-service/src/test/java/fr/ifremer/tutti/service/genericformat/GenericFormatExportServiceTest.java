package fr.ifremer.tutti.service.genericformat;

/*
 * #%L
 * Tutti :: Service
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.io.Files;
import fr.ifremer.tutti.persistence.ProgressionModel;
import fr.ifremer.tutti.persistence.model.ProgramDataModel;
import fr.ifremer.tutti.service.PersistenceService;
import fr.ifremer.tutti.service.ServiceDbResource;
import fr.ifremer.tutti.service.TuttiServiceContext;
import org.junit.Assert;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Ignore;
import org.junit.Test;

import java.io.File;

/**
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.3
 */
public class GenericFormatExportServiceTest {

    @ClassRule
    public static final ServiceDbResource dbResource = ServiceDbResource.readDb("dbCGFS");

    public static final String PROGRAM_ID = "CAM-CGFS";

    public static final Integer CRUISE_ID = 100001;

//    public static final String CRUISE_CGFS_ID = "100000";

    public static final Integer OPERATION_1_ID = 100106;

    public static final Integer OPERATION_2_ID = 100107;

    public static final String SURVEY_CONTENT =
            "Annee;Serie;Serie_Partielle;Navire;Nombre_Poches;Pays;Zone_Etude;Campagne;Id_Sismer;Date_Deb_Campagne;Port_Deb_Campagne;Date_Fin_Campagne;Port_Fin_Campagne;Chef_Mission;Resp_Salle_Tri;Commentaire;Serie_Id;Navire_Id;Engin_Id;Port_Deb_Campagne_Id;Port_Fin_Campagne_Id;Chef_Mission_Id;Resp_Salle_Tri_Id\n" +
            "2013;Campagne CGFS;;278970;1;FRA;CGFS - Manche Est / Sud Mer du Nord;Campagne CGFS_2013;;01/05/2013 00:00:00;La Barbotière (Gujan-Mestras);31/05/2013 00:00:00;Etang de Palo;Vincent AURECHE;Alain TETARD;;CAM-CGFS;278970;377|-2;892;12407;1981;104";

    public static final String GEAR_CARACTERISTICS_CONTENT =
            "Annee;Serie;Serie_Partielle;Engin;Rang_Engin;Code_PMFM;Libelle_PMFM;Valeur;Serie_Id;Engin_Id;Valeur_Id\n" +
            "2013;Campagne CGFS;;ScientificGear;1;289;Armature (drague) - engin - totale - Déclaration d'un professionnel;Lame;CAM-CGFS;-2;361\n" +
            "2013;Campagne CGFS;;ScientificGear;1;121;Diamètre du goulot (Casier) - engin - goulotte - Déclaration d'un professionnel;120.0;CAM-CGFS;-2;120.0\n" +
            "2013;Campagne CGFS;;ScientificGear;1;927;Maillage étiré de l'engin - engin - Maille des ailes - Inconnue;15.0;CAM-CGFS;-2;15.0\n" +
            "2013;Campagne CGFS;;ScientificGear;1;287;Volet dépresseur (drague) - engin - totale - Déclaration d'un professionnel;Oui;CAM-CGFS;-2;401";

    public static final String OPERATION_WITH_NO_CATCH_CONTENT =
            "Annee;Serie;Serie_Partielle;Code_Station;Id_Operation;Poche;Engin;Rang_Engin;Navire;DateDeb;LatDeb;LongDeb;DateFin;LatFin;LongFin;Duree;Strate;Sous_Strate;Localite;Validite_OP;Rectiligne;Distance;Saisisseur;Navire_Associe;Commentaire;Poids_Total;Poids_Total_Calcule;Poids_Total_Vrac;Poids_Total_Vrac_Calcule;Poids_Total_HorsVrac;Poids_Total_HorsVrac_Calcule;Poids_Total_Non_Trie;Poids_Total_Non_Trie_Calcule;Poids_Total_Tremis;Poids_Total_Tremis_Calcule;Poids_Total_Carroussel;Poids_Total_Carroussel_Calcule;Poids_Total_Espece;Poids_Total_Espece_Calcule;Poids_Total_Espece_Vrac;Poids_Total_Espece_Vrac_Calcule;Poids_Total_Espece_Vrac_Trie;Poids_Total_Espece_Vrac_Trie_Calcule;Poids_Total_Espece_HorsVrac;Poids_Total_Espece_HorsVrac_Calcule;Poids_Total_Espece_Inerte_Trie;Poids_Total_Espece_Inerte_Trie_Calcule;Poids_Total_Espece_Vivant_non_detaille_trie;Poids_Total_Espece_Vivant_non_detaille_trie_Calcule;Poids_Total_Benthos;Poids_Total_Benthos_Calcule;Poids_Total_Benthos_Vrac;Poids_Total_Benthos_Vrac_Calcule;Poids_Total_Benthos_Vrac_Trie;Poids_Total_Benthos_Vrac_Trie_Calcule;Poids_Total_Benthos_HorsVrac;Poids_Total_Benthos_HorsVrac_Calcule;Poids_Total_Benthos_Inerte_Trie;Poids_Total_Benthos_Inerte_Trie_Calcule;Poids_Total_Benthos_Vivant_non_detaille_trie;Poids_Total_Benthos_Vivant_non_detaille_trie_Calcule;Poids_Total_Macro_Dechet;Poids_Total_Macro_Dechet_Calcule;Serie_Id;Engin_Id;Navire_Id;Strate_Id;Sous_Strate_Id;Localite_Id;Saisisseur_Id;Navire_Associe_Id\n" +
            "2010;Campagne CGFS;;65;65;1;GOV 19.7/25.9;1;278970;13/10/2010 13:35:00;50.22833;0.31833;13/10/2010 14:05:00;50.22167;0.28333;30;Strate 4J;NA;Localité 4J2;N;Y;2512;;;avarie - chalut annulé completement à poil;-9.0;?;-9.0;Y;-9.0;Y;-9.0;?;-9.0;N;-9.0;N;-9.0;Y;-9.0;?;-9.0;Y;-9.0;Y;-9.0;?;-9.0;?;-9.0;Y;-9.0;?;-9.0;Y;-9.0;Y;-9.0;?;-9.0;?;-9.0;?;CAM-CGFS;377;278970;57345;;57837;;";

    public static final String OPERATION_WITH_NO_CATCH_CONTENT_AND_NO_GEAR =
            "Annee;Serie;Serie_Partielle;Code_Station;Id_Operation;Poche;Engin;Rang_Engin;Navire;DateDeb;LatDeb;LongDeb;DateFin;LatFin;LongFin;Duree;Strate;Sous_Strate;Localite;Validite_OP;Rectiligne;Distance;Saisisseur;Navire_Associe;Commentaire;Poids_Total;Poids_Total_Calcule;Poids_Total_Vrac;Poids_Total_Vrac_Calcule;Poids_Total_HorsVrac;Poids_Total_HorsVrac_Calcule;Poids_Total_Non_Trie;Poids_Total_Non_Trie_Calcule;Poids_Total_Tremis;Poids_Total_Tremis_Calcule;Poids_Total_Carroussel;Poids_Total_Carroussel_Calcule;Poids_Total_Espece;Poids_Total_Espece_Calcule;Poids_Total_Espece_Vrac;Poids_Total_Espece_Vrac_Calcule;Poids_Total_Espece_Vrac_Trie;Poids_Total_Espece_Vrac_Trie_Calcule;Poids_Total_Espece_HorsVrac;Poids_Total_Espece_HorsVrac_Calcule;Poids_Total_Espece_Inerte_Trie;Poids_Total_Espece_Inerte_Trie_Calcule;Poids_Total_Espece_Vivant_non_detaille_trie;Poids_Total_Espece_Vivant_non_detaille_trie_Calcule;Poids_Total_Benthos;Poids_Total_Benthos_Calcule;Poids_Total_Benthos_Vrac;Poids_Total_Benthos_Vrac_Calcule;Poids_Total_Benthos_Vrac_Trie;Poids_Total_Benthos_Vrac_Trie_Calcule;Poids_Total_Benthos_HorsVrac;Poids_Total_Benthos_HorsVrac_Calcule;Poids_Total_Benthos_Inerte_Trie;Poids_Total_Benthos_Inerte_Trie_Calcule;Poids_Total_Benthos_Vivant_non_detaille_trie;Poids_Total_Benthos_Vivant_non_detaille_trie_Calcule;Poids_Total_Macro_Dechet;Poids_Total_Macro_Dechet_Calcule;Serie_Id;Engin_Id;Navire_Id;Strate_Id;Sous_Strate_Id;Localite_Id;Saisisseur_Id;Navire_Associe_Id\n" +
            "2010;Campagne CGFS;;65;65;1;;;278970;13/10/2010 13:35:00;50.22833;0.31833;13/10/2010 14:05:00;50.22167;0.28333;30;Strate 4J;NA;Localité 4J2;N;Y;2512;;;avarie - chalut annulé completement à poil;-9.0;?;-9.0;Y;-9.0;Y;-9.0;?;-9.0;N;-9.0;N;-9.0;Y;-9.0;?;-9.0;Y;-9.0;Y;-9.0;?;-9.0;?;-9.0;Y;-9.0;?;-9.0;Y;-9.0;Y;-9.0;?;-9.0;?;-9.0;?;CAM-CGFS;;278970;57345;;57837;;";

    public static final String OPERATION_CONTENT =
            "Annee;Serie;Serie_Partielle;Code_Station;Id_Operation;Poche;Engin;Rang_Engin;Navire;DateDeb;LatDeb;LongDeb;DateFin;LatFin;LongFin;Duree;Strate;Sous_Strate;Localite;Validite_OP;Rectiligne;Distance;Saisisseur;Navire_Associe;Commentaire;Poids_Total;Poids_Total_Calcule;Poids_Total_Vrac;Poids_Total_Vrac_Calcule;Poids_Total_HorsVrac;Poids_Total_HorsVrac_Calcule;Poids_Total_Non_Trie;Poids_Total_Non_Trie_Calcule;Poids_Total_Tremis;Poids_Total_Tremis_Calcule;Poids_Total_Carroussel;Poids_Total_Carroussel_Calcule;Poids_Total_Espece;Poids_Total_Espece_Calcule;Poids_Total_Espece_Vrac;Poids_Total_Espece_Vrac_Calcule;Poids_Total_Espece_Vrac_Trie;Poids_Total_Espece_Vrac_Trie_Calcule;Poids_Total_Espece_HorsVrac;Poids_Total_Espece_HorsVrac_Calcule;Poids_Total_Espece_Inerte_Trie;Poids_Total_Espece_Inerte_Trie_Calcule;Poids_Total_Espece_Vivant_non_detaille_trie;Poids_Total_Espece_Vivant_non_detaille_trie_Calcule;Poids_Total_Benthos;Poids_Total_Benthos_Calcule;Poids_Total_Benthos_Vrac;Poids_Total_Benthos_Vrac_Calcule;Poids_Total_Benthos_Vrac_Trie;Poids_Total_Benthos_Vrac_Trie_Calcule;Poids_Total_Benthos_HorsVrac;Poids_Total_Benthos_HorsVrac_Calcule;Poids_Total_Benthos_Inerte_Trie;Poids_Total_Benthos_Inerte_Trie_Calcule;Poids_Total_Benthos_Vivant_non_detaille_trie;Poids_Total_Benthos_Vivant_non_detaille_trie_Calcule;Poids_Total_Macro_Dechet;Poids_Total_Macro_Dechet_Calcule;Serie_Id;Engin_Id;Navire_Id;Strate_Id;Sous_Strate_Id;Localite_Id;Saisisseur_Id;Navire_Associe_Id\n" +
            "2013;Campagne CGFS;;A;1;1;GOV 19.7/25.9;1;278970;01/05/2013 00:00:00;;;01/05/2013 00:23:00;;;23;Strate 1D;NA;Localité 1D2;?;N;-9;Vincent AURECHE;;op1;120.0;Y;100.0;Y;20.0;Y;0.0;Y;-9.0;N;-9.0;N;120.0;Y;100.0;Y;100.0;Y;20.0;Y;0.0;Y;0.0;Y;0.0;Y;0.0;Y;0.0;Y;0.0;Y;0.0;Y;0.0;Y;6.0;Y;CAM-CGFS;377;278970;57308;;57776;1981;\n" +
            "2013;Campagne CGFS;;A;2;1;GOV 19.7/25.9;1;278970;11/05/2013 08:00:00;;;11/05/2013 08:23:00;;;23;Strate 1D;NA;Localité 1D2;?;N;-9;Robert BELLAIL|Herve BARONE;;OP2;0.0;Y;0.0;Y;0.0;Y;0.0;Y;-9.0;N;-9.0;N;0.0;Y;0.0;Y;0.0;Y;0.0;Y;0.0;Y;0.0;Y;0.0;Y;0.0;Y;0.0;Y;0.0;Y;0.0;Y;0.0;Y;0.0;Y;CAM-CGFS;377;278970;57308;;57776;30|62;";

    public static final String PARAMETER_CONTENT =
            "Annee;Serie;Serie_Partielle;Code_Station;Id_Operation;Poche;Code_PMFM;Libelle_PMFM;Valeur;Type;Serie_Id;Valeur_Id\n" +
            "2013;Campagne CGFS;;A;1;1;1164;Catégorie UE - produit/lot - totale - Diffusion par une Halle à marée;Cat UE10;GEAR;CAM-CGFS;1482\n" +
            "2013;Campagne CGFS;;A;1;1;1302;(Gross Tonnage) : augmentation de tonnage accordée pour des raisons de sécurité - navire - totale - Déclaration d'un professionnel;10.0;GEAR;CAM-CGFS;10.0\n" +
            "2013;Campagne CGFS;;A;1;1;194;\"Etat de la mer - masse d'eau, eau brute - totale - Observation par un observateur\";\"2 - belle, vagues de 0.1 à 0.5 mètres\";VESSEL;CAM-CGFS;281\n" +
            "2013;Campagne CGFS;;A;1;1;821;Direction vent - air - totale - Instrument de bord;0.1;VESSEL;CAM-CGFS;0.1\n" +
            "2013;Campagne CGFS;;A;1;1;149;Coût de la glace - navire - totale - Déclaration d'un professionnel;10.0;VESSEL;CAM-CGFS;10.0\n" +
            "2013;Campagne CGFS;;A;2;1;1062;Catégorie de fraicheur - produit/lot - totale - Diffusion par une Halle à marée;A - Catégorie A;GEAR;CAM-CGFS;1421\n" +
            "2013;Campagne CGFS;;A;2;1;308;Nombre d'engin - engin - totale - Déclaration d'un professionnel;2.0;VESSEL;CAM-CGFS;2.0";

    public static final String CATCH_CONTENT =
            "Annee;Serie;Serie_Partielle;Code_Station;Id_Operation;Poche;Code_Taxon;Code_Espece_Campagne;Nom_Scientifique;Benthos;Commentaire;V_HV;Num_Ordre_V_HV_H2;Tot_V_HV;Ech_V_HV;Type_Volume_Poids_V_HV;Unite_Volume_Poids_V_HV;Class_Tri;Num_Ordre_Class_Tri_H2;Tot_Class_Tri;Ech_Class_Tri;Type_Volume_Poids_Class_Tri;Unite_Volume_Poids_Class_Tri;Sexe;Num_Ordre_Sexe_H2;Tot_Sexe;Ech_Sexe;Type_Volume_Poids_Sexe;Unite_Volume_Poids_Sexe;Maturite;Num_Ordre_Maturite_H2;Tot_Maturite;Ech_Maturite;Type_Volume_Poids_Maturite;Unite_Volume_Poids_Maturite;Age;Num_Ordre_Age_H2;Tot_Age;Ech_Age;Type_Volume_Poids_Age;Unite_Volume_Poids_Age;Code_Longueur;Libelle_Longueur;Taille;NumOrdre_Taille_H2;Poids_Classe_Taille;Unite_Taille;Precision_Mesure;Nbr;Nbr_Calcule;Poids_Reference;Coef_Elev_Espece_Capture;Coef_Final_Elevation;Serie_Id;V_HV_Id;Class_Tri_Id;Sexe_Id;Maturite_Id;Age_Id\n" +
            "2013;Campagne CGFS;;A;1;1;11242;;Aaptos;N;|||;Vrac;1;100.0;;Poids;kg;G - Gros;1;80.0;;Poids;kg;Mâle;1;30.0;;Poids;kg;1 - Stade 1;1;10.0;5.0;Poids;kg;NA;;;;;kg;307;Longueur totale (LT) - individu - totale - Mesure au 1/2 cm par un observateur;10.0;1;;cm;0.5;5;N;5.0;20.0;3.0;CAM-CGFS;311;305;300;272;NA\n" +
            "2013;Campagne CGFS;;A;1;1;11242;;Aaptos;N;|||;Vrac;1;100.0;;Poids;kg;G - Gros;1;80.0;;Poids;kg;Mâle;1;30.0;;Poids;kg;1 - Stade 1;1;10.0;5.0;Poids;kg;NA;;;;;kg;307;Longueur totale (LT) - individu - totale - Mesure au 1/2 cm par un observateur;10.5;2;;cm;0.5;2;N;5.0;20.0;3.0;CAM-CGFS;311;305;300;272;NA\n" +
            "2013;Campagne CGFS;;A;1;1;11242;;Aaptos;N;|||;Vrac;1;100.0;;Poids;kg;G - Gros;1;80.0;;Poids;kg;Mâle;1;30.0;;Poids;kg;1 - Stade 1;1;10.0;5.0;Poids;kg;NA;;;;;kg;307;Longueur totale (LT) - individu - totale - Mesure au 1/2 cm par un observateur;11.0;3;;cm;0.5;1;N;5.0;20.0;3.0;CAM-CGFS;311;305;300;272;NA\n" +
            "2013;Campagne CGFS;;A;1;1;11242;;Aaptos;N;|||;Vrac;1;100.0;;Poids;kg;G - Gros;1;80.0;;Poids;kg;Mâle;1;30.0;;Poids;kg;3 - Stade 3;3;10.0;;Poids;kg;NA;;;;;kg;307;Longueur totale (LT) - individu - totale - Mesure au 1/2 cm par un observateur;11.0;1;;cm;0.5;5;N;10.0;10.0;1.5;CAM-CGFS;311;305;300;274;NA\n" +
            "2013;Campagne CGFS;;A;1;1;11242;;Aaptos;N;||;Vrac;1;100.0;;Poids;kg;G - Gros;1;80.0;;Poids;kg;Femelle;2;50.0;30.0;Poids;kg;NA;;;;;kg;NA;;;;;kg;1425;Longueur totale (LT) - individu - queue - Mesure au cm par un observateur;10.0;1;;cm;;5;N;30.0;3.3333333;1.6666666;CAM-CGFS;311;305;301;NA;NA\n" +
            "2013;Campagne CGFS;;A;1;1;11242;;Aaptos;N;||;Vrac;1;100.0;;Poids;kg;G - Gros;1;80.0;;Poids;kg;Femelle;2;50.0;30.0;Poids;kg;NA;;;;;kg;NA;;;;;kg;1425;Longueur totale (LT) - individu - queue - Mesure au cm par un observateur;11.0;2;;cm;;6;N;30.0;3.3333333;1.6666666;CAM-CGFS;311;305;301;NA;NA\n" +
            "2013;Campagne CGFS;;A;1;1;11242;;Aaptos;N;||;Vrac;1;100.0;;Poids;kg;G - Gros;1;80.0;;Poids;kg;Femelle;2;50.0;30.0;Poids;kg;NA;;;;;kg;NA;;;;;kg;1425;Longueur totale (LT) - individu - queue - Mesure au cm par un observateur;12.0;3;;cm;;7;N;30.0;3.3333333;1.6666666;CAM-CGFS;311;305;301;NA;NA\n" +
            "2013;Campagne CGFS;;A;1;1;11242;;Aaptos;N;|;Vrac;1;100.0;;Poids;kg;M - Moyen;2;20.0;;Poids;kg;NA;;;;;kg;NA;;;;;kg;NA;;;;;kg;;;;;;;;;?;20.0;5.0;1.0;CAM-CGFS;311;306;NA;NA;NA\n" +
            "2013;Campagne CGFS;;A;1;1;11242;;Aaptos;N;;Hors Vrac;1;20.0;;Poids;kg;NA;;;;;kg;NA;;;;;kg;NA;;;;;kg;NA;;;;;kg;;;;;;;;2;?;20.0;1.0;1.0;CAM-CGFS;310;NA;NA;NA;NA";

    public static final String CATCH_CONTENT_2 =
            "Annee;Serie;Serie_Partielle;Code_Station;Id_Operation;Poche;Code_Taxon;Code_Espece_Campagne;Nom_Scientifique;Benthos;Commentaire;V_HV;Num_Ordre_V_HV_H2;Tot_V_HV;Ech_V_HV;Type_Volume_Poids_V_HV;Unite_Volume_Poids_V_HV;Class_Tri;Num_Ordre_Class_Tri_H2;Tot_Class_Tri;Ech_Class_Tri;Type_Volume_Poids_Class_Tri;Unite_Volume_Poids_Class_Tri;Sexe;Num_Ordre_Sexe_H2;Tot_Sexe;Ech_Sexe;Type_Volume_Poids_Sexe;Unite_Volume_Poids_Sexe;Maturite;Num_Ordre_Maturite_H2;Tot_Maturite;Ech_Maturite;Type_Volume_Poids_Maturite;Unite_Volume_Poids_Maturite;Age;Num_Ordre_Age_H2;Tot_Age;Ech_Age;Type_Volume_Poids_Age;Unite_Volume_Poids_Age;Code_Longueur;Libelle_Longueur;Taille;NumOrdre_Taille_H2;Poids_Classe_Taille;Unite_Taille;Precision_Mesure;Nbr;Nbr_Calcule;Poids_Reference;Coef_Elev_Espece_Capture;Coef_Final_Elevation;Serie_Id;V_HV_Id;Class_Tri_Id;Sexe_Id;Maturite_Id;Age_Id\n" +
            "2010;Campagne CGFS;;20;20;1;365;;Aequipecten opercularis;N;taxon;Vrac;1;0.005;0.005;Poids;kg;NA;;;;;kg;NA;;;;;kg;NA;;;;;kg;NA;;;;;kg;;;;;;;;1;?;0.005;1.0;1.0;CAM-CGFS;311;NA;NA;NA;NA\n" +
            "2010;Campagne CGFS;;20;20;1;491;ALLOSPP;Alloteuthis;N;taxon;Vrac;2;0.004;0.004;Poids;kg;NA;;;;;kg;NA;;;;;kg;NA;;;;;kg;NA;;;;;kg;;;;;;;;1;?;0.004;1.0;1.0;CAM-CGFS;311;NA;NA;NA;NA\n" +
            "2010;Campagne CGFS;;20;20;1;300;;Buccinum undatum;N;taxon;Vrac;3;0.015;0.015;Poids;kg;NA;;;;;kg;NA;;;;;kg;NA;;;;;kg;NA;;;;;kg;;;;;;;;1;?;0.015;1.0;1.0;CAM-CGFS;311;NA;NA;NA;NA\n" +
            "2010;Campagne CGFS;;20;20;1;1811;CALMLYR;Callionymus lyra;N;taxon;Vrac;4;0.07;0.07;Poids;kg;NA;;;;;kg;NA;;;;;kg;NA;;;;;kg;NA;;;;;kg;;;;;;;;1;?;0.07;1.0;1.0;CAM-CGFS;311;NA;NA;NA;NA\n" +
            "2010;Campagne CGFS;;20;20;1;1644;DICELAB;Dicentrarchus labrax;N;taxon;Vrac;5;1.06;1.06;Poids;kg;NA;;;;;kg;NA;;;;;kg;NA;;;;;kg;NA;;;;;kg;306;Longueur totale (LT) - individu - totale - Mesure au cm par un observateur;36.0;1;;cm;1.0;1;N;1.06;1.0;1.0;CAM-CGFS;311;NA;NA;NA;NA\n" +
            "2010;Campagne CGFS;;20;20;1;1644;DICELAB;Dicentrarchus labrax;N;taxon;Vrac;5;1.06;1.06;Poids;kg;NA;;;;;kg;NA;;;;;kg;NA;;;;;kg;NA;;;;;kg;306;Longueur totale (LT) - individu - totale - Mesure au cm par un observateur;38.0;2;;cm;1.0;1;N;1.06;1.0;1.0;CAM-CGFS;311;NA;NA;NA;NA\n" +
            "2010;Campagne CGFS;;20;20;1;1362;ENGRENC;Engraulis encrasicolus;N;taxon;Vrac;6;0.038;0.038;Poids;kg;NA;;;;;kg;NA;;;;;kg;NA;;;;;kg;NA;;;;;kg;307;Longueur totale (LT) - individu - totale - Mesure au 1/2 cm par un observateur;10.5;1;;cm;0.5;2;N;0.038;1.0;1.0;CAM-CGFS;311;NA;NA;NA;NA\n" +
            "2010;Campagne CGFS;;20;20;1;1362;ENGRENC;Engraulis encrasicolus;N;taxon;Vrac;6;0.038;0.038;Poids;kg;NA;;;;;kg;NA;;;;;kg;NA;;;;;kg;NA;;;;;kg;307;Longueur totale (LT) - individu - totale - Mesure au 1/2 cm par un observateur;12.5;2;;cm;0.5;1;N;0.038;1.0;1.0;CAM-CGFS;311;NA;NA;NA;NA\n" +
            "2010;Campagne CGFS;;20;20;1;1362;ENGRENC;Engraulis encrasicolus;N;taxon;Vrac;6;0.038;0.038;Poids;kg;NA;;;;;kg;NA;;;;;kg;NA;;;;;kg;NA;;;;;kg;307;Longueur totale (LT) - individu - totale - Mesure au 1/2 cm par un observateur;13.0;3;;cm;0.5;1;N;0.038;1.0;1.0;CAM-CGFS;311;NA;NA;NA;NA\n" +
            "2010;Campagne CGFS;;20;20;1;1986;;Limanda limanda;N;taxon;Vrac;7;0.66;0.66;Poids;kg;NA;;;;;kg;NA;;;;;kg;NA;;;;;kg;NA;;;;;kg;306;Longueur totale (LT) - individu - totale - Mesure au cm par un observateur;25.0;1;;cm;1.0;1;N;0.66;1.0;1.0;CAM-CGFS;311;NA;NA;NA;NA\n" +
            "2010;Campagne CGFS;;20;20;1;1986;;Limanda limanda;N;taxon;Vrac;7;0.66;0.66;Poids;kg;NA;;;;;kg;NA;;;;;kg;NA;;;;;kg;NA;;;;;kg;306;Longueur totale (LT) - individu - totale - Mesure au cm par un observateur;28.0;2;;cm;1.0;1;N;0.66;1.0;1.0;CAM-CGFS;311;NA;NA;NA;NA\n" +
            "2010;Campagne CGFS;;20;20;1;1986;;Limanda limanda;N;taxon;Vrac;7;0.66;0.66;Poids;kg;NA;;;;;kg;NA;;;;;kg;NA;;;;;kg;NA;;;;;kg;306;Longueur totale (LT) - individu - totale - Mesure au cm par un observateur;30.0;3;;cm;1.0;1;N;0.66;1.0;1.0;CAM-CGFS;311;NA;NA;NA;NA\n" +
            "2010;Campagne CGFS;;20;20;1;489;LOLIVUL;Loligo vulgaris;N;taxon;Vrac;8;0.28;0.28;Poids;kg;NA;;;;;kg;NA;;;;;kg;NA;;;;;kg;NA;;;;;kg;302;Longueur du manteau (LM) - individu - manteau - Mesure au cm par un observateur;9.0;1;;cm;1.0;1;N;0.28;1.0;1.0;CAM-CGFS;311;NA;NA;NA;NA\n" +
            "2010;Campagne CGFS;;20;20;1;489;LOLIVUL;Loligo vulgaris;N;taxon;Vrac;8;0.28;0.28;Poids;kg;NA;;;;;kg;NA;;;;;kg;NA;;;;;kg;NA;;;;;kg;302;Longueur du manteau (LM) - individu - manteau - Mesure au cm par un observateur;10.0;2;;cm;1.0;3;N;0.28;1.0;1.0;CAM-CGFS;311;NA;NA;NA;NA\n" +
            "2010;Campagne CGFS;;20;20;1;489;LOLIVUL;Loligo vulgaris;N;taxon;Vrac;8;0.28;0.28;Poids;kg;NA;;;;;kg;NA;;;;;kg;NA;;;;;kg;NA;;;;;kg;302;Longueur du manteau (LM) - individu - manteau - Mesure au cm par un observateur;11.0;3;;cm;1.0;2;N;0.28;1.0;1.0;CAM-CGFS;311;NA;NA;NA;NA\n" +
            "2010;Campagne CGFS;;20;20;1;1988;;Microstomus kitt;N;taxon;Vrac;9;0.152;0.152;Poids;kg;NA;;;;;kg;NA;;;;;kg;NA;;;;;kg;NA;;;;;kg;306;Longueur totale (LT) - individu - totale - Mesure au cm par un observateur;15.0;1;;cm;1.0;1;N;0.152;1.0;1.0;CAM-CGFS;311;NA;NA;NA;NA\n" +
            "2010;Campagne CGFS;;20;20;1;1988;;Microstomus kitt;N;taxon;Vrac;9;0.152;0.152;Poids;kg;NA;;;;;kg;NA;;;;;kg;NA;;;;;kg;NA;;;;;kg;306;Longueur totale (LT) - individu - totale - Mesure au cm par un observateur;18.0;2;;cm;1.0;1;N;0.152;1.0;1.0;CAM-CGFS;311;NA;NA;NA;NA\n" +
            "2010;Campagne CGFS;;20;20;1;1690;MULLSUR;Mullus surmuletus;N;taxon;Vrac;10;0.036;0.036;Poids;kg;NA;;;;;kg;NA;;;;;kg;NA;;;;;kg;NA;;;;;kg;306;Longueur totale (LT) - individu - totale - Mesure au cm par un observateur;11.0;1;;cm;1.0;1;N;0.036;1.0;1.0;CAM-CGFS;311;NA;NA;NA;NA\n" +
            "2010;Campagne CGFS;;20;20;1;1978;;Pleuronectes platessa;N;taxon;Vrac;11;0.852;0.852;Poids;kg;NA;;;;;kg;NA;;;;;kg;NA;;;;;kg;NA;;;;;kg;306;Longueur totale (LT) - individu - totale - Mesure au cm par un observateur;26.0;1;;cm;1.0;1;N;0.852;1.0;1.0;CAM-CGFS;311;NA;NA;NA;NA\n" +
            "2010;Campagne CGFS;;20;20;1;1978;;Pleuronectes platessa;N;taxon;Vrac;11;0.852;0.852;Poids;kg;NA;;;;;kg;NA;;;;;kg;NA;;;;;kg;NA;;;;;kg;306;Longueur totale (LT) - individu - totale - Mesure au cm par un observateur;30.0;2;;cm;1.0;1;N;0.852;1.0;1.0;CAM-CGFS;311;NA;NA;NA;NA\n" +
            "2010;Campagne CGFS;;20;20;1;1351;SARDPIL;Sardina pilchardus;N;taxon;Vrac;12;0.022;0.022;Poids;kg;NA;;;;;kg;NA;;;;;kg;NA;;;;;kg;NA;;;;;kg;307;Longueur totale (LT) - individu - totale - Mesure au 1/2 cm par un observateur;10.5;1;;cm;0.5;1;N;0.022;1.0;1.0;CAM-CGFS;311;NA;NA;NA;NA\n" +
            "2010;Campagne CGFS;;20;20;1;1772;SCOMSCO;Scomber scombrus;N;taxon;Vrac;13;0.18;0.18;Poids;kg;NA;;;;;kg;NA;;;;;kg;NA;;;;;kg;NA;;;;;kg;306;Longueur totale (LT) - individu - totale - Mesure au cm par un observateur;27.0;1;;cm;1.0;1;N;0.18;1.0;1.0;CAM-CGFS;311;NA;NA;NA;NA\n" +
            "2010;Campagne CGFS;;20;20;1;1242;SCYOCAN;Scyliorhinus canicula;N;taxon|categorie_individu;Vrac;14;;;Poids;kg;NA;;;;;kg;Femelle;1;1.0;1.0;Poids;kg;NA;;;;;kg;NA;;;;;kg;306;Longueur totale (LT) - individu - totale - Mesure au cm par un observateur;51.0;1;;cm;1.0;1;N;1.0;1.0;1.0;CAM-CGFS;311;NA;301;NA;NA\n" +
            "2010;Campagne CGFS;;20;20;1;1242;SCYOCAN;Scyliorhinus canicula;N;taxon|categorie_individu;Vrac;14;;;Poids;kg;NA;;;;;kg;Femelle;1;1.0;1.0;Poids;kg;NA;;;;;kg;NA;;;;;kg;306;Longueur totale (LT) - individu - totale - Mesure au cm par un observateur;55.0;2;;cm;1.0;1;N;1.0;1.0;1.0;CAM-CGFS;311;NA;301;NA;NA\n" +
            "2010;Campagne CGFS;;20;20;1;1662;TRACTRA;Trachurus trachurus;N;taxon|categorie_individu;Vrac;15;;;Poids;kg;P - Petit;1;17908.896;131.12;Poids;kg;NA;;;;;kg;NA;;;;;kg;NA;;;;;kg;306;Longueur totale (LT) - individu - totale - Mesure au cm par un observateur;8.0;1;;cm;1.0;1;N;131.12;136.58502;136.58403;CAM-CGFS;311;307;NA;NA;NA\n" +
            "2010;Campagne CGFS;;20;20;1;1662;TRACTRA;Trachurus trachurus;N;taxon|categorie_individu;Vrac;15;;;Poids;kg;P - Petit;1;17908.896;131.12;Poids;kg;NA;;;;;kg;NA;;;;;kg;NA;;;;;kg;306;Longueur totale (LT) - individu - totale - Mesure au cm par un observateur;9.0;2;;cm;1.0;20;N;131.12;136.58502;136.58403;CAM-CGFS;311;307;NA;NA;NA\n" +
            "2010;Campagne CGFS;;20;20;1;1662;TRACTRA;Trachurus trachurus;N;taxon|categorie_individu;Vrac;15;;;Poids;kg;P - Petit;1;17908.896;131.12;Poids;kg;NA;;;;;kg;NA;;;;;kg;NA;;;;;kg;306;Longueur totale (LT) - individu - totale - Mesure au cm par un observateur;10.0;3;;cm;1.0;89;N;131.12;136.58502;136.58403;CAM-CGFS;311;307;NA;NA;NA\n" +
            "2010;Campagne CGFS;;20;20;1;1662;TRACTRA;Trachurus trachurus;N;taxon|categorie_individu;Vrac;15;;;Poids;kg;P - Petit;1;17908.896;131.12;Poids;kg;NA;;;;;kg;NA;;;;;kg;NA;;;;;kg;306;Longueur totale (LT) - individu - totale - Mesure au cm par un observateur;11.0;4;;cm;1.0;5;N;131.12;136.58502;136.58403;CAM-CGFS;311;307;NA;NA;NA\n" +
            "2010;Campagne CGFS;;20;20;1;1662;TRACTRA;Trachurus trachurus;N;taxon|categorie_individu;Vrac;15;;;Poids;kg;G - Gros;2;0.13;0.13;Poids;kg;NA;;;;;kg;NA;;;;;kg;NA;;;;;kg;306;Longueur totale (LT) - individu - totale - Mesure au cm par un observateur;23.0;1;;cm;1.0;1;N;0.13;137761.75;1.0;CAM-CGFS;311;305;NA;NA;NA";

    public static final String MARINE_LITTER_CONTENT =
            "Annee;Serie;Serie_Partielle;Code_Station;Id_Operation;Poche;Categorie;Categorie_Taille;Nombre;Poids;Commentaire;Serie_Id;Categorie_Id;Categorie_Taille_Id\n" +
            "2013;Campagne CGFS;;A;1;1;L1 PLASTIQUE;A: <5*5 cm= 25 cm2;2;5.0;S1;CAM-CGFS;2119;2120\n" +
            "2013;Campagne CGFS;;A;1;1;L1a Sacs;B: <10*10 cm= 100 cm2;3;1.0;S2;CAM-CGFS;2126;2121";

    public static final String INDIVIDUAL_OBSERVATION_CONTENT =
            "Annee;Serie;Serie_Partielle;Code_Station;Id_Operation;Poche;Id_Lot;Code_Taxon;Nom_Scientifique;Commentaire;Code_PMFM;Libelle_PMFM;Valeur;Serie_Id;Valeur_Id\n" +
            "2013;Campagne CGFS;;A;1;1;100000;11242;Aaptos;P1;220;Poids - produit/lot - totale - Mesure par un observateur;0.1;CAM-CGFS;0.1\n" +
            "2013;Campagne CGFS;;A;1;1;100000;11242;Aaptos;P1;1433;ID du PSFM - données - sans objet - Organisation des données de campagnes;307;CAM-CGFS;307\n" +
            "2013;Campagne CGFS;;A;1;1;100000;11242;Aaptos;P1;307;Longueur totale (LT) - individu - totale - Mesure au 1/2 cm par un observateur;10.0;CAM-CGFS;10.0\n" +
            "2013;Campagne CGFS;;A;1;1;100000;11242;Aaptos;P1;1436;N° pièce calcifiée - individu - totale - Inconnue;10;CAM-CGFS;10\n" +
            "2013;Campagne CGFS;;A;1;1;100000;11242;Aaptos;P1;1435;Code du prélèvement - individu - totale - Inconnue;A20;CAM-CGFS;A20\n" +
            "2013;Campagne CGFS;;A;1;1;100000;11242;Aaptos;P1;101;Age - individu - otolithe - Lecture d'âge;10.0;CAM-CGFS;10.0\n" +
            "2013;Campagne CGFS;;A;1;1;100000;11242;Aaptos;P1;46;Algorithme de déterminaison - marée - totale - Estimation algorithme SACROIS;0L - 0 VMS - 1 LB;CAM-CGFS;168\n" +
            "2013;Campagne CGFS;;A;1;1;100000;11242;Aaptos;P1;1388;Caisse - produit/lot - totale - Observation par un observateur;5.0;CAM-CGFS;5.0";

    public static final String ACCIDENTAL_CATCH_CONTENT =
            "Annee;Serie;Serie_Partielle;Code_Station;Id_Operation;Poche;Id_Lot;Code_Taxon;Nom_Scientifique;Commentaire;Code_PMFM;Libelle_PMFM;Valeur;Serie_Id;Valeur_Id\n" +
            "2013;Campagne CGFS;;A;1;1;100001;3835;Abalistes;;1393;Rejet vivant ou mort ? - produit/lot - totale - Observation par un observateur;Rejet mort;CAM-CGFS;1769\n" +
            "2013;Campagne CGFS;;A;1;1;100001;3835;Abalistes;;196;Sexe - produit/lot - totale - Observation par un observateur;Femelle;CAM-CGFS;301\n" +
            "2013;Campagne CGFS;;A;1;1;100001;3835;Abalistes;;220;Poids - produit/lot - totale - Mesure par un observateur;10.0;CAM-CGFS;10.0\n" +
            "2013;Campagne CGFS;;A;1;1;100001;3835;Abalistes;;1433;ID du PSFM - données - sans objet - Organisation des données de campagnes;1425;CAM-CGFS;1425\n" +
            "2013;Campagne CGFS;;A;1;1;100001;3835;Abalistes;;1425;Longueur totale (LT) - individu - queue - Mesure au cm par un observateur;4.0;CAM-CGFS;4.0";

    public static final String SPECIES_CONTENT =
            "Code_Taxon;Code_Rubin;Nom_Scientifique;Code_Campagne\n" +
            "3835;ABAL;Abalistes;\n" +
            "11242;AAPT;Aaptos;\n" +
            "11243;AAPTAAP;Aaptos aaptos;";

    public static final String SPECIES_CONTENT_2 =
            "Code_Taxon;Code_Rubin;Nom_Scientifique;Code_Campagne\n" +
            "300;BUCCUND;Buccinum undatum;\n" +
            "365;AEQUOPE;Aequipecten opercularis;\n" +
            "489;LOLIVUL;Loligo vulgaris;LOLIVUL\n" +
            "491;ALLO;Alloteuthis;ALLOSPP\n" +
            "1242;SCYOCAN;Scyliorhinus canicula;SCYOCAN\n" +
            "1351;SARDPIL;Sardina pilchardus;SARDPIL\n" +
            "1362;ENGRENC;Engraulis encrasicolus;ENGRENC\n" +
            "1644;DICELAB;Dicentrarchus labrax;DICELAB\n" +
            "1662;TRACTRA;Trachurus trachurus;TRACTRA\n" +
            "1690;MULLSUR;Mullus surmuletus;MULLSUR\n" +
            "1772;SCOMSCO;Scomber scombrus;SCOMSCO\n" +
            "1811;CALMLYR;Callionymus lyra;CALMLYR\n" +
            "1978;PLEUPLA;Pleuronectes platessa;\n" +
            "1986;LIMDLIM;Limanda limanda;\n" +
            "1988;MICTKIT;Microstomus kitt;";

    public static final int NB_EXPECTED_CGFS_OPERATIONS = 106;

    protected GenericFormatExportService service;

    protected PersistenceService persistenceService;

    protected ServiceDbResource.DataContext dataContext;

    protected ProgressionModel progressionModel;

    protected File dataDirectory;

    protected File exportFile;

    protected GenericFormatArchive archive;

    @Before
    public void setUp() throws Exception {

        dataDirectory = new File(dbResource.getConfig().getDataDirectory(), "Export-" + System.nanoTime());

        java.nio.file.Files.createDirectory(dataDirectory.toPath());

        TuttiServiceContext serviceContext = dbResource.getServiceContext();

        persistenceService = serviceContext.getService(PersistenceService.class);

        dbResource.setCountryInConfig("12");
        dbResource.openDataContext();

        service = serviceContext.getService(GenericFormatExportService.class);

        dataContext = dbResource.loadContext(PROGRAM_ID, CRUISE_ID, 2, OPERATION_1_ID, OPERATION_2_ID);

        progressionModel = new ProgressionModel();
        progressionModel.setTotal(1);

        exportFile = new File(dataDirectory, "export.zip");

        archive = GenericFormatArchive.forExport(exportFile, dataDirectory);

    }


    @Ignore
    @Test
    public void exportProgram() throws Exception {

        Files.createParentDirs(exportFile);

        Assert.assertFalse(exportFile.exists());

        String programId = dataContext.program.getId();

        ProgramDataModel dataToExport = persistenceService.loadProgram(programId, true);

        GenericFormatExportConfiguration exportConfiguration = new GenericFormatExportConfiguration();
        exportConfiguration.setExportFile(exportFile);
        exportConfiguration.setExportAttachments(true);
        exportConfiguration.setExportSpecies(true);
        exportConfiguration.setExportBenthos(true);
        exportConfiguration.setExportMarineLitter(true);
        exportConfiguration.setExportAccidentalCatch(true);
        exportConfiguration.setExportIndividualObservation(true);
        exportConfiguration.setDataToExport(dataToExport);

        int nbSteps = service.getExportNbSteps(exportConfiguration);
        progressionModel.setTotal(nbSteps);

        service.export(exportConfiguration, progressionModel);
        Assert.assertTrue(exportFile.exists());
    }

    @Test
    public void exportCruise() throws Exception {

        Files.createParentDirs(exportFile);

        Assert.assertFalse(exportFile.exists());

        String programId = dataContext.program.getId();
        Integer cruiseId = dataContext.cruise.getIdAsInt();

        ProgramDataModel dataToExport = persistenceService.loadCruises(programId, true, cruiseId);

        GenericFormatExportConfiguration exportConfiguration = new GenericFormatExportConfiguration();
        exportConfiguration.setExportFile(exportFile);
        exportConfiguration.setExportAttachments(true);
        exportConfiguration.setExportSpecies(true);
        exportConfiguration.setExportBenthos(true);
        exportConfiguration.setExportMarineLitter(true);
        exportConfiguration.setExportAccidentalCatch(true);
        exportConfiguration.setExportIndividualObservation(true);
        exportConfiguration.setDataToExport(dataToExport);

        int nbSteps = service.getExportNbSteps(exportConfiguration);
        progressionModel.setTotal(nbSteps);

        service.export(exportConfiguration, progressionModel);
        Assert.assertTrue(exportFile.exists());
    }

//    @Test
//    public void exportSurvey() throws Exception {
//
//        try (GenericFormatExportContext exportContext = service.createExportContext(progressionModel, archive)) {
//            service.exportCruise(exportContext, dataContext.cruise, dataContext.operations);
//        }
//        ServiceDbResource.assertFileContent("Survey export:\n",
//                                            archive.getSurveyPath().toFile(),
//                                            SURVEY_CONTENT);
//    }
//
//    @Test
//    public void exportGearCaracteristics() throws Exception {
//
//        try (GenericFormatExportContext exportContext = service.createExportContext(progressionModel, archive)) {
//            service.exportGearCaracteristics(exportContext, dataContext.cruise);
//        }
//        ServiceDbResource.assertFileContent("Gear caracteristics export:\n",
//                                            archive.getGearCaracteristicsPath().toFile(),
//                                            GEAR_CARACTERISTICS_CONTENT);
//    }
//
//    @Test
//    public void exportOperations() throws Exception {
//
//        try (GenericFormatExportContext exportContext = service.createExportContext(progressionModel, archive)) {
//            service.exportCruise(exportContext, dataContext.cruise, dataContext.operations);
//        }
//        ServiceDbResource.assertFileContent("Operation export:\n",
//                                            archive.getOperationPath().toFile(),
//                                            OPERATION_CONTENT);
//    }
//
//    @Test
//    public void exportOperationsWithNoCatches() throws Exception {
//
//        dataContext = dbResource.loadContext(PROGRAM_ID, CRUISE_CGFS_ID, NB_EXPECTED_CGFS_OPERATIONS);
//
//        FishingOperation operation = TuttiEntities.findById(dataContext.operations, "100105");
//
//        try (GenericFormatExportContext exportContext = service.createExportContext(progressionModel, archive)) {
//            service.exportOperation(exportContext, dataContext.cruise, operation);
//        }
//
//        ServiceDbResource.assertFileContent("Operation export:\n",
//                                            archive.getOperationPath().toFile(),
//                                            OPERATION_WITH_NO_CATCH_CONTENT);
//    }
//
//    @Test
//    public void exportOperationsWithNoGear() throws Exception {
//
//        dataContext = dbResource.loadContext(PROGRAM_ID, CRUISE_CGFS_ID, NB_EXPECTED_CGFS_OPERATIONS);
//
//        FishingOperation operation = TuttiEntities.findById(dataContext.operations, "100105");
//        operation.setGear(null);
//
//        try (GenericFormatExportContext exportContext = service.createExportContext(progressionModel, archive)) {
//            service.exportOperation(exportContext, dataContext.cruise, operation);
//        }
//
//        ServiceDbResource.assertFileContent("Operation export:\n",
//                                            archive.getOperationPath().toFile(),
//                                            OPERATION_WITH_NO_CATCH_CONTENT_AND_NO_GEAR);
//    }
//
//    @Test
//    public void exportParameters() throws Exception {
//
//        try (GenericFormatExportContext exportContext = service.createExportContext(progressionModel, archive)) {
//            service.exportCruise(exportContext, dataContext.cruise, dataContext.operations);
//        }
//
//        ServiceDbResource.assertFileContent("Parameter export:\n",
//                                            archive.getParameterPath().toFile(),
//                                            PARAMETER_CONTENT);
//    }
//
//    @Test
//    public void exportCatches() throws Exception {
//
//        try (GenericFormatExportContext exportContext = service.createExportContext(progressionModel, archive)) {
//            service.exportCruise(exportContext, dataContext.cruise, dataContext.operations);
//        }
//        ServiceDbResource.assertFileContent("Catch export:\n",
//                                            archive.getCatchPath().toFile(),
//                                            CATCH_CONTENT);
//    }

    // To fix http://forge.codelutin.com/issues/2692
//    @Test
//    public void exportCatchesAndSpecies() throws Exception {
//
//        File protocolFile = new File("src/test/resources/tuttiProtocol.tuttiProtocol");
//
//        TuttiProtocol protocol = dbResource.loadProtocol(protocolFile);
//
//        persistenceService.setProtocol(protocol);
//
//        dataContext = dbResource.loadContext(PROGRAM_ID, CRUISE_CGFS_ID, NB_EXPECTED_CGFS_OPERATIONS);
//
//        FishingOperation operation = TuttiEntities.findById(dataContext.operations, "100000");
//
//        try (GenericFormatExportContext exportContext = service.createExportContext(progressionModel, archive)) {
//            service.exportOperation(exportContext, dataContext.cruise, operation);
//            service.exportSpecies(exportContext);
//        }
//
//        ServiceDbResource.assertFileContent("species export:\n",
//                                            archive.getSpeciesPath().toFile(),
//                                            SPECIES_CONTENT_2);
//
//        ServiceDbResource.assertFileContent("Catch export:\n",
//                                            archive.getCatchPath().toFile(),
//                                            CATCH_CONTENT_2);
//
//
//    }

    // To fix http://forge.codelutin.com/issues/2523
//    @Test
//    public void exportBadCatches() throws Exception {
//
//        dataContext = dbResource.loadContext(PROGRAM_ID, CRUISE_CGFS_ID, NB_EXPECTED_CGFS_OPERATIONS);
//
//        try (GenericFormatExportContext exportContext = service.createExportContext(progressionModel, archive)) {
//            service.exportCruise(exportContext, dataContext.cruise, dataContext.operations);
//        }
//    }
//
//    @Test
//    public void exportMarineLitters() throws Exception {
//
//        try (GenericFormatExportContext exportContext = service.createExportContext(progressionModel, archive)) {
//            service.exportCruise(exportContext, dataContext.cruise, dataContext.operations);
//        }
//        ServiceDbResource.assertFileContent("MarineLitter export:\n",
//                                            archive.getMarineLitterPath().toFile(),
//                                            MARINE_LITTER_CONTENT);
//    }
//
//    @Test
//    public void exportIndividualObservations() throws Exception {
//
//        try (GenericFormatExportContext exportContext = service.createExportContext(progressionModel, archive)) {
//            service.exportCruise(exportContext, dataContext.cruise, dataContext.operations);
//        }
//        ServiceDbResource.assertFileContent("individualObservation export:\n",
//                                            archive.getIndividualObservationPath().toFile(),
//                                            INDIVIDUAL_OBSERVATION_CONTENT);
//    }
//
//    @Test
//    public void exportAccidentalCatch() throws Exception {
//
//        try (GenericFormatExportContext exportContext = service.createExportContext(progressionModel, archive)) {
//            service.exportCruise(exportContext, dataContext.cruise, dataContext.operations);
//        }
//        ServiceDbResource.assertFileContent("accidentalCatch export:\n",
//                                            archive.getAccidentalCatchPath().toFile(),
//                                            ACCIDENTAL_CATCH_CONTENT);
//    }
//
//    @Test
//    public void exportSpecies() throws Exception {
//
//        try (GenericFormatExportContext exportContext = service.createExportContext(progressionModel, archive)) {
//            List<Species> allReferentSpecies =
//                    persistenceService.getAllReferentSpecies();
//
//            Map<String, Species> allReferentSpeciesById =
//                    TuttiEntities.splitById(allReferentSpecies);
//            exportContext.getProducerForSpecies().addSpecies(allReferentSpeciesById.get("35883"));
//            exportContext.getProducerForSpecies().addSpecies(allReferentSpeciesById.get("35884"));
//            exportContext.getProducerForSpecies().addSpecies(allReferentSpeciesById.get("19279"));
//
//            service.exportSpecies(exportContext);
//        }
//        ServiceDbResource.assertFileContent("species export:\n",
//                                            archive.getSpeciesPath().toFile(),
//                                            SPECIES_CONTENT);
//    }

}

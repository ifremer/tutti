/*
 * #%L
 * Tutti :: Service
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
/* ===================================================================
 * common.js
 * -------------------------------------------------------------------
 * 
 * -------------------------------------------------------------------       
 * @Author  :   
 * @Version :                      
 * =================================================================== */

/** parametres de connexion **/
//var cnxHOME="C:\\DevSih\\Allegro_campagnes\\exportCruise-100001";
//var cnxHOME="C:\\DevSih\\Allegro_campagnes\\exportCruise-CGFS2010\\exportCruise-100000";
var cnxDELIMTYPE="SEMICOLON";
var cnxCHARSET="UTF-8";
var cnxINCLCOLUMNNAME="YES";
var cnxINCLTYPELINE="NO";

/** parametres existance capture accident et macro dechets **/
var existsCapAcc=0;
var existsMacDech=0;

<?xml version="1.0" encoding="UTF-8"?>
<!--
  #%L
  Tutti :: Persistence API
  %%
  Copyright (C) 2012 - 2013 Ifremer
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation, either version 3 of the 
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public 
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/gpl-3.0.html>.
  #L%
  -->

<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
  <modelVersion>4.0.0</modelVersion>

  <parent>
    <groupId>fr.ifremer</groupId>
    <artifactId>tutti</artifactId>
    <version>4.6.2-SNAPSHOT</version>
  </parent>

  <groupId>fr.ifremer.tutti</groupId>
  <artifactId>tutti-persistence</artifactId>

  <name>Tutti :: Persistence</name>

  <dependencies>

    <!-- JAXX -->
    <dependency>
      <groupId>org.nuiton.jaxx</groupId>
      <artifactId>jaxx-application-api</artifactId>
    </dependency>

    <!-- Adagio -->
    <dependency>
      <groupId>fr.ifremer.adagio</groupId>
      <artifactId>adagio-core-shared</artifactId>
    </dependency>
    <dependency>
      <groupId>fr.ifremer.adagio</groupId>
      <artifactId>adagio-core-allegro</artifactId>
    </dependency>

    <!-- Spring -->
    <dependency>
      <groupId>org.springframework</groupId>
      <artifactId>spring-beans</artifactId>
    </dependency>
    <dependency>
      <groupId>org.springframework</groupId>
      <artifactId>spring-context</artifactId>
    </dependency>
    <dependency>
      <groupId>org.springframework</groupId>
      <artifactId>spring-jdbc</artifactId>
    </dependency>
    <dependency>
      <groupId>org.springframework</groupId>
      <artifactId>spring-core</artifactId>
    </dependency>
    <dependency>
      <groupId>org.springframework</groupId>
      <artifactId>spring-tx</artifactId>
    </dependency>

    <!-- Utilisé par adagio pour que les versions de spring sont syncho -->
    <dependency>
      <groupId>org.springframework</groupId>
      <artifactId>spring-context-support</artifactId>
    </dependency>
    <dependency>
      <groupId>org.hibernate</groupId>
      <artifactId>hibernate-core</artifactId>
    </dependency>

    <!-- Nuiton -->
    <dependency>
      <groupId>org.nuiton</groupId>
      <artifactId>nuiton-version</artifactId>
    </dependency>
    <dependency>
      <groupId>org.nuiton</groupId>
      <artifactId>nuiton-utils</artifactId>
    </dependency>
    <dependency>
      <groupId>org.nuiton</groupId>
      <artifactId>nuiton-config</artifactId>
    </dependency>
    <dependency>
      <groupId>org.nuiton</groupId>
      <artifactId>nuiton-decorator</artifactId>
    </dependency>
    <dependency>
      <groupId>org.nuiton</groupId>
      <artifactId>nuiton-updater</artifactId>
    </dependency>
    <dependency>
      <groupId>org.nuiton.i18n</groupId>
      <artifactId>nuiton-i18n</artifactId>
    </dependency>
    <dependency>
      <groupId>org.nuiton</groupId>
      <artifactId>nuiton-converter</artifactId>
    </dependency>

    <!-- Commons -->
    <dependency>
      <groupId>org.apache.commons</groupId>
      <artifactId>commons-lang3</artifactId>
    </dependency>
    <dependency>
      <groupId>commons-io</groupId>
      <artifactId>commons-io</artifactId>
    </dependency>
    <dependency>
      <groupId>commons-beanutils</groupId>
      <artifactId>commons-beanutils</artifactId>
    </dependency>
    <dependency>
      <groupId>org.apache.commons</groupId>
      <artifactId>commons-collections4</artifactId>
    </dependency>
    <dependency>
      <groupId>commons-logging</groupId>
      <artifactId>commons-logging</artifactId>
    </dependency>

    <!-- Guava -->
    <dependency>
      <groupId>com.google.guava</groupId>
      <artifactId>guava</artifactId>
    </dependency>

    <!-- Yaml -->
    <dependency>
      <groupId>com.esotericsoftware.yamlbeans</groupId>
      <artifactId>yamlbeans</artifactId>
    </dependency>

    <!-- jdbc drivers -->
    <!--dependency>
      <groupId>org.hsqldb</groupId>
      <artifactId>hsqldb</artifactId>
    </dependency-->
    <dependency>
      <groupId>hsqldb</groupId>
      <artifactId>hsqldb</artifactId>
    </dependency>

    <!--
     | use to trace all sql query. Config file is src/main/resources/spy.properties
     | to active it, uncomment dependency and change jdbc:hsqldb:... to jdbc:p6spy:hsqldb:...
     | if you don't change spy.properties, log file is /tmp/tutti-jdbc-spy.log
     | you can show most used query with:
     |   cat /tmp/tutti-jdbc-spy.log |grep "|select" |cut -f5 -d'|' |sort |uniq -c |sort -n
     | you can compute sum of time with:
     |   cat /tmp/tutti-jdbc-spy.log |grep "<put here your query find with previous command>" |cut -f2 -d'|' |paste -s -d+ |bc
     + -->
    <!--
    <dependency>
      <groupId>p6spy</groupId>
      <artifactId>p6spy</artifactId>
      <version>2.3.1</version>
    </dependency>
    -->
    
    <!-- Logging -->
    <dependency>
      <groupId>org.slf4j</groupId>
      <artifactId>slf4j-jcl</artifactId>
      <scope>runtime</scope>
    </dependency>

    <!-- Test -->
    <dependency>
      <groupId>junit</groupId>
      <artifactId>junit</artifactId>
      <scope>provided</scope>
    </dependency>
    <dependency>
      <groupId>org.springframework</groupId>
      <artifactId>spring-test</artifactId>
    </dependency>
    <dependency>
      <groupId>log4j</groupId>
      <artifactId>log4j</artifactId>
      <scope>test</scope>
    </dependency>

  </dependencies>

  <build>
    <pluginManagement>
      <plugins>
        <plugin>
          <artifactId>maven-surefire-plugin</artifactId>
          <configuration>
            <properties>
            <property>
              <name>listener</name>
              <value>fr.ifremer.tutti.persistence.test.TuttiRunListener</value>
            </property>
            </properties>
          </configuration>
        </plugin>
      </plugins>
    </pluginManagement>
    <plugins>

      <plugin>
        <groupId>org.nuiton.eugene</groupId>
        <artifactId>eugene-maven-plugin</artifactId>
        <configuration>
          <resolver>org.nuiton.util.FasterCachedResourceResolver</resolver>
          <templates>
            org.nuiton.eugene.java.JavaInterfaceTransformer,
            org.nuiton.eugene.java.SimpleJavaBeanTransformer,
            org.nuiton.eugene.java.JavaEnumerationTransformer
          </templates>
        </configuration>
        <executions>
          <execution>
            <phase>generate-sources</phase>
            <configuration>
              <defaultPackage>fr.ifremer.tutti.persistence.entities
              </defaultPackage>
              <fullPackagePath>fr.ifremer.tutti.persistence.entities
              </fullPackagePath>
              <inputs>zargo</inputs>
              <failIfUnsafe>true</failIfUnsafe>
            </configuration>
            <goals>
              <goal>generate</goal>
            </goals>
          </execution>
        </executions>
      </plugin>

      <plugin>
        <groupId>org.nuiton.i18n</groupId>
        <artifactId>i18n-maven-plugin</artifactId>
        <executions>
          <execution>
            <id>scan-sources</id>
            <goals>
              <goal>parserJava</goal>
              <goal>gen</goal>
            </goals>
          </execution>
        </executions>
      </plugin>

    </plugins>
  </build>

</project>

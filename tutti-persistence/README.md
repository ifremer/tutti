
Pour se connecter à la base
---------------------------

java -jar ~/.m2/repository/hsqldb/hsqldb/1.8.1.3/hsqldb-1.8.1.3.jar --inlineRC URL=jdbc:hsqldb:file:src/test/db/allegro,USER=sa


Pour lancer la base en mode serveur
-----------------------------------

(cd src/test ; ./startServer.sh)

On peut ensuite accéder à la base via l'url jdbc:hsqldb://localhost/allegro

Se connecter à une superbe ui swing de requétage
------------------------------------------------


java -classpath ~/.m2/repository/hsqldb/hsqldb/1.8.1.3/hsqldb-1.8.1.3.jar org.hsqldb.util.DatabaseManager

ou bien

(cd src/test ; ./startDbManager.sh)


package fr.ifremer.tutti.util;

/*
 * #%L
 * Tutti :: Persistence
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.junit.Assert;
import org.junit.Test;

public class NumbersTest {

    @Test
    public void testRoundToInt() throws Exception {

        Assert.assertEquals(1, Numbers.roundToInt(1f));
        Assert.assertEquals(1, Numbers.roundToInt(1.1f));
        Assert.assertEquals(1, Numbers.roundToInt(1.2f));
        Assert.assertEquals(1, Numbers.roundToInt(1.3f));
        Assert.assertEquals(1, Numbers.roundToInt(1.4f));
        Assert.assertEquals(1, Numbers.roundToInt(1.49f));
        Assert.assertEquals(1, Numbers.roundToInt(1.499f));
        Assert.assertEquals(2, Numbers.roundToInt(1.5f));
        Assert.assertEquals(2, Numbers.roundToInt(1.6f));
        Assert.assertEquals(2, Numbers.roundToInt(1.7f));
        Assert.assertEquals(2, Numbers.roundToInt(1.8f));
        Assert.assertEquals(2, Numbers.roundToInt(1.9f));
        Assert.assertEquals(2, Numbers.roundToInt(2f));

    }
}
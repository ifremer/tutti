package fr.ifremer.tutti.persistence.service;

/*
 * #%L
 * Tutti :: Persistence
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.entities.data.Program;
import fr.ifremer.tutti.persistence.test.DatabaseResource;
import org.junit.Assert;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Test;

import java.util.List;

/**
 * To test {@link ProgramPersistenceService} for read operation.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 0.3
 */
public class ProgramPersistenceServiceReadTest {

    @ClassRule
    public static final DatabaseResource dbResource = DatabaseResource.readDb();

    protected ProgramPersistenceService service;

    @Before
    public void setUp() throws Exception {
        service = TuttiPersistenceServiceLocator.getProgramPersistenceService();
    }

    @Test
    public void getAllProgram() {
        List<Program> result = service.getAllProgram();
        Assert.assertNotNull(result);
        Assert.assertTrue("More than one program must be found", result.size() > 0);
        for (Program program : result) {
            Assert.assertNotNull(program.getId());
            Assert.assertNotEquals("SIH-OBSMER", program.getId());
            Assert.assertNotEquals("SIH-ACTIFLOT", program.getId());
        }
    }

    @Test
    public void getProgram(/*String id*/) {
        String programCode = dbResource.getFixtures().programCode();
        String zoneId = dbResource.getFixtures().zoneId();
        Program actual = service.getProgram(programCode);
        Assert.assertNotNull(actual);
        Assert.assertNotNull(actual.getId());
        Assert.assertNotNull(actual.getName());
        Assert.assertNotNull(actual.getDescription());
        Assert.assertNotNull(actual.getZone());
        Assert.assertEquals(zoneId, actual.getZone().getId());
        Assert.assertEquals(programCode, actual.getId());
    }

}

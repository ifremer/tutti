package fr.ifremer.tutti.persistence.service;

/*
 * #%L
 * Tutti :: Persistence
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.entities.CaracteristicMap;
import fr.ifremer.tutti.persistence.entities.TuttiEntities;
import fr.ifremer.tutti.persistence.entities.data.AccidentalBatch;
import fr.ifremer.tutti.persistence.entities.data.Cruise;
import fr.ifremer.tutti.persistence.entities.data.FishingOperation;
import fr.ifremer.tutti.persistence.entities.referential.Caracteristic;
import fr.ifremer.tutti.persistence.service.referential.CaracteristicPersistenceService;
import fr.ifremer.tutti.persistence.test.DatabaseResource;
import org.apache.commons.collections4.CollectionUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Test;

import java.util.List;

/**
 * To test {@link AccidentalBatchPersistenceService} for write operation.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 0.3
 */
public class AccidentalBatchPersistenceServiceWriteTest {

    @ClassRule
    public static final DatabaseResource dbResource = DatabaseResource.writeDb();

    protected CaracteristicPersistenceService caracteristicService;

    protected AccidentalBatchPersistenceService service;

    protected FishingOperation fishingOperation;

    @Before
    public void setUp() throws Exception {
        caracteristicService = TuttiPersistenceServiceLocator.getCaracteristicPersistenceService();
        service = TuttiPersistenceServiceLocator.getAccidentalBatchPersistenceService();
        Cruise cruise = dbResource.getFixtures().createCruise();
        fishingOperation = dbResource.getFixtures().createFishingOperation(cruise);
    }

    @Test
    public void createAccidentalBatch(/*AccidentalBatch bean*/) {

        // -----------------------------------------------------------------------------
        // 1. Create with only mandatory properties
        // -----------------------------------------------------------------------------

        AccidentalBatch createdAccidentalBatch = dbResource.getFixtures().createMinimalAccidentalBatch(fishingOperation);

        Assert.assertNotNull(createdAccidentalBatch);
        Assert.assertNotNull(createdAccidentalBatch.getId());

        // reload it
        List<AccidentalBatch> allAccidentalBatch = service.getAllAccidentalBatch(fishingOperation.getIdAsInt());
        Assert.assertTrue(CollectionUtils.isNotEmpty(allAccidentalBatch));
        AccidentalBatch reloadedAccidentalBatch = allAccidentalBatch.get(0);
        reloadedAccidentalBatch.setFishingOperation(fishingOperation);
        assertEqualsAccidentalBatch(createdAccidentalBatch, reloadedAccidentalBatch);

        // -----------------------------------------------------------------------------
        // 2. Create a full with all properties
        // -----------------------------------------------------------------------------

        createdAccidentalBatch.setId((String) null);

        CaracteristicMap caracteristicMap = new CaracteristicMap();
        createdAccidentalBatch.setCaracteristics(caracteristicMap);

        // add a qualitative caracteristic
        Caracteristic maturityCaracteristic = caracteristicService.getMaturityCaracteristic();
        caracteristicMap.put(maturityCaracteristic, maturityCaracteristic.getQualitativeValue(0));

        // add a numeric caracteristic
        Caracteristic ageCaracteristic = caracteristicService.getAgeCaracteristic();
        caracteristicMap.put(ageCaracteristic, 10.f);

        // add a string caracteristic
        Caracteristic stringCaracteristic = caracteristicService.getCaracteristic(dbResource.getFixtures().refAlphanumericPmfmId());
        caracteristicMap.put(stringCaracteristic, "Un texte!");

        createdAccidentalBatch.setComment("AccidentalBatch-full");
        createdAccidentalBatch.setDeadOrAlive(caracteristicService.getDeadOrAliveCaracteristic().getQualitativeValue(0));
        createdAccidentalBatch.setGender(caracteristicService.getSexCaracteristic().getQualitativeValue(0));

        createdAccidentalBatch.setLengthStepCaracteristic(caracteristicService.getCaracteristic(dbResource.getFixtures().refNumericalPmfmId()));
        createdAccidentalBatch.setSize(10.0f);

        createdAccidentalBatch.setWeight(5.f);

        AccidentalBatch createdAccidentalBatch2 =
                service.createAccidentalBatch(createdAccidentalBatch);

        assertEqualsAccidentalBatch(createdAccidentalBatch, createdAccidentalBatch2);

        Assert.assertNotNull(createdAccidentalBatch2);
        Assert.assertNotNull(createdAccidentalBatch2.getId());
        assertEqualsAccidentalBatch(createdAccidentalBatch, createdAccidentalBatch2);

        // reload it
        allAccidentalBatch = service.getAllAccidentalBatch(fishingOperation.getIdAsInt());
        Assert.assertTrue(CollectionUtils.isNotEmpty(allAccidentalBatch));
        Assert.assertEquals(2, allAccidentalBatch.size());
        AccidentalBatch reloadedAccidentalBatch2 = TuttiEntities.splitById(allAccidentalBatch).get(createdAccidentalBatch2.getId());
        reloadedAccidentalBatch2.setFishingOperation(fishingOperation);
        assertEqualsAccidentalBatch(createdAccidentalBatch2, reloadedAccidentalBatch2);

        // -----------------------------------------------------------------------------
        // 2. Modify some properties and save
        // -----------------------------------------------------------------------------
        reloadedAccidentalBatch2.setComment("Comment2");
        reloadedAccidentalBatch2.setSize(15f);
        reloadedAccidentalBatch2.setWeight(35f);
        reloadedAccidentalBatch2.setDeadOrAlive(caracteristicService.getDeadOrAliveCaracteristic().getQualitativeValue(1));
        reloadedAccidentalBatch2.setGender(caracteristicService.getSexCaracteristic().getQualitativeValue(1));
        reloadedAccidentalBatch2.getCaracteristics().clear();

        AccidentalBatch savedAccidentalBatch = service.saveAccidentalBatch(reloadedAccidentalBatch2);
        assertEqualsAccidentalBatch(reloadedAccidentalBatch2, savedAccidentalBatch);

        allAccidentalBatch = service.getAllAccidentalBatch(fishingOperation.getIdAsInt());
        Assert.assertTrue(CollectionUtils.isNotEmpty(allAccidentalBatch));
        AccidentalBatch reloadedAccidentalBatch3 = TuttiEntities.splitById(allAccidentalBatch).get(createdAccidentalBatch2.getId());
        reloadedAccidentalBatch3.setFishingOperation(fishingOperation);
        assertEqualsAccidentalBatch(savedAccidentalBatch, reloadedAccidentalBatch3);
    }

    @Test
    public void deleteAccidentalBatch(/*String fishingOperationId*/) {

        AccidentalBatch createdAccidentalBatch =
                dbResource.getFixtures().createMinimalAccidentalBatch(
                        fishingOperation);

        List<AccidentalBatch> allAccidentalBatch = service.getAllAccidentalBatch(fishingOperation.getIdAsInt());
        Assert.assertFalse(CollectionUtils.isEmpty(allAccidentalBatch));

        service.deleteAccidentalBatch(createdAccidentalBatch.getId());

        List<AccidentalBatch> allAccidentalBatch2 = service.getAllAccidentalBatch(fishingOperation.getIdAsInt());
        Assert.assertTrue(CollectionUtils.isEmpty(allAccidentalBatch2));
    }

    @Test
    public void deleteAccidentalBatchForFishingOperation(/*String id*/) {

        dbResource.getFixtures().createMinimalAccidentalBatch(
                fishingOperation);

        dbResource.getFixtures().createMinimalAccidentalBatch(
                fishingOperation);

        List<AccidentalBatch> allAccidentalBatch = service.getAllAccidentalBatch(fishingOperation.getIdAsInt());
        Assert.assertFalse(CollectionUtils.isEmpty(allAccidentalBatch));

        service.deleteAccidentalBatchForFishingOperation(fishingOperation.getIdAsInt());

        List<AccidentalBatch> allAccidentalBatch2 = service.getAllAccidentalBatch(fishingOperation.getIdAsInt());
        Assert.assertTrue(CollectionUtils.isEmpty(allAccidentalBatch2));
    }

    protected void assertEqualsAccidentalBatch(AccidentalBatch expected,
                                               AccidentalBatch actual) {
        Assert.assertEquals(expected, actual);
        Assert.assertEquals(expected.getId(), actual.getId());
        Assert.assertEquals(expected.getSize(), actual.getSize());
        Assert.assertEquals(expected.getComment(), actual.getComment());
        Assert.assertEquals(expected.getDeadOrAlive(), actual.getDeadOrAlive());
        Assert.assertEquals(expected.getGender(), actual.getGender());
        Assert.assertEquals(expected.getLengthStepCaracteristic(), actual.getLengthStepCaracteristic());
        Assert.assertEquals(expected.getWeight(), actual.getWeight());
        Assert.assertEquals(expected.getCaracteristics(), actual.getCaracteristics());
        Assert.assertEquals(expected.getFishingOperation(), actual.getFishingOperation());
    }
}

package fr.ifremer.tutti.persistence.service;

/*
 * #%L
 * Tutti :: Persistence
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.InvalidBatchModelException;
import fr.ifremer.tutti.persistence.entities.data.FishingOperation;
import fr.ifremer.tutti.persistence.test.DatabaseResource;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Ignore;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

/**
 * To test {@link CatchBatchPersistenceService} for read operation.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.0
 */
@Ignore
public class CatchBatchPersistenceServiceReadTest {

    @ClassRule
    public static final DatabaseResource dbResource = DatabaseResource.writeDb();

    protected CatchBatchPersistenceService service;

    protected FishingOperationPersistenceService fishingOperationService;

    protected FishingOperation fishingOperation;

    @Before
    public void setUp() throws Exception {
        service = TuttiPersistenceServiceLocator.getCatchBatchPersistenceService();
        fishingOperationService = TuttiPersistenceServiceLocator.getFishingOperationPersistenceService();

        List<FishingOperation> fishingOperations = fishingOperationService.getAllFishingOperation(dbResource.getFixtures().cruiseId());
        assertNotNull(fishingOperations);
        assertTrue(fishingOperations.size() > 0);
        fishingOperation = fishingOperations.get(0);
        assertNotNull(fishingOperation);
        assertNotNull(fishingOperation.getId());
    }

    @Test(expected = InvalidBatchModelException.class)
    public void getCatchBatchFromFishingOperation() throws Exception {

        //TODO-TC Change test when data will be Tutti-aware
        // old CGFS tree structure is still not valid
        service.getCatchBatchFromFishingOperation(fishingOperation.getIdAsInt());
    }

}

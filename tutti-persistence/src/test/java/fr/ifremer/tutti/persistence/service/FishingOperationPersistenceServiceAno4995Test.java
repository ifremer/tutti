package fr.ifremer.tutti.persistence.service;

/*
 * #%L
 * Tutti :: Persistence
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.entities.data.Cruise;
import fr.ifremer.tutti.persistence.entities.data.FishingOperation;
import fr.ifremer.tutti.persistence.test.DatabaseResource;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Test;

import static org.junit.Assert.assertNotNull;

/**
 * To fix http://forge.codelutin.com/issues/4995.
 *
 * Created on 4/25/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.5
 */
public class FishingOperationPersistenceServiceAno4995Test {

    @ClassRule
    public static final DatabaseResource dbResource = DatabaseResource.writeDb("dbCGFS_ANO4995");

    protected FishingOperationPersistenceService service;

    protected FishingOperation fishingOperation;

    @Before
    public void setUp() throws Exception {
        service = TuttiPersistenceServiceLocator.getFishingOperationPersistenceService();

        // get fishing operation
        Cruise cruise = TuttiPersistenceServiceLocator.getCruisePersistenceService().getCruise(0);
        fishingOperation = service.getFishingOperation(0);
        fishingOperation.setCruise(cruise);
    }

    @Test
    public void saveFishingOperation() {

        assertNotNull(fishingOperation);
        assertNotNull(fishingOperation.getCruise());
        assertNotNull(fishingOperation.getStrata());
        assertNotNull(fishingOperation.getLocation());

        // modifiy it
        fishingOperation.setStrata(null);

        // save it
        service.saveFishingOperation(fishingOperation);
    }
}

package fr.ifremer.tutti.persistence.service;

/*
 * #%L
 * Tutti :: Persistence
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.entities.CaracteristicMap;
import fr.ifremer.tutti.persistence.entities.data.Cruise;
import fr.ifremer.tutti.persistence.entities.data.FishingOperation;
import fr.ifremer.tutti.persistence.entities.data.FishingOperations;
import fr.ifremer.tutti.persistence.entities.referential.Caracteristic;
import fr.ifremer.tutti.persistence.entities.referential.CaracteristicType;
import fr.ifremer.tutti.persistence.entities.referential.Gear;
import fr.ifremer.tutti.persistence.entities.referential.TuttiLocation;
import fr.ifremer.tutti.persistence.entities.referential.TuttiLocations;
import fr.ifremer.tutti.persistence.entities.referential.Vessel;
import fr.ifremer.tutti.persistence.service.referential.CaracteristicPersistenceService;
import fr.ifremer.tutti.persistence.service.referential.GearPersistenceService;
import fr.ifremer.tutti.persistence.service.referential.VesselPersistenceService;
import fr.ifremer.tutti.persistence.test.DatabaseResource;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Assert;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.DataRetrievalFailureException;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

/**
 * To test {@link FishingOperationPersistenceService} for write operation.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.0
 */
public class FishingOperationPersistenceServiceWriteTest {

    @ClassRule
    public static final DatabaseResource dbResource = DatabaseResource.writeDb();

    /** Logger. */
    private static final Log log =
            LogFactory.getLog(FishingOperationPersistenceServiceWriteTest.class);

    protected FishingOperationPersistenceService service;

    protected CatchBatchPersistenceService catchBatchPersistenceService;

    protected AccidentalBatchPersistenceService accidentalBatchPersistenceService;

    protected IndividualObservationBatchPersistenceService individualObservationBatchPersistenceService;

    private CaracteristicPersistenceService caracteristicService;

    private GearPersistenceService gearService;

    protected Cruise cruise = null;

    protected Gear cruiseGear = null;

    protected Vessel notCruiseVessel = null;

    @Before
    public void setUp() throws Exception {
        service = TuttiPersistenceServiceLocator.getFishingOperationPersistenceService();
        caracteristicService = TuttiPersistenceServiceLocator.getCaracteristicPersistenceService();
        gearService = TuttiPersistenceServiceLocator.getGearPersistenceService();


        catchBatchPersistenceService = TuttiPersistenceServiceLocator.getCatchBatchPersistenceService();
        accidentalBatchPersistenceService = TuttiPersistenceServiceLocator.getAccidentalBatchPersistenceService();
        individualObservationBatchPersistenceService = TuttiPersistenceServiceLocator.getIndividualObservationBatchPersistenceService();

        cruise = dbResource.getFixtures().createCruise();
        Assert.assertFalse(cruise.getGear().isEmpty());
        cruiseGear = cruise.getGear().get(0);

        // Find a vessel, different from the cruise vessel
        VesselPersistenceService vesselService = TuttiPersistenceServiceLocator.getVesselPersistenceService();
        notCruiseVessel = vesselService.getVessel(dbResource.getFixtures().fishingVesselCode());
        assertNotNull(notCruiseVessel.getId());
    }

    @Test
    public void createFishingOperation(/*FishingOperation bean*/) {
        // -----------------------------------------------------------------------------
        // Prepare data and other entities
        // -----------------------------------------------------------------------------
        Calendar calendar = new GregorianCalendar();
        FishingOperation reloadedFishingOperation;
        FishingOperation createdFishingOperation;

        // Retrieve some environment caracteristics
        List<Caracteristic> allEnvironmentCaracteristics = caracteristicService.getAllCaracteristic();
        CaracteristicMap environmentCaracteristics = new CaracteristicMap();
        CaracteristicMap environmentValuesOneEntry = new CaracteristicMap();
        for (Caracteristic caracteristic : allEnvironmentCaracteristics) {
            Serializable value = null;
            if (caracteristic.getCaracteristicType() == CaracteristicType.NUMBER) {
                value = 1.0f;
            } else if (caracteristic.getCaracteristicType() == CaracteristicType.TEXT) {
                value = "some text";
            } else if (caracteristic.getCaracteristicType() == CaracteristicType.QUALITATIVE
                       && caracteristic.getQualitativeValue(0) != null) {
                // Choose the first qualitative value
                value = caracteristic.getQualitativeValue(0);
            }
            if (value != null) {
                environmentCaracteristics.put(caracteristic, value);
                if (environmentValuesOneEntry.size() == 0) {
                    environmentValuesOneEntry.put(caracteristic, value);
                }
            }
        }

        // Retrieve some gear use caracteristics
        List<Caracteristic> allGearShootingCaracteristics = caracteristicService.getAllCaracteristic();
        CaracteristicMap gearShootingCaracteristics = new CaracteristicMap();
        CaracteristicMap gearShootingCaracteristicsOneEntry = new CaracteristicMap();
        for (Caracteristic caracteristic : allGearShootingCaracteristics) {
            Serializable value = null;
            if (caracteristic.getCaracteristicType() == CaracteristicType.NUMBER) {
                value = 1.0f;
            } else if (caracteristic.getCaracteristicType() == CaracteristicType.TEXT) {
                value = "some text";
            } else if (caracteristic.getCaracteristicType() == CaracteristicType.QUALITATIVE
                       && caracteristic.getQualitativeValue(0) != null) {
                // Choose the first qualitative value
                value = caracteristic.getQualitativeValue(0);
            }
            if (value != null) {
                gearShootingCaracteristics.put(caracteristic, value);
                if (gearShootingCaracteristicsOneEntry.size() == 0) {
                    gearShootingCaracteristicsOneEntry.put(caracteristic, value);
                }
            }
        }

        // Create new fishing operation :
        FishingOperation fishingOperation = FishingOperations.newFishingOperation();

        // -----------------------------------------------------------------------------
        // 1. Test with only mandatory properties
        // -----------------------------------------------------------------------------

        // Set properties (with optional value to null)
        fishingOperation.setCruise(cruise);
        fishingOperation.setStationNumber("STA1");
        fishingOperation.setFishingOperationNumber(1);
        fishingOperation.setMultirigAggregation("1");
        fishingOperation.setGearShootingStartDate(null);
        fishingOperation.setGearShootingEndDate(null);
        fishingOperation.setGear(null);
        fishingOperation.setVessel(null);
        fishingOperation.setGearShootingStartLatitude(33.2541f);
        fishingOperation.setComment(null);

        // Store fishing operation into database :
        createdFishingOperation = service.createFishingOperation(fishingOperation);
        assertNotNull("Fishing operation ID must not be null after creation in database", createdFishingOperation);
        assertNotNull(createdFishingOperation.getId());

        // Trying to reload this fishing operation
        reloadedFishingOperation = service.getFishingOperation(createdFishingOperation.getIdAsInt());
        assertNotNull(reloadedFishingOperation);
        assertNull(reloadedFishingOperation.getGearShootingStartDate());

        // -----------------------------------------------------------------------------
        // 2. Test with all properties set
        // -----------------------------------------------------------------------------
        // Set properties
        fishingOperation.setId((String) null);
        fishingOperation.setStationNumber("STA2");
        fishingOperation.setFishingOperationNumber(2);
        fishingOperation.setMultirigAggregation("1");
        calendar.setTime(new Date());
        calendar.set(Calendar.HOUR_OF_DAY, 1);
        calendar.set(Calendar.MILLISECOND, 99);
        fishingOperation.setGearShootingStartDate(calendar.getTime());
        calendar.setTime(new Date());
        calendar.set(Calendar.HOUR_OF_DAY, 10);
        calendar.set(Calendar.MILLISECOND, 99);
        fishingOperation.setGearShootingEndDate(calendar.getTime());
        fishingOperation.setGearShootingStartLatitude(47.6f);
        fishingOperation.setGearShootingStartLongitude(-5.05f);
        fishingOperation.setGearShootingEndLatitude(47.9854f);
        fishingOperation.setGearShootingEndLongitude(-5.597f);

        fishingOperation.setTrawlDistance(100);
        fishingOperation.setFishingOperationRectiligne(true);
        fishingOperation.setFishingOperationValid(Boolean.TRUE);
        fishingOperation.setComment("Unit test createFishingOperation() - Part n°2 : All properties set");
        fishingOperation.setGear(cruiseGear);
        fishingOperation.setVessel(notCruiseVessel);
        fishingOperation.setVesselUseFeatures(environmentCaracteristics);
        fishingOperation.setGearUseFeatures(gearShootingCaracteristics);

        TuttiLocation strata = TuttiLocations.newTuttiLocation();
        strata.setId(dbResource.getFixtures().strataId());
        fishingOperation.setStrata(strata);
        TuttiLocation subStrata = TuttiLocations.newTuttiLocation();
        subStrata.setId(dbResource.getFixtures().subStrataId());
        fishingOperation.setSubStrata(subStrata);
        TuttiLocation localite = TuttiLocations.newTuttiLocation();
        localite.setId(dbResource.getFixtures().localite());
        fishingOperation.setLocation(localite);

        // Store fishing operation into database :
        createdFishingOperation = service.createFishingOperation(fishingOperation);
        assertNotNull("Fishing operation ID must not be null after creation in database", createdFishingOperation);
        assertNotNull(createdFishingOperation.getId());

        reloadedFishingOperation = service.getFishingOperation(createdFishingOperation.getIdAsInt());
        assertNotNull(reloadedFishingOperation.getGear());
        assertNotNull(reloadedFishingOperation.getVessel());
        assertEquals(fishingOperation.getVessel().getId(), reloadedFishingOperation.getVessel().getId());
        assertEquals(fishingOperation.getStationNumber(), reloadedFishingOperation.getStationNumber());
        assertEquals(fishingOperation.getFishingOperationNumber(), reloadedFishingOperation.getFishingOperationNumber());
        assertEquals(fishingOperation.getMultirigAggregation(), reloadedFishingOperation.getMultirigAggregation());
        assertNotNull(reloadedFishingOperation.getGearShootingStartDate());
        calendar.setTime(fishingOperation.getGearShootingStartDate());
        calendar.set(Calendar.MILLISECOND, 0);
        assertEquals(calendar.getTime(), reloadedFishingOperation.getGearShootingStartDate());
        assertEquals(fishingOperation.getGearShootingStartLatitude(), reloadedFishingOperation.getGearShootingStartLatitude());
        assertEquals(fishingOperation.getGearShootingStartLongitude(), reloadedFishingOperation.getGearShootingStartLongitude());
        assertNotNull(reloadedFishingOperation.getGearShootingEndDate());
        calendar.setTime(fishingOperation.getGearShootingEndDate());
        calendar.set(Calendar.MILLISECOND, 0);
        assertEquals(calendar.getTime(), reloadedFishingOperation.getGearShootingEndDate());
        assertEquals(fishingOperation.getGearShootingEndLatitude(), reloadedFishingOperation.getGearShootingEndLatitude());
        assertEquals(fishingOperation.getGearShootingEndLongitude(), reloadedFishingOperation.getGearShootingEndLongitude());
        assertEquals(fishingOperation.getTrawlDistance(), reloadedFishingOperation.getTrawlDistance());
        assertEquals(fishingOperation.isFishingOperationRectiligne(), reloadedFishingOperation.isFishingOperationRectiligne());
        assertEquals(fishingOperation.getFishingOperationValid(), reloadedFishingOperation.getFishingOperationValid());
        assertEquals(fishingOperation.getComment(), reloadedFishingOperation.getComment());
        assertNotNull(reloadedFishingOperation.getVesselUseFeatures());
        assertEquals(environmentCaracteristics.size(), fishingOperation.getVesselUseFeatures().size());
        assertNotNull(reloadedFishingOperation.getGearUseFeatures());
        assertEquals(gearShootingCaracteristics.size(), reloadedFishingOperation.getGearUseFeatures().size());
        assertNotNull(reloadedFishingOperation.getStrata());
        assertNotNull(reloadedFishingOperation.getStrata().getId());
        assertEquals(fishingOperation.getStrata().getId(), reloadedFishingOperation.getStrata().getId());
        assertNotNull(reloadedFishingOperation.getSubStrata());
        assertNotNull(reloadedFishingOperation.getSubStrata().getId());
        assertEquals(fishingOperation.getSubStrata().getId(), reloadedFishingOperation.getSubStrata().getId());
        assertNotNull(reloadedFishingOperation.getLocation());
        assertNotNull(reloadedFishingOperation.getLocation().getId());
        assertEquals(fishingOperation.getLocation().getId(), reloadedFishingOperation.getLocation().getId());

        //FIXME-TC Benoit fix this please
        // -----------------------------------------------------------------------------
        // 3. Test :
        //    - startDate and startLat filled, but empty endLong
        //    - endLat and endLong filled but empty endDate
        //    - isFishingOperationRectiligne = false
        //    - trawlDistance empty
        //    - fishingOperationValid = false
        // -----------------------------------------------------------------------------
//        fishingOperation.setId(null);
//        fishingOperation.setStationNumber("STA3");
//        fishingOperation.setFishingOperationNumber(3);
//        fishingOperation.setMultirigAggregation("2");
//        fishingOperation.setGearShootingStartLongitude(null);
//        fishingOperation.setGearShootingEndDate(null);
//        fishingOperation.setTrawlDistance(null);
//        fishingOperation.setFishingOperationRectiligne(false);
//        fishingOperation.setFishingOperationValid(false);
//        fishingOperation.setComment("Unit test createFishingOperation() - Part n°3 :\n-startDate and startLat filled, but empty endLong\n- endLat and endLong filled but empty endDate");
//        createdFishingOperation = service.createFishingOperation(fishingOperation);
//        reloadedFishingOperation = service.getFishingOperation(createdFishingOperation.getId());
//        assertEquals(fishingOperation.getGearShootingStartLatitude(), reloadedFishingOperation.getGearShootingStartLatitude());
//        assertEquals(fishingOperation.getGearShootingStartLongitude(), reloadedFishingOperation.getGearShootingStartLongitude());
//        assertNull(reloadedFishingOperation.getGearShootingEndDate());
//        assertEquals(fishingOperation.getGearShootingEndLatitude(), reloadedFishingOperation.getGearShootingEndLatitude());
//        assertEquals(fishingOperation.getGearShootingEndLongitude(), reloadedFishingOperation.getGearShootingEndLongitude());
//        assertEquals(fishingOperation.getTrawlDistance(), reloadedFishingOperation.getTrawlDistance());
//        assertEquals(fishingOperation.isFishingOperationRectiligne(), reloadedFishingOperation.isFishingOperationRectiligne());
//        assertEquals(fishingOperation.getFishingOperationValid(), reloadedFishingOperation.getFishingOperationValid());

        // -----------------------------------------------------------------------------
        // 4. Test exceptions :
        //    - try to save a operation using a gear not declared in the cruise
        //    - try to save a operation using a multirig aggregation greater than the cruise multirig number
        // -----------------------------------------------------------------------------
        fishingOperation.setId((String) null);

        // Find and set a gear not used in the cruise
        List<Gear> gears = gearService.getAllScientificGear();
        assertNotNull(gears);
        assertTrue(gears.size() > 0);
        for (Gear gear : gears) {
            if (!cruiseGear.getId().equals(gear.getId())) {
                fishingOperation.setGear(gear);
                break;
            }
        }

        try {
            createdFishingOperation = service.createFishingOperation(fishingOperation);
            fail("A fishing operation must not be saved if the gear is not declared in the cruise.");
        } catch (DataIntegrityViolationException dive) {
            assertNotNull(dive);
            fishingOperation.setGear(cruiseGear);
        }

        fishingOperation.setMultirigAggregation("3");
        try {
            createdFishingOperation = service.createFishingOperation(fishingOperation);
            fail("A fishing operation must not be saved if the 'multirig aggregation' > 'cruise multirig number'.");
        } catch (DataIntegrityViolationException dive) {
            assertNotNull(dive);
            fishingOperation.setMultirigAggregation("1");
        }

        // -----------------------------------------------------------------------------
        // 5. Test update (delete unecessary data)
        //    - remove positons
        //    - remove strata, substrata, localite
        //    - remove some environment carateristics
        //    - remove some gear shooting carateristics
        //    - change vessel (to same as cruise)
        // -----------------------------------------------------------------------------
        fishingOperation.setId(reloadedFishingOperation.getId());
        fishingOperation.setGearShootingStartLatitude(null);
        fishingOperation.setGearShootingStartLongitude(null);
        fishingOperation.setGearShootingEndDate(null);
        fishingOperation.setGearShootingEndLatitude(null);
        fishingOperation.setGearShootingEndLongitude(null);
        fishingOperation.setTrawlDistance(null);
        fishingOperation.setStrata(null);
        fishingOperation.setSubStrata(null);
        fishingOperation.setLocation(null);
        fishingOperation.setVessel(cruise.getVessel());
        fishingOperation.setVesselUseFeatures(environmentValuesOneEntry);
        fishingOperation.setGearUseFeatures(gearShootingCaracteristicsOneEntry);
        fishingOperation.setComment(fishingOperation.getComment() + "\n\nUnit test createFishingOperation() - Part n°5 : check if deleted sub items in DB");
        createdFishingOperation = service.saveFishingOperation(fishingOperation);
        reloadedFishingOperation = service.getFishingOperation(createdFishingOperation.getIdAsInt());
        assertNull(reloadedFishingOperation.getGearShootingStartLatitude());
        assertNull(reloadedFishingOperation.getGearShootingStartLongitude());
        assertNull(reloadedFishingOperation.getGearShootingEndDate());
        assertNull(reloadedFishingOperation.getGearShootingEndLatitude());
        assertNull(reloadedFishingOperation.getGearShootingEndLongitude());
        assertNull(reloadedFishingOperation.getStrata());
        assertNull(reloadedFishingOperation.getSubStrata());
        assertNull(reloadedFishingOperation.getLocation());
        assertNotNull(reloadedFishingOperation.getVesselUseFeatures());
        assertEquals(environmentValuesOneEntry.size(), reloadedFishingOperation.getVesselUseFeatures().size());
        assertNotNull(reloadedFishingOperation.getGearUseFeatures());
        assertEquals(gearShootingCaracteristicsOneEntry.size(), reloadedFishingOperation.getGearUseFeatures().size());
    }

    @Test
    @Ignore
    public void saveFishingOperation(/*FishingOperation bean*/) {

    }

    @Test
    public void deleteFishingOperation(/*String id*/) {

        FishingOperation fishingOperation = dbResource.getFixtures().createFishingOperation(cruise);

        dbResource.getFixtures().createMinimalCatchBatch(fishingOperation);
        dbResource.getFixtures().createMinimalAccidentalBatch(fishingOperation);

        Integer fishingOperationId = fishingOperation.getIdAsInt();
        Assert.assertFalse(CollectionUtils.isEmpty(service.getAllFishingOperation(cruise.getIdAsInt())));
        Assert.assertNotNull(catchBatchPersistenceService.getCatchBatchFromFishingOperation(fishingOperationId));
        Assert.assertFalse(CollectionUtils.isEmpty(accidentalBatchPersistenceService.getAllAccidentalBatch(fishingOperationId)));
        Assert.assertFalse(CollectionUtils.isEmpty(individualObservationBatchPersistenceService.getAllIndividualObservationBatchsForFishingOperation(fishingOperationId)));

        if (log.isInfoEnabled()) {
            log.info("\n\n\nWill delete fishing operation: " + fishingOperationId);
        }

        service.deleteFishingOperation(fishingOperationId);

        Assert.assertTrue(CollectionUtils.isEmpty(service.getAllFishingOperation(cruise.getIdAsInt())));
        try {
            catchBatchPersistenceService.getCatchBatchFromFishingOperation(fishingOperationId);
            Assert.fail();
        } catch (DataRetrievalFailureException e) {
            // feel good, no catch batch associated with fishing operation
        }
        try {
            individualObservationBatchPersistenceService.getAllIndividualObservationBatchsForFishingOperation(fishingOperationId);
            Assert.fail();
        } catch (DataRetrievalFailureException e) {
            // feel good, no catch batch associated with fishing operation
        }
        Assert.assertTrue(CollectionUtils.isEmpty(accidentalBatchPersistenceService.getAllAccidentalBatch(fishingOperationId)));
    }

}

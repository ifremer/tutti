package fr.ifremer.tutti.persistence.config;

/*
 * #%L
 * Tutti :: Persistence
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Lists;
import fr.ifremer.tutti.TuttiConfiguration;
import fr.ifremer.tutti.persistence.entities.data.SampleCategoryModel;
import fr.ifremer.tutti.persistence.entities.data.SampleCategoryModelEntry;
import fr.ifremer.tutti.persistence.test.DatabaseResource;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestName;
import org.nuiton.config.ApplicationConfig;
import org.nuiton.config.ArgumentsParserException;
import org.nuiton.util.FileUtil;
import org.nuiton.converter.ConverterUtil;

import java.io.File;

/**
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.4
 */
public class TuttiPersistenceConfigTest {

    public static final long TIMESTAMP = System.nanoTime();

    @Rule
    public final TestName name = new TestName();

    protected File datadirectory;

    @Before
    public void setUp() throws Exception {

        ConverterUtil.deregister();
        ConverterUtil.initConverters();

        datadirectory = FileUtil.getTestSpecificDirectory(getClass(),
                                                          name.getMethodName(),
                                                          null,
                                                          TIMESTAMP);
    }

    @Test
    public void getDefaultSampleCategoryModel() throws Exception {

        File configFile = new File(datadirectory, "empty.properties");

        Assert.assertFalse(configFile.exists());

        TuttiConfiguration config = getConfig(configFile);

        SampleCategoryModelEntry vracHVrac = new SampleCategoryModelEntry();
        vracHVrac.setCategoryId(1428);
        vracHVrac.setCode("V_HV");
        vracHVrac.setLabel("V/HV");
        vracHVrac.setOrder(0);

        SampleCategoryModelEntry classDeTri = new SampleCategoryModelEntry();
        classDeTri.setCategoryId(198);
        classDeTri.setCode("Class_Tri");
        classDeTri.setLabel("Class Tri");
        classDeTri.setOrder(1);

        SampleCategoryModelEntry sex = new SampleCategoryModelEntry();
        sex.setCategoryId(196);
        sex.setCode("Sexe");
        sex.setLabel("Sexe");
        sex.setOrder(2);

        SampleCategoryModelEntry maturity = new SampleCategoryModelEntry();
        maturity.setCategoryId(174);
        maturity.setCode("Maturite");
        maturity.setLabel("Maturite");
        maturity.setOrder(3);

        SampleCategoryModelEntry age = new SampleCategoryModelEntry();
        age.setCategoryId(1430);
        age.setCode("Age");
        age.setLabel("Age");
        age.setOrder(4);

        SampleCategoryModel model = config.getSampleCategoryModel();
        Assert.assertNotNull(model);
        Assert.assertNotNull(model.getCategory());
        Assert.assertEquals(5, model.getCategory().size());

        assertModelEntry(vracHVrac, model.getCategoryByIndex(0));
        assertModelEntry(classDeTri, model.getCategoryByIndex(1));
        assertModelEntry(sex, model.getCategoryByIndex(2));
        assertModelEntry(maturity, model.getCategoryByIndex(3));
        assertModelEntry(age, model.getCategoryByIndex(4));
    }


    @Test
    public void getSampleCategoryModel() throws Exception {

        File configFile = new File(datadirectory, "empty.properties");

        Assert.assertFalse(configFile.exists());

        TuttiConfiguration config = getConfig(configFile);


        SampleCategoryModelEntry e = new SampleCategoryModelEntry();
        e.setCategoryId(1);
        e.setCode("Label1");
        e.setLabel("Label1");
        e.setOrder(0);

        SampleCategoryModelEntry e2 = new SampleCategoryModelEntry();
        e2.setCategoryId(2);
        e2.setCode("Label2");
        e2.setLabel("Label2");
        e2.setOrder(1);

        SampleCategoryModel model = new SampleCategoryModel(Lists.newArrayList(e, e2));
        config.setSampleCategoryModel(model);

        SampleCategoryModel model2 = config.getSampleCategoryModel();
        Assert.assertNotNull(model2);
        Assert.assertNotNull(model2.getCategory());
        Assert.assertEquals(2, model2.getCategory().size());

        assertModelEntry(e, model2.getCategoryByIndex(0));
        assertModelEntry(e2, model2.getCategoryByIndex(1));

        config.getApplicationConfig().save(configFile, false);

        Assert.assertTrue(configFile.exists());

        TuttiConfiguration config2 = getConfig(configFile);
        model2 = config2.getSampleCategoryModel();
        Assert.assertNotNull(model2);
        Assert.assertNotNull(model2.getCategory());
        Assert.assertEquals(2, model2.getCategory().size());

        assertModelEntry(e, model2.getCategoryByIndex(0));
        assertModelEntry(e2, model2.getCategoryByIndex(1));

        model2.getCategory().remove(1);

        config.setSampleCategoryModel(model2);

        model2 = config2.getSampleCategoryModel();
        Assert.assertNotNull(model2);
        Assert.assertNotNull(model2.getCategory());
        Assert.assertEquals(1, model2.getCategory().size());

        assertModelEntry(e, model2.getCategoryByIndex(0));
    }

    protected void assertModelEntry(SampleCategoryModelEntry expected, SampleCategoryModelEntry actual) {
        Assert.assertEquals(expected.getCategoryId(), actual.getCategoryId());
        Assert.assertEquals(expected.getCode(), actual.getCode());
        Assert.assertEquals(expected.getLabel(), actual.getLabel());
        Assert.assertEquals(expected.getOrder(), actual.getOrder());

    }

    protected TuttiConfiguration getConfig(File configFile) throws ArgumentsParserException {
        ApplicationConfig applicationConfig = new ApplicationConfig(configFile.getAbsolutePath());

        DatabaseResource databaseResource = DatabaseResource.noDb();
        databaseResource.prepareConfig(applicationConfig, datadirectory);

        applicationConfig.parse();

        return new TuttiConfiguration(applicationConfig);
    }
}

package fr.ifremer.tutti.persistence.entities.protocol;

/*
 * #%L
 * Tutti :: Persistence
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Charsets;
import com.google.common.collect.Lists;
import com.google.common.io.Files;
import org.apache.commons.lang3.reflect.FieldUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestName;
import org.nuiton.util.FileUtil;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.List;

/**
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.0
 */
public class TuttiProtocolsTest {

    public static final String PROTOCOL_FILE_CONTENT =
            "id: 1\n" +
            "name: protocolName\n" +
            "benthos: \n" +
            "- !SpeciesProtocol\n" +
            "   id: 1\n" +
            "   lengthStepPmfmId: 1394\n" +
            "   mandatorySampleCategoryId: \n" +
            "   - 174\n" +
            "   - 196\n" +
            "   speciesReferenceTaxonId: 11242\n" +
            "   speciesSurveyCode: BAR\n" +
            "   weightEnabled: true\n" +
            "- !SpeciesProtocol\n" +
            "   id: 2\n" +
            "   lengthStepPmfmId: 323\n" +
            "   mandatorySampleCategoryId: \n" +
            "   - 1430\n" +
            "   - 174\n" +
            "   - 196\n" +
            "   - 198\n" +
            "   speciesReferenceTaxonId: 3835\n" +
            "   speciesSurveyCode: CHIN\n" +
            "   weightEnabled: true\n" +
            "caracteristicMapping: \n" +
            "- !CaracteristicMappingRow\n" +
            "   pmfmId: 114\n" +
            "   tab: VESSEL_USE_FEATURE\n" +
            "- !CaracteristicMappingRow\n" +
            "   pmfmId: 228\n" +
            "   tab: VESSEL_USE_FEATURE\n" +
            "- !CaracteristicMappingRow\n" +
            "   pmfmId: 821\n" +
            "   tab: VESSEL_USE_FEATURE\n" +
            "- !CaracteristicMappingRow\n" +
            "   pmfmId: 21\n" +
            "   tab: GEAR_USE_FEATURE\n" +
            "- !CaracteristicMappingRow\n" +
            "   pmfmId: 22\n" +
            "   tab: GEAR_USE_FEATURE\n" +
            "comment: Commentaire\n" +
            "lengthClassesPmfmId: \n" +
            "- 14\n" +
            "- 18\n" +
            "species: \n" +
            "- !SpeciesProtocol\n" +
            "   id: 1\n" +
            "   lengthStepPmfmId: 1394\n" +
            "   mandatorySampleCategoryId: \n" +
            "   - 174\n" +
            "   - 196\n" +
            "   speciesReferenceTaxonId: 11242\n" +
            "   speciesSurveyCode: BAR\n" +
            "   weightEnabled: true\n" +
            "- !SpeciesProtocol\n" +
            "   id: 2\n" +
            "   lengthStepPmfmId: 323\n" +
            "   mandatorySampleCategoryId: \n" +
            "   - 1430\n" +
            "   - 174\n" +
            "   - 196\n" +
            "   - 198\n" +
            "   speciesReferenceTaxonId: 3835\n" +
            "   speciesSurveyCode: CHIN\n" +
            "   weightEnabled: true\n" +
            "version: " + TuttiProtocols.CURRENT_PROTOCOL_VERSION;

    public static final int VRAC_HORS_VRAC = 1428;

    public static final int SIZE = 198;

    public static final int SEX = 196;

    public static final int MATURITY = 174;

    public static final int AGE = 1430;

    public static final long TIMESTAMP = System.nanoTime();

    @Rule
    public final TestName name = new TestName();

    protected File datadirectory;

    @Before
    public void setUp() throws Exception {

        datadirectory = FileUtil.getTestSpecificDirectory(getClass(),
                                                          name.getMethodName(),
                                                          null,
                                                          TIMESTAMP);
    }

    @Test
    public void toFile() throws IOException {

        File file = new File(datadirectory, "exportProtocol.yaml");

        Files.createParentDirs(file);

        Assert.assertFalse(file.exists());
        TuttiProtocol protocol = createProtocolFixture();

        TuttiProtocols.toFile(protocol, file);
        Assert.assertTrue(file.exists());

        String exportFileToString = Files.toString(file, Charsets.UTF_8).trim();
        Assert.assertEquals(PROTOCOL_FILE_CONTENT, exportFileToString);
    }

    protected void setField(Object o, String fieldName, Object value) throws IllegalAccessException {
        Field field = FieldUtils.getField(o.getClass(), fieldName);
//        field.setAccessible(true);
        FieldUtils.writeField(field, o, value, true);
    }

    @Test
    public void fromFile() throws Exception {

        File file = new File(datadirectory, "importProtocol.yaml");

        Files.createParentDirs(file);

        Files.write(PROTOCOL_FILE_CONTENT, file, Charsets.UTF_8);

//        TuttiEnumerationFile enumeration = new TuttiEnumerationFile();
//
//        setField(enumeration, "PMFM_ID_SORTED_UNSORTED", VRAC_HORS_VRAC);
//        setField(enumeration, "PMFM_ID_SIZE_CATEGORY", SIZE);
//        setField(enumeration, "PMFM_ID_SEX", SEX);
//        setField(enumeration, "PMFM_ID_MATURITY", MATURITY);
//        setField(enumeration, "PMFM_ID_AGE", AGE);

        TuttiProtocol protocol = TuttiProtocols.fromFile(file);

        Assert.assertNotNull(protocol);
        Assert.assertEquals("1", protocol.getId());
        Assert.assertEquals("protocolName", protocol.getName());
        Assert.assertEquals("Commentaire", protocol.getComment());
        Assert.assertEquals(Lists.newArrayList("14", "18"), protocol.getLengthClassesPmfmId());
        Assert.assertEquals(Lists.newArrayList(
                                    TuttiProtocols.caracteristicMappingRow("114", CaracteristicType.VESSEL_USE_FEATURE),
                                    TuttiProtocols.caracteristicMappingRow("228", CaracteristicType.VESSEL_USE_FEATURE),
                                    TuttiProtocols.caracteristicMappingRow("821", CaracteristicType.VESSEL_USE_FEATURE),
                                    TuttiProtocols.caracteristicMappingRow("21", CaracteristicType.GEAR_USE_FEATURE),
                                    TuttiProtocols.caracteristicMappingRow("22", CaracteristicType.GEAR_USE_FEATURE)),
                            protocol.getCaracteristicMapping());

        Assert.assertNotNull(protocol.getSpecies());
        Assert.assertEquals(2, protocol.getSpecies().size());
        SpeciesProtocol sp1 = protocol.getSpecies().get(0);
        Assert.assertNotNull(sp1);
        Assert.assertEquals("1", sp1.getId());
        Assert.assertEquals(11242, sp1.getSpeciesReferenceTaxonId(), 0);
        Assert.assertEquals("BAR", sp1.getSpeciesSurveyCode());
        Assert.assertEquals("1394", sp1.getLengthStepPmfmId());
        Assert.assertFalse(sp1.containsMandatorySampleCategoryId(AGE));
        Assert.assertTrue(sp1.containsMandatorySampleCategoryId(MATURITY));
        Assert.assertTrue(sp1.containsMandatorySampleCategoryId(SEX));
        Assert.assertFalse(sp1.containsMandatorySampleCategoryId(SIZE));
        Assert.assertTrue(sp1.isWeightEnabled());

        SpeciesProtocol sp2 = protocol.getSpecies().get(1);
        Assert.assertNotNull(sp2);
        Assert.assertEquals("2", sp2.getId());
        Assert.assertEquals(3835, sp2.getSpeciesReferenceTaxonId(), 0);
        Assert.assertEquals("CHIN", sp2.getSpeciesSurveyCode());
        Assert.assertEquals("323", sp2.getLengthStepPmfmId());
        Assert.assertTrue(sp2.containsMandatorySampleCategoryId(AGE));
        Assert.assertTrue(sp2.containsMandatorySampleCategoryId(MATURITY));
        Assert.assertTrue(sp2.containsMandatorySampleCategoryId(SEX));
        Assert.assertTrue(sp2.containsMandatorySampleCategoryId(SIZE));
        Assert.assertTrue(sp2.isWeightEnabled());

        Assert.assertNotNull(protocol.getBenthos());
        Assert.assertEquals(2, protocol.getBenthos().size());
        SpeciesProtocol b1 = protocol.getBenthos().get(0);
        Assert.assertNotNull(b1);
        Assert.assertEquals("1", b1.getId());
        Assert.assertEquals(11242, b1.getSpeciesReferenceTaxonId(), 0);
        Assert.assertEquals("BAR", b1.getSpeciesSurveyCode());
        Assert.assertEquals("1394", b1.getLengthStepPmfmId());
        Assert.assertFalse(b1.containsMandatorySampleCategoryId(AGE));
        Assert.assertTrue(b1.containsMandatorySampleCategoryId(MATURITY));
        Assert.assertTrue(b1.containsMandatorySampleCategoryId(SEX));
        Assert.assertFalse(b1.containsMandatorySampleCategoryId(SIZE));
        Assert.assertTrue(b1.isWeightEnabled());

        SpeciesProtocol b2 = protocol.getBenthos().get(1);
        Assert.assertNotNull(b2);
        Assert.assertEquals("2", b2.getId());
        Assert.assertEquals(3835, b2.getSpeciesReferenceTaxonId(), 0);
        Assert.assertEquals("CHIN", b2.getSpeciesSurveyCode());
        Assert.assertEquals("323", b2.getLengthStepPmfmId());
        Assert.assertTrue(b2.containsMandatorySampleCategoryId(AGE));
        Assert.assertTrue(b2.containsMandatorySampleCategoryId(MATURITY));
        Assert.assertTrue(b2.containsMandatorySampleCategoryId(SEX));
        Assert.assertTrue(b2.containsMandatorySampleCategoryId(SIZE));
        Assert.assertTrue(b2.isWeightEnabled());
    }

    private TuttiProtocol createProtocolFixture() {
        TuttiProtocol protocol = TuttiProtocols.newTuttiProtocol();
        protocol.setVersion(TuttiProtocols.CURRENT_PROTOCOL_VERSION);
        protocol.setId("1");
        protocol.setName("protocolName");
        protocol.setComment("Commentaire");
        protocol.setLengthClassesPmfmId(Lists.newArrayList("14", "18"));

        protocol.setCaracteristicMapping(Lists.newArrayList(
                TuttiProtocols.caracteristicMappingRow("114", CaracteristicType.VESSEL_USE_FEATURE),
                TuttiProtocols.caracteristicMappingRow("228", CaracteristicType.VESSEL_USE_FEATURE),
                TuttiProtocols.caracteristicMappingRow("821", CaracteristicType.VESSEL_USE_FEATURE),
                TuttiProtocols.caracteristicMappingRow("21", CaracteristicType.GEAR_USE_FEATURE),
                TuttiProtocols.caracteristicMappingRow("22", CaracteristicType.GEAR_USE_FEATURE)));

        protocol.setSpecies(Lists.<SpeciesProtocol>newArrayList());
        SpeciesProtocol sp1 = SpeciesProtocols.newSpeciesProtocol();
        sp1.setMandatorySampleCategoryId(Lists.<Integer>newArrayList());
        sp1.setId("1");
        sp1.setSpeciesReferenceTaxonId(11242);
        sp1.setSpeciesSurveyCode("BAR");
        sp1.setLengthStepPmfmId("1394");
        sp1.addMandatorySampleCategoryId(MATURITY);
        sp1.addMandatorySampleCategoryId(SEX);
        sp1.setWeightEnabled(true);
        protocol.addSpecies(sp1);

        SpeciesProtocol sp2 = SpeciesProtocols.newSpeciesProtocol();
        sp2.setMandatorySampleCategoryId(Lists.<Integer>newArrayList());
        sp2.setId("2");
        sp2.setSpeciesReferenceTaxonId(3835);
        sp2.setSpeciesSurveyCode("CHIN");
        sp2.setLengthStepPmfmId("323");
        sp2.addMandatorySampleCategoryId(AGE);
        sp2.addMandatorySampleCategoryId(MATURITY);
        sp2.addMandatorySampleCategoryId(SEX);
        sp2.addMandatorySampleCategoryId(SIZE);
        sp2.setWeightEnabled(true);
        protocol.addSpecies(sp2);

        protocol.setBenthos(Lists.<SpeciesProtocol>newArrayList());
        SpeciesProtocol b1 = SpeciesProtocols.newSpeciesProtocol();
        b1.setMandatorySampleCategoryId(Lists.<Integer>newArrayList());
        b1.setId("1");
        b1.setSpeciesReferenceTaxonId(11242);
        b1.setSpeciesSurveyCode("BAR");
        b1.setLengthStepPmfmId("1394");
        b1.addMandatorySampleCategoryId(MATURITY);
        b1.addMandatorySampleCategoryId(SEX);
        b1.setWeightEnabled(true);
        protocol.addBenthos(b1);

        SpeciesProtocol b2 = SpeciesProtocols.newSpeciesProtocol();
        b2.setMandatorySampleCategoryId(Lists.<Integer>newArrayList());
        b2.setId("2");
        b2.setSpeciesReferenceTaxonId(3835);
        b2.setSpeciesSurveyCode("CHIN");
        b2.setLengthStepPmfmId("323");
        b2.addMandatorySampleCategoryId(AGE);
        b2.addMandatorySampleCategoryId(MATURITY);
        b2.addMandatorySampleCategoryId(SEX);
        b2.addMandatorySampleCategoryId(SIZE);
        b2.setWeightEnabled(true);
        protocol.addBenthos(b2);

        return protocol;
    }

    @Test
    public void testGetFirstAvailableName() {

        List<String> names = Lists.newArrayList("a", "b", "c", "a-0", "a-1", "b-1");

        Assert.assertEquals("a-2", TuttiProtocols.getFirstAvailableName("a", names));
        Assert.assertEquals("b-0", TuttiProtocols.getFirstAvailableName("b", names));
        Assert.assertEquals("c-0", TuttiProtocols.getFirstAvailableName("c", names));

    }
}

package fr.ifremer.tutti.persistence.service.referential;

/*
 * #%L
 * Tutti :: Persistence
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.entities.referential.TuttiLocation;
import fr.ifremer.tutti.persistence.service.TuttiPersistenceServiceLocator;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

public class LocationPersistenceServiceReadTest extends ReferentialPersistenceServiceReadTestSupport {

    protected LocationPersistenceService service;

    @Before
    public void setUp() throws Exception {
        service = TuttiPersistenceServiceLocator.getLocationPersistenceService();
        super.setUp();
    }

    @Test
    public void getAllProgramZone() {
        List<TuttiLocation> result = service.getAllProgramZone();
        assertResultList(result, fixtures.refNbProgramZone());
    }

    @Test
    public void getAllCountry() {
        List<TuttiLocation> result = service.getAllCountry();
        assertResultList(result, fixtures.refNbCountry());
    }

    @Test
    public void getAllHarbour() {
        List<TuttiLocation> result = service.getAllHarbour();
        assertResultList(result, fixtures.refNbHarbour());
    }

    @Test
    public void getAllFishingOperationStrata(/*String zoneId*/) {
        String zoneId = dbResource.getFixtures().zoneId();
        List<TuttiLocation> result =
                service.getAllFishingOperationStrata(zoneId);
        assertResultList(result, fixtures.refNbStrata());
    }

    @Test
    public void getAllFishingOperationSubStrata(/*String zoneId, String strataId*/) {
        String zoneId = dbResource.getFixtures().zoneId();
        List<TuttiLocation> result =
                service.getAllFishingOperationSubStrata(zoneId, null);
        assertResultList(result, 0);

        // try with a strataId
        String strataId = dbResource.getFixtures().strataId();
        result = service.getAllFishingOperationSubStrata(zoneId, strataId);
        assertResultList(result, 0);
    }

    @Test
    public void getAllFishingOperationLocation(/*String zoneId, String strataId, String subStrataId*/) {
        String zoneId = dbResource.getFixtures().zoneId();
        List<TuttiLocation> result =
                service.getAllFishingOperationLocation(zoneId, null, null);
        assertResultList(result, fixtures.refNbLocalite());

        // try with a strataId
        String strataId = dbResource.getFixtures().strataId();
        result = service.getAllFishingOperationLocation(zoneId, strataId, null);
        assertResultList(result, 1);

        // try with a subStrataId
        String subStrataId = dbResource.getFixtures().subStrataId();
        result = service.getAllFishingOperationLocation(zoneId, strataId, subStrataId);
        assertResultList(result, 3);
    }

}
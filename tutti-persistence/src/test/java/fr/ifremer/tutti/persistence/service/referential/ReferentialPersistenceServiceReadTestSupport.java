package fr.ifremer.tutti.persistence.service.referential;

/*
 * #%L
 * Tutti :: Persistence
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.adagio.core.service.technical.CacheService;
import fr.ifremer.tutti.persistence.entities.TuttiEntities;
import fr.ifremer.tutti.persistence.entities.TuttiEntity;
import fr.ifremer.tutti.persistence.service.TuttiPersistenceServiceLocator;
import fr.ifremer.tutti.persistence.test.DatabaseFixtures;
import fr.ifremer.tutti.persistence.test.DatabaseResource;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.ClassRule;

import java.io.IOException;
import java.util.List;

public abstract class ReferentialPersistenceServiceReadTestSupport {


    @ClassRule
    public static final DatabaseResource dbResource = DatabaseResource.readDb();

    protected CacheService cacheService;

    protected DatabaseFixtures fixtures;

    @BeforeClass
    public static void beforeClass() throws IOException {

        TuttiPersistenceServiceLocator.instance().getCacheService().clearAllCaches();
    }

    @Before
    public void setUp() throws Exception {
        cacheService = TuttiPersistenceServiceLocator.instance().getCacheService();

        // This is need for test : getAllFishingVessel()
//        cacheService.clearAllCaches();
        fixtures = dbResource.getFixtures();
    }


    protected void assertSize(List<?> expectedList, List<?> storageList) {
        Assert.assertNotNull(expectedList);
        Assert.assertNotNull(storageList);
        expectedList.removeAll(storageList);
        Assert.assertTrue("Some " + expectedList.size() + " entities were not persisted in storage :" +
                          expectedList, expectedList.isEmpty());

    }

    protected <E extends TuttiEntity> void assertResultList(List<E> result,
                                                            int expectedsize) {

        // result not null
        Assert.assertNotNull(result);

        // id are unique
        TuttiEntities.splitById(result);

        // correct size
        Assert.assertEquals(expectedsize, result.size());
    }
}
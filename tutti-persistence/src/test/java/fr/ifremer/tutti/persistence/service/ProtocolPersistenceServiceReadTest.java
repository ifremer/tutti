package fr.ifremer.tutti.persistence.service;

/*
 * #%L
 * Tutti :: Persistence
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Lists;
import fr.ifremer.tutti.persistence.entities.protocol.SpeciesProtocol;
import fr.ifremer.tutti.persistence.entities.protocol.SpeciesProtocols;
import fr.ifremer.tutti.persistence.entities.protocol.TuttiProtocol;
import fr.ifremer.tutti.persistence.entities.protocol.TuttiProtocols;
import fr.ifremer.tutti.persistence.test.DatabaseResource;
import fr.ifremer.tutti.persistence.entities.protocol.CaracteristicType;
import org.junit.Assert;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Test;

import java.util.List;

/**
 * To test {@link ProtocolPersistenceService} for read operations.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 0.3
 */
public class ProtocolPersistenceServiceReadTest {

    @ClassRule
    public static final DatabaseResource dbResource = DatabaseResource.readDb();

    protected ProtocolPersistenceService service;

    public static final int VRAC_HORS_VRAC = 1428;

    public static final int SIZE = 198;

    public static final int SEX = 196;

    public static final int MATURITY = 174;

    public static final int AGE = 1430;

    @Before
    public void setUp() throws Exception {
        service = TuttiPersistenceServiceLocator.getProtocolPersistenceService();
    }

    @Test
    public void getAllProtocol() {

        List<TuttiProtocol> allProtocol = service.getAllProtocol();
        Assert.assertTrue(allProtocol.isEmpty());

        TuttiProtocol protocol = createProtocolFixture();
        TuttiProtocol createdProtocol = service.createProtocol(protocol);
        allProtocol = service.getAllProtocol();
        Assert.assertFalse(allProtocol.isEmpty());
        Assert.assertEquals(createdProtocol, allProtocol.get(0));
    }

    @Test
    public void getProtocol(/*String id*/) {
        TuttiProtocol protocol = createProtocolFixture();
        TuttiProtocol createdProtocol = service.createProtocol(protocol);
        Assert.assertNotNull(createdProtocol);
        String id = createdProtocol.getId();
        TuttiProtocol loadedProtocol = service.getProtocol(id);
        Assert.assertNotNull(loadedProtocol);
    }

    protected static TuttiProtocol createProtocolFixture() {
        TuttiProtocol protocol = TuttiProtocols.newTuttiProtocol();
        protocol.setVersion(TuttiProtocols.CURRENT_PROTOCOL_VERSION);
        protocol.setId("1");
        protocol.setName("protocolName");
        protocol.setComment("Commentaire");
        protocol.setLengthClassesPmfmId(Lists.newArrayList("14", "18"));

        protocol.setCaracteristicMapping(Lists.newArrayList(
                TuttiProtocols.caracteristicMappingRow("114", CaracteristicType.VESSEL_USE_FEATURE),
                TuttiProtocols.caracteristicMappingRow("228", CaracteristicType.VESSEL_USE_FEATURE),
                TuttiProtocols.caracteristicMappingRow("821", CaracteristicType.VESSEL_USE_FEATURE),
                TuttiProtocols.caracteristicMappingRow("21", CaracteristicType.GEAR_USE_FEATURE),
                TuttiProtocols.caracteristicMappingRow("22", CaracteristicType.GEAR_USE_FEATURE)));

        protocol.setSpecies(Lists.<SpeciesProtocol>newArrayList());
        SpeciesProtocol sp1 = SpeciesProtocols.newSpeciesProtocol();
        sp1.setMandatorySampleCategoryId(Lists.<Integer>newArrayList());
        sp1.setId("1");
        sp1.setSpeciesReferenceTaxonId(11242);
        sp1.setSpeciesSurveyCode("BAR");
        sp1.setLengthStepPmfmId("1394");
        sp1.addMandatorySampleCategoryId(MATURITY);
        sp1.addMandatorySampleCategoryId(SEX);
        sp1.setWeightEnabled(true);
        protocol.addSpecies(sp1);

        SpeciesProtocol sp2 = SpeciesProtocols.newSpeciesProtocol();
        sp2.setMandatorySampleCategoryId(Lists.<Integer>newArrayList());
        sp2.setId("2");
        sp2.setSpeciesReferenceTaxonId(3835);
        sp2.setSpeciesSurveyCode("CHIN");
        sp2.setLengthStepPmfmId("323");
        sp2.addMandatorySampleCategoryId(AGE);
        sp2.addMandatorySampleCategoryId(MATURITY);
        sp2.addMandatorySampleCategoryId(SEX);
        sp2.addMandatorySampleCategoryId(SIZE);
        sp2.setWeightEnabled(true);
        protocol.addSpecies(sp2);

        protocol.setBenthos(Lists.<SpeciesProtocol>newArrayList());
        SpeciesProtocol b1 = SpeciesProtocols.newSpeciesProtocol();
        b1.setMandatorySampleCategoryId(Lists.<Integer>newArrayList());
        b1.setId("1");
        b1.setSpeciesReferenceTaxonId(11242);
        b1.setSpeciesSurveyCode("BAR");
        b1.setLengthStepPmfmId("1394");
        b1.addMandatorySampleCategoryId(MATURITY);
        b1.addMandatorySampleCategoryId(SEX);
        b1.setWeightEnabled(true);
        protocol.addBenthos(b1);

        SpeciesProtocol b2 = SpeciesProtocols.newSpeciesProtocol();
        b2.setMandatorySampleCategoryId(Lists.<Integer>newArrayList());
        b2.setId("2");
        b2.setSpeciesReferenceTaxonId(3835);
        b2.setSpeciesSurveyCode("CHIN");
        b2.setLengthStepPmfmId("323");
        b2.addMandatorySampleCategoryId(AGE);
        b2.addMandatorySampleCategoryId(MATURITY);
        b2.addMandatorySampleCategoryId(SEX);
        b2.addMandatorySampleCategoryId(SIZE);
        b2.setWeightEnabled(true);
        protocol.addBenthos(b2);

        return protocol;
    }

}

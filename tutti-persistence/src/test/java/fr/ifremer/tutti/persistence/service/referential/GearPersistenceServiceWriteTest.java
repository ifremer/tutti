package fr.ifremer.tutti.persistence.service.referential;

/*
 * #%L
 * Tutti :: Persistence
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import fr.ifremer.tutti.persistence.entities.referential.Gear;
import fr.ifremer.tutti.persistence.entities.referential.Gears;
import fr.ifremer.tutti.persistence.service.TuttiPersistenceServiceLocator;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Collection;
import java.util.List;

/**
 * Created on 11/3/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.8
 */
public class GearPersistenceServiceWriteTest extends ReferentialPersistenceServiceWriteTestSupport {

    protected GearPersistenceService service;

    @Before
    public void setUp() throws Exception {
        service = TuttiPersistenceServiceLocator.getGearPersistenceService();
    }

    @Test
    public void testAddTemporaryGears() {
        List<Gear> gears = Lists.newArrayList();

        // scientific gear
        long timestamp1 = System.nanoTime();
        Gear g1 = Gears.newGear();
        g1.setId("Don't care" + timestamp1);
        g1.setDescription("Don't care" + timestamp1);
        g1.setName("Name" + timestamp1);
        g1.setLabel("Label" + timestamp1);
        g1.setScientificGear(true);
        gears.add(g1);

        // fishing gear
        long timestamp2 = System.nanoTime();
        Gear g2 = Gears.newGear();
        g2.setId("Don't care" + timestamp2);
        g2.setDescription("Don't care" + timestamp2);
        g2.setName("Name" + timestamp2);
        g2.setLabel("Label" + timestamp2);
        g2.setScientificGear(false);

        gears.add(g2);

        Collection<Gear> gearList = service.addTemporaryGears(gears);

        List<Gear> allScientificGear = service.getAllScientificGear();
        List<Gear> allFishingGear = service.getAllFishingGear();

        Gear createdG1 = Iterables.get(gearList, 0);
        Assert.assertNotNull(createdG1);
        Assert.assertEquals(g1.getName(), createdG1.getName());
        Assert.assertEquals(g1.getLabel(), createdG1.getLabel());

        Assert.assertNotNull(createdG1.getId());
        Assert.assertNotSame(g1.getId(), createdG1.getId());
        Assert.assertEquals(createdG1, service.getGear(Integer.valueOf(createdG1.getId())));
        Assert.assertTrue(allScientificGear.contains(createdG1));
        Assert.assertFalse(allFishingGear.contains(createdG1));

        Gear createdG2 = Iterables.get(gearList, 1);
        Assert.assertNotNull(createdG2);
        Assert.assertEquals(g2.getName(), createdG2.getName());
        Assert.assertEquals(g2.getLabel(), createdG2.getLabel());

        Assert.assertNotNull(createdG2.getId());
        Assert.assertNotSame(g2.getId(), createdG2.getId());
        Assert.assertEquals(createdG2, service.getGear(Integer.valueOf(createdG2.getId())));
        Assert.assertFalse(allScientificGear.contains(createdG2));
        Assert.assertTrue(allFishingGear.contains(createdG2));
    }
}

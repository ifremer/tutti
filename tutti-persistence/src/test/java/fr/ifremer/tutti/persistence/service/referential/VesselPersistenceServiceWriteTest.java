package fr.ifremer.tutti.persistence.service.referential;

/*
 * #%L
 * Tutti :: Persistence
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import fr.ifremer.tutti.persistence.entities.referential.Vessel;
import fr.ifremer.tutti.persistence.entities.referential.Vessels;
import fr.ifremer.tutti.persistence.service.TuttiPersistenceServiceLocator;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Collection;
import java.util.List;

/**
 * Created on 11/3/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.8
 */
public class VesselPersistenceServiceWriteTest extends ReferentialPersistenceServiceWriteTestSupport {

    protected VesselPersistenceService service;

    @Before
    public void setUp() throws Exception {
        service = TuttiPersistenceServiceLocator.getVesselPersistenceService();
    }

    @Test
    public void testAddTemporaryVessels() {
        List<Vessel> vessels = Lists.newArrayList();

        // scientificVessel
        long timestamp1 = System.nanoTime();
        Vessel v1 = Vessels.newVessel();
        v1.setId("Don't care" + timestamp1);
        v1.setName("Name" + timestamp1);
        v1.setInternationalRegistrationCode("Immat" + timestamp1);
        v1.setScientificVessel(true);
        vessels.add(v1);

        // fishingVessel
        long timestamp2 = System.nanoTime();
        Vessel v2 = Vessels.newVessel();
        v2.setId("Don't care" + timestamp2);
        v2.setName("Name" + timestamp2);
        v2.setInternationalRegistrationCode("Immat" + timestamp2);
        v2.setScientificVessel(false);
        vessels.add(v2);

        Assert.assertNull(service.getVessel(v1.getId()));
        Assert.assertNull(service.getVessel(v2.getId()));

        Collection<Vessel> vesselList = service.addTemporaryVessels(vessels);

        Assert.assertNotNull(vesselList);
        Assert.assertEquals(2, vesselList.size());

        List<Vessel> allScientificVessel = service.getAllScientificVessel();
        List<Vessel> allFishingVessel = service.getAllFishingVessel();

        Vessel createdV1 = Iterables.get(vesselList, 0);
        Assert.assertNotNull(createdV1);
        Assert.assertEquals(v1.getName(), createdV1.getName());
        Assert.assertEquals(v1.getInternationalRegistrationCode(), createdV1.getInternationalRegistrationCode());
        Assert.assertNotNull(createdV1.getId());
        Assert.assertNotSame(v1.getId(), createdV1.getId());
        Assert.assertEquals(createdV1, service.getVessel(createdV1.getId()));

        Assert.assertTrue(allScientificVessel.contains(createdV1));
        Assert.assertFalse(allFishingVessel.contains(createdV1));

        Vessel createdV2 = Iterables.get(vesselList, 1);
        Assert.assertNotNull(createdV2);
        Assert.assertEquals(v2.getName(), createdV2.getName());
        Assert.assertEquals(v2.getInternationalRegistrationCode(), createdV2.getInternationalRegistrationCode());
        Assert.assertNotNull(createdV2.getId());
        Assert.assertNotSame(v2.getId(), createdV2.getId());
        Assert.assertEquals(createdV2, service.getVessel(createdV2.getId()));
        Assert.assertFalse(allScientificVessel.contains(createdV2));
        Assert.assertTrue(allFishingVessel.contains(createdV2));
    }

}

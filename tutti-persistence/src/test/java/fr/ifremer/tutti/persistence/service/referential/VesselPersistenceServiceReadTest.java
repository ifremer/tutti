package fr.ifremer.tutti.persistence.service.referential;

/*
 * #%L
 * Tutti :: Persistence
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.entities.referential.Vessel;
import fr.ifremer.tutti.persistence.service.TuttiPersistenceServiceLocator;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

public class VesselPersistenceServiceReadTest extends ReferentialPersistenceServiceReadTestSupport {

    /** Logger. */
    private static final Log log = LogFactory.getLog(VesselPersistenceServiceReadTest.class);
    
    protected VesselPersistenceService service;

    @Before
    public void setUp() throws Exception {
        service = TuttiPersistenceServiceLocator.getVesselPersistenceService();
        super.setUp();

    }

    @Test
    public void getAllScientificVessel() {
        List<Vessel> result = service.getAllScientificVessel();
        assertResultList(result, fixtures.refNbScientificVessel());
    }

    @Test
    public void getAllFishingVessel() {

        // clear cache before this test
        cacheService.clearCache("fishingVessels");

        long time = System.currentTimeMillis();
        List<Vessel> result = service.getAllFishingVessel();
        long delta1 = System.currentTimeMillis() - time;

        assertResultList(result, fixtures.refNbFishingVessel());
        log.debug("Load fishing vessels (without cache) in: " + delta1 + "ms");

        // try again, to check cache is enable
        time = System.currentTimeMillis();
        result = service.getAllFishingVessel();
        assertResultList(result, fixtures.refNbFishingVessel());

        long delta2 = System.currentTimeMillis() - time;
        log.debug("Load fishing vessels (with cache enable) in: " + delta2 + "ms");

        //TODO-tc We should not test cache performance like this, depends to much of the computer usage,...
//        float reduceFactor = delta2 * 100 / delta1;
//        Assert.assertTrue("The cache on getAllFishingVessel() should have speed up more than factor 10. Make sure EhCache is well configured.", reduceFactor < 0.1);
    }

}
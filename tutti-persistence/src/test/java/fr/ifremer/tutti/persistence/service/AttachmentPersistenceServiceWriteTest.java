package fr.ifremer.tutti.persistence.service;

/*
 * #%L
 * Tutti :: Persistence
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.adagio.core.dao.referential.ObjectTypeCode;
import fr.ifremer.tutti.persistence.entities.data.Attachment;
import fr.ifremer.tutti.persistence.entities.data.Attachments;
import fr.ifremer.tutti.persistence.test.DatabaseResource;
import org.junit.Assert;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Test;

import java.io.File;
import java.util.List;

/**
 * To test {@link AttachmentPersistenceService} for write operation.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.0.2
 */
public class AttachmentPersistenceServiceWriteTest {

    @ClassRule
    public static final DatabaseResource dbResource = DatabaseResource.writeDb();

    protected AttachmentPersistenceService service;

    @Before
    public void setUp() throws Exception {
        service = TuttiPersistenceServiceLocator.getAttachmentPersistenceService();
    }

    @Test
    public void createAndSaveAttachment() {

        List<Attachment> allAttachments;

        Integer cruiseId = Integer.valueOf(dbResource.getFixtures().cruiseId());

        allAttachments = service.getAllAttachments(
                ObjectTypeCode.SCIENTIFIC_CRUISE, cruiseId);
        Assert.assertTrue(allAttachments.isEmpty());

        //
        // create attachment
        //

        Attachment attachment = Attachments.newAttachment();
        attachment.setObjectType(ObjectTypeCode.SCIENTIFIC_CRUISE);
        attachment.setObjectId(cruiseId);
        long buildTime = System.nanoTime();
        attachment.setName("AttachmentName-" + buildTime);
        attachment.setComment("AttachmentComment-" + buildTime);
        File fileToAttach = dbResource.getConfig().getConfigFile();
        Attachment savedAttachment = service.createAttachment(attachment, fileToAttach);
        assertAttachment(attachment, savedAttachment);

        //
        // get Attachment file
        //

        File attachmentFile = service.getAttachmentFile(attachment.getId());
        Assert.assertNotNull(attachmentFile);
        Assert.assertTrue(attachmentFile.exists());
        Assert.assertEquals(fileToAttach.length(), attachmentFile.length());

        //
        // reload it
        //

        allAttachments = service.getAllAttachments(
                ObjectTypeCode.SCIENTIFIC_CRUISE, cruiseId);
        Assert.assertFalse(allAttachments.isEmpty());
        Assert.assertEquals(1, allAttachments.size());

        Attachment reloadedAttachment = allAttachments.get(0);
        assertAttachment(attachment, reloadedAttachment);

        //
        // modifiy it and save it
        //

        attachment.setName(attachment.getName() + "-2");
        attachment.setComment(attachment.getComment() + "-2");

        savedAttachment = service.saveAttachment(attachment);
        assertAttachment(attachment, savedAttachment);

        //
        // reload it
        //

        allAttachments = service.getAllAttachments(
                ObjectTypeCode.SCIENTIFIC_CRUISE, cruiseId);
        Assert.assertFalse(allAttachments.isEmpty());
        Assert.assertEquals(1, allAttachments.size());

        reloadedAttachment = allAttachments.get(0);
        assertAttachment(attachment, reloadedAttachment);

        //
        // delete it
        //

        service.deleteAttachment(attachment.getId());
        allAttachments = service.getAllAttachments(
                ObjectTypeCode.SCIENTIFIC_CRUISE, cruiseId);
        Assert.assertTrue(allAttachments.isEmpty());
        Assert.assertFalse(attachmentFile.exists());
    }

    protected void assertAttachment(Attachment expected, Attachment actual) {
        Assert.assertNotNull(expected);
        Assert.assertNotNull(actual);
        Assert.assertEquals(expected.getId(), actual.getId());
        Assert.assertEquals(expected.getPath(), actual.getPath());
        Assert.assertEquals(expected.getName(), actual.getName());
        Assert.assertEquals(expected.getComment(), actual.getComment());
        Assert.assertEquals(expected.getObjectType(), actual.getObjectType());
        Assert.assertEquals(expected.getObjectId(), actual.getObjectId());
    }
}

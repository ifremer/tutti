package fr.ifremer.tutti.persistence.service;

/*
 * #%L
 * Tutti :: Persistence
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.entities.data.BatchContainer;
import fr.ifremer.tutti.persistence.entities.data.CatchBatch;
import fr.ifremer.tutti.persistence.entities.data.CatchBatchs;
import fr.ifremer.tutti.persistence.entities.data.Cruise;
import fr.ifremer.tutti.persistence.entities.data.FishingOperation;
import fr.ifremer.tutti.persistence.entities.data.MarineLitterBatch;
import fr.ifremer.tutti.persistence.entities.data.MarineLitterBatchs;
import fr.ifremer.tutti.persistence.entities.referential.Caracteristic;
import fr.ifremer.tutti.persistence.entities.referential.TuttiLocation;
import fr.ifremer.tutti.persistence.service.referential.CaracteristicPersistenceService;
import fr.ifremer.tutti.persistence.service.referential.LocationPersistenceService;
import fr.ifremer.tutti.persistence.test.DatabaseResource;
import org.junit.Assert;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Ignore;
import org.junit.Test;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

/**
 * To test {@link MarineLitterBatchPersistenceService} for write operation.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 0.3
 */
@Ignore
public class MarineLitterBatchPersistenceServiceWriteTest {

    @ClassRule
    public static final DatabaseResource dbResource = DatabaseResource.writeDb();

    protected MarineLitterBatchPersistenceService service;

    /*
    * Entities prepared in setUp() :
    * */
    protected FishingOperation fishingOperationNoCatchBatch;

    protected FishingOperation fishingOperationWithEmptyBatch;

    protected CatchBatch catchBacth;

    protected Caracteristic marineLitterCategoryCaracteristic;

    protected Caracteristic marineLitterSizeCategoryCaracteristic;

    @Before
    public void setUp() throws Exception {
        service = TuttiPersistenceServiceLocator.getMarineLitterBatchPersistenceService();


        CruisePersistenceService cruiseService = TuttiPersistenceServiceLocator.getCruisePersistenceService();
        CatchBatchPersistenceService catchBatchService = TuttiPersistenceServiceLocator.getCatchBatchPersistenceService();
        FishingOperationPersistenceService fishingOperationService = TuttiPersistenceServiceLocator.getFishingOperationPersistenceService();

        LocationPersistenceService locationService = TuttiPersistenceServiceLocator.getLocationPersistenceService();
        CaracteristicPersistenceService caracteristicService = TuttiPersistenceServiceLocator.getCaracteristicPersistenceService();

        marineLitterCategoryCaracteristic = caracteristicService.getMarineLitterCategoryCaracteristic();

        marineLitterSizeCategoryCaracteristic = caracteristicService.getMarineLitterSizeCategoryCaracteristic();

        Cruise cruise = cruiseService.getCruise(dbResource.getFixtures().cruiseId());
        cruise.setId((String) null);
        Calendar calendar = new GregorianCalendar();
        cruise.setBeginDate(calendar.getTime());
        calendar.add(Calendar.MONTH, 1); // add one month
        cruise.setEndDate(calendar.getTime());
        List<TuttiLocation> allHarbour = locationService.getAllHarbour();
        Assert.assertNotNull(allHarbour);
        Assert.assertTrue(allHarbour.size() > 1);
        cruise.setDepartureLocation(allHarbour.get(0));
        cruise.setReturnLocation(allHarbour.get(1));

        cruise = cruiseService.createCruise(cruise);

        // Create a first operation, with no cacth batch : to test CatchBatch insert/update :
        List<FishingOperation> fishingOperations = fishingOperationService.getAllFishingOperation(dbResource.getFixtures().cruiseId());
        assertNotNull(fishingOperations);
        assertTrue(fishingOperations.size() > 0);
        fishingOperationNoCatchBatch = fishingOperations.get(0);
        fishingOperationNoCatchBatch = fishingOperationService.getFishingOperation(fishingOperationNoCatchBatch.getIdAsInt() );
        fishingOperationNoCatchBatch.setId((String) null);
        fishingOperationNoCatchBatch.setCruise(cruise);
        calendar.setTime(new Date());
        calendar.set(Calendar.HOUR_OF_DAY, 1);
        calendar.set(Calendar.MILLISECOND, 0);
        fishingOperationNoCatchBatch.setGearShootingStartDate(calendar.getTime());
        calendar.setTime(new Date());
        calendar.set(Calendar.HOUR_OF_DAY, 10);
        calendar.set(Calendar.MILLISECOND, 0);
        fishingOperationNoCatchBatch.setGearShootingEndDate(calendar.getTime());
        fishingOperationNoCatchBatch = fishingOperationService.createFishingOperation(fishingOperationNoCatchBatch);

        // Create a second operation, with no cacth batch : to test CatchBatch insert/update :
        fishingOperationWithEmptyBatch = fishingOperations.get(1);
        fishingOperationWithEmptyBatch = fishingOperationService.getFishingOperation(fishingOperationWithEmptyBatch.getIdAsInt() );
        fishingOperationWithEmptyBatch.setId((String) null);
        fishingOperationWithEmptyBatch.setCruise(cruise);
        calendar.setTime(new Date());
        calendar.set(Calendar.HOUR_OF_DAY, 11);
        calendar.set(Calendar.MILLISECOND, 0);
        fishingOperationWithEmptyBatch.setGearShootingStartDate(calendar.getTime());
        calendar.setTime(new Date());
        calendar.set(Calendar.HOUR_OF_DAY, 12);
        calendar.set(Calendar.MILLISECOND, 0);
        fishingOperationWithEmptyBatch.setGearShootingEndDate(calendar.getTime());
        fishingOperationWithEmptyBatch = fishingOperationService.createFishingOperation(fishingOperationWithEmptyBatch);

        catchBacth = CatchBatchs.newCatchBatch();
        catchBacth.setFishingOperation(fishingOperationWithEmptyBatch);
        catchBacth = catchBatchService.createCatchBatch(catchBacth);
    }

    @Test
    public void createMarineLitterBatch(/*MarineLitterBatch bean*/) {

        MarineLitterBatch newMarineLitter =
                MarineLitterBatchs.newMarineLitterBatch();

        newMarineLitter.setFishingOperation(fishingOperationWithEmptyBatch);
        newMarineLitter.setMarineLitterCategory(marineLitterCategoryCaracteristic.getQualitativeValue(0));
        newMarineLitter.setMarineLitterSizeCategory(marineLitterSizeCategoryCaracteristic.getQualitativeValue(0));
        newMarineLitter.setNumber(10);

        MarineLitterBatch createdBatch = service.createMarineLitterBatch(newMarineLitter);

        assertMarineLitterBatch(newMarineLitter, createdBatch);

        // then reload (for round trip check)
        MarineLitterBatch reloadedBatch = getMarineLitterBatch(
                newMarineLitter.getFishingOperation().getIdAsInt(), createdBatch.getIdAsInt());

        assertMarineLitterBatch(newMarineLitter, reloadedBatch);
    }

    @Test
    public void saveMarineLitterBatch(/*MarineLitterBatch bean*/) {

        MarineLitterBatch newMarineLitter =
                MarineLitterBatchs.newMarineLitterBatch();

        newMarineLitter.setFishingOperation(fishingOperationWithEmptyBatch);
        newMarineLitter.setMarineLitterCategory(marineLitterCategoryCaracteristic.getQualitativeValue(0));
        newMarineLitter.setMarineLitterSizeCategory(marineLitterSizeCategoryCaracteristic.getQualitativeValue(0));
        newMarineLitter.setNumber(10);

        MarineLitterBatch createdBatch = service.createMarineLitterBatch(newMarineLitter);
        newMarineLitter.setId(createdBatch.getId());

        // modify some values
        newMarineLitter.setComment("A comment");
        newMarineLitter.setWeight(5.f);
        newMarineLitter.setMarineLitterCategory(marineLitterCategoryCaracteristic.getQualitativeValue(1));
        MarineLitterBatch savedMarineLitterBatch = service.saveMarineLitterBatch(newMarineLitter);

        assertMarineLitterBatch(newMarineLitter, savedMarineLitterBatch);
    }

    @Test
    public void deleteMarineLitterBatch(/*String id*/) {
        MarineLitterBatch newMarineLitter =
                MarineLitterBatchs.newMarineLitterBatch();

        newMarineLitter.setFishingOperation(fishingOperationWithEmptyBatch);
        newMarineLitter.setMarineLitterCategory(marineLitterCategoryCaracteristic.getQualitativeValue(0));
        newMarineLitter.setMarineLitterSizeCategory(marineLitterSizeCategoryCaracteristic.getQualitativeValue(0));
        newMarineLitter.setNumber(10);

        MarineLitterBatch createdMarineLitterBatch = service.createMarineLitterBatch(newMarineLitter);

        BatchContainer<MarineLitterBatch> rootMarineLitterBatch = service.getRootMarineLitterBatch(fishingOperationWithEmptyBatch.getIdAsInt() );
        Assert.assertNotNull(rootMarineLitterBatch);
        Assert.assertFalse(rootMarineLitterBatch.getChildren().isEmpty());

        // delete it

        service.deleteMarineLitterBatch(createdMarineLitterBatch.getIdAsInt() );

        rootMarineLitterBatch = service.getRootMarineLitterBatch(fishingOperationWithEmptyBatch.getIdAsInt() );
        Assert.assertNotNull(rootMarineLitterBatch);
        Assert.assertTrue(rootMarineLitterBatch.getChildren().isEmpty());

    }

    protected void assertMarineLitterBatch(MarineLitterBatch expectedBatch,
                                           MarineLitterBatch actualBatch) {
        assertNotNull(actualBatch);
        assertNotNull(actualBatch.getId());
        assertEquals(expectedBatch.getId(), actualBatch.getId());

        assertEquals(expectedBatch.getMarineLitterCategory(), actualBatch.getMarineLitterCategory());
        assertEquals(expectedBatch.getMarineLitterSizeCategory(), actualBatch.getMarineLitterSizeCategory());
        assertEquals(expectedBatch.getNumber(), actualBatch.getNumber());
        assertEquals(expectedBatch.getWeight(), actualBatch.getWeight());
        assertEquals(expectedBatch.getComment(), actualBatch.getComment());
    }

    protected MarineLitterBatch getMarineLitterBatch(Integer fishingOperationId, Integer id) {
        BatchContainer<MarineLitterBatch> rootMarineLitterBatch = service.getRootMarineLitterBatch(fishingOperationId);
        for (MarineLitterBatch marineLitterBatch : rootMarineLitterBatch.getChildren()) {
            if (id.equals(marineLitterBatch.getIdAsInt())) {
                return marineLitterBatch;
            }
        }
        return null;
    }
}

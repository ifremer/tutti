package fr.ifremer.tutti.persistence.service;

/*
 * #%L
 * Tutti :: Persistence
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.entities.data.Program;
import fr.ifremer.tutti.persistence.entities.data.Programs;
import fr.ifremer.tutti.persistence.entities.referential.TuttiLocation;
import fr.ifremer.tutti.persistence.service.referential.LocationPersistenceService;
import fr.ifremer.tutti.persistence.test.DatabaseResource;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

/**
 * To test {@link ProgramPersistenceService} for write operation.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.0
 */
public class ProgramPersistenceServiceWriteTest {

    @ClassRule
    public static final DatabaseResource dbResource = DatabaseResource.writeDb();

    protected ProgramPersistenceService service;

    private LocationPersistenceService locationService;

    @Before
    public void setUp() throws Exception {
        service = TuttiPersistenceServiceLocator.getProgramPersistenceService();
        locationService = TuttiPersistenceServiceLocator.getLocationPersistenceService();
    }

    @Test
    public void createAndSaveProgram(/*Program bean*/) {

        List<TuttiLocation> zones = locationService.getAllProgramZone();
        assertNotNull(zones);
        assertTrue(zones.size() > 0);

        Program program = Programs.newProgram();
        String name = "UniTest" + System.currentTimeMillis();
        if (name.length() > 40) {
            name = name.substring(0, 39);
        }
        program.setName(name);
        program.setDescription("Comment-" + name);

        program.setZone(zones.get(0));

        // Create program
        Program createdProgram = service.createProgram(program);
        assertNotNull(createdProgram);
        assertNotNull(createdProgram.getId());
        assertNotNull(createdProgram.getDescription());
        assertNotNull(createdProgram.getZone());
        assertEquals(program.getName(), createdProgram.getName());
        assertEquals(program.getDescription(), createdProgram.getDescription());

        // Reload program and compare
        Program reloadedProgram = service.getProgram(createdProgram.getId());
        assertNotNull(reloadedProgram);
        assertEquals(createdProgram, reloadedProgram);
        assertEquals(program.getName(), reloadedProgram.getName());
        assertEquals(program.getDescription(), reloadedProgram.getDescription());
        assertNotNull(program.getZone());
        assertEquals(program.getZone(), reloadedProgram.getZone());

        // Modify program
        program.setId(createdProgram.getId());
        program.setName("NEW_NAME");
        program.setDescription("Add some modification");
        program.setZone(zones.get(1));

        // Save program
        Program savedProgram = service.saveProgram(program);
        assertNotNull(savedProgram);
        assertNotNull(savedProgram.getId());

        // Reload program and compare
        reloadedProgram = service.getProgram(savedProgram.getId());
        assertNotNull(reloadedProgram);
        assertEquals(program.getId(), reloadedProgram.getId());
        assertEquals(program.getName(), reloadedProgram.getName());
        assertEquals(program.getDescription(), reloadedProgram.getDescription());
        assertNotNull(program.getZone());
        assertEquals(program.getZone(), reloadedProgram.getZone());
    }

}

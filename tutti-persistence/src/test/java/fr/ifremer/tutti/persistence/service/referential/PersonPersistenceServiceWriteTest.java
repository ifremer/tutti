package fr.ifremer.tutti.persistence.service.referential;

/*
 * #%L
 * Tutti :: Persistence
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import fr.ifremer.tutti.persistence.entities.referential.Person;
import fr.ifremer.tutti.persistence.entities.referential.Persons;
import fr.ifremer.tutti.persistence.service.TuttiPersistenceServiceLocator;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Collection;
import java.util.List;

/**
 * Created on 11/3/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.8
 */
public class PersonPersistenceServiceWriteTest extends ReferentialPersistenceServiceWriteTestSupport {

    protected PersonPersistenceService service;

    @Before
    public void setUp() throws Exception {
        service = TuttiPersistenceServiceLocator.getPersonPersistenceService();
    }

    @Test
    public void testAddTemporaryPersons() {
        List<Person> persons = Lists.newArrayList();

        long timestamp1 = System.nanoTime();
        Person p1 = Persons.newPerson();
        p1.setId("Don't care" + timestamp1);
        p1.setName("Don't care" + timestamp1);
        p1.setFirstName("FirstName" + timestamp1);
        p1.setLastName("MastName" + timestamp1);
        p1.setDescription("Dont care" + timestamp1);
        p1.setDescription("Dont care" + timestamp1);
        persons.add(p1);

        long timestamp2 = System.nanoTime();
        Person p2 = Persons.newPerson();
        p2.setId("Don't care" + timestamp2);
        p2.setName("Don't care" + timestamp2);
        p2.setFirstName("FirstName" + timestamp2);
        p2.setLastName("LastName" + timestamp2);
        p2.setDescription("Don't care" + timestamp2);
        p2.setDepartment("Don't care" + timestamp2);
        persons.add(p2);

        Collection<Person> personList = service.addTemporaryPersons(persons);

        Assert.assertNotNull(personList);
        Assert.assertEquals(2, personList.size());

        Person createdP1 = Iterables.get(personList, 0);
        Assert.assertNotNull(createdP1);
        Assert.assertEquals(p1.getFirstName(), createdP1.getFirstName());
        Assert.assertEquals(p1.getLastName(), createdP1.getLastName());

        Assert.assertNull(createdP1.getName());
        Assert.assertNull(createdP1.getDepartment());
        Assert.assertNull(createdP1.getDescription());

        Assert.assertNotNull(createdP1.getId());
        Assert.assertNotSame(p1.getId(), createdP1.getId());

        Assert.assertEquals(createdP1, service.getPerson(Integer.valueOf(createdP1.getId())));


        Person createdP2 = Iterables.get(personList, 1);
        Assert.assertNotNull(createdP2);
        Assert.assertEquals(p2.getFirstName(), createdP2.getFirstName());
        Assert.assertEquals(p2.getLastName(), createdP2.getLastName());

        Assert.assertNull(createdP1.getName());
        Assert.assertNull(createdP1.getDepartment());
        Assert.assertNull(createdP1.getDescription());

        Assert.assertNotNull(createdP2.getId());
        Assert.assertNotSame(p2.getId(), createdP2.getId());

        Assert.assertEquals(createdP2, service.getPerson(Integer.valueOf(createdP2.getId())));
    }

}

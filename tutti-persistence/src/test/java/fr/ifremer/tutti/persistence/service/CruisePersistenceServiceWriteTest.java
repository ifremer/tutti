package fr.ifremer.tutti.persistence.service;

/*
 * #%L
 * Tutti :: Persistence
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Lists;
import fr.ifremer.tutti.persistence.entities.data.Cruise;
import fr.ifremer.tutti.persistence.entities.data.Cruises;
import fr.ifremer.tutti.persistence.entities.referential.Gear;
import fr.ifremer.tutti.persistence.entities.referential.GearWithOriginalRankOrder;
import fr.ifremer.tutti.persistence.entities.referential.GearWithOriginalRankOrders;
import fr.ifremer.tutti.persistence.entities.referential.Person;
import fr.ifremer.tutti.persistence.entities.referential.TuttiLocation;
import fr.ifremer.tutti.persistence.entities.referential.Vessel;
import fr.ifremer.tutti.persistence.entities.referential.Vessels;
import fr.ifremer.tutti.persistence.service.referential.GearPersistenceService;
import fr.ifremer.tutti.persistence.service.referential.LocationPersistenceService;
import fr.ifremer.tutti.persistence.service.referential.PersonPersistenceService;
import fr.ifremer.tutti.persistence.test.DatabaseResource;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Assert;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Ignore;
import org.junit.Test;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * To test {@link CruisePersistenceService} for write operation.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.0
 */
@Ignore
public class CruisePersistenceServiceWriteTest {

    /** Logger. */
    private static final Log log =
            LogFactory.getLog(CruisePersistenceServiceWriteTest.class);

    @ClassRule
    public static final DatabaseResource dbResource = DatabaseResource.writeDb();

    private GearPersistenceService gearService;

    private LocationPersistenceService locationService;

    private PersonPersistenceService personService;

    protected ProgramPersistenceService programService;

    protected CruisePersistenceService service;

    @Before
    public void setUp() throws Exception {
        service = TuttiPersistenceServiceLocator.getCruisePersistenceService();
        gearService = TuttiPersistenceServiceLocator.getGearPersistenceService();
        locationService = TuttiPersistenceServiceLocator.getLocationPersistenceService();
        personService = TuttiPersistenceServiceLocator.getPersistenceService();
        programService = TuttiPersistenceServiceLocator.getProgramPersistenceService();
    }

    @Test
    public void createCruise(/*Cruise bean*/) {
        String programCode = dbResource.getFixtures().programCode();
        Cruise cruise = Cruises.newCruise();

        // -----------------------------------------------------------------------------
        // 1. Test with all properties filled
        // -----------------------------------------------------------------------------
        cruise.setId((String) null);

        cruise.setName("Unit-test-" + System.currentTimeMillis());

        cruise.setProgram(programService.getProgram(programCode));

        Calendar calendar = new GregorianCalendar();
        cruise.setBeginDate(calendar.getTime());

        calendar.add(Calendar.MONTH, 1); // add one month
        cruise.setEndDate(calendar.getTime());

        List<TuttiLocation> allHarbour = locationService.getAllHarbour();
        Assert.assertNotNull(allHarbour);
        Assert.assertTrue(allHarbour.size() > 1);
        cruise.setDepartureLocation(allHarbour.get(0));
        cruise.setReturnLocation(allHarbour.get(1));

        List<Gear> gears = gearService.getAllFishingGear();
        List<GearWithOriginalRankOrder> gears2 = Lists.newArrayListWithCapacity(gears.size());
        for (Gear gear : gears) {
            gears2.add(GearWithOriginalRankOrders.newGearWithOriginalRankOrder(gear));
        }
        cruise.setGear(gears2);

        cruise.setComment("My comments on cruise");
        cruise.setSurveyPart("SurveyPart");

        Person managerPerson = personService.getAllPerson().get(0);
        cruise.setHeadOfMission(Lists.newArrayList(managerPerson));
        cruise.setHeadOfSortRoom(Lists.newArrayList(managerPerson));

        Vessel fishingVessel = Vessels.newVessel();
        fishingVessel.setId(dbResource.getFixtures().fishingVesselCode());

        cruise.setVessel(fishingVessel);

        cruise.setMultirigNumber(2);

        // Create cruise in database
        Cruise createdCruise = service.createCruise(cruise);
        assertNotNull(createdCruise);
        assertNotNull(createdCruise.getId());
        assertEquals(cruise.getName(), createdCruise.getName());
        assertEquals(cruise.getSurveyPart(), createdCruise.getSurveyPart());
        assertNotNull(createdCruise.getDepartureLocation());
        assertEquals(cruise.getDepartureLocation(), createdCruise.getDepartureLocation());
        assertNotNull(createdCruise.getReturnLocation());
        assertEquals(cruise.getReturnLocation(), createdCruise.getReturnLocation());
        assertEquals(cruise.getHeadOfMission(), createdCruise.getHeadOfMission());
        assertEquals(cruise.getHeadOfSortRoom(), createdCruise.getHeadOfSortRoom());

        if (log.isInfoEnabled()) {
            log.info("Created cruise: " + createdCruise.getId());
        }

        // Then reload cruise and compare
        Cruise reloadedCruise = service.getCruise(createdCruise.getIdAsInt());
        calendar.setTime(createdCruise.getBeginDate());
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        assertEquals(calendar.getTime(), reloadedCruise.getBeginDate());
        calendar.setTime(createdCruise.getEndDate());
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        assertEquals(calendar.getTime(), reloadedCruise.getEndDate());
        assertEquals(cruise.getMultirigNumber(), reloadedCruise.getMultirigNumber());
        assertEquals(cruise.getDepartureLocation(), reloadedCruise.getDepartureLocation());
        assertEquals(cruise.getReturnLocation(), reloadedCruise.getReturnLocation());
        assertEquals(cruise.getHeadOfMission(), reloadedCruise.getHeadOfMission());
        assertEquals(cruise.getHeadOfSortRoom(), reloadedCruise.getHeadOfSortRoom());

        // -----------------------------------------------------------------------------
        // 2. Test with only mandatory properties
        // -----------------------------------------------------------------------------
        createdCruise.setId((String) null);
//        createdCruise.setHeadOfMission(null);
//        createdCruise.setBeginDate(cruise.getBeginDate());
//        createdCruise.setEndDate(cruise.getEndDate());
        createdCruise.setComment(null);
        createdCruise.setMultirigNumber(null);

        // Save cruise
        createdCruise = service.createCruise(cruise);
        if (log.isInfoEnabled()) {
            log.info("Created cruise: " + createdCruise.getId());
        }
        assertNotNull(createdCruise);
        assertNotNull(createdCruise.getId());
        assertEquals(cruise.getName(), createdCruise.getName());
        assertNotNull(createdCruise.getDepartureLocation());
        assertEquals(cruise.getDepartureLocation(), createdCruise.getDepartureLocation());
        assertNotNull(createdCruise.getReturnLocation());
        assertEquals(cruise.getReturnLocation(), createdCruise.getReturnLocation());
        assertEquals(cruise.getHeadOfMission(), createdCruise.getHeadOfMission());
        assertEquals(cruise.getHeadOfSortRoom(), createdCruise.getHeadOfSortRoom());

        // Reload to compare
        reloadedCruise = service.getCruise(createdCruise.getIdAsInt());

        calendar.setTime(createdCruise.getBeginDate());
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        assertEquals(calendar.getTime(), reloadedCruise.getBeginDate());
        calendar.setTime(createdCruise.getEndDate());
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        assertEquals(calendar.getTime(), reloadedCruise.getEndDate());

//        assertEquals(createdCruise.getBeginDate(), reloadedCruise.getBeginDate());
//        assertEquals(createdCruise.getEndDate(), reloadedCruise.getEndDate());
        assertEquals(createdCruise.getComment(), reloadedCruise.getComment());
        assertEquals(createdCruise.getSurveyPart(), reloadedCruise.getSurveyPart());
        assertEquals(cruise.getDepartureLocation(), reloadedCruise.getDepartureLocation());
        assertEquals(cruise.getReturnLocation(), reloadedCruise.getReturnLocation());
//        assertEquals(createdCruise.getMultirigNumber(), reloadedCruise.getMultirigNumber());
        assertEquals(1, reloadedCruise.getMultirigNumber(), 0);
//        assertNull(reloadedCruise.getHeadOfMission());
        assertNotNull(reloadedCruise.getVessel());
        assertEquals(createdCruise.getVessel(), reloadedCruise.getVessel());
        assertNotNull(reloadedCruise.getVessel());
        assertNotNull(reloadedCruise.getGear());
        assertEquals(gears.size(), reloadedCruise.getGear().size());
        assertEquals(cruise.getHeadOfMission(), reloadedCruise.getHeadOfMission());
        assertEquals(cruise.getHeadOfSortRoom(), reloadedCruise.getHeadOfSortRoom());
    }

    @Test
    public void saveCruise(/*Cruise bean*/) {
        // -----------------------------------------------------------------------------
        // 1. Init a cruise (by copy)
        // -----------------------------------------------------------------------------
//        Cruise cruise = service.getCruise(dbResource.getFixtures().cruiseId());
        Cruise cruise = dbResource.getFixtures().createCruise();

        cruise.setId((String) null);
        Calendar calendar = new GregorianCalendar();
        cruise.setBeginDate(calendar.getTime());

        calendar.add(Calendar.MONTH, 1); // add one month
        cruise.setEndDate(calendar.getTime());
        List<TuttiLocation> allHarbour = locationService.getAllHarbour();
        Assert.assertNotNull(allHarbour);
        Assert.assertTrue(allHarbour.size() > 1);
        cruise.setDepartureLocation(allHarbour.get(0));
        cruise.setReturnLocation(allHarbour.get(1));

        cruise = service.createCruise(cruise);

        // -----------------------------------------------------------------------------
        // 2. Apply some changes
        // -----------------------------------------------------------------------------

        // Name :
        cruise.setName("Unit-test-" + System.currentTimeMillis());
        cruise.setSurveyPart("SurveryPart" + cruise.getName());
        cruise.setDepartureLocation(allHarbour.get(1));
        cruise.setReturnLocation(allHarbour.get(0));

        // Remove gear, then add another gear
        Gear previousGear = cruise.getGear(0);
        cruise.getGear().clear();
        List<Gear> gears = gearService.getAllFishingGear();
        for (Gear gear : gears) {
            // Make sure the gear is different before to add it
            if (!gear.equals(previousGear)) {
                cruise.addGear(GearWithOriginalRankOrders.newGearWithOriginalRankOrder(gear));
                break;
            }
        }

        cruise.addHeadOfMission(personService.getAllPerson().get(1));
        cruise.setHeadOfSortRoom(Lists.<Person>newArrayList(personService.getAllPerson().get(0)));

        // Save changes, then check
        Cruise savedCruise = service.saveCruise(cruise, true, true);
        assertNotNull(savedCruise);
        Cruise reloadedCruise = service.getCruise(savedCruise.getIdAsInt());

        assertEquals(cruise.getId(), reloadedCruise.getId());
        assertEquals(cruise.getName(), reloadedCruise.getName());
        assertEquals(cruise.getComment(), reloadedCruise.getComment());
        assertEquals(cruise.getSurveyPart(), reloadedCruise.getSurveyPart());
        assertNotNull(reloadedCruise.getGear());
        assertEquals(cruise.getGear().size(), reloadedCruise.getGear().size());
        assertEquals(cruise.getGear(0), reloadedCruise.getGear(0));
        assertEquals(cruise.getDepartureLocation(), reloadedCruise.getDepartureLocation());
        assertEquals(cruise.getReturnLocation(), reloadedCruise.getReturnLocation());
        assertEquals(cruise.getHeadOfMission(), reloadedCruise.getHeadOfMission());
        assertEquals(cruise.getHeadOfSortRoom(), reloadedCruise.getHeadOfSortRoom());
    }
}

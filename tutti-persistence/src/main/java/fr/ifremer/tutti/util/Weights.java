package fr.ifremer.tutti.util;

/*
 * #%L
 * Tutti :: Persistence
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.entities.protocol.Rtp;
import fr.ifremer.tutti.type.WeightUnit;

import java.util.Objects;

/**
 * Created on 8/26/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.7
 */
public class Weights {

//    private static DecimalFormat decimalFormat;

//    public static DecimalFormat getDecimalFormat(int minDecimal, int maxDecimal) {
//        if (decimalFormat == null) {
//            decimalFormat = new DecimalFormat();
//
//            DecimalFormatSymbols symbols = new DecimalFormatSymbols();
//            symbols.setDecimalSeparator('.');
//            symbols.setGroupingSeparator(' ');
//
//            decimalFormat.setDecimalFormatSymbols(symbols);
//            decimalFormat.setGroupingUsed(false);
//        }
//        decimalFormat.setMinimumFractionDigits(minDecimal);
//        decimalFormat.setMaximumFractionDigits(maxDecimal);
//        return decimalFormat;
//    }

//    public static String getWeightStringValue(Float weight) {
//        String textValue;
//        if (weight != null) {
//            DecimalFormat weightDecimalFormat = getDecimalFormat(1, 3);
//            textValue = weightDecimalFormat.format(weight);
//
//        } else {
//            textValue = "";
//        }
//        return textValue;
//    }

    /**
     * Calcul du poids <strong>(en gramme)</strong> à partir de la classe de taille et d'une définition de
     * Relation Taille Poids.
     *
     * La formule est {@code Poids (gramme) = a Taille (cm) ^ b},
     *
     * @param rtp            la définition du RTP à appliquer
     * @param lengthStep     la classe de taille à transformer
     * @param lengthStepUnit l'unité de la classe de taille
     * @return Le poids calculé en gramme à partir de la classe de taille et du RTP
     * @since 4.5
     */
    public static float computeWithRtp(Rtp rtp, float lengthStep, String lengthStepUnit) {
        lengthStep = Numbers.convertToCm(lengthStep, lengthStepUnit);
        return (float) (rtp.getA() * Math.pow(lengthStep, rtp.getB()));
    }

    /**
     * Pour convertir d'une unité vers une autre.
     *
     * @param sourceWeight     le poids à convertir
     * @param sourceWeightUnit l'unité de convertion
     * @param targetWeightUnit l'unité de convertion
     * @return le poids converti (et arrondi) dans l'unité cible
     */
    public static float convert(WeightUnit sourceWeightUnit, WeightUnit targetWeightUnit, float sourceWeight) {

        Objects.requireNonNull(sourceWeightUnit);
        Objects.requireNonNull(targetWeightUnit);

        // on convertit le poids source en mode «entité»
        float sourceWeightToEntity = sourceWeightUnit.toEntity(sourceWeight);

        // on convertit depuis le mode «entité» vers l'unité cible
        float targetWeightFromEntity = targetWeightUnit.fromEntity(sourceWeightToEntity);

        // on arrondit selon l'unité cible
        return targetWeightUnit.round(targetWeightFromEntity);

    }

    /**
     * Pour convertir d'une unité vers une autre.
     *
     * @param sourceWeight     le poids à convertir
     * @param sourceWeightUnit l'unité de convertion
     * @param targetWeightUnit l'unité de convertion
     * @return le poids converti (et arrondi) dans l'unité cible
     */
    public static Float convert(WeightUnit sourceWeightUnit, WeightUnit targetWeightUnit, Float sourceWeight) {

        Float targetWeight = sourceWeight;
        if (sourceWeight != null) {
            targetWeight = convert(sourceWeightUnit, targetWeightUnit, (float) sourceWeight);
        }

        return targetWeight;

    }
}

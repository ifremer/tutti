package fr.ifremer.tutti.util;

/*
 * #%L
 * Tutti :: Persistence
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.TuttiConfiguration;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.cfg.Environment;

import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;

/**
 * Created on 8/26/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.7
 */
public class Jdbcs {

    /** Logger. */
    private static final Log log = LogFactory.getLog(Jdbcs.class);

    public static void closeSilently(Statement statement) {
        try {
            if (statement != null && !statement.isClosed()) {

                statement.close();
            }
        } catch (AbstractMethodError e) {
            if (log.isDebugEnabled()) {
                log.debug("Fix this linkage error, damned hsqlsb 1.8.0.7:(");
            }
        } catch (IllegalAccessError e) {
            if (log.isDebugEnabled()) {
                log.debug("Fix this IllegalAccessError error, damned hsqlsb 1.8.0.7:(");
            }
        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Could not close statement, but do not care", e);
            }
        }
    }

    public static void closeSilently(ResultSet statement) {
        try {
            if (statement != null && !statement.isClosed()) {

                statement.close();
            }
        } catch (AbstractMethodError e) {
            if (log.isDebugEnabled()) {
                log.debug("Fix this linkage error, damned hsqlsb 1.8.0.7:(");
            }
        } catch (IllegalAccessError e) {
            if (log.isDebugEnabled()) {
                log.debug("Fix this IllegalAccessError error, damned hsqlsb 1.8.0.7:(");
            }
        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Could not close statement, but do not care", e);
            }
        }
    }

    public static Connection createConnection(Properties connectionProperties) throws SQLException {
        return createConnection(
                connectionProperties.getProperty(Environment.URL),
                connectionProperties.getProperty(Environment.USER),
                connectionProperties.getProperty(Environment.PASS)
        );
    }

    public static String getUrl(Properties connectionProperties) {
        return connectionProperties.getProperty(Environment.URL);
    }

    public static Connection createConnection(String jdbcUrl,
                                              String user,
                                              String password) throws SQLException {
        Connection connection = DriverManager.getConnection(jdbcUrl,
                                                            user,
                                                            password);
        connection.setAutoCommit(false);
        return connection;
    }

    public static void fillConnectionProperties(Properties p,
                                                String url,
                                                String username,
                                                String password) {
        p.put(Environment.URL, url);
        p.put(Environment.USER, username);
        p.put(Environment.PASS, password);
    }

    public static String getJdbcUrl(File directory, String dbName) {
        return "jdbc:hsqldb:file:" + directory.getAbsolutePath() + "/" + dbName;
    }

    public static void shutdown(TuttiConfiguration configuration) throws SQLException {

        String jdbcUrl = configuration.getJdbcUrl();
        String jdbcUsername = configuration.getJdbcUsername();
        String jdbcPassword = configuration.getJdbcPassword();
        try (Connection connection = createConnection(jdbcUrl, jdbcUsername, jdbcPassword)) {

            try (PreparedStatement pStmt = connection.prepareStatement("SHUTDOWN")) {
                pStmt.execute();
            }
        }

    }

    public static void shutdownCompact(TuttiConfiguration configuration) throws SQLException {

        String jdbcUrl = configuration.getJdbcUrl();
        String jdbcUsername = configuration.getJdbcUsername();
        String jdbcPassword = configuration.getJdbcPassword();
        try (Connection connection = createConnection(jdbcUrl, jdbcUsername, jdbcPassword)) {

            try (PreparedStatement pStmt = connection.prepareStatement("SHUTDOWN COMPACT")) {
                pStmt.execute();
            }
        }

    }

}

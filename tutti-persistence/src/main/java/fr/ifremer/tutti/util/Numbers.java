package fr.ifremer.tutti.util;

/*
 * #%L
 * Tutti :: Persistence
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.math.BigDecimal;

/**
 * Created on 8/26/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.7
 */
public class Numbers {

    public static final String MM_UNIT = "mm";

    public static final String CM_UNIT = "cm";

    public static float getRoundedLengthStep(float lengthStep, boolean aroundUp) {
        int intValue = (int) ((lengthStep + (aroundUp ? 0.001f : 0f)) * 10);
        return intValue / 10f;
    }

    public static <N extends Number> N getValueOrComputedValue(N value, N computedValue) {
        return value == null ? computedValue : value;
    }

    public static <N extends Number> Boolean getValueOrComputedValueComputed(N value, N computedValue) {
        Boolean result;
        if (value == null) {

            result = computedValue == null ? null : true;
        } else {
            result = false;
        }
        return result;
    }

    public static int roundToInt(float floatValue) {
        int intValue = (int) floatValue;
        if (floatValue - (float) intValue >= 0.5) {
            intValue++;
        }
        return intValue;
    }

    /**
     * Round the given value to max 5 digits.
     *
     * @param value the float to round.
     * @return the rounded value
     * @since 4.2
     */
    public static Float roundDecimalCoordinateComponent(Float value) {
        Float result = null;
        if (value != null) {
            BigDecimal sumB = new BigDecimal(String.valueOf(value))
                    .setScale(4, BigDecimal.ROUND_HALF_UP);
            // pourquoi abs() ?
//                    .abs();
            result = sumB.floatValue();
        }
        return result;
    }

    public static float convertToCm(float source, String sourceUnit) {
        if (MM_UNIT.equals(sourceUnit)) {
            // measurement in cm asked
            source = source / 10;
        }
        return source;
    }

    public static int convertToMm(float source, String sourceUnit) {
        if (CM_UNIT.equals(sourceUnit)) {
            // measurement in mm asked
            source = source * 10;
        }
        return (int) source;
    }

    public static float convertFromMm(float source, String targetUnit) {
        if (CM_UNIT.equals(targetUnit)) {
            // measurement in cm asked
            source = source / 10;
        }
        return source;
    }

}

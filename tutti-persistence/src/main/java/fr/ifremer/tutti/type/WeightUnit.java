package fr.ifremer.tutti.type;

/*
 * #%L
 * Tutti :: Persistence
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Comparator;
import java.util.Objects;

import static org.nuiton.i18n.I18n.n;
import static org.nuiton.i18n.I18n.t;

/**
 * Different types of weight unit.
 *
 * Created on 09/22/13.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.2
 */
public enum WeightUnit implements Comparator<Float> {

    G(n("application.common.unit.g"), n("application.common.unit.short.g"), 1, "\\d{0,8}(\\.\\d{0,1})?", 0.01f) {
        @Override
        public Float fromEntity(Float weight) {
            return weight == null ? null : weight * 1000;
        }

        @Override
        public Float toEntity(Float weight) {
            return weight == null ? null : weight / 1000.0f;
        }

    },
    KG(n("application.common.unit.kg"), n("application.common.unit.short.kg"), 4, "\\d{0,6}(\\.\\d{0,4})?", 0.00001f) {
        @Override
        public Float fromEntity(Float weight) {
            return weight;
        }

        @Override
        public Float toEntity(Float weight) {
            return weight;
        }

    };

    private final String i18nShortKey;

    private final String i18nKey;

    private final int numberDigits;

    private final String numberEditorPattern;

    private final float rawPrecision;

    private final DecimalFormat decimalFormat;

    WeightUnit(String i18nKey, String i18nShortKey, int numberDigits, String numberEditorPattern, float rawPrecision) {
        this.i18nKey = i18nKey;
        this.i18nShortKey = i18nShortKey;
        this.numberDigits = numberDigits;
        this.numberEditorPattern = numberEditorPattern;
        this.rawPrecision = rawPrecision;
        DecimalFormatSymbols symbols = new DecimalFormatSymbols();
        symbols.setDecimalSeparator('.');
        symbols.setGroupingSeparator(' ');
        this.decimalFormat = new DecimalFormat();
        this.decimalFormat.setDecimalFormatSymbols(symbols);
        this.decimalFormat.setGroupingUsed(false);
        this.decimalFormat.setMinimumFractionDigits(1);
        this.decimalFormat.setMaximumFractionDigits(numberDigits);
    }

    @Override
    public int compare(Float v0, Float v1) {

        Objects.requireNonNull(v0, "can not compare null weights");
        Objects.requireNonNull(v1, "can not compare null weights");

        float delta = roundWithSign(v0) - roundWithSign(v1);
        int result;
        if (delta > rawPrecision) {
            // v0 > v1
            result = 1;
        } else if (delta < -rawPrecision) {
            // v0 < v1
            result = -1;
        } else {
            // v0 == v1
            result = 0;
        }
        return result;

    }

    public String getLabel() {
        return t(i18nKey);
    }

    public String getShortLabel() {
        return t(i18nShortKey);
    }

    public int getNumberDigits() {
        return numberDigits;
    }

    public String getNumberEditorPattern() {
        return numberEditorPattern;
    }

    public String renderWeight(Float weight) {
        String textValue;
        if (weight == null) {
            textValue = "";
        } else {
            textValue = decimalFormat.format(weight);

        }
        return textValue;
    }

    /**
     * Transform the given {@code weight} coming from db to ui.
     *
     * @param weight weigth to transform
     * @return the ui representation of the given {@code weight}.
     */
    public abstract Float fromEntity(Float weight);

    /**
     * Transform the given {@code weight} coming from ui to db.
     *
     * @param weight weigth to transform
     * @return the db representation of the given {@code weight}.
     */
    public abstract Float toEntity(Float weight);

    public Float round(Float weight) {
        Float result;
        if (weight == null) {
            result = null;
        } else {

            BigDecimal sumB = new BigDecimal(String.valueOf(weight))
                    .setScale(numberDigits, BigDecimal.ROUND_HALF_UP)
                    .abs();
            result = sumB.floatValue();
        }
        return result;
    }

    public Float roundWithSign(Float weight) {
        Float result;
        if (weight == null) {
            result = null;
        } else {

            BigDecimal sumB = new BigDecimal(String.valueOf(weight))
                    .setScale(numberDigits, BigDecimal.ROUND_HALF_UP);
            result = sumB.floatValue();
        }
        return result;
    }

    public String decorateLabel(String label) {
        return String.format("%s (%s)", label, getShortLabel());
    }

    public String renderFromEntityWithShortLabel(float o) {
        return round(fromEntity(o)) + getShortLabel();
    }

    public String decorateTip(String tip) {
        String unit = t("application.common.unit");
        return String.format("%s (%s %s)", tip, unit, getLabel());
    }

    public boolean isNullOrZero(Float o) {
        return o == null || isZero(o);
    }

    public boolean isNotNullNorZero(Float o) {
        return o != null && !isZero(o);
    }

    public boolean isEquals(float o1, float o2) {
        return compare(o1, o2) == 0;
    }

    public boolean isZero(float o) {
        return isEquals(o, 0f);
    }

    public boolean isNotEquals(float o1, float o2) {
        return compare(o1, o2) != 0;
    }

    public boolean isSmallerThan(float o1, float o2) {
        return compare(o1, o2) < 0;
    }

    public boolean isGreaterThan(float o1, float o2) {
        return compare(o1, o2) > 0;
    }

    public boolean isGreaterThanZero(float o) {
        return isGreaterThan(o, 0f);
    }

    public boolean isSmallerThanZero(float o) {
        return isSmallerThan(o, 0f);
    }
}

package fr.ifremer.tutti.persistence.service.referential;

/*
 * #%L
 * Tutti :: Persistence
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import fr.ifremer.adagio.core.dao.referential.pmfm.PmfmId;
import fr.ifremer.adagio.core.dao.referential.pmfm.QualitativeValueId;
import fr.ifremer.adagio.core.dao.referential.pmfm.UnitId;
import fr.ifremer.tutti.persistence.dao.referential.pmfm.PmfmId2;
import fr.ifremer.tutti.persistence.entities.data.SpeciesBatch;
import fr.ifremer.tutti.persistence.entities.data.SpeciesBatchs;
import fr.ifremer.tutti.persistence.entities.referential.Caracteristic;
import fr.ifremer.tutti.persistence.entities.referential.CaracteristicQualitativeValue;
import fr.ifremer.tutti.persistence.entities.referential.CaracteristicQualitativeValues;
import fr.ifremer.tutti.persistence.entities.referential.CaracteristicType;
import fr.ifremer.tutti.persistence.entities.referential.Caracteristics;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.type.IntegerType;
import org.springframework.cache.Cache;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

/**
 * Created on 11/3/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.8
 */
@Service("caracteristicPersistenceService")
public class CaracteristicPersistenceServiceImpl extends ReferentialPersistenceServiceSupport implements CaracteristicPersistenceService {

    /** Logger. */
    private static final Log log = LogFactory.getLog(CaracteristicPersistenceServiceImpl.class);

    //TODO-TC We should prefer AOP thant Proxy stuff to avoid this.
    @Resource(name = "caracteristicPersistenceService")
    protected CaracteristicPersistenceService thisService;

    protected Set<Integer> propertedPmfmIds;

    protected Predicate<SpeciesBatch> batchVracPredicate;

    @Override
    public void init() {
        super.init();

        batchVracPredicate = SpeciesBatchs.newSpeciesAbleBatchCategoryPredicate(
                PmfmId.SORTED_UNSORTED.getValue(),
                QualitativeValueId.SORTED_VRAC.getValue());

        // init protected pmfm ids
        propertedPmfmIds = Sets.newHashSet(
                PmfmId.MULTIRIG_AGGREGATION.getValue(),
                PmfmId.MULTIRIG_NUMBER.getValue(),
                PmfmId.STATION_NUMBER.getValue(),
                PmfmId.RECTILINEAR_OPERATION.getValue(),
                PmfmId.HAUL_VALID.getValue(),
                PmfmId.TRAWL_DISTANCE.getValue(),
                PmfmId.SURVEY_PART.getValue(),
                PmfmId.SORTED_UNSORTED.getValue(),
                PmfmId.ID_PMFM.getValue(),
                PmfmId2.COPY_METHOD.getValue(),
                PmfmId2.CALCIFIED_STRUCTURE.getValue()
        );

    }

    @Override
    public List<Caracteristic> getAllCaracteristic() {
        return getAllCaracteristics(false);
    }

    @Override
    public List<Caracteristic> getAllCaracteristicWithProtected() {
        return getAllCaracteristics(true);
    }

    @Override
    public List<Caracteristic> getAllCaracteristicForSampleCategory() {
        List<Caracteristic> allCaracteristicWithProtected = thisService.getAllCaracteristicWithProtected();
        return Lists.newArrayList(
                Iterables.filter(allCaracteristicWithProtected, Caracteristics.newSampleCategoryModelPredicate()));
    }

    protected List<Caracteristic> getAllCaracteristics(boolean withProtected) {
        Iterator<Object[]> sources = queryListWithStatus(
                "allPmfm",
                "unitIdNone", IntegerType.INSTANCE, UnitId.NONE.getValue());
        List<Caracteristic> result = Lists.newArrayList();
        Cache pmfmByIdCache = cacheService.getCache("pmfmById");
        while (sources.hasNext()) {
            Object[] source = sources.next();
            Integer pmfmId = (Integer) source[0];
            Caracteristic target = loadCaracteristic(source);
            // Skip some protected PSFM
            if (withProtected || !isProtectedCaracteristic(pmfmId)) {
                result.add(target);
            }
            pmfmByIdCache.put(pmfmId, target);
        }
        return Collections.unmodifiableList(result);
    }

    @Override
    public List<Caracteristic> getAllNumericCaracteristic() {
        List<Caracteristic> result = Lists.newArrayList();

        for (Caracteristic caracteristic : getAllCaracteristic()) {
            if (caracteristic.isNumericType()) {
                result.add(caracteristic);
            }
        }
        return Collections.unmodifiableList(result);
    }

    @Override
    public Caracteristic getSizeCategoryCaracteristic() {
        Integer pmfmId = PmfmId.SIZE_CATEGORY.getValue();
        return getCaracteristic(pmfmId);
    }

    @Override
    public Caracteristic getSexCaracteristic() {
        Integer pmfmId = PmfmId.SEX.getValue();
        return getCaracteristic(pmfmId);
    }

    @Override
    public Caracteristic getSortedUnsortedCaracteristic() {
        Integer pmfmId = PmfmId.SORTED_UNSORTED.getValue();
        Caracteristic result = thisService.getCaracteristic(pmfmId);

        // Search the qualitative value to skip
        //FIXME-TC How can this works with cache ? result is the cached instance, so we are modifing cache
        List<CaracteristicQualitativeValue> qualitativeValue =
                Lists.newArrayList(result.getQualitativeValue());
        Iterator<CaracteristicQualitativeValue> iterator = qualitativeValue.iterator();
        while (iterator.hasNext()) {
            CaracteristicQualitativeValue qv = iterator.next();
            if (QualitativeValueId.UNSORTED.getValue().equals(Integer.valueOf(qv.getId()))) {
                iterator.remove();
                break;
            }
        }
        result.setQualitativeValue(Collections.unmodifiableList(qualitativeValue));
        return result;
    }

    @Override
    public Caracteristic getMaturityCaracteristic() {
        Integer pmfmId = PmfmId.MATURITY.getValue();
        return thisService.getCaracteristic(pmfmId);
    }

    @Override
    public Caracteristic getAgeCaracteristic() {
        Integer pmfmId = PmfmId.AGE.getValue();
        return thisService.getCaracteristic(pmfmId);
    }

    @Override
    public Caracteristic getMarineLitterCategoryCaracteristic() {
        Integer pmfmId = PmfmId.MARINE_LITTER_TYPE.getValue();
        return thisService.getCaracteristic(pmfmId);
    }

    @Override
    public Caracteristic getMarineLitterSizeCategoryCaracteristic() {
        Integer pmfmId = PmfmId.MARINE_LITTER_SIZE_CATEGORY.getValue();
        return thisService.getCaracteristic(pmfmId);
    }

    @Override
    public Caracteristic getVerticalOpeningCaracteristic() {
        Integer pmfmId = PmfmId.VERTICAL_OPENING.getValue();
        return thisService.getCaracteristic(pmfmId);
    }

    @Override
    public Caracteristic getHorizontalOpeningWingsCaracteristic() {
        Integer pmfmId = PmfmId.HORIZONTAL_OPENING_WINGS.getValue();
        return thisService.getCaracteristic(pmfmId);
    }

    @Override
    public Caracteristic getHorizontalOpeningDoorCaracteristic() {
        Integer pmfmId = PmfmId.HORIZONTAL_OPENING_DOOR.getValue();
        return thisService.getCaracteristic(pmfmId);
    }

    @Override
    public Caracteristic getDeadOrAliveCaracteristic() {
        Integer pmfmId = PmfmId.DEAD_OR_ALIVE.getValue();
        return thisService.getCaracteristic(pmfmId);
    }

    @Override
    public Caracteristic getCalcifiedStructureCaracteristic() {
        Integer pmfmId = PmfmId2.CALCIFIED_STRUCTURE.getValue();
        return thisService.getCaracteristic(pmfmId);
    }

    @Override
    public Caracteristic getPmfmIdCaracteristic() {
        Integer pmfmId = PmfmId.ID_PMFM.getValue();
        return thisService.getCaracteristic(pmfmId);
    }

    @Override
    public Caracteristic getWeightMeasuredCaracteristic() {
        Integer pmfmId = PmfmId.WEIGHT_MEASURED.getValue();
        return thisService.getCaracteristic(pmfmId);
    }

    @Override
    public Caracteristic getCopyIndividualObservationModeCaracteristic() {
        Integer pmfmId = PmfmId2.COPY_METHOD.getValue();
        return thisService.getCaracteristic(pmfmId);
    }

    @Override
    public Caracteristic getSampleCodeCaracteristic() {
        Integer pmfmId = PmfmId2.SAMPLE_ID.getValue();
        return thisService.getCaracteristic(pmfmId);
    }

    @Override
    public Caracteristic getCaracteristic(Integer pmfmId) {
        if (log.isInfoEnabled()) {
            log.info("Loading caracteristic: " + pmfmId);
        }
        Object[] source = queryUniqueWithStatus("pmfmById",
                                                "pmfmId", IntegerType.INSTANCE, pmfmId,
                                                "unitIdNone", IntegerType.INSTANCE, UnitId.NONE.getValue());
        return loadCaracteristic(source);
    }

    @Override
    public boolean isVracBatch(SpeciesBatch speciesBatch) {
        return batchVracPredicate.apply(speciesBatch);
    }

    @Override
    public boolean isHorsVracBatch(SpeciesBatch speciesBatch) {
        return !batchVracPredicate.apply(speciesBatch);
    }

    @Override
    public Predicate<SpeciesBatch> getVracBatchPredicate() {
        return batchVracPredicate;
    }

    protected Caracteristic loadCaracteristic(Object[] source) {

        Integer pmfmId = (Integer) source[0];

        Caracteristic result = Caracteristics.newCaracteristic();
        result.setId(pmfmId);
        result.setParameterName((String) source[1]);
        result.setMatrixName((String) source[2]);
        result.setFractionName((String) source[3]);
        result.setMethodName((String) source[4]);
        CaracteristicType type = Caracteristics.getType((Boolean) source[5],
                                                        (Boolean) source[6]);
        result.setCaracteristicType(type);
        result.setNumericType(Caracteristics.isNumberCaracteristic(result));

        result.setSignifFiguresNumber((Integer) source[7]);
        result.setMaximumNumberDecimals((Integer) source[8]);
        result.setPrecision((Float) source[9]);
        result.setUnit((String) source[10]);
        setStatus((String) source[11], result);

        if (Caracteristics.isQualitativeCaracteristic(result)) {

            // load qualitative values

            Iterator<Object[]> sources = queryListWithStatus(
                    "pmfmQualitativeValues",
                    "pmfmId", IntegerType.INSTANCE, pmfmId);

            List<CaracteristicQualitativeValue> values = Lists.newArrayList();
            while (sources.hasNext()) {
                Object[] source2 = sources.next();
                CaracteristicQualitativeValue target2 = CaracteristicQualitativeValues.newCaracteristicQualitativeValue();
                target2.setId(String.valueOf(source2[0]));
                target2.setName(String.valueOf(source2[1]));
                target2.setDescription(String.valueOf(source2[2]));
                setStatus((String) source2[3], target2);
                values.add(target2);
            }
            result.setQualitativeValue(Collections.unmodifiableList(values));
        }
        return result;
    }

    /**
     * @param pmfmId id of pmfm to test
     * @return {@code true} if the pmfm should NOT be used for caracteristics lists
     * (i.e. because used somewhere when storing some properties into the database)
     */
    protected boolean isProtectedCaracteristic(Integer pmfmId) {
        return propertedPmfmIds.contains(pmfmId);
    }

}

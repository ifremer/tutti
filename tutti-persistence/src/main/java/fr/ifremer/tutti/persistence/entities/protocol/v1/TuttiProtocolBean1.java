package fr.ifremer.tutti.persistence.entities.protocol.v1;

/*
 * #%L
 * Tutti :: Persistence
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.entities.TuttiEntityBean;

import java.util.Collection;
import java.util.List;

/**
 * To migrate old protocol to last version.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.4
 */
public class TuttiProtocolBean1 extends TuttiEntityBean implements TuttiProtocol1 {

    private static final long serialVersionUID = 3847260679792845110L;

    protected String name;

    protected String comment;

    protected List<String> gearUseFeaturePmfmId;

    protected List<String> vesselUseFeaturePmfmId;

    protected List<String> lengthClassesPmfmId;

    protected List<SpeciesProtocol1> species;

    protected List<SpeciesProtocol1> benthos;

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String getComment() {
        return comment;
    }

    @Override
    public void setComment(String comment) {
        this.comment = comment;
    }

    @Override
    public String getGearUseFeaturePmfmId(int index) {
        return getChild(gearUseFeaturePmfmId, index);
    }

    @Override
    public boolean isGearUseFeaturePmfmIdEmpty() {
        return gearUseFeaturePmfmId == null || gearUseFeaturePmfmId.isEmpty();
    }

    @Override
    public int sizeGearUseFeaturePmfmId() {
        return gearUseFeaturePmfmId == null ? 0 : gearUseFeaturePmfmId.size();
    }

    @Override
    public void addGearUseFeaturePmfmId(String gearUseFeaturePmfmId) {
        getGearUseFeaturePmfmId().add(gearUseFeaturePmfmId);
    }

    @Override
    public void addAllGearUseFeaturePmfmId(Collection<String> gearUseFeaturePmfmId) {
        getGearUseFeaturePmfmId().addAll(gearUseFeaturePmfmId);
    }

    @Override
    public boolean removeGearUseFeaturePmfmId(String gearUseFeaturePmfmId) {
        return getGearUseFeaturePmfmId().remove(gearUseFeaturePmfmId);
    }

    @Override
    public boolean removeAllGearUseFeaturePmfmId(Collection<String> gearUseFeaturePmfmId) {
        return getGearUseFeaturePmfmId().removeAll(gearUseFeaturePmfmId);
    }

    @Override
    public boolean containsGearUseFeaturePmfmId(String gearUseFeaturePmfmId) {
        return getGearUseFeaturePmfmId().contains(gearUseFeaturePmfmId);
    }

    @Override
    public boolean containsAllGearUseFeaturePmfmId(Collection<String> gearUseFeaturePmfmId) {
        return getGearUseFeaturePmfmId().containsAll(gearUseFeaturePmfmId);
    }

    @Override
    public List<String> getGearUseFeaturePmfmId() {
        return gearUseFeaturePmfmId;
    }

    @Override
    public void setGearUseFeaturePmfmId(List<String> gearUseFeaturePmfmId) {
        this.gearUseFeaturePmfmId = gearUseFeaturePmfmId;
    }

    @Override
    public String getVesselUseFeaturePmfmId(int index) {
        return getChild(vesselUseFeaturePmfmId, index);
    }

    @Override
    public boolean isVesselUseFeaturePmfmIdEmpty() {
        return vesselUseFeaturePmfmId == null || vesselUseFeaturePmfmId.isEmpty();
    }

    @Override
    public int sizeVesselUseFeaturePmfmId() {
        return vesselUseFeaturePmfmId == null ? 0 : vesselUseFeaturePmfmId.size();
    }

    @Override
    public void addVesselUseFeaturePmfmId(String vesselUseFeaturePmfmId) {
        getVesselUseFeaturePmfmId().add(vesselUseFeaturePmfmId);
    }

    @Override
    public void addAllVesselUseFeaturePmfmId(Collection<String> vesselUseFeaturePmfmId) {
        getVesselUseFeaturePmfmId().addAll(vesselUseFeaturePmfmId);
    }

    @Override
    public boolean removeVesselUseFeaturePmfmId(String vesselUseFeaturePmfmId) {
        return getVesselUseFeaturePmfmId().remove(vesselUseFeaturePmfmId);
    }

    @Override
    public boolean removeAllVesselUseFeaturePmfmId(Collection<String> vesselUseFeaturePmfmId) {
        return getVesselUseFeaturePmfmId().removeAll(vesselUseFeaturePmfmId);
    }

    @Override
    public boolean containsVesselUseFeaturePmfmId(String vesselUseFeaturePmfmId) {
        return getVesselUseFeaturePmfmId().contains(vesselUseFeaturePmfmId);
    }

    @Override
    public boolean containsAllVesselUseFeaturePmfmId(Collection<String> vesselUseFeaturePmfmId) {
        return getVesselUseFeaturePmfmId().containsAll(vesselUseFeaturePmfmId);
    }

    @Override
    public List<String> getVesselUseFeaturePmfmId() {
        return vesselUseFeaturePmfmId;
    }

    @Override
    public void setVesselUseFeaturePmfmId(List<String> vesselUseFeaturePmfmId) {
        this.vesselUseFeaturePmfmId = vesselUseFeaturePmfmId;
    }

    @Override
    public String getLengthClassesPmfmId(int index) {
        return getChild(lengthClassesPmfmId, index);
    }

    @Override
    public boolean isLengthClassesPmfmIdEmpty() {
        return lengthClassesPmfmId == null || lengthClassesPmfmId.isEmpty();
    }

    @Override
    public int sizeLengthClassesPmfmId() {
        return lengthClassesPmfmId == null ? 0 : lengthClassesPmfmId.size();
    }

    @Override
    public void addLengthClassesPmfmId(String lengthClassesPmfmId) {
        getLengthClassesPmfmId().add(lengthClassesPmfmId);
    }

    @Override
    public void addAllLengthClassesPmfmId(Collection<String> lengthClassesPmfmId) {
        getLengthClassesPmfmId().addAll(lengthClassesPmfmId);
    }

    @Override
    public boolean removeLengthClassesPmfmId(String lengthClassesPmfmId) {
        return getLengthClassesPmfmId().remove(lengthClassesPmfmId);
    }

    @Override
    public boolean removeAllLengthClassesPmfmId(Collection<String> lengthClassesPmfmId) {
        return getLengthClassesPmfmId().removeAll(lengthClassesPmfmId);
    }

    @Override
    public boolean containsLengthClassesPmfmId(String lengthClassesPmfmId) {
        return getLengthClassesPmfmId().contains(lengthClassesPmfmId);
    }

    @Override
    public boolean containsAllLengthClassesPmfmId(Collection<String> lengthClassesPmfmId) {
        return getLengthClassesPmfmId().containsAll(lengthClassesPmfmId);
    }

    @Override
    public List<String> getLengthClassesPmfmId() {
        return lengthClassesPmfmId;
    }

    @Override
    public void setLengthClassesPmfmId(List<String> lengthClassesPmfmId) {
        this.lengthClassesPmfmId = lengthClassesPmfmId;
    }

    @Override
    public SpeciesProtocol1 getSpecies(int index) {
        return getChild(species, index);
    }

    @Override
    public boolean isSpeciesEmpty() {
        return species == null || species.isEmpty();
    }

    @Override
    public int sizeSpecies() {
        return species == null ? 0 : species.size();
    }

    @Override
    public void addSpecies(SpeciesProtocol1 species) {
        getSpecies().add(species);
    }

    @Override
    public void addAllSpecies(Collection<SpeciesProtocol1> species) {
        getSpecies().addAll(species);
    }

    @Override
    public boolean removeSpecies(SpeciesProtocol1 species) {
        return getSpecies().remove(species);
    }

    @Override
    public boolean removeAllSpecies(Collection<SpeciesProtocol1> species) {
        return getSpecies().removeAll(species);
    }

    @Override
    public boolean containsSpecies(SpeciesProtocol1 species) {
        return getSpecies().contains(species);
    }

    @Override
    public boolean containsAllSpecies(Collection<SpeciesProtocol1> species) {
        return getSpecies().containsAll(species);
    }

    @Override
    public List<SpeciesProtocol1> getSpecies() {
        return species;
    }

    @Override
    public void setSpecies(List<SpeciesProtocol1> species) {
        this.species = species;
    }

    @Override
    public SpeciesProtocol1 getBenthos(int index) {
        return getChild(benthos, index);
    }

    @Override
    public boolean isBenthosEmpty() {
        return benthos == null || benthos.isEmpty();
    }

    @Override
    public int sizeBenthos() {
        return benthos == null ? 0 : benthos.size();
    }

    @Override
    public void addBenthos(SpeciesProtocol1 benthos) {
        getBenthos().add(benthos);
    }

    @Override
    public void addAllBenthos(Collection<SpeciesProtocol1> benthos) {
        getBenthos().addAll(benthos);
    }

    @Override
    public boolean removeBenthos(SpeciesProtocol1 benthos) {
        return getBenthos().remove(benthos);
    }

    @Override
    public boolean removeAllBenthos(Collection<SpeciesProtocol1> benthos) {
        return getBenthos().removeAll(benthos);
    }

    @Override
    public boolean containsBenthos(SpeciesProtocol1 benthos) {
        return getBenthos().contains(benthos);
    }

    @Override
    public boolean containsAllBenthos(Collection<SpeciesProtocol1> benthos) {
        return getBenthos().containsAll(benthos);
    }

    @Override
    public List<SpeciesProtocol1> getBenthos() {
        return benthos;
    }

    @Override
    public void setBenthos(List<SpeciesProtocol1> benthos) {
        this.benthos = benthos;
    }

}

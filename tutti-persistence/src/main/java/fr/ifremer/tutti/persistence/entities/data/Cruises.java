package fr.ifremer.tutti.persistence.entities.data;

/*
 * #%L
 * Tutti :: Persistence
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.adagio.core.dao.technical.synchronization.SynchronizationStatus;
import org.apache.commons.lang3.builder.EqualsBuilder;

import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;

public class Cruises extends AbstractCruises {

    public static boolean isDirty(Cruise cruise) {
        String synchronizationStatus = cruise.getSynchronizationStatus();
        return SynchronizationStatus.DIRTY.getValue().equals(synchronizationStatus);
    }

    public static boolean isReadyToSynch(Cruise cruise) {
        String synchronizationStatus = cruise.getSynchronizationStatus();
        return SynchronizationStatus.READY_TO_SYNCHRONIZE.getValue().equals(synchronizationStatus);
    }

    public static boolean isSynch(Cruise cruise) {
        String synchronizationStatus = cruise.getSynchronizationStatus();
        return SynchronizationStatus.SYNCHRONIZED.getValue().equals(synchronizationStatus);
    }

    public static boolean equalsNaturalId(Cruise cruise1, String naturalId2) {

        String naturalId1 = getNaturalId(cruise1);
        return Objects.equals(naturalId1, naturalId2);

    }

    public static boolean equals(Cruise cruise1, Cruise cruise2) {

        EqualsBuilder equalsBuilder = new EqualsBuilder();

        Calendar instance = Calendar.getInstance();
        int year1;

        if (cruise1.getBeginDate() == null) {
            year1 = -1;
        } else {
            instance.setTime(cruise1.getBeginDate());
            year1 = instance.get(Calendar.YEAR);
        }

        int year2;
        if (cruise1.getBeginDate() == null) {
            year2 = -2;
        } else {
            instance.setTime(cruise2.getBeginDate());
            year2 = instance.get(Calendar.YEAR);
        }
        equalsBuilder.append(year1, year2);
        String surveyPart1 = cruise1.getSurveyPart();
        if (surveyPart1 == null) {
            surveyPart1 = "";
        }
        String surveyPart2 = cruise2.getSurveyPart();
        if (surveyPart2 == null) {
            surveyPart2 = "";
        }
        equalsBuilder.append(surveyPart1, surveyPart2);
        equalsBuilder.append(cruise1.getProgram(), cruise2.getProgram());

        return equalsBuilder.isEquals();

    }

    public static String getNaturalId(Cruise cruise) {

        Calendar instance = Calendar.getInstance();
        int year1;

        if (cruise.getBeginDate() == null) {
            year1 = -1;
        } else {
            instance.setTime(cruise.getBeginDate());
            year1 = instance.get(Calendar.YEAR);
        }
        String surveyPart1 = cruise.getSurveyPart();
        if (surveyPart1 == null) {
            surveyPart1 = "";
        }
        String programId = cruise.getProgram().getId();

        return year1 + "--" + surveyPart1 + "--" + programId;

    }

    public static void sort(List<Cruise> cruises) {
        Collections.sort(cruises, CRUISE_COMPARATOR);
    }

    public static Comparator<Cruise> CRUISE_COMPARATOR = (o1, o2) -> o1.getName().compareTo(o2.getName());

}

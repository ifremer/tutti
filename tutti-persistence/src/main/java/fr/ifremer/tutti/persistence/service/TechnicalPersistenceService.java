package fr.ifremer.tutti.persistence.service;

/*
 * #%L
 * Tutti :: Persistence
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.TuttiPersistenceServiceImplementor;
import fr.ifremer.tutti.persistence.entities.referential.TuttiReferentialEntity;
import org.nuiton.version.Version;
import org.springframework.transaction.annotation.Transactional;

import java.util.concurrent.Callable;

/**
 * Contains some technical services.
 *
 * Created on 4/20/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.5
 */
@Transactional(readOnly = true)
public interface TechnicalPersistenceService extends TuttiPersistenceServiceImplementor {

    boolean isTemporary(TuttiReferentialEntity entity);

    /**
     * To clear all caches.
     *
     * @since 1.0.1
     */
    void clearAllCaches();

    /**
     * To invoke the given call code.
     *
     * <strong>Note:</strong> this is mainly to execute a code in a single
     * transaction.
     *
     * @param call call to invoke
     * @param <V>  return type
     * @return the return of the call
     * @since 2.4
     */
    @Transactional(readOnly = false)
    <V> V invoke(Callable<V> call);

    Version getSchemaVersion();

    Version getSchemaVersionIfUpdate();

    /**
     * To init the update schema context.
     *
     * We are initialized it with current schema version, next schema version and use the call back if needed to
     * interact with user.
     *
     * @param <U>     type of context
     * @param context context to fill.
     * @since 3.12
     */
    <U extends UpdateSchemaContextSupport> void prepareUpdateSchemaContext(U context);

    @Transactional(readOnly = false)
    void updateSchema();

    @Transactional(readOnly = false)
    void sanityDb();
}

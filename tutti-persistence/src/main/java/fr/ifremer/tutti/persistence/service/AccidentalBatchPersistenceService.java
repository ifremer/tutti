package fr.ifremer.tutti.persistence.service;

/*
 * #%L
 * Tutti :: Persistence
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.TuttiPersistenceServiceImplementor;
import fr.ifremer.tutti.persistence.entities.data.AccidentalBatch;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.List;

/**
 * CRUD of {@link AccidentalBatch} entity.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 0.3
 */
@Transactional(readOnly = true)
public interface AccidentalBatchPersistenceService extends TuttiPersistenceServiceImplementor {

    List<AccidentalBatch> getAllAccidentalBatch(Integer fishingOperationId);

    @Transactional(readOnly = false)
    AccidentalBatch createAccidentalBatch(AccidentalBatch bean);

    @Transactional(readOnly = false)
    Collection<AccidentalBatch> createAccidentalBatches(Collection<AccidentalBatch> beans);

    @Transactional(readOnly = false)
    AccidentalBatch saveAccidentalBatch(AccidentalBatch bean);

    @Transactional(readOnly = false)
    void deleteAccidentalBatch(String id);

    @Transactional(readOnly = false)
    void deleteAccidentalBatchForFishingOperation(Integer fishingOperationId);
}

package fr.ifremer.tutti.persistence.entities.referential;

/*
 * #%L
 * Tutti :: Persistence
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Predicate;

import java.util.Collection;
import java.util.Iterator;

public class CaracteristicQualitativeValues extends AbstractCaracteristicQualitativeValues {

    public static CaracteristicQualitativeValue getQualitativeValue(Caracteristic caracteristic, Integer qualitativeValueId) {
        CaracteristicQualitativeValue value = null;
        for (CaracteristicQualitativeValue qv : caracteristic.getQualitativeValue()) {
            if (qualitativeValueId.equals(qv.getIdAsInt())) {
                value = qv;
                break;
            }
        }
        return value;
    }

    public static CaracteristicQualitativeValue getQualitativeValue(Caracteristic caracteristic, String qualitativeValueId) {
        CaracteristicQualitativeValue value = null;
        for (CaracteristicQualitativeValue qv : caracteristic.getQualitativeValue()) {
            if (qualitativeValueId.equals(qv.getId())) {
                value = qv;
                break;
            }
        }
        return value;
    }

    public static void removeQualitativeValue(Collection<CaracteristicQualitativeValue> values, int id) {
        Iterator<CaracteristicQualitativeValue> iterator = values.iterator();
        while (iterator.hasNext()) {
            CaracteristicQualitativeValue next = iterator.next();
            if (id == next.getIdAsInt()) {
                iterator.remove();
                break;
            }
        }
    }

    protected static class CaracteristicQualitativeValuePredicate implements Predicate<CaracteristicQualitativeValue> {

        private final Integer id;

        public CaracteristicQualitativeValuePredicate(Integer id) {
            this.id = id;
        }

        @Override
        public boolean apply(CaracteristicQualitativeValue input) {
            return id.equals(input.getIdAsInt());
        }
    }
}
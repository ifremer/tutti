package fr.ifremer.tutti.persistence.service.util.tree;

/*
 * #%L
 * Tutti :: Persistence
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Joiner;
import com.google.common.collect.Sets;
import fr.ifremer.adagio.core.dao.data.batch.Batch;
import fr.ifremer.adagio.core.dao.data.batch.Batchs;
import fr.ifremer.adagio.core.dao.data.batch.CatchBatch;
import fr.ifremer.adagio.core.dao.data.batch.CatchBatchExtendDao;
import fr.ifremer.adagio.core.dao.data.batch.SortingBatch;
import fr.ifremer.adagio.core.dao.data.measure.QuantificationMeasurement;
import fr.ifremer.adagio.core.dao.data.measure.SortingMeasurement;
import fr.ifremer.adagio.core.dao.referential.QualityFlagCode;
import fr.ifremer.adagio.core.dao.referential.QualityFlagImpl;
import fr.ifremer.adagio.core.dao.referential.pmfm.PmfmId;
import fr.ifremer.adagio.core.dao.referential.pmfm.QualitativeValueId;
import fr.ifremer.adagio.core.dao.referential.taxon.ReferenceTaxon;
import fr.ifremer.adagio.core.dao.referential.taxon.ReferenceTaxonImpl;
import fr.ifremer.tutti.persistence.service.AbstractPersistenceService;
import fr.ifremer.tutti.persistence.service.util.MeasurementPersistenceHelper;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.SystemUtils;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Set;

/**
 * Helper to build or navigauet into the batch tree.
 *
 * Created on 4/20/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.5
 */
public abstract class BatchTreeHelperSupport extends AbstractPersistenceService {

    public static final Integer SORTING_TYPE_ID = PmfmId.SCIENTIFIC_CRUISE_SORTING_TYPE.getValue();

    public static final Integer SORTING_TYPE2_ID = PmfmId.SCIENTIFIC_CRUISE_SORTING_TYPE2.getValue();

    /** Logger. */
    private static final Log log = LogFactory.getLog(BatchTreeHelperSupport.class);

    @Resource(name = "measurementPersistenceHelper")
    protected MeasurementPersistenceHelper measurementPersistenceHelper;

    @Resource(name = "catchBatchDao")
    protected CatchBatchExtendDao catchBatchDao;

    protected Comparator<Batch> batchComparator = Batchs.newRankOrderComparator();

    //------------------------------------------------------------------------//
    //-- Get CatchBatch navigation methods                                  --//
    //------------------------------------------------------------------------//

    public final SortingBatch getVracBatch(CatchBatch batch) {
        return getSortingBatch(batch,
                               "Vrac",
                               PmfmId.SORTED_UNSORTED.getValue(),
                               QualitativeValueId.SORTED_VRAC.getValue());
    }

    public final SortingBatch getHorsVracBatch(CatchBatch batch) {
        return getSortingBatch(batch,
                               "Hors Vrac",
                               PmfmId.SORTED_UNSORTED.getValue(),
                               QualitativeValueId.SORTED_HORS_VRAC.getValue());
    }

    public final SortingBatch getOrCreateVracBatch(CatchBatch batch, Float weight, Float weightBeforeSampling) {
        return getOrCreate(batch,
                           batch,
                           "Vrac",
                           PmfmId.SORTED_UNSORTED.getValue(),
                           QualitativeValueId.SORTED_VRAC.getValue(),
                           weight,
                           weightBeforeSampling,
                           (short) 1);
    }

    public final SortingBatch getOrCreateHorsVracBatch(CatchBatch batch) {
        return getOrCreate(batch,
                           batch,
                           "Hors Vrac",
                           PmfmId.SORTED_UNSORTED.getValue(),
                           QualitativeValueId.SORTED_HORS_VRAC.getValue(),
                           (short) 2);
    }

    public final void setWeightAndSampleRatio(SortingBatch target, Float weight, Float weightBeforeSampling) {

        catchBatchDao.setSortingBatchWeights(target,
                                             weight,
                                             weightBeforeSampling,
                                             PmfmId.WEIGHT_MEASURED.getValue(),
                                             measurementPersistenceHelper.getRecorderDepartmentId());

    }

    public final void setSortingSamplingRatio(SortingBatch target, Float weight, Float weightBeforeSampling) {

        catchBatchDao.setSortingSamplingRatio(target, weight, weightBeforeSampling);

    }

    //------------------------------------------------------------------------//
    //-- Internal methods                                                   --//
    //------------------------------------------------------------------------//

    protected final SortingBatch get(Batch parentBatch, Integer sortingPmfmId, Integer sortingQualitativeValueId) {
        return getSortingBatch(
                parentBatch,
                null,
                sortingPmfmId,
                sortingQualitativeValueId);
    }

    protected final SortingBatch getSortingBatch(Batch source, String debugMessage, Integer... ids) {

        return getSortingBatch(source.getChildBatchs(), debugMessage, ids);
    }

    protected final SortingBatch getSortingBatch(Collection<Batch> childs, String debugMessage, Integer... ids) {

        int nbParams = ids.length / 2;

        Object[] params = new Object[nbParams * 3];
        for (int i = 0; i < nbParams; i++) {
            Integer sortingPmfmId = ids[2 * i];
            Integer sortingQualitativeValueId = ids[2 * i + 1];
            params[3 * i] = CatchBatchExtendDao.PMFM_ID;
            params[3 * i + 1] = sortingPmfmId;
            params[3 * i + 2] = sortingQualitativeValueId;
        }
        SortingBatch result = catchBatchDao.getSortingBatch(childs, params);
        if (result != null && debugMessage != null && log.isDebugEnabled()) {
            log.debug("Loaded " + debugMessage + ": " + result.getId());
        }
        return result;
    }

    protected final SortingBatch getOrCreate(CatchBatch rootBatch,
                                             Batch batch,
                                             String debugMessage,
                                             Integer sortingPmfmId,
                                             Integer sortingQualitativeValueId,
                                             short rankOrder) {
        return getOrCreate(rootBatch,
                           batch,
                           debugMessage,
                           sortingPmfmId,
                           sortingQualitativeValueId,
                           null,
                           null,
                           rankOrder);
    }

    protected final SortingBatch getOrCreate(CatchBatch rootBatch,
                                             Batch batch,
                                             String debugMessage,
                                             Integer sortingPmfmId,
                                             Integer sortingQualitativeValueId,
                                             Float totalWeight,
                                             short rankOrder) {
        return getOrCreate(rootBatch,
                           batch,
                           debugMessage,
                           sortingPmfmId,
                           sortingQualitativeValueId,
                           totalWeight,
                           null,
                           rankOrder);
    }

    protected final SortingBatch getOrCreate(CatchBatch rootBatch,
                                             Batch batch,
                                             String debugMessage,
                                             Integer sortingPmfmId,
                                             Integer sortingQualitativeValueId,
                                             Float weight,
                                             Float weightBeforeSampling,
                                             short rankOrder) {

        SortingBatch result = getSortingBatch(
                batch,
                debugMessage,
                sortingPmfmId,
                sortingQualitativeValueId);

        if (result == null) {

            result = SortingBatch.Factory.newInstance();
            if (batch.getChildBatchs() == null) {
                batch.setChildBatchs(Sets.<Batch>newHashSet());
            }
            batch.getChildBatchs().add(result);

            // --- Some mandatory properties --- //

            QualityFlagImpl qualityFlag = load(QualityFlagImpl.class, QualityFlagCode.NOTQUALIFIED.getValue());
            result.setQualityFlag(qualityFlag);
            result.setRootBatch(rootBatch);
            result.setParentBatch(batch);
            result.setExhaustiveInventory(true);
            result.setRankOrder(rankOrder);

            // No taxon or taxon group
            result.setReferenceTaxon(null);
            result.setTaxonGroup(null);

            // --- Sorting measurement --- //

            Collection<SortingMeasurement> sortingMeasurements = result.getSortingMeasurements();
            Set<SortingMeasurement> notChangedSortingMeasurements = Sets.newHashSet();
            if (sortingMeasurements != null) {
                notChangedSortingMeasurements.addAll(sortingMeasurements);
            }

            if (sortingPmfmId != null && sortingQualitativeValueId != null) {
                SortingMeasurement sm = measurementPersistenceHelper.setSortingMeasurement(
                        result,
                        sortingPmfmId,
                        sortingQualitativeValueId);
                notChangedSortingMeasurements.remove(sm);
            }
            if (sortingMeasurements != null) {
                sortingMeasurements.removeAll(notChangedSortingMeasurements);
            }

            catchBatchDao.createSortingBatch(result, rootBatch);
        }

        // --- Sampling Ratio + QuantificationMeasurement --- //

        setWeightAndSampleRatio(result, weight, weightBeforeSampling);

        return result;
    }

    //------------------------------------------------------------------------//
    //-- Debug methods                                                      --//
    //------------------------------------------------------------------------//

    public final void displayCatchBatch(CatchBatch result) {
        StringBuilder sb = new StringBuilder();
        displayBatch(result, 0, sb);
        log.info(sb.toString());
    }

    protected final void displayBatch(Batch batch, int level, StringBuilder sb) {

        ToStringStyle style = new BatchTreeToStringStyle();

        ToStringBuilder builder = new ToStringBuilder(batch, style);
        builder.append("id", batch.getId());
        builder.append("rankOrder", batch.getRankOrder());

        if (batch instanceof CatchBatch) {
            CatchBatch catchBatch = (CatchBatch) batch;
            builder.append("synchronizationStatus", catchBatch.getSynchronizationStatus());
        }
        if (batch instanceof SortingBatch) {
            SortingBatch sortingBatch = (SortingBatch) batch;
            if (sortingBatch.getSamplingRatio() != null) {
                builder.append("samplingRatio", sortingBatch.getSamplingRatio());
            }
            if (sortingBatch.getSamplingRatioText() != null) {
                builder.append("samplingRatioText", sortingBatch.getSamplingRatioText());
            }
            if (sortingBatch.getIndividualCount() != null) {
                builder.append("individualCount", sortingBatch.getIndividualCount());
            }
            if (sortingBatch.getReferenceTaxon() != null) {
                ReferenceTaxon referenceTaxon = sortingBatch.getReferenceTaxon();
                builder.append("referenceTaxon", load(ReferenceTaxonImpl.class, referenceTaxon.getId()).getName());
            }

            SortingMeasurement sm = null;
            if (sortingBatch.getSortingMeasurements() != null && sortingBatch.getSortingMeasurements().size() == 1) {
                sm = sortingBatch.getSortingMeasurements().iterator().next();
            } else if (sortingBatch.getReferenceTaxon() != null && sortingBatch.getReferenceTaxon().getId() != null) {
                sm = measurementPersistenceHelper.getInheritedSortingMeasurement(sortingBatch);
            }
            if (sm != null) {
                String sortingMeasurementStr = measurementPersistenceHelper.toString(sm);
                builder.append("sortingMeasurement", sortingMeasurementStr);
            }
        }
        QuantificationMeasurement quantificationMeasurement = measurementPersistenceHelper.getWeightMeasurementQuantificationMeasurement(batch);
        if (quantificationMeasurement != null) {
            String quantificationMeasurementStr = measurementPersistenceHelper.toString(quantificationMeasurement);
            builder.append("weightQuantificationMeasurement", quantificationMeasurementStr);
            builder.append("weightQuantificationMeasurement.isReferenceQuantification", quantificationMeasurement.getIsReferenceQuantification());
        }
        if (batch.getWeight() != null) {
            builder.append("weight", batch.getWeight());
        }
        if (batch.getWeightBeforeSampling() != null) {
            builder.append("weightBeforeSampling", batch.getWeightBeforeSampling());
        }
        builder.append("qualityFlag", load(QualityFlagImpl.class, batch.getQualityFlag().getCode()).getName());

        String prefix = "\n" + StringUtils.leftPad("", 2 * level);
        String batchStr = Joiner.on(prefix).join(builder.build().split("\n"));
        sb.append(prefix).append(batchStr);
        Collection<Batch> childBatchs = batch.getChildBatchs();
        if (childBatchs != null) {
            List<Batch> childBatchList = new ArrayList<>(childBatchs);
            Collections.sort(childBatchList, batchComparator);
            for (Batch childBatch : childBatchList) {
                displayBatch(childBatch, level + 1, sb);
            }
        }
    }

    public final CatchBatch loadCatchBatch(Integer catchBatchId) {
        return catchBatchDao.loadFullTreeWithCache(catchBatchId, PmfmId.WEIGHT_MEASURED.getValue(), true, true);
    }


    static final class BatchTreeToStringStyle extends ToStringStyle {

        private static final long serialVersionUID = 1L;

        BatchTreeToStringStyle() {
            super();
            this.setUseClassName(true);
            this.setUseShortClassName(true);
            this.setContentStart("");
            this.setFieldSeparator(SystemUtils.LINE_SEPARATOR + "  | ");
            this.setFieldSeparatorAtStart(true);
            this.setContentEnd("");
        }
    }
}

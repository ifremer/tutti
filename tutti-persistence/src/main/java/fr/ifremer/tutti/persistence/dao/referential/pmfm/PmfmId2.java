package fr.ifremer.tutti.persistence.dao.referential.pmfm;

/*
 * #%L
 * Tutti :: Persistence
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2016 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import fr.ifremer.adagio.core.dao.referential.pmfm.PmfmId;
import fr.ifremer.adagio.core.dao.technical.AdagioEnumerationDef;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import static org.nuiton.i18n.I18n.n;

/**
 * L'extension de {@link PmfmId} pour les pmfm non encore géré dans adagio.
 *
 * Created on 25/04/16.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 4.5
 */
public enum PmfmId2 implements Serializable, AdagioEnumerationDef<Integer> {

    /** TODO: Model Documentation for Enumeration Literal SAMPLE_ID value 1435 */
    SAMPLE_ID(
            "adagio.enumeration.PmfmId.SAMPLE_ID",
            n("adagio.enumeration.PmfmId.SAMPLE_ID.description"),
            1435),
    /** TODO: Model Documentation for Enumeration Literal COPY_METHOD value 1762 */
    COPY_METHOD(
            "adagio.enumeration.PmfmId.COPY_METHOD",
            n("adagio.enumeration.PmfmId.COPY_METHOD.description"),
            1762),

    /** TODO: Model Documentation for Enumeration Literal CALCIFIED_STRUCTURE value 1807 */
    CALCIFIED_STRUCTURE(
            "adagio.enumeration.PmfmId.CALCIFIED_STRUCTURE",
            n("adagio.enumeration.PmfmId.CALCIFIED_STRUCTURE.description"),
            1807);

    private static final long serialVersionUID = 1L;

    private String key;
    private String description;
    private Integer enumValue;

    private PmfmId2(String key, String description, Integer value) {
        this.key = key;
        this.description = description;
        this.enumValue = value;
    }

    @Override
    public void setValue(Integer newValue) {
        if (newValue != null && !this.enumValue.equals(newValue)) {
            // Update static lists
            values.remove(this.enumValue);
            literals.remove(this.enumValue);
            this.enumValue = newValue;
            values.put(this.enumValue, this);
            literals.add(this.enumValue);
        }
    }


    @Override
    public String getValueAsString() {
        return String.valueOf(this.enumValue);
    }

    /**
     * Retrieves an instance of PmfmId2 from <code>its name</code>.
     *
     * @param name the name to create the PmfmId2 from.
     * @return The enumeration literal named after the 'name' argument
     */
    public static PmfmId2 fromString(String name) {
        return PmfmId2.valueOf(name);
    }

    /**
     * Returns an enumeration literal Integer <code>value</code>.
     * Required by JAXB2 enumeration implementation
     *
     * @return Integer with corresponding value
     */
    public Integer value() {
        return this.enumValue;
    }

    /**
     * Returns an instance of PmfmId2 from Integer <code>value</code>.
     * Required by JAXB2 enumeration implementation
     *
     * @param value the value to create the PmfmId2 from.
     * @return static Enumeration with corresponding value
     */
    public static PmfmId2 fromValue(Integer value) {
        for (PmfmId2 enumName : PmfmId2.values()) {
            if (enumName.getValue().equals(value)) {
                return enumName;
            }
        }
        throw new IllegalArgumentException("PmfmId2.fromValue(" + value.toString() + ')');
    }

    /**
     * Gets the underlying value of this type safe enumeration.
     * This method is necessary to comply with DaoBase implementation.
     *
     * @return The name of this literal.
     */
    public Integer getValue() {
        return this.enumValue;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public String getKey() {
        return key;
    }

    @Override
    public Class<?> getType() {
        return Integer.class;
    }

    /**
     * Returns an unmodifiable list containing the literals that are known by this enumeration.
     *
     * @return A List containing the actual literals defined by this enumeration, this list
     * can not be modified.
     */
    public static List<Integer> literals() {
        return PmfmId2.literals;
    }

    /**
     * Returns an unmodifiable list containing the names of the literals that are known
     * by this enumeration.
     *
     * @return A List containing the actual names of the literals defined by this
     * enumeration, this list can not be modified.
     */
    public static List<String> names() {
        return PmfmId2.names;
    }

    private static final Map<Integer, PmfmId2> values = ImmutableMap
            .<Integer, PmfmId2>builder()
            .put(SAMPLE_ID.enumValue, SAMPLE_ID)
            .put(COPY_METHOD.enumValue, COPY_METHOD)
            .put(CALCIFIED_STRUCTURE.enumValue, CALCIFIED_STRUCTURE)
            .build();

    private static final List<Integer> literals = ImmutableList.of(SAMPLE_ID.enumValue,
                                                                   COPY_METHOD.enumValue,
                                                                   CALCIFIED_STRUCTURE.enumValue);

    private static final List<String> names = ImmutableList.of("SAMPLE_ID",
                                                               "COPY_METHOD",
                                                               "CALCIFIED_STRUCTURE");

}
package fr.ifremer.tutti.persistence.entities.protocol;

/*
 * #%L
 * Tutti :: Persistence
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.esotericsoftware.yamlbeans.YamlConfig;
import com.esotericsoftware.yamlbeans.YamlReader;
import com.esotericsoftware.yamlbeans.YamlWriter;
import com.google.common.base.Joiner;
import com.google.common.base.Preconditions;
import com.google.common.collect.Collections2;
import com.google.common.collect.Maps;
import com.google.common.collect.Multimap;
import com.google.common.io.Files;
import fr.ifremer.adagio.core.dao.referential.pmfm.PmfmId;
import fr.ifremer.tutti.persistence.TuttiPersistence;
import fr.ifremer.tutti.persistence.entities.data.SampleCategoryModel;
import fr.ifremer.tutti.persistence.entities.protocol.v1.SpeciesProtocol1;
import fr.ifremer.tutti.persistence.entities.protocol.v1.SpeciesProtocolBean1;
import fr.ifremer.tutti.persistence.entities.protocol.v1.TuttiProtocol1;
import fr.ifremer.tutti.persistence.entities.protocol.v1.TuttiProtocolBean1;
import fr.ifremer.tutti.persistence.entities.protocol.v2.TuttiProtocol2;
import fr.ifremer.tutti.persistence.entities.protocol.v2.TuttiProtocolBean2;
import fr.ifremer.tutti.persistence.entities.protocol.v3.SpeciesProtocol3;
import fr.ifremer.tutti.persistence.entities.protocol.v3.SpeciesProtocolBean3;
import fr.ifremer.tutti.persistence.entities.protocol.v3.TuttiProtocol3;
import fr.ifremer.tutti.persistence.entities.protocol.v3.TuttiProtocolBean3;
import fr.ifremer.tutti.persistence.entities.referential.Caracteristic;
import fr.ifremer.tutti.persistence.entities.referential.Species;
import fr.ifremer.tutti.persistence.entities.referential.Speciess;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.decorator.Decorator;
import org.nuiton.jaxx.application.ApplicationTechnicalException;
import org.nuiton.util.beans.Binder;
import org.nuiton.util.beans.BinderFactory;

import java.io.BufferedWriter;
import java.io.File;
import java.io.Reader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.TreeMap;

import static org.nuiton.i18n.I18n.t;

/**
 * Helper class around {@link TuttiProtocol}.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.0
 */
public class TuttiProtocols extends AbstractTuttiProtocols {

    /** Logger. */
    private static final Log log = LogFactory.getLog(TuttiProtocols.class);

    public static final Integer CURRENT_PROTOCOL_VERSION = 4;

    public static void toFile(TuttiProtocol protocol, File file) {

        String id = protocol.getId();

        BufferedWriter fileWriter = null;
        try {
            fileWriter = Files.newWriter(file, StandardCharsets.UTF_8);
            YamlWriter writer = new YamlWriter(fileWriter, createConfig());
            writer.write(protocol);
            writer.close();
            fileWriter.close();
        } catch (Exception e) {
            throw new ApplicationTechnicalException(t("tutti.persistence.protocol.fromFile.error", id, file), e);//"Could not transform protocol " + to file
        } finally {
            IOUtils.closeQuietly(fileWriter);
        }
    }

    public static TuttiProtocol fromFile(File file) {
        TuttiProtocol result;
        Reader fileReader = null;
        try {
            fileReader = Files.newReader(file, StandardCharsets.UTF_8);
            YamlReader reader = new YamlReader(fileReader, createConfig());
            result = reader.read(typeOfTuttiProtocol());
            fileReader.close();

            if (!CURRENT_PROTOCOL_VERSION.equals(result.getVersion())) {
                result = migrateProtocol(file);
            }

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Error loading protocol, try to migrate", e);
            }
            result = migrateProtocol(file);

        } finally {
            IOUtils.closeQuietly(fileReader);
        }

        Integer sampleCategoryIdToRemove = PmfmId.SORTED_UNSORTED.getValue();
        // transform String to Integer...
        if (!result.isBenthosEmpty()) {
            for (SpeciesProtocol speciesProtocol : result.getBenthos()) {
                List mandatorySampleCategoryId = speciesProtocol.getMandatorySampleCategoryId();
                List<Integer> mandatorySampleCategoryIdInteger = new ArrayList<>();
                for (Object o : mandatorySampleCategoryId) {
                    mandatorySampleCategoryIdInteger.add(Integer.valueOf(o.toString()));
                }
                mandatorySampleCategoryIdInteger.remove(sampleCategoryIdToRemove);
                speciesProtocol.setMandatorySampleCategoryId(mandatorySampleCategoryIdInteger);

            }
        }
        if (!result.isSpeciesEmpty()) {
            for (SpeciesProtocol speciesProtocol : result.getSpecies()) {
                List mandatorySampleCategoryId = speciesProtocol.getMandatorySampleCategoryId();
                List<Integer> mandatorySampleCategoryIdInteger = new ArrayList<>();
                for (Object o : mandatorySampleCategoryId) {
                    mandatorySampleCategoryIdInteger.add(Integer.valueOf(o.toString()));
                }
                mandatorySampleCategoryIdInteger.remove(sampleCategoryIdToRemove);
                speciesProtocol.setMandatorySampleCategoryId(mandatorySampleCategoryIdInteger);

            }
        }

        return result;
    }

    public static void translateReferenceTaxonIds(TuttiProtocol result, Map<Integer, Integer> idTranslationMap) {

        List<SpeciesProtocol> species = result.getSpecies();
        translateReferenceTaxonIds(species, idTranslationMap);

        List<SpeciesProtocol> benthos = result.getBenthos();
        translateReferenceTaxonIds(benthos, idTranslationMap);

    }

    protected static void translateReferenceTaxonIds(List<SpeciesProtocol> species, Map<Integer, Integer> idTranslationMap) {

        for (SpeciesProtocol speciesProtocol : species) {
            Integer speciesReferenceTaxonId = speciesProtocol.getSpeciesReferenceTaxonId();
            if (idTranslationMap.containsKey(speciesReferenceTaxonId)) {

                Integer newSpeciesReferenceTaxonId = idTranslationMap.get(speciesReferenceTaxonId);
                speciesProtocol.setSpeciesReferenceTaxonId(newSpeciesReferenceTaxonId);

                if (log.isInfoEnabled()) {
                    log.info("Translate reference taxon from " + speciesReferenceTaxonId + " to " + newSpeciesReferenceTaxonId);
                }

            }
        }
    }

    protected static TuttiProtocol migrateProtocol(File file) {
        try {
            //try to load a v3
            TuttiProtocol3 tuttiProtocol3 = fromFileV3(file);

            return fromTuttiProtocol3(tuttiProtocol3);

        } catch (Exception ee) {
            if (log.isErrorEnabled()) {
                log.error("Error on loading a v3", ee);
            }
            try {
                //try to load a v2
                TuttiProtocol2 tuttiProtocol2 = fromFileV2(file);

                TuttiProtocol3 tuttiProtocol3 = fromTuttiProtocol2(tuttiProtocol2);

                return fromTuttiProtocol3(tuttiProtocol3);

            } catch (Exception ee2) {
                if (log.isErrorEnabled()) {
                    log.error("Error on loading a v2", ee2);
                }
                // try to load a v1
                TuttiProtocol1 tuttiProtocol1 = fromFileV1(file);

                TuttiProtocol2 tuttiProtocol2 = fromTuttiProtocol1(tuttiProtocol1);

                TuttiProtocol3 tuttiProtocol3 = fromTuttiProtocol2(tuttiProtocol2);

                return fromTuttiProtocol3(tuttiProtocol3);
            }
        }
    }

    public static TuttiProtocol1 fromFileV1(File file) {

        Reader fileReader = null;
        try {
            fileReader = Files.newReader(file, StandardCharsets.UTF_8);
            YamlReader reader = new YamlReader(fileReader, createConfigV1());
            TuttiProtocol1 result = reader.read(TuttiProtocolBean1.class);
            fileReader.close();
            return result;
        } catch (Exception e) {
            throw new ApplicationTechnicalException(t("tutti.persistence.protocol.fromFile.error", file), e);
        } finally {
            IOUtils.closeQuietly(fileReader);
        }
    }

    public static TuttiProtocol2 fromFileV2(File file) {

        Reader fileReader = null;
        try {
            fileReader = Files.newReader(file, StandardCharsets.UTF_8);
            YamlReader reader = new YamlReader(fileReader, createConfigV2());
            TuttiProtocol2 result = reader.read(TuttiProtocolBean2.class);
            fileReader.close();
            return result;
        } catch (Exception e) {
            throw new ApplicationTechnicalException(t("tutti.persistence.protocol.fromFile.error", file), e);
        } finally {
            IOUtils.closeQuietly(fileReader);
        }
    }

    public static TuttiProtocol3 fromFileV3(File file) {

        Reader fileReader = null;
        try {
            fileReader = Files.newReader(file, StandardCharsets.UTF_8);
            YamlReader reader = new YamlReader(fileReader, createConfigV3());
            TuttiProtocol3 result = reader.read(TuttiProtocolBean3.class);
            fileReader.close();
            return result;
        } catch (Exception e) {
            throw new ApplicationTechnicalException(t("tutti.persistence.protocol.fromFile.error", file), e);
        } finally {
            IOUtils.closeQuietly(fileReader);
        }
    }

    public static void checkSampleCategories(SampleCategoryModel sampleCategoryModel,
                                             TuttiProtocol protocol,
                                             Set<Integer> badCategories) {

        if (CollectionUtils.isNotEmpty(protocol.getSpecies())) {
            for (SpeciesProtocol entry : protocol.getSpecies()) {
                List<Integer> mandatorySampleCategoryId = entry.getMandatorySampleCategoryId();
                for (Integer categoryId : mandatorySampleCategoryId) {
                    if (!sampleCategoryModel.containsCategoryId(categoryId)) {
                        badCategories.add(categoryId);
                    }
                }
            }
        }
        if (CollectionUtils.isNotEmpty(protocol.getBenthos())) {
            for (SpeciesProtocol entry : protocol.getBenthos()) {
                List<Integer> mandatorySampleCategoryId = entry.getMandatorySampleCategoryId();
                for (Integer categoryId : mandatorySampleCategoryId) {
                    if (!sampleCategoryModel.containsCategoryId(categoryId)) {
                        badCategories.add(categoryId);
                    }
                }
            }
        }
    }

    public static void removeBadCategories(SampleCategoryModel sampleCategoryModel, TuttiProtocol protocol) {
        List<Integer> samplingOrder = sampleCategoryModel.getSamplingOrder();
        if (!protocol.isSpeciesEmpty()) {

            for (SpeciesProtocol entry : protocol.getSpecies()) {
                entry.getMandatorySampleCategoryId().retainAll(samplingOrder);
            }
        }

        if (!protocol.isBenthosEmpty()) {

            for (SpeciesProtocol entry : protocol.getBenthos()) {
                entry.getMandatorySampleCategoryId().retainAll(samplingOrder);
            }
        }
    }

    public static String getBadCategoriesMessage(Set<Integer> badCategories,
                                                 Decorator<Caracteristic> decorator,
                                                 TuttiPersistence persistenceService) {
        List<String> badCategoriesStr = new ArrayList<>();

        for (Integer id : badCategories) {
            String caracteristicStr;
            try {
                Caracteristic caracteristic = persistenceService.getCaracteristic(id);
                caracteristicStr = decorator.toString(caracteristic);
            } catch (NullPointerException e) {
                if (log.isWarnEnabled()) {
                    log.warn("Could not find caracteristic with id: " + id);
                }
                caracteristicStr = t("tutti.persistence.error.caracteristic.notFound");
            }
            badCategoriesStr.add("<li>" + id + " : " + caracteristicStr + "</li>");
        }
        return t("tutti.persistence.error.protocol.categories.not.compatible", Joiner.on("").join(badCategoriesStr));
    }

    protected static TuttiProtocol2 fromTuttiProtocol1(TuttiProtocol1 tuttiProtocol1) {
        TuttiProtocol2 result = new TuttiProtocolBean2();
        Binder<TuttiProtocol1, TuttiProtocol2> binder = BinderFactory.newBinder(TuttiProtocol1.class, TuttiProtocol2.class);
        binder.copy(tuttiProtocol1, result);
        if (!tuttiProtocol1.isSpeciesEmpty()) {
            result.setSpecies(new ArrayList<>());
            Binder<SpeciesProtocol1, SpeciesProtocol> binderSpecies = BinderFactory.newBinder(SpeciesProtocol1.class, SpeciesProtocol.class);
            for (SpeciesProtocol1 speciesProtocol1 : tuttiProtocol1.getSpecies()) {
                SpeciesProtocol row = SpeciesProtocols.newSpeciesProtocol();
                row.setMandatorySampleCategoryId(new ArrayList<>());
                binderSpecies.copy(speciesProtocol1, row);
                if (speciesProtocol1.isAgeEnabled()) {
                    row.addMandatorySampleCategoryId(PmfmId.AGE.getValue());
                }
                if (speciesProtocol1.isSizeEnabled()) {
                    row.addMandatorySampleCategoryId(PmfmId.SIZE_CATEGORY.getValue());
                }
                if (speciesProtocol1.isMaturityEnabled()) {
                    row.addMandatorySampleCategoryId(PmfmId.MATURITY.getValue());
                }
                if (speciesProtocol1.isSexEnabled()) {
                    row.addMandatorySampleCategoryId(PmfmId.SEX.getValue());
                }
                result.addSpecies(row);
            }
        }
        if (!tuttiProtocol1.isBenthosEmpty()) {
            result.setBenthos(new ArrayList<>());
            Binder<SpeciesProtocol1, SpeciesProtocol> binderSpecies = BinderFactory.newBinder(SpeciesProtocol1.class, SpeciesProtocol.class);
            for (SpeciesProtocol1 speciesProtocol1 : tuttiProtocol1.getBenthos()) {
                SpeciesProtocol row = SpeciesProtocols.newSpeciesProtocol();
                row.setMandatorySampleCategoryId(new ArrayList<>());
                binderSpecies.copy(speciesProtocol1, row);
                if (speciesProtocol1.isAgeEnabled()) {
                    row.addMandatorySampleCategoryId(PmfmId.AGE.getValue());
                }
                if (speciesProtocol1.isSizeEnabled()) {
                    row.addMandatorySampleCategoryId(PmfmId.SIZE_CATEGORY.getValue());
                }
                if (speciesProtocol1.isMaturityEnabled()) {
                    row.addMandatorySampleCategoryId(PmfmId.MATURITY.getValue());
                }
                if (speciesProtocol1.isSexEnabled()) {
                    row.addMandatorySampleCategoryId(PmfmId.SEX.getValue());
                }
                result.addBenthos(row);
            }
        }
        return result;
    }

    protected static TuttiProtocol3 fromTuttiProtocol2(TuttiProtocol2 tuttiProtocol2) {
        TuttiProtocol3 result = new TuttiProtocolBean3();
        Binder<TuttiProtocol2, TuttiProtocol3> binder = BinderFactory.newBinder(TuttiProtocol2.class, TuttiProtocol3.class);
        binder.copy(tuttiProtocol2, result);

        List<CaracteristicMappingRow> caracteristicMappingRows = new ArrayList<>();
        for (String id : tuttiProtocol2.getGearUseFeaturePmfmId()) {
            CaracteristicMappingRow row = new CaracteristicMappingRowBean();
            row.setPmfmId(id);
            row.setTab(CaracteristicType.GEAR_USE_FEATURE.name());
            caracteristicMappingRows.add(row);
        }
        for (String id : tuttiProtocol2.getVesselUseFeaturePmfmId()) {
            CaracteristicMappingRow row = new CaracteristicMappingRowBean();
            row.setPmfmId(id);
            row.setTab(CaracteristicType.VESSEL_USE_FEATURE.name());
            caracteristicMappingRows.add(row);
        }
        result.setCaracteristicMapping(caracteristicMappingRows);

        //TODO finish

        return result;
    }

    protected static TuttiProtocol fromTuttiProtocol3(TuttiProtocol3 tuttiProtocol3) {
        TuttiProtocol result = newTuttiProtocol();
        Binder<TuttiProtocol3, TuttiProtocol> binder = BinderFactory.newBinder(TuttiProtocol3.class, TuttiProtocol.class);
        binder.copy(tuttiProtocol3, result);

        if (!tuttiProtocol3.isSpeciesEmpty()) {
            result.setSpecies(new ArrayList<>());
            Binder<SpeciesProtocol3, SpeciesProtocol> binderSpecies = BinderFactory.newBinder(SpeciesProtocol3.class, SpeciesProtocol.class);
            for (SpeciesProtocol3 speciesProtocol3 : tuttiProtocol3.getSpecies()) {
                SpeciesProtocol row = SpeciesProtocols.newSpeciesProtocol();
                row.setMandatorySampleCategoryId(new ArrayList<>());
                binderSpecies.copy(speciesProtocol3, row);
                result.addSpecies(row);
            }
        }
        if (!tuttiProtocol3.isBenthosEmpty()) {
            result.setBenthos(new ArrayList<>());
            Binder<SpeciesProtocol3, SpeciesProtocol> binderSpecies = BinderFactory.newBinder(SpeciesProtocol3.class, SpeciesProtocol.class);
            for (SpeciesProtocol3 speciesProtocol3 : tuttiProtocol3.getBenthos()) {
                SpeciesProtocol row = SpeciesProtocols.newSpeciesProtocol();
                row.setMandatorySampleCategoryId(new ArrayList<>());
                binderSpecies.copy(speciesProtocol3, row);
                result.addBenthos(row);
            }
        }

        return result;
    }

    protected static YamlConfig createConfig() {
        YamlConfig result = new YamlConfig();
        result.setClassTag(SpeciesProtocol.class.getSimpleName(),
                           SpeciesProtocols.typeOfSpeciesProtocol());
        result.setClassTag(CalcifiedPiecesSamplingDefinition.class.getSimpleName(),
                           CalcifiedPiecesSamplingDefinitions.typeOfCalcifiedPiecesSamplingDefinition());
        result.setClassTag(CaracteristicMappingRow.class.getSimpleName(),
                           CaracteristicMappingRows.typeOfCaracteristicMappingRow());
        result.setClassTag(Zone.class.getSimpleName(), Zones.typeOfZone());
        result.setClassTag(Strata.class.getSimpleName(), Stratas.typeOfStrata());
        result.setClassTag(SubStrata.class.getSimpleName(), SubStratas.typeOfSubStrata());
        result.setClassTag(MaturityCaracteristic.class.getSimpleName(), MaturityCaracteristics.typeOfMaturityCaracteristic());
        result.writeConfig.setAlwaysWriteClassname(false);
        result.writeConfig.setWriteRootTags(false);
        return result;
    }

    protected static YamlConfig createConfigV1() {
        YamlConfig result = new YamlConfig();
        result.setClassTag(SpeciesProtocol.class.getSimpleName(),
                           SpeciesProtocolBean1.class);
        result.writeConfig.setAlwaysWriteClassname(false);
        result.writeConfig.setWriteRootTags(false);
        return result;
    }

    protected static YamlConfig createConfigV2() {
        YamlConfig result = new YamlConfig();
        result.setClassTag(SpeciesProtocol.class.getSimpleName(),
                           SpeciesProtocols.typeOfSpeciesProtocol());
        result.writeConfig.setAlwaysWriteClassname(false);
        result.writeConfig.setWriteRootTags(false);
        return result;
    }

    protected static YamlConfig createConfigV3() {
        YamlConfig result = new YamlConfig();
        result.setClassTag(SpeciesProtocol.class.getSimpleName(), SpeciesProtocolBean3.class);
        result.setClassTag(CaracteristicMappingRow.class.getSimpleName(),
                           CaracteristicMappingRows.typeOfCaracteristicMappingRow());
        result.setClassTag(Zone.class.getSimpleName(), Zones.typeOfZone());
        result.setClassTag(Strata.class.getSimpleName(), Stratas.typeOfStrata());
        result.setClassTag(SubStrata.class.getSimpleName(), SubStratas.typeOfSubStrata());
        result.setClassTag(CalcifiedPiecesSamplingDefinition.class.getSimpleName(),
                           CalcifiedPiecesSamplingDefinitions.typeOfCalcifiedPiecesSamplingDefinition());
        result.writeConfig.setAlwaysWriteClassname(false);
        result.writeConfig.setWriteRootTags(false);
        return result;
    }

    public static SpeciesProtocol getSpeciesOrBenthosProtocol(TuttiProtocol protocol, Integer speciesReferenceTaxonId) {
        SpeciesProtocol speciesProtocol = getSpeciesProtocol(protocol, speciesReferenceTaxonId);
        if (speciesProtocol == null) {
            speciesProtocol = getBenthosProtocol(protocol, speciesReferenceTaxonId);
        }
        return speciesProtocol;
    }

    public static boolean removeSpeciesOrBenthosProtocol(TuttiProtocol protocol, Integer speciesReferenceTaxonId) {

        boolean wasRemoved = false;

        SpeciesProtocol speciesProtocol = getSpeciesProtocol(protocol, speciesReferenceTaxonId);
        if (speciesProtocol != null) {

            if (log.isInfoEnabled()) {
                log.info("Remove referenceTaxonId " + speciesReferenceTaxonId + " from species protocol " + speciesProtocol.getSpeciesSurveyCode());
            }
            protocol.getSpecies().remove(speciesProtocol);
            wasRemoved = true;

        } else {

            SpeciesProtocol benthosProtocol = getBenthosProtocol(protocol, speciesReferenceTaxonId);

            if (benthosProtocol != null) {

                if (log.isInfoEnabled()) {
                    log.info("Remove referenceTaxonId " + speciesReferenceTaxonId + " from benthos protocol " + benthosProtocol.getSpeciesSurveyCode());
                }
                protocol.getBenthos().remove(benthosProtocol);
                wasRemoved = true;

            }

        }

        return wasRemoved;

    }

    /**
     * Return the species Protocol corresponding to the species of the given protocol.
     *
     * @param protocol                protocol to scan
     * @param speciesReferenceTaxonId species referenceTaxonId filter
     * @return the species protocol for the given species
     * @since 2.5
     */
    public static SpeciesProtocol getSpeciesProtocol(TuttiProtocol protocol, Integer speciesReferenceTaxonId) {
        return getSpeciesProtocol(speciesReferenceTaxonId, protocol.getSpecies());
    }

    /**
     * Return the benthos Protocol corresponding to the species of the given protocol.
     *
     * @param protocol                protocol to scan
     * @param speciesReferenceTaxonId species referenceTaxonId filter
     * @return the benthos protocol for the given species
     * @since 3.9
     */
    public static SpeciesProtocol getBenthosProtocol(TuttiProtocol protocol, Integer speciesReferenceTaxonId) {
        return getSpeciesProtocol(speciesReferenceTaxonId, protocol.getBenthos());
    }

    /**
     * Return the speciesProtocol row corresponding to the species of the species protocols.
     *
     * @param speciesProtocols species protocols to scan
     * @param species          species filter
     * @return the species protocol for the given species
     */
    public static SpeciesProtocol getSpeciesProtocol(Species species,
                                                     List<SpeciesProtocol> speciesProtocols) {

        return getSpeciesProtocol(species.getReferenceTaxonId(), speciesProtocols);

    }

    /**
     * Return the speciesProtocol row corresponding to the species of the species protocols.
     *
     * @param speciesProtocols        species protocols to scan
     * @param speciesReferenceTaxonId speciesReferenceTaxonId filter
     * @return the species protocol for the given species
     */
    public static SpeciesProtocol getSpeciesProtocol(Integer speciesReferenceTaxonId,
                                                     List<SpeciesProtocol> speciesProtocols) {
        for (SpeciesProtocol speciesProtocol : speciesProtocols) {
            if (speciesReferenceTaxonId.equals(speciesProtocol.getSpeciesReferenceTaxonId())) {
                return speciesProtocol;
            }
        }
        return null;
    }

    public static CaracteristicMappingRow caracteristicMappingRow(String pmfmId, CaracteristicType tab) {
        CaracteristicMappingRow caracteristicMappingRow = new CaracteristicMappingRowBean();
        caracteristicMappingRow.setPmfmId(pmfmId);
        caracteristicMappingRow.setTab(tab.name());
        return caracteristicMappingRow;
    }

    public static List<String> gearUseFeaturePmfmIds(TuttiProtocol protocol) {
        return pmfmIdsForCaracteristicType(protocol, CaracteristicType.GEAR_USE_FEATURE);
    }

    public static List<String> vesselUseFeaturePmfmIds(TuttiProtocol protocol) {
        return pmfmIdsForCaracteristicType(protocol, CaracteristicType.VESSEL_USE_FEATURE);
    }

    public static List<String> pmfmIdsForCaracteristicType(TuttiProtocol protocol, final CaracteristicType type) {
        Preconditions.checkNotNull(protocol);
        Preconditions.checkNotNull(type);

        List<CaracteristicMappingRow> caracteristicMapping = protocol.getCaracteristicMapping();
        Collection<CaracteristicMappingRow> gearUseFeatureCaracteristics =
                Collections2.filter(caracteristicMapping, new CaracteristicMappingRows.TabPredicate(type));

        Collection<String> pmfmIds = Collections2.transform(gearUseFeatureCaracteristics, CaracteristicMappingRows.GET_PMFM_ID);
        return new ArrayList<>(pmfmIds);
    }

    public static String getFirstAvailableName(String name, List<String> allProtocolNames) {

        String baseName = name + "-";
        int index = 0;
        do {

            name = baseName + (index++);

        } while (allProtocolNames.contains(name));

        return name;

    }

    public static Map<Integer, SpeciesProtocol> toSpeciesProtocolMap(TuttiProtocol protocol) {
        final Map<Integer, SpeciesProtocol> result = Maps.newHashMap();
        for (SpeciesProtocol sp : protocol.getSpecies()) {
            result.put(sp.getSpeciesReferenceTaxonId(), sp);
        }
        return result;
    }

    public static Map<Integer, SpeciesProtocol> toBenthosProtocolMap(TuttiProtocol protocol) {
        final Map<Integer, SpeciesProtocol> result = Maps.newHashMap();
        for (SpeciesProtocol sp : protocol.getBenthos()) {
            result.put(sp.getSpeciesReferenceTaxonId(), sp);
        }
        return result;
    }

    public static Map<Integer, String> detectMissingSpecies(TuttiProtocol protocol, List<Species> species) {

        return TuttiProtocols.detectMissingSpecies(species, toSpeciesProtocolMap(protocol));

    }

    public static Map<Integer, String> detectMissingBenthos(TuttiProtocol protocol, List<Species> species) {

        return TuttiProtocols.detectMissingSpecies(species, toBenthosProtocolMap(protocol));

    }

    public static void removeBadSpecies(TuttiProtocol protocol, Map<Integer, String> speciesProtocolNotFound) {

        if (!speciesProtocolNotFound.isEmpty()) {

            Iterator<SpeciesProtocol> iterator = protocol.getSpecies().iterator();
            while (iterator.hasNext()) {
                SpeciesProtocol speciesProtocol = iterator.next();
                Integer taxonId = speciesProtocol.getSpeciesReferenceTaxonId();
                boolean containsSpecies = speciesProtocolNotFound.containsKey(taxonId);
                if (containsSpecies) {
                    if (log.isWarnEnabled()) {
                        log.warn("Could not find protocol species " + taxonId + " (" + speciesProtocol.getSpeciesSurveyCode() + ") in referential.");
                    }
                    iterator.remove();
                }
            }

        }

    }

    public static void removeBadBenthos(TuttiProtocol protocol, Map<Integer, String> benthosProtocolNotFound) {

        if (!benthosProtocolNotFound.isEmpty()) {

            Iterator<SpeciesProtocol> iterator = protocol.getBenthos().iterator();
            while (iterator.hasNext()) {
                SpeciesProtocol speciesProtocol = iterator.next();
                Integer taxonId = speciesProtocol.getSpeciesReferenceTaxonId();
                boolean containsSpecies = benthosProtocolNotFound.containsKey(taxonId);
                if (containsSpecies) {
                    if (log.isWarnEnabled()) {
                        log.warn("Could not find protocol benthos " + taxonId + " (" + speciesProtocol.getSpeciesSurveyCode() + ") in referential.");
                    }
                    iterator.remove();
                }
            }

        }

    }

    protected static Map<Integer, String> detectMissingSpecies(List<Species> species, Map<Integer, SpeciesProtocol> speciesProtocolMap) {

        Multimap<String, Species> speciesByRefTaxonId = Speciess.splitByReferenceTaxonId(species);
        Map<Integer, String> badSpecies = new TreeMap<>();
        for (SpeciesProtocol speciesProtocol : speciesProtocolMap.values()) {
            Integer referenceTaxonId = speciesProtocol.getSpeciesReferenceTaxonId();
            if (referenceTaxonId != null && !speciesByRefTaxonId.containsKey(String.valueOf(referenceTaxonId))) {
                badSpecies.put(referenceTaxonId, speciesProtocol.getSpeciesSurveyCode());
            }
        }
        return Collections.unmodifiableMap(badSpecies);

    }

    public static String getBadSpeciesMessage(Map<Integer, String> speciesProtocolNotFound) {

        List<String> badCategoriesStr = new ArrayList<>();
        for (Map.Entry<Integer, String> id : speciesProtocolNotFound.entrySet()) {
            badCategoriesStr.add("<li>" + id.getKey() + " : " + (id.getValue() == null ? "" : id.getValue()) + "</li>");
        }
        return t("tutti.persistence.error.protocol.species.not.found", Joiner.on("").join(badCategoriesStr));

    }

    public static String getBadBenthosMessage(Map<Integer, String> benthosProtocolNotFound) {

        List<String> badCategoriesStr = new ArrayList<>();
        for (Map.Entry<Integer, String> id : benthosProtocolNotFound.entrySet()) {
            badCategoriesStr.add("<li>" + id.getKey() + " : " + (id.getValue() == null ? "" : id.getValue()) + "</li>");
        }
        return t("tutti.persistence.error.protocol.benthos.not.found", Joiner.on("").join(badCategoriesStr));

    }

    public static boolean matchProgramId(TuttiProtocol protocol, String programId) {
        Preconditions.checkNotNull(protocol, "Protocol can't be null.");
        return Objects.equals(programId, protocol.getProgramId());
    }

}
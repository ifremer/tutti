package fr.ifremer.tutti.persistence.service;

/*
 * #%L
 * Tutti :: Persistence
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import fr.ifremer.adagio.core.dao.administration.programStrategy.ProgramCode;
import fr.ifremer.adagio.core.dao.administration.programStrategy.ProgramDao;
import fr.ifremer.adagio.core.dao.referential.gear.GearClassificationId;
import fr.ifremer.adagio.core.dao.referential.gear.GearClassificationImpl;
import fr.ifremer.adagio.core.dao.referential.location.Location;
import fr.ifremer.adagio.core.dao.referential.location.LocationClassificationId;
import fr.ifremer.adagio.core.dao.referential.location.LocationDao;
import fr.ifremer.adagio.core.dao.referential.location.LocationLevelId;
import fr.ifremer.adagio.core.dao.referential.taxon.TaxonGroupTypeCode;
import fr.ifremer.adagio.core.dao.referential.taxon.TaxonGroupTypeImpl;
import fr.ifremer.tutti.persistence.entities.data.Program;
import fr.ifremer.tutti.persistence.entities.data.Programs;
import fr.ifremer.tutti.persistence.entities.referential.TuttiLocation;
import fr.ifremer.tutti.persistence.entities.referential.TuttiLocations;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.type.IntegerType;
import org.hibernate.type.StringType;
import org.springframework.dao.DataRetrievalFailureException;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

/**
 * @author Tony Chemit - chemit@codelutin.com
 * @since 0.3
 */
@Service("programPersistenceService")
public class ProgramPersistenceServiceImpl extends AbstractPersistenceService implements ProgramPersistenceService {

    /** Logger. */
    private static final Log log =
            LogFactory.getLog(ProgramPersistenceServiceImpl.class);

    @Resource(name = "programDao")
    protected ProgramDao programDao;

    @Resource(name = "locationDao")
    protected LocationDao locationDao;

    protected final int maxCodeLengthInDatabase = 40;

    @Override
    public List<Program> getAllProgram() {
        String codePattern = "%";
        if (ProgramCode.SCIENTIFIC_CRUISE_PREFIX.getValue() != null) {
            codePattern = ProgramCode.SCIENTIFIC_CRUISE_PREFIX.getValue() + codePattern;
        }
        Iterator<Object[]> list = queryList(
                "allPrograms",
                "codePattern", StringType.INSTANCE, codePattern,
                "locationLevelId", IntegerType.INSTANCE, LocationLevelId.SCIENTIFIC_CRUISE_PROGRAM.getValue(),
                "locationClassificationId", IntegerType.INSTANCE, LocationClassificationId.SECTOR.getValue()
        );

        List<Program> result = Lists.newArrayList();
        while (list.hasNext()) {
            Object[] source = list.next();
            Program target = Programs.newProgram();
            loadProgram(target, source);
            result.add(target);
        }
        return Collections.unmodifiableList(result);
    }

    @Override
    public Program getProgram(String id) {
        Iterator<Object[]> list = queryList(
                "program",
                "programCode", StringType.INSTANCE, id,
                "locationLevelId", IntegerType.INSTANCE, LocationLevelId.SCIENTIFIC_CRUISE_PROGRAM.getValue(),
                "locationClassificationId", IntegerType.INSTANCE, LocationClassificationId.SECTOR.getValue());

        Program result;

        if (list.hasNext()) {

            // Keep only the first row (=the first location, if many found)
            Object[] source = list.next();

            result = Programs.newProgram();
            loadProgram(result, source);
        } else {
            result = null;
        }
        return result;
    }

    @Override
    public Program createProgram(Program bean) {
        Preconditions.checkNotNull(bean);
        Preconditions.checkNotNull(bean.getName());
        Preconditions.checkNotNull(bean.getDescription());
        Preconditions.checkArgument(bean.getId() == null);

        if (log.isDebugEnabled()) {
            log.debug("Create program with name: " + bean.getName());
        }
        fr.ifremer.adagio.core.dao.administration.programStrategy.Program program = fr.ifremer.adagio.core.dao.administration.programStrategy.Program.Factory.newInstance();
        programToEntity(bean, program);
        program = programDao.create(program);
        bean.setId(String.valueOf(program.getCode()));

        return bean;
    }

    @Override
    public Program saveProgram(Program bean) {
        Preconditions.checkNotNull(bean);
        Preconditions.checkNotNull(bean.getName());
        Preconditions.checkNotNull(bean.getId());
        Preconditions.checkNotNull(bean.getDescription());

        if (log.isDebugEnabled()) {
            log.debug("Save program with name: " + bean.getName());
        }
        fr.ifremer.adagio.core.dao.administration.programStrategy.Program program = programDao.load(bean.getId());
        if (program == null) {
            throw new DataRetrievalFailureException("Could not retrieve program with code=" + bean.getId());
        }

        programToEntity(bean, program);
        programDao.update(program);

        return bean;
    }

    // ------------------------------------------------------------------------//
    // -- Internal methods --//
    // ------------------------------------------------------------------------//

    protected void loadProgram(Program result, Object[] source) {
        result.setId((String) source[0]);
        result.setName((String) source[1]);
        result.setDescription((String) source[2]);
        if (source[3] != null) {
            TuttiLocation zone = TuttiLocations.newTuttiLocation();
            zone.setId(String.valueOf(source[3]));
            zone.setLabel((String) source[4]);
            zone.setName((String) source[5]);
            result.setZone(zone);
        }
    }

    protected void programToEntity(Program source,
                                   fr.ifremer.adagio.core.dao.administration.programStrategy.Program target) {
        // Code : compute with : <prefixe><name>
        if (target.getCode() == null && source.getName() != null) {
            // Compute a program code (remove spaces, and capitalize the name)
            String programCode = source.getName().toUpperCase().replaceAll(" ", "_");
            // Add a prefix
            if (ProgramCode.SCIENTIFIC_CRUISE_PREFIX.getValue() != null) {
                programCode = ProgramCode.SCIENTIFIC_CRUISE_PREFIX.getValue() + programCode;
            }
            // Trunc the code if too long 
            if (programCode.length() > maxCodeLengthInDatabase) {
                programCode = programCode.substring(0, maxCodeLengthInDatabase - 1);
            }
            target.setCode(programCode);

            // Mandatory properties :
            // Gear classification :
            target.setGearClassification(load(GearClassificationImpl.class, GearClassificationId.SCIENTIFIC_CRUISE.getValue()));

            // taxon group type
            target.setTaxonGroupType(load(TaxonGroupTypeImpl.class, TaxonGroupTypeCode.COMMERCIAL_SPECIES.getValue()));

            // Creation date
            target.setCreationDate(newCreateDate());
        }

        // Name (mandatory in database)
        target.setName(source.getName());

        // Description (mandatory in database)
        target.setDescription(source.getDescription());

        // Zone 
        if (source.getZone() == null) {
            // Remove program location classifications :
            if (target.getLocationClassifications() != null) {
                target.getLocationClassifications().clear();
            }
            // Remove program locations :
            if (target.getLocations() != null) {
                target.getLocations().clear();
            }
        } else if (source.getZone() != null && source.getZone().getId() != null) {
            Location location = locationDao.load(Integer.valueOf(source.getZone().getId()));

            // Program location classifications :
            if (target.getLocationClassifications() == null) {
                target.setLocationClassifications(Lists.newArrayList(location.getLocationClassification()));
            } else {
                target.getLocationClassifications().clear();
                target.getLocationClassifications().add(location.getLocationClassification());
            }

            // Program locations :
            if (target.getLocations() == null) {
                target.setLocations(Lists.newArrayList(location));
            } else {
                target.getLocations().clear();
                target.getLocations().add(location);
            }
        }
    }

//    public int getProgramNameMaxLength() {
//        int maxCodeLengthInDatabase = 40;
//        if (ProgramCode.SCIENTIFIC_CRUISE_PREFIX.getValue() == null
//            || ProgramCode.SCIENTIFIC_CRUISE_PREFIX.getValue().trim().isEmpty()) {
//            return maxCodeLengthInDatabase;
//        }
//        return (maxCodeLengthInDatabase - ProgramCode.SCIENTIFIC_CRUISE_PREFIX.getValue().length());
//    }
}

package fr.ifremer.tutti.persistence.entities.data;

/*
 * #%L
 * Tutti :: Persistence
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Lists;
import fr.ifremer.tutti.persistence.entities.TuttiEntityBean;

import java.util.List;

/**
 * Container of batches.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.0.2
 */
public class BatchContainer<B extends TuttiBatchEntity> extends TuttiEntityBean {

    private static final long serialVersionUID = 1L;

    protected final List<B> children = Lists.newArrayList();

    public List<B> getChildren() {
        return children;
    }

    public void addChildren(B batch) {
        children.add(batch);
    }

    public int sizeChildren() {
        return children.size();
    }

    public boolean isEmptyChildren() {
        return children.isEmpty();
    }
}

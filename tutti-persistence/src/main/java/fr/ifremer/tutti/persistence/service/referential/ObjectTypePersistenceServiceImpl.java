package fr.ifremer.tutti.persistence.service.referential;

/*
 * #%L
 * Tutti :: Persistence
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Lists;
import fr.ifremer.tutti.persistence.entities.referential.ObjectType;
import fr.ifremer.tutti.persistence.entities.referential.ObjectTypes;
import org.hibernate.type.StringType;
import org.springframework.cache.Cache;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.Iterator;
import java.util.List;

/**
 * Created on 11/3/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.8
 */
@Service("objectTypePersistenceService")
public class ObjectTypePersistenceServiceImpl extends ReferentialPersistenceServiceSupport implements ObjectTypePersistenceService {

    @Override
    public List<ObjectType> getAllObjectType() {
        Iterator<Object[]> sources = queryList("allObjectType");
        List<ObjectType> result = Lists.newArrayList();
        Cache cache = cacheService.getCache("objectTypeByCode");
        while (sources.hasNext()) {
            Object[] source = sources.next();
            String code = (String) source[0];
            ObjectType target = loadObjectType(source);
            cache.put(code, target);
            result.add(target);
        }
        return Collections.unmodifiableList(result);
    }

    @Override
    public ObjectType getObjectType(String objectTypeCode) {
        Object[] source = queryUnique("objectType",
                                      "objectTypeCode", StringType.INSTANCE, objectTypeCode);
        return loadObjectType(source);
    }

    protected ObjectType loadObjectType(Object[] source) {
        ObjectType result = ObjectTypes.newObjectType();
        result.setId((String) source[0]);
        result.setName((String) source[1]);
        result.setDescription((String) source[2]);
        return result;
    }

}

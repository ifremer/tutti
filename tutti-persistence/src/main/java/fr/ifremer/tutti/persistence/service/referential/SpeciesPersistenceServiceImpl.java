package fr.ifremer.tutti.persistence.service.referential;

/*
 * #%L
 * Tutti :: Persistence
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import fr.ifremer.adagio.core.dao.referential.taxon.TaxonName;
import fr.ifremer.adagio.core.dao.referential.taxon.TaxonNameImpl;
import fr.ifremer.adagio.core.dao.referential.taxon.TaxonRefVO;
import fr.ifremer.adagio.core.dao.referential.transcribing.TranscribingItemType;
import fr.ifremer.adagio.core.dao.referential.transcribing.TranscribingItemTypeDao;
import fr.ifremer.adagio.core.dao.referential.transcribing.TranscribingItemTypeId;
import fr.ifremer.adagio.core.dao.technical.hibernate.TemporaryDataHelper;
import fr.ifremer.tutti.persistence.dao.TaxonNameDaoTutti;
import fr.ifremer.tutti.persistence.entities.referential.Species;
import fr.ifremer.tutti.persistence.entities.referential.Speciess;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Query;
import org.hibernate.type.IntegerType;
import org.hibernate.type.StringType;
import org.nuiton.jaxx.application.ApplicationBusinessException;
import org.springframework.cache.Cache;
import org.springframework.dao.DataRetrievalFailureException;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 * Created on 11/3/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.8
 */
@Service("speciesPersistenceService")
public class SpeciesPersistenceServiceImpl extends ReferentialPersistenceServiceSupport implements SpeciesPersistenceService {

    /** Logger. */
    private static final Log log = LogFactory.getLog(SpeciesPersistenceServiceImpl.class);

    public static final String TAXINOMIE_COMMUN_REFERENCE_HISTORY = "TAXINOMIE-COMMUN.REFERENCE_HISTORY";

    @Resource(name = "taxonNameDaoTutti")
    protected TaxonNameDaoTutti taxonNameDao;

    @Resource(name = "transcribingItemTypeDao")
    protected TranscribingItemTypeDao transcribingItemTypeDao;

    @Override
    public List<Species> getAllSpecies() {

        TaxonRefVO[] sources = taxonNameDao.getAllTaxonNames(
                true, TranscribingItemTypeId.TAXON_NAME_REFTAX_CODE.getValue());

        List<Species> result = Lists.newArrayListWithCapacity(sources.length);
        List<Species> referenceTaxonsOnly = Lists.newArrayList();
        Cache referentSpeciesByIdCache = cacheService.getCache("referentSpeciesById");
        for (TaxonRefVO source : sources) {
            Species target = loadSpecies(source);

            // FIXME kmorin 20130423 http://forge.codelutin.com/issues/2350
            target.setRefTaxCode(StringUtils.trim(target.getExternalCode()));

            if (target.isReferenceTaxon()) {

                // Add to cache :
                referentSpeciesByIdCache.put(target.getReferenceTaxonId(), target);

                referenceTaxonsOnly.add(target);
            }
            result.add(target);
        }

        // Add to cache :
        Cache allReferentSpeciesCache = cacheService.getCache("referentSpecies");
        allReferentSpeciesCache.put("", referenceTaxonsOnly);

        return Collections.unmodifiableList(result);

    }

    @Override
    public List<Species> getAllReferentSpecies() {

        TaxonRefVO[] sources = taxonNameDao.getAllTaxonNames(
                false, TranscribingItemTypeId.TAXON_NAME_REFTAX_CODE.getValue());
        List<Species> result = Lists.newArrayListWithCapacity(sources.length);
        Cache referentSpeciesByIdCache = cacheService.getCache("referentSpeciesById");
        loadReferentSpecies(sources, result, referentSpeciesByIdCache);

        return Collections.unmodifiableList(result);

    }

    @Override
    public List<Species> getAllReferentSpeciesWithObsoletes() {

        TaxonRefVO[] sources = taxonNameDao.getAllTaxonNamesWithObsoletes(
                false, TranscribingItemTypeId.TAXON_NAME_REFTAX_CODE.getValue());
        List<Species> result = Lists.newArrayListWithCapacity(sources.length);

        loadReferentSpecies(sources, result, null);
        return Collections.unmodifiableList(result);

    }

    protected void loadReferentSpecies(TaxonRefVO[] sources, List<Species> result, Cache referentSpeciesByIdCache) {
        for (TaxonRefVO source : sources) {
            Species target = loadSpecies(source);

            // FIXME kmorin 20130423 http://forge.codelutin.com/issues/2350
            target.setRefTaxCode(StringUtils.trim(target.getExternalCode()));

            if (referentSpeciesByIdCache != null) {
                // Add to cache
                referentSpeciesByIdCache.put(target.getReferenceTaxonId(), target);
            }

            result.add(target);
        }
    }

    @Override
    public Species getSpeciesByReferenceTaxonId(Integer referenceTaxonId) {

        try {
            Species result = getSpeciesByReferenceTaxonId(
                    referenceTaxonId,
                    TranscribingItemTypeId.TAXON_NAME_REFTAX_CODE.getValue());
            if (result != null) {
                result.setRefTaxCode(result.getExternalCode());
            }
            return result;
        } catch (Exception e) {
            throw new CouldNotLoadTaxonException(referenceTaxonId, "Could not getSpeciesByReferenceTaxonId with referenceTaxonId: " + referenceTaxonId, e);
        }
    }

    @Override
    public Species getSpeciesByReferenceTaxonIdWithVernacularCode(Integer referenceTaxonId) {

        try {
            Species result = getSpeciesByReferenceTaxonId(
                    referenceTaxonId,
                    TranscribingItemTypeId.TAXON_NAME_LOCAL_NAME.getValue());
            if (result != null) {
                result.setVernacularCode(result.getExternalCode());
            }
            return result;
        } catch (Exception e) {
            throw new CouldNotLoadTaxonException(referenceTaxonId, "Could not getSpeciesByReferenceTaxonId with referenceTaxonId: " + referenceTaxonId, e);
        }


    }

    @Override
    public Map<Integer, Integer> getAllObsoleteReferentTaxons() {

        TranscribingItemType transcribingItemType = transcribingItemTypeDao.searchUniqueLabel(TAXINOMIE_COMMUN_REFERENCE_HISTORY);

        Map<Integer, Integer> result = new TreeMap<>();

        if (transcribingItemType == null) {

            if (log.isWarnEnabled()) {
                log.warn("Could not find transcribing item type with label: '" + TAXINOMIE_COMMUN_REFERENCE_HISTORY + "'");
            }

        } else {

            Query query = createQuery("allTranscribingForAType", "transcribingTypeId", IntegerType.INSTANCE, transcribingItemType.getId());

            for (Object o : query.list()) {
                Object[] cols = (Object[]) o;
                Integer referencetaxonId = (Integer) cols[0];
                String externalCode = (String) cols[1];
                result.put(Integer.valueOf(externalCode), referencetaxonId);
            }

            if (log.isInfoEnabled()) {
                log.info("Loaded allObsoleteReferentTaxons : " + result.size());
            }

            if (log.isDebugEnabled()) {
                log.info("Loaded allObsoleteReferentTaxons : " + result.keySet());
            }

        }

        return result;

    }

    protected Species getSpeciesByReferenceTaxonId(Integer referenceTaxonId,
                                                   Integer transcribingTypeId) {

        Species target;
        try {
            TaxonRefVO source = taxonNameDao.getTaxonNameReferent(
                    referenceTaxonId, transcribingTypeId);
            target = loadSpecies(source);
        } catch (DataRetrievalFailureException drfe) {
            target = null;
        }
        return target;

    }

    @Override
    public boolean isTemporarySpeciesUsed(Integer referenceTaxonId) {

        Long count = queryUniqueTyped("countReferenceTaxonInSortingBatch", "id", IntegerType.INSTANCE, referenceTaxonId);
        boolean result = count > 0;

        if (!result) {
            count = queryUniqueTyped("countReferenceTaxonInSample", "id", IntegerType.INSTANCE, referenceTaxonId);
            result = count > 0;
        }
        return result;

    }

    @Override
    public List<Species> addTemporarySpecies(List<Species> species) {

        List<Species> result = Lists.newArrayList();
        for (Species source : species) {
            Species added = addTemporarySpecies(source);
            result.add(added);
        }
        return Collections.unmodifiableList(result);

    }

    @Override
    public List<Species> updateTemporarySpecies(List<Species> species) {

        List<Species> result = Lists.newArrayList();
        for (Species source : species) {
            Species updated = updateTemporarySpecies(source);
            result.add(updated);
        }
        return Collections.unmodifiableList(result);

    }

    @Override
    public List<Species> linkTemporarySpecies(List<Species> species) {

        List<Species> result = Lists.newArrayList();
        for (Species source : species) {
            Species linked = linkTemporarySpecies(source);
            result.add(linked);
        }
        return Collections.unmodifiableList(result);

    }

    @Override
    public void replaceSpecies(Species source, Species target, boolean delete) {

        Preconditions.checkNotNull(source);
        Preconditions.checkNotNull(target);
        Preconditions.checkState(Speciess.isTemporary(source));
        Preconditions.checkState(!Speciess.isTemporary(target));

        Integer sourceId = source.getReferenceTaxonId();
        Integer targetId = target.getReferenceTaxonId();

        queryUpdate("replaceReferenceTaxonInSortingBatch",
                "sourceId", IntegerType.INSTANCE, sourceId,
                "targetId", IntegerType.INSTANCE, targetId);

        queryUpdate("replaceReferenceTaxonInSample",
                "sourceId", IntegerType.INSTANCE, sourceId,
                "targetId", IntegerType.INSTANCE, targetId);

        //TODO Check doublon...

        if (delete) {

            deleteTemporarySpecies(sourceId);

        }

    }

    @Override
    public void deleteTemporarySpecies(Collection<Integer> referenceTaxonIds) {

        for (Integer id : referenceTaxonIds) {
            deleteTemporarySpecies(id);
        }

    }

    @Override
    public void deleteTemporarySpecies(Integer referenceTaxonId) {

        Preconditions.checkNotNull(referenceTaxonId);
        if (referenceTaxonId > 0) {
            throw new ApplicationBusinessException(String.format("Can't delete a Species with a positive id %d.", referenceTaxonId));
        }
        Species species = getSpeciesByReferenceTaxonId(referenceTaxonId);
        if (species == null) {
            throw new ApplicationBusinessException(String.format("Species with id %d does not exists", referenceTaxonId));
        }

        taxonNameDao.remove(species.getIdAsInt());

    }

    protected Species addTemporarySpecies(Species source) {

        Preconditions.checkNotNull(source);
        Preconditions.checkNotNull(source.getName());
        Preconditions.checkArgument(source.getId() == null || Speciess.isTemporaryId(source.getIdAsInt()));

        TaxonRefVO taxonRefVO = null;
        if (source.getReferenceTaxonId() != null) {
            taxonRefVO = taxonNameDao.getTaxonNameReferent(source.getReferenceTaxonId());
        }
        if (taxonRefVO == null) {
            taxonRefVO = new TaxonRefVO();
            taxonRefVO.setName(source.getName());
            taxonRefVO = taxonNameDao.createAsTemporary(
                    taxonRefVO,
                    "Added by tutti (file import).");
        } else {
            TaxonName taxonName = load(TaxonNameImpl.class, taxonRefVO.getTaxonNameId());
            taxonName.setName(source.getName());
            taxonRefVO.setName(source.getName());
            taxonNameDao.update(taxonName);
        }

        // update the source
        Species result = loadSpecies(taxonRefVO);
        result.setRefTaxCode(result.getExternalCode());

        // Add to cache
        Cache cache = cacheService.getCache("referentSpeciesById");
        cache.put(result.getIdAsInt(), result);

        return result;

    }

    protected Species updateTemporarySpecies(Species source) {

        Preconditions.checkNotNull(source);
        Preconditions.checkNotNull(source.getName());
        Preconditions.checkNotNull(source.getId());
        Preconditions.checkNotNull(source.getReferenceTaxonId());
        Preconditions.checkArgument(Speciess.isTemporaryId(source.getIdAsInt()));

        TaxonRefVO taxonRefVO = taxonNameDao.getTaxonNameReferent(source.getReferenceTaxonId());

        TaxonName taxonName = load(TaxonNameImpl.class, taxonRefVO.getTaxonNameId());
        taxonName.setName(source.getName());
        taxonRefVO.setName(source.getName());
        taxonNameDao.update(taxonName);

        // update the source
        Species result = loadSpecies(taxonRefVO);
        result.setRefTaxCode(result.getExternalCode());

        // Add to cache
        Cache cache = cacheService.getCache("referentSpeciesById");
        cache.put(result.getIdAsInt(), result);

        return result;

    }

    protected Species linkTemporarySpecies(Species source) {

        Preconditions.checkNotNull(source);
        Preconditions.checkNotNull(source.getName());
        Preconditions.checkNotNull(source.getId());
        Preconditions.checkArgument(Speciess.isTemporaryId(source.getIdAsInt()));

        String taxonName = TemporaryDataHelper.TEMPORARY_NAME_PREFIX + source.getName();
        Integer referenceTaxonId = queryUniqueTyped(
                "taxonNameReferenceTaxonIdByName",
                "taxonName", StringType.INSTANCE, taxonName);

        return getSpeciesByReferenceTaxonId(referenceTaxonId);

    }

    protected Species loadSpecies(TaxonRefVO source) {

        Species target = Speciess.newSpecies();
        target.setId(source.getTaxonNameId());
        target.setName(source.getName());
        target.setExternalCode(source.getExternalCode());
        target.setReferenceTaxonId(source.getReferenceTaxonId());
        target.setReferenceTaxon(source.getIsReference());
        setStatus(source.getStatus().getValue(), target);
        return target;

    }
}

package fr.ifremer.tutti.persistence.entities.referential;

/*
 * #%L
 * Tutti :: Persistence
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Function;

public class TuttiLocations extends AbstractTuttiLocations {

//    public static Map<String, TuttiLocation> splitByLabel(List<TuttiLocation> tuttiLocations) {
//        return Maps.uniqueIndex(tuttiLocations, GET_LABEL);
//    }
//
//    public static Map<String, TuttiLocation> splitByName(List<TuttiLocation> tuttiLocations) {
//        return Maps.uniqueIndex(tuttiLocations, GET_NAME);
//    }

    public static final Function<TuttiLocation, String> GET_LABEL = TuttiLocation::getLabel;

    public static final Function<TuttiLocation, String> GET_NAME = TuttiLocation::getName;

} 

package fr.ifremer.tutti.persistence.entities.data;

/*
 * #%L
 * Tutti :: Persistence
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import org.apache.commons.beanutils.ConversionException;
import org.apache.commons.lang3.StringUtils;
import org.nuiton.converter.NuitonConverter;

import java.util.List;

import static org.nuiton.i18n.I18n.t;

/**
 * To convert to a {@link SampleCategoryModel} from a string representation.
 *
 * Each entry is a couple (categoryId,label).
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.4
 */
public class SampleCategoryModelConverter implements NuitonConverter {

    @Override
    public <T> T convert(Class<T> aClass, Object value) {
        Preconditions.checkNotNull(
                value, "Can not convert a null SampleCategoryModel");
        if (isEnabled(aClass)) {
            Object result;
            if (isEnabled(value.getClass())) {
                result = value;
                return (T) result;
            }
            if (value instanceof String) {

                List<SampleCategoryModelEntry> entries = Lists.newArrayList();

                String strValue = (String) value;

                if (StringUtils.isNotEmpty(strValue)) {
                    String[] entryStrs = strValue.split("\\s*\\|\\s*");

                    for (String entryStr : entryStrs) {
                        String[] entryParts = entryStr.split("\\s*\\,\\s*");
                        SampleCategoryModelEntry entry = new SampleCategoryModelEntry();
                        entry.setCategoryId(Integer.valueOf(entryParts[0]));
                        entry.setLabel(entryParts[1]);
                        if (entryParts.length > 2) {
                            entry.setCode(entryParts[2]);
                        } else {
                            entry.setCode(SampleCategoryModels.getCode(entryParts[1]));
                        }
                        entry.setOrder(entries.size());
                        entries.add(entry);
                    }
                }
                result = new SampleCategoryModel(entries);
                return (T) result;
            }
        }
        throw new ConversionException(
                t("tutti.persistence.error.no.convertor", aClass.getName(), value));
    }

    protected boolean isEnabled(Class<?> aClass) {
        return SampleCategoryModel.class.equals(aClass);
    }

    @Override
    public Class<?> getType() {
        return SampleCategoryModel.class;
    }
}

package fr.ifremer.tutti.persistence.service;

/*
 * #%L
 * Tutti :: Persistence
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import fr.ifremer.adagio.core.dao.administration.programStrategy.Program;
import fr.ifremer.adagio.core.dao.administration.user.DepartmentId;
import fr.ifremer.adagio.core.dao.administration.user.DepartmentImpl;
import fr.ifremer.adagio.core.dao.administration.user.PersonId;
import fr.ifremer.adagio.core.dao.administration.user.PersonImpl;
import fr.ifremer.adagio.core.dao.data.operation.FishingOperationImpl;
import fr.ifremer.adagio.core.dao.data.sample.Sample;
import fr.ifremer.adagio.core.dao.referential.QualityFlagCode;
import fr.ifremer.adagio.core.dao.referential.QualityFlagImpl;
import fr.ifremer.adagio.core.dao.referential.pmfm.Matrix;
import fr.ifremer.adagio.core.dao.referential.pmfm.MatrixId;
import fr.ifremer.adagio.core.dao.referential.pmfm.MatrixImpl;
import fr.ifremer.adagio.core.dao.referential.taxon.ReferenceTaxonImpl;
import fr.ifremer.tutti.persistence.entities.CaracteristicMap;
import fr.ifremer.tutti.persistence.entities.TuttiEntities;
import fr.ifremer.tutti.persistence.entities.data.AccidentalBatch;
import fr.ifremer.tutti.persistence.entities.data.AccidentalBatchs;
import fr.ifremer.tutti.persistence.entities.referential.Caracteristic;
import fr.ifremer.tutti.persistence.entities.referential.CaracteristicQualitativeValue;
import fr.ifremer.tutti.persistence.entities.referential.Species;
import fr.ifremer.tutti.persistence.service.referential.CaracteristicPersistenceService;
import fr.ifremer.tutti.persistence.service.referential.SpeciesPersistenceService;
import fr.ifremer.tutti.persistence.service.util.SamplePersistenceHelper;
import fr.ifremer.tutti.persistence.service.util.SynchronizationStatusHelper;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.type.IntegerType;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

/**
 * @author Tony Chemit - chemit@codelutin.com
 * @since 0.3
 */
@Service("accidentalBatchPersistenceService")
public class AccidentalBatchPersistenceServiceImpl extends AbstractPersistenceService implements AccidentalBatchPersistenceService {

    /** Logger. */
    private static final Log log =
            LogFactory.getLog(AccidentalBatchPersistenceServiceImpl.class);

    @Resource(name = "caracteristicPersistenceService")
    private CaracteristicPersistenceService caracteristicService;

    @Resource(name = "speciesPersistenceService")
    private SpeciesPersistenceService speciesService;

    @Resource(name = "samplePersistenceHelper")
    protected SamplePersistenceHelper samplePersistenceHelper;

    @Resource(name = "synchronizationStatusHelper")
    protected SynchronizationStatusHelper synchronizationStatusHelper;

    @Override
    public List<AccidentalBatch> getAllAccidentalBatch(Integer fishingOperationId) {
        Preconditions.checkNotNull(fishingOperationId);

        Iterator<Object[]> list = queryList(
                "allFishingOperationSamplesWithoutBatch",
                "fishingOperationId", IntegerType.INSTANCE, fishingOperationId
        );

        List<AccidentalBatch> result = Lists.newArrayList();

        while (list.hasNext()) {
            Object[] source = list.next();

            AccidentalBatch accidentalBatch = AccidentalBatchs.newAccidentalBatch();

            int colIndex = 0;

            // Id
            accidentalBatch.setId((Integer) source[colIndex++]);

            // TaxonId
            Integer taxonId = (Integer) source[colIndex++];
            Species species = speciesService.getSpeciesByReferenceTaxonId(taxonId);
            accidentalBatch.setSpecies(species);

            // Comment
            accidentalBatch.setComment((String) source[colIndex++]);

            // synchronizationStatus
            accidentalBatch.setSynchronizationStatus((String) source[colIndex]);

            // Sample Measurements
            accidentalBatch.setCaracteristics(new CaracteristicMap());

            // fill all measurements
            fillSampleMeasurements(accidentalBatch);

            result.add(accidentalBatch);
        }
        return Collections.unmodifiableList(result);
    }

    @Override
    public Collection<AccidentalBatch> createAccidentalBatches(Collection<AccidentalBatch> beans) {

        Preconditions.checkNotNull(beans);
        Collection<AccidentalBatch> result = new ArrayList<>();

        for (AccidentalBatch bean : beans) {
            AccidentalBatch created = createAccidentalBatch(bean);
            result.add(created);
        }
        return result;

    }

    @Override
    public AccidentalBatch saveAccidentalBatch(AccidentalBatch bean) {
        Preconditions.checkNotNull(bean);
        Preconditions.checkState(!TuttiEntities.isNew(bean));
        Preconditions.checkNotNull(bean.getSpecies());
        Preconditions.checkNotNull(bean.getFishingOperation());
        Preconditions.checkState(!TuttiEntities.isNew(bean.getFishingOperation()));

        Sample sample = samplePersistenceHelper.load(bean.getIdAsInt());
        beanToEntity(bean, sample);
        samplePersistenceHelper.update(sample);
        synchronizationStatusHelper.setDirty(bean);
        return bean;
    }

    @Override
    public void deleteAccidentalBatch(String id) {
        Preconditions.checkNotNull(id);

        Integer batchId = Integer.valueOf(id);

        if (log.isInfoEnabled()) {
            log.info("Will delete accidental batch: " + batchId);
        }
        samplePersistenceHelper.deleteSample(batchId);
    }

    @Override
    public void deleteAccidentalBatchForFishingOperation(Integer fishingOperationId) {
        Preconditions.checkNotNull(fishingOperationId);

        List<AccidentalBatch> batches =
                getAllAccidentalBatch(fishingOperationId);

        if (CollectionUtils.isNotEmpty(batches)) {
            for (AccidentalBatch batch : batches) {
                deleteAccidentalBatch(batch.getId());
            }
        }
    }

    // ------------------------------------------------------------------------//
    // -- Internal methods                                                   --//
    // ------------------------------------------------------------------------//

    @Override
    public AccidentalBatch createAccidentalBatch(AccidentalBatch bean) {
        Preconditions.checkNotNull(bean);
        Preconditions.checkState(TuttiEntities.isNew(bean));
        Preconditions.checkNotNull(bean.getSpecies());
        Preconditions.checkNotNull(bean.getFishingOperation());
        Preconditions.checkState(!TuttiEntities.isNew(bean.getFishingOperation()));

        //TODO Optimize this (in the method we load each time the fishing operation...)
        Sample sample = Sample.Factory.newInstance();
        beanToEntity(bean, sample);
        samplePersistenceHelper.create(sample);
        bean.setId(String.valueOf(sample.getId()));
        synchronizationStatusHelper.setDirty(bean);
        return bean;
    }

    protected void beanToEntity(AccidentalBatch source, Sample target) {

        if (TuttiEntities.isNew(source)) {

            // operation
            fr.ifremer.adagio.core.dao.data.operation.FishingOperation fishingOperation;

            fishingOperation = load(FishingOperationImpl.class, source.getFishingOperation().getIdAsInt());
            fishingOperation.getSamples().add(target); // Inverse link

            // Link to parent operation
            target.setFishingOperation(fishingOperation);

            // Label
            String label = fishingOperation.getId() + "_" + source.getSpecies().getReferenceTaxonId();
            target.setLabel(label);

            // Matrix (product / batch)
            Matrix matrix = load(MatrixImpl.class, MatrixId.PRODUCE_BATCH.getValue());
            target.setMatrix(matrix);

            // IndividualCount
            target.setIndividualCount((short) 1);

            // Quality Flag
            target.setQualityFlag(load(QualityFlagImpl.class, QualityFlagCode.NOTQUALIFIED.getValue()));

            // Sample Date
            if (target.getSampleDate() == null) {
                target.setSampleDate(fishingOperation.getFishingStartDateTime());
            }

            // Create Date
            target.setCreationDate(fishingOperation.getFishingStartDateTime());

            // Recorder Departement
            target.setRecorderDepartment(load(DepartmentImpl.class,
                                              DepartmentId.UNKNOWN_RECORDER_DEPARTMENT.getValue()));

            // Recorder Person
            target.setRecorderPerson(load(PersonImpl.class,
                                          PersonId.UNKNOWN_RECORDER_PERSON.getValue()));

            // Program
            Program program = fishingOperation.getFishingTrip().getProgram();
            target.setProgram(program);
        }

        // Id
        target.setId(source.getIdAsInt());

        // Always use a null batch (to differ with other samples)
        target.setBatch(null);

        // Comment
        target.setComments(source.getComment());

        // ReferenceTaxon
        Species species = source.getSpecies();
        Integer referenceTaxonId = species.getReferenceTaxonId();
        target.setReferenceTaxon(load(ReferenceTaxonImpl.class, referenceTaxonId));

        // Taxongroup TODO

        // FishingAreas TODO

        // Prepare sample measurements

        CaracteristicMap caracteristics = samplePersistenceHelper.extractCommonSampleCaracteristics(source);

        if (source.getDeadOrAlive() != null) {
            caracteristics.put(caracteristicService.getDeadOrAliveCaracteristic(), source.getDeadOrAlive());
        }

        if (source.getGender() != null) {
            caracteristics.put(caracteristicService.getSexCaracteristic(), source.getGender());
        }

        samplePersistenceHelper.setSampleMeasurements(target, caracteristics);

    }

    protected void fillSampleMeasurements(AccidentalBatch batch) {

        Caracteristic deadOrAliveCaracteristic = caracteristicService.getDeadOrAliveCaracteristic();

        Caracteristic genderCaracteristic = caracteristicService.getSexCaracteristic();

        samplePersistenceHelper.loadSampleMeasurements(batch);

        CaracteristicMap caracteristics = batch.getCaracteristics();

        CaracteristicQualitativeValue deadOrAliveValue = caracteristics.removeQualitativeValue(deadOrAliveCaracteristic);
        if (deadOrAliveValue != null) {
            batch.setDeadOrAlive(deadOrAliveValue);
        }

        CaracteristicQualitativeValue genderValue = caracteristics.removeQualitativeValue(genderCaracteristic);
        if (genderValue != null) {
            batch.setGender(genderValue);
        }

    }

}

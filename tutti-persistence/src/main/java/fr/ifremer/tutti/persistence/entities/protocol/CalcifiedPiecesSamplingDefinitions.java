package fr.ifremer.tutti.persistence.entities.protocol;

/*
 * #%L
 * Tutti :: Persistence
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2016 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.nuiton.util.beans.Binder;

public class CalcifiedPiecesSamplingDefinitions extends AbstractCalcifiedPiecesSamplingDefinitions {

    public static boolean isOperationValueUpperMax(CalcifiedPiecesSamplingDefinition cpsDef, int value) {
        return isValueUpperMax(value, cpsDef.getOperationLimitation());
    }

    public static boolean isZoneValueUpperMax(CalcifiedPiecesSamplingDefinition cpsDef, int value) {
        return isValueUpperMax(value, cpsDef.getZoneLimitation());
    }

    public static boolean isCruiseValueUpperMax(CalcifiedPiecesSamplingDefinition cpsDef, int value) {
        return isValueUpperMax(value, cpsDef.getMaxByLenghtStep());
    }

    static boolean isValueUpperMax(int value, Integer max) {
        return max != null && max > 0 && value > max;
    }

    public static <BeanType extends CalcifiedPiecesSamplingDefinition, Source> BeanType newCalcifiedPiecesSamplingDefinition(Source source, Binder<Source, BeanType> binder) {
        BeanType result = (BeanType) newCalcifiedPiecesSamplingDefinition();
        binder.copy(source, result);
        return result;
    }
}

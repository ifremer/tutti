package fr.ifremer.tutti.persistence.entities.referential;

/*
 * #%L
 * Tutti :: Persistence
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */


import com.google.common.base.Function;
import com.google.common.base.Preconditions;
import com.google.common.base.Predicate;

public class Gears extends AbstractGears {


//    public static Map<String, Gear> splitByName(List<Gear> gears) {
//        return Maps.uniqueIndex(gears, GET_NAME);
//    }

    public static final Predicate<Gear> IS_FISHING_GEAR = input -> !input.isScientificGear();

    public static final Predicate<Gear> IS_SCIENTIFIC_GEAR = Gear::isScientificGear;

    public static final Predicate<Gear> IS_TEMPORARY = Gears::isTemporary;

    public static final Function<Gear, String> GET_NAME = Gear::getName;

    /**
     * Is the given {@code gear} a temporary data ?
     *
     * @param gear gear to test
     * @return {@code true} if the given {@code gear} is temporary
     * @since 3.8
     */
    public static boolean isTemporary(Gear gear) {

        Preconditions.checkNotNull(gear);
        Preconditions.checkNotNull(gear.getId());

        return TuttiReferentialEntities.isStatusTemporary(gear) && isTemporaryId(gear.getIdAsInt());

    }

    /**
     * Is the given {@code id} is a gear temporary id ?
     *
     * @param id id to test
     * @return {@code true} if the id is a gear pecies temporary id
     * @since 3.14
     */
    public static boolean isTemporaryId(Integer id) {

        Preconditions.checkNotNull(id);
        return id < 0;

    }

}

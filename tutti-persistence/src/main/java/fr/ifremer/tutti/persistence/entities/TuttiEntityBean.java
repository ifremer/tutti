package fr.ifremer.tutti.persistence.entities;

/*
 * #%L
 * Tutti :: Persistence
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.MoreObjects;
import org.apache.commons.lang3.ObjectUtils;
import org.nuiton.util.CollectionUtil;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;
import java.util.Objects;

/**
 * Abstract tutti entity.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 0.1
 */
public abstract class TuttiEntityBean implements Serializable, TuttiEntity {

    private static final long serialVersionUID = 1L;

    protected String id;

    protected Integer intId;

    @Override
    public String getId() {
        return id;
    }

    @Override
    public void setId(String id) {
        this.id = id;
        intId = null;
    }

    @Override
    public void setId(Integer id) {
        intId = id;
        this.id = id == null ? null : String.valueOf(id);
    }

    @Override
    public Integer getIdAsInt() {
        if (intId == null && id != null) {
            intId = Integer.valueOf(id);
        }
        return intId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null) {
            return false;
        }
        if (ObjectUtils.notEqual(o.getClass(), getClass())) {
            // not sale class
            return false;
        }
        if (!(o instanceof TuttiEntityBean)) return false;

        TuttiEntityBean that = (TuttiEntityBean) o;

        return Objects.equals(getId(), that.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                          .add(PROPERTY_ID, getId())
                          .toString();
    }

    protected <B> B getChild(Collection<B> child, int index) {
        return CollectionUtil.getOrNull(child, index);
    }

    protected <B> B getChild(List<B> child, int index) {
        return CollectionUtil.getOrNull(child, index);
    }
}

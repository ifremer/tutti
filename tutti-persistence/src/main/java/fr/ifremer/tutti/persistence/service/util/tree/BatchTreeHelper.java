package fr.ifremer.tutti.persistence.service.util.tree;

/*
 * #%L
 * Tutti :: Persistence
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.adagio.core.dao.data.batch.CatchBatch;
import fr.ifremer.adagio.core.dao.data.batch.SortingBatch;
import fr.ifremer.adagio.core.dao.referential.pmfm.PmfmId;
import fr.ifremer.adagio.core.dao.referential.pmfm.QualitativeValueId;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * Helper to build or navigauet into the batch tree.
 *
 * Created on 4/20/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.5
 */
@Component("batchTreeHelper")
public class BatchTreeHelper extends BatchTreeHelperSupport {

//    /** Logger. */
//    private static final Log log = LogFactory.getLog(BatchTreeHelper.class);

    @Resource(name = "benthosBatchTreeHelper")
    protected BenthosBatchTreeHelper benthosBatchTreeHelper;

    @Resource(name = "speciesBatchTreeHelper")
    protected SpeciesBatchTreeHelper speciesBatchTreeHelper;

    //------------------------------------------------------------------------//
    //-- Get CatchBatch navigation methods                                  --//
    //------------------------------------------------------------------------//

//    public SortingBatch getSpeciesVracAliveItemizedRootBatch(CatchBatch batch) {
//        return speciesBatchTreeHelper.getVracAliveItemizedRootBatch(batch);
//    }
//
//    public SortingBatch getBenthosVracAliveItemizedRootBatch(CatchBatch batch) {
//        return benthosBatchTreeHelper.getVracAliveItemizedRootBatch(batch);
//    }
//
//    public SortingBatch getSpeciesHorsVracRootBatch(CatchBatch batch) {
//        return speciesBatchTreeHelper.getHorsVracRootBatch(batch);
//    }
//
//    public SortingBatch getBenthosHorsVracRootBatch(CatchBatch batch) {
//        return benthosBatchTreeHelper.getHorsVracRootBatch(batch);
//    }

    public SortingBatch getMarineLitterRootBatch(CatchBatch batch) {
        return getSortingBatch(batch,
                               "Hors Vrac > Marine Litter",
                               PmfmId.SORTED_UNSORTED.getValue(),
                               QualitativeValueId.SORTED_HORS_VRAC.getValue(),
                               SORTING_TYPE_ID,
                               QualitativeValueId.SORTING_TYPE_MARINE_LITTER.getValue());
    }

//    public SortingBatch getVracBatch(CatchBatch batch) {
//        return getSortingBatch(
//                batch,
//                "Vrac",
//                PmfmId.SORTED_UNSORTED.getValue(),
//                QualitativeValueId.SORTED_VRAC.getValue()
//        );
//    }

//    public SortingBatch getHorsVracBatch(CatchBatch batch) {
//        return getSortingBatch(
//                batch,
//                "Hors Vrac",
//                PmfmId.SORTED_UNSORTED.getValue(),
//                QualitativeValueId.SORTED_HORS_VRAC.getValue()
//        );
//    }
//
    public SortingBatch getRejectedBatch(CatchBatch batch) {
        return getSortingBatch(
                batch,
                "Unsorted",
                PmfmId.SORTED_UNSORTED.getValue(),
                QualitativeValueId.UNSORTED.getValue()
        );
    }

    //------------------------------------------------------------------------//
    //-- Get SortingBatch navigation methods                                --//
    //------------------------------------------------------------------------//

    public SortingBatch getSpeciesVracRootBatch(SortingBatch batch) {
        return speciesBatchTreeHelper.getVracRootBatch(batch);
    }

    public SortingBatch getBenthosVracRootBatch(SortingBatch batch) {
        return benthosBatchTreeHelper.getVracRootBatch(batch);
    }

    public SortingBatch getSpeciesVracAliveNotItemizedRootBatch(SortingBatch batch) {
        return speciesBatchTreeHelper.getVracAliveNotItemizedRootBatch(batch);
    }

    public SortingBatch getBenthosVracAliveNotItemizedRootBatch(SortingBatch batch) {
        return benthosBatchTreeHelper.getVracAliveNotItemizedRootBatch(batch);
    }

    public SortingBatch getSpeciesVracInertRootBatch(SortingBatch batch) {
        return speciesBatchTreeHelper.getVracInertRootBatch(batch);
    }

    public SortingBatch getBenthosVracInertRootBatch(SortingBatch batch) {
        return benthosBatchTreeHelper.getVracInertRootBatch(batch);
    }

    public SortingBatch getSpeciesVracAliveItemizedRootBatch(SortingBatch batch) {
        return speciesBatchTreeHelper.getVracAliveItemizedRootBatch(batch);
    }

    public SortingBatch getBenthosVracAliveItemizedRootBatch(SortingBatch batch) {
        return benthosBatchTreeHelper.getVracAliveItemizedRootBatch(batch);
    }

    public SortingBatch getSpeciesHorsVracRootBatch(SortingBatch batch) {
        return speciesBatchTreeHelper.getHorsVracRootBatch(batch);
    }

    public SortingBatch getBenthosHorsVracRootBatch(SortingBatch batch) {
        return benthosBatchTreeHelper.getHorsVracRootBatch(batch);
    }

    public SortingBatch getMarineLitterRootBatch(SortingBatch batch) {
        return getSortingBatch(
                batch,
                "Hors Vrac > MarineLitter",
                SORTING_TYPE_ID,
                QualitativeValueId.SORTING_TYPE_MARINE_LITTER.getValue());
    }

    //------------------------------------------------------------------------//
    //-- getOrCreate methods                                                --//
    //------------------------------------------------------------------------//

//    public SortingBatch getOrCreateVracBatch(CatchBatch batch, Float weight, Float weightBeforeSampling) {
//        return getOrCreate(batch,
//                           batch,
//                           "Vrac",
//                           PmfmId.SORTED_UNSORTED.getValue(),
//                           QualitativeValueId.SORTED_VRAC.getValue(),
//                           weight,
//                           weightBeforeSampling,
//                           (short) 1);
//    }

    public SortingBatch getOrCreateSpeciesVracRootBatch(CatchBatch target, SortingBatch batch, Float totalWeight) {
        return speciesBatchTreeHelper.getOrCreateVracRootBatch(target, batch, totalWeight);
//                target,
//                batch,
//                "Vrac > Species",
//                SORTING_TYPE_ID,
//                QualitativeValueId.SORTING_TYPE_SPECIES.getValue(),
//                totalWeight,
//                (short) 1
//        );
    }

    public SortingBatch getOrCreateSpeciesVracAliveNotItemizedRootBatch(CatchBatch target, SortingBatch batch, Float totalWeight) {
        return speciesBatchTreeHelper.getOrCreateVracAliveNotItemizedRootBatch(target, batch, totalWeight);
//        return getOrCreate(
//                target,
//                batch,
//                "Vrac > Species > Alive Not Itemized",
//                SORTING_TYPE2_ID,
//                QualitativeValueId.SORTING_TYPE2_ALIVE_NOT_ITEMIZED.getValue(),
//                totalWeight,
//                (short) 1
//        );
    }

    public SortingBatch getOrCreateSpeciesVracInertRootBatch(CatchBatch target, SortingBatch batch, Float totalWeight) {
        return speciesBatchTreeHelper.getOrCreateVracInertRootBatch(target, batch, totalWeight);
//        return getOrCreate(
//                target,
//                batch,
//                "Vrac > Species > Inert",
//                SORTING_TYPE2_ID,
//                QualitativeValueId.SORTING_TYPE2_INERT.getValue(),
//                totalWeight,
//                (short) 2
//        );
    }

    public SortingBatch getOrCreateSpeciesVracAliveItemizedRootBatch(CatchBatch target, SortingBatch batch) {
        return speciesBatchTreeHelper.getOrCreateVracAliveItemizedRootBatch(target, batch);
//        return getOrCreate(
//                target,
//                batch,
//                "Vrac > Benthos > Alive Itemized",
//                SORTING_TYPE2_ID,
//                QualitativeValueId.SORTING_TYPE2_ALIVE_ITEMIZED.getValue(),
//                (short) 3
//        );
    }

    public SortingBatch getOrCreateBenthosVracRootBatch(CatchBatch target, SortingBatch batch, Float totalWeight) {
        return benthosBatchTreeHelper.getOrCreateVracRootBatch(target, batch, totalWeight);
//        return getOrCreate(
//                target,
//                batch,
//                "Vrac > Benthos",
//                SORTING_TYPE_ID,
//                QualitativeValueId.SORTING_TYPE_BENTHOS.getValue(),
//                totalWeight,
//                (short) 2
//        );
    }

    public SortingBatch getOrCreateBenthosVracAliveNotItemizedRootBatch(CatchBatch target, SortingBatch batch, Float totalWeight) {
        return benthosBatchTreeHelper.getOrCreateVracAliveNotItemizedRootBatch(target, batch, totalWeight);
//        return getOrCreate(
//                target,
//                batch,
//                "Vrac > Benthos > Alive Not itemized",
//                SORTING_TYPE2_ID,
//                QualitativeValueId.SORTING_TYPE2_ALIVE_NOT_ITEMIZED.getValue(),
//                totalWeight,
//                (short) 1
//        );
    }

    public SortingBatch getOrCreateBenthosVracInertRootBatch(CatchBatch target, SortingBatch batch, Float totalWeight) {
        return benthosBatchTreeHelper.getOrCreateVracInertRootBatch(target, batch, totalWeight);
//        return getOrCreate(
//                target,
//                batch,
//                "Vrac > Benthos > Inert",
//                SORTING_TYPE2_ID,
//                QualitativeValueId.SORTING_TYPE2_INERT.getValue(),
//                totalWeight,
//                (short) 2
//        );
    }

    public SortingBatch getOrCreateBenthosVracAliveItemizedRootBatch(CatchBatch target, SortingBatch batch) {
        return benthosBatchTreeHelper.getOrCreateVracAliveItemizedRootBatch(target, batch);
//        return getOrCreate(
//                target,
//                batch,
//                "Vrac > Benthos > Alive Itemized",
//                SORTING_TYPE2_ID,
//                QualitativeValueId.SORTING_TYPE2_ALIVE_ITEMIZED.getValue(),
//                (short) 3
//        );
    }

//    public SortingBatch getOrCreateHorsVracBatch(CatchBatch batch) {
//        return getOrCreate(batch,
//                           batch,
//                "Hors Vrac",
//                PmfmId.SORTED_UNSORTED.getValue(),
//                QualitativeValueId.SORTED_HORS_VRAC.getValue(),
//                (short) 2);
//    }

    public SortingBatch getOrCreateSpeciesHorsVracRootBatch(CatchBatch target, SortingBatch batch) {
        return speciesBatchTreeHelper.getOrCreateHorsVracRootBatch(target, batch);
//        return getOrCreate(
//                target,
//                batch,
//                "Hors Vrac > Species",
//                SORTING_TYPE_ID,
//                QualitativeValueId.SORTING_TYPE_SPECIES.getValue(),
//                (short) 1
//        );
    }

    public SortingBatch getOrCreateBenthosHorsVracRootBatch(CatchBatch target, SortingBatch batch) {
        return benthosBatchTreeHelper.getOrCreateHorsVracRootBatch(target, batch);
//        return getOrCreate(
//                target,
//                batch,
//                "Hors Vrac > Benthos",
//                SORTING_TYPE_ID,
//                QualitativeValueId.SORTING_TYPE_BENTHOS.getValue(),
//                (short) 2
//        );
    }

    public SortingBatch getOrCreateMarineLitterRootBatch(CatchBatch target, SortingBatch batch, Float totalWeight) {
        return getOrCreate(target,
                           batch,
                           "Hors Vrac > Marine Litter",
                           SORTING_TYPE_ID,
                           QualitativeValueId.SORTING_TYPE_MARINE_LITTER.getValue(),
                           totalWeight,
                           (short) 3);
    }

    public SortingBatch getOrCreateRejectedBatch(CatchBatch batch, Float weight) {
        return getOrCreate(batch,
                           batch,
                           "Unsorted",
                           PmfmId.SORTED_UNSORTED.getValue(),
                           QualitativeValueId.UNSORTED.getValue(),
                           weight,
                           (short) 3);
    }

}

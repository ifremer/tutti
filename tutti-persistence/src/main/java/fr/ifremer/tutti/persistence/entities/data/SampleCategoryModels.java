package fr.ifremer.tutti.persistence.entities.data;

/*
 * #%L
 * Tutti :: Persistence
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.apache.commons.lang3.StringUtils;

/**
 * Created on 1/28/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since XXX
 */
public class SampleCategoryModels {

    public static String getCode(String label) {
        String canonicalLabel = "";

        if (StringUtils.isNotEmpty(label)) {
            for (int i = 0; i < label.length(); i++) {
                char a = label.charAt(i);
                if (!Character.isLetterOrDigit(a)) {
                    a = '_';
                }
                canonicalLabel += a;
            }
        }

        return canonicalLabel;
    }
}

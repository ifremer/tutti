package fr.ifremer.tutti.persistence.service;

/*
 * #%L
 * Tutti :: Persistence
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import fr.ifremer.adagio.core.dao.data.fishingArea.FishingArea;
import fr.ifremer.adagio.core.dao.data.fishingArea.FishingArea2RegulationLocation;
import fr.ifremer.adagio.core.dao.data.fishingArea.FishingArea2RegulationLocationDao;
import fr.ifremer.adagio.core.dao.data.fishingArea.FishingArea2RegulationLocationPK;
import fr.ifremer.adagio.core.dao.data.fishingArea.FishingAreaDao;
import fr.ifremer.adagio.core.dao.data.fishingArea.FishingAreaImpl;
import fr.ifremer.adagio.core.dao.data.measure.GearPhysicalMeasurement;
import fr.ifremer.adagio.core.dao.data.measure.GearUseMeasurement;
import fr.ifremer.adagio.core.dao.data.measure.VesselUseMeasurement;
import fr.ifremer.adagio.core.dao.data.operation.FishingOperationDao;
import fr.ifremer.adagio.core.dao.data.operation.Operation;
import fr.ifremer.adagio.core.dao.data.operation.OperationImpl;
import fr.ifremer.adagio.core.dao.data.operation.OperationVesselAssociation;
import fr.ifremer.adagio.core.dao.data.operation.OperationVesselAssociationDao;
import fr.ifremer.adagio.core.dao.data.operation.OperationVesselAssociationPK;
import fr.ifremer.adagio.core.dao.data.survey.fishingTrip.FishingTrip;
import fr.ifremer.adagio.core.dao.data.survey.scientificCruise.ScientificCruise;
import fr.ifremer.adagio.core.dao.data.survey.scientificCruise.ScientificCruiseDao;
import fr.ifremer.adagio.core.dao.data.vessel.VesselImpl;
import fr.ifremer.adagio.core.dao.data.vessel.feature.person.VesselPersonFeatures;
import fr.ifremer.adagio.core.dao.data.vessel.feature.physical.GearPhysicalFeatures;
import fr.ifremer.adagio.core.dao.data.vessel.feature.use.GearUseFeatures;
import fr.ifremer.adagio.core.dao.data.vessel.feature.use.VesselUseFeatures;
import fr.ifremer.adagio.core.dao.data.vessel.feature.use.isActive;
import fr.ifremer.adagio.core.dao.data.vessel.position.VesselPosition;
import fr.ifremer.adagio.core.dao.referential.ObjectTypeCode;
import fr.ifremer.adagio.core.dao.referential.QualityFlagCode;
import fr.ifremer.adagio.core.dao.referential.QualityFlagImpl;
import fr.ifremer.adagio.core.dao.referential.VesselPersonRole;
import fr.ifremer.adagio.core.dao.referential.VesselPersonRoleId;
import fr.ifremer.adagio.core.dao.referential.gear.GearImpl;
import fr.ifremer.adagio.core.dao.referential.location.LocationExtendDao;
import fr.ifremer.adagio.core.dao.referential.location.LocationImpl;
import fr.ifremer.adagio.core.dao.referential.location.LocationLevelId;
import fr.ifremer.adagio.core.dao.referential.pmfm.PmfmId;
import fr.ifremer.adagio.core.dao.referential.pmfm.QualitativeValueId;
import fr.ifremer.tutti.persistence.dao.GearPhysicalFeaturesDaoTutti;
import fr.ifremer.tutti.persistence.entities.CaracteristicMap;
import fr.ifremer.tutti.persistence.entities.data.FishingOperation;
import fr.ifremer.tutti.persistence.entities.data.FishingOperations;
import fr.ifremer.tutti.persistence.entities.referential.Caracteristic;
import fr.ifremer.tutti.persistence.entities.referential.CaracteristicQualitativeValue;
import fr.ifremer.tutti.persistence.entities.referential.CaracteristicType;
import fr.ifremer.tutti.persistence.entities.referential.Gear;
import fr.ifremer.tutti.persistence.entities.referential.GearWithOriginalRankOrders;
import fr.ifremer.tutti.persistence.entities.referential.Person;
import fr.ifremer.tutti.persistence.entities.referential.TuttiLocation;
import fr.ifremer.tutti.persistence.entities.referential.Vessel;
import fr.ifremer.tutti.persistence.service.referential.CaracteristicPersistenceService;
import fr.ifremer.tutti.persistence.service.referential.GearPersistenceService;
import fr.ifremer.tutti.persistence.service.referential.LocationPersistenceService;
import fr.ifremer.tutti.persistence.service.referential.PersonPersistenceService;
import fr.ifremer.tutti.persistence.service.referential.VesselPersistenceService;
import fr.ifremer.tutti.persistence.service.util.MeasurementPersistenceHelper;
import fr.ifremer.tutti.persistence.service.util.SynchronizationStatusHelper;
import fr.ifremer.tutti.persistence.service.util.VesselPersonFeaturesPersistenceHelper;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.FlushMode;
import org.hibernate.type.IntegerType;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.DataRetrievalFailureException;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.io.Serializable;
import java.sql.Timestamp;
import java.text.MessageFormat;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

/**
 * @author Tony Chemit - chemit@codelutin.com
 * @since 0.3
 */
@Service("fishingOperationPersistenceService")
public class FishingOperationPersistenceServiceImpl extends AbstractPersistenceService implements FishingOperationPersistenceService {

    /** Logger. */
    private static final Log log =
            LogFactory.getLog(FishingOperationPersistenceServiceImpl.class);

    @Resource(name = "caracteristicPersistenceService")
    private CaracteristicPersistenceService caracteristicService;

    @Resource(name = "gearPersistenceService")
    private GearPersistenceService gearService;

    @Resource(name = "locationPersistenceService")
    private LocationPersistenceService locationService;

    @Resource(name = "personPersistenceService")
    private PersonPersistenceService personService;

    @Resource(name = "vesselPersistenceService")
    private VesselPersistenceService vesselService;

    @Resource(name = "batchPersistenceService")
    protected CatchBatchPersistenceService catchBatchService;

    @Resource(name = "attachmentPersistenceService")
    protected AttachmentPersistenceService attachmentPersistenceService;

    @Resource(name = "vesselPersonFeaturesPersistenceHelper")
    protected VesselPersonFeaturesPersistenceHelper vesselPersonFeaturesPersistenceHelper;

    @Resource(name = "measurementPersistenceHelper")
    protected MeasurementPersistenceHelper measurementPersistenceHelper;

    @Resource(name = "scientificCruiseDao")
    protected ScientificCruiseDao scientificCruiseDao;

    @Resource(name = "gearPhysicalFeaturesDaoTutti")
    protected GearPhysicalFeaturesDaoTutti gearPhysicalFeaturesDao;

    @Resource(name = "fishingArea2RegulationLocationDao")
    protected FishingArea2RegulationLocationDao fishingArea2RegulationLocationDao;

    @Resource(name = "fishingOperationDao")
    protected FishingOperationDao fishingOperationDao;

    @Resource(name = "fishingAreaDao")
    protected FishingAreaDao fishingAreaDao;

    @Resource(name = "operationVesselAssociationDao")
    protected OperationVesselAssociationDao operationVesselAssociationDao;

    @Resource(name = "locationDao")
    protected LocationExtendDao locationDao;

    @Resource(name = "synchronizationStatusHelper")
    protected SynchronizationStatusHelper synchronizationStatusHelper;

    protected static Float DEFAULT_EMPTY_LATITUDE = 0.0001f;

    protected static Float DEFAULT_EMPTY_LONGITUDE = 0.0001f;

    @Override
    public int getFishingOperationCount(Integer cruiseId) {
        Preconditions.checkNotNull(cruiseId);
        Iterator<Object[]> list = queryList(
                "allFishingOperationIds",
                "cruiseId", IntegerType.INSTANCE, cruiseId
        );
        int result = 0;
        while (list.hasNext()) {
            list.next();
            result++;
        }
        return result;
    }

    @Override
    public List<Integer> getAllFishingOperationIds(Integer cruiseId) {
        Preconditions.checkNotNull(cruiseId);

        Iterator list = queryList(
                "allFishingOperationIds",
                "cruiseId", IntegerType.INSTANCE, cruiseId
        );

        List<Integer> result = Lists.newArrayList();
        while (list.hasNext()) {
            Integer id = (Integer) list.next();
            result.add(id);
        }
        return result;
    }

    @Override
    public List<FishingOperation> getAllFishingOperation(Integer cruiseId) {
        Preconditions.checkNotNull(cruiseId);

        Iterator<Object[]> list = queryList(
                "allFishingOperations",
                "cruiseId", IntegerType.INSTANCE, cruiseId,
                "pmfmIdStationNumber", IntegerType.INSTANCE, PmfmId.STATION_NUMBER.getValue(),
                "pmfmIdMultirigAggregation", IntegerType.INSTANCE, PmfmId.MULTIRIG_AGGREGATION.getValue()
        );

        List<FishingOperation> result = Lists.newArrayList();
        int fishingOperationRankOrder = 0;
        while (list.hasNext()) {
            Object[] source = list.next();
            fishingOperationRankOrder++;

            FishingOperation fishingOperation = FishingOperations.newFishingOperation();
            int colIndex = 0;

            // Id
            fishingOperation.setId(source[colIndex++].toString());

            // Fishing operation number : trying to retrieve from name
            String name = (String) source[colIndex++];
            if (StringUtils.isNotBlank(name)) {
                fishingOperation.setFishingOperationNumber(Integer.valueOf(name));
            }

            // SynchronizationStatus
            fishingOperation.setSynchronizationStatus((String) source[colIndex++]);

            // If not found, compute it using a counter (see "order by startDateTime" in HQL query)
            if (fishingOperation.getFishingOperationNumber() == null) {
                fishingOperation.setFishingOperationNumber(fishingOperationRankOrder);
            }

            // Start date time
            Timestamp startDateTime = (Timestamp) source[colIndex++];
            fishingOperation.setGearShootingStartDate(convertDatabase2UI(startDateTime));

            // Station number
            String stationNumber = (String) source[colIndex++];
            if (stationNumber != null) {
                fishingOperation.setStationNumber(stationNumber);
            }

            // Multirig Aggregation
            String multirigAggregation = (String) source[colIndex];
            if (multirigAggregation != null) {
                fishingOperation.setMultirigAggregation(multirigAggregation);
            }
            // Force initialization to 1 initialization (need for UI)
            else {
                log.warn(MessageFormat.format("FishingOperation with id={0} has been load with a default multirigAggregation={1}, because no multirigAggregation were found in database.", fishingOperation.getId(), fishingOperation.getMultirigAggregation()));
                fishingOperation.setMultirigAggregation("1");
            }

            result.add(fishingOperation);
        }
        return Collections.unmodifiableList(result);
    }

    @Override
    public FishingOperation getFishingOperation(Integer id) {
        Preconditions.checkNotNull(id);
        Object[] source = queryUnique(
                "fishingOperation",
                "fishingOperationId", IntegerType.INSTANCE, id,
                "locationLevelIdStrata", IntegerType.INSTANCE, LocationLevelId.SCIENTIFIC_CRUISE_STRATA.getValue(),
                "locationLevelIdSubStrata", IntegerType.INSTANCE, LocationLevelId.SCIENTIFIC_CRUISE_SUB_STRATA.getValue(),
                "locationLevelIdLocalite", IntegerType.INSTANCE, LocationLevelId.SCIENTIFIC_CRUISE_LOCALITE.getValue()
        );

        if (source == null) {
            throw new DataRetrievalFailureException("Could not retrieve fishingOperation with id=" + id);
        }
        FishingOperation result = FishingOperations.newFishingOperation();
        result.setId(id);

        // Cruise : 
        // do load load Cruise here, because it will be attach upper in the call stack

        int colIndex = 0;

        // Name
        String name = (String) source[colIndex++];
        if (StringUtils.isNotBlank(name)) {
            result.setFishingOperationNumber(Integer.valueOf(name));
        }

        // SynchronizationStatus
        result.setSynchronizationStatus((String) source[colIndex++]);

        Short rankOrder = (Short) source[colIndex++];

        // If not found, compute it using a counter (see "order by startDateTime" in HQL query)
        if (result.getFishingOperationNumber() == null) {
            Integer fishingOperationRankOrder = queryUniqueTyped(
                    "fishingOperationRankOrder",
                    "fishingOperationId", IntegerType.INSTANCE, id
            );
            result.setFishingOperationNumber(fishingOperationRankOrder);
        }

        // Start date
        result.setGearShootingStartDate(convertDatabase2UI((Timestamp) source[colIndex++]));

        // End date
        result.setGearShootingEndDate(convertDatabase2UI((Timestamp) source[colIndex++]));

        // RecorderPerson
        result.setRecorderPerson(Lists.<Person>newArrayList());
        Iterator<Object[]> vesselPersonFeaturesList = queryList(
                "fishingOperationVesselPersonFeatures",
                "fishingOperationId", IntegerType.INSTANCE, id);
        while (vesselPersonFeaturesList.hasNext()) {
            Object[] vesselPersonFeatures = vesselPersonFeaturesList.next();

            Integer personId = (Integer) vesselPersonFeatures[0];
            Person person = personService.getPerson(personId);
            Integer roleId = (Integer) vesselPersonFeatures[1];
            if (VesselPersonRoleId.RECORDER_PERSON.getValue().equals(roleId)) {
                result.addRecorderPerson(person);
            }
        }

        // Comment :
        String comment = (String) source[colIndex++];
        result.setComment(comment);

        // Gear :
        Integer gearId = (Integer) source[colIndex++];
        if (gearId != null) {
            // get gear from referential
            Gear gear = gearService.getGear(gearId);
            Preconditions.checkNotNull(gear);
            // use a copy of it with rankOrder
            Gear toSet = GearWithOriginalRankOrders.newGearWithOriginalRankOrder(gear);
            toSet.setRankOrder(rankOrder);

            if (log.isDebugEnabled()) {
                log.debug("FishingOperation: " + id + ", use gear: " + gearId + " - " + rankOrder);
            }

            result.setGear(toSet);
        }

        // Start position
        VesselPosition startVesselPosition = (VesselPosition) source[colIndex++];
        if (startVesselPosition == null) {
            result.setGearShootingStartLatitude(null);
            result.setGearShootingStartLongitude(null);
        } else {
            result.setGearShootingStartLatitude(convertLatitude2UI(startVesselPosition.getLatitude()));
            result.setGearShootingStartLongitude(convertLongitude2UI(startVesselPosition.getLongitude()));
        }

        // End position
        VesselPosition endVesselPosition = (VesselPosition) source[colIndex++];
        if (endVesselPosition == null) {
            result.setGearShootingEndLatitude(null);
            result.setGearShootingEndLongitude(null);
        } else {
            result.setGearShootingEndLatitude(DEFAULT_EMPTY_LATITUDE.equals(endVesselPosition.getLatitude()) ? null : endVesselPosition.getLatitude());
            result.setGearShootingEndLongitude(DEFAULT_EMPTY_LONGITUDE.equals(endVesselPosition.getLongitude()) ? null : endVesselPosition.getLongitude());
        }

        // Strata :
        Integer strataId = (Integer) source[colIndex++];
        if (strataId != null) {
            TuttiLocation strata = locationService.getLocation(strataId.toString());
            result.setStrata(strata);
        }

        // Sub Strata :
        Integer subStrataId = (Integer) source[colIndex++];
        if (subStrataId != null) {
            TuttiLocation subStrata = locationService.getLocation(subStrataId.toString());
            result.setSubStrata(subStrata);
        }

        // Localite :
        Integer localiteId = (Integer) source[colIndex++];
        if (localiteId != null) {
            TuttiLocation localite = locationService.getLocation(localiteId.toString());
            result.setLocation(localite);
        }

        // Vessel (the one with the catch batch)
        String vesselCode = (String) source[colIndex];
        if (vesselCode != null) {
            Vessel vessel = vesselService.getVessel(vesselCode);
            result.setVessel(vessel);
        }

        // Retrieve secondary vessels
        List<Vessel> secondaryVessel = getFishingOperationSecondaryVessel(id);
        result.setSecondaryVessel(secondaryVessel);

        // Retrieve gear use features
        getGearUseCaracteristics(id, result);

        // Retrieve vessel use features
        getVesselUseCaracteristics(id, result);

        return result;
    }

    @Override
    public List<Vessel> getFishingOperationSecondaryVessel(Integer fishingOperationId) {
        Iterator<Object[]> secondaryVesselList = queryList(
                "fishingOperationSecondaryVessel",
                "fishingOperationId", IntegerType.INSTANCE, fishingOperationId);
        List<Vessel> result = Lists.newArrayList();
        while (secondaryVesselList.hasNext()) {

            Object[] next = secondaryVesselList.next();
            String secondaryVesselCode = (String) next[0];
            if (secondaryVesselCode != null) {
                Boolean isCatchOnOperationVessel = (Boolean) next[1];
                if (isCatchOnOperationVessel == null || !isCatchOnOperationVessel) {

                    Vessel vessel = vesselService.getVessel(secondaryVesselCode);
                    result.add(vessel);
                }
            }
        }
        return result;
    }

    @Override
    public FishingOperation createFishingOperation(FishingOperation bean) {
        Preconditions.checkNotNull(bean);
        Preconditions.checkArgument(bean.getId() == null);
        Preconditions.checkNotNull(bean.getCruise());
        Preconditions.checkNotNull(bean.getCruise().getId());
        //TODO-TC Voir si il n'y a pas d'autre données obligatoires

        if (bean.getGearShootingStartDate() != null && bean.getGearShootingEndDate() != null) {

            //make sure not same date
            Preconditions.checkArgument(!bean.getGearShootingStartDate().equals(bean.getGearShootingEndDate()));
        }

        getCurrentSession().setFlushMode(FlushMode.COMMIT);
        fr.ifremer.adagio.core.dao.data.operation.FishingOperation fishingOperation = fr.ifremer.adagio.core.dao.data.operation.FishingOperation.Factory.newInstance();
        beanToEntity(bean, fishingOperation);
        fishingOperationDao.create(fishingOperation);
        bean.setId(String.valueOf(fishingOperation.getId()));
        getCurrentSession().flush();

        return bean;
    }

    @Override
    public FishingOperation saveFishingOperation(FishingOperation bean) {
        Preconditions.checkNotNull(bean);
        Preconditions.checkNotNull(bean.getId());
        Preconditions.checkNotNull(bean.getCruise());
        Preconditions.checkNotNull(bean.getCruise().getId());
        //TODO-TC Voir si il n'y a pas d'autre données obligatoires

        if (bean.getGearShootingStartDate() != null && bean.getGearShootingEndDate() != null) {

            //make sure not same date
            Preconditions.checkArgument(!bean.getGearShootingStartDate().equals(bean.getGearShootingEndDate()));
        }

        getCurrentSession().clear();
        getCurrentSession().setFlushMode(FlushMode.COMMIT);
        fr.ifremer.adagio.core.dao.data.operation.FishingOperation fishingOperation = fishingOperationDao.load(Integer.valueOf(bean.getId()));
        if (fishingOperation == null) {
            throw new DataRetrievalFailureException("Could not retrieve fishing operation with id=" + bean.getId());
        }
        beanToEntity(bean, fishingOperation);
        fishingOperationDao.update(fishingOperation);
        getCurrentSession().flush();
        return bean;
    }

    @Override
    public Collection<FishingOperation> saveFishingOperations(Collection<FishingOperation> beans) {
        getCurrentSession().clear();
        getCurrentSession().setFlushMode(FlushMode.COMMIT);

        List<fr.ifremer.adagio.core.dao.data.operation.FishingOperation> operations = Lists.newArrayList();
        for (FishingOperation bean : beans) {
            fr.ifremer.adagio.core.dao.data.operation.FishingOperation fishingOperation =
                    fishingOperationDao.load(Integer.valueOf(bean.getId()));
            if (fishingOperation == null) {
                throw new DataRetrievalFailureException("Could not retrieve fishing operation with id=" + bean.getId());
            }
            beanToEntity(bean, fishingOperation);
            operations.add(fishingOperation);
        }

        fishingOperationDao.update(operations);
        getCurrentSession().flush();
        return beans;
    }

    @Override
    public void deleteFishingOperation(Integer fishingOperationId) {
        Preconditions.checkNotNull(fishingOperationId);

        fr.ifremer.adagio.core.dao.data.operation.FishingOperation fishingOperation = fishingOperationDao.load(fishingOperationId);
        if (fishingOperation == null) {
            throw new DataRetrievalFailureException("Could not retrieve fishing operation with id=" + fishingOperationId);
        }

        attachmentPersistenceService.deleteAllAttachment(ObjectTypeCode.OPERATION, fishingOperationId);
        getCurrentSession().flush();

        // delete catch batch
        catchBatchService.deleteCatchBatch(fishingOperationId);
        getCurrentSession().flush();

        Set<FishingArea> fishingAreas = Sets.newHashSet();

        // remove gear use features
        if (CollectionUtils.isNotEmpty(fishingOperation.getGearUseFeatures())) {
            for (GearUseFeatures gearUseFeatures : fishingOperation.getGearUseFeatures()) {

                gearUseFeatures.setOperation(null);
                gearUseFeatures.getGearUseMeasurements().clear();

                if (CollectionUtils.isNotEmpty(gearUseFeatures.getFishingAreas())) {
                    for (FishingArea fishingArea : gearUseFeatures.getFishingAreas()) {
                        fishingArea.setGearUseFeatures(null);
                    }
                    fishingAreas.addAll(gearUseFeatures.getFishingAreas());
                }
            }
            // must remove all features content before removing them (data integrity will then failed otherwise)
            getCurrentSession().flush();
            fishingOperation.getGearUseFeatures().clear();
        }

        // remove vessel use features
        if (CollectionUtils.isNotEmpty(fishingOperation.getVesselUseFeatures())) {
            for (VesselUseFeatures vesselUseFeatures : fishingOperation.getVesselUseFeatures()) {

                vesselUseFeatures.setOperation(null);
                vesselUseFeatures.getVesselUseMeasurements().clear();

                if (CollectionUtils.isNotEmpty(vesselUseFeatures.getFishingAreas())) {
                    for (FishingArea fishingArea : vesselUseFeatures.getFishingAreas()) {
                        fishingArea.setVesselUseFeatures(null);
                    }
                    fishingAreas.addAll(vesselUseFeatures.getFishingAreas());
                }
            }

            // must remove all features content before removing them (data integrity will then failed otherwise)
            getCurrentSession().flush();
            fishingOperation.getVesselUseFeatures().clear();
        }

        // remove vessel position
        if (CollectionUtils.isNotEmpty(fishingOperation.getVesselPositions())) {
            fishingOperation.getVesselPositions().clear();
        }

        // remove vessel person features
        if (CollectionUtils.isNotEmpty(fishingOperation.getVesselPersonFeatures())) {
            fishingOperation.getVesselPersonFeatures().clear();
        }

        // remove gear physical features
        fishingOperation.setGearPhysicalFeatures(null);

        // remove catch batch
        fishingOperation.setCatchBatch(null);

        // remove samples
        if (CollectionUtils.isNotEmpty(fishingOperation.getSamples())) {
            fishingOperation.getSamples().clear();
        }

        // remove operation vessel associations
        if (CollectionUtils.isNotEmpty(fishingOperation.getOperationVesselAssociations())) {
            for (OperationVesselAssociation asso : fishingOperation.getOperationVesselAssociations()) {
                operationVesselAssociationDao.remove(asso);
            }
            fishingOperation.getOperationVesselAssociations().clear();
        }

        // remove fishing areas
        if (CollectionUtils.isNotEmpty(fishingAreas)) {
            for (FishingArea fishingArea : fishingAreas) {
                fishingArea.setProduce(null);
                fishingArea2RegulationLocationDao.remove(fishingArea.getRegulationLocations());
                fishingArea.getRegulationLocations().clear();
            }
            getCurrentSession().flush();
            fishingAreaDao.remove(fishingAreas);
        }

        getCurrentSession().flush();

        // remove fishing operation produces
        if (CollectionUtils.isNotEmpty(fishingOperation.getProduces())) {
            fishingOperation.getProduces().clear();
            getCurrentSession().flush();
        }

        // remove fishing operations
        fishingOperationDao.remove(fishingOperation);

        getCurrentSession().flush();

//        attachmentPersistenceService.deleteAllAttachment(
//                ObjectTypeCode.OPERATION,
//                fishingOperationId);
    }

    //------------------------------------------------------------------------//
    //-- Internal methods                                                   --//
    //------------------------------------------------------------------------//

    protected void getVesselUseCaracteristics(Integer fishingOperationId, FishingOperation result) {
        // retrieve fishing operation caracteristics
        Iterator<Object[]> list = queryList(
                "fishingOperationVesselUseFeatures",
                "fishingOperationId", IntegerType.INSTANCE, fishingOperationId
        );

        CaracteristicMap vesselUseCaracteristics = new CaracteristicMap();
        while (list.hasNext()) {
            int colIndex = 0;
            Object[] source = list.next();
            Integer pmfmId = (Integer) source[colIndex++];
            Float numericalValue = (Float) source[colIndex++];
            String alphanumericalValue = (String) source[colIndex++];
            Integer qualitativeValueId = (Integer) source[colIndex];

            // Trawl distance
            if (PmfmId.TRAWL_DISTANCE.getValue().equals(pmfmId)) {
                result.setTrawlDistance(numericalValue == null ? null : numericalValue.intValue());
            }

            // Rectilinear operation ?
            else if (PmfmId.RECTILINEAR_OPERATION.getValue().equals(pmfmId)) {
                result.setFishingOperationRectiligne(QualitativeValueId.RECTILINEAR_OPERATION_YES.getValue().equals(qualitativeValueId));
            }

            // Haul valid ?
            else if (PmfmId.HAUL_VALID.getValue().equals(pmfmId)) {
                if (qualitativeValueId != null) {
                    result.setFishingOperationValid(QualitativeValueId.HAUL_VALID_YES.getValue().equals(qualitativeValueId));
                } else {
                    result.setFishingOperationValid(null);
                }
            }

            // Station Number :
            else if (PmfmId.STATION_NUMBER.getValue().equals(pmfmId)) {
                result.setStationNumber(alphanumericalValue);
            }

            // Vessel Use caracteristic
            else {

                Caracteristic environmentCaracteristic = caracteristicService.getCaracteristic(pmfmId);
                Serializable value = null;
                if (environmentCaracteristic.getCaracteristicType() == CaracteristicType.NUMBER) {
                    value = numericalValue;
                } else if (environmentCaracteristic.getCaracteristicType() == CaracteristicType.TEXT) {
                    value = alphanumericalValue;
                } else if (environmentCaracteristic.getCaracteristicType() == CaracteristicType.QUALITATIVE) {
                    for (CaracteristicQualitativeValue qv : environmentCaracteristic.getQualitativeValue()) {
                        if (Objects.equals(qualitativeValueId ,Integer.parseInt(qv.getId()))) {
                            value = qv;
                            break;
                        }
                    }
                }
                vesselUseCaracteristics.put(environmentCaracteristic, value);
            }
        }

        if (vesselUseCaracteristics.size() > 0) {
            result.setVesselUseFeatures(vesselUseCaracteristics);
        }
    }

    protected void getGearUseCaracteristics(Integer fishingOperationId, FishingOperation result) {
        // retrieve fishing operation caracteristics
        Iterator<Object[]> list = queryList(
                "fishingOperationGearUseFeatures",
                "fishingOperationId", IntegerType.INSTANCE, fishingOperationId
        );

        CaracteristicMap gearShootingCaracteristics = new CaracteristicMap();
        while (list.hasNext()) {
            int colIndex = 0;
            Object[] source = list.next();
            Integer pmfmId = (Integer) source[colIndex++];
            Float numericalValue = (Float) source[colIndex++];
            String alphanumericalValue = (String) source[colIndex++];
            Integer qualitativeValueId = (Integer) source[colIndex];

            // Trawl net number
            if (PmfmId.MULTIRIG_AGGREGATION.getValue().equals(pmfmId)
                && alphanumericalValue != null
                && alphanumericalValue.matches("\\d+")) {
                result.setMultirigAggregation(alphanumericalValue);
            }

            // Gear Shooting Caracteristics
            else {
                Caracteristic gearShootingCaracteristic = caracteristicService.getCaracteristic(pmfmId);
                Serializable value = null;
                if (gearShootingCaracteristic.getCaracteristicType() == CaracteristicType.NUMBER) {
                    value = numericalValue;
                } else if (gearShootingCaracteristic.getCaracteristicType() == CaracteristicType.TEXT) {
                    value = alphanumericalValue;
                } else if (gearShootingCaracteristic.getCaracteristicType() == CaracteristicType.QUALITATIVE) {
                    for (CaracteristicQualitativeValue qv : gearShootingCaracteristic.getQualitativeValue()) {
                        if (qualitativeValueId == Integer.parseInt(qv.getId())) {
                            value = qv;
                            break;
                        }
                    }
                }
                gearShootingCaracteristics.put(gearShootingCaracteristic, value);
            }
        }

        if (gearShootingCaracteristics.size() > 0) {
            result.setGearUseFeatures(gearShootingCaracteristics);
        }
    }

    protected void beanToEntity(FishingOperation source,
                                fr.ifremer.adagio.core.dao.data.operation.FishingOperation target) {
        // Retrieve entities : FishingTrip and ScientificCruise
        ScientificCruise scientificCruise;
        FishingTrip fishingTrip = target.getFishingTrip();
        if (fishingTrip == null) {
            scientificCruise = scientificCruiseDao.load(source.getCruise().getIdAsInt());
            fishingTrip = scientificCruise.getFishingTrips().iterator().next();
            fishingTrip.getOperations().add(target); // Inverse link
        } else {
            scientificCruise = fishingTrip.getScientificCruise();
        }
        // Link to parent fishing trip
        target.setFishingTrip(fishingTrip);

        // SynchronizationStatus
        synchronizationStatusHelper.setDirty(fishingTrip);


        // Retrieve entities : VesselPosition (start and end)
        VesselPosition startPosition = null;
        VesselPosition endPosition = null;
        if (target.getVesselPositions() != null) {
            for (VesselPosition position : target.getVesselPositions()) {
                if (position.getDateTime() != null && position.getDateTime().getTime() == target.getStartDateTime().getTime()) {
                    startPosition = position;
                } else if (position.getDateTime() == null || (target.getEndDateTime() != null && position.getDateTime().getTime() == target.getEndDateTime().getTime())) {
                    endPosition = position;
                }
            }
        }

        // Retrieve entities : Gear Use Features
        GearUseFeatures gearUseFeatures;
        if (CollectionUtils.isEmpty(target.getGearUseFeatures())) {
            gearUseFeatures = GearUseFeatures.Factory.newInstance();
            gearUseFeatures.setOperation(target);
            if (target.getGearUseFeatures() == null) {
                target.setGearUseFeatures(Sets.newHashSet(gearUseFeatures));
            } else {
                target.getGearUseFeatures().add(gearUseFeatures);
            }
        } else {
            gearUseFeatures = target.getGearUseFeatures().iterator().next();
        }

        // Create a list to store all updates, then remove not updated items
        Set<GearUseMeasurement> notChangedGearUseMeasurements = new HashSet<>();
        if (gearUseFeatures.getGearUseMeasurements() != null) {
            notChangedGearUseMeasurements.addAll(gearUseFeatures.getGearUseMeasurements());
        }

        // Retrieve entities : Vessel Use Features
        VesselUseFeatures vesselUseFeatures;
        if (CollectionUtils.isEmpty(target.getVesselUseFeatures())) {
            vesselUseFeatures = VesselUseFeatures.Factory.newInstance();
            if (target.getVesselUseFeatures() == null) {
                target.setVesselUseFeatures(Sets.newHashSet(vesselUseFeatures));
                vesselUseFeatures.setOperation(target);
            } else {
                target.getVesselUseFeatures().add(vesselUseFeatures);
                vesselUseFeatures.setOperation(target);
            }
        } else {
            vesselUseFeatures = target.getVesselUseFeatures().iterator().next();
        }

        // Create a list to trace not updated items, to be able to remove them later 
        Set<VesselUseMeasurement> notChangedVesselUseMeasurements = new HashSet<>();
        if (vesselUseFeatures.getVesselUseMeasurements() != null) {
            notChangedVesselUseMeasurements.addAll(vesselUseFeatures.getVesselUseMeasurements());
        }

        boolean withGear = source.getGear() != null;

        Short gearRankOrder = null;
        if (withGear) {
            gearRankOrder = source.getGear().getRankOrder();
        }

        if (gearRankOrder == null) {
            gearRankOrder = 1;
        }

        if (log.isInfoEnabled()) {
            log.info("Use gear rankOrder: " + gearRankOrder);
        }

        // Retrieve entities : Gear Physical Features
        GearPhysicalFeatures gearPhysicalFeatures = target.getGearPhysicalFeatures();
        if (withGear) {

            // use selected gear

            if (gearPhysicalFeatures == null) {
                gearPhysicalFeatures = gearPhysicalFeaturesDao.getGearPhysicalfeatures(fishingTrip, source.getGear().getIdAsInt(), false);
                if (gearPhysicalFeatures == null) {
                    throw new DataIntegrityViolationException("An operation could not use a gear that is not declared in the cruise.");
                }
                target.setGearPhysicalFeatures(gearPhysicalFeatures);
                if (gearPhysicalFeatures.getOperations() == null) {
                    gearPhysicalFeatures.setOperations(Sets.newHashSet((Operation) target));
                } else {
                    gearPhysicalFeatures.getOperations().add(target);
                }
            }
        } else {

            // no gear
            gearUseFeatures.setGear(null);
            gearUseFeatures.setRankOrder((short) 0);
        }

        // Retrieve entities : Fishing Area
        FishingArea fishingArea;
        List<FishingArea2RegulationLocation> notChangedRegulationLocation;

        if (CollectionUtils.isEmpty(gearUseFeatures.getFishingAreas())) {
            fishingArea = FishingArea.Factory.newInstance();
            if (gearUseFeatures.getFishingAreas() == null) {
                gearUseFeatures.setFishingAreas(Sets.newHashSet(fishingArea));
                fishingArea.setGearUseFeatures(gearUseFeatures);
            } else {
                gearUseFeatures.getFishingAreas().add(fishingArea);
                fishingArea.setGearUseFeatures(gearUseFeatures);
            }
            notChangedRegulationLocation = Lists.newArrayList();
        } else {
            fishingArea = gearUseFeatures.getFishingAreas().iterator().next();

            notChangedRegulationLocation = Lists.newArrayList(fishingArea.getRegulationLocations());
            // Reset all other fishing areas
        }

        // Retrieve multirig number, from Gear physical features
        int cruiseMultirigCount = 1; // default value
        if (gearPhysicalFeatures != null) {
            GearPhysicalMeasurement gpmMultirigCount = gearPhysicalFeaturesDao.getGearPhysicalMeasurement(gearPhysicalFeatures, PmfmId.MULTIRIG_NUMBER.getValue());
            if (gpmMultirigCount != null && gpmMultirigCount.getNumericalValue() != null) {
                cruiseMultirigCount = gpmMultirigCount.getNumericalValue().intValue();
            }
        }

        // StationNumber
        if (source.getStationNumber() != null) {
            VesselUseMeasurement vum = measurementPersistenceHelper.setVesselUseMeasurement(scientificCruise, vesselUseFeatures, PmfmId.STATION_NUMBER.getValue(), null, source.getStationNumber(), null);
            notChangedVesselUseMeasurements.remove(vum);
        }

        // OP N°
        if (source.getFishingOperationNumber() != null) {
            target.setName(source.getFishingOperationNumber().toString());
        }

        // Multirig Aggregation
        if (source.getMultirigAggregation() != null) {
            if (source.getMultirigAggregation().matches("\\d+")) {
                int mutlirigNumber = Integer.valueOf(source.getMultirigAggregation());
                if (mutlirigNumber > cruiseMultirigCount) {
                    throw new DataIntegrityViolationException("An operation could not have a 'multirig number' greater than 'multirig count' defined in the cruise.");
                }
            }

            // Store into Gear Use Features
            GearUseMeasurement gum = measurementPersistenceHelper.setGearUseMeasurement(scientificCruise, gearUseFeatures, PmfmId.MULTIRIG_AGGREGATION.getValue(), null, source.getMultirigAggregation(), null);
            notChangedGearUseMeasurements.remove(gum);
        }

        // Start date :
        if (source.getGearShootingStartDate() == null) {
            target.setStartDateTime(null);
            target.setFishingStartDateTime(null);
        } else if (source.getGearShootingStartDate() != null) {
            // Reset millisecond (as need for Allegro)
            Date d = dateWithNoMiliSecond(source.getGearShootingStartDate());
            target.setStartDateTime(d);
            target.setFishingStartDateTime(d);
        }

        // End date :
        if (source.getGearShootingEndDate() == null) {
            target.setEndDateTime(null);
            target.setFishingEndDateTime(null);
        } else if (source.getGearShootingEndDate() != null) {
            // Reset millisecond (as need for Allegro)
            Date d = dateWithNoMiliSecond(source.getGearShootingEndDate());
            target.setEndDateTime(d);
            target.setFishingEndDateTime(d);
        }

        // Trawl distance
        if (source.getTrawlDistance() != null) {
            VesselUseMeasurement vum = measurementPersistenceHelper.setVesselUseMeasurement(scientificCruise, vesselUseFeatures, PmfmId.TRAWL_DISTANCE.getValue(), source.getTrawlDistance().floatValue(), null, null);
            notChangedVesselUseMeasurements.remove(vum);
        }

        // Rectilinear operation
        {
            VesselUseMeasurement vum = measurementPersistenceHelper.setVesselUseMeasurement(scientificCruise, vesselUseFeatures, PmfmId.RECTILINEAR_OPERATION.getValue(), null, null, source.isFishingOperationRectiligne() ? QualitativeValueId.RECTILINEAR_OPERATION_YES.getValue() : QualitativeValueId.RECTILINEAR_OPERATION_NO.getValue());
            notChangedVesselUseMeasurements.remove(vum);
        }

        // Operation is valid ?
        if (source.getFishingOperationValid() != null) {
            VesselUseMeasurement vum = measurementPersistenceHelper.setVesselUseMeasurement(scientificCruise, vesselUseFeatures, PmfmId.HAUL_VALID.getValue(), null, null, source.getFishingOperationValid() ? QualitativeValueId.HAUL_VALID_YES.getValue() : QualitativeValueId.HAUL_VALID_NO.getValue());
            notChangedVesselUseMeasurements.remove(vum);
        }

        // Vessel
        target.setVessel(fishingTrip.getVessel());

        // Associated vessel
        List<String> vesselIds = Lists.newArrayList();
        if (!source.isSecondaryVesselEmpty()) {
            for (Vessel vessel : source.getSecondaryVessel()) {
                vesselIds.add(vessel.getId());
            }
        }
        setOperationVesselAssociation(target, vesselIds);

        // Quality Flag :
        if (target.getQualityFlag() == null) {
            target.setQualityFlag(load(QualityFlagImpl.class, QualityFlagCode.NOTQUALIFIED.getValue()));
        }

        // Settings not null properties :
        if (target.getStartDateTime() == null) {
            // Generate a fake departureDate (precision=minute) then add 1 millisecond
            Date d = dateWithNoSecondAndOneMiliSecond(scientificCruise.getDepartureDateTime());
            target.setStartDateTime(d);
            target.setFishingStartDateTime(d);
        }

        // VesselUseFeatures :
        vesselUseFeatures.setStartDate(target.getStartDateTime());
        if (vesselUseFeatures.getStartDate() == null) {
            vesselUseFeatures.setStartDate(scientificCruise.getDepartureDateTime());
        }
        vesselUseFeatures.setEndDate(target.getEndDateTime());
        vesselUseFeatures.setVessel(target.getVessel());
        vesselUseFeatures.setProgram(scientificCruise.getProgram());
        vesselUseFeatures.setIsActive(isActive.ACTIVE.getValue());
        if (vesselUseFeatures.getCreationDate() == null) {
            Date d = newCreateDate();
            vesselUseFeatures.setCreationDate(d);
        }
        if (vesselUseFeatures.getQualityFlag() == null) {
            vesselUseFeatures.setQualityFlag(load(QualityFlagImpl.class, QualityFlagCode.NOTQUALIFIED.getValue()));
        }

        // GearUseFeatures :
        gearUseFeatures.setStartDate(target.getStartDateTime());
        if (gearUseFeatures.getStartDate() == null) {
            gearUseFeatures.setStartDate(scientificCruise.getDepartureDateTime());
        }
        gearUseFeatures.setEndDate(target.getEndDateTime());
        gearUseFeatures.setVessel(target.getVessel());
        gearUseFeatures.setProgram(scientificCruise.getProgram());
        if (gearUseFeatures.getCreationDate() == null) {
            Date d = newCreateDate();
            gearUseFeatures.setCreationDate(d);
        }
        if (gearUseFeatures.getQualityFlag() == null) {
            gearUseFeatures.setQualityFlag(load(QualityFlagImpl.class, QualityFlagCode.NOTQUALIFIED.getValue()));
        }

        // GearUseFeatures.Gear
        if (!withGear) {
            gearUseFeatures.setGear(null);
            gearUseFeatures.setRankOrder((short) 0);
        } else {
            gearUseFeatures.setGear(load(GearImpl.class, source.getGear().getIdAsInt()));
            gearUseFeatures.setRankOrder(gearRankOrder);
        }

        // Start position :
        // If need to be store
        if (source.getGearShootingEndLatitude() != null
            || source.getGearShootingEndDate() != null
            || source.getGearShootingEndLongitude() != null) {

            if (startPosition == null) {
                startPosition = VesselPosition.Factory.newInstance();
                startPosition.setOperation(target);
                if (target.getVesselPositions() == null) {
                    target.setVesselPositions(Sets.newHashSet(startPosition));
                }
                target.getVesselPositions().add(startPosition);
            }
            startPosition.setDateTime(target.getStartDateTime());
            startPosition.setLatitude(source.getGearShootingStartLatitude() == null ? DEFAULT_EMPTY_LATITUDE : source.getGearShootingStartLatitude());
            startPosition.setLongitude(source.getGearShootingStartLongitude() == null ? DEFAULT_EMPTY_LONGITUDE : source.getGearShootingStartLongitude());
            startPosition.setVessel(target.getVessel());
            startPosition.setProgram(scientificCruise.getProgram());
            startPosition.setRecorderDepartment(scientificCruise.getRecorderDepartment());
            startPosition.setQualityFlag(target.getQualityFlag());
        }

        // If not need to be store, delete start position from list 
        else if (startPosition != null && target.getVesselPositions() != null) {
            target.getVesselPositions().remove(startPosition);
        }

        // End position :
        // If need to be store
        if (source.getGearShootingEndLatitude() != null
            || source.getGearShootingEndDate() != null
            || source.getGearShootingEndLongitude() != null) {

            if (endPosition == null) {
                endPosition = VesselPosition.Factory.newInstance();
                endPosition.setOperation(target);
                target.getVesselPositions().add(endPosition);
            }
            Date endDateTime = convertUI2DatabaseMandatoryDate(
                    target.getEndDateTime(),
                    startPosition.getDateTime(), true);
            endPosition.setDateTime(endDateTime);
            if (!endDateTime.equals(target.getEndDateTime())) {
                // To link position with the operation end, Allegro need to have exactly the same dates
                target.setEndDateTime(endDateTime);
                target.setFishingEndDateTime(endDateTime);
            }
            endPosition.setLatitude(convertUI2DatabaseMandatoryLatitude(source.getGearShootingEndLatitude()));
            endPosition.setLongitude(convertUI2DatabaseMandatoryLatitude(source.getGearShootingEndLongitude()));
            endPosition.setVessel(target.getVessel());
            endPosition.setProgram(scientificCruise.getProgram());
            endPosition.setRecorderDepartment(scientificCruise.getRecorderDepartment());
            endPosition.setQualityFlag(target.getQualityFlag());
        }

        // If not need to be store, delete end position from list 
        else if (endPosition != null && target.getVesselPositions() != null) {
            target.getVesselPositions().remove(endPosition);
        }

        // Vessel user features
        CaracteristicMap vesselUseCaracteristics = source.getVesselUseFeatures();
        if (MapUtils.isNotEmpty(vesselUseCaracteristics)) {
            for (Caracteristic caracteristic : vesselUseCaracteristics.keySet()) {
                VesselUseMeasurement vum = measurementPersistenceHelper.setVesselUseMeasurement(scientificCruise, vesselUseFeatures, caracteristic, vesselUseCaracteristics.get(caracteristic));
                notChangedVesselUseMeasurements.remove(vum);
            }
        }

        // ----------------------------------------------------------------
        // Recorder persons                                            ---
        // ----------------------------------------------------------------

        Map<Integer, VesselPersonFeatures> vesselPersonFeaturesMap = Maps.newTreeMap();

        VesselPersonRole recorderPersonRole =
                vesselPersonFeaturesPersistenceHelper.getRecorderPersonRole();

        short personRankOrder = 1;
        if (CollectionUtils.isNotEmpty(source.getRecorderPerson())) {
            for (Person person : source.getRecorderPerson()) {
                Integer personId = person.getIdAsInt();
                vesselPersonFeaturesPersistenceHelper.fillVesselPersonFeatures(
                        vesselPersonFeaturesMap,
                        personId,
                        target,
                        recorderPersonRole,
                        personRankOrder++);
            }
        }
        if (target.getVesselPersonFeatures() == null) {
            target.setVesselPersonFeatures(Sets.<VesselPersonFeatures>newHashSet());
        }
        target.getVesselPersonFeatures().clear();
        target.getVesselPersonFeatures().addAll(vesselPersonFeaturesMap.values());

        // Comment
        target.setComments(source.getComment());

        // ----------------------------------------------------------------
        // Removed unecessary *UseMeasurement :                         ---
        // ----------------------------------------------------------------

        // Gear use Caracteristics
        CaracteristicMap gearUseCaracteristics = source.getGearUseFeatures();
        if (MapUtils.isNotEmpty(gearUseCaracteristics)) {
            for (Caracteristic caracteristic : gearUseCaracteristics.keySet()) {
                GearUseMeasurement gum = measurementPersistenceHelper.setGearUseMeasurement(
                        scientificCruise,
                        gearUseFeatures,
                        caracteristic,
                        gearUseCaracteristics.get(caracteristic));
                notChangedGearUseMeasurements.remove(gum);
            }
        }

        // Removed not changed measurements (in Vessel & Gear Use Measurement lists)
        if (vesselUseFeatures.getVesselUseMeasurements() != null &&
            notChangedVesselUseMeasurements.size() > 0) {
            vesselUseFeatures.getVesselUseMeasurements().removeAll(notChangedVesselUseMeasurements);

        }
        if (gearUseFeatures.getGearUseMeasurements() != null &&
            notChangedGearUseMeasurements.size() > 0) {
            gearUseFeatures.getGearUseMeasurements().removeAll(notChangedGearUseMeasurements);
        }

        // ----------------------------------------------------------------
        // Fishing Area : Strata, substrata, localite                   ---
        // ----------------------------------------------------------------

        // Compute a statistical rectangle, using lat/long (to be used in FishingArea.location)
        Integer statisticalLocationId = null;
        if (source.getGearShootingStartLatitude() != null && source.getGearShootingStartLongitude() != null) {
            statisticalLocationId = locationService.getLocationIdByLatLong(source.getGearShootingStartLatitude(), source.getGearShootingStartLongitude());
        }
        if (statisticalLocationId == null && source.getGearShootingEndLatitude() != null && source.getGearShootingEndLongitude() != null) {
            statisticalLocationId = locationService.getLocationIdByLatLong(source.getGearShootingEndLatitude(), source.getGearShootingEndLongitude());
        }

        // Strata :
        if (source.getStrata() != null && source.getStrata().getId() != null) {
            FishingArea2RegulationLocation fa2rl = getFishingArea2RegulationLocation(fishingArea,
                                                                                     source.getStrata().getIdAsInt(),
                                                                                     true /*create if need*/);
            notChangedRegulationLocation.remove(fa2rl);

            // If no statistical location define yet, then use strata
            if (statisticalLocationId == null) {
                statisticalLocationId = source.getStrata().getIdAsInt();
            }
        }

        // Sub-Strata :
        if (source.getSubStrata() != null && source.getSubStrata().getId() != null) {
            FishingArea2RegulationLocation fa2rl = getFishingArea2RegulationLocation(fishingArea,
                                                                                     source.getSubStrata().getIdAsInt(),
                                                                                     true /*create if need*/);
            notChangedRegulationLocation.remove(fa2rl);

            // If no statistical location define yet, then use sub-strata
            if (statisticalLocationId == null) {
                statisticalLocationId = source.getSubStrata().getIdAsInt();
            }
        }

        // Localite :
        if (source.getLocation() != null && source.getLocation().getId() != null) {
            FishingArea2RegulationLocation fa2rl = getFishingArea2RegulationLocation(fishingArea,
                                                                                     source.getLocation().getIdAsInt(),
                                                                                     true /*create if need*/);
            notChangedRegulationLocation.remove(fa2rl);

            // If no statistical location define yet, then use Localité
            if (statisticalLocationId == null) {
                statisticalLocationId = source.getLocation().getIdAsInt();
            }
        }

        // Remove unused regulation locations
        fishingArea.getRegulationLocations().removeAll(notChangedRegulationLocation);

        // Fishing Area location (should be a statistical location)
        if (statisticalLocationId == null) {
            gearUseFeatures.getFishingAreas().remove(fishingArea);
            //Nothing to do : a gearUseFeatures.getFishingAreas().clear() has been done before
            if (fishingArea.getRegulationLocations() != null) {
                fishingArea.getRegulationLocations().clear();
            }
        } else if (statisticalLocationId != null) {
            fishingArea.setLocation(load(LocationImpl.class, statisticalLocationId));
        }
    }

    /**
     * Test if the latitude is null, and return a default value if yes
     *
     * @param databaseValue the latitude used in UI (could be null)
     * @return null the latitude to store in database (could not be null)
     */
    protected Float convertUI2DatabaseMandatoryLatitude(Float databaseValue) {
        return databaseValue != null ? databaseValue : DEFAULT_EMPTY_LATITUDE;
    }

    /**
     * Test if the latitude is a fake value. This yes, return null, then return the given value.
     *
     * @param databaseValue the latitude stored in the database (could be fake date, not null only because of database constraints)
     * @return null if the latitude is fake
     */
    protected Float convertLatitude2UI(Float databaseValue) {
        return DEFAULT_EMPTY_LATITUDE.equals(databaseValue) ? null : databaseValue;
    }

//    /**
//     * Test if the latitude is null, and return a default value if yes
//     *
//     * @param databaseValue the latitude used in UI (could be null)
//     * @return null the latitude to store in database (could not be null)
//     */
//    protected Float convertUI2DatabaseMandatoryLongitude(Float databaseValue) {
//        return (databaseValue != null) ? databaseValue : DEFAULT_EMPTY_LONGITUDE;
//    }

    /**
     * Test if the longitude is a fake value. This yes, return null, then return the given value.
     *
     * @param databaseValue the longitude stored in the database (could be fake date, not null only because of database constraints)
     * @return null if the longitude is fake
     */
    protected Float convertLongitude2UI(Float databaseValue) {
        return DEFAULT_EMPTY_LONGITUDE.equals(databaseValue) ? null : databaseValue;
    }

    protected void setOperationVesselAssociation(Operation target, List<String> vesselCodes) {

        boolean noOperationVesselAssociations =
                target.getOperationVesselAssociations() == null;

        Set<OperationVesselAssociation> toAdd = Sets.newHashSet();
        Set<OperationVesselAssociation> notChanged = Sets.newHashSet();

        if (!noOperationVesselAssociations) {
            notChanged.addAll(target.getOperationVesselAssociations());
        }

        for (String vesselCode : vesselCodes) {

            OperationVesselAssociation ova = null;
            OperationVesselAssociationPK ovaPK = new OperationVesselAssociationPK();
            ovaPK.setVessel(load(VesselImpl.class, vesselCode));
            ovaPK.setOperation((OperationImpl) target);

            // If vessel is equal as cruise vessel : do note store any VesselOperationAssociation entity
            if (vesselCode.equals(target.getVessel().getCode())) {
                //removeAllOperationVesselAssociation(target);
                break;
            }

            // Retrieve existing association
            for (OperationVesselAssociation asso : notChanged) {
                if (asso.getOperationVesselAssociationPk().equals(ovaPK)) {
                    ova = asso;
                    break;
                }
            }

            // Create a new association
            if (ova == null) {
                ova = OperationVesselAssociation.Factory.newInstance();
                ova.setOperationVesselAssociationPk(ovaPK);
//                if (target.getOperationVesselAssociations() == null) {
//                    target.setOperationVesselAssociations(Lists.newArrayList(ova));
//                } else {
//                    removeAllOperationVesselAssociation(target);
//                    target.getOperationVesselAssociations().add(ova);
//                }
            }

            ova.setIsCatchOnOperationVessel(Boolean.FALSE);
            toAdd.add(ova);
            notChanged.remove(ova);
        }

        if (!noOperationVesselAssociations) {

            // remove all not changed

            for (OperationVesselAssociation operationVesselAssociation : notChanged) {
                operationVesselAssociationDao.remove(operationVesselAssociation);
            }
            target.getOperationVesselAssociations().removeAll(notChanged);
        }

        if (!toAdd.isEmpty()) {

            if (noOperationVesselAssociations) {
                target.setOperationVesselAssociations(Sets.<OperationVesselAssociation>newHashSet());
            }
            target.getOperationVesselAssociations().addAll(toAdd);
        }
    }

    /**
     * Issue #4995 : use this method to avoid re-create of existing regulation location
     *
     * @param fishingArea TODO
     * @param regulationLocationId TODO
     * @param createIfNotExists TODO
     * @return a entity FishingArea2RegulationLocation, or {@code null} if not found and createIfNotExists=false
     */
    protected FishingArea2RegulationLocation getFishingArea2RegulationLocation(FishingArea fishingArea,
                                                                               int regulationLocationId,
                                                                               boolean createIfNotExists) {
        Preconditions.checkNotNull(fishingArea);

        // Create the PK
        FishingArea2RegulationLocationPK pk = new FishingArea2RegulationLocationPK();
        pk.setFishingArea((FishingAreaImpl) fishingArea);
        pk.setLocation(load(LocationImpl.class, regulationLocationId));

        // Retrieve existing regulation location
        FishingArea2RegulationLocation fa2rl = null;
        if (CollectionUtils.isNotEmpty(fishingArea.getRegulationLocations())) {
            for (FishingArea2RegulationLocation existingFa2rl : fishingArea.getRegulationLocations()) {
                FishingArea2RegulationLocationPK existingPk = existingFa2rl.getFishingArea2RegulationLocationPk();
                if (Objects.equals(existingPk, pk)) {
                    fa2rl = existingFa2rl;
                    break;
                }
            }
        }

        // If not exists, create if need
        if (fa2rl != null || !createIfNotExists) {
            return fa2rl;
        }

        fa2rl = FishingArea2RegulationLocation.Factory.newInstance();
        fa2rl.setFishingArea2RegulationLocationPk(pk);

        if (fishingArea.getRegulationLocations() == null) {
            fishingArea.setRegulationLocations(Sets.newHashSet(fa2rl));
        } else {
            fishingArea.getRegulationLocations().add(fa2rl);
        }

        return fa2rl;
    }
}

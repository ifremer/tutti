package fr.ifremer.tutti.persistence.service;

/*
 * #%L
 * Tutti :: Persistence
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Lists;
import fr.ifremer.adagio.core.dao.administration.programStrategy.Program;
import fr.ifremer.adagio.core.dao.administration.user.DepartmentId;
import fr.ifremer.adagio.core.dao.administration.user.DepartmentImpl;
import fr.ifremer.adagio.core.dao.administration.user.PersonId;
import fr.ifremer.adagio.core.dao.administration.user.PersonImpl;
import fr.ifremer.adagio.core.dao.data.batch.Batch;
import fr.ifremer.adagio.core.dao.data.batch.CatchBatch;
import fr.ifremer.adagio.core.dao.data.batch.SortingBatch;
import fr.ifremer.adagio.core.dao.data.operation.FishingOperationImpl;
import fr.ifremer.adagio.core.dao.data.sample.Sample;
import fr.ifremer.adagio.core.dao.referential.QualityFlagCode;
import fr.ifremer.adagio.core.dao.referential.QualityFlagImpl;
import fr.ifremer.adagio.core.dao.referential.pmfm.Matrix;
import fr.ifremer.adagio.core.dao.referential.pmfm.MatrixId;
import fr.ifremer.adagio.core.dao.referential.pmfm.MatrixImpl;
import fr.ifremer.adagio.core.dao.referential.taxon.ReferenceTaxonImpl;
import fr.ifremer.tutti.persistence.entities.CaracteristicMap;
import fr.ifremer.tutti.persistence.entities.TuttiEntities;
import fr.ifremer.tutti.persistence.entities.data.CopyIndividualObservationMode;
import fr.ifremer.tutti.persistence.entities.data.FishingOperation;
import fr.ifremer.tutti.persistence.entities.data.IndividualObservationBatch;
import fr.ifremer.tutti.persistence.entities.data.IndividualObservationBatchs;
import fr.ifremer.tutti.persistence.entities.referential.Caracteristic;
import fr.ifremer.tutti.persistence.entities.referential.CaracteristicQualitativeValue;
import fr.ifremer.tutti.persistence.entities.referential.Species;
import fr.ifremer.tutti.persistence.service.referential.CaracteristicPersistenceService;
import fr.ifremer.tutti.persistence.service.referential.SpeciesPersistenceService;
import fr.ifremer.tutti.persistence.service.util.BatchPersistenceHelper;
import fr.ifremer.tutti.persistence.service.util.SamplePersistenceHelper;
import fr.ifremer.tutti.persistence.service.util.SynchronizationStatusHelper;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.type.IntegerType;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.4
 */
@Service("individualObservationBatchPersistenceService")
public class IndividualObservationBatchPersistenceServiceImpl extends AbstractPersistenceService implements IndividualObservationBatchPersistenceService {

    /** Logger. */
    private static final Log log =
            LogFactory.getLog(IndividualObservationBatchPersistenceServiceImpl.class);

    @Resource(name = "caracteristicPersistenceService")
    private CaracteristicPersistenceService caracteristicService;

    @Resource(name = "speciesPersistenceService")
    private SpeciesPersistenceService speciesService;

    @Resource(name = "attachmentPersistenceService")
    protected AttachmentPersistenceService attachmentPersistenceService;

    @Resource(name = "samplePersistenceHelper")
    protected SamplePersistenceHelper samplePersistenceHelper;

    @Resource(name = "batchPersistenceHelper")
    protected BatchPersistenceHelper batchHelper;

    @Resource(name = "synchronizationStatusHelper")
    protected SynchronizationStatusHelper synchronizationStatusHelper;

    @Resource(name = "fishingOperationPersistenceService")
    protected FishingOperationPersistenceService fishingOperationPersistenceService;

    protected Caracteristic sampleCodeCaracteristic;

    @Override
    public void init() {
        super.init();

        sampleCodeCaracteristic = caracteristicService.getSampleCodeCaracteristic();
    }

    @Override
    public List<IndividualObservationBatch> getAllIndividualObservationBatchsForCruise(Integer cruiseId) {
        Preconditions.checkNotNull(cruiseId);

        List<IndividualObservationBatch> result = new ArrayList<>();
        List<Integer> allFishingOperationIds = fishingOperationPersistenceService.getAllFishingOperationIds(cruiseId);
        allFishingOperationIds.forEach(fishingOperationId -> {

            Iterator<Object[]> list = queryList("allFishingOperationSamplesWithBatch",
                                                "fishingOperationId", IntegerType.INSTANCE, fishingOperationId);

            List<IndividualObservationBatch> resultForFishingOperation = toBeanListWithFishingOperation(list, fishingOperationId);
            result.addAll(resultForFishingOperation);

        });
        return Collections.unmodifiableList(result);

    }

    @Override
    public boolean isSamplingCodeAvailable(Integer cruiseId, Integer referenceTaxonId, String samplingCodeSuffix) {

        Preconditions.checkNotNull(cruiseId);
        Preconditions.checkNotNull(referenceTaxonId);
        Preconditions.checkNotNull(samplingCodeSuffix);

        List<Integer> allFishingOperationIds = fishingOperationPersistenceService.getAllFishingOperationIds(cruiseId);
        for (Integer fishingOperationId : allFishingOperationIds) {

            Iterator<Integer> list = queryListTyped("allFishingOperationSampleIdsWithBatchForSpecies",
                                                    "fishingOperationId", IntegerType.INSTANCE, fishingOperationId,
                                                    "referenceTaxonId", IntegerType.INSTANCE, referenceTaxonId);

            while (list.hasNext()) {

                Integer sampleId = list.next();

                Serializable sampleMeasurementValue = samplePersistenceHelper.getSampleMeasurementValue(sampleId, sampleCodeCaracteristic);
                if (sampleMeasurementValue != null) {

                    if (sampleMeasurementValue.toString().endsWith(samplingCodeSuffix)) {

                        // sampling code suffix found, stop NOW!
                        return false;

                    }

                }

            }

        }

        // Free to use
        return true;

    }

    @Override
    public List<IndividualObservationBatch> getAllIndividualObservationBatchsForFishingOperation(Integer fishingOperationId) {
        Preconditions.checkNotNull(fishingOperationId);

        Iterator<Object[]> list = queryList("allFishingOperationSamplesWithBatch",
                                            "fishingOperationId", IntegerType.INSTANCE, fishingOperationId);

        List<IndividualObservationBatch> result = toBeanListWithFishingOperation(list, fishingOperationId);

        return Collections.unmodifiableList(result);

    }

    @Override
    public List<IndividualObservationBatch> getAllIndividualObservationBatchsForBatch(Integer batchId) {
        Preconditions.checkNotNull(batchId);

        Iterator<Object[]> list = queryList("allFishingOperationSamplesForBatch",
                                            "batchId", IntegerType.INSTANCE, batchId);

        List<IndividualObservationBatch> result = toBeanList(list);
        return Collections.unmodifiableList(result);

    }

    @Override
    public List<IndividualObservationBatch> createIndividualObservationBatches(FishingOperation fishingOperation, Collection<IndividualObservationBatch> individualObservations) {

        Preconditions.checkNotNull(individualObservations);

        // remove null observations (says with no length step caracteristic)
        List<IndividualObservationBatch> notNullObservations = individualObservations.stream()
                                                                                     .filter(source -> source.getLengthStepCaracteristic() != null)
                                                                                     .collect(Collectors.toList());

        ArrayListMultimap<Integer, IndividualObservationBatch> individualObservationBatchesByBatchId = ArrayListMultimap.create();

        notNullObservations.forEach(individualObservationBatch -> individualObservationBatchesByBatchId.put(individualObservationBatch.getBatchId(), individualObservationBatch));

        for (Map.Entry<Integer, Collection<IndividualObservationBatch>> entry : individualObservationBatchesByBatchId.asMap().entrySet()) {

            Integer speciesBatchId = entry.getKey();
            Collection<IndividualObservationBatch> individualObservationBatches = entry.getValue();

            createOrSave(speciesBatchId, individualObservationBatches);

        }

        return Collections.unmodifiableList(notNullObservations);

    }

    @Override
    public List<IndividualObservationBatch> saveBatchIndividualObservation(Integer speciesBatchId,
                                                                           List<IndividualObservationBatch> individualObservation) {

        Preconditions.checkNotNull(speciesBatchId);
        Preconditions.checkNotNull(individualObservation);

        // remove null observations (says with no length step caracteristic)
        List<IndividualObservationBatch> notNullObservations = individualObservation.stream()
                                                                                    .filter(source -> source.getLengthStepCaracteristic() != null)
                                                                                    .collect(Collectors.toList());

        createOrSave(speciesBatchId, individualObservation);

        return Collections.unmodifiableList(notNullObservations);

    }

    @Override
    public void deleteAllIndividualObservationsForFishingOperation(Integer fishingOperationId) {

        Set<Integer> individualObservationIds = getAllIndividualObservationBatchIds(fishingOperationId);

        if (log.isInfoEnabled()) {
            log.info(String.format("[Fishing Operation: %d] Delete %d individual observations.", fishingOperationId, individualObservationIds.size()));
        }

        individualObservationIds.forEach(samplePersistenceHelper::deleteSample);

    }

    @Override
    public void deleteAllIndividualObservationsForBatch(Integer speciesBatchId) {

        Set<Integer> allSpeciesBatchIds = batchHelper.getBatchIds(speciesBatchId);

        for (Integer aSpeciesBatchId : allSpeciesBatchIds) {

            Set<Integer> individualObservationIds = getAllIndividualObservationBatchIdsForBatch(aSpeciesBatchId);

            if (log.isInfoEnabled()) {
                log.info(String.format("[Species batch: %d] Delete %d individual observations.", aSpeciesBatchId, individualObservationIds.size()));
            }
            individualObservationIds.forEach(samplePersistenceHelper::deleteSample);

        }

    }

    // ------------------------------------------------------------------------//
    // -- Internal methods                                                   --//
    // ------------------------------------------------------------------------//

    protected void beanToEntity(IndividualObservationBatch source, Sample target, Batch batch) {

        // operation
        fr.ifremer.adagio.core.dao.data.operation.FishingOperation fishingOperation;

        fishingOperation = load(FishingOperationImpl.class, source.getFishingOperation().getIdAsInt());

        // Link to fishing operation
        target.setFishingOperation(fishingOperation);

        // Link to parent batch
        target.setBatch(batch);
        target.setFishingOperation(fishingOperation);

        if (TuttiEntities.isNew(source)) {

            // Label
            String label = batch.getId() + "_" + source.getSpecies().getReferenceTaxonId();
            target.setLabel(label);

            // Matrix (product / batch)
            Matrix matrix = load(MatrixImpl.class, MatrixId.PRODUCE_BATCH.getValue());
            target.setMatrix(matrix);

            // IndividualCount
            target.setIndividualCount((short) 1);

            // Quality Flag
            target.setQualityFlag(load(QualityFlagImpl.class, QualityFlagCode.NOTQUALIFIED.getValue()));

            // Sample Date
            if (target.getSampleDate() == null) {
                target.setSampleDate(fishingOperation.getFishingStartDateTime());
            }

            // Create Date
            target.setCreationDate(fishingOperation.getFishingStartDateTime());

            // Recorder Departement
            target.setRecorderDepartment(load(DepartmentImpl.class, DepartmentId.UNKNOWN_RECORDER_DEPARTMENT.getValue()));

            // Recorder Person
            target.setRecorderPerson(load(PersonImpl.class, PersonId.UNKNOWN_RECORDER_PERSON.getValue()));

            // Program
            Program program = fishingOperation.getFishingTrip().getProgram();
            target.setProgram(program);
        }

        // Id
        target.setId(source.getIdAsInt());

        // Comment
        target.setComments(source.getComment());

        // ReferenceTaxon
        Species species = source.getSpecies();
        Integer referenceTaxonId = species.getReferenceTaxonId();
        target.setReferenceTaxon(load(ReferenceTaxonImpl.class, referenceTaxonId));

        // Taxongroup TODO

        // FishingAreas TODO

        // Prepare sample measurements

        CaracteristicMap caracteristics = samplePersistenceHelper.extractCommonSampleCaracteristics(source);

        CopyIndividualObservationMode copyIndividualObservationMode = source.getCopyIndividualObservationMode();
        Objects.requireNonNull(copyIndividualObservationMode, "Any individual observation requires a copy mode");

        Caracteristic copyIndividualObservationModeCaracteristic = caracteristicService.getCopyIndividualObservationModeCaracteristic();
        CaracteristicQualitativeValue qualitativeValue = copyIndividualObservationMode.getQualitativeValue(copyIndividualObservationModeCaracteristic);
        caracteristics.put(copyIndividualObservationModeCaracteristic, qualitativeValue);

        if (StringUtils.isNotBlank(source.getSamplingCode())) {
            Caracteristic sampleCodeCaracteristic = caracteristicService.getSampleCodeCaracteristic();
            caracteristics.put(sampleCodeCaracteristic, source.getSamplingCode());
        }

        samplePersistenceHelper.setSampleMeasurements(target, caracteristics);

    }

    protected IndividualObservationBatch toBean(Object[] source) {

        IndividualObservationBatch batch = IndividualObservationBatchs.newIndividualObservationBatch();

        int colIndex = 0;

        // Id
        batch.setId((Integer) source[colIndex++]);

        // BatchId
        batch.setBatchId((Integer) source[colIndex++]);

        // TaxonId
        Integer taxonId = (Integer) source[colIndex++];
        Species species = speciesService.getSpeciesByReferenceTaxonId(taxonId);
        batch.setSpecies(species);

        // Comment
        batch.setComment((String) source[colIndex]);

        // synchronizationStatus
        batch.setSynchronizationStatus((String) source[colIndex]);

        // Sample Measurements
        batch.setCaracteristics(new CaracteristicMap());

        // fill all measurements
        fillSampleMeasurements(batch);

        return batch;

    }

    protected List<IndividualObservationBatch> toBeanList(Iterator<Object[]> list) {

        List<IndividualObservationBatch> result = Lists.newArrayList();

        while (list.hasNext()) {

            IndividualObservationBatch individualObservationBatch = toBean(list.next());
            result.add(individualObservationBatch);

        }

        return result;

    }

    protected List<IndividualObservationBatch> toBeanListWithFishingOperation(Iterator<Object[]> list, Integer fishingOperationId) {

        List<IndividualObservationBatch> result = toBeanList(list);
        FishingOperation fishingOperation = fishingOperationPersistenceService.getFishingOperation(fishingOperationId);
        result.forEach(individualObservationBatch -> individualObservationBatch.setFishingOperation(fishingOperation));
        return result;
    }

    protected Batch getBatch(Integer operationId) {
        Preconditions.checkNotNull(operationId);

        return batchHelper.getRootCatchBatchByFishingOperationId(operationId, false);
    }

    protected void fillSampleMeasurements(IndividualObservationBatch batch) {

        samplePersistenceHelper.loadSampleMeasurements(batch);

        CaracteristicMap caracteristics = batch.getCaracteristics();

        Caracteristic copyIndividualObservationModeCaracteristic = caracteristicService.getCopyIndividualObservationModeCaracteristic();

        CaracteristicQualitativeValue copyIndividualObservationMode = caracteristics.removeQualitativeValue(copyIndividualObservationModeCaracteristic);
        if (copyIndividualObservationMode != null) {
            batch.setCopyIndividualObservationMode(CopyIndividualObservationMode.valueOf(copyIndividualObservationMode.getIdAsInt()));
        } else {
            batch.setCopyIndividualObservationMode(CopyIndividualObservationMode.NOTHING);
        }

        Caracteristic sampleCodeCaracteristic = caracteristicService.getSampleCodeCaracteristic();
        String sampleCode = caracteristics.removeStringValue(sampleCodeCaracteristic);
        batch.setSamplingCode(sampleCode);
    }

    protected Set<Integer> getAllIndividualObservationBatchIdsForBatch(Integer batchId) {

        Preconditions.checkNotNull(batchId);
        Iterator<Integer> list = queryListTyped("allFishingOperationSampleIdsForBatch",
                                                "batchId", IntegerType.INSTANCE, batchId);

        return toIds(list);

    }

    protected Set<Integer> getAllIndividualObservationBatchIds(Integer fishingOperationId) {

        Preconditions.checkNotNull(fishingOperationId);

        Iterator<Integer> list = queryListTyped("allFishingOperationSampleIds",
                                                "fishingOperationId", IntegerType.INSTANCE, fishingOperationId);

        return toIds(list);

    }

    protected Set<Integer> toIds(Iterator<Integer> list) {

        Set<Integer> result = new LinkedHashSet<>();

        while (list.hasNext()) {
            Integer id = list.next();
            result.add(id);
        }
        return result;

    }

    protected void createOrSave(Integer speciesBatchId, Collection<IndividualObservationBatch> notNullObservations) {

        CatchBatch catchBatch = batchHelper.getRootCatchBatchByBatchId(speciesBatchId);

        if (catchBatch == null) {

            throw new IllegalStateException("Can't find catch batch from batchId: " + speciesBatchId);

        }

        // Synchronization status
        synchronizationStatusHelper.setDirty(catchBatch);

        // Retrieve batch
        SortingBatch batch = batchHelper.getSortingBatchById(catchBatch, speciesBatchId);
        if (batch == null) {

            throw new IllegalStateException("Can't find batch from batchId: " + speciesBatchId);

        }

        // Remember child ids, to remove unchanged item (see at bottom in this method)
        Set<Integer> notUpdatedChildIds = getAllIndividualObservationBatchIdsForBatch(speciesBatchId);

        for (IndividualObservationBatch source : notNullObservations) {

            Sample target;
            if (TuttiEntities.isNew(source)) {

                // Not existing batch
                target = Sample.Factory.newInstance();

                // Fill the sample from the source
                beanToEntity(source, target, batch);

                // Create the targeted sample, then update the source id
                samplePersistenceHelper.create(target);
                source.setId(String.valueOf(target.getId()));

                if (log.isInfoEnabled()) {
                    log.info("Create individual observation (" + source.getRankOrder() + "): " + target.getId());
                }

            } else {

                // Existing batch
                target = samplePersistenceHelper.load(source.getIdAsInt());

                // Fill the sorting batch from the source
                beanToEntity(source, target, batch);
                samplePersistenceHelper.update(target);

                // Remove id from id to remove
                notUpdatedChildIds.remove(target.getId());

                if (log.isInfoEnabled()) {
                    log.info("Update individual observation (" + source.getRankOrder() + "): " + target.getId());
                }
            }

            synchronizationStatusHelper.setDirty(source);

        }

        if (CollectionUtils.isNotEmpty(notUpdatedChildIds)) {

            // Remove obsolete individual observations
            for (Integer observationId : notUpdatedChildIds) {

                if (log.isInfoEnabled()) {
                    log.info("Remove obsolete individual observation: " + observationId);
                }
                samplePersistenceHelper.deleteSample(observationId);
            }

        }

    }
}

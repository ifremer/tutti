package fr.ifremer.tutti.persistence.model;

/*
 * #%L
 * Tutti :: Persistence
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.entities.data.FishingOperation;

/**
 * Created on 3/29/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.14.3
 */
public class OperationDataModel extends DataModelSupport {

    private static final long serialVersionUID = 1L;

    private static String getLabel(FishingOperation operation) {
        return String.format("%1$s - %2$d - %3$s - %4$td/%4$tm/%4$tY",
                             operation.getStationNumber(),
                             operation.getFishingOperationNumber(),
                             operation.getMultirigAggregation(),
                             operation.getGearShootingStartDate());
    }

    public OperationDataModel(FishingOperation operation) {
        this(operation.getId(), getLabel(operation));
    }

    public OperationDataModel(String id, String label) {
        super(id,label);
    }


}

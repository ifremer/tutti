package fr.ifremer.tutti.persistence.service;

/*
 * #%L
 * Tutti :: Persistence
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Multimap;
import fr.ifremer.tutti.persistence.InvalidBatchModelException;
import fr.ifremer.tutti.persistence.entities.data.BatchContainer;
import fr.ifremer.tutti.persistence.entities.data.SampleCategoryModel;
import fr.ifremer.tutti.persistence.entities.data.SpeciesBatch;
import fr.ifremer.tutti.persistence.entities.data.SpeciesBatchFrequency;
import fr.ifremer.tutti.persistence.entities.data.SpeciesBatchFrequencys;
import fr.ifremer.tutti.persistence.entities.data.SpeciesBatchs;
import fr.ifremer.tutti.persistence.entities.referential.Species;
import fr.ifremer.tutti.persistence.service.util.BatchPersistenceHelper;
import fr.ifremer.tutti.persistence.service.util.tree.BenthosBatchTreeHelper;
import fr.ifremer.tutti.persistence.service.util.tree.SpeciesBatchTreeHelperSupport;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Collection;
import java.util.List;

/**
 * Default implementation of {@link BenthosBatchPersistenceService}.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.2
 */
@Service("benthosBatchPersistenceService")
public class BenthosBatchPersistenceServiceImpl extends SpeciesBatchPersistenceServiceSupport
        implements BenthosBatchPersistenceService {

    /** Logger. */
    private static final Log log = LogFactory.getLog(BenthosBatchPersistenceServiceImpl.class);

    @Resource(name = "benthosBatchTreeHelper")
    protected BenthosBatchTreeHelper batchTreeHelper;

    public BenthosBatchPersistenceServiceImpl() {
        super("Benthos",  SpeciesBatchs::newBenthosBatch, SpeciesBatchFrequencys::newBenthosBatchFrequency);
    }

    //------------------------------------------------------------------------//
    //-- Benthos Batch methods                                              --//
    //------------------------------------------------------------------------//

    @Override
    protected SpeciesBatchTreeHelperSupport getBatchTreeHelper() {
        return batchTreeHelper;
    }

    @Override
    protected void validate(BatchPersistenceHelper batchHelper, SampleCategoryModel sampleCategoryModel, BatchContainer<SpeciesBatch> result) {
        batchHelper.validateBenthos(sampleCategoryModel, result);
    }

    @Override
    public BatchContainer<SpeciesBatch> getRootBenthosBatch(Integer fishingOperationId, boolean validateTree) {

        return getRootSpeciesBatch0(fishingOperationId, validateTree);

//        Preconditions.checkNotNull(fishingOperationId);
//
//        DateFormat df = new SimpleDateFormat("dd/MM/yyy");
//
//        CatchBatch catchBatch = batchHelper.getRootCatchBatchByFishingOperationId(fishingOperationId, false);
//
//        // -- Vrac > Benthos > Alive Itemized
//        SortingBatch vracBenthosBatch = batchTreeHelper.getBenthosVracAliveItemizedRootBatch(catchBatch);
//
//        SampleCategoryModel sampleCategoryModel = getSampleCategoryModel();
//
//        BatchContainer<SpeciesBatch> result = new BatchContainer<>();
//        if (vracBenthosBatch != null) {
//            result.setId(vracBenthosBatch.getId());
//
//            for (Batch batch : vracBenthosBatch.getChildBatchs()) {
//                SortingBatch source = (SortingBatch) batch;
//                ReferenceTaxon referenceTaxon = source.getReferenceTaxon();
//                Preconditions.checkNotNull(referenceTaxon, "Can't have a rootBenthosBatch with a null taxon, but was for " + batch.getId());
//                Species species = speciesService.getSpeciesByReferenceTaxonId(referenceTaxon.getId());
//                if (species == null) {
//                    FishingOperation fishingOperation = fishingOperationPersistenceService.getFishingOperation(fishingOperationId);
//                    String fishingOperationName = fishingOperation.getStationNumber() + " - " + fishingOperation.getFishingOperationNumber() + " - " + df.format(fishingOperation.getGearShootingStartDate());
//                    throw new InvalidBatchModelException(t("tutti.persistence.benthosBatch.validation.unkonwn.taxon", fishingOperationName, source.getId(), referenceTaxon.getId()));
//                }
//                SpeciesBatch target = SpeciesBatchs.newBenthosBatch();
//                target.setSpecies(species);
//
//                batchHelper.entityToBean(sampleCategoryModel, source, target);
//                result.addChildren(target);
//                if (log.isDebugEnabled()) {
//                    log.debug("Loaded CatchBatch Vrac > Benthos > Alive Itemized > " + target.getSpecies().getReferenceTaxonId() + ": " + target.getId());
//                }
//            }
//        }
//
//        // -- Hors Vrac > Benthos
//        SortingBatch horsVracBenthosBatch = batchTreeHelper.getBenthosHorsVracRootBatch(catchBatch);
//
//        if (horsVracBenthosBatch != null) {
//            for (Batch batch : horsVracBenthosBatch.getChildBatchs()) {
//                SortingBatch source = (SortingBatch) batch;
//                ReferenceTaxon referenceTaxon = source.getReferenceTaxon();
//                Preconditions.checkNotNull(referenceTaxon, "Can't have a rootSpeciesBatch with a null taxon, but was for " + source.getId());
//                Species species = speciesService.getSpeciesByReferenceTaxonId(referenceTaxon.getId());
//                if (species == null) {
//                    FishingOperation fishingOperation = fishingOperationPersistenceService.getFishingOperation(fishingOperationId);
//                    String fishingOperationName = fishingOperation.getStationNumber() + " - " + fishingOperation.getFishingOperationNumber() + " - " + df.format(fishingOperation.getGearShootingStartDate());
//                    throw new InvalidBatchModelException(t("tutti.persistence.benthosBatch.validation.unkonwn.taxon", fishingOperationName, source.getId(), referenceTaxon.getId()));
//                }
//
//                SpeciesBatch target = SpeciesBatchs.newBenthosBatch();
//                target.setSpecies(species);
//
//                batchHelper.entityToBean(sampleCategoryModel, source, target);
//                result.addChildren(target);
//                if (log.isDebugEnabled()) {
//                    log.debug("Loaded CatchBatch Hors Vrac > Benthos > " + target.getSpecies().getReferenceTaxonId() + ": " + target.getId());
//                }
//            }
//        }
//
//        if (validateTree) {
//
//            // validate with given sample category model
//            batchHelper.validateBenthos(sampleCategoryModel, result);
//        }
//
//        return result;
    }

    @Override
    public SpeciesBatch createBenthosBatch(SpeciesBatch bean, Integer parentBatchId, boolean computeRankOrder) {

        return createSpeciesBatch0(bean, parentBatchId, computeRankOrder);

//        Preconditions.checkNotNull(bean);
//        Preconditions.checkArgument(TuttiEntities.isNew(bean));
//        Preconditions.checkNotNull(bean.getSpecies());
//        Preconditions.checkNotNull(bean.getSpecies().getId());
//        Preconditions.checkNotNull(bean.getFishingOperation());
//        Preconditions.checkNotNull(bean.getFishingOperation().getId());
//
//        CatchBatch catchBatch = batchHelper.getRootCatchBatchByFishingOperationId(bean.getFishingOperation().getIdAsInt(), false);
//
//        return createBenthosBatch(bean, parentBatchId, catchBatch, computeRankOrder);
    }

    @Override
    public Collection<SpeciesBatch> createBenthosBatches(Integer fishingOperationId, Collection<SpeciesBatch> beans) {

        return createSpeciesBatches0(fishingOperationId, beans);

//        Preconditions.checkNotNull(beans);
//        Preconditions.checkNotNull(fishingOperationId);
//
//        CatchBatch catchBatch = batchHelper.getRootCatchBatchByFishingOperationId(fishingOperationId, false);
//
//        Collection<SpeciesBatch> result = new ArrayList<>();
//        for (SpeciesBatch bean : beans) {
//
//            SpeciesBatch created = createBenthosBatch(bean, null, catchBatch, true);
//            result.add(created);
//
//        }
//        return result;

    }

//    protected SpeciesBatch createBenthosBatch(SpeciesBatch bean, Integer parentBatchId, CatchBatch catchBatch, boolean computeRankOrder) {
//
//        Preconditions.checkNotNull(bean);
//        Preconditions.checkArgument(TuttiEntities.isNew(bean));
//        Preconditions.checkNotNull(bean.getSpecies());
//        Preconditions.checkNotNull(bean.getSpecies().getId());
//        Preconditions.checkNotNull(bean.getFishingOperation());
//        Preconditions.checkNotNull(bean.getFishingOperation().getId());
//
//        SortingBatch batch = SortingBatch.Factory.newInstance();
//        beanToEntity(bean, batch, parentBatchId, catchBatch, computeRankOrder);
//        bean = batchHelper.createSortingBatch(bean, catchBatch, batch);
//
//        return bean;
//    }

    @Override
    public SpeciesBatch saveBenthosBatch(SpeciesBatch bean) {

        return saveSpeciesBatch0(bean);

//        Preconditions.checkNotNull(bean);
//        Preconditions.checkNotNull(bean.getId());
//
//        Integer batchId = bean.getIdAsInt();
//        CatchBatch catchBatch = batchHelper.getRootCatchBatchByBatchId(batchId);
//        SortingBatch batch = batchHelper.getSortingBatchById(catchBatch, batchId);
//
//        Integer parentBatchId = null;
//        if (bean.getParentBatch() != null) {
//            parentBatchId = bean.getParentBatch().getIdAsInt();
//        }
//        beanToEntity(bean, batch, parentBatchId, catchBatch, true);
//        batchHelper.updateSortingBatch(batch, catchBatch);
//
//        return bean;
    }

    @Override
    public void deleteBenthosBatch(Integer id) {
        deleteSpeciesBatch0(id);
//        Preconditions.checkNotNull(id);
//        batchHelper.deleteBatch(id);
    }

    @Override
    public void deleteBenthosSubBatch(Integer id) {
        deleteSpeciesSubBatch0(id);
//        Preconditions.checkNotNull(id);
//        batchHelper.deleteSpeciesSubBatch(id);
    }

    @Override
    public void changeBenthosBatchSpecies(Integer id, Species species) {
        changeSpeciesBatchSpecies0(id, species);
//        Preconditions.checkNotNull(id);
//        Preconditions.checkNotNull(species);
//        Preconditions.checkNotNull(species.getReferenceTaxonId());
//        batchHelper.changeBatchSpecies(id, species);
    }

    @Override
    public List<SpeciesBatch> getAllBenthosBatchToConfirm(Integer fishingOperationId) throws InvalidBatchModelException {
        return getAllSpeciesBatchToConfirm0(fishingOperationId);
//        List<SpeciesBatch> batchesToConfirm = new ArrayList<>();
//
//        BatchContainer<SpeciesBatch> rootBenthosBatch = getRootBenthosBatch(fishingOperationId, false);
//        for (SpeciesBatch benthosBatch : rootBenthosBatch.getChildren()) {
//            findBenthosBatchesToConfirm(benthosBatch, batchesToConfirm);
//        }
//
//        return batchesToConfirm;
    }

//    protected void findBenthosBatchesToConfirm(SpeciesBatch benthosBatch, List<SpeciesBatch> batchesToConfirm) {
//        if (benthosBatch.isSpeciesToConfirm()) {
//            batchesToConfirm.add(benthosBatch);
//
//        } else if (!benthosBatch.isChildBatchsEmpty()) {
//            for (SpeciesBatch batch : benthosBatch.getChildBatchs()) {
//                findBenthosBatchesToConfirm(batch, batchesToConfirm);
//            }
//        }
//    }

    //------------------------------------------------------------------------//
    //-- SpeciesBatchFrequency methods (for benthos)                        --//
    //------------------------------------------------------------------------//

    @Override
    public List<SpeciesBatchFrequency> getAllBenthosBatchFrequency(Integer benthosBatchId) {
        return getAllSpeciesBatchFrequency0(benthosBatchId);
//        Preconditions.checkNotNull(benthosBatchId);
//
//        List<SortingBatch> frequencyChilds = batchHelper.getFrequencies(
//                benthosBatchId);
//        List<SpeciesBatchFrequency> results = Lists.newArrayList();
//        for (SortingBatch child : frequencyChilds) {
//            SpeciesBatchFrequency target = SpeciesBatchFrequencys.newBenthosBatchFrequency();
//
//            batchHelper.entityToBatchFrequency(child, target);
//            results.add(target);
//        }
//        return Collections.unmodifiableList(results);
    }

    @Override
    public Multimap<Species, SpeciesBatchFrequency> getAllBenthosBatchFrequencyForBatch(BatchContainer<SpeciesBatch> batchContainer) {
        return getAllSpeciesBatchFrequencyForBatch0(batchContainer);
//        Multimap<Species, SpeciesBatchFrequency> result = ArrayListMultimap.create();
//        for (SpeciesBatch speciesBatch : batchContainer.getChildren()) {
//            getAllSpeciesBatchFrequencyForBatch(speciesBatch, result);
//        }
//        return result;
    }

//    protected void getAllSpeciesBatchFrequencyForBatch(SpeciesBatch batch,
//                                                       Multimap<Species, SpeciesBatchFrequency> result) {
//
//        List<SpeciesBatchFrequency> speciesBatchFrequency = getAllBenthosBatchFrequency(batch.getIdAsInt());
//        result.putAll(batch.getSpecies(), speciesBatchFrequency);
//
//        if (!batch.isChildBatchsEmpty()) {
//            for (SpeciesBatch child : batch.getChildBatchs()) {
//                getAllSpeciesBatchFrequencyForBatch(child, result);
//            }
//        }
//    }

    @Override
    public List<SpeciesBatchFrequency> saveBenthosBatchFrequency(Integer benthosBatchId,
                                                                 List<SpeciesBatchFrequency> frequencies) {
        return saveSpeciesBatchFrequency0(benthosBatchId, frequencies);
//        Preconditions.checkNotNull(benthosBatchId);
//        Preconditions.checkNotNull(frequencies);
//
//        // Check that all frequencies have the same length PMFM (before doing any db call)
//        String pmfmId = null;
//        for (SpeciesBatchFrequency source : frequencies) {
//
//            if (pmfmId == null) {
//                pmfmId = source.getLengthStepCaracteristic().getId();
//            } else if (!pmfmId.equals(source.getLengthStepCaracteristic().getId())) {
//                throw new DataIntegrityViolationException("Batch frequencies under one Speciesbatch must have all the same lengthStepCaracteristic");
//            }
//        }
//
//        CatchBatch catchBatch = batchHelper.getRootCatchBatchByBatchId(benthosBatchId);
//
//        if (catchBatch == null) {
//            return frequencies;
//        }
//
//        // Synchronization status
//        synchronizationStatusHelper.setDirty(catchBatch);
//
//        // Retrieve parent
//        SortingBatch parentBatch = batchHelper.getSortingBatchById(catchBatch, benthosBatchId);
//
//        // Remember child ids, to remove unchanged item (see at bottom in this method)
//        List<Integer> notUpdatedChildIds = Lists.newArrayList();
//        List<SortingBatch> frequencyChilds = batchHelper.getFrequencyChilds(
//                parentBatch);
//        for (SortingBatch child : frequencyChilds) {
//            notUpdatedChildIds.add(child.getId());
//        }
//
//        short rankOrder = 0;
//        List<SortingBatch> batchsToUpdate = Lists.newArrayList();
//        for (SpeciesBatchFrequency source : frequencies) {
//            rankOrder++;
//
//            // Not existing batch
//            SortingBatch target;
//            if (source.getId() == null) {
//                target = SortingBatch.Factory.newInstance();
//
//                // Fill the sorting batch from the source
//                batchHelper.beanToEntity(source, target, parentBatch, rankOrder);
//
//                // Create the targeted batch, then update the source id
//                batchHelper.createSortingBatch(source, catchBatch, target);
//            }
//
//            // Existing batch
//            else {
//                target = batchHelper.getSortingBatchById(catchBatch, source.getIdAsInt());
//
//                // Fill the sorting batch from the source
//                batchHelper.beanToEntity(source, target, parentBatch, rankOrder);
//
//                // Add the batch into a list (will be update later, using this list)
//                batchsToUpdate.add(target);
//
//                notUpdatedChildIds.remove(target.getId());
//            }
//        }
//
//        // If some batchs need to be update, do it
//        if (CollectionUtils.isNotEmpty(batchsToUpdate)) {
//            batchHelper.updateSortingBatch(batchsToUpdate, catchBatch);
//        }
//
//        if (CollectionUtils.isNotEmpty(notUpdatedChildIds)) {
//            for (Integer batchId : notUpdatedChildIds) {
//                batchHelper.removeWithChildren(batchId, catchBatch);
//            }
//        }
//
//        return Collections.unmodifiableList(frequencies);
    }

    //------------------------------------------------------------------------//
    //-- Internal methods                                                   --//
    //------------------------------------------------------------------------//

//    protected void beanToEntity(SpeciesBatch source,
//                                SortingBatch target,
//                                Integer parentBatchId,
//                                CatchBatch catchBatch,
//                                boolean computeRankOrder) {
//
//        Preconditions.checkNotNull(source.getFishingOperation());
//        Preconditions.checkNotNull(source.getFishingOperation().getId());
//
//        // If parent and root need to be set
//        if (target.getId() == null
//                || target.getRootBatch() == null
//                || (target.getParentBatch() != null && !target.getParentBatch().getId().equals(parentBatchId))) {
//            batchHelper.setBenthosBatchParents(
//                    source.getSampleCategoryId(),
//                    source.getSampleCategoryValue(),
//                    target,
//                    parentBatchId,
//                    catchBatch);
//        }
//
//        batchHelper.beanToEntity(parentBatchId, source, target, computeRankOrder);
//    }

}

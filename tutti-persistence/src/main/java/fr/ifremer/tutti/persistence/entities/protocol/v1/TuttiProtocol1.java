package fr.ifremer.tutti.persistence.entities.protocol.v1;

/*
 * #%L
 * Tutti :: Persistence
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.entities.CommentAware;
import fr.ifremer.tutti.persistence.entities.TuttiEntity;

import java.util.Collection;
import java.util.List;

/**
 * To migrate old protocol to last version.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.4
 */
public interface TuttiProtocol1 extends CommentAware, TuttiEntity {

    String PROPERTY_NAME = "name";

    String PROPERTY_COMMENT = "comment";

    String PROPERTY_GEAR_USE_FEATURE_PMFM_ID = "gearUseFeaturePmfmId";

    String PROPERTY_VESSEL_USE_FEATURE_PMFM_ID = "vesselUseFeaturePmfmId";

    String PROPERTY_LENGTH_CLASSES_PMFM_ID = "lengthClassesPmfmId";

    String PROPERTY_SPECIES = "species";

    String PROPERTY_BENTHOS = "benthos";

    String getName();

    void setName(String name);

    String getComment();

    void setComment(String comment);

    String getGearUseFeaturePmfmId(int index);

    boolean isGearUseFeaturePmfmIdEmpty();

    int sizeGearUseFeaturePmfmId();

    void addGearUseFeaturePmfmId(String gearUseFeaturePmfmId);

    void addAllGearUseFeaturePmfmId(Collection<String> gearUseFeaturePmfmId);

    boolean removeGearUseFeaturePmfmId(String gearUseFeaturePmfmId);

    boolean removeAllGearUseFeaturePmfmId(Collection<String> gearUseFeaturePmfmId);

    boolean containsGearUseFeaturePmfmId(String gearUseFeaturePmfmId);

    boolean containsAllGearUseFeaturePmfmId(Collection<String> gearUseFeaturePmfmId);

    List<String> getGearUseFeaturePmfmId();

    void setGearUseFeaturePmfmId(List<String> gearUseFeaturePmfmId);

    String getVesselUseFeaturePmfmId(int index);

    boolean isVesselUseFeaturePmfmIdEmpty();

    int sizeVesselUseFeaturePmfmId();

    void addVesselUseFeaturePmfmId(String vesselUseFeaturePmfmId);

    void addAllVesselUseFeaturePmfmId(Collection<String> vesselUseFeaturePmfmId);

    boolean removeVesselUseFeaturePmfmId(String vesselUseFeaturePmfmId);

    boolean removeAllVesselUseFeaturePmfmId(Collection<String> vesselUseFeaturePmfmId);

    boolean containsVesselUseFeaturePmfmId(String vesselUseFeaturePmfmId);

    boolean containsAllVesselUseFeaturePmfmId(Collection<String> vesselUseFeaturePmfmId);

    List<String> getVesselUseFeaturePmfmId();

    void setVesselUseFeaturePmfmId(List<String> vesselUseFeaturePmfmId);

    String getLengthClassesPmfmId(int index);

    boolean isLengthClassesPmfmIdEmpty();

    int sizeLengthClassesPmfmId();

    void addLengthClassesPmfmId(String lengthClassesPmfmId);

    void addAllLengthClassesPmfmId(Collection<String> lengthClassesPmfmId);

    boolean removeLengthClassesPmfmId(String lengthClassesPmfmId);

    boolean removeAllLengthClassesPmfmId(Collection<String> lengthClassesPmfmId);

    boolean containsLengthClassesPmfmId(String lengthClassesPmfmId);

    boolean containsAllLengthClassesPmfmId(Collection<String> lengthClassesPmfmId);

    List<String> getLengthClassesPmfmId();

    void setLengthClassesPmfmId(List<String> lengthClassesPmfmId);

    SpeciesProtocol1 getSpecies(int index);

    boolean isSpeciesEmpty();

    int sizeSpecies();

    void addSpecies(SpeciesProtocol1 species);

    void addAllSpecies(Collection<SpeciesProtocol1> species);

    boolean removeSpecies(SpeciesProtocol1 species);

    boolean removeAllSpecies(Collection<SpeciesProtocol1> species);

    boolean containsSpecies(SpeciesProtocol1 species);

    boolean containsAllSpecies(Collection<SpeciesProtocol1> species);

    List<SpeciesProtocol1> getSpecies();

    void setSpecies(List<SpeciesProtocol1> species);

    SpeciesProtocol1 getBenthos(int index);

    boolean isBenthosEmpty();

    int sizeBenthos();

    void addBenthos(SpeciesProtocol1 benthos);

    void addAllBenthos(Collection<SpeciesProtocol1> benthos);

    boolean removeBenthos(SpeciesProtocol1 benthos);

    boolean removeAllBenthos(Collection<SpeciesProtocol1> benthos);

    boolean containsBenthos(SpeciesProtocol1 benthos);

    boolean containsAllBenthos(Collection<SpeciesProtocol1> benthos);

    List<SpeciesProtocol1> getBenthos();

    void setBenthos(List<SpeciesProtocol1> benthos);

}

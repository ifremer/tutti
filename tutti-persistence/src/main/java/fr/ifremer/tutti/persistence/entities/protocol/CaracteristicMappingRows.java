package fr.ifremer.tutti.persistence.entities.protocol;

/*
 * #%L
 * Tutti :: Persistence
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2016 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Function;
import com.google.common.base.Predicate;

public class CaracteristicMappingRows extends AbstractCaracteristicMappingRows {

    public static final Function<CaracteristicMappingRow, String> GET_PMFM_ID = CaracteristicMappingRow::getPmfmId;

    public static class TabPredicate implements Predicate<CaracteristicMappingRow> {
        private final CaracteristicType type;

        public TabPredicate(CaracteristicType type) {
            this.type = type;
        }

        @Override
        public boolean apply(CaracteristicMappingRow caracteristicMappingRow) {
            return type.name().equals(caracteristicMappingRow.getTab());
        }
    }
}

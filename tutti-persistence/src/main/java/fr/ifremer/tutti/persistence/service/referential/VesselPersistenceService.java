package fr.ifremer.tutti.persistence.service.referential;

/*
 * #%L
 * Tutti :: Persistence
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.TuttiPersistenceServiceImplementor;
import fr.ifremer.tutti.persistence.entities.data.Cruise;
import fr.ifremer.tutti.persistence.entities.referential.Vessel;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.List;

/**
 * Created on 11/3/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.8
 */
@Transactional(readOnly = true)
public interface VesselPersistenceService extends TuttiPersistenceServiceImplementor {

    /**
     * @return all scientific vessels (used by a {@link Cruise}).
     * @see Cruise#getVessel()
     * @see Cruise#setVessel(Vessel)
     * @since 0.3
     */
    List<Vessel> getAllScientificVessel();

    /**
     * @return all commercial vessels (used by a {@link Cruise}).
     * @see Cruise#getVessel()
     * @see Cruise#setVessel(Vessel)
     * @since 0.3
     */
    @Cacheable(value = "fishingVessels")
    List<Vessel> getAllFishingVessel();

    @Cacheable(value = "vesselsWithObsoletes")
    List<Vessel> getAllVesselWithObsoletes();

    /**
     * @param vesselCode code of the vessel to find
     * @return the vessel
     * @since 0.3
     */
    @Cacheable(value = "vesselByCode", key = "#vesselCode")
    Vessel getVessel(String vesselCode);

    /**
     * Check if the temporary vessel with the given {@code id} is used.
     *
     * @param code code of the vessel to remove
     * @return {@code true} if vessel is temporary
     * @since 3.8
     */
    boolean isTemporaryVesselUsed(String code);

    /**
     * Add temporary vessels.
     *
     * @param vessels vessels to add
     * @return added vessels
     * @since 3.14
     */
    @Transactional(readOnly = false)
    @CacheEvict(value = {"fishingVessels", "vesselByCode", "vesselsWithObsoletes"}, allEntries = true)
    List<Vessel> addTemporaryVessels(List<Vessel> vessels);

    /**
     * Update temporary vessels.
     *
     * @param vessels vessels to update
     * @return updated vessels
     * @since 3.14
     */
    @Transactional(readOnly = false)
    @CacheEvict(value = {"fishingVessels", "vesselByCode", "vesselsWithObsoletes"}, allEntries = true)
    List<Vessel> updateTemporaryVessels(List<Vessel> vessels);

    /**
     * Link temporary vessels. (Means get existing references using vessels natural ids).
     *
     * @param vessels vessels to link
     * @return linked vessels
     * @since 3.14
     */
    List<Vessel> linkTemporaryVessels(List<Vessel> vessels);

    /**
     * Replace the {@code source} vessel by
     * the {@code target} one in all data.
     *
     * @param source the source vessel to replace
     * @param target the target vessel to use
     * @param delete flag to delete the temporary vessel after replacement
     * @since 3.6
     */
    @Transactional(readOnly = false)
    @CacheEvict(value = {"fr.ifremer.adagio.core.dao.data.batch.CatchBatchCache", "fishingVessels", "vesselByCode", "vesselsWithObsoletes"},
            allEntries = true)
    void replaceVessel(Vessel source, Vessel target, boolean delete);

    /**
     * Delete the temporary vessels with the given {@code codes}.
     *
     * @param codes code of the vessels to remove
     * @since 3.8
     */
    @Transactional(readOnly = false)
    @CacheEvict(value = {"fishingVessels", "vesselByCode", "vesselsWithObsoletes"}, allEntries = true)
    void deleteTemporaryVessels(Collection<String> codes);

    /**
     * Delete the temporary vessel with the given {@code code}.
     *
     * @param code code of the vessel to remove
     * @since 3.8
     */
    @Transactional(readOnly = false)
    @CacheEvict(value = {"fishingVessels", "vesselByCode", "vesselsWithObsoletes"}, allEntries = true)
    void deleteTemporaryVessel(String code);

}

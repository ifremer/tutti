package fr.ifremer.tutti.persistence.entities.data;

/*
 * #%L
 * Tutti :: Persistence
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.apache.commons.lang3.builder.EqualsBuilder;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;

public class FishingOperations extends AbstractFishingOperations {

    public static final String PROPERTY_GEAR_SHOOTING_START_DAY = "gearShootingStartDay";

    public static final String PROPERTY_GEAR_SHOOTING_START_TIME = "gearShootingStartTime";

    public static final String PROPERTY_GEAR_SHOOTING_END_DAY = "gearShootingEndDay";

    public static final String PROPERTY_GEAR_SHOOTING_END_TIME = "gearShootingEndTime";

    public static boolean equals(FishingOperation fishingOperation1, FishingOperation fishingOperation2) {

        EqualsBuilder equalsBuilder = new EqualsBuilder();

//        Date day1 = DateUtil.getDay(fishingOperation1.getGearShootingStartDate());
//        Date day2 = DateUtil.getDay(fishingOperation2.getGearShootingStartDate());
//
//        equalsBuilder.append(day1, day2);
        equalsBuilder.append(fishingOperation1.getStationNumber(), fishingOperation2.getStationNumber());
        equalsBuilder.append(fishingOperation1.getFishingOperationNumber(), fishingOperation2.getFishingOperationNumber());
        equalsBuilder.append(fishingOperation1.getMultirigAggregation(), fishingOperation2.getMultirigAggregation());

        return equalsBuilder.isEquals();

    }

    public static boolean equalsNaturalId(FishingOperation fishingOperation1, String naturalId2) {

        String naturalId1 = getNaturalId(fishingOperation1);
        return Objects.equals(naturalId1, naturalId2);

    }

    public static String getNaturalId(FishingOperation fishingOperation) {

        String cruiseId = Cruises.getNaturalId(fishingOperation.getCruise());
        return cruiseId + "--" + fishingOperation.getStationNumber() + "--" + fishingOperation.getFishingOperationNumber() + "--" + fishingOperation.getMultirigAggregation();

    }

    public static void sort(List<FishingOperation> cruises) {
        Collections.sort(cruises, FISHING_OPERATION_COMPARATOR);
    }

    public static Comparator<FishingOperation> FISHING_OPERATION_COMPARATOR = new Comparator<FishingOperation>() {
        @Override
        public int compare(FishingOperation o1, FishingOperation o2) {

            int result = compare(o1.getGearShootingStartDate(), o2.getGearShootingStartDate());
            if (result == 0) {
                result = compare(o1.getStationNumber(), o2.getStationNumber());
            }
            if (result == 0) {
                result = compare(o1.getFishingOperationNumber(), o2.getFishingOperationNumber());
            }

            return result;
        }

        private <C extends Comparable<C>> int compare(C o1, C o2) {
            int result;
            if (Objects.equals(o1, o2)) {
                result = 0;
            } else if (o1 == null) {
                result = -1;
            } else if (o2 == null) {
                result = 1;
            } else {
                result = o1.compareTo(o2);
            }

            return result;
        }
    };

}

package fr.ifremer.tutti.persistence.service.referential;

/*
 * #%L
 * Tutti :: Persistence
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Predicate;
import fr.ifremer.tutti.persistence.TuttiPersistenceServiceImplementor;
import fr.ifremer.tutti.persistence.entities.data.SampleCategoryModel;
import fr.ifremer.tutti.persistence.entities.data.SpeciesBatch;
import fr.ifremer.tutti.persistence.entities.referential.Caracteristic;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created on 11/3/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.8
 */
@Transactional(readOnly = true)
public interface CaracteristicPersistenceService extends TuttiPersistenceServiceImplementor {

    /**
     * @return all caracteristics of the system.
     * @since 1.0
     */
    @Cacheable(value = "pmfms")
    List<Caracteristic> getAllCaracteristic();

    /**
     * @return all caracteristics of the system with the ones which are kind of protected.
     * @since 2.3
     */
    @Cacheable(value = "pmfmsWithProtected")
    List<Caracteristic> getAllCaracteristicWithProtected();

    /**
     * @return all caracteristics of the system useable for {@link SampleCategoryModel}.
     * @since 2.4
     */
    List<Caracteristic> getAllCaracteristicForSampleCategory();

    /**
     * @return all numeric caracteristics of the system.
     * @since 1.0.2
     */
    List<Caracteristic> getAllNumericCaracteristic();

    Caracteristic getSizeCategoryCaracteristic();

    Caracteristic getSexCaracteristic();

    Caracteristic getSortedUnsortedCaracteristic();

    Caracteristic getMaturityCaracteristic();

    Caracteristic getAgeCaracteristic();

    Caracteristic getMarineLitterCategoryCaracteristic();

    Caracteristic getMarineLitterSizeCategoryCaracteristic();

    Caracteristic getVerticalOpeningCaracteristic();

    Caracteristic getHorizontalOpeningWingsCaracteristic();

    Caracteristic getHorizontalOpeningDoorCaracteristic();

    Caracteristic getDeadOrAliveCaracteristic();

    Caracteristic getCalcifiedStructureCaracteristic();

    Caracteristic getPmfmIdCaracteristic();

    Caracteristic getWeightMeasuredCaracteristic();

    Caracteristic getCopyIndividualObservationModeCaracteristic();

    Caracteristic getSampleCodeCaracteristic();

    @Cacheable(value = "pmfmById", key = "#pmfmId")
    Caracteristic getCaracteristic(Integer pmfmId);

    Predicate<SpeciesBatch> getVracBatchPredicate();

    boolean isVracBatch(SpeciesBatch speciesBatch);

    boolean isHorsVracBatch(SpeciesBatch speciesBatch);

}

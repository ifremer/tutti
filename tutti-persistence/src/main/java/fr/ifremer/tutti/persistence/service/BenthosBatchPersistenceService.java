package fr.ifremer.tutti.persistence.service;

/*
 * #%L
 * Tutti :: Persistence
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Multimap;
import fr.ifremer.tutti.persistence.InvalidBatchModelException;
import fr.ifremer.tutti.persistence.TuttiPersistenceServiceImplementor;
import fr.ifremer.tutti.persistence.entities.data.BatchContainer;
import fr.ifremer.tutti.persistence.entities.data.SpeciesBatch;
import fr.ifremer.tutti.persistence.entities.data.SpeciesBatchFrequency;
import fr.ifremer.tutti.persistence.entities.referential.Species;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.List;
import java.util.Set;

/**
 * Service to persist {@link SpeciesBatch}.
 *
 * FIXME Merge it with speciesBatch.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.2
 */
@Transactional(readOnly = true)
public interface BenthosBatchPersistenceService extends TuttiPersistenceServiceImplementor {

    //------------------------------------------------------------------------//
    //-- BenthosBatch methods                                               --//
    //------------------------------------------------------------------------//

    /**
     * Get all root {@link SpeciesBatch} for the given fishing operation.
     *
     * <strong>Note:</strong> All childs of the batch should be loaded here.
     *
     * @param fishingOperationId if of the fishing operation to seek
     * @param validateTree       flag to validate sample category model
     * @return the list of root {@link SpeciesBatch}
     * @throws InvalidBatchModelException if batch does not respect the sample category model
     * @since 1.0
     */
    BatchContainer<SpeciesBatch> getRootBenthosBatch(Integer fishingOperationId, boolean validateTree) throws InvalidBatchModelException;

    /**
     * Récupérer les identifiants de tous les lots de la descendance du lot donné.
     *
     * @param id identifiant du lot parent
     * @return l'ensemble des identifiants de toute la descendance d'un lot
     */
    Set<Integer> getBatchChildIds(Integer id);

    @Transactional(readOnly = false)
    SpeciesBatch createBenthosBatch(SpeciesBatch bean, Integer parentBatchId, boolean computeRankOrder);

    @Transactional(readOnly = false)
    Collection<SpeciesBatch> createBenthosBatches(Integer fishingOperationId, Collection<SpeciesBatch> beans);

    @Transactional(readOnly = false)
    SpeciesBatch saveBenthosBatch(SpeciesBatch bean);

    @Transactional(readOnly = false)
    void deleteBenthosBatch(Integer id);

    @Transactional(readOnly = false)
    void deleteBenthosSubBatch(Integer id);

    /**
     * Change the species in the given {@code batchId} and in all his
     * sub batches.
     *
     * @param batchId id of the root Benthos batch to treat
     * @param species species to affect to all batches
     */
    @Transactional(readOnly = false)
    void changeBenthosBatchSpecies(Integer batchId, Species species);

    /**
     * Get all the batches to confirm
     *
     * @param fishingOperationId if of the fishing operation to seek
     * @return the list of {@link fr.ifremer.tutti.persistence.entities.data.SpeciesBatch} to confirm
     * @throws InvalidBatchModelException if batch does not respect the sample category model
     * @since 3.13
     */
    List<SpeciesBatch> getAllBenthosBatchToConfirm(Integer fishingOperationId) throws InvalidBatchModelException;

    //------------------------------------------------------------------------//
    //-- BenthosBatchFrequency methods                                      --//
    //------------------------------------------------------------------------//

    /**
     * Get all frequencies for the given species batch.
     *
     * @param benthosBatchId the id of the species batch to seek.
     * @return the list of frequencies for the given benthos batch id
     * @since 1.0
     */
    List<SpeciesBatchFrequency> getAllBenthosBatchFrequency(Integer benthosBatchId);

    /**
     * Get all frequencies for the given root benthos batch container.
     *
     * @param batchContainer the root batch containter
     * @return the list of benthos frequencies indexed by their species
     * @since 3.3
     */
    Multimap<Species, SpeciesBatchFrequency> getAllBenthosBatchFrequencyForBatch(BatchContainer<SpeciesBatch> batchContainer);

    /**
     * Save all given {@link SpeciesBatchFrequency} into the given
     * {@code benthosBatchId}. If some are not existing then creates them.
     *
     * <strong>Note:</strong> This will as a side effect remove all previous
     * frequency for this benthos batch.
     *
     * @param benthosBatchId id of the {@link SpeciesBatch} to use
     * @param frequencies    list of frequencies to create or update
     * @return the persisted list of frequencies
     * @since 1.0
     */
    @Transactional(readOnly = false)
    List<SpeciesBatchFrequency> saveBenthosBatchFrequency(Integer benthosBatchId,
                                                          List<SpeciesBatchFrequency> frequencies);
}

package fr.ifremer.tutti.persistence.entities;

/*
 * #%L
 * Tutti :: Persistence
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * Usefull method around tutti entities.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 0.1
 */
public class TuttiEntities {

    protected TuttiEntities() {
        // helper class does not instanciate
    }

//    public static <B extends TuttiEntity> List<String> toIds(Collection<B> list) {
//        return list == null ?
//                              Collections.<String>emptyList() :
//                              Lists.transform(list instanceof List? (List<B>)list: new ArrayList<>(list), GET_ID);
//    }

    public static <B extends TuttiEntity> List<Integer> toIntegerIds(Collection<B> list) {
        return list == null ?
                               Collections.<Integer>emptyList() :
                               Lists.transform(Lists.newArrayList(list), GET_ID_AS_INT);
    }

    public static <B extends TuttiEntity> Map<String, B> splitById(Iterable<B> list) {
        return Maps.uniqueIndex(list, GET_ID);
    }

    public static <B extends TuttiEntity> Map<Integer, B> splitByIdAsInt(Iterable<B> list) {
        return Maps.uniqueIndex(list, GET_ID_AS_INT);
    }

    public static <B extends TuttiEntity> boolean isNew(B bean) {
        return bean.getId() == null;
    }

    public static final Function<TuttiEntity, String> GET_ID = TuttiEntity::getId;

    public static <E extends TuttiEntity> Function<E, String> newIdFunction() {
        return (Function<E, String>) GET_ID;
    }

    public static <E extends TuttiEntity> Function<E, Integer> newIdAstIntFunction() {
        return (Function<E, Integer>) GET_ID_AS_INT;
    }

    public static final Function<TuttiEntity, Integer> GET_ID_AS_INT = TuttiEntity::getIdAsInt;

    public static <B extends TuttiEntity> Predicate<B> newIdPredicate(String id) {
        return new IdPredicate<>(id);
    }

    public static <B extends TuttiEntity> B findById(Iterable<B> beans, String id) {
        return Iterables.tryFind(beans, newIdPredicate(id)).orNull();
    }

    public static <B extends TuttiEntity> List<String> collecIds(List<B> list) {
        return Lists.transform(list, GET_ID);
    }

    protected static class IdPredicate<B extends TuttiEntity> implements Predicate<B> {

        private final String id;

        public IdPredicate(String id) {
            this.id = id;
        }

        @Override
        public boolean apply(B input) {
            return id.equals(input.getId());
        }
    }

}



package fr.ifremer.tutti.persistence.model;

/*
 * #%L
 * Tutti :: Persistence
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ImmutableSet;
import fr.ifremer.tutti.persistence.entities.data.Program;

import java.util.Iterator;
import java.util.Set;

/**
 * Created on 3/29/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.14.3
 */
public class ProgramDataModel extends DataModelSupport implements Iterable<CruiseDataModel> {

    private static final long serialVersionUID = 1L;

    private final Set<CruiseDataModel> cruises;

    public ProgramDataModel(Program program, Set<CruiseDataModel> cruises) {
        this(program.getId(), program.getName(), cruises);
    }

    public ProgramDataModel(String id, String label, Set<CruiseDataModel> cruises) {
        super(id, label);
        this.cruises = ImmutableSet.copyOf(cruises);
    }

    public int size() {
        return cruises.size();
    }

    public int getNbOperations() {
        int result = 0;
        for (CruiseDataModel cruise : this) {
            result += cruise.size();
        }
        return result;
    }

    @Override
    public Iterator<CruiseDataModel> iterator() {
        return cruises.iterator();
    }

    public CruiseDataModel getCruise(String id) {

        CruiseDataModel result = null;
        for (CruiseDataModel cruise : this) {

            if (id.equals(cruise.getId())) {
                result = cruise;
                break;
            }
        }
        return result;

    }

}

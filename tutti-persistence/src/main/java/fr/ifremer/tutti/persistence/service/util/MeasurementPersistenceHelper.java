package fr.ifremer.tutti.persistence.service.util;

/*
 * #%L
 * Tutti :: Persistence
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import com.google.common.collect.Sets;
import fr.ifremer.adagio.core.dao.administration.user.DepartmentId;
import fr.ifremer.adagio.core.dao.data.batch.Batch;
import fr.ifremer.adagio.core.dao.data.batch.CatchBatchExtendDao;
import fr.ifremer.adagio.core.dao.data.batch.SortingBatch;
import fr.ifremer.adagio.core.dao.data.measure.GearUseMeasurement;
import fr.ifremer.adagio.core.dao.data.measure.Measurement;
import fr.ifremer.adagio.core.dao.data.measure.QuantificationMeasurement;
import fr.ifremer.adagio.core.dao.data.measure.SortingMeasurement;
import fr.ifremer.adagio.core.dao.data.measure.SurveyMeasurement;
import fr.ifremer.adagio.core.dao.data.measure.VesselUseMeasurement;
import fr.ifremer.adagio.core.dao.data.survey.fishingTrip.FishingTrip;
import fr.ifremer.adagio.core.dao.data.survey.scientificCruise.ScientificCruise;
import fr.ifremer.adagio.core.dao.data.vessel.feature.use.GearUseFeatures;
import fr.ifremer.adagio.core.dao.data.vessel.feature.use.VesselUseFeatures;
import fr.ifremer.adagio.core.dao.referential.QualityFlagCode;
import fr.ifremer.adagio.core.dao.referential.QualityFlagImpl;
import fr.ifremer.adagio.core.dao.referential.pmfm.Pmfm;
import fr.ifremer.adagio.core.dao.referential.pmfm.PmfmId;
import fr.ifremer.adagio.core.dao.referential.pmfm.PmfmImpl;
import fr.ifremer.adagio.core.dao.referential.pmfm.QualitativeValue;
import fr.ifremer.adagio.core.dao.referential.pmfm.QualitativeValueImpl;
import fr.ifremer.tutti.persistence.entities.referential.Caracteristic;
import fr.ifremer.tutti.persistence.entities.referential.CaracteristicQualitativeValue;
import fr.ifremer.tutti.persistence.service.AbstractPersistenceService;
import fr.ifremer.tutti.persistence.service.referential.CaracteristicPersistenceService;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.io.Serializable;
import java.util.Collection;
import java.util.Iterator;
import java.util.Set;

/**
 * Helper around {@link Measurement}.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.0
 */
@Component("measurementPersistenceHelper")
public class MeasurementPersistenceHelper extends AbstractPersistenceService {

    @Resource(name = "caracteristicPersistenceService")
    private CaracteristicPersistenceService caracteristicService;

    @Resource(name = "catchBatchDao")
    protected CatchBatchExtendDao catchBatchDao;

    public void setMeasurement(Measurement measurement,
                               Caracteristic caracteristic,
                               Serializable value) {
        if (value == null) {
            return;
        }
        switch (caracteristic.getCaracteristicType()) {

            case NUMBER:
                measurement.setNumericalValue((Float) value);
                break;
            case QUALITATIVE:
                Integer qvId = null;
                if (value instanceof CaracteristicQualitativeValue) {
                    qvId = Integer.valueOf(((CaracteristicQualitativeValue) value).getId());
                } else if (value instanceof Integer) {
                    qvId = (Integer) value;
                }else if (value instanceof String) {
                    qvId = Integer.valueOf((String)value);
                }
                QualitativeValue qv = load(QualitativeValueImpl.class, qvId);
                measurement.setQualitativeValue(qv);
                break;
            case TEXT:
                measurement.setAlphanumericalValue((String) value);
                break;
        }
    }

    //------------------------------------------------------------------------//
    //-- SortingMeasurement                                                 --//
    //------------------------------------------------------------------------//

    public SortingMeasurement setSortingMeasurement(
            SortingBatch sortingBatch,
            Integer pmfmId,
            Serializable value) {
        Preconditions.checkNotNull(pmfmId);
        Preconditions.checkNotNull(value);

        Caracteristic caracteristic = caracteristicService.getCaracteristic(pmfmId);
        SortingMeasurement sortingMeasurement = catchBatchDao.getSortingMeasurement(
                sortingBatch, pmfmId, getRecorderDepartmentId(), true);
        setMeasurement(sortingMeasurement,
                       caracteristic,
                       value);
        return sortingMeasurement;
    }

    public QuantificationMeasurement setWeightMeasurementQuantificationMeasurement(Batch batch, Float weightValue) {

        Collection<QuantificationMeasurement> quantificationMeasurements = batch.getQuantificationMeasurements();
        Set<QuantificationMeasurement> notChangedQuantificationMeasurements = Sets.newHashSet();
        if (quantificationMeasurements != null) {
            notChangedQuantificationMeasurements.addAll(quantificationMeasurements);
        }

        QuantificationMeasurement quantificationMeasurement = catchBatchDao.setQuantificationMeasurement(batch, PmfmId.WEIGHT_MEASURED.getValue(), getRecorderDepartmentId(), weightValue, true);

        notChangedQuantificationMeasurements.remove(quantificationMeasurement);
        // Removed not changed measurements (in sorting and quantification measurement lists)
        if (quantificationMeasurements != null) {
            quantificationMeasurements.removeAll(notChangedQuantificationMeasurements);
        }

        return quantificationMeasurement;
    }

    public QuantificationMeasurement getWeightMeasurementQuantificationMeasurement(Batch batch) {
        return catchBatchDao.getQuantificationMeasurement(batch, PmfmId.WEIGHT_MEASURED.getValue());
    }

    public void removeWeightMeasurementQuantificationMeasurement(Batch batch, QuantificationMeasurement quantificationMeasurement) {
        Collection<QuantificationMeasurement> measurements = batch.getQuantificationMeasurements();
        Iterator<QuantificationMeasurement> iterator = measurements.iterator();
        while (iterator.hasNext()) {
            QuantificationMeasurement next = iterator.next();
            if (quantificationMeasurement.getId().equals(next.getId())) {
                iterator.remove();
                break;
            }
        }
    }

    public SortingMeasurement getInheritedSortingMeasurement(SortingBatch sortingBatch) {
        return catchBatchDao.getInheritedSortingMeasurement(sortingBatch, PmfmId.SORTED_UNSORTED.getValue());
    }

    //------------------------------------------------------------------------//
    //-- VesselUseUseMeasurement                                            --//
    //------------------------------------------------------------------------//

    public VesselUseMeasurement setVesselUseMeasurement(ScientificCruise scientificCruise, VesselUseFeatures vesselUseFeatures,
                                                        Integer pmfmId,
                                                        Float numericalValue,
                                                        String alphanumericalValue,
                                                        Integer qualitativevalueId) {
        VesselUseMeasurement vesselUseMeasurement = getVesselUseMeasurement(scientificCruise, vesselUseFeatures, pmfmId, true);

        if (alphanumericalValue != null) {
            vesselUseMeasurement.setAlphanumericalValue(alphanumericalValue);
        } else if (numericalValue != null) {
            vesselUseMeasurement.setNumericalValue(numericalValue);
        } else if (qualitativevalueId != null) {
            vesselUseMeasurement.setQualitativeValue(load(QualitativeValueImpl.class, qualitativevalueId));
        }

        return vesselUseMeasurement;
    }

    public VesselUseMeasurement setVesselUseMeasurement(ScientificCruise scientificCruise, VesselUseFeatures vesselUseFeatures,
                                                        Caracteristic caracteristic, Serializable value) {
        VesselUseMeasurement vesselUseMeasurement = getVesselUseMeasurement(scientificCruise, vesselUseFeatures, Integer.valueOf(caracteristic.getId()), true);
        setMeasurement(vesselUseMeasurement, caracteristic, value);
        return vesselUseMeasurement;
    }

    public VesselUseMeasurement getVesselUseMeasurement(ScientificCruise scientificCruise, VesselUseFeatures vesselUseFeatures,
                                                        Integer pmfmId, boolean createIfNotExists) {
        VesselUseMeasurement vesselUseMeasurement = null;
        if (vesselUseFeatures.getVesselUseMeasurements() != null) {
            for (VesselUseMeasurement vum : vesselUseFeatures.getVesselUseMeasurements()) {
                if (pmfmId.equals(vum.getPmfm().getId())) {
                    vesselUseMeasurement = vum;
                    break;
                }
            }
        }
        if (vesselUseMeasurement == null) {
            if (!createIfNotExists) {
                return null;
            }
            vesselUseMeasurement = VesselUseMeasurement.Factory.newInstance();
            vesselUseMeasurement.setVesselUseFeatures(vesselUseFeatures);
            if (vesselUseFeatures.getVesselUseMeasurements() == null) {
                vesselUseFeatures.setVesselUseMeasurements(Sets.newHashSet(vesselUseMeasurement));
            } else {
                vesselUseFeatures.getVesselUseMeasurements().add(vesselUseMeasurement);
            }
            vesselUseMeasurement.setQualityFlag(load(QualityFlagImpl.class, QualityFlagCode.NOTQUALIFIED.getValue()));
            vesselUseMeasurement.setDepartment(scientificCruise.getRecorderDepartment());
            vesselUseMeasurement.setPmfm(load(PmfmImpl.class, pmfmId));
        }

        return vesselUseMeasurement;
    }

    protected VesselUseMeasurement setVesselUseMeasurement(ScientificCruise scientificCruise, VesselUseFeatures vesselUseFeatures,
                                                           Integer pmfmId,
                                                           Serializable value) {
        VesselUseMeasurement vesselUseMeasurement = getVesselUseMeasurement(scientificCruise, vesselUseFeatures, pmfmId, true);

        if (value instanceof String) {
            vesselUseMeasurement.setAlphanumericalValue((String) value);
        } else if (value instanceof Float) {
            vesselUseMeasurement.setNumericalValue((Float) value);
        } else if (value instanceof Integer) {
            vesselUseMeasurement.setQualitativeValue(load(QualitativeValueImpl.class, value));
        }

        return vesselUseMeasurement;
    }

    //------------------------------------------------------------------------//
    //-- GearUseMeasurement                                                 --//
    //------------------------------------------------------------------------//

    public GearUseMeasurement setGearUseMeasurement(ScientificCruise scientificCruise, GearUseFeatures gearUseFeatures,
                                                    Integer pmfmId,
                                                    Float numericalValue,
                                                    String alphanumericalValue,
                                                    Integer qualitativevalueId) {
        GearUseMeasurement gearUseMeasurement = getGearUseMeasurement(scientificCruise, gearUseFeatures, pmfmId, true);

        if (alphanumericalValue != null) {
            gearUseMeasurement.setAlphanumericalValue(alphanumericalValue);
        } else if (numericalValue != null) {
            gearUseMeasurement.setNumericalValue(numericalValue);
        } else if (qualitativevalueId != null) {
            QualitativeValue qv = load(QualitativeValueImpl.class, qualitativevalueId);
            gearUseMeasurement.setQualitativeValue(qv);
        }

        return gearUseMeasurement;
    }

    public GearUseMeasurement setGearUseMeasurement(ScientificCruise scientificCruise, GearUseFeatures gearUseFeatures,
                                                    Caracteristic caracteristic, Serializable value) {
        GearUseMeasurement gearUseMeasurement = getGearUseMeasurement(scientificCruise, gearUseFeatures, Integer.valueOf(caracteristic.getId()), true);
        setMeasurement(gearUseMeasurement, caracteristic, value);
        return gearUseMeasurement;
    }

    protected GearUseMeasurement getGearUseMeasurement(ScientificCruise scientificCruise, GearUseFeatures gearUseFeatures,
                                                       Integer pmfmId, boolean createIfNotExists) {
        GearUseMeasurement gearUseMeasurement = null;
        if (gearUseFeatures.getGearUseMeasurements() != null) {
            for (GearUseMeasurement vum : gearUseFeatures.getGearUseMeasurements()) {
                if (pmfmId.equals(vum.getPmfm().getId())) {
                    gearUseMeasurement = vum;
                    break;
                }
            }
        }
        if (gearUseMeasurement == null) {
            if (!createIfNotExists) {
                return null;
            }
            gearUseMeasurement = GearUseMeasurement.Factory.newInstance();
            gearUseMeasurement.setGearUseFeatures(gearUseFeatures);
            if (gearUseFeatures.getGearUseMeasurements() == null) {
                gearUseFeatures.setGearUseMeasurements(Sets.newHashSet(gearUseMeasurement));
            } else {
                gearUseFeatures.getGearUseMeasurements().add(gearUseMeasurement);
            }
            gearUseMeasurement.setQualityFlag(load(QualityFlagImpl.class, QualityFlagCode.NOTQUALIFIED.getValue()));
            gearUseMeasurement.setDepartment(scientificCruise.getRecorderDepartment());
            Pmfm pmfm = (Pmfm) getCurrentSession().load(PmfmImpl.class, pmfmId);
            gearUseMeasurement.setPmfm(pmfm);
        }

        return gearUseMeasurement;
    }


    //------------------------------------------------------------------------//
    //-- Misc                                                               --//
    //------------------------------------------------------------------------//

    public String toString(Measurement measurement) {
        Pmfm pmfm = load(PmfmImpl.class, measurement.getPmfm().getId());
        String value = null;
        if (measurement.getQualitativeValue() != null) {
            QualitativeValue qualitativeValue = measurement.getQualitativeValue();
            String qualitativeName = load(QualitativeValueImpl.class, qualitativeValue.getId()).getName();

            value = qualitativeName + " (" + qualitativeValue.getId() + ")";
        } else if (measurement.getNumericalValue() != null) {
            value = "" + measurement.getNumericalValue();
        } else if (measurement.getAlphanumericalValue() != null) {
            value = measurement.getAlphanumericalValue();
        }
        return "[id:" + measurement.getId() + "] - " + pmfm.getParameter().getName() + " (" + pmfm.getId() + ") / " + value;
    }

    //------------------------------------------------------------------------//
    //-- SurveyMeasurement                                                  --//
    //------------------------------------------------------------------------//

    public SurveyMeasurement setSurveyMeasurement(FishingTrip fishingTrip,
                                                  Integer pmfmId,
                                                  Float numericalValue,
                                                  String alphanumericalValue,
                                                  Integer qualitativevalueId) {
        SurveyMeasurement result = getSurveyMeasurement(fishingTrip, pmfmId);

        if (result == null) {
            result = createSurveyMeasurement(fishingTrip,
                                             pmfmId,
                                             numericalValue,
                                             alphanumericalValue,
                                             qualitativevalueId);
        }

        return result;
    }

    public SurveyMeasurement removeSurveyMeasurement(FishingTrip fishingTrip,
                                                     Integer pmfmId) {
        SurveyMeasurement result = getSurveyMeasurement(fishingTrip, pmfmId);

        if (result != null) {

            // measurement found, remove it
            result.setFishingTrip(null);
            fishingTrip.getSurveyMeasurements().remove(result);
        }

        return result;
    }

    protected SurveyMeasurement createSurveyMeasurement(FishingTrip fishingTrip,
                                                        Integer pmfmId,
                                                        Float numericalValue,
                                                        String alphanumericalValue,
                                                        Integer qualitativevalueId) {

        SurveyMeasurement result = SurveyMeasurement.Factory.newInstance();
        result.setQualityFlag(load(QualityFlagImpl.class, QualityFlagCode.NOTQUALIFIED.getValue()));
        result.setDepartment(fishingTrip.getRecorderDepartment());
        Pmfm pmfm = load(PmfmImpl.class, pmfmId);
        result.setPmfm(pmfm);
        result.setFishingTrip(fishingTrip);

        if (alphanumericalValue != null) {
            result.setAlphanumericalValue(alphanumericalValue);
        } else if (numericalValue != null) {
            result.setNumericalValue(numericalValue);
        } else if (qualitativevalueId != null) {
            result.setQualitativeValue(load(QualitativeValueImpl.class, qualitativevalueId));
        }

        // add it to fishingTrip
        if (fishingTrip.getSurveyMeasurements() == null) {

            //create new set of measurements
            fishingTrip.setSurveyMeasurements(Sets.<SurveyMeasurement>newHashSet());
        }

        fishingTrip.getSurveyMeasurements().add(result);

        return result;
    }

    protected SurveyMeasurement getSurveyMeasurement(FishingTrip fishingTrip, Integer pmfmId) {
        SurveyMeasurement result = null;

        for (SurveyMeasurement vum : fishingTrip.getSurveyMeasurements()) {
            if (pmfmId.equals(vum.getPmfm().getId())) {
                result = vum;
                break;
            }
        }

        return result;
    }

    public Integer getRecorderDepartmentId() {
        // TODO BL : voir si on peut récupérer le departement (du 1er saisisseur ?)
        return DepartmentId.UNKNOWN_RECORDER_DEPARTMENT.getValue();
    }
}

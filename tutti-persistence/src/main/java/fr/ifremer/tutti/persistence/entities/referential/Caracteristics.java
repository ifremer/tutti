package fr.ifremer.tutti.persistence.entities.referential;

/*
 * #%L
 * Tutti :: Persistence
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Predicate;

public class Caracteristics extends AbstractCaracteristics {

    public static CaracteristicType getType(boolean isAlpha,
                                            boolean isQualitative) {
        CaracteristicType result;
        if (isAlpha) {
            result = CaracteristicType.TEXT;
        } else if (isQualitative) {
            result = CaracteristicType.QUALITATIVE;
        } else {
            result = CaracteristicType.NUMBER;
        }
        return result;
    }

    public static boolean isNumberCaracteristic(Caracteristic c) {
        return CaracteristicType.NUMBER == c.getCaracteristicType();
    }

    public static boolean isTextCaracteristic(Caracteristic c) {
        return CaracteristicType.TEXT == c.getCaracteristicType();
    }

    public static boolean isQualitativeCaracteristic(Caracteristic c) {
        return CaracteristicType.QUALITATIVE == c.getCaracteristicType();
    }

    public static Predicate<Caracteristic> newSampleCategoryModelPredicate() {
        return new SampleCategoryCaracteristicPredicate();
    }

    public static class SampleCategoryCaracteristicPredicate implements Predicate<Caracteristic> {

        @Override
        public boolean apply(Caracteristic input) {
            return !CaracteristicType.TEXT.equals(input.getCaracteristicType());
        }
    }
} //Caracteristics

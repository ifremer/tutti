package fr.ifremer.tutti.persistence.entities.referential;

/*
 * #%L
 * Tutti :: Persistence
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.TuttiPersistence;
import fr.ifremer.tutti.persistence.entities.data.SpeciesBatch;
import fr.ifremer.tutti.persistence.entities.protocol.SpeciesProtocol;

import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 * Created on 2/17/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.14
 */
public class TaxonCache {

    protected final boolean loadVernacularCode;

    protected final TuttiPersistence persistenceService;

    protected final Map<Integer, SpeciesProtocol> protocolMap;

    protected final Map<Integer, Species> speciesByReferenceTaxonId;

    TaxonCache(boolean loadVernacularCode, TuttiPersistence persistenceService, Map<Integer, SpeciesProtocol> protocolMap) {
        this.loadVernacularCode = loadVernacularCode;
        this.persistenceService = persistenceService;
        this.protocolMap = protocolMap;
        this.speciesByReferenceTaxonId = new TreeMap<>();
    }

    public String getLengthStepPmfmId(Species species) {

        SpeciesProtocol speciesProtocol = protocolMap.get(species.getReferenceTaxonId());
        return speciesProtocol == null ? null : speciesProtocol.getLengthStepPmfmId();

    }

    public Float getLengthStep(Species species) {

        SpeciesProtocol speciesProtocol = protocolMap.get(species.getReferenceTaxonId());
        return speciesProtocol == null ? null : speciesProtocol.getLengthStep();

    }

    public void load(List<Species> speciesList) {

        for (Species species : speciesList) {
            load(species);
        }

    }

    public void loadInBatches(List<SpeciesBatch> speciesAbleBatches) {

        for (SpeciesBatch speciesAbleBatch : speciesAbleBatches) {
            load(speciesAbleBatch.getSpecies());
        }

    }

    public void load(Species species) {

        Integer referenceTaxonId = species.getReferenceTaxonId();

        Species speciesLoaded = speciesByReferenceTaxonId.get(referenceTaxonId);

        if (speciesLoaded == null) {

            if (loadVernacularCode) {

                Species speciesWithVerncularCode =
                        persistenceService.getSpeciesByReferenceTaxonIdWithVernacularCode(referenceTaxonId);
                species.setVernacularCode(speciesWithVerncularCode.getVernacularCode());

            }

            if (protocolMap.containsKey(species.getReferenceTaxonId())) {

                SpeciesProtocol speciesProtocol = protocolMap.get(species.getReferenceTaxonId());
                String surveyCode = speciesProtocol.getSpeciesSurveyCode();
                species.setSurveyCode(surveyCode);

            }

            speciesByReferenceTaxonId.put(species.getReferenceTaxonId(), species);

        } else {

            species.setVernacularCode(speciesLoaded.getVernacularCode());
            species.setSurveyCode(speciesLoaded.getSurveyCode());

        }

    }

    public boolean containsLengthStepPmfmId(Species species) {
        return getLengthStepPmfmId(species)!=null;
    }
}

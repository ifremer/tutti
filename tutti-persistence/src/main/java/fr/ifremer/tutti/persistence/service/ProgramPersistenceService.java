package fr.ifremer.tutti.persistence.service;

/*
 * #%L
 * Tutti :: Persistence
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.TuttiPersistenceServiceImplementor;
import fr.ifremer.tutti.persistence.entities.data.Program;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * CRUD of {@link Program} entity.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 0.3
 */
@Transactional(readOnly = true)
public interface ProgramPersistenceService extends TuttiPersistenceServiceImplementor {

    /**
     * Get all programs.
     *
     * <strong>Note:</strong> For each program, his zone is loaded.
     *
     * @return the list of programs found in db.
     */
    @Cacheable(value = "programs")
    List<Program> getAllProgram();

    /**
     * Get the program for the given {@code id}.
     *
     * <strong>Note:</strong> his zone is loaded.
     *
     * @param id id of the program to load
     * @return the loaded program
     */
    Program getProgram(String id);

    @Transactional(readOnly = false)
    @CacheEvict(value = {"programs", "programZones"}, allEntries = true)
    Program createProgram(Program bean);

    @Transactional(readOnly = false)
    @CacheEvict(value = {"programs", "programZones"}, allEntries = true)
    Program saveProgram(Program bean);

}

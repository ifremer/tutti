package fr.ifremer.tutti.persistence.service;

/*
 * #%L
 * Tutti :: Persistence
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import fr.ifremer.adagio.core.dao.data.measure.file.MeasurementFile;
import fr.ifremer.adagio.core.dao.data.measure.file.MeasurementFileDao;
import fr.ifremer.adagio.core.dao.referential.ObjectTypeCode;
import fr.ifremer.adagio.core.dao.referential.ObjectTypeImpl;
import fr.ifremer.adagio.core.dao.referential.QualityFlagCode;
import fr.ifremer.adagio.core.dao.referential.QualityFlagImpl;
import fr.ifremer.tutti.persistence.entities.data.Attachment;
import fr.ifremer.tutti.persistence.entities.data.Attachments;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.type.IntegerType;
import org.hibernate.type.StringType;
import org.nuiton.jaxx.application.ApplicationIOUtil;
import org.springframework.dao.DataRetrievalFailureException;
import org.springframework.dao.InvalidDataAccessResourceUsageException;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.io.File;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import static org.nuiton.i18n.I18n.t;

/**
 * Default implementation of the service {@link AttachmentPersistenceService}.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.0.2
 */
@Service("attachmentPersistenceService")
public class AttachmentPersistenceServiceImpl extends AbstractPersistenceService implements AttachmentPersistenceService {

    /** Logger. */
    private static final Log log =
            LogFactory.getLog(AttachmentPersistenceServiceImpl.class);

    protected static final String ATTACHMENT_PATH_FORMAT = "%1$s/OBJ%2$s/%1$s-OBJ%2$s-%3$s.%4$s";

    @Resource(name = "measurementFileDao")
    protected MeasurementFileDao measurementFileDao;

    protected File dbAttachmentDirectory;

    @Override
    public void init() {
        super.init();
        dbAttachmentDirectory = config.getDbAttachmentDirectory();

        if (log.isDebugEnabled()) {
            log.debug("Db Attachment storage: " + dbAttachmentDirectory);
        }
    }

    //------------------------------------------------------------------------//
    //-- AttachmentPersistenceService implementation                        --//
    //------------------------------------------------------------------------//

    @Override
    public List<Attachment> getAllAttachments(ObjectTypeCode objectType,
                                              Integer objectId) {
        Iterator<Object[]> list = queryList(
                "allAttachment",
                "objectId", IntegerType.INSTANCE, objectId,
                "objectTypeCode", StringType.INSTANCE, objectType.getValue()
        );

        List<Attachment> result = Lists.newArrayList();
        while (list.hasNext()) {
            Object[] source = list.next();
            Attachment target = Attachments.newAttachment();
            loadAttachment(source, target);
            result.add(target);
        }
        return Collections.unmodifiableList(result);
    }

    @Override
    public File getAttachmentFile(String attachmentId) {
        Object[] source = queryUnique(
                "attachment",
                "attachmentId", IntegerType.INSTANCE, Integer.valueOf(attachmentId));

        if (source == null) {
            throw new DataRetrievalFailureException(
                    "Could not retrieve Attachment with id=" + attachmentId);
        }
        Attachment target = Attachments.newAttachment();
        loadAttachment(source, target);

        return getFile(target);
    }

    @Override
    public Attachment createAttachment(Attachment attachment, File file) {
        Preconditions.checkNotNull(attachment);
        Preconditions.checkNotNull(attachment.getObjectType());
        Preconditions.checkNotNull(attachment.getObjectId());
        Preconditions.checkArgument(
                attachment.getId() == null,
                "Attachment 'id' must be null to call createAttachment().");
        Preconditions.checkNotNull(file);

        // Create measurement file
        MeasurementFile measurementFile = MeasurementFile.Factory.newInstance();

        // no usage of pmfm (since version 1.5)
        measurementFile.setPmfm(null);

        // set not qualifed flag
        measurementFile.setQualityFlag(load(QualityFlagImpl.class, QualityFlagCode.NOTQUALIFIED.getValue()));

        // set objectType
        measurementFile.setObjectType(load(ObjectTypeImpl.class, attachment.getObjectType().getValue()));

        // set objectId
        measurementFile.setObjectId(attachment.getObjectId());

        // Use first a fake filePath (we don't know the id of attachment entity)
        measurementFile.setPath("FAKE-" + System.nanoTime());

        // copy our property
        attachmentToEntity(attachment, measurementFile);
        measurementFileDao.create(measurementFile);

        // get created id
        attachment.setId(String.valueOf(measurementFile.getId()));

        // Build now the real path
        String filePath = String.format(ATTACHMENT_PATH_FORMAT,
                                        measurementFile.getObjectType().getCode(),
                                        measurementFile.getObjectId(),
                                        measurementFile.getId(),
                                        ApplicationIOUtil.getExtension(file.getName()));

        // store the path
        attachment.setPath(filePath);
        measurementFile.setPath(filePath);

        if (log.isDebugEnabled()) {
            log.debug("Created attachment: " + attachment.getId() +
                      ", path: " + filePath);
        }

        // update measurementFile with correct path
        measurementFileDao.update(measurementFile);

        // copy file to disk local storage
        File targetFile = getFile(attachment);

        ApplicationIOUtil.copyFile(
                file, targetFile,
                t("tutti.persistence.attachment.copyFile.error", file, targetFile));

        return attachment;
    }

    @Override
    public Attachment saveAttachment(Attachment attachment) {
        Preconditions.checkNotNull(attachment);
        Preconditions.checkNotNull(attachment.getObjectType());
        Preconditions.checkNotNull(attachment.getObjectId());
        Preconditions.checkNotNull(
                attachment.getId(),
                "Attachment 'id' must not be null or empty to be saved.");

        MeasurementFile measurementFile = measurementFileDao.load(Integer.valueOf(attachment.getId()));
        if (measurementFile == null) {
            throw new DataRetrievalFailureException("Could not retrieve Attachment with id=" + attachment.getId());
        }

        // can't change the objectType
        String oldObjectTypeCode = measurementFile.getObjectType().getCode();
        if (ObjectUtils.notEqual(attachment.getObjectType().getValue(),
                                 oldObjectTypeCode)) {
            throw new InvalidDataAccessResourceUsageException(
                    "Can't change objectType, was before " + oldObjectTypeCode);
        }

        // can't change either objectId
        Integer oldObjectId = measurementFile.getObjectId();
        if (ObjectUtils.notEqual(attachment.getObjectId(),
                                 oldObjectId)) {
            throw new InvalidDataAccessResourceUsageException(
                    "Can't change objectId, was before " + oldObjectId);
        }

        attachmentToEntity(attachment, measurementFile);
        measurementFileDao.update(measurementFile);
        return attachment;
    }

    @Override
    public void deleteAttachment(String attachmentId) {
        Integer id = Integer.valueOf(attachmentId);
        Object[] source = queryUnique(
                "attachment",
                "attachmentId", IntegerType.INSTANCE, id);

        if (source == null) {
            throw new DataRetrievalFailureException(
                    "Could not retrieve Attachment with id=" + attachmentId);
        }
        Attachment target = Attachments.newAttachment();
        loadAttachment(source, target);

        delete(target);

        getCurrentSession().flush();
    }

    @Override
    public void deleteAllAttachment(ObjectTypeCode objectType, Integer objectId) {

        List<Attachment> attachments = getAllAttachments(objectType, objectId);
        attachments.forEach(this::delete);

    }

    @Override
    public void deleteAllAttachment(ObjectTypeCode objectType, Set<Integer> objectIds) {

        for (Integer objectId : objectIds) {
            List<Attachment> attachments = getAllAttachments(objectType, objectId);
            attachments.forEach(this::delete);
        }

    }

    //------------------------------------------------------------------------//
    //-- Internal methods                                                   --//
    //------------------------------------------------------------------------//

    protected void loadAttachment(Object[] source,
                                  Attachment target) {

        target.setObjectType(ObjectTypeCode.valueOf((String) source[0]));
        target.setObjectId((Integer) source[1]);
        target.setId(String.valueOf(source[2]));
        target.setPath((String) source[3]);
        target.setName((String) source[4]);
        target.setComment((String) source[5]);
    }

    protected void attachmentToEntity(Attachment attachment,
                                      MeasurementFile measurementFile) {
        measurementFile.setName(attachment.getName());
        measurementFile.setComments(attachment.getComment());
    }

    protected File getFile(Attachment attachment) {
        return new File(dbAttachmentDirectory, attachment.getPath());
    }

    protected void delete(Attachment target) {

        File file = getFile(target);

        if (log.isDebugEnabled()) {
            log.debug("Will delete attachment: " + target.getName() + " -- " + file);
        }
        measurementFileDao.remove(target.getIdAsInt());

        if (file.exists()) {
            ApplicationIOUtil.deleteFile(file, t("tutti.persistence.attachment.deleteFile.error", file));
        } else {
            if (log.isWarnEnabled()) {
                log.warn("COULD NOT FIND Attachement at " + file);
            }
        }

        File parentFile = file.getParentFile();
        while (!parentFile.equals(dbAttachmentDirectory)) {

            File[] files = parentFile.listFiles();
            if (files != null && files.length == 0) {

                // can delete this directory
                if (log.isDebugEnabled()) {
                    log.debug("Remove empty directory: " + parentFile);
                }
                ApplicationIOUtil.deleteDirectory(parentFile, "Could not clean directory");
                parentFile = parentFile.getParentFile();
            } else {
                break;
            }

        }

    }
}

package fr.ifremer.tutti.persistence.entities.protocol.v1;

/*
 * #%L
 * Tutti :: Persistence
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.entities.TuttiEntityBean;

/**
 * To migrate old species protocol to last version.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.4
 */
public class SpeciesProtocolBean1 extends TuttiEntityBean implements SpeciesProtocol1 {

    private static final long serialVersionUID = 3486411950096802662L;

    protected Integer speciesReferenceTaxonId;

    protected String speciesSurveyCode;

    protected boolean sizeEnabled;

    protected boolean sexEnabled;

    protected boolean maturityEnabled;

    protected boolean ageEnabled;

    protected String lengthStepPmfmId;

    protected boolean weightEnabled;

    protected boolean countIfNoFrequencyEnabled;

    protected boolean calcifySampleEnabled;

    protected Float lengthStep;

    protected boolean madeFromAReferentTaxon;

    @Override
    public Integer getSpeciesReferenceTaxonId() {
        return speciesReferenceTaxonId;
    }

    @Override
    public void setSpeciesReferenceTaxonId(Integer speciesReferenceTaxonId) {
        this.speciesReferenceTaxonId = speciesReferenceTaxonId;
    }

    @Override
    public String getSpeciesSurveyCode() {
        return speciesSurveyCode;
    }

    @Override
    public void setSpeciesSurveyCode(String speciesSurveyCode) {
        this.speciesSurveyCode = speciesSurveyCode;
    }

    @Override
    public boolean isSizeEnabled() {
        return sizeEnabled;
    }

    @Override
    public void setSizeEnabled(boolean sizeEnabled) {
        this.sizeEnabled = sizeEnabled;
    }

    @Override
    public boolean isSexEnabled() {
        return sexEnabled;
    }

    @Override
    public void setSexEnabled(boolean sexEnabled) {
        this.sexEnabled = sexEnabled;
    }

    @Override
    public boolean isMaturityEnabled() {
        return maturityEnabled;
    }

    @Override
    public void setMaturityEnabled(boolean maturityEnabled) {
        this.maturityEnabled = maturityEnabled;
    }

    @Override
    public boolean isAgeEnabled() {
        return ageEnabled;
    }

    @Override
    public void setAgeEnabled(boolean ageEnabled) {
        this.ageEnabled = ageEnabled;
    }

    @Override
    public String getLengthStepPmfmId() {
        return lengthStepPmfmId;
    }

    @Override
    public void setLengthStepPmfmId(String lengthStepPmfmId) {
        this.lengthStepPmfmId = lengthStepPmfmId;
    }

    @Override
    public boolean isWeightEnabled() {
        return weightEnabled;
    }

    @Override
    public void setWeightEnabled(boolean weightEnabled) {
        this.weightEnabled = weightEnabled;
    }

    @Override
    public boolean isCountIfNoFrequencyEnabled() {
        return countIfNoFrequencyEnabled;
    }

    @Override
    public void setCountIfNoFrequencyEnabled(boolean countIfNoFrequencyEnabled) {
        this.countIfNoFrequencyEnabled = countIfNoFrequencyEnabled;
    }

    @Override
    public boolean isCalcifySampleEnabled() {
        return calcifySampleEnabled;
    }

    @Override
    public void setCalcifySampleEnabled(boolean calcifySampleEnabled) {
        this.calcifySampleEnabled = calcifySampleEnabled;
    }

    @Override
    public Float getLengthStep() {
        return lengthStep;
    }

    @Override
    public void setLengthStep(Float lengthStep) {
        this.lengthStep = lengthStep;
    }

    @Override
    public boolean isMadeFromAReferentTaxon() {
        return madeFromAReferentTaxon;
    }

    @Override
    public void setMadeFromAReferentTaxon(boolean madeFromAReferentTaxon) {
        this.madeFromAReferentTaxon = madeFromAReferentTaxon;
    }

}

package fr.ifremer.tutti.persistence.service;

/*
 * #%L
 * Tutti :: Persistence
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import com.google.common.collect.Sets;
import fr.ifremer.adagio.core.dao.data.batch.Batch;
import fr.ifremer.adagio.core.dao.data.batch.CatchBatch;
import fr.ifremer.adagio.core.dao.data.batch.SortingBatch;
import fr.ifremer.adagio.core.dao.data.measure.SortingMeasurement;
import fr.ifremer.adagio.core.dao.referential.QualityFlagCode;
import fr.ifremer.adagio.core.dao.referential.QualityFlagImpl;
import fr.ifremer.adagio.core.dao.referential.pmfm.QualitativeValue;
import fr.ifremer.tutti.persistence.entities.TuttiEntities;
import fr.ifremer.tutti.persistence.entities.data.BatchContainer;
import fr.ifremer.tutti.persistence.entities.data.MarineLitterBatch;
import fr.ifremer.tutti.persistence.entities.data.MarineLitterBatchs;
import fr.ifremer.tutti.persistence.entities.referential.Caracteristic;
import fr.ifremer.tutti.persistence.entities.referential.CaracteristicQualitativeValue;
import fr.ifremer.tutti.persistence.service.referential.CaracteristicPersistenceService;
import fr.ifremer.tutti.persistence.service.util.BatchPersistenceHelper;
import fr.ifremer.tutti.persistence.service.util.tree.BatchTreeHelper;
import fr.ifremer.tutti.persistence.service.util.MeasurementPersistenceHelper;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;
import java.util.Set;

/**
 * @author Tony Chemit - chemit@codelutin.com
 * @since 0.3
 */
@Service("marineLitterBatchPersistenceService")
public class MarineLitterBatchPersistenceServiceImpl extends AbstractPersistenceService implements MarineLitterBatchPersistenceService {

    /** Logger. */
    private static final Log log =
            LogFactory.getLog(MarineLitterBatchPersistenceServiceImpl.class);

    @Resource(name = "caracteristicPersistenceService")
    private CaracteristicPersistenceService caracteristicService;

    @Resource(name = "batchPersistenceHelper")
    protected BatchPersistenceHelper batchHelper;

    @Resource(name = "measurementPersistenceHelper")
    protected MeasurementPersistenceHelper measurementPersistenceHelper;

    @Resource(name = "batchTreeHelper")
    protected BatchTreeHelper batchTreeHelper;

    protected Integer marineLitterCategory;

    protected Integer marineLitterSizeCategory;

    protected Map<Integer, CaracteristicQualitativeValue> marineLitterCategoryCaracteristicValues;

    protected Map<Integer, CaracteristicQualitativeValue> marineLitterSizeCategoryCaracteristicValues;

    @Override
    public void init() {
        super.init();

        Caracteristic marineLitterCategoryCaracteristic = caracteristicService.getMarineLitterCategoryCaracteristic();
        marineLitterCategory = marineLitterCategoryCaracteristic.getIdAsInt();
        marineLitterCategoryCaracteristicValues = TuttiEntities.splitByIdAsInt(marineLitterCategoryCaracteristic.getQualitativeValue());

        Caracteristic marineLitterSizeCategoryCaracteristic = caracteristicService.getMarineLitterSizeCategoryCaracteristic();
        marineLitterSizeCategory = marineLitterSizeCategoryCaracteristic.getIdAsInt();
        marineLitterSizeCategoryCaracteristicValues = TuttiEntities.splitByIdAsInt(marineLitterSizeCategoryCaracteristic.getQualitativeValue());
    }

    @Override
    public BatchContainer<MarineLitterBatch> getRootMarineLitterBatch(Integer fishingOperationId) {
        Preconditions.checkNotNull(fishingOperationId);

        CatchBatch catchBatch = batchHelper.getRootCatchBatchByFishingOperationId(fishingOperationId, false);

        // Get marine litter root batch
        SortingBatch sortingBatch = batchTreeHelper.getMarineLitterRootBatch(catchBatch);

        BatchContainer<MarineLitterBatch> result = new BatchContainer<>();

        if (sortingBatch != null) {
            result.setId(sortingBatch.getId());

            for (Batch batch1 : sortingBatch.getChildBatchs()) {
                SortingBatch source = (SortingBatch) batch1;

                MarineLitterBatch target = MarineLitterBatchs.newMarineLitterBatch();
                entityToBean(source, target);
                result.addChildren(target);

                if (log.isDebugEnabled()) {
                    log.debug("Loaded CatchBatch Hors Vrac > MarineLetter > " +
                              target.getMarineLitterCategory().getName() + " / " +
                              target.getMarineLitterSizeCategory().getName() + ": " +
                              target.getId());
                }
            }
        }

        return result;
    }

    @Override
    public MarineLitterBatch createMarineLitterBatch(MarineLitterBatch bean) {
        Preconditions.checkNotNull(bean);
        Preconditions.checkNotNull(bean.getFishingOperation());
        Preconditions.checkNotNull(bean.getFishingOperation().getId());

        CatchBatch catchBatch = batchHelper.getRootCatchBatchByFishingOperationId(bean.getFishingOperation().getIdAsInt(), false);

        return createMarineLitterBatch(catchBatch, bean);

    }

    @Override
    public Collection<MarineLitterBatch> createMarineLitterBatches(Integer fishingOperationId, Collection<MarineLitterBatch> beans) {

        Preconditions.checkNotNull(beans);

        CatchBatch catchBatch = batchHelper.getRootCatchBatchByFishingOperationId(fishingOperationId, false);

        Collection<MarineLitterBatch> result = new ArrayList<>(beans.size());

        for (MarineLitterBatch bean : beans) {

            MarineLitterBatch  created = createMarineLitterBatch(catchBatch, bean);
            result.add(created);

        }
        return result;
    }

    protected MarineLitterBatch  createMarineLitterBatch(CatchBatch catchBatch, MarineLitterBatch bean) {

        Preconditions.checkNotNull(bean);
        Preconditions.checkArgument(bean.getId() == null);
        Preconditions.checkNotNull(bean.getMarineLitterCategory());
        Preconditions.checkNotNull(bean.getMarineLitterSizeCategory());
        Preconditions.checkNotNull(bean.getNumber());

        SortingBatch batch = SortingBatch.Factory.newInstance();
        beanToEntity(bean, batch, catchBatch);
        return batchHelper.createSortingBatch(bean, catchBatch, batch);

    }

    @Override
    public MarineLitterBatch saveMarineLitterBatch(MarineLitterBatch bean) {
        Preconditions.checkNotNull(bean);
        Preconditions.checkNotNull(bean.getId());
        Integer batchId = bean.getIdAsInt();
        CatchBatch catchBatch = batchHelper.getRootCatchBatchByBatchId(batchId);
        SortingBatch batch = batchHelper.getSortingBatchById(catchBatch, batchId);
        beanToEntity(bean, batch, catchBatch);
        batchHelper.updateSortingBatch(batch, catchBatch);

        return bean;
    }

    @Override
    public void deleteMarineLitterBatch(Integer id) {
        Preconditions.checkNotNull(id);
        batchHelper.deleteBatch(id);
    }

    public MarineLitterBatch entityToBean(SortingBatch source, MarineLitterBatch target) {

        target.setId(source.getId().toString());

        // Rank order
        target.setRankOrder(Integer.valueOf(source.getRankOrder()));

        // Individual count
        target.setNumber(source.getIndividualCount());

        // Weight
        target.setWeight(source.getWeight());

        // Comments
        target.setComment(source.getComments());

        // Should have 2 quantification measurements

        for (SortingMeasurement measurement : source.getSortingMeasurements()) {
            Integer pmfmId = measurement.getPmfm().getId();
            if (marineLitterCategory.equals(pmfmId)) {
                QualitativeValue qualitativeValue = measurement.getQualitativeValue();
                Integer id = qualitativeValue.getId();
                CaracteristicQualitativeValue value = marineLitterCategoryCaracteristicValues.get(id);
                target.setMarineLitterCategory(value);
            } else if (marineLitterSizeCategory.equals(pmfmId)) {
                QualitativeValue qualitativeValue = measurement.getQualitativeValue();
                Integer id = qualitativeValue.getId();
                CaracteristicQualitativeValue value = marineLitterSizeCategoryCaracteristicValues.get(id);
                target.setMarineLitterSizeCategory(value);
            }
        }
        return target;
    }

    protected void beanToEntity(MarineLitterBatch source,
                                SortingBatch target,
                                CatchBatch catchBatch) {

//        Preconditions.checkNotNull(source.getFishingOperation());
//        Preconditions.checkNotNull(source.getFishingOperation().getId());

        // If parent and root need to be set
        if (target.getId() == null || target.getRootBatch() == null) {
            setMarineLitterBatchParents(target, catchBatch);
        }

        // RankOrder (initialize once, at creation)
        if (target.getRankOrder() == null) {

            short rankOrder = batchHelper.computeRankOrder(target);
            target.setRankOrder(rankOrder);

        }

        // Force subgroup count to '1', as Allegro
        target.setSubgroupCount(1f);

        // Sorting Measurements
        {
            Collection<SortingMeasurement> sortingMeasurements = target.getSortingMeasurements();

            Set<SortingMeasurement> notChangedSortingMeasurements = Sets.newHashSet();
            if (sortingMeasurements != null) {
                notChangedSortingMeasurements.addAll(sortingMeasurements);
            }

            // MarineLitterCategory
            if (source.getMarineLitterCategory() != null) {

                SortingMeasurement measurement = measurementPersistenceHelper.setSortingMeasurement(
                        target,
                        marineLitterCategory,
                        source.getMarineLitterCategory().getIdAsInt()
                );
                notChangedSortingMeasurements.remove(measurement);
            }

            // MarineLitterSizeCategory
            if (source.getMarineLitterSizeCategory() != null) {

                SortingMeasurement measurement = measurementPersistenceHelper.setSortingMeasurement(
                        target,
                        marineLitterSizeCategory,
                        source.getMarineLitterSizeCategory().getIdAsInt()
                );
                notChangedSortingMeasurements.remove(measurement);
            }

            if (sortingMeasurements != null) {
                sortingMeasurements.removeAll(notChangedSortingMeasurements);
            }
        }

        // Sampling Ratio + Weight
        batchTreeHelper.setWeightAndSampleRatio(target, source.getWeight(), null);

        // Individual count
        target.setIndividualCount(source.getNumber());

        // QualityFlag
        QualityFlagImpl qualityFlag = load(QualityFlagImpl.class, QualityFlagCode.NOTQUALIFIED.getValue());
        target.setQualityFlag(qualityFlag);

        // Comments
        target.setComments(source.getComment());

        // Exhaustive inventory (always true under a marine litter batch)
        target.setExhaustiveInventory(true);

    }

    private void setMarineLitterBatchParents(SortingBatch target, CatchBatch catchBatch) {

        Preconditions.checkNotNull(target);

        // -- Hors Vrac > Marine Litter
        SortingBatch parentBatch = batchTreeHelper.getMarineLitterRootBatch(catchBatch);

        if (parentBatch == null) {

            // -- Hors Vrac
            SortingBatch horsVracBatch = batchTreeHelper.getOrCreateHorsVracBatch(catchBatch);

            // -- Hors Vrac > Marine Litter
            parentBatch = batchTreeHelper.getOrCreateMarineLitterRootBatch(catchBatch, horsVracBatch, null);
        }

        target.setParentBatch(parentBatch);
        target.setRootBatch(catchBatch);
    }
}

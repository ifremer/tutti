package fr.ifremer.tutti.persistence.service.referential;

/*
 * #%L
 * Tutti :: Persistence
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import fr.ifremer.adagio.core.dao.administration.user.DepartmentCode;
import fr.ifremer.adagio.core.dao.administration.user.DepartmentId;
import fr.ifremer.adagio.core.dao.administration.user.PersonExtendDao;
import fr.ifremer.adagio.core.dao.administration.user.UserProfilId;
import fr.ifremer.adagio.core.dao.referential.StatusCode;
import fr.ifremer.adagio.core.vo.administration.user.PersonVO;
import fr.ifremer.tutti.persistence.entities.referential.Person;
import fr.ifremer.tutti.persistence.entities.referential.Persons;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.type.IntegerType;
import org.hibernate.type.StringType;
import org.nuiton.jaxx.application.ApplicationBusinessException;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

/**
 * Created on 11/3/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.8
 */
@Service("personPersistenceService")
public class PersonPersistenceServiceImpl extends ReferentialPersistenceServiceSupport implements PersonPersistenceService {

    /** Logger. */
    private static final Log log = LogFactory.getLog(PersonPersistenceServiceImpl.class);

    @Resource(name = "personDao")
    protected PersonExtendDao personDao;

    @Override
    public List<Person> getAllPerson() {

        Iterator<Object[]> list = queryListWithStatus(
                "allPersons",
                "observerProfilId", IntegerType.INSTANCE, UserProfilId.OBSERVER.getValue(),
                "projectMemberProfilId", IntegerType.INSTANCE, UserProfilId.PROJECT_MEMBER.getValue(),
                "userProfilId", IntegerType.INSTANCE, UserProfilId.USER.getValue(),
                "departementPrefixCode", StringType.INSTANCE, DepartmentCode.INSIDE_PREFIX.getValue()
        );

        List<Person> result = Lists.newArrayList();
        loadPersons(list, result);
        return Collections.unmodifiableList(result);

    }

    @Override
    public List<Person> getAllPersonWithObsoletes() {

        Iterator<Object[]> list = queryListWithStatus2(
                "allPersonsWithObsoletes",
                "observerProfilId", IntegerType.INSTANCE, UserProfilId.OBSERVER.getValue(),
                "projectMemberProfilId", IntegerType.INSTANCE, UserProfilId.PROJECT_MEMBER.getValue(),
                "userProfilId", IntegerType.INSTANCE, UserProfilId.USER.getValue(),
                "departementPrefixCode", StringType.INSTANCE, DepartmentCode.INSIDE_PREFIX.getValue()
        );

        List<Person> result = Lists.newArrayList();
        loadPersons(list, result);
        return Collections.unmodifiableList(result);
    }

    @Override
    public Person getPerson(Integer personId) {

        Object[] source = queryUniqueWithStatus2(
                "personById",
                "personId", IntegerType.INSTANCE, personId);

        return source == null ? null : loadPerson(source);

    }

    @Override
    public boolean isTemporaryPersonUsed(Integer id) {
        Long count = queryUniqueTyped("countManagerPersonInScientificCruise", "id", IntegerType.INSTANCE, id);
        boolean result = count > 0;

        if (!result) {
            count = queryUniqueTyped("countRecorderPersonInScientificCruise", "id", IntegerType.INSTANCE, id);
            result = count > 0;
        }

        if (!result) {
            count = queryUniqueTyped("countRecorderPersonInFishingTrip", "id", IntegerType.INSTANCE, id);
            result = count > 0;
        }

        if (!result) {
            count = queryUniqueTyped("countPersonInVesselPersonFeatures", "id", IntegerType.INSTANCE, id);
            result = count > 0;
        }

        return result;
    }

    @Override
    public List<Person> addTemporaryPersons(List<Person> persons) {

        List<Person> result = Lists.newArrayList();
        for (Person source : persons) {
            Person added = addTemporaryPerson(source);
            result.add(added);
        }
        return Collections.unmodifiableList(result);

    }

    @Override
    public List<Person> updateTemporaryPersons(List<Person> persons) {

        List<Person> result = Lists.newArrayList();
        for (Person source : persons) {
            Person updated = updateTemporaryPerson(source);
            result.add(updated);
        }
        return Collections.unmodifiableList(result);

    }

    @Override
    public List<Person> linkTemporaryPersons(List<Person> persons) {

        List<Person> result = Lists.newArrayList();
        for (Person source : persons) {
            Person linked = linkTemporaryPerson(source);
            result.add(linked);
        }
        return Collections.unmodifiableList(result);

    }

    @Override
    public void replacePerson(Person source, Person target, boolean delete) {

        Preconditions.checkNotNull(source);
        Preconditions.checkNotNull(target);
        Preconditions.checkState(Persons.isTemporary(source));
        Preconditions.checkState(!Persons.isTemporary(target));

        Integer sourceId = source.getIdAsInt();
        Integer targetId = target.getIdAsInt();

        queryUpdate("replaceManagerPersonInScientificCruise",
                    "sourceId", IntegerType.INSTANCE, sourceId,
                    "targetId", IntegerType.INSTANCE, targetId);

        queryUpdate("replaceRecorderPersonInScientificCruise",
                    "sourceId", IntegerType.INSTANCE, sourceId,
                    "targetId", IntegerType.INSTANCE, targetId);

        queryUpdate("replaceRecorderPersonInFishingTrip",
                    "sourceId", IntegerType.INSTANCE, sourceId,
                    "targetId", IntegerType.INSTANCE, targetId);

        queryUpdate("replacePersonInVesselPersonFeatures",
                    "sourceId", IntegerType.INSTANCE, sourceId,
                    "targetId", IntegerType.INSTANCE, targetId);

        cacheService.clearAllCaches();

        //TODO Check doublon...

        if (delete) {

            deleteTemporaryPerson(sourceId);

        }

    }

    @Override
    public void deleteTemporaryPersons(Collection<Integer> ids) {

        for (Integer id : ids) {
            deleteTemporaryPerson(id);
        }

    }

    @Override
    public void deleteTemporaryPerson(Integer id) {

        Preconditions.checkNotNull(id);
        if (id > 0) {
            throw new ApplicationBusinessException(String.format("Can't delete a Person with a positive id %d.", id));
        }
        Person person = getPerson(id);
        if (person == null) {
            throw new ApplicationBusinessException(String.format("Person with id %d does not exists", id));
        }
        personDao.remove(id);

    }

    protected Person addTemporaryPerson(Person source) {

        Preconditions.checkNotNull(source);
        Preconditions.checkNotNull(source.getFirstName());
        Preconditions.checkNotNull(source.getLastName());
        Preconditions.checkArgument(source.getIdAsInt() == null || Persons.isTemporaryId(source.getIdAsInt()));

        fr.ifremer.adagio.core.dao.administration.user.Person target =
                personDao.createAsTemporary(source.getLastName(), source.getFirstName(),
                                            DepartmentId.UNKNOWN_RECORDER_DEPARTMENT.getValue());
        Person result = Persons.newPerson();
        result.setId(target.getId());

        // Fill the result bean
        result.setLastName(source.getLastName());
        result.setFirstName(source.getFirstName());
        setStatus(StatusCode.TEMPORARY.getValue(), result);
        return result;

    }

    protected Person updateTemporaryPerson(Person source) {

        Preconditions.checkNotNull(source);
        Preconditions.checkNotNull(source.getFirstName());
        Preconditions.checkNotNull(source.getLastName());
        Preconditions.checkNotNull(source.getId());
        Preconditions.checkArgument(Persons.isTemporaryId(source.getIdAsInt()));

        Person result = getPerson(source.getIdAsInt());

        // Fill the result bean
        result.setLastName(source.getLastName());
        result.setFirstName(source.getFirstName());
        setStatus(StatusCode.TEMPORARY.getValue(), result);

        PersonVO toUpdate = new PersonVO();
        toUpdate.setId(source.getIdAsInt());
        toUpdate.setLastname(source.getLastName());
        toUpdate.setFirstname(source.getFirstName());
        toUpdate.setDepartmentId(DepartmentId.UNKNOWN_RECORDER_DEPARTMENT.getValue());
        toUpdate.setCreationDate(personDao.load(source.getIdAsInt()).getCreationDate());
        toUpdate.setStatusCode(StatusCode.TEMPORARY.getValue());
        personDao.save(toUpdate);

        return result;

    }

    protected Person linkTemporaryPerson(Person source) {

        Preconditions.checkNotNull(source);
        Preconditions.checkNotNull(source.getFirstName());
        Preconditions.checkNotNull(source.getLastName());
        Preconditions.checkNotNull(source.getId());
        Preconditions.checkArgument(Persons.isTemporaryId(source.getIdAsInt()));

        Iterator<Object[]> sources =queryListWithStatus(
                "personByFullName",
                "personFirstName", StringType.INSTANCE, source.getFirstName(),
                "personLastName", StringType.INSTANCE, source.getLastName());

        Object[] row = null;
        if (sources.hasNext()) {
            row = sources.next();

            if (sources.hasNext()) {
                Object[] next = sources.next();
                Person person = loadPerson(next);
                if (log.isInfoEnabled()) {
                    log.info("Il existe une autre personne d'identifiant "+person.getId()+" trouvée en pase pour la personne temporaire "+source.getFirstName()+" - "+source.getLastName());
                }
            }
        }

//        Object[] row = queryUniqueWithStatus(
//                "personByFullName",
//                "personFirstName", StringType.INSTANCE, source.getFirstName(),
//                "personLastName", StringType.INSTANCE, source.getLastName());

        return row == null ? null : loadPerson(row);

    }

    protected void loadPersons(Iterator<Object[]> list, List<Person> result) {
        while (list.hasNext()) {
            Object[] source = list.next();
            Person target = loadPerson(source);
            result.add(target);
        }
    }

    protected Person loadPerson(Object... source) {

        Person target = Persons.newPerson();
        target.setId(String.valueOf(source[0]));
        target.setLastName((String) source[1]);
        target.setFirstName((String) source[2]);
        target.setDepartment((String) source[3]);
        setStatus((String) source[4], target);
        return target;

    }

}

package fr.ifremer.tutti.persistence;

/*
 * #%L
 * Tutti :: Persistence
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.nuiton.jaxx.application.type.ApplicationProgressionModel;
import org.nuiton.updater.DownloadMonitor;

/**
 * Simple model for a progression long task.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.0
 */
public class ProgressionModel extends ApplicationProgressionModel implements DownloadMonitor {

    private static final long serialVersionUID = 1L;

    @Override
    public void setSize(long size) {
        setTotal((int) size);
    }

    @Override
    public void setCurrent(long current) {
        super.setCurrent((int) current);
    }
}

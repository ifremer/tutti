package fr.ifremer.tutti.persistence.test;

/*
 * #%L
 * Tutti :: Persistence
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Charsets;
import com.google.common.base.Preconditions;
import com.google.common.base.Predicate;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.google.common.io.Files;
import fr.ifremer.tutti.TuttiConfiguration;
import fr.ifremer.tutti.TuttiConfigurationOption;
import fr.ifremer.tutti.persistence.service.TuttiPersistenceServiceLocator;
import fr.ifremer.tutti.util.Jdbcs;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Assume;
import org.junit.rules.TestRule;
import org.junit.runner.Description;
import org.junit.runners.model.Statement;
import org.nuiton.config.ApplicationConfig;
import org.nuiton.converter.ConverterUtil;
import org.nuiton.util.FileUtil;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;
import java.util.Properties;
import java.util.Set;

/**
 * To box the persistence service as a test resource.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 0.3
 */
public class DatabaseResource implements TestRule {

    /** Logger. */
    private static final Log log = LogFactory.getLog(DatabaseResource.class);

    public static long BUILD_TIMESTAMP = System.nanoTime();

    private File resourceDirectory;

    private TuttiConfiguration config;

    private DatabaseFixtures fixtures;

    protected final String beanFactoryReferenceLocation;

    private final String beanRefFactoryReferenceId;

    private final boolean writeDb;

    private String dbName;

    private boolean destroyResources;

    public static DatabaseResource readDb() {
        return new DatabaseResource("");
    }

    public static DatabaseResource writeDb() {
        return new DatabaseResource("", true);
    }

    public static DatabaseResource readDb(String dbName) {
        return new DatabaseResource(dbName);
    }

    public static DatabaseResource writeDb(String dbName) {
        return new DatabaseResource(dbName, true);
    }

    public static DatabaseResource noDb() {
        return new DatabaseResource(
                "", "beanRefFactoryWitNoDb.xml", "TuttiBeanRefFactoryWithNoDb");
    }

    protected DatabaseResource(String dbName) {
        this(dbName, null, null, false);
    }

    protected DatabaseResource(String dbName, boolean writeDb) {
        this(dbName, null, null, writeDb);
    }

    protected DatabaseResource(String dbName, String beanFactoryReferenceLocation,
                               String beanRefFactoryReferenceId) {
        this(dbName, beanFactoryReferenceLocation,
             beanRefFactoryReferenceId, false);
    }

    protected DatabaseResource(String dbName, String beanFactoryReferenceLocation,
                               String beanRefFactoryReferenceId,
                               boolean writeDb) {
        this.dbName = dbName;
        this.beanFactoryReferenceLocation = beanFactoryReferenceLocation;
        this.beanRefFactoryReferenceId = beanRefFactoryReferenceId;
        this.writeDb = writeDb;
        this.destroyResources = true;
    }

    public boolean withDb() {
        return dbName!=null;
    }


    public TuttiConfiguration getConfig() {
        return config;
    }

    public DatabaseFixtures getFixtures() {
        return fixtures;
    }

    public File getResourceDirectory(String name) {
        return new File(resourceDirectory, name);
    }

    public void setDestroyResources(boolean destroyResources) {
        this.destroyResources = destroyResources;
    }

    public String getDbName() {
        return dbName;
    }

    public boolean isWriteDb() {
        return writeDb;
    }

    @Override
    public Statement apply(final Statement base, final Description description) {

        return new Statement() {
            @Override
            public void evaluate() throws Throwable {
                before(description);
                try {
                    base.evaluate();
                } finally {
                    after(description);
                }
            }
        };
    }

    Class<?> testClass;

    public void prepareConfig(ApplicationConfig applicationConfig,
                              File resourceDirectory) {

        TuttiConfiguration.getDefaultApplicationConfig(applicationConfig);

        applicationConfig.setDefaultOption(TuttiConfigurationOption.DATA_DIRECTORY.getKey(),
                                           new File(resourceDirectory, "data").getAbsolutePath());

        if (!writeDb) {

            // set tutti.persistence.db.directory
            File dbDirectory = FileUtil.getFileFromPaths(new File("src"), "test", "data", dbName);

            applicationConfig.setDefaultOption(TuttiConfigurationOption.DB_DIRECTORY.getKey(),
                                               dbDirectory.getAbsolutePath());

            // set tutti.persistence.jdbc.url
            applicationConfig.setDefaultOption(TuttiConfigurationOption.JDBC_URL.getKey(),
                                               String.format("jdbc:hsqldb:file:%s/allegro", dbDirectory.getAbsolutePath()));

        }
    }

    public File copyClassPathResource(String path, String destinationName) throws IOException {
        InputStream inputStream = getClass().getResourceAsStream("/" + path);
        Preconditions.checkNotNull(inputStream, "Could not find " + path + " in test class-path");
        File output = new File(resourceDirectory, destinationName);

        OutputStream outputStream = FileUtils.openOutputStream(output);
        try {
            IOUtils.copy(inputStream, outputStream);
            inputStream.close();
            outputStream.close();
        } finally {
            IOUtils.closeQuietly(inputStream);
            IOUtils.closeQuietly(outputStream);
        }
        return output;
    }

    protected void before(Description description) throws Throwable {

        TuttiTestSupport.registerDescriptionAndResource(description, this);

        testClass = description.getTestClass();

        boolean withDb = withDb();

        boolean defaultDbName = withDb  && StringUtils.isEmpty(dbName);

        if (defaultDbName) {
            dbName = "db";
        }

        if (log.isInfoEnabled()) {
            log.info("Prepare test " + testClass);
        }

        fixtures = new DatabaseFixtures();

        resourceDirectory = FileUtil.getTestSpecificDirectory(testClass, "", description.getMethodName(), BUILD_TIMESTAMP);
        addToDestroy(resourceDirectory);

        ConverterUtil.deregister();
        ConverterUtil.initConverters();

        ApplicationConfig applicationConfig= createApplicationConfig(defaultDbName);

        prepareConfig(applicationConfig, resourceDirectory);

        applicationConfig.parse();

        config = new TuttiConfiguration(applicationConfig);
        TuttiConfiguration.setInstance(config);

        if (withDb) {
            prepareDb();
        }

        config.initConfig();

        if (withDb) {
            if (log.isInfoEnabled()) {
                log.info("Use db: " + config.getJdbcUrl());
            }
        } else {
            if (log.isInfoEnabled()) {
                log.info("No db configured.");
            }
        }

        if (beanFactoryReferenceLocation != null) {
            TuttiPersistenceServiceLocator.initTutti(
                    beanFactoryReferenceLocation,
                    beanRefFactoryReferenceId);
        }
    }

    protected ApplicationConfig createApplicationConfig(boolean defaultDbName) {

        ApplicationConfig applicationConfig=null;
        if (withDb()) {

            String configFilename = writeDb ?
                                    "tutti-test-write" :
                                    "tutti-test-read";
            if (!defaultDbName) {
                configFilename += "-" + dbName;
            }

            configFilename += ".properties";
            InputStream resourceAsStream = getClass().getResourceAsStream("/" + configFilename);

            if (resourceAsStream!=null) {

                if (log.isInfoEnabled()) {
                    log.info("Use configuration file found in classpath at " + configFilename);
                }

                IOUtils.closeQuietly(resourceAsStream);

                applicationConfig = new ApplicationConfig(configFilename);

            }

        }

        if (applicationConfig==null) {

            if (log.isInfoEnabled()) {
                log.info("Use default configuration, with no configuration from class-path");
            }
            applicationConfig = new ApplicationConfig();
        }

        return applicationConfig;

    }

    protected void prepareDb() throws IOException {

        File db;

        if (writeDb) {

            // check db exist in src/test/data/dbName
            db = FileUtil.getFileFromPaths(new File("src"), "test", "data", dbName);

        } else {
            // check config.dbDirectory exist

            db = config.getDbDirectory();
        }

        if (!db.exists()) {

            if (log.isWarnEnabled()) {
                log.warn("Could not find db at " + db + ", test [" + testClass + "] is skipped.");
            }
            Assume.assumeTrue(false);
        }

        if (writeDb) {
            copyDb(config.getDbDirectory(), false, null);
        }

        addToDestroy(config.getDbAttachmentDirectory());

        // load db config
        File dbConfig = new File(config.getDbDirectory(), config.getDbName() + ".properties");
        Properties p = new Properties();
        BufferedReader reader = Files.newReader(dbConfig, Charsets.UTF_8);
        p.load(reader);
        reader.close();

        if (log.isDebugEnabled()) {
            log.debug("Db config: " + dbConfig + "\n" + p);
        }

        if (writeDb) {

            // make sure db is on readonly mode
            String readonly = p.getProperty("readonly");
            Preconditions.checkNotNull(readonly, "Could not find readonly property on db confg: " + dbConfig);
            Preconditions.checkState("false".equals(readonly), "readonly property must be at false value in write mode test in  db confg: " + dbConfig);
        } else {
            // make sure db is on readonly mode
            String readonly = p.getProperty("readonly");
            Preconditions.checkNotNull(readonly, "Could not find readonly property on db confg: " + dbConfig);
            Preconditions.checkState("true".equals(readonly), "readonly property must be at true value in read mode test in  db confg: " + dbConfig);
        }

    }

    protected final Set<File> toDetroy = Sets.newHashSet();

    public void addToDestroy(File dir) {
        toDetroy.add(dir);
    }

    public void copyDb(String dbDirectory, boolean readonly, Properties p) throws IOException {
        File externalDbFile = getResourceDirectory(dbDirectory);
        copyDb(externalDbFile, readonly, p);
    }

    public void copyDb(File target, boolean readonly, Properties p) throws IOException {
        File db = FileUtil.getFileFromPaths(new File("src"), "test", "data", dbName);
        if (!db.exists()) {

            if (log.isWarnEnabled()) {
                log.warn("Could not find db at " + db + ", test [" + testClass + "] is skipped.");
            }
            Assume.assumeTrue(false);
        }
        addToDestroy(target);
        FileUtils.copyDirectory(db, target);
        if (p != null) {
            Jdbcs.fillConnectionProperties(
                    p,
                    Jdbcs.getJdbcUrl(target, config.getDbName()),
                    config.getJdbcUsername(),
                    config.getJdbcPassword());
        }

        // load db config
        File dbConfig = new File(target, config.getDbName() + ".properties");
        Properties dbconf = new Properties();
        BufferedReader reader = Files.newReader(dbConfig, Charsets.UTF_8);
        dbconf.load(reader);
        reader.close();

        // switch readonly flag according to the write parameter
        dbconf.setProperty("readonly", String.valueOf(readonly));
        BufferedWriter writer = Files.newWriter(dbConfig, Charsets.UTF_8);
        dbconf.store(writer, "");
        writer.close();

    }

    public void cleanResources(Description description) {

        if (log.isDebugEnabled()) {
            log.debug("Clean resources for test: " + description);
        }

        if (destroyResources) {

            // can destroy directories
            for (File file : toDetroy) {
                if (file.exists()) {
                    if (log.isInfoEnabled()) {
                        log.info("Destroy directory: " + file);
                    }
                    try {
                        FileUtils.deleteDirectory(file);
                    } catch (IOException e) {
                        if (log.isErrorEnabled()) {
                            log.error("Could not delete directory: " + file, e);
                        }
                    }
                }
            }
        } else {

            if (log.isWarnEnabled()) {
                log.warn("Won't destroy directories (destroyResources flag was false).");
            }

        }

        toDetroy.clear();

    }

    protected void after(Description description) {
        if (log.isInfoEnabled()) {
            log.info("After test " + description);
        }

        closeSpring();
        TuttiConfiguration.setInstance(null);

        if (description.getMethodName() == null) {

            // rule used on a class
            CleanResourcesRule.cleanResources(description);

        }

    }

    protected void closeSpring() {

        TuttiPersistenceServiceLocator.shutdownTutti();

        if (beanFactoryReferenceLocation != null) {

            // push back default tutti configuration
            TuttiPersistenceServiceLocator.initTuttiDefault();
        }
    }

    protected List<String> getImportScriptSql(File scriptFile) throws IOException {
        List<String> lines = Files.readLines(scriptFile, Charsets.UTF_8);

        List<String> result = Lists.newArrayListWithCapacity(lines.size());

        Predicate<String> predicate = new Predicate<String>() {

            Set<String> forbiddenStarts = Sets.newHashSet(
                    "SET ",
                    "CREATE USER ",
                    "CREATE SCHEMA ",
                    "GRANT DBA TO ");

            @Override
            public boolean apply(String input) {
                boolean accept = true;
                for (String forbiddenStart : forbiddenStarts) {
                    if (input.startsWith(forbiddenStart)) {
                        accept = false;
                        break;
                    }
                }
                return accept;
            }
        };
        for (String line : lines) {
            if (predicate.apply(line.trim().toUpperCase())) {
                result.add(line);
            }
        }
        return result;
    }
}

package fr.ifremer.tutti.persistence.entities.protocol;

/*
 * #%L
 * Tutti :: Persistence
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2016 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.entities.referential.Caracteristic;

import java.util.List;
import java.util.function.Predicate;

public class SpeciesProtocols extends AbstractSpeciesProtocols {

    public static Predicate<SpeciesProtocol> speciesProtocolWhoseCategoryIsMandatoryPredicate(Caracteristic caracteristic) {
        return new SpeciesProtocolWhoseCategoryIsMandatoryPredicate(caracteristic);
    }

    private static class SpeciesProtocolWhoseCategoryIsMandatoryPredicate implements Predicate<SpeciesProtocol> {

        private final Integer caracteristicId;

        SpeciesProtocolWhoseCategoryIsMandatoryPredicate(Caracteristic caracteristic) {
            this.caracteristicId = caracteristic.getIdAsInt();
        }

        @Override
        public boolean test(SpeciesProtocol input) {
            List<Integer> mandatorySampleCategoryId = input.getMandatorySampleCategoryId();
            return mandatorySampleCategoryId != null && mandatorySampleCategoryId.contains(caracteristicId);
        }
    }
}

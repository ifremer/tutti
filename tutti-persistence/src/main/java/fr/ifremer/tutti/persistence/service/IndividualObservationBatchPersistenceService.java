package fr.ifremer.tutti.persistence.service;

/*
 * #%L
 * Tutti :: Persistence
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.TuttiPersistenceServiceImplementor;
import fr.ifremer.tutti.persistence.entities.data.FishingOperation;
import fr.ifremer.tutti.persistence.entities.data.IndividualObservationBatch;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.List;

/**
 * To manager {@link IndividualObservationBatch}.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.4
 */
@Transactional(readOnly = true)
public interface IndividualObservationBatchPersistenceService extends TuttiPersistenceServiceImplementor {

    List<IndividualObservationBatch> getAllIndividualObservationBatchsForBatch(Integer batchId);

    List<IndividualObservationBatch> getAllIndividualObservationBatchsForFishingOperation(Integer fishingOperationId);

    List<IndividualObservationBatch> getAllIndividualObservationBatchsForCruise(Integer cruiseId);

    /**
     * Test if a sampling code suffix is available for a given cruise and species all over the existing individual observations.
     *
     * @param cruiseId           id of the cruise of individual observations to test
     * @param referenceTaxonId   id of the species used in individual observations to test
     * @param samplingCodeSuffix the sampling code suffix to test
     * @return {@code true} if given sampling code is not already used in database, {@code false} otherwise.
     */
    boolean isSamplingCodeAvailable(Integer cruiseId, Integer referenceTaxonId, String samplingCodeSuffix);

    @Transactional(readOnly = false)
    List<IndividualObservationBatch> createIndividualObservationBatches(FishingOperation fishingOperation, Collection<IndividualObservationBatch> individualObservations);

    @Transactional(readOnly = false)
    List<IndividualObservationBatch> saveBatchIndividualObservation(Integer batchId, List<IndividualObservationBatch> individualObservation);

    @Transactional(readOnly = false)
    void deleteAllIndividualObservationsForFishingOperation(Integer fishingOperationId);

    @Transactional(readOnly = false)
    void deleteAllIndividualObservationsForBatch(Integer speciesBatchId);


}

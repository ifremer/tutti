package fr.ifremer.tutti.persistence.entities.referential;

/*
 * #%L
 * Tutti :: Persistence
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Function;
import com.google.common.base.Preconditions;
import com.google.common.base.Predicate;
import fr.ifremer.adagio.core.dao.technical.hibernate.TemporaryDataHelper;

public class Vessels extends AbstractVessels {

//    public static Map<String, Vessel> splitByRegistrationCode(List<Vessel> programs) {
//        return Maps.uniqueIndex(programs, GET_REGISTRATION_CODE);
//    }
//
//    public static Map<String, Vessel> splitByInternationalRegistrationCode(List<Vessel> programs) {
//        return Maps.uniqueIndex(programs, GET_INTERNAL_REGISTRATION_CODE);
//    }

    public static final Predicate<Vessel> IS_TEMPORARY = Vessels::isTemporary;

    public static Function<Vessel, String> GET_REGISTRATION_CODE_OR_INTERNATIONAL_REGISTRATION_CODE = input -> {

        String result = input.getRegistrationCode();
        if (result == null) {
            result = input.getInternationalRegistrationCode();
        }
        return result;
    };

//    public static final Function<Vessel, String> GET_REGISTRATION_CODE = Vessel::getRegistrationCode;

    public static final Function<Vessel, String> GET_INTERNAL_REGISTRATION_CODE = Vessel::getInternationalRegistrationCode;

    /**
     * Is the given {@code vessel} a temporary data ?
     *
     * @param vessel vessel to test
     * @return {@code true} if the given {@code vessel} is temporary
     * @since 3.8
     */
    public static boolean isTemporary(Vessel vessel) {

        Preconditions.checkNotNull(vessel);
        Preconditions.checkNotNull(vessel.getId());

        return TuttiReferentialEntities.isStatusTemporary(vessel) && isTemporaryId(vessel.getId());

    }

    /**
     * Is the given {@code id} is a vessel temporary id ?
     *
     * @param id id to test
     * @return {@code true} if the id is a temporary vessel id
     * @since 3.14
     */
    public static boolean isTemporaryId(String id) {

        Preconditions.checkNotNull(id);
        return id.startsWith(TemporaryDataHelper.TEMPORARY_NAME_PREFIX);

    }
}

package fr.ifremer.tutti.persistence.test;

/*
 * #%L
 * Tutti :: Persistence
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Lists;
import fr.ifremer.tutti.persistence.entities.CaracteristicMap;
import fr.ifremer.tutti.persistence.entities.data.AccidentalBatch;
import fr.ifremer.tutti.persistence.entities.data.AccidentalBatchs;
import fr.ifremer.tutti.persistence.entities.data.CatchBatch;
import fr.ifremer.tutti.persistence.entities.data.CatchBatchs;
import fr.ifremer.tutti.persistence.entities.data.Cruise;
import fr.ifremer.tutti.persistence.entities.data.Cruises;
import fr.ifremer.tutti.persistence.entities.data.FishingOperation;
import fr.ifremer.tutti.persistence.entities.data.FishingOperations;
import fr.ifremer.tutti.persistence.entities.referential.Caracteristic;
import fr.ifremer.tutti.persistence.entities.referential.CaracteristicType;
import fr.ifremer.tutti.persistence.entities.referential.Gear;
import fr.ifremer.tutti.persistence.entities.referential.GearWithOriginalRankOrder;
import fr.ifremer.tutti.persistence.entities.referential.GearWithOriginalRankOrders;
import fr.ifremer.tutti.persistence.entities.referential.Person;
import fr.ifremer.tutti.persistence.entities.referential.TuttiLocation;
import fr.ifremer.tutti.persistence.entities.referential.TuttiLocations;
import fr.ifremer.tutti.persistence.entities.referential.Vessel;
import fr.ifremer.tutti.persistence.entities.referential.Vessels;
import fr.ifremer.tutti.persistence.service.AccidentalBatchPersistenceService;
import fr.ifremer.tutti.persistence.service.CatchBatchPersistenceService;
import fr.ifremer.tutti.persistence.service.CruisePersistenceService;
import fr.ifremer.tutti.persistence.service.FishingOperationPersistenceService;
import fr.ifremer.tutti.persistence.service.IndividualObservationBatchPersistenceService;
import fr.ifremer.tutti.persistence.service.ProgramPersistenceService;
import fr.ifremer.tutti.persistence.service.TuttiPersistenceServiceLocator;
import fr.ifremer.tutti.persistence.service.referential.CaracteristicPersistenceService;
import fr.ifremer.tutti.persistence.service.referential.GearPersistenceService;
import fr.ifremer.tutti.persistence.service.referential.LocationPersistenceService;
import fr.ifremer.tutti.persistence.service.referential.PersonPersistenceService;
import fr.ifremer.tutti.persistence.service.referential.SpeciesPersistenceService;
import org.junit.Assert;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

/**
 * Fixtures for the allegro db.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 0.3
 */
public class DatabaseFixtures {

    public String programCode() {
        // campaign CGFS
        return "CAM-CGFS";
    }

    public Integer cruiseId() {
        // cruise CGFS2010
        return 100000;
    }

    public String fishingVesselCode() {
        return "851751";
    }

    public String zoneId() {
        // zone CGFS
        return "61979";
    }

    public String strataId() {
        // strate 6M
        return "57377";
    }

    public String subStrataId() {
        // substrata 61995
        return "61995";
    }

    public String localite() {
        // Localite 8Q3
        return "57774";
    }

    public int refNbCaracteristic() {
        return 485;
    }

    public int refNbSpecies() {
        return 16961;
    }

    public int refNbReferentSpecies() {
        return 8649;
    }

    public int refNbScientificGear() {
        return 9;
    }

    public int refNbFishingGear() {
        return 75;
    }

    public int refNbCountry() {
        return 239;
    }

    public int refNbHarbour() {
        return 1898;
    }

    public int refNbProgramZone() {
        return 14;
    }

    public int refNbStrata() {
        return 76;
    }

    public int refNbSubStrata() {
        return 16;
    }

    public int refNbLocalite() {
        return 129;
    }

    public int refNbFishingVessel() {
        return 24837;
    }

    public int refNbScientificVessel() {
        return 3;
    }

    public int refNbPerson() {
        return 128;
    }

    public int refNbObjectType() {
        return 57;
    }

    public String refSpeciesId() {
        return "15923";
    }

    public Integer refSpeciesTaxonId() {
        return 467;
    }

    public Integer refNumericalPmfmId() {
        return 113;
    }

    public Integer refAlphanumericPmfmId() {
        return 1424;
    }

    public String refSpeciesRefTaxCode() {
        return "SEPIOFF";
    }

    public String refSpeciesVernacularCode() {
        return "Seiche";
    }

    public Integer refBadSpeciesTaxonId() {
        return 7632;
    }

    public Integer refBad2SpeciesTaxonId() {
        return 2320;
    }

    public String objectTypeCode() {
        return "BATCH";
    }

    public Integer caracteristicWithNullQualitativeValue() {
        return 114;
    }

    protected CruisePersistenceService cruiseService;

    protected ProgramPersistenceService programService;

    protected FishingOperationPersistenceService fishingOperationService;

    protected CatchBatchPersistenceService catchBatchService;

    private CaracteristicPersistenceService caracteristicService;

    private GearPersistenceService gearService;

    private LocationPersistenceService locationService;

    private PersonPersistenceService personService;

    private SpeciesPersistenceService speciesService;

    protected AccidentalBatchPersistenceService accidentalBatchService;

    protected IndividualObservationBatchPersistenceService individualObservationBatchService;

    public void setUp() {
        if (caracteristicService == null) {
            caracteristicService = TuttiPersistenceServiceLocator.getCaracteristicPersistenceService();
            gearService = TuttiPersistenceServiceLocator.getGearPersistenceService();
            locationService = TuttiPersistenceServiceLocator.getLocationPersistenceService();
            personService = TuttiPersistenceServiceLocator.getPersonPersistenceService();
            speciesService = TuttiPersistenceServiceLocator.getSpeciesPersistenceService();
            programService = TuttiPersistenceServiceLocator.getProgramPersistenceService();
            cruiseService = TuttiPersistenceServiceLocator.getCruisePersistenceService();
            fishingOperationService = TuttiPersistenceServiceLocator.getFishingOperationPersistenceService();
            catchBatchService = TuttiPersistenceServiceLocator.getCatchBatchPersistenceService();
            accidentalBatchService = TuttiPersistenceServiceLocator.getAccidentalBatchPersistenceService();
            individualObservationBatchService = TuttiPersistenceServiceLocator.getIndividualObservationBatchPersistenceService();
        }
    }

    public Cruise createCruise() {

        setUp();

        String programCode = programCode();
        Cruise cruise = Cruises.newCruise();

        // -----------------------------------------------------------------------------
        // 1. Test with all properties filled
        // -----------------------------------------------------------------------------
        cruise.setId((String) null);

        cruise.setName("Unit-test-" + System.currentTimeMillis());

        cruise.setProgram(programService.getProgram(programCode));

        Calendar calendar = new GregorianCalendar();
        cruise.setBeginDate(calendar.getTime());

        calendar.add(Calendar.MONTH, 1); // add one month
        cruise.setEndDate(calendar.getTime());

        List<TuttiLocation> allHarbour = locationService.getAllHarbour();
        Assert.assertNotNull(allHarbour);
        Assert.assertTrue(allHarbour.size() > 1);
        cruise.setDepartureLocation(allHarbour.get(0));
        cruise.setReturnLocation(allHarbour.get(1));

        List<Gear> gears = gearService.getAllFishingGear();
        List<GearWithOriginalRankOrder> gearsWithOrder = Lists.newArrayListWithCapacity(gears.size());
        short rankOrder = (short) 0;
        for (Gear gear : gears) {
            GearWithOriginalRankOrder gearWithOriginalRankOrder = GearWithOriginalRankOrders.newGearWithOriginalRankOrder(gear);
            gearWithOriginalRankOrder.setOriginalRankOrder(rankOrder);
            gearWithOriginalRankOrder.setRankOrder(rankOrder);
            rankOrder++;
            gearsWithOrder.add(gearWithOriginalRankOrder);
        }
        cruise.setGear(Lists.newArrayList(gearsWithOrder));

        cruise.setComment("My comments on cruise");
        cruise.setSurveyPart("SurveyPart");

        Person managerPerson = personService.getAllPerson().get(0);
        cruise.setHeadOfMission(Lists.newArrayList(managerPerson));

        Vessel fishingVessel = Vessels.newVessel();
        fishingVessel.setId(fishingVesselCode());

        cruise.setVessel(fishingVessel);

        cruise.setMultirigNumber(2);

        return cruiseService.createCruise(cruise);
    }

    public FishingOperation createFishingOperation(Cruise cruise) {
        setUp();

        FishingOperation fishingOperation = FishingOperations.newFishingOperation();
        fishingOperation.setCruise(cruise);
        fishingOperation.setComment("newFishingOperation");
        fishingOperation.setFishingOperationNumber(1);

        // Retrieve some environment caracteristics
        List<Caracteristic> allEnvironmentCaracteristics = caracteristicService.getAllCaracteristic();
        CaracteristicMap environmentCaracteristics = new CaracteristicMap();
        CaracteristicMap environmentValuesOneEntry = new CaracteristicMap();
        int count = 0;
        for (Caracteristic caracteristic : allEnvironmentCaracteristics) {
            if (count == 3) {
                break;
            }
            Serializable value = null;
            if (caracteristic.getCaracteristicType() == CaracteristicType.NUMBER) {
                value = 1.0f;
                count++;
            } else if (caracteristic.getCaracteristicType() == CaracteristicType.TEXT) {
                value = "some text";
                count++;
            } else if (caracteristic.getCaracteristicType() == CaracteristicType.QUALITATIVE
                       && caracteristic.getQualitativeValue(0) != null) {
                // Choose the first qualitative value
                value = caracteristic.getQualitativeValue(0);
                count++;
            }
            if (value != null) {
                environmentCaracteristics.put(caracteristic, value);
                if (environmentValuesOneEntry.size() == 0) {
                    environmentValuesOneEntry.put(caracteristic, value);
                }
            }
        }

        // Retrieve some gear use caracteristics
        List<Caracteristic> allGearShootingCaracteristics = caracteristicService.getAllCaracteristic();
        CaracteristicMap gearShootingCaracteristics = new CaracteristicMap();
        CaracteristicMap gearShootingCaracteristicsOneEntry = new CaracteristicMap();
        count = 0;
        for (Caracteristic caracteristic : allGearShootingCaracteristics) {
            if (count == 3) {
                break;
            }
            Serializable value = null;
            if (caracteristic.getCaracteristicType() == CaracteristicType.NUMBER) {
                value = 1.0f;
                count++;
            } else if (caracteristic.getCaracteristicType() == CaracteristicType.TEXT) {
                value = "some text";
                count++;
            } else if (caracteristic.getCaracteristicType() == CaracteristicType.QUALITATIVE
                       && caracteristic.getQualitativeValue(0) != null) {
                // Choose the first qualitative value
                value = caracteristic.getQualitativeValue(0);
                count++;
            }
            if (value != null) {
                gearShootingCaracteristics.put(caracteristic, value);
                if (gearShootingCaracteristicsOneEntry.size() == 0) {
                    gearShootingCaracteristicsOneEntry.put(caracteristic, value);
                }
            }
        }

        Calendar calendar = Calendar.getInstance();
        fishingOperation.setId((String) null);
        fishingOperation.setStationNumber("STA2");
        fishingOperation.setFishingOperationNumber(2);
        fishingOperation.setMultirigAggregation("1");
        calendar.setTime(new Date());
        calendar.set(Calendar.HOUR_OF_DAY, 1);
        calendar.set(Calendar.MILLISECOND, 99);
        fishingOperation.setGearShootingStartDate(calendar.getTime());
        calendar.setTime(new Date());
        calendar.set(Calendar.HOUR_OF_DAY, 10);
        calendar.set(Calendar.MILLISECOND, 99);
        fishingOperation.setGearShootingEndDate(calendar.getTime());
        fishingOperation.setGearShootingStartLatitude(47.6f);
        fishingOperation.setGearShootingStartLongitude(-5.05f);
        fishingOperation.setGearShootingEndLatitude(47.9854f);
        fishingOperation.setGearShootingEndLongitude(-5.597f);

        fishingOperation.setTrawlDistance(100);
        fishingOperation.setFishingOperationRectiligne(true);
        fishingOperation.setFishingOperationValid(Boolean.TRUE);
        fishingOperation.setComment("Unit test createFishingOperation() - Part n°2 : All properties set");
        fishingOperation.setGear(cruise.getGear(0));
        fishingOperation.setVessel(cruise.getVessel());
        fishingOperation.setVesselUseFeatures(environmentCaracteristics);
        fishingOperation.setGearUseFeatures(gearShootingCaracteristics);

        TuttiLocation strata = TuttiLocations.newTuttiLocation();
        strata.setId(strataId());
        fishingOperation.setStrata(strata);
        TuttiLocation subStrata = TuttiLocations.newTuttiLocation();
        subStrata.setId(subStrataId());
        fishingOperation.setSubStrata(subStrata);
        TuttiLocation localite = TuttiLocations.newTuttiLocation();
        localite.setId(localite());
        fishingOperation.setLocation(localite);

        return fishingOperationService.createFishingOperation(fishingOperation);
    }

    public CatchBatch createMinimalCatchBatch(FishingOperation fishingOperation) {

        setUp();

        CatchBatch catchBatch = CatchBatchs.newCatchBatch();
        catchBatch.setFishingOperation(fishingOperation);
        return catchBatchService.createCatchBatch(catchBatch);
    }

    public AccidentalBatch createMinimalAccidentalBatch(FishingOperation fishingOperation) {

        setUp();

        AccidentalBatch minimalAccidentalBatch = AccidentalBatchs.newAccidentalBatch();
        minimalAccidentalBatch.setFishingOperation(fishingOperation);
        minimalAccidentalBatch.setSpecies(speciesService.getSpeciesByReferenceTaxonId(refSpeciesTaxonId()));
        minimalAccidentalBatch.setCaracteristics(new CaracteristicMap());
        return accidentalBatchService.createAccidentalBatch(minimalAccidentalBatch);
    }

}

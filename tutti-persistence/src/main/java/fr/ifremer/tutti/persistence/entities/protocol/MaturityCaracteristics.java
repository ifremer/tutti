package fr.ifremer.tutti.persistence.entities.protocol;

/*
 * #%L
 * Tutti :: Persistence
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2016 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import javax.annotation.Generated;

@Generated(value = "org.nuiton.eugene.java.SimpleJavaBeanTransformer", date = "Wed Mar 23 10:50:48 CET 2016")
public class MaturityCaracteristics extends AbstractMaturityCaracteristics {

    public static MaturityCaracteristic newMaturityCaracteristic(String id) {
        MaturityCaracteristic caracteristic = newMaturityCaracteristic();
        caracteristic.setId(id);
        return caracteristic;
    }

} //MaturityCaracteristics

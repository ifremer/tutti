package fr.ifremer.tutti.persistence.service.batch;

/*
 * #%L
 * Tutti :: Persistence
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.adagio.core.dao.data.batch.validator.CatchBatchValidationError;
import fr.ifremer.adagio.core.dao.data.batch.validator.CatchBatchValidator;
import fr.ifremer.tutti.persistence.entities.data.BatchContainer;
import fr.ifremer.tutti.persistence.entities.data.SampleCategoryModel;
import fr.ifremer.tutti.persistence.entities.data.SpeciesBatch;

import java.util.List;

/**
 * Override adagio validator to add a validate within a {@link SampleCategoryModel}.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.4
 */
public interface TuttiCatchBatchValidator extends CatchBatchValidator {

    List<CatchBatchValidationError> validateSpecies(SampleCategoryModel sampleCategoryModel,
                                                    BatchContainer<SpeciesBatch> species);

    List<CatchBatchValidationError> validateBenthos(SampleCategoryModel sampleCategoryModel,
                                                    BatchContainer<SpeciesBatch> benthos);
}

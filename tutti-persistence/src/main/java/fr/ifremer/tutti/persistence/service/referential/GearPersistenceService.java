package fr.ifremer.tutti.persistence.service.referential;

/*
 * #%L
 * Tutti :: Persistence
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.TuttiPersistenceServiceImplementor;
import fr.ifremer.tutti.persistence.entities.referential.Gear;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.List;

/**
 * Created on 11/3/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.8
 */
@Transactional(readOnly = true)
public interface GearPersistenceService extends TuttiPersistenceServiceImplementor {

    List<Gear> getAllScientificGear();

    List<Gear> getAllFishingGear();

    @Cacheable(value = "gearsWithObsoletes")
    List<Gear> getAllGearWithObsoletes();

    Gear getGear(Integer gearId);

    /**
     * Check if the temporary gear with the given {@code id} is used.
     *
     * @param id id of the gear to remove
     * @return {@code true} if gear is temporary
     * @since 3.8
     */
    boolean isTemporaryGearUsed(Integer id);

    /**
     * Add given temporary gears.
     *
     * @param gears gears to add
     * @return added gears
     * @since 3.14
     */
    @Transactional(readOnly = false)
    @CacheEvict(value = {"gears", "gearsWithObsoletes"}, allEntries = true)
    List<Gear> addTemporaryGears(List<Gear> gears);

    /**
     * Update given temporary gears.
     *
     * @param gears gears to update
     * @return updated gears
     * @since 3.14
     */
    @Transactional(readOnly = false)
    @CacheEvict(value = {"gears", "gearsWithObsoletes"}, allEntries = true)
    List<Gear> updateTemporaryGears(List<Gear> gears);

    /**
     * Link temporary gears. (Means get existing references using gears natural ids).
     *
     * @param gears gears to link
     * @return linked gears
     * @since 3.14
     */
    List<Gear> linkTemporaryGears(List<Gear> gears);

    /**
     * Replace the {@code source} gear by
     * the {@code target} one in all data.
     *
     * @param source the source gear to replace
     * @param target the target gear to use
     * @param delete flag to delete the temporary vessel after replacement
     * @since 3.6
     */
    @Transactional(readOnly = false)
    @CacheEvict(value = {"fr.ifremer.adagio.core.dao.data.batch.CatchBatchCache", "gears", "gearsWithObsoletes"}, allEntries = true)
    void replaceGear(Gear source, Gear target, boolean delete);

    /**
     * Delete the temporary gear with the given {@code id}.
     *
     * @param id id of the gear to remove
     * @since 3.8
     */
    @Transactional(readOnly = false)
    @CacheEvict(value = {"gears", "gearsWithObsoletes"}, allEntries = true)
    void deleteTemporaryGear(Integer id);

    /**
     * Delete the temporary gears with the given {@code idss}.
     *
     * @param ids ids of the gears to remove
     * @since 3.8
     */
    @Transactional(readOnly = false)
    @CacheEvict(value = {"gears", "gearsWithObsoletes"}, allEntries = true)
    void deleteTemporaryGears(Collection<Integer> ids);

}

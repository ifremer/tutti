package fr.ifremer.tutti.persistence.entities.referential;

/*
 * #%L
 * Tutti :: Persistence
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import javax.annotation.Generated;

@Generated(value = "org.nuiton.eugene.java.SimpleJavaBeanTransformer", date = "Wed Oct 02 14:29:49 CEST 2013")
public interface GearWithOriginalRankOrder extends Gear {

    String PROPERTY_ORIGINAL_RANK_ORDER = "originalRankOrder";

    Short getOriginalRankOrder();

    void setOriginalRankOrder(Short originalRankOrder);

} //GearWithOriginalRankOrder

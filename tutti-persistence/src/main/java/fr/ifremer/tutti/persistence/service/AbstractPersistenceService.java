package fr.ifremer.tutti.persistence.service;

/*
 * #%L
 * Tutti :: Persistence
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import fr.ifremer.tutti.TuttiConfiguration;
import fr.ifremer.tutti.persistence.TuttiPersistenceServiceImplementor;
import fr.ifremer.tutti.persistence.entities.data.SampleCategoryModel;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.type.Type;

import javax.annotation.Resource;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Iterator;

/**
 * TODO
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 0.3
 */
public abstract class AbstractPersistenceService implements TuttiPersistenceServiceImplementor {

    /** Logger. */
    private static final Log log = LogFactory.getLog(AbstractPersistenceService.class);

    /**
     * Session factory.
     *
     * @since 0.3
     */
    @Resource
    protected SessionFactory sessionFactory;

    @Resource(name = "tuttiConfiguration")
    protected TuttiConfiguration config;

    /**
     * Etat pour savoir si le service a ete initialise.
     */
    private boolean init;

    public void lazyInit() {

        if (!init) {

            try {

                init();

            } finally {

                init = true;

            }

        }

    }

    @Override
    public void init() {
        // do nothing by default
    }

    @Override
    public void close() {
        // do nothing by default
        init = false;
    }

    public void setConfig(TuttiConfiguration config) {
        this.config = config;
    }

    protected final SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    protected final Session getCurrentSession() {
        return getSessionFactory().getCurrentSession();
    }

    protected Object[] queryUnique(String queryName, Object... params) {

        Query query = createQuery(queryName, params);
        Object result = query.uniqueResult();
        return (Object[]) result;
    }

    protected <T> T queryUniqueTyped(String queryName, Object... params) {

        Query query = createQuery(queryName, params);
        Object result = query.uniqueResult();
        return (T) result;
    }

    protected Iterator<Object[]> queryList(String queryName, Object... params) {

        Query query = createQuery(queryName, params);
        return query.iterate();
    }

    protected <T> Iterator<T> queryListTyped(String queryName, Object... params) {

        Query query = createQuery(queryName, params);
        return query.iterate();
    }

//    protected <T> List<T> queryList2Typed(String queryName, Object... params) {
//
//        Query query = createQuery(queryName, params);
//        return query.list();
//    }

    protected Query createQuery(String queryName, Object... params) {
        Query query = getCurrentSession().getNamedQuery(queryName);

        if (params.length > 0) {

            Preconditions.checkArgument(
                    params.length % 3 == 0,
                    "Params must be tuple (paramName, paramType, paramValue)");

            int nbParams = params.length / 3;

            for (int i = 0; i < nbParams; i++) {
                String paramName = (String) params[3 * i];
                Type paramType = (Type) params[3 * i + 1];
                Object paramValue = params[3 * i + 2];

                if (paramValue != null && Collection.class.isAssignableFrom(paramValue.getClass())) {
                    query.setParameterList(paramName, (Collection) paramValue, paramType);
                } else {
                    query.setParameter(paramName, paramValue, paramType);
                }
                if (log.isDebugEnabled()) {
                    log.debug("query [" + queryName + "] (param " + i
                            + " [" + paramName + '=' + paramValue + "])");
                }
            }
        }
        return query;
    }

    protected <T extends Serializable> T load(Class<? extends T> clazz, Serializable id) {
        // TODO BLA : ajouter une annotation qui rejoue une méthode
        // avec debug=true. puis repasse à faux,
        // lorsqu'une erreur
//        boolean debug = false;
//        if (debug) {
//            T load = (T) getCurrentSession().get(clazz, id);
//            if (load == null) {
//                throw new DataIntegrityViolationException("Unable to load entity " + clazz.getName() + " with id=" + id + " : not found in database.");
//            }
//        }
        return (T) getCurrentSession().load(clazz, id);
    }

    protected <T extends Serializable> T get(Class<? extends T> clazz, Serializable id) {
        return (T) getCurrentSession().get(clazz, id);
    }

    protected int queryUpdate(String queryName, Object... params) {

        Query query = createQuery(queryName, params);

        int result = query.executeUpdate();
        if (log.isInfoEnabled()) {
            log.info(queryName + ": " + result);
        }
        return result;
    }

    protected Date newCreateDate() {
        return dateWithNoTime(new Date());
    }

    protected Date dateWithNoTime(Date date) {
        calendar.setTime(date);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        return calendar.getTime();
    }

    protected Date dateWithNoMiliSecond(Date date) {
        calendar.setTime(date);
        calendar.set(Calendar.MILLISECOND, 0);
        return calendar.getTime();
    }

    protected Date dateWithNoSecondAndMiliSecond(Date date) {
        calendar.setTime(date);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        return calendar.getTime();
    }

    protected Date dateWithNoSecondAndOneMiliSecond(Date date) {
        calendar.setTime(date);
        calendar.add(Calendar.SECOND, 0);
        calendar.add(Calendar.MILLISECOND, 1);
        return calendar.getTime();
    }

//    protected Date dateWithOneMiliSecond(Date date) {
//        calendar.setTime(date);
//        calendar.add(Calendar.MILLISECOND, 1);
//        return calendar.getTime();
//    }
//
//    protected Date dateOfYearWithOneMiliSecond(int year) {
//        calendar.setTimeInMillis(0);
//        calendar.set(Calendar.YEAR, year);
//        calendar.set(Calendar.MILLISECOND, 1);
//        return calendar.getTime();
//    }
//
//    protected long dateOfYearWithOneMiliSecondInMili(int year) {
//        calendar.setTimeInMillis(0);
//        calendar.set(Calendar.YEAR, year);
//        calendar.set(Calendar.MILLISECOND, 1);
//        return calendar.getTimeInMillis();
//    }

    /**
     * Test if the date has millisecond set. This yes, return null, then return the date itself.
     *
     * @param databaseValue the date stored in the database (could be fake date, not null only because of database constraints)
     * @return null if the date is a fake date
     */
    protected Date convertDatabase2UI(Timestamp databaseValue) {
        Date result;
        if (databaseValue == null) {
            result = null;
        } else {

            calendar.setTimeInMillis(databaseValue.getTime());
            if (calendar.get(Calendar.MILLISECOND) != 0) {
                result = null;
            } else {
                result = calendar.getTime();
            }
        }
        return result;
    }

    /**
     * Convert a UI date, when the database value is mandatory.
     * If the given value is null, use the default date, then set millisecond to '1', to be able to retrieve the null value later.
     *
     * @param uiValue                   the date used in the UI
     * @param addOneSecondToDefaultDate TODO
     * @param defaultNotEmptyDate       TODO
     * @return null if the date is a fake date
     */
    protected Date convertUI2DatabaseMandatoryDate(Date uiValue,
                                                   Date defaultNotEmptyDate,
                                                   boolean addOneSecondToDefaultDate) {
        Date result;

        // if ui date is not empty, then use it (but reset millisecond)
        if (uiValue == null) {

            Preconditions.checkState(
                    defaultNotEmptyDate != null,
                    "'defaultNotEmptyDate' could not be null.");


            calendar.setTime(defaultNotEmptyDate);
            if (addOneSecondToDefaultDate) {
                calendar.add(Calendar.SECOND, 1);
            }
            calendar.set(Calendar.MILLISECOND, 1);
            result = calendar.getTime();
        } else {

            result = dateWithNoMiliSecond(uiValue);
        }

        return result;
    }


    public SampleCategoryModel getSampleCategoryModel() {
        return config.getSampleCategoryModel();
    }

    private Calendar calendar = new GregorianCalendar();


}

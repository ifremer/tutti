package fr.ifremer.tutti.persistence.entities.referential;

/*
 * #%L
 * Tutti :: Persistence
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.TuttiPersistence;
import fr.ifremer.tutti.persistence.entities.protocol.SpeciesProtocol;
import fr.ifremer.tutti.persistence.entities.protocol.TuttiProtocol;
import fr.ifremer.tutti.persistence.entities.protocol.TuttiProtocols;

import java.util.HashMap;
import java.util.Map;

/**
 * Created on 2/17/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since XXX
 */
public class TaxonCaches {

    public static TaxonCache createSpeciesCacheWithoutVernacularCode(TuttiPersistence persistenceService, TuttiProtocol protocol) {

        boolean noProtocol = protocol == null;

        Map<Integer, SpeciesProtocol> protocolMap;

        if (noProtocol) {

            protocolMap = new HashMap<>();

        } else {

            protocolMap = TuttiProtocols.toSpeciesProtocolMap(protocol);

        }


        return new TaxonCache(false, persistenceService, protocolMap);

    }

    public static TaxonCache createSpeciesCache(TuttiPersistence persistenceService, TuttiProtocol protocol) {

        boolean noProtocol = protocol == null;

        Map<Integer, SpeciesProtocol> protocolMap;

        if (noProtocol) {

            protocolMap = new HashMap<>();

        } else {

            protocolMap = TuttiProtocols.toSpeciesProtocolMap(protocol);

        }

        return new TaxonCache(true, persistenceService, protocolMap);

    }

    public static TaxonCache createBenthosCacheWithoutVernacularCode(TuttiPersistence persistenceService, TuttiProtocol protocol) {

        boolean noProtocol = protocol == null;

        Map<Integer, SpeciesProtocol> protocolMap;

        if (noProtocol) {

            protocolMap = new HashMap<>();

        } else {

            protocolMap = TuttiProtocols.toBenthosProtocolMap(protocol);

        }


        return new TaxonCache(false, persistenceService, protocolMap);

    }

    public static TaxonCache createBenthosCache(TuttiPersistence persistenceService, TuttiProtocol protocol) {

        boolean noProtocol = protocol == null;

        Map<Integer, SpeciesProtocol> protocolMap;

        if (noProtocol) {

            protocolMap = new HashMap<>();

        } else {

            protocolMap = TuttiProtocols.toBenthosProtocolMap(protocol);

        }

        return new TaxonCache(true, persistenceService, protocolMap);

    }

}

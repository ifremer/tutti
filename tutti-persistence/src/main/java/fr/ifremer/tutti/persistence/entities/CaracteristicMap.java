package fr.ifremer.tutti.persistence.entities;

/*
 * #%L
 * Tutti :: Persistence
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.entities.referential.Caracteristic;
import fr.ifremer.tutti.persistence.entities.referential.CaracteristicQualitativeValue;
import fr.ifremer.tutti.persistence.entities.referential.CaracteristicType;

import java.io.Serializable;
import java.util.Collection;
import java.util.LinkedHashMap;

/**
 * A map (key are {@link Caracteristic}, values are values of caracteristics).
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 0.3
 */
public class CaracteristicMap extends LinkedHashMap<Caracteristic, Serializable> {

    private static final long serialVersionUID = 1L;

    public static CaracteristicMap copy(CaracteristicMap map) {
        CaracteristicMap result = new CaracteristicMap();
        if (map != null) {
            result.putAll(map);
        }
        return result;
    }

    public static CaracteristicMap fromCollection(Collection<Caracteristic> caracterstics) {
        CaracteristicMap result = new CaracteristicMap();
        if (caracterstics != null) {
            caracterstics.forEach(caracteristic -> result.put(caracteristic, null));
        }
        return result;
    }

    public boolean hasNonNullValues(Collection<Caracteristic> caracteristicsToIgnore) {
        return keySet().stream()
                       .filter(caracteristic -> !caracteristicsToIgnore.contains(caracteristic) && get(caracteristic) != null)
                       .count() > 0;
    }

    public CaracteristicQualitativeValue getQualitativeValue(Caracteristic caracteristic) {
        Serializable value = get(caracteristic);
        if (value != null && !(value instanceof CaracteristicQualitativeValue)) {
            throw new IllegalArgumentException("caracteristic value for " + caracteristic + " is not qualitative: " + value);
        }
        return (CaracteristicQualitativeValue) value;
    }

    public String getStringValue(Caracteristic caracteristic) {
        Serializable value = get(caracteristic);
        if (value != null && !(value instanceof String)) {
            throw new IllegalArgumentException("caracteristic value for " + caracteristic + " is not text: " + value);
        }
        return (String) value;
    }

    public Float getFloatValue(Caracteristic caracteristic) {
        Serializable value = get(caracteristic);
        if (value != null && !(value instanceof Float)) {
            throw new IllegalArgumentException("caracteristic value for " + caracteristic + " is not float: " + value);
        }
        return (Float) value;
    }

    public CaracteristicQualitativeValue removeQualitativeValue(Caracteristic caracteristic) {
        Serializable remove = remove(caracteristic);
        if (remove != null && !(remove instanceof CaracteristicQualitativeValue)) {
            throw new IllegalArgumentException("caracteristic value for " + caracteristic + " is not qualitative: " + remove);
        }
        return (CaracteristicQualitativeValue) remove;
    }

    public String removeStringValue(Caracteristic caracteristic) {
        Serializable remove = remove(caracteristic);
        if (remove != null && !(remove instanceof String)) {
            throw new IllegalArgumentException("caracteristic value for " + caracteristic + " is not text: " + remove);
        }
        return (String) remove;
    }

    public Float removeFloatValue(Caracteristic caracteristic) {
        Serializable remove = remove(caracteristic);
        if (remove != null && !(remove instanceof Float)) {
            throw new IllegalArgumentException("caracteristic value for " + caracteristic + " is not float: " + remove);
        }
        return (Float) remove;
    }

    @Override
    public Serializable put(Caracteristic key, Serializable value) {

        if (value != null) {
            CaracteristicType caracteristicType = key.getCaracteristicType();
            switch (caracteristicType) {

                case NUMBER:
                    if (!(value instanceof Number)) {
                        throw new IllegalArgumentException("caracteristic value for " + key + " is not a number: " + value);
                    }
                    break;
                case QUALITATIVE:

                    // check value is a qualitative value
                    if (!(value instanceof CaracteristicQualitativeValue)) {
                        throw new IllegalArgumentException("caracteristic value for " + key + " is not qualitative: " + value);
                    }
                    break;
                case TEXT:
                    if (!(value instanceof String)) {
                        throw new IllegalArgumentException("caracteristic value for " + key + " is not text: " + value);
                    }
                    break;
            }
        }

        return super.put(key, value);
    }
}

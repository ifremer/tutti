package fr.ifremer.tutti.persistence.service;

/*
 * #%L
 * Tutti :: Persistence
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.version.Version;

/**
 * Created on 1/12/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.12
 */
public abstract class UpdateSchemaContextSupport {

    /** Logger. */
    private static final Log log = LogFactory.getLog(UpdateSchemaContextSupport.class);

    private Version schemaVersion;

    private Version schemaVersionIfUpdate;

    private boolean willUpdate;

    public Version getSchemaVersion() {
        return schemaVersion;
    }

    public Version getSchemaVersionIfUpdate() {
        return schemaVersionIfUpdate;
    }

    public boolean isNeedUpdate() {
        return schemaVersion.compareTo(schemaVersionIfUpdate) < 0;
    }

    public boolean isSchemaVersionTooHigh() {
        return schemaVersion.compareTo(schemaVersionIfUpdate) > 0;
    }

    public boolean isWillUpdate() {
        return willUpdate;
    }

    protected abstract boolean askUserToMigrate(Version schemaVersion, Version schemaVersionIfUpdate);

    public void init(Version schemaVersion, Version schemaVersionIfUpdate) {

        this.schemaVersion = schemaVersion;

        if (log.isInfoEnabled()) {
            log.info(String.format("Detected schema version: %s", schemaVersion));
        }

        this.schemaVersionIfUpdate = schemaVersionIfUpdate;

        if (log.isInfoEnabled()) {
            log.info(String.format("Detected schema version if update: %s", schemaVersionIfUpdate));
        }

        if (isSchemaVersionTooHigh()) {

            if (log.isInfoEnabled()) {
                log.info(String.format("Schema version %s is higher than incoming schema version %s, can't update!", schemaVersion, schemaVersionIfUpdate));
            }

        } else if (isNeedUpdate()) {

            if (log.isInfoEnabled()) {
                log.info(String.format("Schema version %s is smaller than incoming schema version %s, ask to update", schemaVersion, schemaVersionIfUpdate));
            }
            willUpdate = askUserToMigrate(schemaVersion, schemaVersionIfUpdate);

            if (log.isInfoEnabled()) {
                log.info("Should we update? " + willUpdate);
            }

        } else {

            if (log.isInfoEnabled()) {
                log.info(String.format("Schema version %s is up-to-date, no need to update.", schemaVersion));
            }

        }

    }

}

package fr.ifremer.tutti.persistence.service;

/*
 * #%L
 * Tutti :: Persistence
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.TuttiPersistenceServiceImplementor;
import fr.ifremer.tutti.persistence.entities.CaracteristicMap;
import fr.ifremer.tutti.persistence.entities.data.Cruise;
import fr.ifremer.tutti.persistence.entities.referential.Gear;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.List;

/**
 * CRUD of {@link Cruise} entity.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 0.3
 */
@Transactional(readOnly = true)
public interface CruisePersistenceService extends TuttiPersistenceServiceImplementor {

    List<Integer> getAllCruiseId(String programId);

    List<Cruise> getAllCruise(String programId);

    Cruise getCruise(Integer id);

    @Transactional(readOnly = false)
    Cruise createCruise(Cruise bean);

    @Transactional(readOnly = false)
    Cruise saveCruise(Cruise bean, boolean updateVessel, boolean updateGear);

    @Transactional(readOnly = false)
    void setCruiseReadyToSynch(Integer cruiseId);

    CaracteristicMap getGearCaracteristics(Integer cruiseId, Integer gearId, short rankOrder);

    @Transactional(readOnly = false)
    void saveGearCaracteristics(Gear gear, Cruise cruise);

    boolean isOperationUseGears(Integer cruiseId, Collection<Gear> gears);
}

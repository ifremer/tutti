package fr.ifremer.tutti.persistence.service;

/*
 * #%L
 * Tutti :: Persistence
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import fr.ifremer.adagio.core.dao.referential.StatusCode;
import fr.ifremer.adagio.core.dao.technical.DatabaseSchemaDao;
import fr.ifremer.adagio.core.dao.technical.DatabaseSchemaUpdateException;
import fr.ifremer.adagio.core.dao.technical.VersionNotFoundException;
import fr.ifremer.adagio.core.dao.technical.hibernate.TemporaryDataHelper;
import fr.ifremer.adagio.core.service.technical.CacheService;
import fr.ifremer.adagio.core.service.technical.sanity.DatabaseSanityService;
import fr.ifremer.tutti.persistence.entities.referential.TuttiReferentialEntity;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.jaxx.application.ApplicationTechnicalException;
import org.nuiton.version.Version;
import org.nuiton.version.Versions;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.concurrent.Callable;

/**
 * Created on 4/20/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.5
 */
@Service("technicalPersistenceService")
public class TechnicalPersistenceServiceImpl extends AbstractPersistenceService implements TechnicalPersistenceService {

    /** Logger. */
    private static final Log log = LogFactory.getLog(TechnicalPersistenceServiceImpl.class);

    @Resource(name = "databaseSanityService")
    protected DatabaseSanityService databaseSanityService;

    @Resource(name = "cacheService")
    protected CacheService cacheService;

    @Resource(name = "databaseSchemaDao")
    protected DatabaseSchemaDao databaseSchemaDao;

    @Override
    public <V> V invoke(Callable<V> call) {

        try {
            return call.call();
        } catch (RuntimeException e) {
            throw e;
        } catch (Exception e) {
            throw new ApplicationTechnicalException(e);
        }
    }

    @Override
    public Version getSchemaVersion() {
        Object version;
        try {
            version = databaseSchemaDao.getSchemaVersion().getVersion();
        } catch (VersionNotFoundException e) {
            if (log.isErrorEnabled()) {
                log.error("Could not find db version", e);
            }
            version = null;
        }
        return version == null ? null : Versions.valueOf(version.toString());
    }

    @Override
    public Version getSchemaVersionIfUpdate() {
        Object version = databaseSchemaDao.getSchemaVersionIfUpdate();
        return Versions.valueOf(version.toString());
    }

    @Override
    public <U extends UpdateSchemaContextSupport> void prepareUpdateSchemaContext(U context) {
        context.init(getSchemaVersion(), getSchemaVersionIfUpdate());
    }

    @Override
    public void updateSchema() {
        try {
            databaseSchemaDao.updateSchema();
        } catch (DatabaseSchemaUpdateException e) {
            throw new ApplicationTechnicalException(e.getCause());
        }
    }

    @Override
    public void sanityDb() {
        databaseSanityService.sanity();
    }

    @Override
    public void clearAllCaches() {
        cacheService.clearAllCaches();
    }

    @Override
    public boolean isTemporary(TuttiReferentialEntity entity) {
        Preconditions.checkNotNull(entity);
        Preconditions.checkNotNull(entity.getId());
        Preconditions.checkNotNull(entity.getStatus());

        return StatusCode.TEMPORARY.getValue().equals(entity.getStatus().getId()) &&
               (entity.getIdAsInt() != null && entity.getIdAsInt() < 0
                || entity.getId() != null && entity.getId().startsWith(TemporaryDataHelper.TEMPORARY_NAME_PREFIX));
    }

}

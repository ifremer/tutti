package fr.ifremer.tutti.persistence.test;

/*
 * #%L
 * Tutti :: Persistence
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.junit.runner.notification.RunNotifier;
import org.junit.runners.BlockJUnit4ClassRunner;
import org.junit.runners.model.FrameworkMethod;
import org.junit.runners.model.InitializationError;

/**
 * To use {@link TuttiRunListener}.
 *
 * Add  this on top of your test (when run in IDE, otherise maven already add
 * the listener for you, so no need of the runner).
 * <pre>
 *     \@RunWith(TuttiRunner.class)
 * </pre>
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.0.2
 */

public class TuttiRunner extends BlockJUnit4ClassRunner {

    public static final TuttiRunListener TUTTI_RUN_LISTENER = new TuttiRunListener();

    /**
     * Creates a BlockJUnit4ClassRunner to run {@code klass}
     *
     * @param klass the test class
     * @throws InitializationError if the test class is malformed.
     */
    public TuttiRunner(Class<?> klass) throws InitializationError {
        super(klass);
    }

    @Override
    protected void runChild(FrameworkMethod method, RunNotifier notifier) {
        notifier.removeListener(TUTTI_RUN_LISTENER);
        notifier.addListener(TUTTI_RUN_LISTENER);
        super.runChild(method, notifier);
    }
}

package fr.ifremer.tutti.persistence.service.util;

/*
 * #%L
 * Tutti :: Persistence
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.adagio.core.dao.data.batch.Batch;
import fr.ifremer.adagio.core.dao.data.batch.CatchBatch;
import fr.ifremer.adagio.core.dao.data.batch.CatchBatchImpl;
import fr.ifremer.adagio.core.dao.data.sample.Sample;
import fr.ifremer.adagio.core.dao.data.survey.fishingTrip.FishingTrip;
import fr.ifremer.adagio.core.dao.data.survey.scientificCruise.ScientificCruise;
import fr.ifremer.adagio.core.dao.technical.synchronization.SynchronizationStatus;
import fr.ifremer.tutti.persistence.entities.data.AccidentalBatch;
import fr.ifremer.tutti.persistence.entities.data.IndividualObservationBatch;
import fr.ifremer.tutti.persistence.service.AbstractPersistenceService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.LazyInitializationException;
import org.hibernate.type.IntegerType;
import org.hibernate.type.StringType;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Component;

/**
 * Helper around SynchronizationStatus.
 *
 * Created on 4/22/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.5
 */
@Component("synchronizationStatusHelper")
public class SynchronizationStatusHelper extends AbstractPersistenceService {

    /** Logger. */
    private static final Log log = LogFactory.getLog(SynchronizationStatusHelper.class);

    /**
     * A internal state to skip any propagation to parent objects.
     *
     * This is used while generic format import.
     *
     * @since 3.15
     */
    private static boolean propagateToParents = true;

    public static void propagateDirtyStatusToParents() {
        SynchronizationStatusHelper.propagateToParents = true;
    }

    public static void doNotPropagateDirtyStatusToParents() {
        SynchronizationStatusHelper.propagateToParents = false;
    }

    public void setDirty(ScientificCruise scientificCruise) {

        String synchronizationStatus = getDirtyValue();
        if (log.isInfoEnabled()) {
            log.info("SetDirty scientificCruise: " + scientificCruise.getId());
        }
        scientificCruise.setSynchronizationStatus(synchronizationStatus);

    }

    public void setDirty(FishingTrip fishingTrip) {

        String synchronizationStatus = getDirtyValue();
        fishingTrip.setSynchronizationStatus(synchronizationStatus);
        if (log.isInfoEnabled()) {
            log.info("SetDirty fishingTrip: " + fishingTrip.getId());
        }

        if (propagateToParents) {
            setDirty(fishingTrip.getScientificCruise());
        }

    }

    public void setDirty(CatchBatch catchBatch) {

        String synchronizationStatus = getDirtyValue();

        if (log.isInfoEnabled()) {
            log.info("SetDirty catchBatch: " + catchBatch.getId());
        }
        catchBatch.setSynchronizationStatus(synchronizationStatus);

        if (propagateToParents) {
            try {
                setDirty(catchBatch.getFishingOperation().getFishingTrip());
            } catch (LazyInitializationException e) {

                // We need to have a loaded catchBatch, since it can come from a cache, prefer to reload it here
                CatchBatchImpl loadedCatchBatch = load(CatchBatchImpl.class, catchBatch.getId());
                loadedCatchBatch.setSynchronizationStatus(synchronizationStatus);
            }
        }

    }

    public void setDirty(Sample sample) {

        String synchronizationStatus = getDirtyValue();
        sample.setSynchronizationStatus(synchronizationStatus);
        if (log.isInfoEnabled()) {
            log.info("SetDirty sample: " + sample.getId());
        }

        if (propagateToParents) {
            Batch batch = sample.getBatch();
            if (batch != null && batch instanceof CatchBatch) {
                setDirty((CatchBatch) batch);
            } else if (sample.getFishingOperation() != null) {
                setDirty(sample.getFishingOperation().getFishingTrip());
            }
        }

    }

    public void setDirty(fr.ifremer.tutti.persistence.entities.data.CatchBatch bean) {
        bean.setSynchronizationStatus(getDirtyValue());
    }

    public void setDirty(IndividualObservationBatch bean) {
        bean.setSynchronizationStatus(getDirtyValue());
    }

    public void setDirty(AccidentalBatch bean) {
        bean.setSynchronizationStatus(getDirtyValue());
    }

    public void setCruiseReadyToSynch(Integer cruiseId) {

        String oldStatus = getDirtyValue();
        String newStatus = SynchronizationStatus.READY_TO_SYNCHRONIZE.getValue();

        if (log.isInfoEnabled()) {
            log.info("setCruiseReadyToSynch cruise: " + cruiseId);
        }

        // update scientificCruise
        int scientificCruiseQueryUpdate = queryUpdate("updateScientificCruiseSynchronizationStatus",
                                                      "cruiseId", IntegerType.INSTANCE, cruiseId,
                                                      "oldStatus", StringType.INSTANCE, oldStatus,
                                                      "newStatus", StringType.INSTANCE, newStatus);

        if (scientificCruiseQueryUpdate == 0) {
            throw new DataIntegrityViolationException("Could not attach update scientific cruise, was not found.");
        }

        // update fishingTrip
        int fishingTripQueryUpdate = queryUpdate("updateFishingTripSynchronizationStatus",
                                                 "cruiseId", IntegerType.INSTANCE, cruiseId,
                                                 "oldStatus", StringType.INSTANCE, oldStatus,
                                                 "newStatus", StringType.INSTANCE, newStatus);

        if (log.isInfoEnabled()) {
            log.info("Nb fishingTrip  updated: " + fishingTripQueryUpdate);
        }

        // update catchBatch
        int catchBatchQueryUpdate = queryUpdate("updateCatchBatchSynchronizationStatus",
                                                "cruiseId", IntegerType.INSTANCE, cruiseId,
                                                "oldStatus", StringType.INSTANCE, oldStatus,
                                                "newStatus", StringType.INSTANCE, newStatus);
        if (log.isInfoEnabled()) {
            log.info("Nb catchBatch  updated: " + catchBatchQueryUpdate);
        }

        // update sample
        int sampleQueryUpdate = queryUpdate("updateSampleSynchronizationStatus",
                                            "cruiseId", IntegerType.INSTANCE, cruiseId,
                                            "oldStatus", StringType.INSTANCE, oldStatus,
                                            "newStatus", StringType.INSTANCE, newStatus);

        if (log.isInfoEnabled()) {
            log.info("Nb sample updated: " + sampleQueryUpdate);
        }
    }

    protected String getDirtyValue() {
        return SynchronizationStatus.DIRTY.getValue();
    }
}

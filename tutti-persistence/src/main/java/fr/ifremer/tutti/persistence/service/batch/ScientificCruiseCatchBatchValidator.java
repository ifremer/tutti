package fr.ifremer.tutti.persistence.service.batch;

/*
 * #%L
 * Tutti :: Persistence
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Lists;
import fr.ifremer.adagio.core.dao.data.batch.CatchBatch;
import fr.ifremer.adagio.core.dao.data.batch.SortingBatch;
import fr.ifremer.adagio.core.dao.data.batch.validator.CatchBatchValidationError;
import fr.ifremer.tutti.persistence.entities.data.BatchContainer;
import fr.ifremer.tutti.persistence.entities.data.SampleCategoryModel;
import fr.ifremer.tutti.persistence.entities.data.SampleCategoryModelEntry;
import fr.ifremer.tutti.persistence.entities.data.SpeciesBatch;
import fr.ifremer.tutti.persistence.service.util.tree.BatchTreeHelper;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

import static org.nuiton.i18n.I18n.n;
import static org.nuiton.i18n.I18n.t;

@Component(value = "scientificCruiseCatchBatchValidator")
public class ScientificCruiseCatchBatchValidator implements TuttiCatchBatchValidator {

    /** Logger. */
    private static final Log log =
            LogFactory.getLog(ScientificCruiseCatchBatchValidator.class);

    @Resource(name = "batchTreeHelper")
    protected BatchTreeHelper batchTreeHelper;

    // -----------------------------------------------------------------------//
    // -- CatchBatchValidator methods                                       --//
    // -----------------------------------------------------------------------//

    @Override
    public boolean isEnable(CatchBatch catchBatch) {
        // Apply validation only on catch batch for fishingOperation
        return (catchBatch.getFishingOperation() != null);
    }

    @Override
    public List<CatchBatchValidationError> validate(CatchBatch catchBatch) {
        List<CatchBatchValidationError> errors = Lists.newArrayList();
        validateCatchBatch(catchBatch, errors);
        return errors;
    }

    // -----------------------------------------------------------------------//
    // -- TuttiCatchBatchValidator methods                                  --//
    // -----------------------------------------------------------------------//

    @Override
    public List<CatchBatchValidationError> validateSpecies(SampleCategoryModel sampleCategoryModel,
                                                           BatchContainer<SpeciesBatch> species) {

        List<CatchBatchValidationError> errors = Lists.newArrayList();

        Map<Integer, SampleCategoryModelEntry> categoriesById =
                sampleCategoryModel.getCategoryMap();

        for (SpeciesBatch speciesBatch : species.getChildren()) {

            // check all sample categories are accepted
            validateSampleCategoriesUniverse(
                    sampleCategoryModel,
                    errors,
                    speciesBatch,
                    n("tutti.persistence.batch.validation.invalid.species.sampleCategoryId"));


            if (errors.isEmpty()) {

                // check sample categories order is ok

                validateSampleCategoriesOrder(
                        categoriesById,
                        errors,
                        speciesBatch,
                        null,
                        n("tutti.persistence.batch.validation.invalid.species.sampleCategoryId.order"));
            }
        }
        return errors;
    }

    @Override
    public List<CatchBatchValidationError> validateBenthos(SampleCategoryModel sampleCategoryModel,
                                                           BatchContainer<SpeciesBatch> benthos) {

        List<CatchBatchValidationError> errors = Lists.newArrayList();

        Map<Integer, SampleCategoryModelEntry> categoriesById =
                sampleCategoryModel.getCategoryMap();

        for (SpeciesBatch benthosBatch : benthos.getChildren()) {

            // check all sample categories are accepted
            validateSampleCategoriesUniverse(
                    sampleCategoryModel,
                    errors,
                    benthosBatch,
                    n("tutti.persistence.batch.validation.invalid.benthos.sampleCategoryId"));

            if (errors.isEmpty()) {

                // check sample categories order is ok

                validateSampleCategoriesOrder(
                        categoriesById,
                        errors,
                        benthosBatch,
                        null,
                        n("tutti.persistence.batch.validation.invalid.benthos.sampleCategoryId.order"));
            }
        }
        return errors;
    }

    // -----------------------------------------------------------------------//
    // -- Internal methods                                                  --//
    // -----------------------------------------------------------------------//

    protected void validateCatchBatch(CatchBatch batch,
                                      List<CatchBatchValidationError> errors) {

        // -- Vrac
        SortingBatch vracBatch = batchTreeHelper.getVracBatch(batch);

        if (log.isDebugEnabled()) {
            log.debug("Try to validate Vrac batch model " + vracBatch);
        }

        if (vracBatch == null) {
            addError(errors, n("tutti.persistence.batch.validation.vracNotFound"));
        } else {
            // -- Vrac > Species
            SortingBatch speciesBatch = batchTreeHelper.getSpeciesVracRootBatch(vracBatch);

            // -- Vrac > Benthos
            SortingBatch benthosBatch = batchTreeHelper.getBenthosVracRootBatch(vracBatch);

            if (speciesBatch == null) {

                if (benthosBatch == null) {
                    addError(errors, n("tutti.persistence.batch.validation.vracSpeciesNotFound"));
                } else {
                    addWarning(errors, n("tutti.persistence.batch.validation.vracSpeciesNotFound"));
                }
            } else {
                // -- Vrac > Species > Alive not itemized
                SortingBatch livingNotItemizedBatch = batchTreeHelper.getSpeciesVracAliveNotItemizedRootBatch(speciesBatch);
                if (livingNotItemizedBatch == null) {
                    addWarning(errors, n("tutti.persistence.batch.validation.vracSpeciesLifeNotFound"));
                }

                // -- Vrac > Species > Inert
                SortingBatch inertBatch = batchTreeHelper.getSpeciesVracInertRootBatch(speciesBatch);
                if (inertBatch == null) {
                    addWarning(errors, n("tutti.persistence.batch.validation.vracSpeciesInertNotFound"));
                }

                // -- Vrac > Species > Alive itemized
                SortingBatch aliveItemizedBatch = batchTreeHelper.getSpeciesVracAliveItemizedRootBatch(speciesBatch);
                if (aliveItemizedBatch == null) {
                    addWarning(errors, n("tutti.persistence.batch.validation.vracSpeciesAliveItemizedNotFound"));
                }

                // TODO verifier que les espèces en haut de grappe ont bien la catégorie voulue (sorted-unsorted)
            }

            if (benthosBatch == null) {
                if (speciesBatch == null) {
                    addError(errors, n("tutti.persistence.batch.validation.vracBenthosNotFound"));
                } else {
                    addWarning(errors, n("tutti.persistence.batch.validation.vracBenthosNotFound"));
                }
            } else {

                // -- Vrac > Benthos > Alive not itemized
                SortingBatch livingNotItemizedBatch = batchTreeHelper.getBenthosVracAliveNotItemizedRootBatch(benthosBatch);
                if (livingNotItemizedBatch == null) {
                    addWarning(errors, n("tutti.persistence.batch.validation.vracBenthosLifeNotFound"));
                }

                // -- Vrac > Benthos > Inert
                SortingBatch inertBatch = batchTreeHelper.getBenthosVracInertRootBatch(benthosBatch);
                if (inertBatch == null) {
                    addWarning(errors, n("tutti.persistence.batch.validation.vracBenthosInertNotFound"));
                }

                // -- Vrac > Benthos > Alive itemized
                SortingBatch aliveItemizedBatch = batchTreeHelper.getBenthosVracAliveItemizedRootBatch(benthosBatch);
                if (aliveItemizedBatch == null) {
                    addWarning(errors, n("tutti.persistence.batch.validation.vracBenthosAliveItemizedNotFound"));
                }
            }
        }

        // -- Hors Vrac
        SortingBatch horsVracBatch = batchTreeHelper.getHorsVracBatch(batch);
        if (horsVracBatch == null) {
            addWarning(errors, n("tutti.persistence.batch.validation.horsVracNotFound"));
        } else {
            // -- Hors Vrac > Species
            SortingBatch speciesBatch = batchTreeHelper.getSpeciesHorsVracRootBatch(horsVracBatch);
            if (speciesBatch == null) {
                addWarning(errors, n("tutti.persistence.batch.validation.horsVracSpeciesNotFound"));
            }

            // -- Hors Vrac > Benthos
            SortingBatch benthosBatch = batchTreeHelper.getBenthosHorsVracRootBatch(horsVracBatch);
            if (benthosBatch == null) {
                addWarning(errors, n("tutti.persistence.batch.validation.horsVracBenthosNotFound"));
            }
            // -- Hors Vrac > Marine Litter
            SortingBatch marineLitterBatch = batchTreeHelper.getMarineLitterRootBatch(horsVracBatch);

            if (marineLitterBatch == null) {
                addWarning(errors, n("tutti.persistence.batch.validation.horsVracMarineLitterNotFound"));
            }
        }

        // -- Unsorted
        SortingBatch unsortedBatch = batchTreeHelper.getRejectedBatch(batch);
        if (unsortedBatch == null) {
            addWarning(errors, n("tutti.persistence.batch.validation.unsortedNotFound"));
        }
    }

    protected void validateSampleCategoriesUniverse(SampleCategoryModel sampleCategoryModel,
                                                    List<CatchBatchValidationError> errors,
                                                    SpeciesBatch aBatch,
                                                    String messageKey) {

        Integer sampleCategoryId = aBatch.getSampleCategoryId();

        if (!sampleCategoryModel.containsCategoryId(sampleCategoryId)) {

            // invalid sample category id

            addError(errors,
                     messageKey,
                     aBatch.getId(),
                     aBatch.getSpecies().getName(),
                     sampleCategoryId);
        }

        if (!aBatch.isChildBatchsEmpty()) {

            for (SpeciesBatch speciesAbleBatch : aBatch.getChildBatchs()) {

                validateSampleCategoriesUniverse(sampleCategoryModel,
                                                 errors,
                                                 speciesAbleBatch,
                                                 messageKey);
            }
        }
    }

    protected void validateSampleCategoriesOrder(Map<Integer, SampleCategoryModelEntry> categoriesById,
                                                 List<CatchBatchValidationError> errors,
                                                 SpeciesBatch aBatch,
                                                 SampleCategoryModelEntry lastSampleCategory,
                                                 String messageKey) {

        Integer sampleCategoryId = aBatch.getSampleCategoryId();

        SampleCategoryModelEntry actualCategory =
                categoriesById.get(sampleCategoryId);

        if (lastSampleCategory != null) {

            // check the category is after the last one

            if (actualCategory.getOrder() < lastSampleCategory.getOrder()) {

                // bad order

                addError(errors,
                         messageKey,
                         aBatch.getId(),
                         aBatch.getSpecies().getName(),
                         actualCategory.getLabel(),
                         lastSampleCategory.getLabel());

                // no need to continue, we got a bad order
                return;
            }

        }

        // keep the last sample category
        lastSampleCategory = actualCategory;

        if (!aBatch.isChildBatchsEmpty()) {

            for (SpeciesBatch speciesAbleBatch : aBatch.getChildBatchs()) {

                validateSampleCategoriesOrder(
                        categoriesById,
                        errors,
                        speciesAbleBatch,
                        lastSampleCategory,
                        messageKey);
            }
        }
    }

    protected void addError(List<CatchBatchValidationError> errors,
                            String messageKey) {
        CatchBatchValidationError error = new CatchBatchValidationError(
                messageKey,
                t(messageKey),
                CatchBatchValidationError.GRAVITY_ERROR);
        errors.add(error);
    }

    protected void addError(List<CatchBatchValidationError> errors,
                            String messageKey, Object... params) {
        CatchBatchValidationError error = new CatchBatchValidationError(
                messageKey,
                t(messageKey, params),
                CatchBatchValidationError.GRAVITY_ERROR);
        errors.add(error);
    }

    protected void addWarning(List<CatchBatchValidationError> errors,
                              String messageKey) {
        CatchBatchValidationError error = new CatchBatchValidationError(
                messageKey,
                t(messageKey),
                CatchBatchValidationError.GRAVITY_WARNING);
        errors.add(error);
    }

}

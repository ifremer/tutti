package fr.ifremer.tutti.persistence.entities.referential;

/*
 * #%L
 * Tutti :: Persistence
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2016 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.MoreObjects;
import fr.ifremer.tutti.util.Numbers;

public class CaracteristicBean extends AbstractCaracteristicBean {

    private static final long serialVersionUID = 1L;

    public static final Float DEFAULT_PRECISION = 0.1f;

    @Override
    public Integer getLengthStepInMm(Float lengthStep) {
        Integer lengthClass;
        if (lengthStep == null) {
            lengthClass = null;
        } else {
            int intValue = (int) (lengthStep * 10);
            int intStep = (int) (getPrecision() * 10);
            int correctIntStep = intValue - (intValue % intStep);
            lengthClass = Numbers.convertToMm(correctIntStep / 10f, getUnit());
        }
        return lengthClass;
    }

    @Override
    public Float getPrecision() {
        Float result = super.getPrecision();
        if (result == null) {
            // on ne met pas 1 c'est la valeur par défaut
            result = DEFAULT_PRECISION;
        }
        return result;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(Caracteristic.class)
                          .add(PROPERTY_ID, getId())
                          .add(PROPERTY_PARAMETER_NAME, getParameterName())
                          .add(PROPERTY_METHOD_NAME, getMethodName())
                          .add(PROPERTY_FRACTION_NAME, getFractionName())
                          .add(PROPERTY_MATRIX_NAME, getMatrixName())
                          .add(PROPERTY_CARACTERISTIC_TYPE, getCaracteristicType())
                          .toString();
    }

}

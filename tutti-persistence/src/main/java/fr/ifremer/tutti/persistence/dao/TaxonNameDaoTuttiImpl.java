package fr.ifremer.tutti.persistence.dao;

/*
 * #%L
 * Tutti :: Persistence
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2016 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Function;
import com.google.common.collect.Multimap;
import com.google.common.collect.Multimaps;
import fr.ifremer.adagio.core.dao.referential.taxon.TaxonNameDaoImpl;
import fr.ifremer.adagio.core.dao.referential.taxon.TaxonRefVO;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

/**
 * Created on 16/01/16.
 *
 * @author Tony Chemit - chemit@codelutin.com
 */
@Repository("taxonNameDaoTutti")
@Lazy
public class TaxonNameDaoTuttiImpl extends TaxonNameDaoImpl implements TaxonNameDaoTutti {

    @Autowired
    public TaxonNameDaoTuttiImpl(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    @Override
    public TaxonRefVO[] getAllTaxonNamesWithObsoletes(boolean withSynonyms, Integer transcribingId) {

        Session session = getSession();

        Query query;

        boolean withTranscribing = transcribingId != null;

        if (withSynonyms) {
            query = session.getNamedQuery("allTaxonNamesWithObsoletes");
        } else {
            query = session.getNamedQuery("allTaxonNamesIsReferentWithObsoletes");
        }

        List<TaxonRefVO> results = new ArrayList<>();
        for (Iterator<Object[]> iterator = query.iterate(); iterator.hasNext(); ) {
            Object[] cols = iterator.next();
            TaxonRefVO taxonNameRefTaxVO = loadTaxon(cols, false);
            results.add(taxonNameRefTaxVO);
        }
        if (results.size() == 0) {
            return null;
        }

        if (withTranscribing) {
            query = session.getNamedQuery("allTranscribingForAType");
            query.setInteger("transcribingTypeId", transcribingId);

            Multimap<Integer, TaxonRefVO> r = Multimaps.index(results, TaxonRefVO::getReferenceTaxonId);

            for (Iterator<Object[]> iterator = query.iterate(); iterator.hasNext(); ) {
                Object[] cols = iterator.next();
                Integer referencetaxonId = (Integer) cols[0];
                String externalCode = (String) cols[1];
                Collection<TaxonRefVO> taxonRefVOs = r.get(referencetaxonId);
                if (taxonRefVOs != null)
                    for (TaxonRefVO taxonRefVO : taxonRefVOs) {
                        taxonRefVO.setExternalCode(externalCode);
                    }
            }
        }

        return results.toArray(new TaxonRefVO[results.size()]);

    }
}

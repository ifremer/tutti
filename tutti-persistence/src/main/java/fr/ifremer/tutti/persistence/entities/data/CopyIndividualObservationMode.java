package fr.ifremer.tutti.persistence.entities.data;

/*
 * #%L
 * Tutti :: Persistence
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2016 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.entities.referential.Caracteristic;
import fr.ifremer.tutti.persistence.entities.referential.CaracteristicQualitativeValue;
import fr.ifremer.tutti.persistence.entities.referential.CaracteristicQualitativeValues;

/**
 * @author Kevin Morin (Code Lutin)
 * @since 4.5
 */
public enum CopyIndividualObservationMode {

    ALL(2558), NOTHING(2559), SIZE(2560);

    private final int qualitativeValueId;

    private CaracteristicQualitativeValue qualitativeValue;

    CopyIndividualObservationMode(int qualitativeValueId) {

        this.qualitativeValueId = qualitativeValueId;
    }

    public CaracteristicQualitativeValue getQualitativeValue(Caracteristic copyIndividualObservationModeCaracteristic) {
        if (qualitativeValue == null) {
            qualitativeValue = CaracteristicQualitativeValues.getQualitativeValue(copyIndividualObservationModeCaracteristic, qualitativeValueId);
        }
        return qualitativeValue;
    }

    public static CopyIndividualObservationMode valueOf(int qualitativeValueId) {

        for (CopyIndividualObservationMode copyIndividualObservationMode : values()) {
            if (qualitativeValueId == copyIndividualObservationMode.qualitativeValueId) {
                return copyIndividualObservationMode;
            }
        }

        throw new IllegalStateException("Could not find CopyIndividualObservationMode for qualitativeValueId: " + qualitativeValueId);
    }
}

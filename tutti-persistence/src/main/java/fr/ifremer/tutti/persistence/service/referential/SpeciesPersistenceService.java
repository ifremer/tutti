package fr.ifremer.tutti.persistence.service.referential;

/*
 * #%L
 * Tutti :: Persistence
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.TuttiPersistenceServiceImplementor;
import fr.ifremer.tutti.persistence.entities.referential.Species;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * Created on 11/3/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.8
 */
@Transactional(readOnly = true)
public interface SpeciesPersistenceService extends TuttiPersistenceServiceImplementor {

    /**
     * @return all referent species
     * @since 0.3
     */
    @Cacheable(value = "referentSpecies")
    List<Species> getAllReferentSpecies();

    @Cacheable(value = "referentSpeciesWithObsoletes")
    List<Species> getAllReferentSpeciesWithObsoletes();

    /**
     * @return all species
     * @since 0.3
     */
    @Cacheable(value = "species")
    List<Species> getAllSpecies();

    /**
     * Obtain a species given his referenceTaxonId.
     *
     * @param referenceTaxonId id of the reference taxon of the species to load
     * @return the species or {@code null} if not found.
     * @see Species#getReferenceTaxonId()
     * @see Species#getRefTaxCode()
     * @since 0.3
     */
    @Cacheable(value = "referentSpeciesById", key = "#referenceTaxonId")
    Species getSpeciesByReferenceTaxonId(Integer referenceTaxonId);

    /**
     * Obtain a species with external code as vernacular code given his referenceTaxonId.
     *
     * <strong>Note:</strong> {@link Species#getRefTaxCode()} will not be
     * filled by with method.
     *
     * @param referenceTaxonId id of the reference taxon of the species to load
     * @return the species or {@code null} if not found.
     * @see Species#getReferenceTaxonId()
     * @see Species#getVernacularCode()
     * @since 2.0
     */
    @Cacheable(value = "referentSpeciesByIdVernacular", key = "#referenceTaxonId")
    Species getSpeciesByReferenceTaxonIdWithVernacularCode(Integer referenceTaxonId);

    /**
     * @return the dictionnary of obsolete referent taxon (key: old id, value : new id)
     * @since 4.2
     */
    @Cacheable(value = "obsoleteReferentTaxons")
    Map<Integer, Integer> getAllObsoleteReferentTaxons();

    /**
     * Check if the temporary species with the given {@code referenceTaxonId} is used.
     *
     * @param referenceTaxonId referenceTaxonId of the species to check
     * @return {@code true} if taxon is temporary
     * @since 3.8
     */
    boolean isTemporarySpeciesUsed(Integer referenceTaxonId);

    /**
     * Add temporary species.
     *
     * @param species species to add
     * @return added species
     * @since 3.14
     */
    @Transactional(readOnly = false)
    @CacheEvict(value = {"species", "referentSpecies", "referentSpeciesById", "referentSpeciesByIdVernacular", "obsoleteReferentTaxons", "referentSpeciesWithObsoletes"}, allEntries = true)
    List<Species> addTemporarySpecies(List<Species> species);

    /**
     * Update temporary species.
     *
     * @param species species to update
     * @return updated species
     * @since 3.14
     */
    @Transactional(readOnly = false)
    @CacheEvict(value = {"species", "referentSpecies", "referentSpeciesById", "referentSpeciesByIdVernacular", "obsoleteReferentTaxons", "referentSpeciesWithObsoletes"}, allEntries = true)
    List<Species> updateTemporarySpecies(List<Species> species);

    /**
     * Link temporary species. (Means get existing references using species natural ids).
     *
     * @param species species to link
     * @return linked species
     * @since 3.14
     */
    List<Species> linkTemporarySpecies(List<Species> species);

    /**
     * Replace the {@code source} species by
     * the {@code target} one in all data.
     *
     * @param source the source species to replace
     * @param target the target species to use
     * @param delete flag to delete the temporary vessel after replacement
     * @since 3.6
     */
    @Transactional(readOnly = false)
    @CacheEvict(value = {"fr.ifremer.adagio.core.dao.data.batch.CatchBatchCache",
            "species", "referentSpecies", "referentSpeciesById", "referentSpeciesByIdVernacular", "obsoleteReferentTaxons", "referentSpeciesWithObsoletes"},
            allEntries = true)
    void replaceSpecies(Species source, Species target, boolean delete);

    /**
     * Delete the temporary species with the given {@code referenceTaxonId}.
     *
     * @param referenceTaxonIds referenceTaxonId of the species to remove
     * @since 3.8
     */
    @Transactional(readOnly = false)
    @CacheEvict(value = {"species", "referentSpecies", "referentSpeciesById", "referentSpeciesByIdVernacular", "obsoleteReferentTaxons", "referentSpeciesWithObsoletes"}, allEntries = true)
    void deleteTemporarySpecies(Collection<Integer> referenceTaxonIds);

    /**
     * Delete the temporary species with the given {@code referenceTaxonId}.
     *
     * @param referenceTaxonId reference taxonId of the species to remove
     * @since 3.8
     */
    @Transactional(readOnly = false)
    @CacheEvict(value = {"species", "referentSpecies", "referentSpeciesById", "referentSpeciesByIdVernacular", "obsoleteReferentTaxons", "referentSpeciesWithObsoletes"}, allEntries = true)
    void deleteTemporarySpecies(Integer referenceTaxonId);

}

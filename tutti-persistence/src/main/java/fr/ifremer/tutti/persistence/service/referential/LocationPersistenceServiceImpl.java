package fr.ifremer.tutti.persistence.service.referential;

/*
 * #%L
 * Tutti :: Persistence
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Lists;
import com.google.common.collect.Multimap;
import fr.ifremer.adagio.core.dao.referential.location.LocationClassificationId;
import fr.ifremer.adagio.core.dao.referential.location.LocationExtendDao;
import fr.ifremer.adagio.core.dao.referential.location.LocationLevelId;
import fr.ifremer.adagio.core.service.referential.location.LocationService;
import fr.ifremer.tutti.persistence.entities.referential.TuttiLocation;
import fr.ifremer.tutti.persistence.entities.referential.TuttiLocations;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.type.IntegerType;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created on 11/3/14.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.8
 */
@Service("locationPersistenceService")
public class LocationPersistenceServiceImpl extends ReferentialPersistenceServiceSupport implements LocationPersistenceService {

    /** Logger. */
    private static final Log log = LogFactory.getLog(LocationPersistenceServiceImpl.class);

    @Resource(name = "locationDao")
    protected LocationExtendDao locationDao;

    @Resource(name = "locationService")
    protected LocationService locationService;

    @Override
    public List<TuttiLocation> getAllProgramZone() {
        Iterator<Object[]> list = queryListWithStatus(
                "allLocationsByLevelAndClassificiation",
                "locationClassificationId", IntegerType.INSTANCE, LocationClassificationId.SECTOR.getValue(),
                "locationLevelId", IntegerType.INSTANCE, LocationLevelId.SCIENTIFIC_CRUISE_PROGRAM.getValue());

        List<TuttiLocation> result = Lists.newArrayList();
        loadLocations(list, result);
        return Collections.unmodifiableList(result);
    }

    @Override
    public List<TuttiLocation> getAllCountry() {
        Iterator<Object[]> list = queryListWithStatus(
                "allLocationsByLevelAndClassificiation",
                "locationClassificationId", IntegerType.INSTANCE, LocationClassificationId.TERRITORIAL.getValue(),
                "locationLevelId", IntegerType.INSTANCE, LocationLevelId.PAYS_ISO3.getValue());

        List<TuttiLocation> result = Lists.newArrayList();
        loadLocations(list, result);
        return Collections.unmodifiableList(result);
    }

    @Override
    public List<TuttiLocation> getAllHarbour() {
        Iterator<Object[]> list = queryListWithStatus(
                "allLocationsByLevelAndClassificiation",
                "locationClassificationId", IntegerType.INSTANCE, LocationClassificationId.TERRITORIAL.getValue(),
                "locationLevelId", IntegerType.INSTANCE, LocationLevelId.PORT.getValue());

        List<TuttiLocation> result = Lists.newArrayList();
        loadLocations(list, result);
        return Collections.unmodifiableList(result);
    }

    @Override
    public List<TuttiLocation> getAllHarbourWithObsoletes() {
        Iterator<Object[]> list = queryListWithStatus2(
                "allLocationsByLevelAndClassificiationWithObsoletes",
                "locationClassificationId", IntegerType.INSTANCE, LocationClassificationId.TERRITORIAL.getValue(),
                "locationLevelId", IntegerType.INSTANCE, LocationLevelId.PORT.getValue());

        List<TuttiLocation> result = Lists.newArrayList();
        loadLocations(list, result);
        return Collections.unmodifiableList(result);
    }

    @Override
    public ImmutableSet<Integer> getAllFishingOperationStratasAndSubstratasIdsForProgram(String zoneId) {
        Preconditions.checkNotNull(zoneId);

        ImmutableSet.Builder<Integer> resultBuilder = ImmutableSet.builder();

        List<TuttiLocation> stratas = getAllFishingOperationStrata(zoneId);

        if (log.isInfoEnabled()) {
            log.info("found " + stratas.size() + " stratas for zone: " + zoneId);
        }

        stratas.forEach(strata -> {

            resultBuilder.add(strata.getIdAsInt());

            List<TuttiLocation> subStrats = getAllFishingOperationSubStrata(zoneId, strata.getId());

            if (log.isInfoEnabled()) {
                log.info("found " + subStrats.size() + " substratas for strata: " + strata);
            }

            subStrats.forEach(subStrata -> resultBuilder.add(subStrata.getIdAsInt()));

        });

        return resultBuilder.build();
    }

    @Override
    public Multimap<TuttiLocation, TuttiLocation> getAllFishingOperationStratasAndSubstratas(final String zoneId) {
        Preconditions.checkNotNull(zoneId);

        Multimap<TuttiLocation, TuttiLocation> result = HashMultimap.create();

        List<TuttiLocation> stratas = getAllFishingOperationStrata(zoneId);

        if (log.isInfoEnabled()) {
            log.info("stratas : " + stratas.stream().map(TuttiLocation::getLabel).collect(Collectors.toSet()));
        }

        stratas.forEach(strata -> {
            List<TuttiLocation> allFishingOperationSubStrata = getAllFishingOperationSubStrata(zoneId, strata.getId());

            if (allFishingOperationSubStrata.isEmpty()) {
                result.put(strata, null);

            } else {
                result.putAll(strata, allFishingOperationSubStrata);
            }
        });

        if (log.isInfoEnabled()) {
            log.info("stratas in result : " + result.keySet().stream().map(TuttiLocation::getLabel).collect(Collectors.toSet()));
        }

        return result;
    }

    @Override
    public List<TuttiLocation> getAllFishingOperationStrata(String zoneId) {
        Preconditions.checkNotNull(zoneId);
        List<TuttiLocation> result = getFishingOperationLocationsByParent(
                LocationLevelId.SCIENTIFIC_CRUISE_STRATA.getValue(),
                Integer.valueOf(zoneId),
                LocationLevelId.SCIENTIFIC_CRUISE_PROGRAM.getValue());
        return Collections.unmodifiableList(result);
    }

    @Override
    public List<TuttiLocation> getAllFishingOperationStrataWithObsoletes(String zoneId) {
        Preconditions.checkNotNull(zoneId);
        List<TuttiLocation> result = getFishingOperationLocationsByParentWithObsoletes(
                LocationLevelId.SCIENTIFIC_CRUISE_STRATA.getValue(),
                Integer.valueOf(zoneId),
                LocationLevelId.SCIENTIFIC_CRUISE_PROGRAM.getValue());
        return Collections.unmodifiableList(result);
    }

    @Override
    public List<TuttiLocation> getAllFishingOperationSubStrata(String zoneId,
                                                               String strataId) {

        String parentId;
        Integer parentLocationLevelId;

        if (strataId != null) {

            // use strata as parent
            parentId = strataId;
            parentLocationLevelId = LocationLevelId.SCIENTIFIC_CRUISE_STRATA.getValue();

        } else {

            // use zoneId as parent
            parentId = zoneId;
            parentLocationLevelId = LocationLevelId.SCIENTIFIC_CRUISE_PROGRAM.getValue();
        }

        Preconditions.checkNotNull(parentId);
        List<TuttiLocation> result = getFishingOperationLocationsByParent(
                LocationLevelId.SCIENTIFIC_CRUISE_SUB_STRATA.getValue(),
                Integer.valueOf(parentId),
                parentLocationLevelId);
        return Collections.unmodifiableList(result);
    }

    @Override
    public List<TuttiLocation> getAllFishingOperationSubStrataWithObsoletes(String zoneId, String strataId) {

        String parentId;
        Integer parentLocationLevelId;

        if (strataId != null) {

            // use strata as parent
            parentId = strataId;
            parentLocationLevelId = LocationLevelId.SCIENTIFIC_CRUISE_STRATA.getValue();

        } else {

            // use zoneId as parent
            parentId = zoneId;
            parentLocationLevelId = LocationLevelId.SCIENTIFIC_CRUISE_PROGRAM.getValue();
        }

        Preconditions.checkNotNull(parentId);
        List<TuttiLocation> result = getFishingOperationLocationsByParentWithObsoletes(
                LocationLevelId.SCIENTIFIC_CRUISE_SUB_STRATA.getValue(),
                Integer.valueOf(parentId),
                parentLocationLevelId);
        return Collections.unmodifiableList(result);

    }

    @Override
    public List<TuttiLocation> getAllFishingOperationLocation(String zoneId,
                                                              String strataId,
                                                              String subStrataId) {

        String parentId;
        Integer parentLocationLevelId;

        if (subStrataId != null) {

            // use substrata as parent
            parentId = subStrataId;
            parentLocationLevelId = LocationLevelId.SCIENTIFIC_CRUISE_SUB_STRATA.getValue();

        } else if (strataId != null) {

            // use strata as parent
            parentId = strataId;
            parentLocationLevelId = LocationLevelId.SCIENTIFIC_CRUISE_STRATA.getValue();

        } else {

            // use zoneId as parent
            parentId = zoneId;
            parentLocationLevelId = LocationLevelId.SCIENTIFIC_CRUISE_PROGRAM.getValue();
        }

        Preconditions.checkNotNull(parentId);
        List<TuttiLocation> result = getFishingOperationLocationsByParent(
                LocationLevelId.SCIENTIFIC_CRUISE_LOCALITE.getValue(),
                Integer.valueOf(parentId),
                parentLocationLevelId);
        return Collections.unmodifiableList(result);
    }

    @Override
    public List<TuttiLocation> getAllFishingOperationLocationWithObsoletes(String zoneId, String strataId, String subStrataId) {

        String parentId;
        Integer parentLocationLevelId;

        if (subStrataId != null) {

            // use substrata as parent
            parentId = subStrataId;
            parentLocationLevelId = LocationLevelId.SCIENTIFIC_CRUISE_SUB_STRATA.getValue();

        } else if (strataId != null) {

            // use strata as parent
            parentId = strataId;
            parentLocationLevelId = LocationLevelId.SCIENTIFIC_CRUISE_STRATA.getValue();

        } else {

            // use zoneId as parent
            parentId = zoneId;
            parentLocationLevelId = LocationLevelId.SCIENTIFIC_CRUISE_PROGRAM.getValue();
        }

        Preconditions.checkNotNull(parentId);
        List<TuttiLocation> result = getFishingOperationLocationsByParentWithObsoletes(
                LocationLevelId.SCIENTIFIC_CRUISE_LOCALITE.getValue(),
                Integer.valueOf(parentId),
                parentLocationLevelId);
        return Collections.unmodifiableList(result);

    }

    @Override
    public TuttiLocation getLocation(String id) {
        Object[] source = queryUnique(
                "locationById",
                "locationId", IntegerType.INSTANCE, Integer.valueOf(id)
        );
        TuttiLocation target;
        if (source == null) {
            target = null;
        } else {
            target = loadLocation(source);
        }
        return target;
    }

    @Override
    public String getLocationLabelByLatLong(Float latitude, Float longitude) {
        return locationService.getLocationLabelByLatLong(latitude, longitude);
    }

    @Override
    public Integer getLocationIdByLatLong(Float latitude, Float longitude) {
        return locationService.getLocationIdByLatLong(latitude, longitude);
    }

    protected List<TuttiLocation> getFishingOperationLocationsByParent(Integer locationLevelId, Integer parentId, Integer parentLocationLevelId) {
        Iterator<Object[]> sources = queryListWithStatus(
                "allFishingOperationLocationByParent",
                "parentId", IntegerType.INSTANCE, parentId,
                "parentLocationLevelId", IntegerType.INSTANCE, parentLocationLevelId,
                "locationClassificationId", IntegerType.INSTANCE, LocationClassificationId.SECTOR.getValue(),
                "locationLevelId", IntegerType.INSTANCE, locationLevelId
        );
        List<TuttiLocation> result = Lists.newArrayList();
        loadLocations(sources, result);
        return result;
    }

    protected List<TuttiLocation> getFishingOperationLocationsByParentWithObsoletes(Integer locationLevelId, Integer parentId, Integer parentLocationLevelId) {
        Iterator<Object[]> sources = queryListWithStatus2(
                "allFishingOperationLocationByParentWithObsoletes",
                "parentId", IntegerType.INSTANCE, parentId,
                "parentLocationLevelId", IntegerType.INSTANCE, parentLocationLevelId,
                "locationClassificationId", IntegerType.INSTANCE, LocationClassificationId.SECTOR.getValue(),
                "locationLevelId", IntegerType.INSTANCE, locationLevelId
        );
        List<TuttiLocation> result = Lists.newArrayList();
        loadLocations(sources, result);
        return result;
    }

    protected TuttiLocation loadLocation(Object... source) {
        TuttiLocation target = TuttiLocations.newTuttiLocation();
        target.setId((Integer) source[0]);
        target.setLabel((String) source[1]);
        target.setName((String) source[2]);
        String statusCode = (String) source[3];
        setStatus(statusCode, target);
        return target;
    }

    protected void loadLocations(Iterator<Object[]> list, List<TuttiLocation> result) {
        while (list.hasNext()) {
            Object[] source = list.next();
            TuttiLocation target = loadLocation(source);
            result.add(target);
        }
    }

}

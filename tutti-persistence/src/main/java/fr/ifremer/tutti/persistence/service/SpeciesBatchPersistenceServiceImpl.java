package fr.ifremer.tutti.persistence.service;

/*
 * #%L
 * Tutti :: Persistence
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Multimap;
import fr.ifremer.adagio.core.dao.data.batch.CatchBatch;
import fr.ifremer.tutti.persistence.InvalidBatchModelException;
import fr.ifremer.tutti.persistence.entities.data.BatchContainer;
import fr.ifremer.tutti.persistence.entities.data.SampleCategoryModel;
import fr.ifremer.tutti.persistence.entities.data.SpeciesBatch;
import fr.ifremer.tutti.persistence.entities.data.SpeciesBatchFrequency;
import fr.ifremer.tutti.persistence.entities.data.SpeciesBatchFrequencys;
import fr.ifremer.tutti.persistence.entities.data.SpeciesBatchs;
import fr.ifremer.tutti.persistence.entities.referential.Species;
import fr.ifremer.tutti.persistence.service.util.BatchPersistenceHelper;
import fr.ifremer.tutti.persistence.service.util.tree.SpeciesBatchTreeHelper;
import fr.ifremer.tutti.persistence.service.util.tree.SpeciesBatchTreeHelperSupport;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Collection;
import java.util.List;

/**
 * Default implementation of {@link SpeciesBatchPersistenceService}.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.2
 */
@Service("speciesBatchPersistenceService")
public class SpeciesBatchPersistenceServiceImpl extends SpeciesBatchPersistenceServiceSupport
        implements SpeciesBatchPersistenceService {

    /** Logger. */
    private static final Log log =
            LogFactory.getLog(SpeciesBatchPersistenceServiceImpl.class);

    @Resource(name = "speciesBatchTreeHelper")
    protected SpeciesBatchTreeHelper batchTreeHelper;

    public SpeciesBatchPersistenceServiceImpl() {
        super("Species", SpeciesBatchs::newSpeciesBatch, SpeciesBatchFrequencys::newSpeciesBatchFrequency);
    }

    //------------------------------------------------------------------------//
    //-- SpeciesBatch methods                                               --//
    //------------------------------------------------------------------------//

    @Override
    protected SpeciesBatchTreeHelperSupport getBatchTreeHelper() {
        return batchTreeHelper;
    }

    @Override
    protected void validate(BatchPersistenceHelper batchHelper, SampleCategoryModel sampleCategoryModel, BatchContainer<SpeciesBatch> result) {
        batchHelper.validateSpecies(sampleCategoryModel, result);
    }

    @Override
    public BatchContainer<SpeciesBatch> getRootSpeciesBatch(Integer fishingOperationId, boolean validateTree) throws InvalidBatchModelException {

        return getRootSpeciesBatch0(fishingOperationId, validateTree);
//
//        Preconditions.checkNotNull(fishingOperationId);
//
//        DateFormat df = new SimpleDateFormat("dd/MM/yyy");
//
//        CatchBatch catchBatch = batchHelper.getRootCatchBatchByFishingOperationId(fishingOperationId, false);
//
//        // -- Vrac > Species > Alive Itemized
//        SortingBatch vracSpeciesBatch = batchTreeHelper.getSpeciesVracAliveItemizedRootBatch(catchBatch);
//
//        // container of speciesBatch is arbitraty put on vrac type (there is
//        // no common ancestor for all species batch).
//        BatchContainer<SpeciesBatch> result = new BatchContainer<>();
//
//        SampleCategoryModel sampleCategoryModel = getSampleCategoryModel();
//
//        if (vracSpeciesBatch != null) {
//
//            result.setId(vracSpeciesBatch.getId());
//
//            for (Batch batch : vracSpeciesBatch.getChildBatchs()) {
//                SortingBatch source = (SortingBatch) batch;
//                ReferenceTaxon referenceTaxon = source.getReferenceTaxon();
//                Preconditions.checkNotNull(referenceTaxon, "Can't have a rootSpeciesBatch with a null taxon, but was for " + batch.getId());
//                if (log.isTraceEnabled()) {
//                    log.trace("Loading CatchBatch Vrac > Species > Alive Itemized > " + referenceTaxon.getId() + " - " + " (batch:" + source.getId() + ")");
//                }
//                Species species = speciesService.getSpeciesByReferenceTaxonId(referenceTaxon.getId());
//                if (species == null) {
//                    FishingOperation fishingOperation = fishingOperationPersistenceService.getFishingOperation(fishingOperationId);
//                    String fishingOperationName = fishingOperation.getStationNumber() + " - " + fishingOperation.getFishingOperationNumber() + " - " + df.format(fishingOperation.getGearShootingStartDate());
//                    throw new InvalidBatchModelException(t("tutti.persistence.speciesBatch.validation.unkonwn.taxon", fishingOperationName, source.getId(), referenceTaxon.getId()));
//                }
//                SpeciesBatch target = SpeciesBatchs.newSpeciesBatch();
//                target.setSpecies(species);
//
//                batchHelper.entityToBean(sampleCategoryModel, source, target);
//                result.addChildren(target);
//
//                if (log.isDebugEnabled()) {
//                    log.debug("Loaded CatchBatch Vrac > Species > Alive Itemized > " + target.getSpecies().getReferenceTaxonId() + ": " + target.getId());
//                }
//            }
//        }
//
//        // -- Hors Vrac > Species
//        SortingBatch horsVracSpeciesBatch = batchTreeHelper.getSpeciesHorsVracRootBatch(catchBatch);
//
//        if (horsVracSpeciesBatch != null) {
//            for (Batch batch : horsVracSpeciesBatch.getChildBatchs()) {
//                SortingBatch source = (SortingBatch) batch;
//                ReferenceTaxon referenceTaxon = source.getReferenceTaxon();
//                Preconditions.checkNotNull(referenceTaxon, "Can't have a rootSpeciesBatch with a null taxon, but was for " + source.getId());
//                if (log.isTraceEnabled()) {
//                    log.trace("Loading CatchBatch Hors Vrac > Species > " + referenceTaxon.getId() + " - " + " (batch:" + source.getId() + ")");
//                }
//                Species species = speciesService.getSpeciesByReferenceTaxonId(referenceTaxon.getId());
//                if (species == null) {
//                    FishingOperation fishingOperation = fishingOperationPersistenceService.getFishingOperation(fishingOperationId);
//                    String fishingOperationName = fishingOperation.getStationNumber() + " - " + fishingOperation.getFishingOperationNumber() + " - " + df.format(fishingOperation.getGearShootingStartDate());
//                    throw new InvalidBatchModelException(t("tutti.persistence.speciesBatch.validation.unkonwn.taxon", fishingOperationName, source.getId(), referenceTaxon.getId()));
//                }
//                SpeciesBatch target = SpeciesBatchs.newSpeciesBatch();
//                target.setSpecies(species);
//                batchHelper.entityToBean(sampleCategoryModel, source, target);
//                result.addChildren(target);
//                if (log.isDebugEnabled()) {
//                    log.debug("Loaded CatchBatch Hors Vrac > Species > " + target.getSpecies().getReferenceTaxonId() + ": " + target.getId());
//                }
//            }
//        }
//
//        if (validateTree) {
//
//            // validate with given sample category model
//            batchHelper.validateSpecies(sampleCategoryModel, result);
//        }
//
//        return result;
    }

    @Override
    public SpeciesBatch createSpeciesBatch(SpeciesBatch bean, Integer parentBatchId, boolean computeRankOrder) {

        return createSpeciesBatch0(bean, parentBatchId, computeRankOrder);

//        Preconditions.checkNotNull(bean);
//        Preconditions.checkArgument(TuttiEntities.isNew(bean));
//        Preconditions.checkNotNull(bean.getSpecies());
//        Preconditions.checkNotNull(bean.getSpecies().getId());
//        Preconditions.checkNotNull(bean.getFishingOperation());
//        Preconditions.checkNotNull(bean.getFishingOperation().getId());
//
//        CatchBatch catchBatch = batchHelper.getRootCatchBatchByFishingOperationId(bean.getFishingOperation().getIdAsInt(), false);
//
//        return createSpeciesBatch(bean, parentBatchId, catchBatch, computeRankOrder);
    }

    @Override
    public Collection<SpeciesBatch> createSpeciesBatches(Integer fishingOperationId, Collection<SpeciesBatch> beans) {

        return createSpeciesBatches0(fishingOperationId, beans);

//        Preconditions.checkNotNull(beans);
//        Preconditions.checkNotNull(fishingOperationId);
//
//        CatchBatch catchBatch = batchHelper.getRootCatchBatchByFishingOperationId(fishingOperationId, false);
//
//        Collection<SpeciesBatch> result = new ArrayList<>();
//        for (SpeciesBatch bean : beans) {
//
//            SpeciesBatch created = createSpeciesBatch(bean, null, catchBatch, true);
//            result.add(created);
//
//        }
//        return result;

    }


    protected SpeciesBatch createSpeciesBatch(SpeciesBatch bean, Integer parentBatchId, CatchBatch catchBatch, boolean computeRankOrder) {
        return createSpeciesBatch0(bean, parentBatchId, catchBatch, computeRankOrder);
//        Preconditions.checkNotNull(bean);
//        Preconditions.checkArgument(TuttiEntities.isNew(bean));
//        Preconditions.checkNotNull(bean.getSpecies());
//        Preconditions.checkNotNull(bean.getSpecies().getId());
//        Preconditions.checkNotNull(bean.getFishingOperation());
//        Preconditions.checkNotNull(bean.getFishingOperation().getId());
//
//        SortingBatch batch = SortingBatch.Factory.newInstance();
//        beanToEntity(bean, batch, parentBatchId, catchBatch, computeRankOrder);
//        bean = batchHelper.createSortingBatch(bean, catchBatch, batch);
//
//        return bean;
    }

    @Override
    public SpeciesBatch saveSpeciesBatch(SpeciesBatch bean) {
        return saveSpeciesBatch0(bean);
//        Preconditions.checkNotNull(bean);
//        Preconditions.checkArgument(!TuttiEntities.isNew(bean));
//
//        Integer batchId = bean.getIdAsInt();
//
//        CatchBatch catchBatch = batchHelper.getRootCatchBatchByBatchId(batchId);
//        SortingBatch batch = batchHelper.getSortingBatchById(catchBatch, batchId);
//        Integer parentBatchId = null;
//        if (bean.getParentBatch() != null) {
//            parentBatchId = bean.getParentBatch().getIdAsInt();
//        }
//        beanToEntity(bean, batch, parentBatchId, catchBatch, true);
//        batchHelper.updateSortingBatch(batch, catchBatch);
//
//        return bean;
    }

    @Override
    public void deleteSpeciesBatch(Integer id) {
        deleteSpeciesBatch0(id);
//        Preconditions.checkNotNull(id);
//        batchHelper.deleteBatch(id);
    }

    @Override
    public void deleteSpeciesSubBatch(Integer id) {
        deleteSpeciesSubBatch0(id);
//        Preconditions.checkNotNull(id);
//        batchHelper.deleteSpeciesSubBatch(id);
    }

    @Override
    public void changeSpeciesBatchSpecies(Integer id, Species species) {
        changeSpeciesBatchSpecies0(id, species);
//        Preconditions.checkNotNull(id);
//        Preconditions.checkNotNull(species);
//        Preconditions.checkNotNull(species.getReferenceTaxonId());
//        batchHelper.changeBatchSpecies(id, species);
    }

    @Override
    public List<SpeciesBatch> getAllSpeciesBatchToConfirm(Integer fishingOperationId) throws InvalidBatchModelException {
        return getAllSpeciesBatchToConfirm0(fishingOperationId);
//        List<SpeciesBatch> batchesToConfirm = new ArrayList<>();
//
//        BatchContainer<SpeciesBatch> rootSpeciesBatch = getRootSpeciesBatch(fishingOperationId, false);
//        for (SpeciesBatch speciesBatch : rootSpeciesBatch.getChildren()) {
//            findSpeciesBatchesToConfirm(speciesBatch, batchesToConfirm);
//        }
//
//        return batchesToConfirm;
    }

//    protected void findSpeciesBatchesToConfirm(SpeciesBatch speciesBatch, List<SpeciesBatch> batchesToConfirm) {
//        if (speciesBatch.isSpeciesToConfirm()) {
//            batchesToConfirm.add(speciesBatch);
//
//        } else if (!speciesBatch.isChildBatchsEmpty()) {
//            for (SpeciesBatch batch : speciesBatch.getChildBatchs()) {
//                findSpeciesBatchesToConfirm(batch, batchesToConfirm);
//            }
//        }
//    }

    //------------------------------------------------------------------------//
    //-- SpeciesBatchFrequency methods                                      --//
    //------------------------------------------------------------------------//

    @Override
    public List<SpeciesBatchFrequency> getAllSpeciesBatchFrequency(Integer speciesBatchId) {
        return getAllSpeciesBatchFrequency0(speciesBatchId);
//        Preconditions.checkNotNull(speciesBatchId);
//
//        List<SortingBatch> frequencyChilds =
//                batchHelper.getFrequencies(speciesBatchId);
//        List<SpeciesBatchFrequency> results = Lists.newArrayList();
//        for (SortingBatch child : frequencyChilds) {
//            SpeciesBatchFrequency target =
//                    SpeciesBatchFrequencys.newSpeciesBatchFrequency();
//
//            batchHelper.entityToBatchFrequency(child, target);
//            results.add(target);
//        }
//        return Collections.unmodifiableList(results);
    }

    @Override
    public Multimap<Species, SpeciesBatchFrequency> getAllSpeciesBatchFrequencyForBatch(BatchContainer<SpeciesBatch> batchContainer) {
        return getAllSpeciesBatchFrequencyForBatch0(batchContainer);
//        Multimap<Species, SpeciesBatchFrequency> result = ArrayListMultimap.create();
//        for (SpeciesBatch speciesBatch : batchContainer.getChildren()) {
//            getAllSpeciesBatchFrequencyForBatch(speciesBatch, result);
//        }
//        return result;
    }

//    protected void getAllSpeciesBatchFrequencyForBatch(SpeciesBatch batch,
//                                                       Multimap<Species, SpeciesBatchFrequency> result) {
//
//        List<SpeciesBatchFrequency> speciesBatchFrequency = getAllSpeciesBatchFrequency(batch.getIdAsInt());
//        result.putAll(batch.getSpecies(), speciesBatchFrequency);
//
//        if (!batch.isChildBatchsEmpty()) {
//            for (SpeciesBatch child : batch.getChildBatchs()) {
//                getAllSpeciesBatchFrequencyForBatch(child, result);
//            }
//        }
//    }

    @Override
    public List<SpeciesBatchFrequency> saveSpeciesBatchFrequency(Integer speciesBatchId, List<SpeciesBatchFrequency> frequencies) {
        return saveSpeciesBatchFrequency0(speciesBatchId, frequencies);
//        Preconditions.checkNotNull(speciesBatchId);
//        Preconditions.checkNotNull(frequencies);
//
//        List<SpeciesBatchFrequency> notNullFrequencies = Lists.newArrayList();
//
//        // Check that all frequencies have the same length PMFM (before doing any db call)
//        // and remove null frequencies
//        String pmfmId = null;
//        for (SpeciesBatchFrequency source : frequencies) {
//
//            if (source.getLengthStepCaracteristic() != null) {
//                if (pmfmId == null) {
//                    pmfmId = source.getLengthStepCaracteristic().getId();
//                } else if (!pmfmId.equals(source.getLengthStepCaracteristic().getId())) {
//                    throw new DataIntegrityViolationException("Batch frequencies under one SpeciesBatch must have all the same lengthStepCaracteristic");
//                }
//                notNullFrequencies.add(source);
//            }
//        }
//
//        CatchBatch catchBatch = batchHelper.getRootCatchBatchByBatchId(speciesBatchId);
//
//        if (catchBatch == null) {
//            return notNullFrequencies;
//        }
//
//        // Synchronization status
//        synchronizationStatusHelper.setDirty(catchBatch);
//
//        // Retrieve parent
//        SortingBatch parentBatch = batchHelper.getSortingBatchById(catchBatch, speciesBatchId);
//
//        // Remember child ids, to remove unchanged item (see at bottom in this method)
//        List<Integer> notUpdatedChildIds = Lists.newArrayList();
//        List<SortingBatch> frequencyChilds = batchHelper.getFrequencyChilds(parentBatch);
//        notUpdatedChildIds.addAll(frequencyChilds.stream().map(SortingBatch::getId).collect(Collectors.toList()));
//
//        short rankOrder = 0;
//        List<SortingBatch> batchsToUpdate = Lists.newArrayList();
//        for (SpeciesBatchFrequency source : notNullFrequencies) {
//            rankOrder++;
//
//            SortingBatch target;
//            if (source.getId() == null) {
//
//                // Not existing batch
//                target = SortingBatch.Factory.newInstance();
//
//                // Fill the sorting batch from the source
//                batchHelper.beanToEntity(source, target, parentBatch, rankOrder);
//
//                // Create the targeted batch, then update the source id
//                batchHelper.createSortingBatch(source, catchBatch, target);
//
//                // push back id of created sortingBatch
//                source.setId(target.getId());
//
//                if (log.isDebugEnabled()) {
//                    log.debug("Create frequency sortingBatch(" + rankOrder + "): " + target.getId());
//                }
//            } else {
//
//                // Existing batch
//                target = batchHelper.loadSortingBatch(source.getIdAsInt(), catchBatch);
//
//                // Fill the sorting batch from the source
//                batchHelper.beanToEntity(source, target, parentBatch, rankOrder);
//
//                // Add the batch into a list (will be update later, using this list)
//                batchsToUpdate.add(target);
//
//                // Remove id from id to remove
//                notUpdatedChildIds.remove(target.getId());
//
//                if (log.isDebugEnabled()) {
//                    log.debug("Update frequency sortingBatch(" + rankOrder + "): " + target.getId());
//                }
//            }
//        }
//
//        if (CollectionUtils.isNotEmpty(batchsToUpdate)) {
//
//            // update some batchs
//            batchHelper.updateSortingBatch(batchsToUpdate, catchBatch);
//        }
//
//        if (CollectionUtils.isNotEmpty(notUpdatedChildIds)) {
//
//            // Remove obsolete frequencies
//            for (Integer batchId : notUpdatedChildIds) {
//
//                if (log.isDebugEnabled()) {
//                    log.debug("Remove obsolete frequency sortingBatch: " + batchId);
//                }
//                batchHelper.removeWithChildren(batchId, catchBatch);
//            }
//        }
//
//        return Collections.unmodifiableList(notNullFrequencies);
    }

    //------------------------------------------------------------------------//
    //-- Internal methods                                                   --//
    //------------------------------------------------------------------------//

//    protected void beanToEntity(SpeciesBatch source,
//                                SortingBatch target,
//                                Integer parentBatchId,
//                                CatchBatch catchBatch,
//                                boolean computeRankOrder) {
//
//        Preconditions.checkNotNull(source.getFishingOperation());
//        Preconditions.checkNotNull(source.getFishingOperation().getId());
//
//        // If parent and root need to be set
//        if (target.getId() == null
//                || target.getRootBatch() == null
//                || (target.getParentBatch() != null && !target.getParentBatch().getId().equals(parentBatchId))) {
//
//            batchHelper.setSpeciesBatchParents(
//                    source.getSampleCategoryId(),
//                    source.getSampleCategoryValue(),
//                    target,
//                    parentBatchId,
//                    catchBatch
//            );
//        }
//
//        batchHelper.beanToEntity(parentBatchId, source, target, computeRankOrder);
//
//    }

}

package fr.ifremer.tutti.persistence.entities.data;

/*
 * #%L
 * Tutti :: Persistence
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Function;
import com.google.common.base.Predicate;
import fr.ifremer.tutti.persistence.entities.referential.CaracteristicQualitativeValue;
import fr.ifremer.tutti.persistence.entities.referential.Species;

import java.io.Serializable;
import java.util.Collection;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

/**
 * @author Kevin Morin (Code Lutin)
 */
public class SpeciesBatchs extends AbstractSpeciesBatchs {

    public static final Function<SpeciesBatch, Serializable> GET_SAMPLE_CATEGORY_VALUE = SpeciesBatch::getSampleCategoryValue;

    public static SpeciesBatch newInstance(SpeciesBatch parent) {
        SpeciesBatch speciesBatch = newSpeciesBatch();
        speciesBatch.setBenthosBatch(parent.isBenthosBatch());
        return speciesBatch;
    }

    public static Map<Integer, SpeciesBatch> getAllSpeciesBatchesById(BatchContainer<SpeciesBatch> rootSpeciesBatch) {
        Map<Integer, SpeciesBatch> result = new TreeMap<>();

        for (SpeciesBatch speciesBatch : rootSpeciesBatch.getChildren()) {
            getAllspeciesBatchesById(speciesBatch, result);
        }
        return result;
    }

    private static void getAllspeciesBatchesById(SpeciesBatch speciesBatch, Map<Integer, SpeciesBatch> result) {
        result.put(speciesBatch.getIdAsInt(), speciesBatch);
        if (!speciesBatch.isChildBatchsEmpty()) {
            for (SpeciesBatch batch : speciesBatch.getChildBatchs()) {
                getAllspeciesBatchesById(batch, result);
            }
        }
    }

    public static Predicate<SpeciesBatch> newSpeciesAbleBatchCategoryPredicate(Integer cateogryId, Integer value) {
        return new SpeciesAbleBatchCategoryPredicate(cateogryId, value);
    }

    public static SpeciesBatch createNewChild(SpeciesBatch parent) {

        SpeciesBatch child = newInstance(parent);
        child.setFishingOperation(parent.getFishingOperation());
        child.setSpecies(parent.getSpecies());
        child.setParentBatch(parent);
        parent.addChildBatchs(child);
        return child;

    }

    public static SpeciesBatch newBenthosBatch() {
        SpeciesBatch speciesBatch = newSpeciesBatch();
        speciesBatch.setBenthosBatch(true);
        return speciesBatch;
    }

    public static void grabSpeciesChildBatchs(Collection<SpeciesBatch> childs, Set<Species> speciesSet) {

        for (SpeciesBatch child : childs) {
            speciesSet.add(child.getSpecies());
        }

    }

    public static void grabSampleCategorieValuesChildBatchs(SpeciesBatch batch, Set<Integer> categoryIds) {

        CaracteristicQualitativeValue sampleCategoryValue = (CaracteristicQualitativeValue) batch.getSampleCategoryValue();

        categoryIds.add(sampleCategoryValue.getIdAsInt());
        if (!batch.isChildBatchsEmpty()) {
            for (SpeciesBatch child : batch.getChildBatchs()) {
                grabSampleCategorieValuesChildBatchs(child, categoryIds);
            }
        }


    }

    public static class SpeciesAbleBatchCategoryPredicate implements Predicate<SpeciesBatch> {

        private final Integer id;

        private final Integer qualitativeValue;

        public SpeciesAbleBatchCategoryPredicate(Integer id, Integer qualitativeValue) {
            this.id = id;
            this.qualitativeValue = qualitativeValue;
        }

        @Override
        public boolean apply(SpeciesBatch input) {
            return id.equals(input.getSampleCategoryId()) &&
                    input.getSampleCategoryValue() instanceof CaracteristicQualitativeValue &&
                    qualitativeValue.equals(((CaracteristicQualitativeValue) input.getSampleCategoryValue()).getIdAsInt());
        }

    }
}

package fr.ifremer.tutti.persistence.service;

/*
 * #%L
 * Tutti :: Persistence
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.TuttiPersistenceServiceImplementor;
import fr.ifremer.tutti.persistence.entities.protocol.TuttiProtocol;

import java.util.List;

/**
 * CRUD of {@link TuttiProtocol} entity.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 0.3
 */
public interface ProtocolPersistenceService extends TuttiPersistenceServiceImplementor {

    /**
     * The protocol used by the persistence lay (used to consolidate entites).
     *
     * @return The protocol used by the persistence layer
     * @see #setProtocol(TuttiProtocol)
     * @since 2.6
     */
    TuttiProtocol getProtocol();

    /**
     * Set the protocol to use by the persistence layer.
     *
     * @param protocol the new protocol to use (can be null)
     * @see #getProtocol()
     * @since 2.6
     */
    void setProtocol(TuttiProtocol protocol);

    boolean isProtocolExist(String id);

    /**
     * Given a {@code protocolName}, find out the first available protocol name.
     *
     * If this name is already used, then suffix with {@code -0}, {@code -1}, until one is not used.
     *
     * @param protocolName base protocle name
     * @return the first available protocol name base on the given one.
     * @since 3.14
     */
    String getFirstAvailableName(String protocolName);

    TuttiProtocol getProtocolByName(String protocolName);

    List<String> getAllProtocolId();

    List<String> getAllProtocolNames();

    List<TuttiProtocol> getAllProtocol();

    /**
     *
     * @param programId l'identifiant de la série de compagne (peut-être null)
     * @return la liste des protocoles dont le {@code programId} est celui passé en paramètre
     */
    List<TuttiProtocol> getAllProtocol(String programId);

    TuttiProtocol getProtocol(String id);

    TuttiProtocol createProtocol(TuttiProtocol bean);

    TuttiProtocol saveProtocol(TuttiProtocol bean);

    void deleteProtocol(String protocolId);
}

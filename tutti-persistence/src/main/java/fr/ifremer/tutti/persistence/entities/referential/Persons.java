package fr.ifremer.tutti.persistence.entities.referential;

/*
 * #%L
 * Tutti :: Persistence
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Function;
import com.google.common.base.Preconditions;
import com.google.common.base.Predicate;
import org.apache.commons.lang3.StringUtils;

public class Persons extends AbstractPersons {

    /**
     * Is the given {@code person} a temporary data ?
     *
     * @param person person to test
     * @return {@code true} if the given {@code person} is temporary
     * @since 3.8
     */
    public static boolean isTemporary(Person person) {

        Preconditions.checkNotNull(person);
        Preconditions.checkNotNull(person.getId());

        return TuttiReferentialEntities.isStatusTemporary(person) && isTemporaryId(person.getIdAsInt());

    }

    /**
     * Is the given {@code id} is a person temporary id ?
     *
     * @param id person to test
     * @return {@code true} if the id is a person pecies temporary id
     * @since 3.14
     */
    public static boolean isTemporaryId(Integer id) {

        Preconditions.checkNotNull(id);
        return id < 0;

    }

    public static String getFullName(Person person) {
        return GET_FULL_NAME.apply(person);
    }

    public static final Predicate<Person> IS_TEMPORARY = Persons::isTemporary;

    public static final Function<Person, String> GET_FULL_NAME = input -> StringUtils.trimToEmpty(input.getFirstName()) + " " + StringUtils.trimToEmpty(input.getLastName());
}

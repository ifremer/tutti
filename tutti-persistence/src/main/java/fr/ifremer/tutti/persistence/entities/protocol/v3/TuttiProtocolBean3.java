package fr.ifremer.tutti.persistence.entities.protocol.v3;

/*
 * #%L
 * Tutti :: Persistence
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2016 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.entities.TuttiEntityBean;
import fr.ifremer.tutti.persistence.entities.protocol.CaracteristicMappingRow;
import fr.ifremer.tutti.persistence.entities.protocol.OperationFieldMappingRow;
import fr.ifremer.tutti.persistence.entities.protocol.Zone;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

public class TuttiProtocolBean3 extends TuttiEntityBean implements TuttiProtocol3 {

    private static final long serialVersionUID = 3847260679792845110L;

    protected String name;

    protected String comment;

    protected List<String> lengthClassesPmfmId;

    protected List<String> individualObservationPmfmId;

    protected Integer version;

    protected Collection<String> importColumns;

    protected String programId;

    protected boolean useCalcifiedPieceSampling;

    protected List<SpeciesProtocol3> species;

    protected List<SpeciesProtocol3> benthos;

    protected List<CaracteristicMappingRow> caracteristicMapping;

    protected Collection<OperationFieldMappingRow> operationFieldMapping;

    protected Collection<Zone> zone;

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String getComment() {
        return comment;
    }

    @Override
    public void setComment(String comment) {
        this.comment = comment;
    }

    @Override
    public String getLengthClassesPmfmId(int index) {
        return getChild(lengthClassesPmfmId, index);
    }

    @Override
    public boolean isLengthClassesPmfmIdEmpty() {
        return lengthClassesPmfmId == null || lengthClassesPmfmId.isEmpty();
    }

    @Override
    public int sizeLengthClassesPmfmId() {
        return lengthClassesPmfmId == null ? 0 : lengthClassesPmfmId.size();
    }

    @Override
    public void addLengthClassesPmfmId(String lengthClassesPmfmId) {
        getLengthClassesPmfmId().add(lengthClassesPmfmId);
    }

    @Override
    public void addAllLengthClassesPmfmId(Collection<String> lengthClassesPmfmId) {
        getLengthClassesPmfmId().addAll(lengthClassesPmfmId);
    }

    @Override
    public boolean removeLengthClassesPmfmId(String lengthClassesPmfmId) {
        return getLengthClassesPmfmId().remove(lengthClassesPmfmId);
    }

    @Override
    public boolean removeAllLengthClassesPmfmId(Collection<String> lengthClassesPmfmId) {
        return getLengthClassesPmfmId().removeAll(lengthClassesPmfmId);
    }

    @Override
    public boolean containsLengthClassesPmfmId(String lengthClassesPmfmId) {
        return getLengthClassesPmfmId().contains(lengthClassesPmfmId);
    }

    @Override
    public boolean containsAllLengthClassesPmfmId(Collection<String> lengthClassesPmfmId) {
        return getLengthClassesPmfmId().containsAll(lengthClassesPmfmId);
    }

    @Override
    public List<String> getLengthClassesPmfmId() {
    if (lengthClassesPmfmId == null) {
        lengthClassesPmfmId = new LinkedList<>();
    }
    return lengthClassesPmfmId;
}

    @Override
    public void setLengthClassesPmfmId(List<String> lengthClassesPmfmId) {
        this.lengthClassesPmfmId = lengthClassesPmfmId;
    }

    @Override
    public String getIndividualObservationPmfmId(int index) {
        return getChild(individualObservationPmfmId, index);
    }

    @Override
    public boolean isIndividualObservationPmfmIdEmpty() {
        return individualObservationPmfmId == null || individualObservationPmfmId.isEmpty();
    }

    @Override
    public int sizeIndividualObservationPmfmId() {
        return individualObservationPmfmId == null ? 0 : individualObservationPmfmId.size();
    }

    @Override
    public void addIndividualObservationPmfmId(String individualObservationPmfmId) {
        getIndividualObservationPmfmId().add(individualObservationPmfmId);
    }

    @Override
    public void addAllIndividualObservationPmfmId(Collection<String> individualObservationPmfmId) {
        getIndividualObservationPmfmId().addAll(individualObservationPmfmId);
    }

    @Override
    public boolean removeIndividualObservationPmfmId(String individualObservationPmfmId) {
        return getIndividualObservationPmfmId().remove(individualObservationPmfmId);
    }

    @Override
    public boolean removeAllIndividualObservationPmfmId(Collection<String> individualObservationPmfmId) {
        return getIndividualObservationPmfmId().removeAll(individualObservationPmfmId);
    }

    @Override
    public boolean containsIndividualObservationPmfmId(String individualObservationPmfmId) {
        return getIndividualObservationPmfmId().contains(individualObservationPmfmId);
    }

    @Override
    public boolean containsAllIndividualObservationPmfmId(Collection<String> individualObservationPmfmId) {
        return getIndividualObservationPmfmId().containsAll(individualObservationPmfmId);
    }

    @Override
    public List<String> getIndividualObservationPmfmId() {
    if (individualObservationPmfmId == null) {
        individualObservationPmfmId = new LinkedList<>();
    }
    return individualObservationPmfmId;
}

    @Override
    public void setIndividualObservationPmfmId(List<String> individualObservationPmfmId) {
        this.individualObservationPmfmId = individualObservationPmfmId;
    }

    @Override
    public Integer getVersion() {
        return version;
    }

    @Override
    public void setVersion(Integer version) {
        this.version = version;
    }

    @Override
    public String getImportColumns(int index) {
        return getChild(importColumns, index);
    }

    @Override
    public boolean isImportColumnsEmpty() {
        return importColumns == null || importColumns.isEmpty();
    }

    @Override
    public int sizeImportColumns() {
        return importColumns == null ? 0 : importColumns.size();
    }

    @Override
    public void addImportColumns(String importColumns) {
        getImportColumns().add(importColumns);
    }

    @Override
    public void addAllImportColumns(Collection<String> importColumns) {
        getImportColumns().addAll(importColumns);
    }

    @Override
    public boolean removeImportColumns(String importColumns) {
        return getImportColumns().remove(importColumns);
    }

    @Override
    public boolean removeAllImportColumns(Collection<String> importColumns) {
        return getImportColumns().removeAll(importColumns);
    }

    @Override
    public boolean containsImportColumns(String importColumns) {
        return getImportColumns().contains(importColumns);
    }

    @Override
    public boolean containsAllImportColumns(Collection<String> importColumns) {
        return getImportColumns().containsAll(importColumns);
    }

    @Override
    public Collection<String> getImportColumns() {
    if (importColumns == null) {
        importColumns = new LinkedList<>();
    }
    return importColumns;
}

    @Override
    public void setImportColumns(Collection<String> importColumns) {
        this.importColumns = importColumns;
    }

    @Override
    public String getProgramId() {
        return programId;
    }

    @Override
    public void setProgramId(String programId) {
        this.programId = programId;
    }

    @Override
    public boolean isUseCalcifiedPieceSampling() {
        return useCalcifiedPieceSampling;
    }

    @Override
    public void setUseCalcifiedPieceSampling(boolean useCalcifiedPieceSampling) {
        this.useCalcifiedPieceSampling = useCalcifiedPieceSampling;
    }

    @Override
    public SpeciesProtocol3 getSpecies(int index) {
        return getChild(species, index);
    }

    @Override
    public boolean isSpeciesEmpty() {
        return species == null || species.isEmpty();
    }

    @Override
    public int sizeSpecies() {
        return species == null ? 0 : species.size();
    }

    @Override
    public void addSpecies(SpeciesProtocol3 species) {
        getSpecies().add(species);
    }

    @Override
    public void addAllSpecies(Collection<SpeciesProtocol3> species) {
        getSpecies().addAll(species);
    }

    @Override
    public boolean removeSpecies(SpeciesProtocol3 species) {
        return getSpecies().remove(species);
    }

    @Override
    public boolean removeAllSpecies(Collection<SpeciesProtocol3> species) {
        return getSpecies().removeAll(species);
    }

    @Override
    public boolean containsSpecies(SpeciesProtocol3 species) {
        return getSpecies().contains(species);
    }

    @Override
    public boolean containsAllSpecies(Collection<SpeciesProtocol3> species) {
        return getSpecies().containsAll(species);
    }

    @Override
    public List<SpeciesProtocol3> getSpecies() {
    if (species == null) {
        species = new LinkedList<>();
    }
    return species;
}

    @Override
    public void setSpecies(List<SpeciesProtocol3> species) {
        this.species = species;
    }

    @Override
    public SpeciesProtocol3 getBenthos(int index) {
        return getChild(benthos, index);
    }

    @Override
    public boolean isBenthosEmpty() {
        return benthos == null || benthos.isEmpty();
    }

    @Override
    public int sizeBenthos() {
        return benthos == null ? 0 : benthos.size();
    }

    @Override
    public void addBenthos(SpeciesProtocol3 benthos) {
        getBenthos().add(benthos);
    }

    @Override
    public void addAllBenthos(Collection<SpeciesProtocol3> benthos) {
        getBenthos().addAll(benthos);
    }

    @Override
    public boolean removeBenthos(SpeciesProtocol3 benthos) {
        return getBenthos().remove(benthos);
    }

    @Override
    public boolean removeAllBenthos(Collection<SpeciesProtocol3> benthos) {
        return getBenthos().removeAll(benthos);
    }

    @Override
    public boolean containsBenthos(SpeciesProtocol3 benthos) {
        return getBenthos().contains(benthos);
    }

    @Override
    public boolean containsAllBenthos(Collection<SpeciesProtocol3> benthos) {
        return getBenthos().containsAll(benthos);
    }

    @Override
    public List<SpeciesProtocol3> getBenthos() {
    if (benthos == null) {
        benthos = new LinkedList<>();
    }
    return benthos;
}

    @Override
    public void setBenthos(List<SpeciesProtocol3> benthos) {
        this.benthos = benthos;
    }

    @Override
    public CaracteristicMappingRow getCaracteristicMapping(int index) {
        return getChild(caracteristicMapping, index);
    }

    @Override
    public boolean isCaracteristicMappingEmpty() {
        return caracteristicMapping == null || caracteristicMapping.isEmpty();
    }

    @Override
    public int sizeCaracteristicMapping() {
        return caracteristicMapping == null ? 0 : caracteristicMapping.size();
    }

    @Override
    public void addCaracteristicMapping(CaracteristicMappingRow caracteristicMapping) {
        getCaracteristicMapping().add(caracteristicMapping);
    }

    @Override
    public void addAllCaracteristicMapping(Collection<CaracteristicMappingRow> caracteristicMapping) {
        getCaracteristicMapping().addAll(caracteristicMapping);
    }

    @Override
    public boolean removeCaracteristicMapping(CaracteristicMappingRow caracteristicMapping) {
        return getCaracteristicMapping().remove(caracteristicMapping);
    }

    @Override
    public boolean removeAllCaracteristicMapping(Collection<CaracteristicMappingRow> caracteristicMapping) {
        return getCaracteristicMapping().removeAll(caracteristicMapping);
    }

    @Override
    public boolean containsCaracteristicMapping(CaracteristicMappingRow caracteristicMapping) {
        return getCaracteristicMapping().contains(caracteristicMapping);
    }

    @Override
    public boolean containsAllCaracteristicMapping(Collection<CaracteristicMappingRow> caracteristicMapping) {
        return getCaracteristicMapping().containsAll(caracteristicMapping);
    }

    @Override
    public List<CaracteristicMappingRow> getCaracteristicMapping() {
    if (caracteristicMapping == null) {
        caracteristicMapping = new LinkedList<>();
    }
    return caracteristicMapping;
}

    @Override
    public void setCaracteristicMapping(List<CaracteristicMappingRow> caracteristicMapping) {
        this.caracteristicMapping = caracteristicMapping;
    }

    @Override
    public OperationFieldMappingRow getOperationFieldMapping(int index) {
        return getChild(operationFieldMapping, index);
    }

    @Override
    public boolean isOperationFieldMappingEmpty() {
        return operationFieldMapping == null || operationFieldMapping.isEmpty();
    }

    @Override
    public int sizeOperationFieldMapping() {
        return operationFieldMapping == null ? 0 : operationFieldMapping.size();
    }

    @Override
    public void addOperationFieldMapping(OperationFieldMappingRow operationFieldMapping) {
        getOperationFieldMapping().add(operationFieldMapping);
    }

    @Override
    public void addAllOperationFieldMapping(Collection<OperationFieldMappingRow> operationFieldMapping) {
        getOperationFieldMapping().addAll(operationFieldMapping);
    }

    @Override
    public boolean removeOperationFieldMapping(OperationFieldMappingRow operationFieldMapping) {
        return getOperationFieldMapping().remove(operationFieldMapping);
    }

    @Override
    public boolean removeAllOperationFieldMapping(Collection<OperationFieldMappingRow> operationFieldMapping) {
        return getOperationFieldMapping().removeAll(operationFieldMapping);
    }

    @Override
    public boolean containsOperationFieldMapping(OperationFieldMappingRow operationFieldMapping) {
        return getOperationFieldMapping().contains(operationFieldMapping);
    }

    @Override
    public boolean containsAllOperationFieldMapping(Collection<OperationFieldMappingRow> operationFieldMapping) {
        return getOperationFieldMapping().containsAll(operationFieldMapping);
    }

    @Override
    public Collection<OperationFieldMappingRow> getOperationFieldMapping() {
    if (operationFieldMapping == null) {
        operationFieldMapping = new LinkedList<>();
    }
    return operationFieldMapping;
}

    @Override
    public void setOperationFieldMapping(Collection<OperationFieldMappingRow> operationFieldMapping) {
        this.operationFieldMapping = operationFieldMapping;
    }

    @Override
    public Zone getZone(int index) {
        return getChild(zone, index);
    }

    @Override
    public boolean isZoneEmpty() {
        return zone == null || zone.isEmpty();
    }

    @Override
    public int sizeZone() {
        return zone == null ? 0 : zone.size();
    }

    @Override
    public void addZone(Zone zone) {
        getZone().add(zone);
    }

    @Override
    public void addAllZone(Collection<Zone> zone) {
        getZone().addAll(zone);
    }

    @Override
    public boolean removeZone(Zone zone) {
        return getZone().remove(zone);
    }

    @Override
    public boolean removeAllZone(Collection<Zone> zone) {
        return getZone().removeAll(zone);
    }

    @Override
    public boolean containsZone(Zone zone) {
        return getZone().contains(zone);
    }

    @Override
    public boolean containsAllZone(Collection<Zone> zone) {
        return getZone().containsAll(zone);
    }

    @Override
    public Collection<Zone> getZone() {
    if (zone == null) {
        zone = new LinkedList<>();
    }
    return zone;
}

    @Override
    public void setZone(Collection<Zone> zone) {
        this.zone = zone;
    }

}

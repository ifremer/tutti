package fr.ifremer.tutti.persistence.test;

/*
 * #%L
 * Tutti :: Persistence
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Sets;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.runner.Description;
import org.junit.runner.notification.Failure;
import org.junit.runner.notification.RunListener;

import java.util.Iterator;
import java.util.Objects;
import java.util.Set;

/**
 * Listen JUnit test and keep failures (in order to remove or not directories
 * in {@link DatabaseResource}.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.0.2
 */
public final class TuttiRunListener extends RunListener {

    /** Logger. */
    private static final Log log = LogFactory.getLog(TuttiRunListener.class);

    protected static final Set<Failure> failures = Sets.newHashSet();

    public static synchronized void beforeClass(Description description) {

        if (log.isDebugEnabled()) {
            log.debug("Start to listen test " + description);
        }

        Iterator<Failure> iterator = failures.iterator();
        String className = description.getClassName();
        while (iterator.hasNext()) {
            Failure failure = iterator.next();
            if (className.equals(failure.getDescription().getClassName())) {
                iterator.remove();
            }
        }
    }

    public static synchronized void register(Description description) {

        if (log.isInfoEnabled()) {
            log.info("Start to listen test " + description);
        }

        Iterator<Failure> iterator = failures.iterator();
        String className = description.getClassName();
        String methodName = description.getMethodName();
        while (iterator.hasNext()) {
            Failure failure = iterator.next();
            if (Objects.equals(className, failure.getDescription().getClassName())
                && Objects.equals(methodName, failure.getDescription().getMethodName())) {
                iterator.remove();
            }
        }

    }

    public static synchronized Set<Failure> getFailuresForClass(String className) {
        Set<Failure> result = Sets.newHashSet();
        for (Failure failure : failures) {
            if (className.equals(failure.getDescription().getClassName())) {
                result.add(failure);
            }
        }
        return result;
    }

    public static synchronized Failure getFailureForDescription(Description description) {
        Failure result = null;
        for (Failure failure : failures) {
            if (description.equals(failure.getDescription())) {
                result = failure;
                break;
            }
        }
        return result;
    }

    @Override
    public void testFailure(Failure failure) throws Exception {
        if (log.isDebugEnabled()) {
            log.debug("Adding failure: " + failure + ", for description: " + failure.getDescription());
        }
        failures.add(failure);
        super.testFailure(failure);

    }

    @Override
    public void testAssumptionFailure(Failure failure) {
        if (log.isDebugEnabled()) {
            log.debug("Adding assumptionFailure: " + failure + ", for description: " + failure.getDescription());
        }
        failures.add(failure);
        super.testAssumptionFailure(failure);
    }
}

package fr.ifremer.tutti.persistence.test;

/*
 * #%L
 * Tutti :: Persistence
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2015 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.rules.TestRule;
import org.junit.runner.Description;
import org.junit.runner.notification.Failure;
import org.junit.runners.model.Statement;

import java.util.HashMap;
import java.util.Map;

/**
 * Created on 2/23/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.14
 */
public class CleanResourcesRule implements TestRule {

    /** Logger. */
    private static final Log log = LogFactory.getLog(CleanResourcesRule.class);

    private static final Map<Description, DatabaseResource> RESSOURCES_BY_DESCRIPTIONS = new HashMap<>();

    public static void registerDescription(Description description, DatabaseResource databaseResource) {
        RESSOURCES_BY_DESCRIPTIONS.put(description, databaseResource);
    }

    public static void cleanResources(Description description) {

        DatabaseResource databaseResource = RESSOURCES_BY_DESCRIPTIONS.get(description);

        if (databaseResource != null) {

            if (log.isInfoEnabled()) {
                log.info("Try to clean resources for test: " + description);
            }

            Failure failure = TuttiRunListener.getFailureForDescription(description);


            if (failure == null) {

                if (log.isInfoEnabled()) {
                    log.info("Clean resources (no failure found) for test: " + description);
                }
                databaseResource.cleanResources(description);

            } else {

                if (log.isWarnEnabled()) {
                    log.warn("Keep resources ((found failure) for test: " + description);
                }

            }

        }

    }

    @Override
    public Statement apply(final Statement base, final Description description) {

        return new Statement() {
            @Override
            public void evaluate() throws Throwable {
                try {
                    base.evaluate();
                } finally {
                    after(description);
                }
            }
        };
    }

    protected void after(Description description) {

        cleanResources(description);

        for (Description childDescription : description.getChildren()) {

            cleanResources(childDescription);

        }

    }

}

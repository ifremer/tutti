package fr.ifremer.tutti.persistence.entities.protocol.v1;

/*
 * #%L
 * Tutti :: Persistence
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.entities.TuttiEntity;

/**
 * To migrate old species protocol to last version.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.4
 */
public interface SpeciesProtocol1 extends TuttiEntity {

    String PROPERTY_SPECIES_REFERENCE_TAXON_ID = "speciesReferenceTaxonId";

    String PROPERTY_SPECIES_SURVEY_CODE = "speciesSurveyCode";

    String PROPERTY_SIZE_ENABLED = "sizeEnabled";

    String PROPERTY_SEX_ENABLED = "sexEnabled";

    String PROPERTY_MATURITY_ENABLED = "maturityEnabled";

    String PROPERTY_AGE_ENABLED = "ageEnabled";

    String PROPERTY_LENGTH_STEP_PMFM_ID = "lengthStepPmfmId";

    String PROPERTY_WEIGHT_ENABLED = "weightEnabled";

    String PROPERTY_COUNT_IF_NO_FREQUENCY_ENABLED = "countIfNoFrequencyEnabled";

    String PROPERTY_CALCIFY_SAMPLE_ENABLED = "calcifySampleEnabled";

    String PROPERTY_LENGTH_STEP = "lengthStep";

    String PROPERTY_MADE_FROM_AREFERENT_TAXON = "madeFromAReferentTaxon";

    Integer getSpeciesReferenceTaxonId();

    void setSpeciesReferenceTaxonId(Integer speciesReferenceTaxonId);

    String getSpeciesSurveyCode();

    void setSpeciesSurveyCode(String speciesSurveyCode);

    boolean isSizeEnabled();

    void setSizeEnabled(boolean sizeEnabled);

    boolean isSexEnabled();

    void setSexEnabled(boolean sexEnabled);

    boolean isMaturityEnabled();

    void setMaturityEnabled(boolean maturityEnabled);

    boolean isAgeEnabled();

    void setAgeEnabled(boolean ageEnabled);

    String getLengthStepPmfmId();

    void setLengthStepPmfmId(String lengthStepPmfmId);

    boolean isWeightEnabled();

    void setWeightEnabled(boolean weightEnabled);

    boolean isCountIfNoFrequencyEnabled();

    void setCountIfNoFrequencyEnabled(boolean countIfNoFrequencyEnabled);

    boolean isCalcifySampleEnabled();

    void setCalcifySampleEnabled(boolean calcifySampleEnabled);

    Float getLengthStep();

    void setLengthStep(Float lengthStep);

    boolean isMadeFromAReferentTaxon();

    void setMadeFromAReferentTaxon(boolean madeFromAReferentTaxon);

}

package fr.ifremer.tutti.persistence.service;

/*
 * #%L
 * Tutti :: Persistence
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.collect.Multimap;
import com.google.common.collect.Sets;
import fr.ifremer.adagio.core.dao.data.batch.Batch;
import fr.ifremer.adagio.core.dao.data.batch.CatchBatch;
import fr.ifremer.adagio.core.dao.data.batch.SortingBatch;
import fr.ifremer.adagio.core.dao.data.measure.SortingMeasurement;
import fr.ifremer.adagio.core.dao.referential.QualityFlag;
import fr.ifremer.adagio.core.dao.referential.QualityFlagCode;
import fr.ifremer.adagio.core.dao.referential.QualityFlagImpl;
import fr.ifremer.adagio.core.dao.referential.pmfm.Pmfm;
import fr.ifremer.adagio.core.dao.referential.pmfm.PmfmId;
import fr.ifremer.adagio.core.dao.referential.taxon.ReferenceTaxon;
import fr.ifremer.adagio.core.dao.referential.taxon.ReferenceTaxonImpl;
import fr.ifremer.tutti.persistence.InvalidBatchModelException;
import fr.ifremer.tutti.persistence.entities.TuttiEntities;
import fr.ifremer.tutti.persistence.entities.data.BatchContainer;
import fr.ifremer.tutti.persistence.entities.data.FishingOperation;
import fr.ifremer.tutti.persistence.entities.data.SampleCategoryModel;
import fr.ifremer.tutti.persistence.entities.data.SpeciesBatch;
import fr.ifremer.tutti.persistence.entities.data.SpeciesBatchFrequency;
import fr.ifremer.tutti.persistence.entities.data.SpeciesBatchs;
import fr.ifremer.tutti.persistence.entities.referential.Caracteristic;
import fr.ifremer.tutti.persistence.entities.referential.CaracteristicQualitativeValue;
import fr.ifremer.tutti.persistence.entities.referential.CaracteristicType;
import fr.ifremer.tutti.persistence.entities.referential.Species;
import fr.ifremer.tutti.persistence.service.referential.CaracteristicPersistenceService;
import fr.ifremer.tutti.persistence.service.referential.SpeciesPersistenceService;
import fr.ifremer.tutti.persistence.service.util.BatchPersistenceHelper;
import fr.ifremer.tutti.persistence.service.util.MeasurementPersistenceHelper;
import fr.ifremer.tutti.persistence.service.util.tree.SpeciesBatchTreeHelperSupport;
import fr.ifremer.tutti.persistence.service.util.SynchronizationStatusHelper;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.dao.DataIntegrityViolationException;

import javax.annotation.Resource;
import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.function.Supplier;
import java.util.stream.Collectors;

import static org.nuiton.i18n.I18n.t;

/**
 * Default implementation of {@link SpeciesBatchPersistenceService}.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.2
 */
public abstract class SpeciesBatchPersistenceServiceSupport extends AbstractPersistenceService {

    /** Logger. */
    private static final Log log = LogFactory.getLog(SpeciesBatchPersistenceServiceSupport.class);

    @Resource(name = "speciesPersistenceService")
    private SpeciesPersistenceService speciesService;

    @Resource(name = "fishingOperationPersistenceService")
    private FishingOperationPersistenceService fishingOperationPersistenceService;

    @Resource(name = "caracteristicPersistenceService")
    private CaracteristicPersistenceService caracteristicService;

    @Resource(name = "batchPersistenceHelper")
    protected BatchPersistenceHelper batchHelper;

    @Resource(name = "measurementPersistenceHelper")
    protected MeasurementPersistenceHelper measurementPersistenceHelper;

    @Resource(name = "synchronizationStatusHelper")
    private SynchronizationStatusHelper synchronizationStatusHelper;

    private final String prefix;

    private final Supplier<SpeciesBatch> batchFactory;
    private final Supplier<SpeciesBatchFrequency> frequencyFactory;

    protected SpeciesBatchPersistenceServiceSupport(String prefix, Supplier<SpeciesBatch> batchFactory, Supplier<SpeciesBatchFrequency> frequencyFactory) {
        this.prefix = prefix;
        this.batchFactory = batchFactory;
        this.frequencyFactory = frequencyFactory;
    }

    public Set<Integer> getBatchChildIds(Integer id) {
        return batchHelper.getBatchIds(id);
    }

    //------------------------------------------------------------------------//
    //-- SpeciesBatch methods                                               --//
    //------------------------------------------------------------------------//

    protected abstract SpeciesBatchTreeHelperSupport getBatchTreeHelper();

    protected abstract void validate(BatchPersistenceHelper batchHelper, SampleCategoryModel sampleCategoryModel, BatchContainer<SpeciesBatch> result);

    protected final BatchContainer<SpeciesBatch> getRootSpeciesBatch0(Integer fishingOperationId, boolean validateTree) throws InvalidBatchModelException {

        Objects.requireNonNull(fishingOperationId);

        DateFormat df = new SimpleDateFormat("dd/MM/yyy");

        CatchBatch catchBatch = batchHelper.getRootCatchBatchByFishingOperationId(fishingOperationId, false);

        // -- Vrac > Species > Alive Itemized
        SortingBatch vracSpeciesBatch = getBatchTreeHelper().getVracAliveItemizedRootBatch(catchBatch);

        // container of speciesBatch is arbitraty put on vrac type (there is
        // no common ancestor for all species batch).
        BatchContainer<SpeciesBatch> result = new BatchContainer<>();

        SampleCategoryModel sampleCategoryModel = getSampleCategoryModel();

        if (vracSpeciesBatch != null) {

            result.setId(vracSpeciesBatch.getId());

            for (Batch batch : vracSpeciesBatch.getChildBatchs()) {
                SortingBatch source = (SortingBatch) batch;
                ReferenceTaxon referenceTaxon = source.getReferenceTaxon();
                Objects.requireNonNull(referenceTaxon, "Can't have a rootSpeciesBatch with a null taxon, but was for " + batch.getId());
                if (log.isTraceEnabled()) {
                    log.trace("Loading CatchBatch Vrac > " + prefix + " > Alive Itemized > " + referenceTaxon.getId() + " - " + " (batch:" + source.getId() + ")");
                }
                Species species = speciesService.getSpeciesByReferenceTaxonId(referenceTaxon.getId());
                if (species == null) {
                    FishingOperation fishingOperation = fishingOperationPersistenceService.getFishingOperation(fishingOperationId);
                    String fishingOperationName = fishingOperation.getStationNumber() + " - " + fishingOperation.getFishingOperationNumber() + " - " + df.format(fishingOperation.getGearShootingStartDate());
                    throw new InvalidBatchModelException(t("tutti.persistence.speciesBatch.validation.unkonwn.taxon", fishingOperationName, source.getId(), referenceTaxon.getId()));
                }
                SpeciesBatch target = batchFactory.get();
                target.setSpecies(species);

                entityToBean(sampleCategoryModel, source, target);
                result.addChildren(target);

                if (log.isDebugEnabled()) {
                    log.debug("Loaded CatchBatch Vrac > " + prefix + " > Alive Itemized > " + target.getSpecies().getReferenceTaxonId() + ": " + target.getId());
                }
            }
        }

        // -- Hors Vrac > Species
        SortingBatch horsVracSpeciesBatch = getBatchTreeHelper().getHorsVracRootBatch(catchBatch);

        if (horsVracSpeciesBatch != null) {
            for (Batch batch : horsVracSpeciesBatch.getChildBatchs()) {
                SortingBatch source = (SortingBatch) batch;
                ReferenceTaxon referenceTaxon = source.getReferenceTaxon();
                Objects.requireNonNull(referenceTaxon, "Can't have a rootSpeciesBatch with a null taxon, but was for " + source.getId());
                if (log.isTraceEnabled()) {
                    log.trace("Loading CatchBatch Hors Vrac > " + prefix + " > " + referenceTaxon.getId() + " - " + " (batch:" + source.getId() + ")");
                }
                Species species = speciesService.getSpeciesByReferenceTaxonId(referenceTaxon.getId());
                if (species == null) {
                    FishingOperation fishingOperation = fishingOperationPersistenceService.getFishingOperation(fishingOperationId);
                    String fishingOperationName = fishingOperation.getStationNumber() + " - " + fishingOperation.getFishingOperationNumber() + " - " + df.format(fishingOperation.getGearShootingStartDate());
                    throw new InvalidBatchModelException(t("tutti.persistence.speciesBatch.validation.unkonwn.taxon", fishingOperationName, source.getId(), referenceTaxon.getId()));
                }
                SpeciesBatch target = batchFactory.get();
                target.setSpecies(species);
                entityToBean(sampleCategoryModel, source, target);
                result.addChildren(target);
                if (log.isDebugEnabled()) {
                    log.debug("Loaded CatchBatch Hors Vrac > " + prefix + " > " + target.getSpecies().getReferenceTaxonId() + ": " + target.getId());
                }
            }
        }

        if (validateTree) {

            // validate with given sample category model
            validate(batchHelper, sampleCategoryModel, result);
        }

        return result;
    }

    protected SpeciesBatch createSpeciesBatch0(SpeciesBatch bean, Integer parentBatchId, boolean computeRankOrder) {
        Objects.requireNonNull(bean);
        Preconditions.checkArgument(TuttiEntities.isNew(bean));
        Objects.requireNonNull(bean.getSpecies());
        Objects.requireNonNull(bean.getSpecies().getId());
        Objects.requireNonNull(bean.getFishingOperation());
        Objects.requireNonNull(bean.getFishingOperation().getId());

        CatchBatch catchBatch = batchHelper.getRootCatchBatchByFishingOperationId(bean.getFishingOperation().getIdAsInt(), false);

        return createSpeciesBatch0(bean, parentBatchId, catchBatch, computeRankOrder);
    }


    protected Collection<SpeciesBatch> createSpeciesBatches0(Integer fishingOperationId, Collection<SpeciesBatch> beans) {

        Objects.requireNonNull(beans);
        Objects.requireNonNull(fishingOperationId);

        CatchBatch catchBatch = batchHelper.getRootCatchBatchByFishingOperationId(fishingOperationId, false);

        Collection<SpeciesBatch> result = new ArrayList<>();
        for (SpeciesBatch bean : beans) {

            SpeciesBatch created = createSpeciesBatch0(bean, null, catchBatch, true);
            result.add(created);

        }
        return result;

    }

    protected SpeciesBatch createSpeciesBatch0(SpeciesBatch bean, Integer parentBatchId, CatchBatch catchBatch, boolean computeRankOrder) {

        Objects.requireNonNull(bean);
        Preconditions.checkArgument(TuttiEntities.isNew(bean));
        Objects.requireNonNull(bean.getSpecies());
        Objects.requireNonNull(bean.getSpecies().getId());
        Objects.requireNonNull(bean.getFishingOperation());
        Objects.requireNonNull(bean.getFishingOperation().getId());

        SortingBatch batch = SortingBatch.Factory.newInstance();
        beanToEntity0(bean, batch, parentBatchId, catchBatch, computeRankOrder);
        bean = batchHelper.createSortingBatch(bean, catchBatch, batch);

        return bean;
    }

    protected SpeciesBatch saveSpeciesBatch0(SpeciesBatch bean) {
        Objects.requireNonNull(bean);
        Preconditions.checkArgument(!TuttiEntities.isNew(bean));

        Integer batchId = bean.getIdAsInt();

        CatchBatch catchBatch = batchHelper.getRootCatchBatchByBatchId(batchId);
        SortingBatch batch = batchHelper.getSortingBatchById(catchBatch, batchId);
        Integer parentBatchId = null;
        if (bean.getParentBatch() != null) {
            parentBatchId = bean.getParentBatch().getIdAsInt();
        }
        beanToEntity0(bean, batch, parentBatchId, catchBatch, true);
        batchHelper.updateSortingBatch(batch, catchBatch);

        return bean;
    }

    protected void deleteSpeciesBatch0(Integer id) {
        Objects.requireNonNull(id);
        batchHelper.deleteBatch(id);
    }

    protected void deleteSpeciesSubBatch0(Integer id) {
        Objects.requireNonNull(id);
        deleteSpeciesSubBatch(id);
    }

    protected void changeSpeciesBatchSpecies0(Integer id, Species species) {
        Objects.requireNonNull(id);
        Objects.requireNonNull(species);
        Objects.requireNonNull(species.getReferenceTaxonId());
        changeBatchSpecies(id, species);
    }

    protected List<SpeciesBatch> getAllSpeciesBatchToConfirm0(Integer fishingOperationId) throws InvalidBatchModelException {
        List<SpeciesBatch> batchesToConfirm = new ArrayList<>();

        BatchContainer<SpeciesBatch> rootSpeciesBatch = getRootSpeciesBatch0(fishingOperationId, false);
        for (SpeciesBatch speciesBatch : rootSpeciesBatch.getChildren()) {
            findSpeciesBatchesToConfirm(speciesBatch, batchesToConfirm);
        }

        return batchesToConfirm;
    }

    private void findSpeciesBatchesToConfirm(SpeciesBatch speciesBatch, List<SpeciesBatch> batchesToConfirm) {
        if (speciesBatch.isSpeciesToConfirm()) {
            batchesToConfirm.add(speciesBatch);

        } else if (!speciesBatch.isChildBatchsEmpty()) {
            for (SpeciesBatch batch : speciesBatch.getChildBatchs()) {
                findSpeciesBatchesToConfirm(batch, batchesToConfirm);
            }
        }
    }

    //------------------------------------------------------------------------//
    //-- SpeciesBatchFrequency methods                                      --//
    //------------------------------------------------------------------------//

    protected List<SpeciesBatchFrequency> getAllSpeciesBatchFrequency0(Integer speciesBatchId) {
        Objects.requireNonNull(speciesBatchId);

        List<SortingBatch> frequencyChilds = getFrequencies(speciesBatchId);
        List<SpeciesBatchFrequency> results = new ArrayList<>();
        for (SortingBatch child : frequencyChilds) {
            SpeciesBatchFrequency target = frequencyFactory.get();

            entityToBatchFrequency(child, target);
            results.add(target);
        }
        return Collections.unmodifiableList(results);
    }

    protected Multimap<Species, SpeciesBatchFrequency> getAllSpeciesBatchFrequencyForBatch0(BatchContainer<SpeciesBatch> batchContainer) {
        Multimap<Species, SpeciesBatchFrequency> result = ArrayListMultimap.create();
        for (SpeciesBatch speciesBatch : batchContainer.getChildren()) {
            getAllSpeciesBatchFrequencyForBatch(speciesBatch, result);
        }
        return result;
    }

    private void getAllSpeciesBatchFrequencyForBatch(SpeciesBatch batch, Multimap<Species, SpeciesBatchFrequency> result) {

        List<SpeciesBatchFrequency> speciesBatchFrequency = getAllSpeciesBatchFrequency0(batch.getIdAsInt());
        result.putAll(batch.getSpecies(), speciesBatchFrequency);

        if (!batch.isChildBatchsEmpty()) {
            for (SpeciesBatch child : batch.getChildBatchs()) {
                getAllSpeciesBatchFrequencyForBatch(child, result);
            }
        }
    }

    protected List<SpeciesBatchFrequency> saveSpeciesBatchFrequency0(Integer speciesBatchId, List<SpeciesBatchFrequency> frequencies) {
        Objects.requireNonNull(speciesBatchId);
        Objects.requireNonNull(frequencies);

        List<SpeciesBatchFrequency> notNullFrequencies = new ArrayList<>();

        // Check that all frequencies have the same length PMFM (before doing any db call)
        // and remove null frequencies
        String pmfmId = null;
        for (SpeciesBatchFrequency source : frequencies) {

            if (source.getLengthStepCaracteristic() != null) {
                if (pmfmId == null) {
                    pmfmId = source.getLengthStepCaracteristic().getId();
                } else if (!pmfmId.equals(source.getLengthStepCaracteristic().getId())) {
                    throw new DataIntegrityViolationException("Batch frequencies under one SpeciesBatch must have all the same lengthStepCaracteristic");
                }
                notNullFrequencies.add(source);
            }
        }

        CatchBatch catchBatch = batchHelper.getRootCatchBatchByBatchId(speciesBatchId);

        if (catchBatch == null) {
            return notNullFrequencies;
        }

        // Synchronization status
        synchronizationStatusHelper.setDirty(catchBatch);

        // Retrieve parent
        SortingBatch parentBatch = batchHelper.getSortingBatchById(catchBatch, speciesBatchId);

        // Remember child ids, to remove unchanged item (see at bottom in this method)
        List<Integer> notUpdatedChildIds = new ArrayList<>();
        List<SortingBatch> frequencyChilds = getFrequencyChilds(parentBatch);
        notUpdatedChildIds.addAll(frequencyChilds.stream().map(SortingBatch::getId).collect(Collectors.toList()));

        short rankOrder = 0;
        List<SortingBatch> batchsToUpdate = new ArrayList<>();
        for (SpeciesBatchFrequency source : notNullFrequencies) {
            rankOrder++;

            SortingBatch target;
            if (source.getId() == null) {

                // Not existing batch
                target = SortingBatch.Factory.newInstance();

                // Fill the sorting batch from the source
                beanToEntity0(source, target, parentBatch, rankOrder);

                // Create the targeted batch, then update the source id
                batchHelper.createSortingBatch(source, catchBatch, target);

                // push back id of created sortingBatch
                source.setId(target.getId());

                if (log.isDebugEnabled()) {
                    log.debug("Create frequency sortingBatch(" + rankOrder + "): " + target.getId());
                }
            } else {

                // Existing batch
                target = batchHelper.loadSortingBatch(source.getIdAsInt(), catchBatch);

                // Fill the sorting batch from the source
                beanToEntity0(source, target, parentBatch, rankOrder);

                // Add the batch into a list (will be update later, using this list)
                batchsToUpdate.add(target);

                // Remove id from id to remove
                notUpdatedChildIds.remove(target.getId());

                if (log.isDebugEnabled()) {
                    log.debug("Update frequency sortingBatch(" + rankOrder + "): " + target.getId());
                }
            }
        }

        if (CollectionUtils.isNotEmpty(batchsToUpdate)) {

            // update some batchs
            batchHelper.updateSortingBatch(batchsToUpdate, catchBatch);
        }

        if (CollectionUtils.isNotEmpty(notUpdatedChildIds)) {

            // Remove obsolete frequencies
            for (Integer batchId : notUpdatedChildIds) {

                if (log.isDebugEnabled()) {
                    log.debug("Remove obsolete frequency sortingBatch: " + batchId);
                }
                batchHelper.removeWithChildren(batchId, catchBatch);
            }
        }

        return Collections.unmodifiableList(notNullFrequencies);
    }

    //------------------------------------------------------------------------//
    //-- Internal methods                                                   --//
    //------------------------------------------------------------------------//

    protected void beanToEntity0(SpeciesBatch source,
                                 SortingBatch target,
                                 Integer parentBatchId,
                                 CatchBatch catchBatch,
                                 boolean computeRankOrder) {

        Objects.requireNonNull(source.getFishingOperation());
        Objects.requireNonNull(source.getFishingOperation().getId());

        // If parent and root need to be set
        if (target.getId() == null
                || target.getRootBatch() == null
                || (target.getParentBatch() != null && !target.getParentBatch().getId().equals(parentBatchId))) {

            getBatchTreeHelper().setBatchParents(source.getSampleCategoryId(),
                                                 source.getSampleCategoryValue(),
                                                 target,
                                                 parentBatchId,
                                                 catchBatch);
        }

        // --- RankOrder (initialize once, at creation) --- //
        {
            if (target.getRankOrder() == null) {

                short rankOrder;

                if (computeRankOrder) {

                    // Start rank order at 1
                    rankOrder = (short) 1;
                    //FIXME : tchemit-2015-04-04 This code can not be used to save multiple batches at the same time, since it will always give the
                    //FIXME : tchemit-2015-04-04 same values for all batches
                    if (source.getParentBatch() != null && CollectionUtils.isNotEmpty(source.getParentBatch().getChildBatchs())) {
                        int maxRankOrder = 0;
                        for (SpeciesBatch batch : source.getParentBatch().getChildBatchs()) {
                            Integer r = batch.getRankOrder();
                            if (r != null && r > maxRankOrder) {
                                maxRankOrder = r;
                            }
                        }
                        rankOrder += (short) maxRankOrder;

                    } else {

                        rankOrder = batchHelper.computeRankOrder(target);

                    }

                } else {

                    Preconditions.checkState(source.getRankOrder() != null, "Not using computeRankOrder requires source rankOrder to be not null, but was on batch: " + source);
                    rankOrder = (short) (int) source.getRankOrder();

                }

                target.setRankOrder(rankOrder);

            }
        }

        // --- Force subgroup count to '1', as Allegro --- //
        target.setSubgroupCount(1f);

        // --- Individual count --- //
        target.setIndividualCount(source.getNumber());

        // --- Comments --- //
        target.setComments(source.getComment());

        // --- Exhaustive inventory (always true under a species batch) --- //
        target.setExhaustiveInventory(true);

        // --- Species --- //
        {
            ReferenceTaxon referenceTaxon;
            if (source.getSpecies() == null || parentBatchId != null) {
                referenceTaxon = null;
            } else {
                referenceTaxon = load(ReferenceTaxonImpl.class, source.getSpecies().getReferenceTaxonId());
            }
            target.setReferenceTaxon(referenceTaxon);
        }

        // --- QualityFlag --- //
        {
            String qualityFlag;
            if (source.isSpeciesToConfirm()) {
                qualityFlag = QualityFlagCode.DOUBTFUL.getValue();
            } else {
                qualityFlag = QualityFlagCode.NOTQUALIFIED.getValue();
            }
            target.setQualityFlag(load(QualityFlagImpl.class, qualityFlag));
        }

        Float weight = source.getWeight();
        Float sampleCategoryWeight = source.getSampleCategoryWeight();

        // --- Sampling Ratio + QuantificationMeasurement --- //
        getBatchTreeHelper().setWeightAndSampleRatio(target, weight, sampleCategoryWeight);

        // --- Sorting measurement --- //
        {
            Collection<SortingMeasurement> sortingMeasurements = target.getSortingMeasurements();
            Set<SortingMeasurement> notChangedSortingMeasurements = Sets.newHashSet();
            if (sortingMeasurements != null) {
                notChangedSortingMeasurements.addAll(sortingMeasurements);
            }

            if (source.getSampleCategoryId() != null && source.getSampleCategoryValue() != null) {
                Integer pmfmId = source.getSampleCategoryId();
                // Do not store sorting measurement if pmfm = SORTED (already store in an ancestor batch)
                if (!pmfmId.equals(PmfmId.SORTED_UNSORTED.getValue())) {
                    SortingMeasurement sortingMeasurement = measurementPersistenceHelper.setSortingMeasurement(
                            target,
                            pmfmId,
                            source.getSampleCategoryValue());
                    notChangedSortingMeasurements.remove(sortingMeasurement);
                }
            }
            if (sortingMeasurements != null) {
                sortingMeasurements.removeAll(notChangedSortingMeasurements);
            }
        }

    }

    private void beanToEntity0(SpeciesBatchFrequency source,
                               SortingBatch target,
                               SortingBatch parentBatch,
                               short rankOrder) {
        Preconditions.checkNotNull(source.getBatch());
        Preconditions.checkNotNull(source.getBatch().getId());

        // If parent and root need to be set
        if (target.getId() == null
                || target.getRootBatch() == null
                || (target.getParentBatch() != null && !target.getParentBatch().getId().equals(parentBatch.getId()))) {

            target.setParentBatch(parentBatch);
            target.setRootBatch(parentBatch.getRootBatch());
        }

        // --- RankOrder --- //
        target.setRankOrder(rankOrder);

        // --- Individual count --- //
        target.setIndividualCount(source.getNumber());

        // --- Species --- //
        target.setReferenceTaxon(null);

        // --- QualityFlag --- //
        target.setQualityFlag(parentBatch.getQualityFlag());

        // --- Exhaustive inventory (always true under a species batch) --- //
        target.setExhaustiveInventory(true);

        // --- Sampling Ratio + QuantificationMeasurement --- //
        getBatchTreeHelper().setWeightAndSampleRatio(target, source.getWeight(), null);

        // --- Sorting measurement --- //
        {
            Collection<SortingMeasurement> sortingMeasurements = target.getSortingMeasurements();
            Set<SortingMeasurement> notChangedSortingMeasurements = Sets.newHashSet();
            if (sortingMeasurements != null) {
                notChangedSortingMeasurements.addAll(sortingMeasurements);
            }
            if ((source.getLengthStepCaracteristic() != null && source.getLengthStep() != null)) {
                Integer pmfmId = source.getLengthStepCaracteristic().getIdAsInt();
                SortingMeasurement sortingMeasurement = measurementPersistenceHelper.setSortingMeasurement(target, pmfmId,
                                                                                                           source.getLengthStep());
                notChangedSortingMeasurements.remove(sortingMeasurement);
            }
            if (sortingMeasurements != null) {
                sortingMeasurements.removeAll(notChangedSortingMeasurements);
            }
        }

    }


    private SpeciesBatch entityToBean(SampleCategoryModel sampleCategoryModel,
                                      SortingBatch source,
                                      SpeciesBatch target) {

        Preconditions.checkNotNull(target.getSpecies());

        target.setId(source.getId().toString());

        // Rank order
        target.setRankOrder(Integer.valueOf(source.getRankOrder()));

        // Individual count
        target.setNumber(source.getIndividualCount());

        // Convert database weight (and sampling ratio) into UI weight and sampleCategoryWeight
        if (source.getWeight() != null && source.getWeightBeforeSampling() == null) {
            target.setSampleCategoryWeight(source.getWeight());
        } else {
            target.setWeight(source.getWeight());
            target.setSampleCategoryWeight(source.getWeightBeforeSampling());

//            if (Objects.equals(source.getWeight(), source.getWeightBeforeSampling())) {
//
//                // after a allegro synchronize, can happen, we do not use quantification measurement on a not leaf node
//                // the weight comes from sampleRatioText, but in facts there only one weight...
//                target.setWeight(null);
//
//            }
        }

//        if (CollectionUtils.isNotEmpty(source.getChildBatchs()) && target.getWeight() != null) {
//
//            // can't use this sample weight on a node
//            // the weight comes from sampleRatioText, but must NOT be used here
//            target.setWeight(null);
//
//        }


        // Comments
        target.setComment(source.getComments());

        // Sample category type (only one is applied)
        SortingMeasurement sm = null;
        if (source.getSortingMeasurements().size() == 1) {
            sm = source.getSortingMeasurements().iterator().next();
        } else if (source.getReferenceTaxon() != null && source.getReferenceTaxon().getId() != null) {
            sm = measurementPersistenceHelper.getInheritedSortingMeasurement(source);
        }
        if (sm != null) {

            boolean isFrequency = isFrequencyBatch(sampleCategoryModel, source);

            if (!isFrequency) {
                Integer qualitativeId = null;
                if (sm.getQualitativeValue() != null) {
                    qualitativeId = sm.getQualitativeValue().getId();
                }
                setSampleCategoryQualitative(
                        target,
                        sm.getPmfm().getId(),
                        sm.getNumericalValue(),
                        sm.getAlphanumericalValue(),
                        qualitativeId);
            }
        }

        if (target.getSampleCategoryId() != null) {
            List<SpeciesBatch> targetChilds = Lists.newArrayList();
            for (Batch batch : source.getChildBatchs()) {
                SortingBatch sourceChild = (SortingBatch) batch;
                SpeciesBatch targetChild = SpeciesBatchs.newInstance(target);
                targetChild.setSpecies(target.getSpecies());
                entityToBean(sampleCategoryModel, sourceChild, targetChild);
                if (log.isDebugEnabled()) {
                    log.debug("Loaded CatchBatch (Vrac|Hors Vrac) > Species > " + targetChild.getSpecies().getReferenceTaxonId() + " : " + target.getId());
                }
                if (targetChild.getSampleCategoryValue() != null) {
                    targetChilds.add(targetChild);
                    targetChild.setParentBatch(target);
                }
            }
            target.setChildBatchs(targetChilds);

        }

        //FIXME tchemit-2014-08-29 We can only do this if not an a leaf node (means with no frequencies...)
        //FIXME tchemit-2014-08-29 But need to see if this is really need to do that .
        // see https://forge.codelutin.com/issues/5698

        if (CollectionUtils.isNotEmpty(source.getChildBatchs()) && target.getWeight() != null) {

            SortingBatch childBatch = (SortingBatch) Iterables.get(source.getChildBatchs(), 0);

            boolean isFrequency = isFrequencyBatch(sampleCategoryModel, childBatch);

            if (!isFrequency) {

                // can't use this sample weight on a node
                // the weight comes from sampleRatioText, but must NOT be used here
                // but we can only do this if childs are not frequencies

                target.setWeight(null);

            }

        }

        QualityFlag qualityFlag = source.getQualityFlag();
        target.setSpeciesToConfirm(qualityFlag != null && QualityFlagCode.DOUBTFUL.getValue().equals(qualityFlag.getCode()));

        return target;

    }

//    private  short computeRankOrder(SortingBatch target) {
//
//        // Start rank order at 1, nothing before it
//        short rankOrder = (short) 1;
//        if (target.getParentBatch() != null && target.getParentBatch().getChildBatchs() != null) {
//            int maxRankOrder = 0;
//            for (Batch batch : target.getParentBatch().getChildBatchs()) {
//                Short r = batch.getRankOrder();
//                if (r != null && r > maxRankOrder) {
//                    maxRankOrder = r;
//                }
//            }
//            rankOrder += maxRankOrder;
//        }
//        return rankOrder;
//
//    }

    private void setSampleCategoryQualitative(SpeciesBatch target,
                                              Integer pmfmId,
                                              Float numericalvalue,
                                              String alphanumericalValue,
                                              Integer qualitativeValueId) {
        // skip if null or corresponding to the SORTING_TYPE PMFM (Espèce, Benthos, Plancton, etc.)
        if (pmfmId == null || pmfmId.equals(SpeciesBatchTreeHelperSupport.SORTING_TYPE_ID)) {
            return;
        }
        SampleCategoryModel sampleCategoryModel = getSampleCategoryModel();

        boolean isSamplingCategory = sampleCategoryModel.containsCategoryId(pmfmId);
        Preconditions.checkNotNull(isSamplingCategory, "Unable to find corresponding SampleCategoryEnum for PMFM.ID : " + pmfmId);

        target.setSampleCategoryId(pmfmId);
        Serializable categoryValue = getSampleCategoryQualitative(
                pmfmId,
                numericalvalue,
                alphanumericalValue,
                qualitativeValueId);
        target.setSampleCategoryValue(categoryValue);
    }

    /**
     * Check if the given {@code sortingBatch} is a frequency one.
     *
     * We test that:
     * <ul>
     * <li>batch has exactly one measurement</li>
     * <li>the measurement pmfm is not a sample category</li>
     * </ul>
     *
     * @param sampleCategoryModel model of authorized sample categories
     * @param sortingBatch        batch to check
     * @return {@code true} if given batch is a frequency batch,
     * {@code false} otherwise.
     */
    private boolean isFrequencyBatch(SampleCategoryModel sampleCategoryModel, SortingBatch sortingBatch) {
        boolean result = false;
        if (sortingBatch.getSortingMeasurements().size() == 1) {
            SortingMeasurement sm
                    = sortingBatch.getSortingMeasurements().iterator().next();
            Pmfm pmfm = sm.getPmfm();

            result = sortingBatch.getIndividualCount() != null &&
                    !sampleCategoryModel.containsCategoryId(pmfm.getId());
        }
        return result;
    }

    private Serializable getSampleCategoryQualitative(Integer pmfmId,
                                                      Float numericalvalue,
                                                      String alphanumericalValue,
                                                      Integer qualitativeValueId) {

        if (numericalvalue != null) {
            return numericalvalue;
        }
        if (alphanumericalValue != null) {
            return alphanumericalValue;
        }

        Caracteristic caracteristic = caracteristicService.getCaracteristic(pmfmId);
        if (caracteristic == null || caracteristic.getCaracteristicType() != CaracteristicType.QUALITATIVE) {
            return null;
        }
        CaracteristicQualitativeValue value = null;
        for (CaracteristicQualitativeValue qv : caracteristic.getQualitativeValue()) {
            if (qualitativeValueId.equals(qv.getIdAsInt())) {
                value = qv;
                break;
            }
        }

        return value;
    }

    private void entityToBatchFrequency(SortingBatch source,
                                        SpeciesBatchFrequency target) {

        target.setId(source.getId());

        // Rank order
        target.setRankOrder(Integer.valueOf(source.getRankOrder()));

        target.setNumber(source.getIndividualCount());
        target.setWeight(source.getWeight());

        Preconditions.checkState(source.getSortingMeasurements().size() == 1, "SortingBatch [" + source.getId() + "] need exactly one sortingMeasurement (to store the length step category), but had " + source.getSortingMeasurements().size());
        SortingMeasurement sm = source.getSortingMeasurements().iterator().next();
        Preconditions.checkNotNull(sm.getPmfm(), "SortingMeasurement [" + sm.getId() + "] can not have a null pmfm");
        Preconditions.checkNotNull(sm.getPmfm().getId(), "SortingMeasurement [" + sm.getId() + "] can not have a pmfm with null id");

        // Length step category
        Caracteristic lengthStepCaracteristic = caracteristicService.getCaracteristic(sm.getPmfm().getId());
        target.setLengthStepCaracteristic(lengthStepCaracteristic);

        // Length
        target.setLengthStep(sm.getNumericalValue());
    }

    private List<SortingBatch> getFrequencyChilds(SortingBatch sortingBatch) {
        List<SortingBatch> result = Lists.newArrayList();

        SampleCategoryModel sampleCategoryModel = getSampleCategoryModel();

        for (Batch batch : sortingBatch.getChildBatchs()) {
            SortingBatch child = (SortingBatch) batch;
            if (isFrequencyBatch(sampleCategoryModel, child)) {
                result.add(child);
            }
        }
        return result;
    }

    private List<SortingBatch> getFrequencies(Integer batchId) {
        Preconditions.checkNotNull(batchId);
        CatchBatch catchBatch = batchHelper.getRootCatchBatchByBatchId(batchId);
        SortingBatch sortingBatch = batchHelper.getSortingBatchById(catchBatch, batchId);

        return getFrequencyChilds(sortingBatch);
    }

    private void deleteSpeciesSubBatch(Integer speciesBatchId) {
        Preconditions.checkNotNull(speciesBatchId);

        CatchBatch catchBatch = batchHelper.getRootCatchBatchByBatchId(speciesBatchId);
        synchronizationStatusHelper.setDirty(catchBatch);

        SortingBatch sortingBatch = batchHelper.getSortingBatchById(catchBatch, speciesBatchId);

        // get his children
        Collection<Batch> childBatchs = sortingBatch.getChildBatchs();

        if (CollectionUtils.isNotEmpty(childBatchs)) {

            for (Batch childBatch : childBatchs) {

                // delete this child and all his children
                Integer childBatchId = childBatch.getId();

                if (log.isDebugEnabled()) {
                    log.debug("Delete child [" + childBatchId + "] of species batch: " + speciesBatchId);
                }
                batchHelper.removeWithChildren(childBatchId);
            }
        }
    }

    private void changeBatchSpecies(Integer batchId, Species species) {

        Preconditions.checkNotNull(batchId);
        Preconditions.checkNotNull(species);
        Preconditions.checkNotNull(species.getReferenceTaxonId());

        CatchBatch catchBatch = batchHelper.getRootCatchBatchByBatchId(batchId);
        synchronizationStatusHelper.setDirty(catchBatch);

        batchHelper.setSortingBatchReferenceTaxon(batchId, species);
    }
}

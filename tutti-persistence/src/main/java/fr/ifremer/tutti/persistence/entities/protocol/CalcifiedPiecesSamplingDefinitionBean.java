package fr.ifremer.tutti.persistence.entities.protocol;

/*
 * #%L
 * Tutti :: Persistence
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2012 - 2016 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.MoreObjects;

import java.util.Objects;

public class CalcifiedPiecesSamplingDefinitionBean extends AbstractCalcifiedPiecesSamplingDefinitionBean {

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CalcifiedPiecesSamplingDefinitionBean that = (CalcifiedPiecesSamplingDefinitionBean) o;
        return minSize == that.minSize &&
                Objects.equals(maxSize, that.maxSize) &&
                Objects.equals(maturity, that.maturity);
    }

    @Override
    public int hashCode() {
        return Objects.hash(minSize, maxSize, maturity);
    }

    @Override
    public String toString() {
        MoreObjects.ToStringHelper toStringHelper =
                MoreObjects.toStringHelper(CalcifiedPiecesSamplingDefinition.class)
                           .add(PROPERTY_MIN_SIZE, minSize)
                           .add(PROPERTY_MAX_SIZE, maxSize == null ? "∞" : maxSize)
                           .add(PROPERTY_SEX, sex)
                           .add(PROPERTY_SAMPLING_INTERVAL, samplingInterval);

        if (maturity != null) {
            toStringHelper.add(PROPERTY_MATURITY, maturity);
        }
        if (maxByLenghtStep != null && maxByLenghtStep > 0) {
            toStringHelper.add(PROPERTY_MAX_BY_LENGHT_STEP, maxByLenghtStep);
        }
        if (operationLimitation != null && operationLimitation > 0) {
            toStringHelper.add(PROPERTY_OPERATION_LIMITATION, operationLimitation);
        }
        if (zoneLimitation != null && zoneLimitation > 0) {
            toStringHelper.add(PROPERTY_ZONE_LIMITATION, zoneLimitation);
        }

        return toStringHelper.toString();
    }
}

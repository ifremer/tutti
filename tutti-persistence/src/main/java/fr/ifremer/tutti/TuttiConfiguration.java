package fr.ifremer.tutti;

/*
 * #%L
 * Tutti :: Persistence
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Charsets;
import fr.ifremer.adagio.core.config.AdagioConfiguration;
import fr.ifremer.adagio.core.config.AdagioConfigurationOption;
import fr.ifremer.tutti.persistence.entities.data.SampleCategoryModel;
import fr.ifremer.tutti.type.CoordinateEditorType;
import fr.ifremer.tutti.type.WeightUnit;
import fr.ifremer.tutti.util.BeepFrequency;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.config.ApplicationConfig;
import org.nuiton.config.ApplicationConfigHelper;
import org.nuiton.config.ApplicationConfigProvider;
import org.nuiton.config.ArgumentsParserException;
import org.nuiton.config.ConfigOptionDef;
import org.nuiton.jaxx.application.ApplicationConfiguration;
import org.nuiton.jaxx.application.ApplicationIOUtil;
import org.nuiton.jaxx.application.ApplicationTechnicalException;
import org.nuiton.version.Version;

import javax.swing.KeyStroke;
import java.awt.Color;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import static org.nuiton.i18n.I18n.t;

/**
 * Created on 11/24/13.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.0
 */
public class TuttiConfiguration extends ApplicationConfiguration {

    /** Logger. */
    private static final Log log = LogFactory.getLog(TuttiConfiguration.class);

    private static TuttiConfiguration instance;

    private final boolean windowsOS;

    public static TuttiConfiguration getInstance() {
        return instance;
    }

    public static void setInstance(TuttiConfiguration instance) {
        TuttiConfiguration.instance = instance;
    }

    public static Set<ApplicationConfigProvider> getDefaultApplicationConfig(ApplicationConfig applicationConfig) {

        // get all config providers
        Set<ApplicationConfigProvider> providers =
                ApplicationConfigHelper.getProviders(null,
                                                     null,
                                                     null,
                                                     true);

        // load all default options
        ApplicationConfigHelper.loadAllDefaultOption(applicationConfig,
                                                     providers);

        // Override some adagio default config
        Map<ConfigOptionDef, ConfigOptionDef> translateOptions = new HashMap<>();

        translateOptions.put(TuttiConfigurationOption.BASEDIR, AdagioConfigurationOption.BASEDIR);
        translateOptions.put(TuttiConfigurationOption.DATA_DIRECTORY, AdagioConfigurationOption.DATA_DIRECTORY);
        translateOptions.put(TuttiConfigurationOption.DB_NAME, AdagioConfigurationOption.DB_NAME);
        translateOptions.put(TuttiConfigurationOption.JDBC_USERNAME, AdagioConfigurationOption.JDBC_USERNAME);
        translateOptions.put(TuttiConfigurationOption.JDBC_PASSWORD, AdagioConfigurationOption.JDBC_PASSWORD);
        translateOptions.put(TuttiConfigurationOption.JDBC_URL, AdagioConfigurationOption.JDBC_URL);

        for (Map.Entry<ConfigOptionDef, ConfigOptionDef> entry : translateOptions.entrySet()) {
            ConfigOptionDef tuttiKey = entry.getKey();
            ConfigOptionDef adagioKey = entry.getValue();
            String tuttiOptionValue = String.format("${%s}", tuttiKey.getKey());
            applicationConfig.setDefaultOption(adagioKey.getKey(), tuttiOptionValue);
        }

        applicationConfig.setDefaultOption(AdagioConfigurationOption.DB_ENUMERATION_RESOURCE.getKey(),
                                           "classpath*:adagio-db-enumerations.properties,classpath*:tutti-db-enumerations.properties");

        return providers;
    }

    protected final String[] optionKeyToNotSave;

    protected File configFile;

    protected AdagioConfiguration adagioConfig;

    public TuttiConfiguration(ApplicationConfig applicationConfig) {
        super(applicationConfig);
        optionKeyToNotSave = null;
        this.windowsOS = System.getProperty("os.name").startsWith("Windows");
    }

    public TuttiConfiguration(String file, String... args) {
        super(new ApplicationConfig());
        this.windowsOS = System.getProperty("os.name").startsWith("Windows");
        applicationConfig.setEncoding(Charsets.UTF_8.name());
        Set<ApplicationConfigProvider> providers = getDefaultApplicationConfig(applicationConfig);

        // get all transient and final option keys
        Set<String> optionToSkip =
                ApplicationConfigHelper.getTransientOptionKeys(providers);

        if (log.isDebugEnabled()) {
            log.debug("Option that won't be saved: " + optionToSkip);
        }
        optionKeyToNotSave = optionToSkip.toArray(new String[optionToSkip.size()]);

        applicationConfig.setConfigFileName(file);

        applicationConfig.addAlias("--launch-mode", "--option", TuttiConfigurationOption.LAUNCH_MODE.getKey());
        applicationConfig.addAlias("--basedir", "--option", TuttiConfigurationOption.BASEDIR.getKey());
        applicationConfig.addAlias("--config-path", "--option", "config.path");
        applicationConfig.addAlias("--db-sanity", "--option", TuttiConfigurationOption.DB_SANITY.getKey());


        try {
            applicationConfig.parse(args);

        } catch (ArgumentsParserException e) {
            throw new ApplicationTechnicalException(t("tutti.config.parse.error"), e);
        }

        //TODO Review this, this is very dirty to do this...
        File tuttiBasedir = applicationConfig.getOptionAsFile(
                TuttiConfigurationOption.BASEDIR.getKey());

        if (tuttiBasedir == null) {
            tuttiBasedir = new File("");
        }
        if (!tuttiBasedir.isAbsolute()) {
            tuttiBasedir = new File(tuttiBasedir.getAbsolutePath());
        }
        if (tuttiBasedir.getName().equals("..")) {
            tuttiBasedir = tuttiBasedir.getParentFile().getParentFile();
        }
        if (tuttiBasedir.getName().equals(".")) {
            tuttiBasedir = tuttiBasedir.getParentFile();
        }
        if (log.isInfoEnabled()) {
            log.info("Application basedir: " + tuttiBasedir);
        }
        applicationConfig.setOption(
                TuttiConfigurationOption.BASEDIR.getKey(),
                tuttiBasedir.getAbsolutePath());
    }

    public void initConfig() {

        // Give the same applicationConfig to adagio
        adagioConfig = new AdagioConfiguration(applicationConfig);
        AdagioConfiguration.setInstance(adagioConfig);

        File directory = getDbDirectory();
        ApplicationIOUtil.forceMkdir(directory, t("tutti.io.mkDir.error", directory));
        directory = getDbAttachmentDirectory();
        ApplicationIOUtil.forceMkdir(directory, t("tutti.io.mkDir.error", directory));
        directory = getProtocolDirectory();
        ApplicationIOUtil.forceMkdir(directory, t("tutti.io.mkDir.error", directory));
        directory = getDbBackupDirectory();
        ApplicationIOUtil.forceMkdir(directory, t("tutti.io.mkDir.error", directory));

        File dataDirectory = getDataDirectory();
        ApplicationIOUtil.forceMkdir(dataDirectory, t("tutti.io.mkDir.error", dataDirectory));

        File tmpDirectory = getTmpDirectory();
        if (tmpDirectory.exists()) {

            try {
                if (tmpDirectory.isFile()) {
                    ApplicationIOUtil.deleteFile(tmpDirectory, t("tutti.io.deleteTempDirectory.error", tmpDirectory));
                } else {
                    ApplicationIOUtil.deleteDirectory(tmpDirectory, t("tutti.io.deleteTempDirectory.error", tmpDirectory));
                }
            } catch (Exception e) {
                if (log.isErrorEnabled()) {
                    log.error("Can't delete temporary directory, but still go forward...",e);
                }
            }
        }
        ApplicationIOUtil.forceMkdir(tmpDirectory, t("tutti.io.mkDir.error", tmpDirectory));

        ApplicationIOUtil.forceMkdir(getReportBackupDirectory(), t("tutti.io.mkDir.error", getReportBackupDirectory()));
        ApplicationIOUtil.forceMkdir(getGenericFormatReportBackupDirectory(), t("tutti.io.mkDir.error", getGenericFormatReportBackupDirectory()));
    }

    public void prepareDirectories() {

        File dataDirectory = getDataDirectory();
        ApplicationIOUtil.forceMkdir(
                dataDirectory,
                t("tutti.io.mkDir.error", dataDirectory));

        File tmpDirectory = getTmpDirectory();
        if (tmpDirectory.exists()) {

            try {
                ApplicationIOUtil.deleteDirectory(
                        tmpDirectory,
                        t("tutti.io.deleteTempDirectory.error", tmpDirectory));
            } catch (Exception e) {
                if (log.isErrorEnabled()) {
                    log.error("Can't delete temporary directory, but still go forward...",e);
                }
            }
            
        }
        ApplicationIOUtil.forceMkdir(
                tmpDirectory,
                t("tutti.io.mkDir.error", tmpDirectory));

        ApplicationIOUtil.forceMkdir(
                getReportBackupDirectory(),
                t("tutti.io.mkDir.error", getReportBackupDirectory()));

        ApplicationIOUtil.forceMkdir(
                getExportBackupDirectory(),
                t("tutti.io.mkDir.error", getExportBackupDirectory()));
    }

    public File newTempFile(String basename) {
        return newTempFile(basename, "");
    }

    public File newTempFile(String basename, String extension) {
        return new File(getTmpDirectory(), basename + "_" + System.nanoTime() + extension);
    }

    public boolean isDbExists() {
        File f = new File(getDbDirectory(), getDbName() + ".data");
        return f.exists();
    }

    public File getConfigFile() {
        if (configFile == null) {
            File dir = getBasedir();
            if (dir == null || !dir.exists() || !isFullLaunchMode()) {
                dir = new File(applicationConfig.getUserConfigDirectory());
            }
            configFile = new File(dir, applicationConfig.getConfigFileName());
        }
        return configFile;
    }

    public String getHelpResourceWithLocale(String value) {
        File directory = new File(getHelpDirectory(),
                                  getI18nLocale().getLanguage());
        File result = new File(directory, value);
        return result.toString();
    }

    public void save() {

        File file = getConfigFile();
        if (log.isInfoEnabled()) {
            log.info("Save configuration at: " + file);
        }
        try {
            applicationConfig.save(file, false, optionKeyToNotSave);
        } catch (IOException e) {
            throw new ApplicationTechnicalException(
                    t("tutti.config.save.error", file), e);
        }
    }

    @Override
    public String toString() {
        return ReflectionToStringBuilder.toString(this);
    }

    //------------------------------------------------------------------------//
    //--- Option setter ------------------------------------------------------//
    //------------------------------------------------------------------------//

    public void setSampleCategoryModel(SampleCategoryModel model) {
        applicationConfig.setOption(TuttiConfigurationOption.SAMPLE_CATEGORY_MODEL.getKey(), String.valueOf(model));
    }

    public void setCsvSeparator(char c) {
        applicationConfig.setOption(
                TuttiConfigurationOption.CSV_SEPARATOR.getKey(), c + "");
    }

    public void setExportCountry(String exportCountryId) {
        applicationConfig.setOption(
                TuttiConfigurationOption.EXPORT_COUNTRY_ID.getKey(), exportCountryId);
    }

    public void setI18nLocale(Locale locale) {
        applicationConfig.setOption(TuttiConfigurationOption.I18N_LOCALE.getKey(), locale.toString());
    }

    public void setProgramId(String programId) {
        if (programId == null) {
            programId = "";
        }
        applicationConfig.setOption(TuttiConfigurationOption.PROGRAM_ID.getKey(), programId);
    }

    public void setCruiseId(Integer cruiseId) {
        if (cruiseId == null) {
            applicationConfig.setOption(TuttiConfigurationOption.CRUISE_ID.getKey(), "");
        }else {
            applicationConfig.setOption(TuttiConfigurationOption.CRUISE_ID.getKey(), cruiseId.toString());
        }
    }

    public void setProtocolId(String protocolId) {
        if (protocolId == null) {
            protocolId = "";
        }
        applicationConfig.setOption(TuttiConfigurationOption.PROTOCOL_ID.getKey(), protocolId);
    }

    //------------------------------------------------------------------------//
    //--- Option getter ------------------------------------------------------//
    //------------------------------------------------------------------------//

    public File getDbDirectory() {
        return applicationConfig.getOptionAsFile(TuttiConfigurationOption.DB_DIRECTORY.getKey());
    }

    public File getDbAttachmentDirectory() {
        return applicationConfig.getOptionAsFile(TuttiConfigurationOption.DB_ATTACHMENT_DIRECTORY.getKey());
    }

    public File getCacheDirectory() {
        return applicationConfig.getOptionAsFile(TuttiConfigurationOption.DB_CACHE_DIRECTORY.getKey());
    }

    public File getDbBackupDirectory() {
        return applicationConfig.getOptionAsFile(TuttiConfigurationOption.DB_BACKUP_DIRECTORY.getKey());
    }

    public File getProtocolDirectory() {
        return applicationConfig.getOptionAsFile(TuttiConfigurationOption.DB_PROTOCOL_DIRECTORY.getKey());
    }

    public String getDbName() {
        return applicationConfig.getOption(TuttiConfigurationOption.DB_NAME.getKey());
    }

    public SampleCategoryModel getSampleCategoryModel() {
        return applicationConfig.getOption(SampleCategoryModel.class, TuttiConfigurationOption.SAMPLE_CATEGORY_MODEL.getKey());
    }

    public String getJdbcUrl() {
        return applicationConfig.getOption(TuttiConfigurationOption.JDBC_URL.getKey());
    }

    public Class getJdbcDriver() {
        return applicationConfig.getOptionAsClass(TuttiConfigurationOption.JDBC_DRIVER.getKey());
    }

    public String getJdbcUsername() {
        return applicationConfig.getOption(TuttiConfigurationOption.JDBC_USERNAME.getKey());
    }

    public String getJdbcPassword() {
        return applicationConfig.getOption(TuttiConfigurationOption.JDBC_PASSWORD.getKey());
    }

    public Class getHibernateDialect() {
        return applicationConfig.getOptionAsClass(TuttiConfigurationOption.HIBERNATE_DIALECT.getKey());
    }

    public boolean isSanityDb() {
        return applicationConfig.getOptionAsBoolean(TuttiConfigurationOption.DB_SANITY.getKey());
    }

    public boolean isHibernateShowSql() {
        return applicationConfig.getOptionAsBoolean(TuttiConfigurationOption.HIBERNATE_SHOW_SQL.getKey());
    }

    public boolean isHibernateUseSqlComment() {
        return applicationConfig.getOptionAsBoolean(TuttiConfigurationOption.HIBERNATE_USE_SQL_COMMENT.getKey());
    }

    public boolean isHibernateFormatSql() {
        return applicationConfig.getOptionAsBoolean(TuttiConfigurationOption.HIBERNATE_FORMAT_SQL.getKey());
    }

    public boolean isShowBatchLog() {
        return applicationConfig.getOptionAsBoolean(TuttiConfigurationOption.SHOW_BATCH_LOG.getKey());
    }

    public boolean isShowMemoryUsage() {
        return applicationConfig.getOptionAsBoolean(TuttiConfigurationOption.SHOW_MEMORY_USAGE.getKey());
    }

    public boolean isGenericFormatImportSkipBackup() {
        return applicationConfig.getOptionAsBoolean(TuttiConfigurationOption.GENERIC_FORMAT_IMPORT_SKIP_BACKUP.getKey());
    }

    public int getGenericFormatImportMaximumRowsInErrorsPerFile() {
        return applicationConfig.getOptionAsInt(TuttiConfigurationOption.GENERIC_FORMAT_IMPORT_MAXIMUM_ROWS_IN_ERROR_PER_FILE.getKey());
    }

    public boolean isImportDbSkipBackup() {
        return applicationConfig.getOptionAsBoolean(TuttiConfigurationOption.IMPORT_DB_SKIP_BACKUP.getKey());
    }

    @Override
    public String getApplicationName() {
        return "Allegro Campagne";
    }

    /** @return {@link TuttiConfigurationOption#VERSION} value */
    @Override
    public Version getVersion() {
        return applicationConfig.getOptionAsVersion(TuttiConfigurationOption.VERSION.getKey());
    }

    /** @return {@link TuttiConfigurationOption#SITE_URL} value */
    public URL getSiteUrl() {
        return applicationConfig.getOptionAsURL(TuttiConfigurationOption.SITE_URL.getKey());
    }

    /** @return {@link TuttiConfigurationOption#ORGANIZATION_NAME} value */
    public String getOrganizationName() {
        return applicationConfig.getOption(TuttiConfigurationOption.ORGANIZATION_NAME.getKey());
    }

    /** @return {@link TuttiConfigurationOption#INCEPTION_YEAR} value */
    public int getInceptionYear() {
        return applicationConfig.getOptionAsInt(TuttiConfigurationOption.INCEPTION_YEAR.getKey());
    }

    /** @return {@link TuttiConfigurationOption#BASEDIR} value */
    public File getBasedir() {
        return applicationConfig.getOptionAsFile(TuttiConfigurationOption.BASEDIR.getKey());
    }

    /** @return {@link TuttiConfigurationOption#DATA_DIRECTORY} value */
    public File getDataDirectory() {
        return applicationConfig.getOptionAsFile(TuttiConfigurationOption.DATA_DIRECTORY.getKey());
    }

    /** @return {@link TuttiConfigurationOption#REPORT_HOME_DIRECTORY} value */
    public File getReportHomeDirectory() {
        return applicationConfig.getOptionAsFile(TuttiConfigurationOption.REPORT_HOME_DIRECTORY.getKey());
    }

    /** @return {@link TuttiConfigurationOption#REPORT_DIRECTORY} value */
    public File getReportDirectory() {
        return applicationConfig.getOptionAsFile(TuttiConfigurationOption.REPORT_DIRECTORY.getKey());
    }

    /** @return {@link TuttiConfigurationOption#EXPORT_BACKUP_DIRECTORY} value */
    public File getExportBackupDirectory() {
        return applicationConfig.getOptionAsFile(TuttiConfigurationOption.EXPORT_BACKUP_DIRECTORY.getKey());
    }


    /** @return {@link TuttiConfigurationOption#REPORT_LOG_DIRECTORY} value */
    public File getReportLogDirectory() {
        return applicationConfig.getOptionAsFile(TuttiConfigurationOption.REPORT_LOG_DIRECTORY.getKey());
    }

    /** @return {@link TuttiConfigurationOption#REPORT_BACKUP_DIRECTORY} value */
    public File getReportBackupDirectory() {
        return applicationConfig.getOptionAsFile(TuttiConfigurationOption.REPORT_BACKUP_DIRECTORY.getKey());
    }

    /** @return {@link TuttiConfigurationOption#GENERIC_FORMAT_REPORT_BACKUP_DIRECTORY} value */
    public File getGenericFormatReportBackupDirectory() {
        return applicationConfig.getOptionAsFile(TuttiConfigurationOption.GENERIC_FORMAT_REPORT_BACKUP_DIRECTORY.getKey());
    }

    public File getTmpDirectory() {
        return applicationConfig.getOptionAsFile(TuttiConfigurationOption.TMP_DIRECTORY.getKey());
    }

    public char getCsvSeparator() {
        return applicationConfig.getOption(
                TuttiConfigurationOption.CSV_SEPARATOR.getKey()).charAt(0);
    }

    public String getExportCountryId() {
        return applicationConfig.getOption(
                TuttiConfigurationOption.EXPORT_COUNTRY_ID.getKey());
    }

    public List<Integer> getSamplingOrderIds() {
        ApplicationConfig.OptionList result = applicationConfig.getOptionAsList(TuttiConfigurationOption.SAMPLING_CATEGORY_ORDER_IDS.getKey());
        return result.getOptionAsInt();
    }

    public WeightUnit getSpeciesWeightUnit() {
        return applicationConfig.getOption(WeightUnit.class, TuttiConfigurationOption.WEIGHT_UNIT_SPECIES.getKey());
    }

    public WeightUnit getBenthosWeightUnit() {
        return applicationConfig.getOption(WeightUnit.class, TuttiConfigurationOption.WEIGHT_UNIT_BENTHOS.getKey());
    }

    public WeightUnit getMarineLitterWeightUnit() {
        return applicationConfig.getOption(WeightUnit.class, TuttiConfigurationOption.WEIGHT_UNIT_MARINE_LITTER.getKey());
    }

    public WeightUnit getIndividualObservationWeightUnit() {
        return applicationConfig.getOption(WeightUnit.class, TuttiConfigurationOption.WEIGHT_UNIT_INDIVIDUAL_OBSERVATION.getKey());
    }

    public WeightUnit getAccidentalCatchWeightUnit() {
        return applicationConfig.getOption(WeightUnit.class, TuttiConfigurationOption.WEIGHT_UNIT_ACCIDENTAL_CATCH.getKey());
    }

    public boolean isFullLaunchMode() {
        return "full".equals(getTuttiLaunchMode());
    }

    public String getTuttiLaunchMode() {
        return applicationConfig.getOption(TuttiConfigurationOption.LAUNCH_MODE.getKey());
    }

    /** @return {@link TuttiConfigurationOption#UI_CONFIG_FILE} value */
    public File getUIConfigFile() {
        return applicationConfig.getOptionAsFile(TuttiConfigurationOption.UI_CONFIG_FILE.getKey());
    }

    /** @return {@link TuttiConfigurationOption#START_ACTION_FILE} value */
    public File getStartActionFile() {
        return applicationConfig.getOptionAsFile(TuttiConfigurationOption.START_ACTION_FILE.getKey());
    }

    public Float getDifferenceRateBetweenSortedAndTotalWeights() {
        return applicationConfig.getOptionAsFloat(TuttiConfigurationOption.TOTAL_SORTED_WEIGHTS_DIFFERENCE_RATE.getKey());
    }

    public Float getDifferenceRateBetweenWeightAndRtpWeight() {
        return applicationConfig.getOptionAsFloat(TuttiConfigurationOption.RTP_WEIGHTS_DIFFERENCE_RATE.getKey());
    }

    public boolean getPupitriImportMissingBatches() {
        return applicationConfig.getOptionAsBoolean(TuttiConfigurationOption.PUPITRI_IMPORT_MISSING_BATCHES.getKey());
    }

    public String getTremieCarousselVesselId() {
        return applicationConfig.getOption(TuttiConfigurationOption.TREMIE_CAROUSSEL_VESSEL_ID.getKey());
    }

    public boolean isAutoPopupNumberEditor() {
        return applicationConfig.getOptionAsBoolean(TuttiConfigurationOption.AUTO_POPUP_NUMBER_EDITOR.getKey());
    }

    public boolean isShowNumberEditorButton() {
        return applicationConfig.getOptionAsBoolean(TuttiConfigurationOption.SHOW_NUMBER_EDITOR_BUTTON.getKey());
    }

    public CoordinateEditorType getCoordinateEditorType() {
        String value = applicationConfig.getOption(TuttiConfigurationOption.COORDINATE_EDITOR_TYPE.getKey());
        return CoordinateEditorType.valueOf(value);
    }

    public String getProgramId() {
        String result = applicationConfig.getOption(TuttiConfigurationOption.PROGRAM_ID.getKey());
        return StringUtils.isBlank(result) ? null : result;
    }

    public Integer getCruiseId() {
        String result = applicationConfig.getOption(TuttiConfigurationOption.CRUISE_ID.getKey());
        return StringUtils.isBlank(result) ? null : Integer.valueOf(result);
    }

    public String getProtocolId() {
        String result = applicationConfig.getOption(TuttiConfigurationOption.PROTOCOL_ID.getKey());
        return StringUtils.isBlank(result) ? null : result;
    }

    public Color getColorRowInvalid() {
        return applicationConfig.getOptionAsColor(TuttiConfigurationOption.COLOR_ROW_INVALID.getKey());
    }

    public Color getColorRowReadOnly() {
        return applicationConfig.getOptionAsColor(TuttiConfigurationOption.COLOR_ROW_READ_ONLY.getKey());
    }

    public Color getColorCellWithValue() {
        return applicationConfig.getOptionAsColor(TuttiConfigurationOption.COLOR_CELL_WITH_VALUE.getKey());
    }

    public Color getColorRowToConfirm() {
        return applicationConfig.getOptionAsColor(TuttiConfigurationOption.COLOR_ROW_TO_CONFIRM.getKey());
    }

    public Color getColorComputedWeights() {
        return applicationConfig.getOptionAsColor(TuttiConfigurationOption.COLOR_COMPUTED_WEIGHTS.getKey());
    }

    public Color getColorBlockingLayer() {
        return applicationConfig.getOptionAsColor(TuttiConfigurationOption.COLOR_BLOCKING_LAYER.getKey());
    }

    public Color getColorAlternateRow() {
        return applicationConfig.getOptionAsColor(TuttiConfigurationOption.COLOR_ALTERNATE_ROW.getKey());
    }

    public Color getColorSelectedRow() {
        return applicationConfig.getOptionAsColor(TuttiConfigurationOption.COLOR_SELECTED_ROW.getKey());
    }

    public Color getColorWarningRow() {
        return applicationConfig.getOptionAsColor(TuttiConfigurationOption.COLOR_WARNING_ROW.getKey());
    }

    public Color getColorCatch() {
        return applicationConfig.getOptionAsColor(TuttiConfigurationOption.COLOR_CATCH.getKey());
    }

    public Color getColorSpecies() {
        return applicationConfig.getOptionAsColor(TuttiConfigurationOption.COLOR_SPECIES.getKey());
    }

    public Color getColorBenthos() {
        return applicationConfig.getOptionAsColor(TuttiConfigurationOption.COLOR_BENTHOS.getKey());
    }

    public Color getColorSpeciesOrBenthosUnsortedComputedWeightInWarning() {
        return applicationConfig.getOptionAsColor(TuttiConfigurationOption.COLOR_SPECIES_OR_BENTHOS_UNSORTED_COMPUTED_WEIGHT_IN_WARNING.getKey());
    }

    public Color getColorMarineLitter() {
        return applicationConfig.getOptionAsColor(TuttiConfigurationOption.COLOR_MARINE_LITTER.getKey());
    }

    public Color getColorHighlightInfoBackground() {
        return applicationConfig.getOptionAsColor(TuttiConfigurationOption.COLOR_HIGHLIGHT_INFO_BACKGROUND.getKey());
    }

    public Color getColorHighlightInfoForeground() {
        return applicationConfig.getOptionAsColor(TuttiConfigurationOption.COLOR_HIGHLIGHT_INFO_FOREGROUND.getKey());
    }

    @Override
    public KeyStroke getShortcutClosePopup() {
        return applicationConfig.getOptionAsKeyStroke(TuttiConfigurationOption.SHORTCUT_CLOSE_POPUP.getKey());
    }

    public int getMaxIndividualObservationRowsToCreate() {
        return applicationConfig.getOptionAsInt(TuttiConfigurationOption.MAX_INDIVIDUAL_OBSERVATION_ROW_TO_CREATE.getKey());
    }

    public String getSamplingCodePrefix() {
        return applicationConfig.getOption(TuttiConfigurationOption.SAMPLING_CODE_PREFIX.getKey());
    }

    public KeyStroke getShortCut(String actionName) {
        return applicationConfig.getOptionAsKeyStroke(
                "tutti.ui." + actionName);
    }

    public String getDateFormat() {
        return applicationConfig.getOption(TuttiConfigurationOption.DATE_FORMAT.getKey());
    }

    public File getNewTmpDirectory(String name) {
        return new File(getTmpDirectory(), name + "_" + System.nanoTime());
    }


    public File getI18nDirectory() {
        return applicationConfig.getOptionAsFile(
                TuttiConfigurationOption.I18N_DIRECTORY.getKey());
    }

    public Locale getI18nLocale() {
        return applicationConfig.getOptionAsLocale(
                TuttiConfigurationOption.I18N_LOCALE.getKey());
    }

    public File getHelpDirectory() {
        return applicationConfig.getOptionAsFile(
                TuttiConfigurationOption.HELP_DIRECTORY.getKey());
    }

    public String getUpdateApplicationUrl() {
        // Trim url values, because of auk theory! (See #7946)
        return applicationConfig.getOption(TuttiConfigurationOption.UPDATE_APPLICATION_URL.getKey()).trim();
    }

    public String getUpdateDataUrl() {
        // Trim url values, because of auk theory! (See #7946)
        return applicationConfig.getOption(TuttiConfigurationOption.UPDATE_DATA_URL.getKey()).trim();
    }

    public String getAdminPassword() {
        return applicationConfig.getOption(TuttiConfigurationOption.ADMIN_PASSWORD.getKey());
    }

    public boolean isFullBluetoothScan() {
        return applicationConfig.getOptionAsBoolean(TuttiConfigurationOption.FULL_BLUETOOTH_SCAN.getKey());
    }

    public int getIchtyometerMaximumNumberOfAttemptToConnect() {
        return applicationConfig.getOptionAsInt(TuttiConfigurationOption.ICHTYOMETER_MAXIMUM_NUMBER_OF_ATTEMPT_TO_CONNECT.getKey());
    }

    public int getCaliperSerialPort() {
        return applicationConfig.getOptionAsInt(TuttiConfigurationOption.CALIPER_SERIAL_PORT.getKey());
    }

    public Boolean isExternalDevicesVoiceEnabled() {
        return applicationConfig.getOptionAsBoolean(TuttiConfigurationOption.EXTERNAL_DEVICES_VOICE_ENABLED.getKey());
    }

    public Boolean isExternalDevicesReadsUnit() {
        return applicationConfig.getOptionAsBoolean(TuttiConfigurationOption.EXTERNAL_DEVICES_READS_UNIT.getKey());
    }

    public Boolean isExternalDevicesDataReceptionBeepEnabled() {
        return applicationConfig.getOptionAsBoolean(TuttiConfigurationOption.EXTERNAL_DEVICES_DATA_RECEPTION_BEEP_ENABLED.getKey());
    }

    public BeepFrequency getExternalDevicesDataReceptionBeepFrequency() {
        return BeepFrequency.valueOf(applicationConfig.getOption(TuttiConfigurationOption.EXTERNAL_DEVICES_DATA_RECEPTION_BEEP_FREQUENCY.getKey()));
    }

    public Boolean isExternalDevicesErrorReceptionBeepEnabled() {
        return applicationConfig.getOptionAsBoolean(TuttiConfigurationOption.EXTERNAL_DEVICES_ERROR_RECEPTION_BEEP_ENABLED.getKey());
    }

    public BeepFrequency getExternalDevicesErrorReceptionBeepFrequency() {
        return BeepFrequency.valueOf(applicationConfig.getOption(TuttiConfigurationOption.EXTERNAL_DEVICES_ERROR_RECEPTION_BEEP_FREQUENCY.getKey()));
    }

    public String getJavaCommandPath() {
        String path;
        String commandName = windowsOS ? "java.exe" : "java";
        if (isFullLaunchMode()) {
            path = getBasedir().toPath().resolve("jre").resolve("bin").resolve(commandName).toFile().getAbsolutePath();
        } else {
            path = commandName;
        }
        return path;
    }

    private File reportJarPath;

    public File getReportJarPath() {
        if (reportJarPath == null) {
            //FIXME Should include version in file name
            String commandName = "tutti-report-generator.jar";
            if (isFullLaunchMode()) {

                reportJarPath = getBasedir().toPath().resolve("tutti").resolve("report-generator").resolve(commandName).toFile();

            } else {

                reportJarPath = guessReportJarPath(commandName);

                if (reportJarPath == null) {

                    if (log.isWarnEnabled()) {
                        log.warn("You must defined yourself the path to " + commandName);
                    }

                } else {

                    if (log.isInfoEnabled()) {
                        log.info("Guess reportGenerarorPath: "+reportJarPath);
                    }
                }

            }
        }

        return reportJarPath;

    }

    public void setReportJarPath(File reportJarPath) {
        this.reportJarPath = reportJarPath;
    }

    protected File guessReportJarPath(String commandName) {

        {
            File file = getBasedir().toPath().resolve("tutti-report-generator").resolve(commandName).toFile();
            if (file.exists()) {
                return file;
            }
        }

        {
            File file = new File("").toPath().resolve("target").resolve("tutti-report-generator").resolve(commandName).toFile();
            if (file.exists()) {
                return file;
            }
        }

        {
            File file = new File("tutti-ui-swing").toPath().resolve("target").resolve("tutti-report-generator").resolve(commandName).toFile();
            if (file.exists()) {
                return file;
            }
        }

        return null;

    }
}

package fr.ifremer.tutti;

/*
 * #%L
 * Tutti :: Persistence
 * %%
 * Copyright (C) 2012 - 2014 Ifremer
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.tutti.persistence.entities.data.SampleCategoryModel;
import fr.ifremer.tutti.type.CoordinateEditorType;
import fr.ifremer.tutti.type.WeightUnit;
import fr.ifremer.tutti.util.BeepFrequency;
import org.hibernate.dialect.HSQLDialect;
import org.hsqldb.jdbcDriver;
import org.nuiton.config.ConfigOptionDef;
import org.nuiton.version.Version;

import javax.swing.KeyStroke;
import java.awt.Color;
import java.io.File;
import java.net.URL;
import java.util.Locale;

import static org.nuiton.i18n.I18n.n;

/**
 * All application configuration options.
 *
 * Created on 11/24/13.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 3.0
 */
public enum TuttiConfigurationOption implements ConfigOptionDef {

    //------------------------------------------------------------------------//
    //-- READ-ONLY OPTIONS ---------------------------------------------------//
    //------------------------------------------------------------------------//

    BASEDIR(
            "tutti.basedir",
            n("tutti.config.option.basedir.description"),
            "${user.home}/.tutti",
            File.class),

    DATA_DIRECTORY(
            "tutti.data.directory",
            n("tutti.config.option.data.directory.description"),
            "${tutti.basedir}/data",
            File.class),

    I18N_DIRECTORY(
            "tutti.i18n.directory",
            n("tutti.config.option.i18n.directory.description"),
            "${tutti.basedir}/i18n",
            File.class),

    HELP_DIRECTORY(
            "tutti.help.directory",
            n("tutti.config.option.help.directory.description"),
            "${tutti.basedir}/help",
            File.class),

    TMP_DIRECTORY(
            "tutti.tmp.directory",
            n("tutti.config.option.tmp.directory.description"),
            "${tutti.data.directory}/temp",
            File.class),

    REPORT_HOME_DIRECTORY(
            "tutti.report.home.directory",
            n("tutti.config.option.report.home.directory.description"),
            "${tutti.basedir}/report",
            File.class),
    REPORT_DIRECTORY(
            "tutti.report.directory",
            n("tutti.config.option.report.directory.description"),
            "${tutti.report.home.directory}/allegro-tutti/reports",
            File.class),
    REPORT_LOG_DIRECTORY(
            "tutti.report.log.directory",
            n("tutti.config.option.report.log.directory.description"),
            "${tutti.data.directory}/reportlogs",
            File.class),
    DB_DIRECTORY(
            "tutti.persistence.db.directory",
            n("tutti.config.option.persistence.db.directory.description"),
            "${tutti.data.directory}/db",
            File.class),

    DB_ATTACHMENT_DIRECTORY(
            "tutti.persistence.db.attachment.directory",
            n("tutti.config.option.persistence.db.attachment.directory.description"),
            "${tutti.data.directory}/meas_files",
            File.class),

    DB_CACHE_DIRECTORY(
            "tutti.persistence.db.cache.directory",
            n("tutti.config.option.persistence.db.cache.directory.description"),
            "${tutti.data.directory}/dbcache",
            File.class),

    DB_PROTOCOL_DIRECTORY(
            "tutti.persistence.db.protocol.directory",
            n("tutti.config.option.persistence.db.protocol.directory.description"),
            "${tutti.data.directory}/protocol",
            File.class),

    DB_NAME(
            "tutti.persistence.db.name",
            n("tutti.config.option.persistence.db.name.description"),
            "allegro",
            String.class),

    DB_SANITY(
            "tutti.persistence.db.sanity",
            n("tutti.persistence.db.sanity.description"),
            "false",
            Boolean.class),

    JDBC_USERNAME(
            "tutti.persistence.jdbc.username",
            n("tutti.config.option.persistence.jdbc.username.description"),
            "sa",
            String.class),

    JDBC_PASSWORD(
            "tutti.persistence.jdbc.password",
            n("tutti.config.option.persistence.jdbc.password.description"),
            "",
            String.class),

    JDBC_URL(
            "tutti.persistence.jdbc.url",
            n("tutti.config.option.persistence.jdbc.url.description"),
            "jdbc:hsqldb:file:${tutti.persistence.db.directory}/${tutti.persistence.db.name}",
            String.class,
            false),

    JDBC_DRIVER(
            "tutti.persistence.jdbc.driver",
            n("tutti.config.option.persistence.jdbc.driver.description"),
            jdbcDriver.class.getName(),
            Class.class),

    HIBERNATE_DIALECT(
            "tutti.persistence.hibernate.dialect",
            n("tutti.config.option.persistence.hibernate.dialect.description"),
            HSQLDialect.class.getName(),
            Class.class),

    LAUNCH_MODE(
            "tutti.launch.mode",
            n("tutti.config.option.launch.mode.description"),
            null,
            String.class),

    START_ACTION_FILE(
            "tutti.startActionFile",
            n("tutti.config.option.startActionFile.description"),
            "${tutti.basedir}/tutti-start-action",
            File.class),

    VERSION(
            "tutti.version",
            n("tutti.config.option.version.description"),
            "",
            Version.class),

    SITE_URL(
            "tutti.site.url",
            n("tutti.config.option.site.url.description"),
            "https://tutti.codelutin.com",
            URL.class),

    ORGANIZATION_NAME(
            "tutti.organizationName",
            n("tutti.config.option.organizationName.description"),
            "",
            String.class),

    INCEPTION_YEAR(
            "tutti.inceptionYear",
            n("tutti.config.option.inceptionYear.description"),
            "2012",
            Integer.class),

    //------------------------------------------------------------------------//
    //-- READ-WRITE OPTIONS --------------------------------------------------//
    //------------------------------------------------------------------------//

    DB_BACKUP_DIRECTORY(
            "tutti.persistence.db.backup.directory",
            n("tutti.config.option.persistence.db.backup.directory.description"),
            "${tutti.data.directory}/dbbackup",
            File.class,
            false),

    EXPORT_BACKUP_DIRECTORY(
            "tutti.export.backup.directory",
            n("tutti.config.option.export.backup.directory.description"),
            "${tutti.data.directory}/exportbackup",
            File.class,
            false),

    HIBERNATE_SHOW_SQL(
            "tutti.persistence.hibernate.showSql",
            n("tutti.config.option.persistence.hibernate.showSql.description"),
            Boolean.FALSE.toString(),
            boolean.class,
            false),

    HIBERNATE_USE_SQL_COMMENT(
            "tutti.persistence.hibernate.useSqlComment",
            n("tutti.config.option.persistence.hibernate.useSqlComment.description"),
            Boolean.FALSE.toString(),
            boolean.class,
            false),

    HIBERNATE_FORMAT_SQL(
            "tutti.persistence.hibernate.formatSql",
            n("tutti.config.option.persistence.hibernate.formatSql.description"),
            Boolean.FALSE.toString(),
            boolean.class,
            false),

    SHOW_BATCH_LOG(
            "tutti.persistence.showBatchLog",
            n("tutti.config.option.persistence.showBatchLog.description"),
            Boolean.FALSE.toString(),
            Boolean.class,
            false),

    SHOW_MEMORY_USAGE(
            "tutti.persistence.showMemoryUsage",
            n("tutti.config.option.persistence.showMemoryUsage.description"),
            Boolean.FALSE.toString(),
            Boolean.class,
            false),

    GENERIC_FORMAT_IMPORT_SKIP_BACKUP(
            "tutti.genericFormat.import.skipBackup",
            n("tutti.config.option.genericFormat.import.skipBackup.description"),
            Boolean.FALSE.toString(),
            Boolean.class,
            false),
    GENERIC_FORMAT_IMPORT_MAXIMUM_ROWS_IN_ERROR_PER_FILE(
            "tutti.genericFormat.import.maximumRowsInErrorPerFile",
            n("tutti.config.option.genericFormat.import.maximumRowsInErrorPerFile.description"),
            "100",
            Integer.class,
            false),

    IMPORT_DB_SKIP_BACKUP(
            "tutti.db.import.skipBackup",
            n("tutti.config.option.db.import.skipBackup.description"),
            Boolean.FALSE.toString(),
            Boolean.class,
            false),

    SAMPLE_CATEGORY_MODEL(
            "tutti.persistence.SampleCategoryModel",
            n("tutti.config.option.persistence.SampleCategoryModel.description"),
            "1428,V/HV,V_HV|198,Class Tri,Class_Tri|196,Sexe,Sexe|174,Maturite,Maturite|1430,Age,Age",
            SampleCategoryModel.class,
            false
    ),

    REPORT_BACKUP_DIRECTORY(
            "tutti.report.backup.directory",
            n("tutti.config.option.report.backup.directory.description"),
            "${tutti.data.directory}/reportbackup",
            File.class,
            false),

    GENERIC_FORMAT_REPORT_BACKUP_DIRECTORY(
            "tutti.genericFormat.report.backup.directory",
            n("tutti.config.option.genericFormat.report.backup.directory.description"),
            "${tutti.data.directory}/genericFormat/reportbackup",
            File.class,
            false),

    CSV_SEPARATOR(
            "tutti.csv.separator",
            n("tutti.config.option.csv.separator.description"),
            ";",
            char.class,
            false),

    EXPORT_COUNTRY_ID(
            "tutti.export.countryId",
            n("tutti.config.option.export.countryId.description"),
            "12",
            String.class,
            false),

    SAMPLING_CATEGORY_ORDER_IDS(
            "tutti.samplingCategoryOrderIds",
            n("tutti.config.option.samplingCategoryOrderIds.description"),
            "198,196,174,1430",
            Integer[].class,
            false),

    WEIGHT_UNIT_SPECIES(
            "tutti.weight.unit.species",
            n("tutti.config.option.weight.unit.species.description"),
            WeightUnit.KG.name(),
            WeightUnit.class,
            false),

    WEIGHT_UNIT_BENTHOS(
            "tutti.weight.unit.benthos",
            n("tutti.config.option.weight.unit.benthos.description"),
            WeightUnit.G.name(),
            WeightUnit.class,
            false),

    WEIGHT_UNIT_MARINE_LITTER(
            "tutti.weight.unit.marineLitter",
            n("tutti.config.option.weight.unit.marineLitter.description"),
            WeightUnit.KG.name(),
            WeightUnit.class,
            false),

    WEIGHT_UNIT_INDIVIDUAL_OBSERVATION(
            "tutti.weight.unit.individualObservation",
            n("tutti.config.option.weight.unit.individualObservation.description"),
            WeightUnit.G.name(),
            WeightUnit.class,
            false),

    WEIGHT_UNIT_ACCIDENTAL_CATCH(
            "tutti.weight.unit.accidentalCatch",
            n("tutti.config.option.weight.unit.accidentalCatch.description"),
            WeightUnit.KG.name(),
            WeightUnit.class,
            false),

    I18N_LOCALE(
            "tutti.i18n.locale",
            n("tutti.config.option.i18n.locale.description"),
            Locale.FRANCE.getCountry(),
            Locale.class,
            false
    ),

    UPDATE_APPLICATION_URL(
            "tutti.update.application.url",
            n("tutti.config.option.update.application.url.description"),
            "https://www.ifremer.fr/sih-resource-private/tutti/tutti-application.properties",
            String.class,
            false
    ),

    UPDATE_DATA_URL(
            "tutti.update.data.url",
            n("tutti.config.option.update.data.url.description"),
            "https://www.ifremer.fr/sih-resource-private/tutti/tutti-data.properties",
            String.class,
            false
    ),

    UI_CONFIG_FILE(
            "tutti.ui.config.file",
            n("tutti.config.option.ui.config.file.description"),
            "${tutti.data.directory}/tuttiUI.xml",
            File.class,
            false
    ),

    TOTAL_SORTED_WEIGHTS_DIFFERENCE_RATE(
            "tutti.db.weights.rate.difference.totalAndSorted",
            n("tutti.config.option.weights.rate.difference.totalAndSorted.description"),
            "1.0",
            Float.class,
            false
    ),

    PUPITRI_IMPORT_MISSING_BATCHES(
            "tutti.pupitriImportMissingBatches",
            n("tutti.config.option.pupitriImportMissingBatches.description"),
            "false",
            Boolean.class,
            false
    ),

    TREMIE_CAROUSSEL_VESSEL_ID(
            "tutti.tremieCarousselVessel",
            n("tutti.config.option.tremieCarousselVessel.description"),
            "",
            String.class,
            false
    ),

    RTP_WEIGHTS_DIFFERENCE_RATE(
            "tutti.db.weights.rate.difference.rtp",
            n("tutti.config.option.weights.rate.difference.rtp.description"),
            "20.0",
            Float.class,
            false
    ),

    PROGRAM_ID(
            "tutti.programId",
            n("tutti.config.option.programId.description"),
            null,
            String.class,
            false
    ),

    CRUISE_ID(
            "tutti.cruiseId",
            n("tutti.config.option.cruiseId.description"),
            null,
            Integer.class,
            false
    ),

    PROTOCOL_ID(
            "tutti.protocolId",
            n("tutti.config.option.protocolId.description"),
            null,
            String.class,
            false
    ),

    AUTO_POPUP_NUMBER_EDITOR(
            "tutti.ui.autoPopupNumberEditor",
            n("tutti.config.option.ui.autoPopupNumberEditor.description"),
            String.valueOf(false),
            Boolean.class,
            false
    ),

    SHOW_NUMBER_EDITOR_BUTTON(
            "tutti.ui.showNumberEditorButton",
            n("tutti.config.option.ui.showNumberEditorButton.description"),
            String.valueOf(true),
            Boolean.class,
            false
    ),

    COLOR_ROW_READ_ONLY(
            "tutti.ui.color.rowReadOnly",
            n("tutti.config.option.ui.color.rowReadOnly.description"),
            new Color(192, 192, 192).toString(),
            Color.class,
            false
    ),

    COLOR_ROW_INVALID(
            "tutti.ui.color.rowInvalid",
            n("tutti.config.option.ui.color.rowInvalid.description"),
            new Color(255, 204, 153).toString(),
            Color.class,
            false
    ),

    COLOR_CELL_WITH_VALUE(
            "tutti.ui.color.cellWithValue",
            n("tutti.config.option.ui.color.cellWithValue.description"),
            new Color(128, 255, 128).toString(),
            Color.class,
            false
    ),

    COLOR_ROW_TO_CONFIRM(
            "tutti.ui.color.rowToConfirm",
            n("tutti.config.option.ui.color.rowToConfirm.description"),
            Color.ORANGE.toString(),
            Color.class,
            false
    ),

    COLOR_ALTERNATE_ROW(
            "tutti.ui.color.alternateRow",
            n("tutti.config.option.ui.color.alternateRow.description"),
            new Color(217, 217, 217).toString(),
            Color.class,
            false
    ),

    COLOR_SELECTED_ROW(
            "tutti.ui.color.selectedRow",
            n("tutti.config.option.ui.color.selectedRow.description"),
            new Color(57, 105, 138).toString(),
            Color.class,
            false
    ),

    COLOR_WARNING_ROW(
            "tutti.ui.color.warningRow",
            n("tutti.config.option.ui.color.warningRow.description"),
            new Color(245, 218, 88).toString(),
            Color.class,
            false
    ),

    COLOR_CATCH(
            "tutti.ui.color.catch",
            n("tutti.config.option.ui.color.catch.description"),
            new Color(136,136,136).toString(),
            Color.class,
            false
    ),

    COLOR_SPECIES(
            "tutti.ui.color.species",
            n("tutti.config.option.ui.color.species.description"),
            new Color(0, 107, 186).toString(),
            Color.class,
            false
    ),

    COLOR_BENTHOS(
            "tutti.ui.color.benthos",
            n("tutti.config.option.ui.color.benthos.description"),
            new Color(255, 127, 42).toString(),
            Color.class,
            false
    ),
    COLOR_SPECIES_OR_BENTHOS_UNSORTED_COMPUTED_WEIGHT_IN_WARNING(
            "tutti.ui.color.speciesOrBenthosUnsortedComputedWeightInWarning",
            n("tutti.config.option.ui.color.speciesOrBenthosUnsortedComputedWeightInWarning.description"),
            new Color(255, 57, 28).toString(),
            Color.class,
            false
    ),

    COLOR_MARINE_LITTER(
            "tutti.ui.color.marineLitter",
            n("tutti.config.option.ui.color.marineLitter.description"),
            new Color(255, 229, 23).toString(),
            Color.class,
            false
    ),

    COLOR_BLOCKING_LAYER(
            "tutti.ui.color.blockingLayer",
            n("tutti.config.option.ui.color.blockingLayer.description"),
            new Color(200, 200, 200).toString(),
            Color.class,
            false
    ),

    COLOR_COMPUTED_WEIGHTS(
            "tutti.ui.color.computedWeights",
            n("tutti.config.option.ui.color.computedWeights.description"),
            Color.BLUE.toString(),
            Color.class,
            false
    ),

    COLOR_HIGHLIGHT_INFO_BACKGROUND(
            "tutti.ui.color.hightlightInfo.background",
            n("tutti.config.option.ui.color.hightlightInfo.background.description"),
            Color.YELLOW.toString(),
            Color.class,
            false
    ),

    COLOR_HIGHLIGHT_INFO_FOREGROUND(
            "tutti.ui.color.hightlightInfo.foreground",
            n("tutti.config.option.ui.color.hightlightInfo.foreground.description"),
            Color.BLACK.toString(),
            Color.class,
            false
    ),

    SHORTCUT_CLOSE_POPUP(
            "tutti.ui.shortcut.closePopup",
            n("tutti.config.option.ui.shortcut.closePopup.description"),
            "alt pressed F",
            KeyStroke.class,
            false
    ),

    COORDINATE_EDITOR_TYPE(
            "tutti.ui.coordinateEditorType",
            n("tutti.config.option.ui.coordinateEditorType.description"),
            CoordinateEditorType.DD.toString(),
            CoordinateEditorType.class,
            false
    ),

    DATE_FORMAT(
            "tutti.ui.dateFormat",
            n("tutti.config.option.ui.dateFormat.description"),
            "dd/MM/yyyy",
            String.class,
            false
    ),

    ADMIN_PASSWORD(
            "tutti.admin.password",
            n("tutti.config.option.ui.adminPassword.description"),
            // "ifremer" by default
            "ef399a6e6ec62c6e142440241a5b98f8",
            String.class,
            false
    ),

    MAX_INDIVIDUAL_OBSERVATION_ROW_TO_CREATE(
            "tutti.ui.max.individualObservationRowsToCreate",
            n("tutti.config.option.ui.individualObservationRowsToCreate.description"),
            "10",
            Integer.class,
            false
    ),

    SAMPLING_CODE_PREFIX(
            "tutti.ui.max.sampligCodePrefix",
            n("tutti.config.option.ui.sampligCodePrefix.description"),
            "",
            String.class,
            false
    ),

    FULL_BLUETOOTH_SCAN(
            "tutti.ichtyometer.fullBluetoothScan",
            n("tutti.config.option.ichtyometer.fullBluetoothScan.description"),
            "false",
            Boolean.class,
            false
    ),
    ICHTYOMETER_MAXIMUM_NUMBER_OF_ATTEMPT_TO_CONNECT(
            "tutti.ichtyometer.maximumNumberOfAttemptToConnect",
            n("tutti.config.option.ichtyometer.maximumNumberOfAttemptToConnect.description"),
            "5",
            int.class,
            false
    ),
    CALIPER_SERIAL_PORT(
            "tutti.ichtyometer.caliperSerialPort",
            n("tutti.config.option.caliper.serialPort.description"),
            "1",
            int.class,
            false
    ),
    EXTERNAL_DEVICES_VOICE_ENABLED(
            "tutti.ichtyometer.voice.enabled",
            n("tutti.config.option.externalDevices.voice.enabled.description"),
            "true",
            Boolean.class,
            false
    ),
    EXTERNAL_DEVICES_READS_UNIT(
            "tutti.ichtyometer.reads.unit",
            n("tutti.config.option.externalDevices.reads.unit.description"),
            "true",
            Boolean.class,
            false
    ),
    EXTERNAL_DEVICES_DATA_RECEPTION_BEEP_ENABLED(
            "tutti.ichtyometer.beep.data.enabled",
            n("tutti.config.option.externalDevices.beep.data.enabled.description"),
            "true",
            Boolean.class,
            false
    ),
    EXTERNAL_DEVICES_DATA_RECEPTION_BEEP_FREQUENCY(
            "tutti.ichtyometer.beep.data.frequency",
            n("tutti.config.option.externalDevices.beep.data.frequency.description"),
            "A2",
            BeepFrequency.class,
            false
    ),
    EXTERNAL_DEVICES_ERROR_RECEPTION_BEEP_ENABLED(
            "tutti.ichtyometer.beep.error.enabled",
            n("tutti.config.option.externalDevices.beep.error.enabled.description"),
            "true",
            Boolean.class,
            false
    ),
    EXTERNAL_DEVICES_ERROR_RECEPTION_BEEP_FREQUENCY(
            "tutti.ichtyometer.beep.error.frequency",
            n("tutti.config.option.externalDevices.beep.error.frequency.description"),
            "A1",
            BeepFrequency.class,
            false
    );

    /** Configuration key. */
    private final String key;

    /** I18n key of option description */
    private final String description;

    /** Type of option */
    private final Class<?> type;

    /** Default value of option. */
    private String defaultValue;

    /** Flag to not keep option value on disk */
    private boolean isTransient;

    /** Flag to not allow option value modification */
    private boolean isFinal;

    TuttiConfigurationOption(String key,
                             String description,
                             String defaultValue,
                             Class<?> type,
                             boolean isTransient) {
        this.key = key;
        this.description = description;
        this.defaultValue = defaultValue;
        this.type = type;
        this.isTransient = isTransient;
        this.isFinal = isTransient;
    }

    TuttiConfigurationOption(String key,
                             String description,
                             String defaultValue,
                             Class<?> type) {
        this(key, description, defaultValue, type, true);
    }

    @Override
    public String getKey() {
        return key;
    }

    @Override
    public Class<?> getType() {
        return type;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public String getDefaultValue() {
        return defaultValue;
    }

    @Override
    public boolean isTransient() {
        return isTransient;
    }

    @Override
    public boolean isFinal() {
        return isFinal;
    }

    @Override
    public void setDefaultValue(String defaultValue) {
        this.defaultValue = defaultValue;
    }

    @Override
    public void setTransient(boolean newValue) {
        // not used
    }

    @Override
    public void setFinal(boolean newValue) {
        // not used
    }
}

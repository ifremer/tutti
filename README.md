Faire une nouvelle version mineure
----------------------------------

Cette release va juste déployer sur le dépot nexus les artifacts + mettre à jour
le fichier de mises à jour sur http://tutti.codelutin.com/update.

mvn jgitflow:release-start
mvn jgitflow:release-finish

Faire une nouvelle version majeure
----------------------------------

Cette release va en plus générer et deployer sur forge.codelutin.com les zip
incluant la jre + l'application.

mvn jgitflow:release-start
mvn jgitflow:release-finish -Darguments="-DperformFullRelease -Ppost-release-profile"

À noter que pour le moment la génération du site n'est plus automatique,
il faut alors suite à la release faire:

git checkout master
mvn clean verify site-deploy -DperformRelease

Faire une nouvelle version du lanceur d'application
---------------------------------------------------

Mettre à jour la propriété *launcherVersion* dans le pom parent.

Ajouter la propriété -DperformLauncherRelease lors de la release.

Cela produira l'archive tutti-launcher-${launcherVersion}.zip qui sera déployée sur le nexus puis utilisée
pour les mises à jour.